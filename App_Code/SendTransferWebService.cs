﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Text;

/// <summary>
/// Summary description for SendTransferWebService
/// </summary>
public class SendTransferWebService
{
	public SendTransferWebService()
	{
		//
		// TODO: Add constructor logic here
		//
       



	}
    public string TransferSearch(string city, string pickcode, string dropcode, string transferdate, string pickuppoint,string droppoint)
    {

        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchTransferPriceRequest");
        XMLChildNode = XMLDoc.CreateElement("ImmediateConfirmationOnly");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("TransferPickUp");
        XMLGrandChild = XMLDoc.CreateElement("PickUpCityCode");
        XMLGrandChild.InnerText = city;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLGrandChild = XMLDoc.CreateElement("PickUpCode");
        XMLGrandChild.InnerText = pickcode;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLGrandChild = XMLDoc.CreateElement("PickUpPointCode");
        XMLGrandChild.InnerText = pickuppoint;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("TransferDropOff");
        XMLGrandChild = XMLDoc.CreateElement("DropOffCode");
        XMLGrandChild.InnerText = dropcode;
        XMLChildNode.AppendChild(XMLGrandChild);
        if (pickcode == "H")
        {
            XMLGrandChild = XMLDoc.CreateElement("DropOffPointCode");
            XMLGrandChild.InnerText = droppoint;
            XMLChildNode.AppendChild(XMLGrandChild);
        }
        else
        {
            XMLGrandChild = XMLDoc.CreateElement("DropOffPointCode");
            XMLGrandChild.InnerText = droppoint;
            XMLChildNode.AppendChild(XMLGrandChild);

        }

        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("TransferDate");
        XMLChildNode.InnerText = transferdate;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("NumberOfPassengers");
        XMLChildNode.InnerText = "2";
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("PreferredLanguage");
        XMLChildNode.InnerText = "E";
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("IncludeChargeConditions");
        XMLChildNode.SetAttribute("DateFormatResponse", "true");

        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "SearchTransferPrice");
    }

    public string HotelSearch(string subcityname)
    {
    
       
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        //return SendRequest(XMLDoc, "City");
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchItemRequest");
        XMLNode.SetAttribute("ItemType", "hotel");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");
        XMLChildNode.SetAttribute("DestinationCode", subcityname);
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "Hotel");
    }
    public string Cancelbooktransfer(string apiref)
   {
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("CancelBookingRequest");
        XMLChildNode= XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "api");
        XMLChildNode.InnerText = apiref;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "CancelBook");
    }
    public string SendRequest(XmlDocument XMLDoc, string SoapAction)
    {
        string strResult = "NORESULT";
        try
        {
            // post document 
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            Byte[] byte1 = encoding.GetBytes(XMLDoc.OuterXml);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            WebRequest HttpWReq = WebRequest.Create(HttpContext.Current.Session["interfaceurlval"].ToString());

            HttpWReq.ContentType = "text/xml";
            HttpWReq.ContentLength = XMLDoc.OuterXml.Length;
            HttpWReq.Method = "POST";

            Stream StreamData = HttpWReq.GetRequestStream();
            StreamData.Write(byte1, 0, byte1.Length);

            // get response
            WebResponse HttpWRes = HttpWReq.GetResponse();
            Stream receiveStream = HttpWRes.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();
            string strHeaders = HttpWRes.Headers.ToString();
            CreateXMLDoc(XMLDoc, strHeaders, strResult, SoapAction);
        }
        catch (WebException ex)
        {
            strResult = "NORESULT";
           
        }
        return strResult;


    }

    public string TransferBooking(string cabname, string cabcode, string seats, string price, string confirmstatuscode, string confirmstatus, string itemcode, string airport, string name, string flightnumber, string address, string city, string postcode, string citycode, string luggage, string arrivetime, string transferdate, string refid, string pickcodes,string picktime,string hotelcode)
    {
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("AddBookingRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.InnerText = refid;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("PaxNames");
        XMLGrandChild = XMLDoc.CreateElement("PaxName");
        XMLGrandChild.SetAttribute("PaxId", "1");
        XMLGrandChild.InnerText = name;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("BookingItems");
        XMLGrandChild = XMLDoc.CreateElement("BookingItem");
        XMLGrandChild.SetAttribute("ItemType", "transfer");
        XMLGrandGrandChild = XMLDoc.CreateElement("ItemReference");
        XMLGrandGrandChild.InnerText = "1";
        XMLGrandChild.AppendChild(XMLGrandGrandChild);
        XMLGrandGrandChild = XMLDoc.CreateElement("ItemCity");
        XMLGrandGrandChild.SetAttribute("Code", citycode);
        XMLGrandChild.AppendChild(XMLGrandGrandChild);
        XMLGrandGrandChild = XMLDoc.CreateElement("Item");
        XMLGrandGrandChild.SetAttribute("Code", itemcode);
        XMLGrandChild.AppendChild(XMLGrandGrandChild);
        XMLGrandGrandChild = XMLDoc.CreateElement("TransferItem");

        if (pickcodes == "A")
        {
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("PickUpDetails");
            XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("PickUpAirport");
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("ArrivingFrom");
            XMLGrandGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("Airport");
            XMLGrandGrandGrandGrandGrandGrandChild.SetAttribute("Code", airport);
            XMLGrandGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("FlightNumber");
            XMLGrandGrandGrandGrandGrandChild.InnerText = flightnumber;
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("EstimatedArrival");
            XMLGrandGrandGrandGrandGrandChild.InnerText = arrivetime;
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("DropOffDetails");
            XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("DropOffAccommodation");
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("DropOffTo");
            XMLGrandGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("Hotel");
            XMLGrandGrandGrandGrandGrandGrandChild.SetAttribute("Code", hotelcode);
            XMLGrandGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandGrandChild);         
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
        }
        else
        {
            decimal t = Convert.ToDecimal(arrivetime);
            decimal c = t - 5;
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("PickUpDetails");
            XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("PickUpAccommodation");
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("PickUpFrom");
            XMLGrandGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("Hotel");
            XMLGrandGrandGrandGrandGrandGrandChild.SetAttribute("Code", hotelcode);
            XMLGrandGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("PickUpTime");
            XMLGrandGrandGrandGrandGrandChild.InnerText = picktime;
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("DropOffDetails");
            XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("DropOffAirport");
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("DepartingTo");
            XMLGrandGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("Airport");
            XMLGrandGrandGrandGrandGrandGrandChild.SetAttribute("Code", airport);
            XMLGrandGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("FlightNumber");
            XMLGrandGrandGrandGrandGrandChild.InnerText = flightnumber;
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandGrandGrandChild = XMLDoc.CreateElement("DepartureTime");
            XMLGrandGrandGrandGrandGrandChild.InnerText = arrivetime;
            XMLGrandGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandGrandChild);
            XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);

        }
        XMLGrandGrandGrandChild = XMLDoc.CreateElement("TransferDate");
        XMLGrandGrandGrandChild.InnerText = transferdate;
        XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
        XMLGrandGrandGrandChild = XMLDoc.CreateElement("TransferVehicle");
        XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("Vehicle");
        XMLGrandGrandGrandGrandChild.SetAttribute("Code", cabcode);
        XMLGrandGrandGrandGrandChild.SetAttribute("MaximumLuggage", "3");
        XMLGrandGrandGrandGrandChild.SetAttribute("MaximumPassengers", seats);
        XMLGrandGrandGrandGrandChild.InnerText = cabname;
        XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
        XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("Passengers");
        XMLGrandGrandGrandGrandChild.InnerText = "1";
        XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
        XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("LeadPaxId");
        XMLGrandGrandGrandGrandChild.InnerText = "1";
        XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
        XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("ItemPrice");
        XMLGrandGrandGrandGrandChild.SetAttribute("Currency", "AED");
        XMLGrandGrandGrandGrandChild.InnerText = price;
        XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
        XMLGrandGrandGrandGrandChild = XMLDoc.CreateElement("Confirmation");
        XMLGrandGrandGrandGrandChild.SetAttribute("Code", confirmstatuscode);
        XMLGrandGrandGrandGrandChild.InnerText = confirmstatus;
        XMLGrandGrandGrandChild.AppendChild(XMLGrandGrandGrandGrandChild);
        XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
        XMLGrandChild.AppendChild(XMLGrandGrandChild);
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "TransferBooking");

    }
    public  string EmailVoucherBookActivity(string ETripName, string ETripID, string APIid, string Email, string mailname,string bookdate, string amount, string commision, string vehicle, string pick, string drop,string status)
    {
        EmailV eamilv = new EmailV();

            string userdt =  HttpContext.Current.Session["UserDetails"].ToString();
        //eamilv = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailV>(userdt);

        //eamilv = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailV>(userdt);
        //XmlDocument _document = new XmlDocument();
        //_document.LoadXml(userdt);
        //dynamic results = JObject.Parse(userdt);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        JArray jQueryObject = JArray.Parse(userdt);
        string qwe = (string)jQueryObject[0]["EmailID"];
        //  Dictionary dict = ser.Deserialize<Dictionary<string, object>>(input);
        // string emailid = (string)results[0]["EmailID"];

        // var dx = _document.DocumentElement.SelectSingleNode("/EmailID");

        string Out = string.Empty;
       
       BALSettings1 bals = new BALSettings1();
        //BALHotelBook BALAD = new BALHotelBook();
        //  BALAD.BOOKINGREFID = (HttpContext.Current.Session["ActBookRefID"]).ToString();
        //DataTable dtCheck = BALAD.CheckVochurlinkavailable();
        using (WebClient client = new WebClient())
        {
            string htmlCode = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/activitycomponents/EmailTemplates/activityemailvoucher.html");
            string ticks = DateTime.Now.Ticks.ToString();
            string path = System.Web.HttpContext.Current.Server.MapPath("") + "/activitycomponents/EmailTemplates/activityemailvoucher2.html";
           // string virtualFilePath = path.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
            // string VoucherUrl = "saf";

            string EmailSub = "Your Trip ID #GTARef#";
            string EmailBody = htmlCode;
            EmailSub = EmailSub.Replace("#GTARef#", ETripID);
           // EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Voucher");
            // EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Voucher");
            EmailBody = EmailBody.Replace("#GTARef#",ETripID);
            EmailBody = EmailBody.Replace("#PaxDetails#", mailname);

            EmailBody = EmailBody.Replace("#PaxDetails#", mailname);
            EmailBody = EmailBody.Replace("#refnum#", ETripID);
            EmailBody = EmailBody.Replace("#bookdate#", bookdate);
            EmailBody = EmailBody.Replace("#Amount#", amount);
            EmailBody = EmailBody.Replace("#comm#", commision);
            EmailBody = EmailBody.Replace("#status#", status);
            EmailBody = EmailBody.Replace("#pick#", pick);
            EmailBody = EmailBody.Replace("#drop#", drop);
            EmailBody = EmailBody.Replace("#veh#", vehicle);
            EmailBody = EmailBody.Replace("#DepartureDet#", ETripName);
            EmailBody = EmailBody.Replace("#ATTACHDET#", "Your voucher is attached along with this email");
            EmailBody = EmailBody.Replace("#bookingdate#",bookdate);
            //string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/BookingVoucher/" + ETripID + "/";
            //if (!Directory.Exists(storePath))
            //    Directory.CreateDirectory(storePath);
            TextWriter attachhtml = new StreamWriter(path);
            attachhtml.WriteLine(EmailBody);
            attachhtml.Flush();
            attachhtml.Close();
          // string voucherurl = "Documents/Bookingvoucher/" + ETripID + ".html";

            string virtualFilePath = path.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
            Out = bals.SendMailSubjectwithBcc(Email, qwe, "", EmailBody, EmailSub, virtualFilePath);



        }
        return Out;
    }
    public string Getsubcity(string pickupc, string subcity2)
    {
        Database2 db = new Database2();
        DataSet ds = new DataSet();
        ds = db.getData("select distinct Code from tblSubcity", "tblSubcity");
        string[] arrvalues = new string[ds.Tables[0].Rows.Count];

        //loopcounter
        for (int loopcounter = 0; loopcounter < ds.Tables[0].Rows.Count; loopcounter++)
        {
            //assign dataset values to array
            arrvalues[loopcounter] = ds.Tables[0].Rows[loopcounter]["Code"].ToString();
        }
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        return serializer.Serialize(arrvalues);
        //return arrvalues.ToString();

     

       
    }
    public string Citysearch(string country)
    {
    
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        //return SendRequest(XMLDoc, "City");
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchCityRequest");
        XMLNode.SetAttribute("CountryCode", "GB");
        XMLChildNode = XMLDoc.CreateElement("CityName");
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);



        return SendRequest(XMLDoc, "City");
        

    }
    public string[,] Countryload()
    {
        Database2 db = new Database2();
        DataSet ds = new DataSet();
        ds = db.getData("select * from TranferCountry", "TranferCountry");
        int dtcount = ds.Tables[0].Rows.Count;
        string[,] arrvalues = new string[dtcount,2];

        //loopcounter
        for (int loopcounter = 0; loopcounter < ds.Tables[0].Rows.Count; loopcounter++)
        {
            //assign dataset values to array
            arrvalues[loopcounter,0] = ds.Tables[0].Rows[loopcounter]["Code"].ToString();
            arrvalues[loopcounter,1] = ds.Tables[0].Rows[loopcounter]["Description"].ToString();

        }
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        string k= serializer.Serialize(arrvalues);
        return arrvalues;

    }
    public static string DataTableToJSONWithStringBuilder(DataTable table)
    {
        var JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            JSONString.Append("[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == table.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]");
        }
        return JSONString.ToString();
    }
    public string GetCityname2(string countryname)
    {
        Database3 db = new Database3();
        DataTable dt = new DataTable();
        dt = db.fillDataTable("select * from tblTransfercredentials where apicode='GTA'");

        string jdata = DataTableToJSONWithStringBuilder(dt);

        JArray credJSONOBJ = JArray.Parse(jdata);
        
        var Clientid = credJSONOBJ.Where(x => (string)x["keyname"] == "Clientid").FirstOrDefault();
        var EMailAddress = credJSONOBJ.Where(x => (string)x["keyname"] == "EMailAddress").FirstOrDefault();
        var Password = credJSONOBJ.Where(x => (string)x["keyname"] == "Password").FirstOrDefault();
        var interfaceurl = credJSONOBJ.Where(x => (string)x["keyname"] == "InterfaceURL").FirstOrDefault();
        string ClientidVal = Clientid["keyvalue"].ToString();
        string EMailAddressVal = EMailAddress["keyvalue"].ToString();
        string PasswordVal = Password["keyvalue"].ToString();
        string interfaceurlval= interfaceurl["keyvalue"].ToString();
        HttpContext.Current.Session["TransferClientID"] = ClientidVal;
        HttpContext.Current.Session["EMailAddressVal"] = EMailAddressVal;
        HttpContext.Current.Session["PasswordVal"] = PasswordVal;
        HttpContext.Current.Session["interfaceurlval"] = interfaceurlval;
        countryname = countryname.Replace(" ", String.Empty);
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        //return SendRequest(XMLDoc, "City");
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchCityRequest");
        XMLNode.SetAttribute("CountryCode", countryname);
        XMLChildNode = XMLDoc.CreateElement("CityName");
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);


        return SendRequest(XMLDoc, "City");


    }
    
    public string GetSubcityname1(string subcityname)
    {
        Database3 db = new Database3();
        DataTable dt = new DataTable();
        dt = db.fillDataTable("select * from tblTransfercredentials where apicode='GTA'");

        string jdata = DataTableToJSONWithStringBuilder(dt);

        JArray credJSONOBJ = JArray.Parse(jdata);

        var Clientid = credJSONOBJ.Where(x => (string)x["keyname"] == "Clientid").FirstOrDefault();
        var EMailAddress = credJSONOBJ.Where(x => (string)x["keyname"] == "EMailAddress").FirstOrDefault();
        var Password = credJSONOBJ.Where(x => (string)x["keyname"] == "Password").FirstOrDefault();
        var interfaceurl = credJSONOBJ.Where(x => (string)x["keyname"] == "InterfaceURL").FirstOrDefault();
        string ClientidVal = Clientid["keyvalue"].ToString();
        string EMailAddressVal = EMailAddress["keyvalue"].ToString();
        string PasswordVal = Password["keyvalue"].ToString();
        string interfaceurlval = interfaceurl["keyvalue"].ToString();
        HttpContext.Current.Session["TransferClientID"] = ClientidVal;
        HttpContext.Current.Session["EMailAddressVal"] = EMailAddressVal;
        HttpContext.Current.Session["PasswordVal"] = PasswordVal;
        HttpContext.Current.Session["interfaceurlval"] = interfaceurlval;
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        //return SendRequest(XMLDoc, "City");
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchItemInformationRequest");
        XMLNode.SetAttribute("ItemType", "transfer");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");
        XMLChildNode.SetAttribute("DestinationCode",subcityname);
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "Subcity");
    }
    public string Getcredential()

    {
        Database3 db = new Database3();
        DataTable dt = new DataTable();
        dt = db.fillDataTable("select * from tblTransfercredentials where apicode='GTA'");

        string jdata = DataTableToJSONWithStringBuilder(dt);

        JArray credJSONOBJ = JArray.Parse(jdata);

        var Clientid = credJSONOBJ.Where(x => (string)x["keyname"] == "Clientid").FirstOrDefault();
        var EMailAddress = credJSONOBJ.Where(x => (string)x["keyname"] == "EMailAddress").FirstOrDefault();
        var Password = credJSONOBJ.Where(x => (string)x["keyname"] == "Password").FirstOrDefault();
        var interfaceurl = credJSONOBJ.Where(x => (string)x["keyname"] == "InterfaceURL").FirstOrDefault();
        string ClientidVal = Clientid["keyvalue"].ToString();
        string EMailAddressVal = EMailAddress["keyvalue"].ToString();
        string PasswordVal = Password["keyvalue"].ToString();
        string interfaceurlval = interfaceurl["keyvalue"].ToString();
        HttpContext.Current.Session["TransferClientID"] = ClientidVal;
        HttpContext.Current.Session["EMailAddressVal"] = EMailAddressVal;
        HttpContext.Current.Session["PasswordVal"] = PasswordVal;
        HttpContext.Current.Session["interfaceurlval"] = interfaceurlval;

        return "sucesss";

    }
    public string GetdropSubcityname1(string subcityname1)
    {
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandChild;
        XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
        //return SendRequest(XMLDoc, "City");
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
        XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
        XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", "en");
        XMLNode.SetAttribute("Currency", "AED");
        XMLNode.SetAttribute("Country", "om");
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLHeader);
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchItemInformationRequest");
        XMLNode.SetAttribute("ItemType", "transfer");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");

        XMLChildNode.SetAttribute("DestinationCode", subcityname1);
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "Subcity");

    }
    public string Countrysearch()
    {
        try
        {
            XmlDocument XMLDoc = new XmlDocument();
            XmlElement XMLRequest;
            XmlElement XMLHeader;
            XmlElement XMLBody;
            XmlElement XMLNode;
            XmlElement XMLChildNode;
            XmlElement XMLGrandChild;
            XmlElement XMLGrandGrandChild;
            XmlElement XMLGrandGrandGrandChild;
            XmlElement XMLGrandGrandGrandGrandChild;
            XmlElement XMLGrandGrandGrandGrandGrandChild;
            XmlElement XMLGrandGrandGrandGrandGrandGrandChild;
            //return SendRequest(XMLDoc, "City");
            XMLRequest = XMLDoc.CreateElement("Request");
            XMLHeader = XMLDoc.CreateElement("Source");
            // client details node
            XMLNode = XMLDoc.CreateElement("RequestorID");
            XMLNode.SetAttribute("Client", HttpContext.Current.Session["TransferClientID"].ToString());
            XMLNode.SetAttribute("EMailAddress", HttpContext.Current.Session["EMailAddressVal"].ToString());
            XMLNode.SetAttribute("Password", HttpContext.Current.Session["PasswordVal"].ToString());
            XMLHeader.AppendChild(XMLNode);
            // client preference node
            XMLNode = XMLDoc.CreateElement("RequestorPreferences");
            XMLNode.SetAttribute("Language", "en");
            XMLNode.SetAttribute("Currency", "AED");
            XMLNode.SetAttribute("Country", "om");
            XMLChildNode = XMLDoc.CreateElement("RequestMode");
            XMLChildNode.InnerText = "SYNCHRONOUS";
            XMLNode.AppendChild(XMLChildNode);
            XMLHeader.AppendChild(XMLNode);
            XMLRequest.AppendChild(XMLHeader);
            XMLBody = XMLDoc.CreateElement("RequestDetails");
            XMLNode = XMLDoc.CreateElement("SearchCountryRequest");

            XMLChildNode = XMLDoc.CreateElement("CountryName");
            XMLNode.AppendChild(XMLChildNode);
            XMLBody.AppendChild(XMLNode);
            XMLRequest.AppendChild(XMLBody);
            XMLDoc.AppendChild(XMLRequest);

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            Byte[] byte1 = encoding.GetBytes(XMLDoc.OuterXml);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            WebRequest HttpWReq = WebRequest.Create("https://rs.gta-travel.com/wbsapi/RequestListenerServlet");

            HttpWReq.ContentType = "text/xml";
            HttpWReq.ContentLength = XMLDoc.OuterXml.Length;
            HttpWReq.Method = "POST";

            Stream StreamData = HttpWReq.GetRequestStream();
            StreamData.Write(byte1, 0, byte1.Length);

            // get response
            WebResponse HttpWRes = HttpWReq.GetResponse();
            Stream receiveStream = HttpWRes.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            string arr = sr.ReadToEnd();
            string strHeaders = HttpWRes.Headers.ToString();
            // CreateXMLDoc(XMLDoc, strHeaders, strResult, SoapAction);


            // return SendRequest(XMLDoc, "Subcity");
            XmlDocument _document = new XmlDocument();
            _document.LoadXml(arr);

            XmlNodeList parentNode = _document.GetElementsByTagName("Response");
            XmlNodeList xnList = _document.SelectNodes("/Response/ResponseDetails/SearchCountryResponse/CountryDetails/Country");
            //var str = _document.SelectNodes("/Response/ResponseDetails/SearchItemInformationResponse/ItemDetails/ItemDetail/City");

            foreach (XmlNode xn in xnList)
            {
                // XmlNodeList xnList1 = xn.SelectNodes("/ItemDetail/transferinformation");




                string countrycode = xn.Attributes["Code"].Value;
                string country = xn.InnerText;


                Database2 db = new Database2();
                string sql = "insert into TranferCountry(Code,Description)VALUES('" + countrycode + "','" + country + "')";
                db.executeNonQuery(sql);

            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
                  

        return "success";
       // return SendRequest(XMLDoc, "City");


    }
    public void CreateXMLDoc(XmlDocument XMLDATA, string XMLHEADER, string RESULT, string FileType)
    {
        try
        {
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/XmlFiles/" + DateTime.Now.ToString("yyyyMMdd") + "/" + "GTA" + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string TimeStamp = DateTime.UtcNow.AddHours(4).ToString("HH-mm-ss");
            string filename = FileType + "-Request-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilename = storePath + "/" + filename;
            string filenameHeader = FileType + "-Header-" + finalsesID + "-" + TimeStamp + ".txt";
            string tempfilenameHeader = storePath + "/" + filenameHeader;
            string filenameResult = FileType + "-Response-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilenameResult = storePath + "/" + filenameResult;

           

            XMLDATA.Save(tempfilename);
            File.WriteAllText(tempfilenameHeader, XMLHEADER);
            XmlDocument XMLResult = new XmlDocument();
            XMLResult.LoadXml(RESULT);
            XMLResult.Save(tempfilenameResult);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {

        }
    }

   

}