﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for SendInsuranceWebservice
/// </summary>
public class SendInsuranceWebservice
{
    string AgencyID =ConfigurationManager.AppSettings["WISAgencyID"];
    string AgencyCode = ConfigurationManager.AppSettings["WISAgencyCode"];
    string InterfaceURl= ConfigurationManager.AppSettings["WISurl"];
    public SendInsuranceWebservice()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string SendRequest(JObject JsonDoc,string InterfaceURL, string Requesttype)
    {

        string strResult = "NORESULT";
        try
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                streamWriter.Write(JsonDoc);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
                //Now you have your response.
                //or false depending on information in the response     
            }
           
           CreateXMLDoc(JsonDoc, strResult, Requesttype);
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();

                errMessage += Environment.NewLine + soapResponse;
                strResult = errMessage;


                ex.Response.Close();
            }
        }
        return strResult;
    }
    public string Insurancesearch(string journeyID, string StartDate, string EndDate, string DestinationID, string family, string group, string adt, string chd, string Inf, string Sen)
    {
        string regionid;
       if (DestinationID=="1" || DestinationID == "2" || DestinationID == "3" || DestinationID == "4" || DestinationID == "13")
        {
          regionid = "region";
        }
        else
        {
             regionid = "airport";
        }
       // List<Post> posts = GetPosts();
        JObject JsonDoc =
    
            new JObject(
               new JProperty("agency_id", AgencyID),
               new JProperty("agency_code", AgencyCode),
                new JProperty("start_date", StartDate),
                 new JProperty("end_date", EndDate),
                  new JProperty("journey_id", journeyID),
                   new JProperty(regionid, DestinationID),
                    new JProperty("group", group),
                     new JProperty("family", family),
                   new JProperty("age_bands",
                 
                       new JObject(
                           new JProperty("33", adt),
                           new JProperty("34", chd),
                           new JProperty("35", Sen), 
                            new JProperty("36", Inf)
                           )));
       // String InterfaceURL = "https://www.wisconnectz.com/api/v1/premium";
        String InterfaceURL = InterfaceURl +"premium";

        return SendRequest(JsonDoc, InterfaceURL, "SearchInsurance");
    }
    public void CreateXMLDoc(JObject XMLDATA,  string RESULT, string FileType)
    {
        
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/JsonFiles/" + DateTime.Now.ToString("yyyyMMdd") + "/" + "WIS" + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string TimeStamp = DateTime.UtcNow.AddHours(4).ToString("HH-mm-ss");
            string filename = FileType + "-Request-" + finalsesID + "-" + TimeStamp + ".json";
            string tempfilename = storePath + "/" + filename;
            string filenameResult = FileType + "-Response-" + finalsesID + "-" + TimeStamp + ".json";
            string tempfilenameResult = storePath + "/" + filenameResult;

        
            if (!File.Exists(tempfilename))
            {
                File.Create(tempfilename).Dispose();
            }
            using (StreamWriter sw = File.AppendText(tempfilename))
            {
              
                //    sw.WriteLine();
                sw.WriteLine(XMLDATA);
               
                //   sw.WriteLine(line);
                sw.Flush();
                sw.Close();

            }
            if (!File.Exists(tempfilenameResult))
            {
                File.Create(tempfilenameResult).Dispose();
            }
            using (StreamWriter sw = File.AppendText(tempfilenameResult))
            {
              
                sw.WriteLine(RESULT);
              
                sw.Flush();
                sw.Close();

            }
       
    }


public string InsuranceBook(string Quotid, string Schmeid, string Mastertitle, string MasterFname, string MasterLname, string MasterEmail, string MasterPhonebnumber, string MasterAddress1, string MasterAddress2, List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> BirthDateArray, List<string> PassportnoArray, List<string> CoverIDArray,string StartDate, string EndDate,List<string> FullCover,string policyselect,string policybascicamount,string Currency,string FullBenfits,string totalprice,string MarkupValue,string DestinationID)
{
        //---------------inserting data to tblbooking-----------------------\\
        BALInsuranceBook BALAD = new BALInsuranceBook();
        DataTable dtbook = new DataTable();
        string BOOKINGREFID = string.Empty;
        // BALAD.UID = Convert.ToInt64(hdMyId.Value);
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.BOOKDATE = System.DateTime.Now;
        BALAD.BookingStatusCode = "RQ";
        BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
        dtbook = BALAD.InsertNewBookingDetails();
        //---------------inserting data to tblbooking-----------------------\\
        //---------------inserting data to tblbookinghistory-----------------------\\
        if (dtbook.Rows.Count > 0)
        {
            BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            HttpContext.Current.Session["InsuranceBookingRefID"] = BOOKINGREFID;
            BALAD.TitleCustomer = Convert.ToInt16(Mastertitle.ToString());
            BALAD.FirstName = MasterFname;
            BALAD.LastName = MasterLname;
            BALAD.EmailID = MasterEmail;
            BALAD.Mobile = MasterPhonebnumber;
            BALAD.Address1 = MasterAddress1;
            BALAD.Address2 = MasterAddress2;
            int insertCustomerInfo = BALAD.InserttblInsuranceCustomerInfo();
            int success = BALAD.InsertBookingHistoryDetails();
            insertintotblInsuranceTravellers(BOOKINGREFID, FNamesArray, NamePrefixArray, SurnameArray, BirthDateArray, PassportnoArray);
            insertintotblInsuranceAddOn(BOOKINGREFID, FullCover);
            inserttblInsurancePremium(BOOKINGREFID, Schmeid, policyselect, policybascicamount, Currency, StartDate, EndDate, DestinationID);
            inserttblInsurancePremiumBenefits(BOOKINGREFID, FullBenfits);
            inserttblInsuranceCost(BOOKINGREFID, policybascicamount, totalprice, Currency, MarkupValue);
        }
        string Out = string.Empty;
        JObject JsonDoc = new JObject();
        List<Post> NamePrefix = Post.GetPostsPrefix(NamePrefixArray);
        List<Post> FNames = Post.GetPosts(FNamesArray);
        List<Post> Surname = Post.GetPosts(SurnameArray);
        List<Post> BirthDate = Post.GetPosts(BirthDateArray);
        List<Post> Passportno = Post.GetPosts(PassportnoArray);
        string leadTitle = GetNamePrefix(Mastertitle);
        JObject Jsonoptions = new JObject();
        if (CoverIDArray[0]=="-1")
        {
             JsonDoc =

         new JObject(
            new JProperty("agency_id", AgencyID),
            new JProperty("agency_code", AgencyCode),
            new JProperty("quote_id", Quotid),
            new JProperty("scheme_id", Schmeid),
            new JProperty("title_customer", leadTitle),
             new JProperty("first_name_customer", MasterFname),
              new JProperty("last_name_customer", MasterLname),
              new JProperty("email", MasterEmail),
              new JProperty("mobile", MasterPhonebnumber),
                 new JProperty("address1", MasterAddress1),
                 new JProperty("address2", MasterAddress2),
                   new JProperty("address3", ""),
                    new JProperty("address4", ""),
                    //new JProperty("options", Jsonoptions),
                       new JProperty("title_traveller",
                                          new JArray(
                                                from p in NamePrefix

                                                select new JValue(p.Title))
                                        ),

                                   new JProperty("first_name_traveller",
                                 new JArray(
                                       from p in FNames
                                       select new JValue(p.Title))
                               ),
                                      new JProperty("last_name_traveller",
                                 new JArray(
                                       from p in Surname
                                       select new JValue(p.Title))
                               ),
                                         new JProperty("dob",
                                 new JArray(
                                       from p in BirthDate
                                       select new JValue(p.Title))
                               ),
                                            new JProperty("passport_number",
                                 new JArray(
                                       from p in Passportno
                                           //    orderby p.Title
                                         select new JValue(p.Title))
                               )
                          );


        }
        else
        {
            List<Post> posts = Post.GetPosts(CoverIDArray);
            for (int i = 0; i < posts.Count; i++)
            {


                Jsonoptions.Add(new JProperty(posts[i].Title, "1"));


            }
            JsonDoc =

         new JObject(
            new JProperty("agency_id", AgencyID),
            new JProperty("agency_code", AgencyCode),
            new JProperty("quote_id", Quotid),
            new JProperty("scheme_id", Schmeid),
            new JProperty("title_customer", leadTitle),
             new JProperty("first_name_customer", MasterFname),
              new JProperty("last_name_customer", MasterLname),
              new JProperty("email", MasterEmail),
              new JProperty("mobile", MasterPhonebnumber),
                 new JProperty("address1", MasterAddress1),
                 new JProperty("address2", MasterAddress2),
                   new JProperty("address3", ""),
                    new JProperty("address4", ""),
                    new JProperty("options", Jsonoptions),
                       new JProperty("title_traveller",
                                          new JArray(
                                                from p in NamePrefix

                                                select new JValue(p.Title))
                                        ),

                                   new JProperty("first_name_traveller",
                                 new JArray(
                                       from p in FNames
                                       select new JValue(p.Title))
                               ),
                                      new JProperty("last_name_traveller",
                                 new JArray(
                                       from p in Surname
                                       select new JValue(p.Title))
                               ),
                                         new JProperty("dob",
                                 new JArray(
                                       from p in BirthDate
                                       select new JValue(p.Title))
                               ),
                                            new JProperty("passport_number",
                                 new JArray(
                                       from p in Passportno
                                           //    orderby p.Title
                                         select new JValue(p.Title))
                               )
                          );


        }







        String InterfaceURL = InterfaceURl + "finalise";


        return SendRequest(JsonDoc, InterfaceURL, "BookInsurance");
        //return Out;

}
    public class Post
    {
        public string Title { get; set; }
        public static List<Post> GetPosts(List<string>  CoverIDArray)
        {
            List<Post> pstList = new List<Post>();

            for (int i = 0; i < CoverIDArray.Count; i++)
            {
                Post pst = new Post
                {
                    Title = CoverIDArray[i].ToString(),
                   
                };

                pstList.Add(pst);
            }

            return pstList;
        }
        public static List<Post> GetPostsPrefix(List<string> CoverIDArray)
        {
            List<Post> pstList = new List<Post>();

            for (int i = 0; i < CoverIDArray.Count; i++)
            {
                Post pst = new Post
                {
                    Title = GetNamePrefixText(CoverIDArray[i].ToString())

                };

                pstList.Add(pst);
            }

            return pstList;
        }

        public static string GetNamePrefixText(string prefValue)
        {
            string FinalPref = "";
            if (prefValue == "1")
            {
                FinalPref = "MR";
            }
            else if (prefValue == "3")
            {
                FinalPref = "MS";
            }
            else if (prefValue == "2")
            {
                FinalPref = "MRS";
            }
            else if (prefValue == "4")
            {
                FinalPref = "MISS";
            }
            else if (prefValue == "5")
            {
                FinalPref = "MSTR";
            }
            else if (prefValue == "6")
            {
                FinalPref = "DR";
            }
            else if (prefValue == "7")
            {
                FinalPref = "HE";
            }
            else if (prefValue == "8")
            {
                FinalPref = "HH";
            }
            return FinalPref;
        }

    }


    public string GetNamePrefix(string prefValue)
    {
        string FinalPref = "";
        if (prefValue == "1")
        {
            FinalPref = "MR";
        }
        else if (prefValue == "3")
        {
            FinalPref = "MS";
        }
        else if (prefValue == "2")
        {
            FinalPref = "MRS";
        }
        else if (prefValue == "4")
        {
            FinalPref = "MISS";
        }
        else if (prefValue == "5")
        {
            FinalPref = "MSTR";
        }
        else if (prefValue == "6")
        {
            FinalPref = "DR";
        }
        else if (prefValue == "7")
        {
            FinalPref = "HE";
        }
        else if (prefValue == "8")
        {
            FinalPref = "HH";
        }
        return FinalPref;
    }
    public string InsurancePurchase(string policyid)
    {

        BALInsuranceBook BALAD = new BALInsuranceBook();
        BALAD.PolicyID = policyid;
        BALAD.BookingStatusCode = "HK";
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.BOOKINGREFID = HttpContext.Current.Session["InsuranceBookingRefID"].ToString();
        BALAD.BOOKDATE = System.DateTime.Now;
      //    int update = BALAD.UpdatetblBooking();
        int updatetblpremium = BALAD.UpdatetblInsurancePremium();
      //  int success = BALAD.InsertBookingHistoryDetails();
        JObject JsonDoc =

            new JObject(
               new JProperty("agency_id", AgencyID),
               new JProperty("agency_code", AgencyCode),
               new JProperty("policy_id", policyid)
               );
        String InterfaceURL = InterfaceURl + "purchase";
        return SendRequest(JsonDoc, InterfaceURL, "PurchaseInsurance");
    }
    public string insertintotblInsuranceTravellers(string BOOKINGREFID, List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> BirthDateArray, List<string> PassportnoArray)
    {
        string Out = string.Empty;
        BALInsuranceBook BALAD = new BALInsuranceBook();
        for (int i = 0; i < FNamesArray.Count; i++)
        {
            BALAD.BOOKINGREFID = BOOKINGREFID;
            BALAD.FirstName = FNamesArray[i].ToString();
            BALAD.LastName = SurnameArray[i].ToString();
            BALAD.TitleCustomer =Convert.ToInt16(NamePrefixArray[i].ToString());
            BALAD.DOB =Convert.ToDateTime(BirthDateArray[i].ToString());
            BALAD.PassportNumber = PassportnoArray[i].ToString();
            int success = BALAD.inserttblInsuranceTravellers();
        }
       return Out;
    }

    public string insertintotblInsuranceAddOn(string BOOKINGREFID, List<string> FullCover)
    {
        string Out = string.Empty;
        BALInsuranceBook BALAD = new BALInsuranceBook();
        BALAD.BOOKINGREFID = BOOKINGREFID;
        for (int i = 0; i < FullCover.Count; i++)
        {
             if(FullCover[0]== "No additional cover added")
                {
                    BALAD.AddOnOptionID = 0;
                    BALAD.AddOnOption = "No additional cover added";
                    BALAD.Price = "";
                    BALAD.CurrencyCode = "";
                    BALAD.Description = "No additional cover added";
                    int success = BALAD.inserttblInsuranceAddOn();
                }
                else {
                if(FullCover[i] !="")
                 { 
                var coversplit = FullCover[i].Split('<');
               
                if (coversplit[0].Contains(",") == true)
                {
                   var id= coversplit[0].Replace(",", "");
                    BALAD.AddOnOptionID = Convert.ToInt16(id);
                }
                else
                {
                    BALAD.AddOnOptionID = Convert.ToInt16(coversplit[0]);
                }
               
                BALAD.AddOnOption = coversplit[1];
                BALAD.Price = coversplit[2];
                BALAD.CurrencyCode = coversplit[3];
                BALAD.Description = coversplit[4];
                int success = BALAD.inserttblInsuranceAddOn();
                }
            }



        }
      return Out;
    }
    public string inserttblInsurancePremium(string BOOKINGREFID,string Schmeid,string policyselect,string policybascicamount,string Currency,string StartDate,string EndDate,string DestinationID)
    {
        string Out = string.Empty;
        BALInsuranceBook BALAD = new BALInsuranceBook();
        BALAD.BOOKINGREFID = BOOKINGREFID;
        BALAD.PremiumID =Convert.ToInt16(Schmeid);
        BALAD.Premium = policyselect;
        BALAD.Price = policybascicamount;
        BALAD.CurrencyCode = Currency;
        BALAD.StartDate =Convert.ToDateTime(StartDate);
        BALAD.EndDate = Convert.ToDateTime(EndDate);
        BALAD.DestinationID = DestinationID;
        int success = BALAD.inserttblInsurancePremium();
        return Out;
    }
    public string inserttblInsurancePremiumBenefits(string BOOKINGREFID, string FullBenfits)
    {
        string Out = string.Empty;
        BALInsuranceBook BALAD = new BALInsuranceBook();
        BALAD.BOOKINGREFID = BOOKINGREFID;
        BALAD.FullBenifits = FullBenfits;
        
        int success = BALAD.inserttblInsurancePremiumBenefits();
        return Out;
    }

    public string inserttblInsuranceCost(string BOOKINGREFID, string policybascicamount, string totalprice, string Currency,string MarkupValue)
    {
        string Out = string.Empty;
        BALInsuranceBook BALAD = new BALInsuranceBook();
        BALAD.BOOKINGREFID = BOOKINGREFID;
        BALAD.BasicAmount = policybascicamount;
        BALAD.TotalAmount = totalprice;
        BALAD.CurrencyCode = Currency;
        BALAD.MarkupValue = Convert.ToInt64(MarkupValue);
        int success = BALAD.inserttblInsuranceCost();
        return Out;
    }

}

