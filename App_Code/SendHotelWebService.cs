﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Net;
using System.IO;
using System.Globalization;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for SendHotelWebService
/// </summary>
public class SendHotelWebService
{
    //SHABEER
    String ClientID = ConfigurationManager.AppSettings["ClientID"];
    String EmailAddress = ConfigurationManager.AppSettings["EmailAddress"];
    String ClientPassword = ConfigurationManager.AppSettings["ClientPassword"];
    String InterfaceURL = ConfigurationManager.AppSettings["InterfaceURL"];
    String ResponseURL = "https://10.100.21.131/receiveRequest.asp";
    String Language = ConfigurationManager.AppSettings["GTALanguage"];
    String Currency = ConfigurationManager.AppSettings["GTACurrency"];
    string Agencyref= ConfigurationManager.AppSettings["AgencyRef"];

    public SendHotelWebService()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string SendRequest(XmlDocument XMLDoc, string SoapAction)
    {
        string strResult = "NORESULT";
        try
        {
            // post document 
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            Byte[] byte1 = encoding.GetBytes(XMLDoc.OuterXml);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebRequest HttpWReq = WebRequest.Create(InterfaceURL);

            HttpWReq.ContentType = "text/xml";
            HttpWReq.ContentLength = XMLDoc.OuterXml.Length;
            HttpWReq.Method = "POST";

            Stream StreamData = HttpWReq.GetRequestStream();
            StreamData.Write(byte1, 0, byte1.Length);

            // get response
            WebResponse HttpWRes = HttpWReq.GetResponse();
            Stream receiveStream = HttpWRes.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();
            string strHeaders = HttpWRes.Headers.ToString();
            CreateXMLDoc(XMLDoc, strHeaders, strResult, SoapAction);
        }
        catch (WebException ex)
        {
            strResult = "NORESULT";
            SaveErrorLog(ex);
        }
        return strResult;
    }
    public void CreateXMLDoc(XmlDocument XMLDATA, string XMLHEADER, string RESULT, string FileType)
    {
        try
        {
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/XmlFiles/" + DateTime.Now.ToString("yyyyMMdd") + "/" + "GTA" + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string TimeStamp = DateTime.UtcNow.AddHours(4).ToString("HH-mm-ss");
            string filename = FileType + "-Request-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilename = storePath + "/" + filename;
            string filenameHeader = FileType + "-Header-" + finalsesID + "-" + TimeStamp + ".txt";
            string tempfilenameHeader = storePath + "/" + filenameHeader;
            string filenameResult = FileType + "-Response-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilenameResult = storePath + "/" + filenameResult;

            XMLDATA.Save(tempfilename);
            File.WriteAllText(tempfilenameHeader, XMLHEADER);
            XmlDocument XMLResult = new XmlDocument();
            XMLResult.LoadXml(RESULT);
            XMLResult.Save(tempfilenameResult);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {

        }
    }
    public void SaveXmlToStaticData(string XMLDATA, string CityCode, string HotelCode)
    {
        try
        {
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("oneconnect/hotel/StaticData/GTAHotels/");
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string filenameResult = CityCode + "_" + HotelCode + ".xml";
            string tempfilenameResult = storePath + "/" + filenameResult;
            XmlDocument XMLResult = new XmlDocument();
            XMLResult.LoadXml(XMLDATA);
            XMLResult.Save(tempfilenameResult);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {

        }
    }
    public string HotelSearch(string nation, string city, string cin, string cout, string night, string numroom, string rooms)
    {

        string CheckInDate = Convert.ToDateTime(cin).ToString("yyyy-MM-dd");
        //string CheckInDate = DateTime.ParseExact(cin, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLNode.SetAttribute("Currency", Currency);
        XMLNode.SetAttribute("Country", nation);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchHotelPricePaxRequest");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");
        XMLChildNode.SetAttribute("DestinationCode", city);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("PeriodOfStay");
        XMLGrandChild = XMLDoc.CreateElement("CheckInDate");
        //XMLGrandChild.InnerText = "2017-10-01";
        XMLGrandChild.InnerText = CheckInDate;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLGrandChild = XMLDoc.CreateElement("Duration");
        //XMLGrandChild.InnerText = "2";
        XMLGrandChild.InnerText = night;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLNode.AppendChild(XMLChildNode);

        XMLChildNode = XMLDoc.CreateElement("IncludePriceBreakdown");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("IncludeChargeConditions");
        XMLNode.AppendChild(XMLChildNode);

        XMLChildNode = XMLDoc.CreateElement("PaxRooms");

        //Room Geneartion START
        string[] roomFullData = rooms.Split('/');
        for (int ir = 0; ir < Convert.ToInt32(numroom); ir++)
        {
            string RoomIndex = Convert.ToString(ir + 1);
            XMLGrandChild = XMLDoc.CreateElement("PaxRoom");
            string roomEach = roomFullData[ir];
            string[] Paxes = roomEach.Split(',');
            for (int ipax = 0; ipax < Paxes.Length; ipax++)
            {
                string[] PaxTypeCount = Paxes[ipax].Split('_');
                if (PaxTypeCount[0] == "ADT")
                {
                    XMLGrandChild.SetAttribute("Adults", PaxTypeCount[1]);
                }
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0")
                {
                    int NumCots = 0;
                    for (int ichcount = 0; ichcount < Convert.ToInt32(PaxTypeCount[1]); ichcount++)
                    {
                        int ChdAge = Convert.ToInt32(PaxTypeCount[ichcount + 2]);
                        if (ChdAge == 1)
                        {
                            NumCots++;
                        }
                    }
                    if (NumCots > 0)
                    {
                        XMLGrandChild.SetAttribute("Cots", NumCots.ToString());
                    }
                }
                XMLGrandChild.SetAttribute("RoomIndex", RoomIndex);
                XMLChildNode.AppendChild(XMLGrandChild);
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0")
                {
                    XMLGrandGrandChild = XMLDoc.CreateElement("ChildAges");
                    int numChd = 0;
                    for (int ichcount = 0; ichcount < Convert.ToInt32(PaxTypeCount[1]); ichcount++)
                    {
                        int ChdAge = Convert.ToInt32(PaxTypeCount[ichcount + 2]);
                        if (ChdAge > 1)
                        {
                            XMLGrandGrandGrandChild = XMLDoc.CreateElement("Age");
                            XMLGrandGrandGrandChild.InnerText = PaxTypeCount[ichcount + 2];
                            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
                            numChd++;
                        }
                    }
                    if (numChd > 0)
                    {
                        XMLGrandChild.AppendChild(XMLGrandGrandChild);
                    }
                }
            }
        }
        //Room Geneartion END

        //Room 1

        //Room 2
        //XMLGrandChild = XMLDoc.CreateElement("PaxRoom");
        //XMLGrandChild.SetAttribute("Adults", "3");
        //XMLGrandChild.SetAttribute("Cots", "1");
        //XMLGrandChild.SetAttribute("RoomIndex", "2");
        //XMLChildNode.AppendChild(XMLGrandChild);

        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "SearchHotelPricePax");
    }
    public string GetHotelInfoGtaa(string CityCode, string HotelCode)
    {
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLNode.SetAttribute("Currency", Currency);
        //XMLNode.SetAttribute("Country", nation);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchItemInformationRequest");
        XMLNode.SetAttribute("ItemType", "hotel");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");
        XMLChildNode.SetAttribute("DestinationCode", CityCode);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ItemCode");
        XMLChildNode.InnerText = HotelCode;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        string XMLHotelInfo = SendRequest(XMLDoc, "SearchItemInformationRequestMoreInfoGta");
        SaveXmlToStaticData(XMLHotelInfo, CityCode, HotelCode);
        return XMLHotelInfo;
    }
    public void SaveErrorLog(WebException ex)
    {
        try
        {
            string errMessage = ex.Message;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();

                //Save
                string ErrorLogFile = System.Web.HttpContext.Current.Server.MapPath("/ELOGS/GTACatchLogs.txt");
                using (StreamWriter writer = new StreamWriter(ErrorLogFile, true))
                {
                    writer.WriteLine(Environment.NewLine + "--------------------------------------" + "Date :" + DateTime.Now.ToString() + "---------------------------------------" + Environment.NewLine);
                    writer.WriteLine("Error :" + errMessage + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace + "");
                    writer.WriteLine(Environment.NewLine + "----------------------------------------------------------------------------------------------------------" + Environment.NewLine);
                }
            }
        }
        catch (Exception exe)
        {

        }
    }

    //DURGA

    //public string HotelBook(List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> RoomsArray, List<string> roomcategoryidArray, string CityCodee, string HotelCodee, string Checkin, string Checkout, string NoofNights, string NoofRoom, string Name, string LName, string MPrefix, string AreaCityCodeArray, string PhoneNumberArray, string EmailArray)
    //{
    //    BALHotelBook BALAD = new BALHotelBook();
    //    DataTable dtbook = new DataTable();
    //    string BOOKINGREFID = string.Empty;
    //    // BALAD.UID = Convert.ToInt64(hdMyId.Value);
    //    BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
    //    BALAD.BOOKDATE = System.DateTime.Now;
    //    BALAD.BookingStatusCode = "RQ";
    //    BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
    //    dtbook = BALAD.InsertNewBookingDetails();
    //    //---------------inserting data to tblbooking-----------------------\\
    //    //---------------inserting data to tblbookinghistory-----------------------\\
    //    if (dtbook.Rows.Count > 0)
    //    {
    //        BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
    //        BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
    //        HttpContext.Current.Session["BookingRefID"] = BOOKINGREFID;
    //        int success = BALAD.InsertBookingHistoryDetails();
    //    }
    //    //---------------inserting data to tblbookinghistory-----------------------\\
    //    System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
    //    System.Xml.XmlElement XMLRequest;
    //    System.Xml.XmlElement XMLHeader;
    //    System.Xml.XmlElement XMLBody;
    //    System.Xml.XmlElement XMLNode;
    //    System.Xml.XmlElement XMLChildNode;
    //    System.Xml.XmlElement XMLGrandChild;
    //    System.Xml.XmlElement XMLGrandGrandChild;
    //    System.Xml.XmlElement XMLGrandGrandGrandChild;
    //    System.Xml.XmlElement XMLCheckinDate;
    //    System.Xml.XmlElement XMLPAX;
    //    System.Xml.XmlElement XMLPAXID;
    //    String ClientID = "2144";
    //    String Language = "en";
    //    String Currency = "USD";
    //    String EmailAddress = "XML.ONEVIEWITSOLUTIONS@AIRBORNETRAVEL.COM";
    //    String ClientPassword = "PASS";
    //    String InterfaceURL = "https://interface.demo.gta-travel.com/wbsapi/RequestListenerServlet";
    //    String ResponseURL = "https://10.100.21.131/receiveRequest.asp";
    //    int NoofRooms = Convert.ToInt16(NoofRoom);
    //    string CheckinDate = Convert.ToDateTime(Checkin).ToString("yyyy-MM-dd");
    //    string CheckoutDate = Convert.ToDateTime(Checkout).ToString("yyyy-MM-dd");

    //    // create XML
    //    // create request
    //    XMLRequest = XMLDoc.CreateElement("Request");
    //    XMLHeader = XMLDoc.CreateElement("Source");
    //    // client details node
    //    XMLNode = XMLDoc.CreateElement("RequestorID");
    //    XMLNode.SetAttribute("Client", ClientID);
    //    XMLNode.SetAttribute("EMailAddress", EmailAddress);
    //    XMLNode.SetAttribute("Password", ClientPassword);
    //    XMLHeader.AppendChild(XMLNode);
    //    // client preference node
    //    XMLNode = XMLDoc.CreateElement("RequestorPreferences");
    //    XMLNode.SetAttribute("Language", Language);
    //    XMLNode.SetAttribute("Currency", Currency);
    //    XMLChildNode = XMLDoc.CreateElement("RequestMode");
    //    XMLChildNode.InnerText = "SYNCHRONOUS";
    //    XMLNode.AppendChild(XMLChildNode);
    //    XMLHeader.AppendChild(XMLNode);
    //    // create request body
    //    XMLBody = XMLDoc.CreateElement("RequestDetails");
    //    XMLNode = XMLDoc.CreateElement("AddBookingRequest");
    //    XMLNode.SetAttribute("Currency", "USD");
    //    XMLChildNode = XMLDoc.CreateElement("BookingReference");
    //    XMLChildNode.InnerText = BOOKINGREFID;
    //    XMLNode.AppendChild(XMLChildNode);
    //    XMLChildNode = XMLDoc.CreateElement("PaxNames");
    //    List<string> paxdet = new List<string>();
    //    for (int iR = 0; iR < RoomsArray.Count; iR++)
    //    {
    //        List<string> Arr = RoomsArray[iR].Split(',').ToList<string>();
    //        for (var i3 = 0; i3 < Arr.Count; i3++)
    //        {

    //            List<string> Paxage = Arr[i3].Split('_').ToList<string>();
    //            if (Paxage[0] == "CHD" && Paxage[1] != "0")
    //            {
    //                int countchd = Convert.ToInt16(Paxage[1]);
    //                if (countchd > 1)
    //                {
    //                    int arrcount = 2;
    //                    for (int i9 = 0; i9 < countchd; i9++)
    //                    {

    //                        paxdet.Add(Paxage[arrcount]);
    //                        arrcount++;
    //                    }
    //                }
    //                else
    //                {
    //                    paxdet.Add(Arr[i3]);
    //                }


    //            }
    //            else if (Paxage[0] == "ADT")
    //            {
    //                paxdet.Add(Arr[i3]);
    //            }


    //        }
    //    }
    //    //Pax
    //    int ArrayNameCount = 0;

    //    for (int i = 0; i < FNamesArray.Count; i++)
    //    {
    //        int paxid = i + 1;

    //        XMLGrandChild = XMLDoc.CreateElement("PaxName");
    //        XMLGrandChild.SetAttribute("PaxId", paxid.ToString());
    //        if (GetTitle(NamePrefixArray[ArrayNameCount]) == "Master" || GetTitle(NamePrefixArray[ArrayNameCount]) == "Miss")
    //        {

    //            XMLGrandChild.SetAttribute("PaxType", "child");
    //            List<string> Paxage = paxdet[i].Split('_').ToList<string>();

    //            if (Paxage[0] == "CHD" && Paxage[1] != "0")
    //            {

    //                XMLGrandChild.SetAttribute("ChildAge", Paxage[2]);
    //            }
    //            else
    //            {
    //                XMLGrandChild.SetAttribute("ChildAge", Paxage[0]);
    //            }




    //        }
    //        string Passname = GetTitle(NamePrefixArray[ArrayNameCount]) + " " + FNamesArray[ArrayNameCount] + " " + SurnameArray[ArrayNameCount];

    //        XMLGrandChild.InnerText = Passname;
    //        XMLChildNode.AppendChild(XMLGrandChild);
    //        ArrayNameCount++;

    //    }
    //    XMLNode.AppendChild(XMLChildNode);
    //    //Pax
    //    //Room 
    //    int ArraypaxCount = 1;
    //    int ArrayNameCountt = 0;
    //    XMLChildNode = XMLDoc.CreateElement("BookingItems");
    //    for (int i = 0; i < NoofRooms; i++)
    //    {
    //        int itemreference = i + 1;
    //        XMLGrandChild = XMLDoc.CreateElement("BookingItem");
    //        XMLGrandChild.SetAttribute("ItemType", "hotel");
    //        XMLGrandGrandChild = XMLDoc.CreateElement("ItemReference");
    //        XMLGrandGrandChild.InnerText = itemreference.ToString();
    //        XMLGrandChild.AppendChild(XMLGrandGrandChild);
    //        XMLGrandGrandChild = XMLDoc.CreateElement("ItemCity");
    //        XMLGrandGrandChild.SetAttribute("Code", CityCodee);
    //        XMLGrandChild.AppendChild(XMLGrandGrandChild);
    //        XMLGrandGrandChild = XMLDoc.CreateElement("Item");
    //        XMLGrandGrandChild.SetAttribute("Code", HotelCodee);
    //        XMLGrandChild.AppendChild(XMLGrandGrandChild);
    //        XMLGrandGrandChild = XMLDoc.CreateElement("HotelItem");
    //        XMLGrandGrandGrandChild = XMLDoc.CreateElement("PeriodOfStay");
    //        XMLCheckinDate = XMLDoc.CreateElement("CheckInDate");
    //        XMLCheckinDate.InnerText = CheckinDate.ToString();
    //        XMLGrandGrandGrandChild.AppendChild(XMLCheckinDate);
    //        XMLCheckinDate = XMLDoc.CreateElement("CheckOutDate");
    //        XMLCheckinDate.InnerText = CheckoutDate.ToString();
    //        XMLGrandGrandGrandChild.AppendChild(XMLCheckinDate);
    //        XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
    //        XMLGrandGrandGrandChild = XMLDoc.CreateElement("HotelPaxRoom");
    //        string chdcount = "0";
    //        string adt = string.Empty;
    //        string cot = "0";
    //        string PName = RoomsArray[i];
    //        int Count = 0;
    //        List<string> strArr = PName.Split(',').ToList<string>();
    //        for (var i3 = 0; i3 < strArr.Count; i3++)
    //        {

    //            List<string> Paxbrkup = strArr[i3].Split('_').ToList<string>();
    //            if (Paxbrkup[0] == "ADT")
    //            {
    //                adt = "Adults";
    //                chdcount = Paxbrkup[1];
    //                Count = Count + Convert.ToInt16(chdcount.ToString());
    //            }
    //            else if (Paxbrkup[0] == "CHD" && Paxbrkup[1] != "0")
    //            {
    //                adt = "Children";
    //                chdcount = Paxbrkup[1];
    //                cot = "1";
    //                Count = Count + Convert.ToInt16(chdcount.ToString());
    //            }

    //            XMLGrandGrandGrandChild.SetAttribute(adt, chdcount);
    //            if (cot != "0")
    //            {
    //                XMLGrandGrandGrandChild.SetAttribute("Cots", cot);

    //            }
    //        }
    //        XMLGrandGrandGrandChild.SetAttribute("Id", roomcategoryidArray[ArrayNameCountt]);
    //        XMLPAX = XMLDoc.CreateElement("PaxIds");

    //        for (var i4 = 0; i4 < Count; i4++)
    //        {
    //            XMLPAXID = XMLDoc.CreateElement("PaxId");
    //            XMLPAXID.InnerText = ArraypaxCount.ToString();
    //            XMLPAX.AppendChild(XMLPAXID);
    //            ArraypaxCount++;
    //        }


    //        XMLGrandGrandGrandChild.AppendChild(XMLPAX);
    //        XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
    //        XMLGrandChild.AppendChild(XMLGrandGrandChild);
    //        XMLChildNode.AppendChild(XMLGrandChild);
    //        ArrayNameCountt++;
    //    }

    //    XMLNode.AppendChild(XMLChildNode);

    //    XMLBody.AppendChild(XMLNode);
    //    // create XML document
    //    XMLRequest.AppendChild(XMLHeader);
    //    XMLRequest.AppendChild(XMLBody);
    //    XMLDoc.AppendChild(XMLRequest);
    //    // post document 
    //    return SendRequest(XMLDoc, "HotelBook");
    //    //return ClientID;

    //}
    public static string GetTitle(string Title)
    {
        string Out = string.Empty;
        if (Title == "1")
        {
            Out = "Mr";
        }
        else if (Title == "2")
        {
            Out = "Miss";
        }
        else if (Title == "3")
        {
            Out = "Mrs";
        }
        else if (Title == "4")
        {
            Out = "Ms";
        }
        else if (Title == "5")
        {
            Out = "Master";
        }

        return Out;
    }
    public string HotelBook(List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> RoomsArray, List<string> roomcategoryidArray, string CityCodee, string HotelCodee, string Checkin, string Checkout, string NoofNights, string NoofRoom, string CountryCode, List<string> ExpectedPriceArray)
    {
        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtbook = new DataTable();
        string BOOKINGREFID = string.Empty;
        string BOOKCLIENTID= string.Empty;
        // BALAD.UID = Convert.ToInt64(hdMyId.Value);
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.BOOKDATE = System.DateTime.Now;
        BALAD.BookingStatusCode = "RQ";
        BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
        dtbook = BALAD.InsertNewBookingDetails();
        //---------------inserting data to tblbooking-----------------------\\
        //---------------inserting data to tblbookinghistory-----------------------\\
        if (dtbook.Rows.Count > 0)
        {
            BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            BOOKINGREFID =dtbook.Rows[0]["BookingRefID"].ToString();
            BOOKCLIENTID = Agencyref +'-'+ dtbook.Rows[0]["BookingRefID"].ToString();
            HttpContext.Current.Session["HotelBookingRefID"] = BOOKINGREFID;
            int success = BALAD.InsertBookingHistoryDetails();
        }
        //---------------inserting data to tblbookinghistory-----------------------\\
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;
        System.Xml.XmlElement XMLGrandChild;
        System.Xml.XmlElement XMLGrandGrandChild;
        System.Xml.XmlElement XMLGrandGrandGrandChild;
        System.Xml.XmlElement XMLCheckinDate;
        System.Xml.XmlElement XMLPAX;
        System.Xml.XmlElement XMLPAXID;

        int NoofRooms = Convert.ToInt16(NoofRoom);
        string CheckinDate = Convert.ToDateTime(Checkin).ToString("yyyy-MM-dd");
        string CheckoutDate = Convert.ToDateTime(Checkout).ToString("yyyy-MM-dd");

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLNode.SetAttribute("Currency", Currency);
        XMLNode.SetAttribute("Country", CountryCode);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("AddBookingRequest");
        XMLNode.SetAttribute("Currency", Currency);
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.InnerText = BOOKCLIENTID;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("PaxNames");
        List<string> paxdet = new List<string>();
        for (int iR = 0; iR < RoomsArray.Count; iR++)
        {
            List<string> Arr = RoomsArray[iR].Split(',').ToList<string>();
            for (var i3 = 0; i3 < Arr.Count; i3++)
            {

                List<string> Paxage = Arr[i3].Split('_').ToList<string>();
                if (Paxage[0] == "CHD" && Paxage[1] != "0")
                {
                    int countchd = Convert.ToInt16(Paxage[1]);
                    int data = countchd;
                    if (countchd >= 1)
                    {
                        int arrcount = 2;
                        for (int i9 = 0; i9 < data; i9++)
                        {
                            if (Paxage[arrcount] == "1")
                            {
                                //paxdet.Add(Paxage[arrcount+1]);
                                countchd = countchd - 1;
                                arrcount++;
                            }
                            else
                            {
                                paxdet.Add(Paxage[arrcount]);
                                arrcount++;
                            }

                        }
                    }
                    else
                    {
                        paxdet.Add(Arr[i3]);
                    }


                }
                else if (Paxage[0] == "ADT")
                {
                    int countadt = Convert.ToInt16(Paxage[1]);
                    if (countadt > 1)
                    {
                        //int arrcount = 1;
                        for (int i9 = 0; i9 < countadt; i9++)
                        {

                            paxdet.Add(Paxage[i3]);
                            //arrcount++;


                        }
                    }
                    else
                    {
                        paxdet.Add(Arr[i3]);
                    }

                }






            }
        }
        //Pax
        int ArrayNameCount = 0;

        for (int i = 0; i < FNamesArray.Count; i++)
        {
            int paxid = i + 1;

            XMLGrandChild = XMLDoc.CreateElement("PaxName");
            XMLGrandChild.SetAttribute("PaxId", paxid.ToString());
            if (GetTitle(NamePrefixArray[ArrayNameCount]) == "Master" || GetTitle(NamePrefixArray[ArrayNameCount]) == "Miss")
            {

                XMLGrandChild.SetAttribute("PaxType", "child");
                List<string> Paxage = paxdet[i].Split('_').ToList<string>();

                if (Paxage[0] == "CHD" && Paxage[1] != "0")
                {

                    XMLGrandChild.SetAttribute("ChildAge", Paxage[2]);
                }
                else
                {
                    XMLGrandChild.SetAttribute("ChildAge", Paxage[0]);
                }




            }
            string Passname = GetTitle(NamePrefixArray[ArrayNameCount]) + " " + FNamesArray[ArrayNameCount] + " " + SurnameArray[ArrayNameCount];

            XMLGrandChild.InnerText = Passname;
            XMLChildNode.AppendChild(XMLGrandChild);
            ArrayNameCount++;

        }
        XMLNode.AppendChild(XMLChildNode);
        //Pax
        //Room 
        int ArraypaxCount = 1;
        int ArrayNameCountt = 0;
        XMLChildNode = XMLDoc.CreateElement("BookingItems");
        for (int i = 0; i < NoofRooms; i++)
        {
            int itemreference = i + 1;
            XMLGrandChild = XMLDoc.CreateElement("BookingItem");
            XMLGrandChild.SetAttribute("ItemType", "hotel");
            XMLGrandChild.SetAttribute("ExpectedPrice", ExpectedPriceArray[i]);
            XMLGrandGrandChild = XMLDoc.CreateElement("ItemReference");
            XMLGrandGrandChild.InnerText = itemreference.ToString();
            XMLGrandChild.AppendChild(XMLGrandGrandChild);
            XMLGrandGrandChild = XMLDoc.CreateElement("ItemCity");
            XMLGrandGrandChild.SetAttribute("Code", CityCodee);
            XMLGrandChild.AppendChild(XMLGrandGrandChild);
            XMLGrandGrandChild = XMLDoc.CreateElement("Item");
            XMLGrandGrandChild.SetAttribute("Code", HotelCodee);
            XMLGrandChild.AppendChild(XMLGrandGrandChild);
            XMLGrandGrandChild = XMLDoc.CreateElement("HotelItem");
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("PeriodOfStay");
            XMLCheckinDate = XMLDoc.CreateElement("CheckInDate");
            XMLCheckinDate.InnerText = CheckinDate.ToString();
            XMLGrandGrandGrandChild.AppendChild(XMLCheckinDate);
            XMLCheckinDate = XMLDoc.CreateElement("CheckOutDate");
            XMLCheckinDate.InnerText = CheckoutDate.ToString();
            XMLGrandGrandGrandChild.AppendChild(XMLCheckinDate);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
            XMLGrandGrandGrandChild = XMLDoc.CreateElement("HotelPaxRoom");
            string chdcount = "0";
            string adt = string.Empty;
            string cot = "0";
            string PName = RoomsArray[i];
            int Count = 0;
            List<string> strArr = PName.Split(',').ToList<string>();
            for (var i3 = 0; i3 < strArr.Count; i3++)
            {

                List<string> Paxbrkup = strArr[i3].Split('_').ToList<string>();
                if (Paxbrkup[0] == "ADT")
                {
                    adt = "Adults";
                    chdcount = Paxbrkup[1];
                    Count = Count + Convert.ToInt16(chdcount.ToString());
                    XMLGrandGrandGrandChild.SetAttribute(adt, chdcount);
                }
                else if (Paxbrkup[0] == "CHD" && Paxbrkup[1] != "0")
                {
                    adt = "Children";
                    chdcount = Paxbrkup[1];
                    int data = Convert.ToInt16(chdcount.ToString());
                    int changecount = Convert.ToInt16(chdcount.ToString());
                    cot = "0";
                    Count = Count + Convert.ToInt16(chdcount.ToString());

                    int actcount = Convert.ToInt16(chdcount.ToString());
                    int checkchd = 2;
                    int noofcots = 1;
                    for (int icount = 0; icount < data; icount++)
                    {
                        if (Paxbrkup[checkchd] == "1")
                        {
                            cot = noofcots.ToString();
                            XMLGrandGrandGrandChild.SetAttribute("Cots", cot);
                            checkchd++;
                            noofcots++;
                            actcount = (actcount - 1);
                            Count = Count - 1;
                            XmlNode node = XMLGrandGrandGrandChild.Attributes[1];
                            if (node.Name == "Children")
                            {
                                XMLGrandGrandGrandChild.Attributes[1].Value = actcount.ToString();

                            }
                        }
                        else
                        {
                            XMLGrandGrandGrandChild.SetAttribute(adt, actcount.ToString());
                            checkchd++;
                        }

                    }




                }



            }
            XMLGrandGrandGrandChild.SetAttribute("Id", roomcategoryidArray[ArrayNameCountt]);
            XMLPAX = XMLDoc.CreateElement("PaxIds");

            for (var i4 = 0; i4 < Count; i4++)
            {
                XMLPAXID = XMLDoc.CreateElement("PaxId");
                XMLPAXID.InnerText = ArraypaxCount.ToString();
                XMLPAX.AppendChild(XMLPAXID);
                ArraypaxCount++;
            }


            XMLGrandGrandGrandChild.AppendChild(XMLPAX);
            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
            XMLGrandChild.AppendChild(XMLGrandGrandChild);
            XMLChildNode.AppendChild(XMLGrandChild);
            ArrayNameCountt++;
        }

        XMLNode.AppendChild(XMLChildNode);

        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "HotelBook");
        //return ClientID;

    }

    public string BookHotelCancel(string APIid)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("CancelBookingRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "api");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "CancelBook");
    }
    public string BookingHotelUpdate(string APIid)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchBookingItemRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "api");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "UpdateBook");
    }
    public string GetGtaAOTNumbers(string CountryCode)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;
        System.Xml.XmlElement XMLGrandChildNode;
        System.Xml.XmlElement XMLGrandGrandChildNode;
        System.Xml.XmlElement XMLChildParentNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchAOTNumberRequest");
        XMLChildNode = XMLDoc.CreateElement("AssistanceLanguage");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("Destination");
        XMLChildNode.InnerText = "AU";
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("Nationality");
        XMLChildNode.InnerText = CountryCode;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ServiceType");
        XMLChildNode.InnerText = "hotel";
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "GetGtaAOTNumbers");
    }

    public string HotelFinalAvailabilityCheck(string cin, string night, string numroom, string rooms, string roomcategoryid, string CityCode, string HotelCode)
    {

        string CheckInDate = Convert.ToDateTime(cin).ToString("yyyy-MM-dd");
        //string CheckInDate = DateTime.ParseExact(cin, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
        XmlDocument XMLDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNode;
        XmlElement XMLChildNode;
        XmlElement XMLGrandChild;
        XmlElement XMLGrandGrandChild;
        XmlElement XMLGrandGrandGrandChild;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLNode.SetAttribute("Currency", Currency);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchHotelPricePaxRequest");
        XMLChildNode = XMLDoc.CreateElement("ItemDestination");
        XMLChildNode.SetAttribute("DestinationType", "city");
        XMLChildNode.SetAttribute("DestinationCode", CityCode);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ImmediateConfirmationOnly");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ItemCode");
        XMLChildNode.InnerText = HotelCode;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("PeriodOfStay");
        XMLGrandChild = XMLDoc.CreateElement("CheckInDate");
        XMLGrandChild.InnerText = CheckInDate;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLGrandChild = XMLDoc.CreateElement("Duration");
        XMLGrandChild.InnerText = night;
        XMLChildNode.AppendChild(XMLGrandChild);
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("IncludePriceBreakdown");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("IncludeChargeConditions");
        XMLNode.AppendChild(XMLChildNode);

        XMLChildNode = XMLDoc.CreateElement("PaxRooms");

        //Room Geneartion START
        string[] roomFullData = rooms.Split('/');
        string[] roomid = roomcategoryid.Split(',');
        for (int ir = 0; ir < Convert.ToInt32(numroom); ir++)
        {
            string RoomIndex = Convert.ToString(ir + 1);
            XMLGrandChild = XMLDoc.CreateElement("PaxRoom");
            string roomEach = roomFullData[ir];
            string[] Paxes = roomEach.Split(',');
            for (int ipax = 0; ipax < Paxes.Length; ipax++)
            {
                string[] PaxTypeCount = Paxes[ipax].Split('_');
                if (PaxTypeCount[0] == "ADT")
                {
                    XMLGrandChild.SetAttribute("Adults", PaxTypeCount[1]);
                }
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0")
                {
                    int NumCots = 0;
                    for (int ichcount = 0; ichcount < Convert.ToInt32(PaxTypeCount[1]); ichcount++)
                    {
                        int ChdAge = Convert.ToInt32(PaxTypeCount[ichcount + 2]);
                        if (ChdAge == 1)
                        {
                            NumCots++;
                        }
                    }
                    if (NumCots > 0)
                    {
                        XMLGrandChild.SetAttribute("Cots", NumCots.ToString());
                    }

                }
                XMLGrandChild.SetAttribute("Id", roomid[ir]);
                XMLGrandChild.SetAttribute("RoomIndex", RoomIndex);
                XMLChildNode.AppendChild(XMLGrandChild);
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0")
                {
                    XMLGrandGrandChild = XMLDoc.CreateElement("ChildAges");
                    int numChd = 0;
                    for (int ichcount = 0; ichcount < Convert.ToInt32(PaxTypeCount[1]); ichcount++)
                    {
                        int ChdAge = Convert.ToInt32(PaxTypeCount[ichcount + 2]);
                        if (ChdAge > 1)
                        {
                            XMLGrandGrandGrandChild = XMLDoc.CreateElement("Age");
                            XMLGrandGrandGrandChild.InnerText = PaxTypeCount[ichcount + 2];
                            XMLGrandGrandChild.AppendChild(XMLGrandGrandGrandChild);
                            numChd++;
                        }
                    }
                    if (numChd > 0)
                    {
                        XMLGrandChild.AppendChild(XMLGrandGrandChild);
                    }
                }
            }
        }


        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        return SendRequest(XMLDoc, "SearchHotelPricePax");
    }
    public string BookingHotelUpdateClient(string APIid)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;
        System.Xml.XmlElement XMLGrandChildNode;
        System.Xml.XmlElement XMLGrandGrandChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchBookingItemRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "client");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "UpdateBook");
    }
    public string OnrequestBookingCancellationpolicies(string ItemRef, string BOOKINGREFID)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;
        System.Xml.XmlElement XMLGrandChildNode;
        System.Xml.XmlElement XMLGrandGrandChildNode;
        System.Xml.XmlElement XMLChildParentNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchChargeConditionsRequest");
        XMLChildNode = XMLDoc.CreateElement("DateFormatResponse");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildParentNode = XMLDoc.CreateElement("ChargeConditionsBookingItem");
        XMLGrandChildNode = XMLDoc.CreateElement("BookingReference");
        XMLGrandChildNode.InnerText = BOOKINGREFID;
        XMLChildParentNode.AppendChild(XMLGrandChildNode);
        XMLGrandGrandChildNode = XMLDoc.CreateElement("ItemReference");
        XMLGrandGrandChildNode.InnerText = ItemRef;
        XMLChildParentNode.AppendChild(XMLGrandGrandChildNode);
        XMLNode.AppendChild(XMLChildParentNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequest(XMLDoc, "OnRequestCancellatonpolicy");
    }
}