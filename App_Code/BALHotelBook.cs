﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for BALHotelBook
/// </summary>
public class BALHotelBook
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();

    public BALHotelBook()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Int64 UID { get; set; }
    public string PNR { get; set; }
    public DateTime BOOKDATE { get; set; }
    public DateTime CANCELDATE { get; set; }
    public DataTable InsertNewBookingDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Position", "1");
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", "2");
        cmd.Parameters.AddWithValue("@BookingDate", BOOKDATE);
        cmd.Parameters.AddWithValue("@SupplierID", "2");
        cmd.Parameters.AddWithValue("@SupplierBookingReference", "");
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", "RQ");
        cmd.Parameters.AddWithValue("@PaymentStatusID", "1");
        cmd.Parameters.AddWithValue("@AgencyRemarks", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertnewbooking";
        return Dalref.ExeReader(cmd);

    }
    public string BOOKINGREFID { get; set; }
    public string BookingStatusCode { get; set; }
    public int InsertBookingHistoryDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ActionDate", BOOKDATE);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHistory";
        return Dalref.ExeNonQuery(cmd);
    }
    public string ClientID { get; set; }
    public string ApiID { get; set; }
    public DateTime CheckInDate { get; set; }
    public DateTime CheckOut { get; set; }
    public string StatusCode { get; set; }

    public DataTable inserthotelinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@ClientRefID", ClientID);
        cmd.Parameters.AddWithValue("@APIRefID", ApiID);
        cmd.Parameters.AddWithValue("@CheckInDate", CheckInDate);
        cmd.Parameters.AddWithValue("@CheckOutDate", CheckOut);
        cmd.Parameters.AddWithValue("@SupplierStatus", StatusCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinserthotelinformation";
        return Dalref.ExeReader(cmd);
    }
    public string HotelInformationID { get; set; }
    public string ItemReference { get; set; }
    public string ItemCityCode { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string ItemStatusCode { get; set; }
    public string ItemConfirmationReference { get; set; }
    public string HotelPaxRoomAdults { get; set; }
    public string HotelPaxRoomChildren { get; set; }
    public string HotelPaxRoomCots { get; set; }
    public string SupplierRoomCategoryID { get; set; }
    public string RoomCategory { get; set; }
    public string MealBasis { get; set; }
    public string SharingBedding { get; set; }
    public string PAXRef { get; set; }
    public string Address { get; set; }
    public DataTable insertBookingHotelRoomsInformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", HotelInformationID);
        cmd.Parameters.AddWithValue("@ItemReference", ItemReference);
        cmd.Parameters.AddWithValue("@ItemCityCode", ItemCityCode);
        cmd.Parameters.AddWithValue("@ItemCode", ItemCode);
        cmd.Parameters.AddWithValue("@ItemName", ItemName);
        cmd.Parameters.AddWithValue("@ItemStatusCode", ItemStatusCode);
        cmd.Parameters.AddWithValue("@ItemConfirmationReference", ItemConfirmationReference);
        cmd.Parameters.AddWithValue("@CheckInDate", CheckInDate);
        cmd.Parameters.AddWithValue("@CheckOutDate", CheckOut);
        cmd.Parameters.AddWithValue("@HotelPaxRoomAdults", HotelPaxRoomAdults);
        cmd.Parameters.AddWithValue("@HotelPaxRoomChildren", HotelPaxRoomChildren);
        cmd.Parameters.AddWithValue("@HotelPaxRoomCots", HotelPaxRoomCots);
        cmd.Parameters.AddWithValue("@SupplierRoomCategoryID", SupplierRoomCategoryID);
        cmd.Parameters.AddWithValue("@RoomCategory", RoomCategory);
        cmd.Parameters.AddWithValue("@MealBasis", MealBasis);
        cmd.Parameters.AddWithValue("@SharingBedding", SharingBedding);
        cmd.Parameters.AddWithValue("@PAXRef", PAXRef);
        cmd.Parameters.AddWithValue("@HotelAddress", Address);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinserthotelBookingHotelRoomsInformation";
        return Dalref.ExeReader(cmd);
    }

    public int PaxTitle { get; set; }
    public string PaxFirstName { get; set; }
    public string PaxLastName { get; set; }
    public string PaxTypeAge { get; set; }
    public int PAXReff { get; set; }
    public Int64 HotelInformationIDD { get; set; }
    public DataTable insertHotelPaxInformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", HotelInformationIDD);
        cmd.Parameters.AddWithValue("@PAXRef", PAXReff);
        cmd.Parameters.AddWithValue("@PaxTitle", PaxTitle);
        cmd.Parameters.AddWithValue("@PaxFirstName", PaxFirstName);
        cmd.Parameters.AddWithValue("@PaxLastName", PaxLastName);
        cmd.Parameters.AddWithValue("@PaxTypeAge", PaxTypeAge);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertHotelPaxInformation";
        return Dalref.ExeReader(cmd);
    }

    public Int64 BookingRoomID { get; set; }
    public string AmendmentType { get; set; }
    public int Allowable { get; set; }
    public DataTable insertHotelRoomAmendmentStatus()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.Parameters.AddWithValue("@AmendmentType", AmendmentType);
        cmd.Parameters.AddWithValue("@Allowable", Allowable);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHotelRoomAmendmentStatus";
        return Dalref.ExeReader(cmd);
    }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public double ChargeAmount { get; set; }
    public int Chrage { get; set; }
    public string Currency { get; set; }
    public DataTable insertBookingHotelRoomCancellationCharges()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.Parameters.AddWithValue("@FromDate", FromDate);
        cmd.Parameters.AddWithValue("@ToDate", ToDate);
        cmd.Parameters.AddWithValue("@ChargeAmount", ChargeAmount);
        cmd.Parameters.AddWithValue("@Currency", Currency);
        cmd.Parameters.AddWithValue("@ConditionCharge", Chrage);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHotelRoomCancellationCharges";
        return Dalref.ExeReader(cmd);
    }
    public double GrossValue { get; set; }
    public string GrossCurrency { get; set; }
    public double NetValue { get; set; }
    public double Commission { get; set; }
    public double Discount { get; set; }
    public Int64 MarkUpType { get; set; }
    public double MarkUpValue { get; set; }
    public string MarkUpCurrency { get; set; }
    public string SellingCurrency { get; set; }
    public double PriceForClient { get; set; }
    public double AdditionalServiceFeebyClient { get; set; }
    public double TotalSellAmount { get; set; }
    public double RateOfExchange { get; set; }
    public DataTable insertHotelBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", BookingRoomID);
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@GrossCurrency", GrossCurrency);
        cmd.Parameters.AddWithValue("@NetValue", NetValue);
        cmd.Parameters.AddWithValue("@Commission", Commission);
        cmd.Parameters.AddWithValue("@Discount", Discount);
        cmd.Parameters.AddWithValue("@MarkUpType", MarkUpType);
        cmd.Parameters.AddWithValue("@MarkUpValue", MarkUpValue);
        cmd.Parameters.AddWithValue("@MarkUpCurrency", MarkUpCurrency);
        cmd.Parameters.AddWithValue("@SellingCurrency", SellingCurrency);
        cmd.Parameters.AddWithValue("@PriceForClient", PriceForClient);
        cmd.Parameters.AddWithValue("@AdditionalServiceFeebyClient", AdditionalServiceFeebyClient);
        cmd.Parameters.AddWithValue("@TotalSellAmount", TotalSellAmount);
        cmd.Parameters.AddWithValue("@RateOfExchange", RateOfExchange);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertHotelBookingPriceDetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable insertBookingHotelRoomPriceBreakup()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@GrossCurrency", GrossCurrency);
        cmd.Parameters.AddWithValue("@NetValue", NetValue);
        cmd.Parameters.AddWithValue("@Commission", Commission);
        cmd.Parameters.AddWithValue("@Discount", Discount);
        cmd.Parameters.AddWithValue("@MarkUpType", MarkUpType);
        cmd.Parameters.AddWithValue("@MarkUpValue", MarkUpValue);
        cmd.Parameters.AddWithValue("@MarkUpCurrency", MarkUpCurrency);
        cmd.Parameters.AddWithValue("@SellingCurrency", SellingCurrency);
        cmd.Parameters.AddWithValue("@PriceForClient", PriceForClient);
        cmd.Parameters.AddWithValue("@AdditionalServiceFeebyClient", AdditionalServiceFeebyClient);
        cmd.Parameters.AddWithValue("@TotalSellAmount", TotalSellAmount);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHotelRoomPriceBreakup";
        return Dalref.ExeReader(cmd);
    }
    public int upadteSupplierBooking()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@SupplierBookingReference", PNR);
        cmd.Parameters.AddWithValue("@BookingStatusCode", StatusCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcupdateHotelSupplierBookingReference";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetBookInfoDataDB()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgethotelbookinginformation";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetPassengerBookInfoDataDB()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgethotelbookingpassengerinformation";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetPassengerBrkup()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", PAXRef);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgethotelbookingpassengerbrkup";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetfareBrkup()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", PAXRef);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgethotelbookingfarebrkup";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Getcancellationpolicy()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", PAXRef);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgethotelbookingGetcancellationpolicy";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetHotelBookingHistory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", PAXRef);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select C.FirstName,C.LastName,A.*,B.* from tblBookingHistory A inner join tblBookingStatus B on B.BookingStatusCode=A.BookingStatusCode inner join tbluser C on C.UserID=A.UserID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
   public string City { get; set; }
   public string Hotel { get; set; }
   public string BookingDateFrom { get; set; }
   public string BookingDateTo { get; set; }
   public string CheckinDateFrom { get; set; }
   public string CheckinDateTo { get; set; }
   public string BookingRefNo { get; set; }
   public string SupplierRefNo { get; set; }
   public string PassengerName { get; set; }
   public Int64 ddlBranch { get; set; }
   public string ddlStatus { get; set; }
    public static object DbNullIfNull(object obj)
    {

        if (obj.ToString() == "0")
        {
            return obj != null ? obj : DBNull.Value;

        }
        else
        {
            return obj.ToString() != "" ? obj : DBNull.Value;
        }


    }
    public DataTable GetHotelSearchData()
    {

        string tempbranch = "";
        if (ddlBranch != 0)
        {
            tempbranch = ddlBranch.ToString();
        }
        //string tempstatus = "";
        //if (ddlStatus == "0")
        //{
        //    tempstatus = ddlStatus.ToString();
        //}

        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ItemCity", DbNullIfNull(City));
        cmd.Parameters.AddWithValue("@ItemName", DbNullIfNull(Hotel));
        cmd.Parameters.AddWithValue("@BookingDateFrom", DbNullIfNull(DbNullIfNull(BookingDateFrom)));
        cmd.Parameters.AddWithValue("@BookingDateTo", DbNullIfNull(BookingDateTo));
        cmd.Parameters.AddWithValue("@CheckinDateFrom", DbNullIfNull(CheckinDateFrom));
        cmd.Parameters.AddWithValue("@CheckinDateTo", DbNullIfNull(CheckinDateTo));
        cmd.Parameters.AddWithValue("@BookingRefNo", DbNullIfNull(BookingRefNo));
        cmd.Parameters.AddWithValue("@SupplierRefNo", DbNullIfNull(SupplierRefNo));
        cmd.Parameters.AddWithValue("@ddlBranch", DbNullIfNull(tempbranch));
        cmd.Parameters.AddWithValue("@ddlStatus", DbNullIfNull(ddlStatus));
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcHotelSearchBooking";
        return Dalref.ExeReader(cmd);
    }
    //public DataTable GetHotelSearchDataOnrequest()
    //{
    //    cmd.Parameters.Clear(); 
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    cmd.CommandText = "ProcHotelSearchBookingonrequest";
    //    return Dalref.ExeReader(cmd);
    //}
    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject)
    {
        DataTable DtEmailSettings = GetEmailSettings(7);
        if (DtEmailSettings.Rows.Count > 0)
        {

        }
        string from = "info@oneviewit.com";
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);
           
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential("info@oneviewit.com", "farishamsa007");
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }

    public DataTable GetEmailSettings(Int64 ESID)
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ESID", ESID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblMailSettings where ID=@ESID";
        return Dalref.ExeReader(cmd);
    }
    public int upadteBookingCancellationstatus()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", StatusCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcupdateHotelCancellationstatus";
        return Dalref.ExeNonQuery(cmd);
    }

    public double CancelAmount { get; set; }
    public string CancelCurrency { get; set; }
    public double CustomerAmount { get; set; }
    public double Dbcancellamount { get; set; }
    public DataTable InsertCancelBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@CancelNetAmount", CancelAmount);
        cmd.Parameters.AddWithValue("@CancelNetCurrency", CancelCurrency);
        cmd.Parameters.AddWithValue("@CustomerAmount", CustomerAmount);
        cmd.Parameters.AddWithValue("@CustomerCurrency", Currency);
        cmd.Parameters.AddWithValue("@DbCancelAmount", Dbcancellamount);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcInsertCancelBookingPriceDetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetCancelBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcGetCancelBookingPriceDetails";
        return Dalref.ExeReader(cmd);
    }
    public string VoucherUrl { get; set; }
    public int insertVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@VoucherUrl", VoucherUrl);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertVoucherUrlinformation";
        return Dalref.ExeNonQuery(cmd);

    }
    public DataTable selectVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcselectVoucherUrlinformation";
        return Dalref.ExeReader(cmd);

    }

    public DataTable UpdateHotelAdress()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", HotelInformationID);
        cmd.Parameters.AddWithValue("@HotelAddress", Address);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingHotelRoomsInformation set HotelAddress=@HotelAddress where 	BookingRoomID=@HotelInformationID";
        return Dalref.ExeReader(cmd);

    }
    public DataTable CheckVochurlinkavailable()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select VoucherUrl from tblVoucherDetails  where BookRefID=@BookRefID";
        return Dalref.ExeReader(cmd);
    }
    public string EssInfo { get; set; }

    public DataTable insertBookingEssentialInformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.Parameters.AddWithValue("@Information", EssInfo);
        cmd.Parameters.AddWithValue("@FromDate", FromDate);
        cmd.Parameters.AddWithValue("@ToDate", ToDate);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblBookingRoomEssentialInformation values(@BookingRoomID,@Information,@FromDate,@ToDate)";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetHotelEssentialInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblBookingRoomEssentialInformation where BookingRoomID=@BookingRoomID";
        return Dalref.ExeReader(cmd);
    }
    public string DestinationCode { get; set; }
    public string OfficeLocation { get; set; }
    public string Language { get; set; }
    public string International { get; set; }
    public string Local { get; set; }
    public string National { get; set; }
    public string OutOfOffice { get; set; }
    public string OfficeHours { get; set; }
    public DataTable insertGtaAOTNumber()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@DestinationCode", DestinationCode);
        cmd.Parameters.AddWithValue("@OfficeLocation", OfficeLocation);
        cmd.Parameters.AddWithValue("@Language", Language);
        cmd.Parameters.AddWithValue("@OfficeHours", OfficeHours);
        cmd.Parameters.AddWithValue("@International", International);
        cmd.Parameters.AddWithValue("@Local", Local);
        cmd.Parameters.AddWithValue("@National", National);
        cmd.Parameters.AddWithValue("@OutOfOffice", OutOfOffice);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblGtaAOTNumbers values(@BookRefID,@DestinationCode,@OfficeLocation,@Language,@OfficeHours,@International,@National,@Local,@OutOfOffice)";
        return Dalref.ExeReader(cmd);
    }
    public string AgencyID { get; set; }
    public DataTable GetHotelSearchDataOnrequest()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcHotelSearchBookingonrequest";
        return Dalref.ExeReader(cmd);
    }
    public int Changestatusoftblbookingonrequest()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode=@BookingStatusCode where BookingRefID=@BookingRefID";
        return Dalref.ExeNonQuery(cmd);
    }
    public Int64 UserID { get; set; }
    public DataTable SendMailid()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select EmailID from tblUser where UserID=@UserID ";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Getusername()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select FirstName,LastName from  tblUser where UserID=@UserID";
        return Dalref.ExeReader(cmd);
    }
    public string Emailbody { get; set; }
    public DataTable insertpendinginfo()
    {

        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PendingEmailBody", Emailbody);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert  into  tblpendinghotelbookinginformation values(@BookRefID,@PendingEmailBody)";
        return Dalref.ExeReader(cmd);
    }
    public DataTable selectpendinghotelbookinginformation()
    {

        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select  PendingEmailBody  from  tblpendinghotelbookinginformation where  BookRefID=@BookRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable insertintoAutoUpdateRequestlog()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@LastRequestedDate", BOOKDATE);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblAutoUpdateRequestlog values(@BookRefID,@LastRequestedDate)";
        return Dalref.ExeReader(cmd);
    }
    public DataTable selectCornjobpendingconfirmation()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcgetCornjobpendingconfirmation";
        return Dalref.ExeReader(cmd);
    }
    public DataTable selectCornjobpendingconfirmationOld()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcgetCornjobpendingconfirmationOld";
        return Dalref.ExeReader(cmd);
    }
    //upadte booking start
    public DataTable Updatehotelinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@ClientRefID", ClientID);
        cmd.Parameters.AddWithValue("@APIRefID", ApiID);
        cmd.Parameters.AddWithValue("@CheckInDate", CheckInDate);
        cmd.Parameters.AddWithValue("@CheckOutDate", CheckOut);
        cmd.Parameters.AddWithValue("@SupplierStatus", StatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingHotelInformation set ClientRefID=@ClientRefID,APIRefID=@APIRefID,CheckInDate=@CheckInDate,CheckOutDate=@CheckOutDate,SupplierStatus=@SupplierStatus where BookingRefID=@BookingRefID select HotelInformationID from tblBookingHotelInformation where BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable UpdateHotelPaxInformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", HotelInformationIDD);
        cmd.Parameters.AddWithValue("@PAXRef", PAXReff);
        cmd.Parameters.AddWithValue("@PaxTitle", PaxTitle);
        cmd.Parameters.AddWithValue("@PaxFirstName", PaxFirstName);
        cmd.Parameters.AddWithValue("@PaxLastName", PaxLastName);
        cmd.Parameters.AddWithValue("@PaxTypeAge", PaxTypeAge);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblHotelPax set PaxTitle=@PaxTitle,PaxFirstName=@PaxFirstName,PaxLastName=@PaxLastName,PaxTypeAge=@PaxTypeAge where PAXRef=@PAXRef and HotelInformationID=@HotelInformationID SELECT PaxID  FROM tblHotelPax where PAXRef=@PAXRef and HotelInformationID=@HotelInformationID";
        return Dalref.ExeReader(cmd);
    }

    public DataTable UpdateHotelBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", BookingRoomID);
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@GrossCurrency", GrossCurrency);
        cmd.Parameters.AddWithValue("@NetValue", NetValue);
        cmd.Parameters.AddWithValue("@Commission", Commission);
        cmd.Parameters.AddWithValue("@Discount", Discount);
        cmd.Parameters.AddWithValue("@MarkUpType", MarkUpType);
        cmd.Parameters.AddWithValue("@MarkUpValue", MarkUpValue);
        cmd.Parameters.AddWithValue("@MarkUpCurrency", MarkUpCurrency);
        cmd.Parameters.AddWithValue("@SellingCurrency", SellingCurrency);
        cmd.Parameters.AddWithValue("@PriceForClient", PriceForClient);
        cmd.Parameters.AddWithValue("@AdditionalServiceFeebyClient", AdditionalServiceFeebyClient);
        cmd.Parameters.AddWithValue("@TotalSellAmount", TotalSellAmount);
        cmd.Parameters.AddWithValue("@RateOfExchange", RateOfExchange);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingHotelBookingPriceDetails set GrossValue=@GrossValue,GrossCurrency=@GrossCurrency,NetValue=@NetValue,Commission=@Commission,Discount=@Discount,MarkUpType=@MarkUpType,MarkUpValue=@MarkUpValue,MarkUpCurrency=@MarkUpCurrency,SellingCurrency=@SellingCurrency,PriceForClient=@PriceForClient,AdditionalServiceFeebyClient=@AdditionalServiceFeebyClient,TotalSellAmount=@TotalSellAmount,RateOfExchange=@RateOfExchange where HotelInformationID=@HotelInformationID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable UpdateBookingHotelRoomsInformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@HotelInformationID", HotelInformationID);
        cmd.Parameters.AddWithValue("@ItemReference", ItemReference);
        cmd.Parameters.AddWithValue("@ItemCityCode", ItemCityCode);
        cmd.Parameters.AddWithValue("@ItemCode", ItemCode);
        cmd.Parameters.AddWithValue("@ItemName", ItemName);
        cmd.Parameters.AddWithValue("@ItemStatusCode", ItemStatusCode);
        cmd.Parameters.AddWithValue("@ItemConfirmationReference", ItemConfirmationReference);
        cmd.Parameters.AddWithValue("@CheckInDate", CheckInDate);
        cmd.Parameters.AddWithValue("@CheckOutDate", CheckOut);
        cmd.Parameters.AddWithValue("@HotelPaxRoomAdults", HotelPaxRoomAdults);
        cmd.Parameters.AddWithValue("@HotelPaxRoomChildren", HotelPaxRoomChildren);
        cmd.Parameters.AddWithValue("@HotelPaxRoomCots", HotelPaxRoomCots);
        cmd.Parameters.AddWithValue("@SupplierRoomCategoryID", SupplierRoomCategoryID);
        cmd.Parameters.AddWithValue("@RoomCategory", RoomCategory);
        cmd.Parameters.AddWithValue("@MealBasis", MealBasis);
        cmd.Parameters.AddWithValue("@SharingBedding", SharingBedding);
        cmd.Parameters.AddWithValue("@PAXRef", PAXRef);
        cmd.Parameters.AddWithValue("@HotelAddress", Address);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingHotelRoomsInformation set ItemCityCode=@ItemCityCode,ItemCode=@ItemCode,ItemName=@ItemName,ItemStatusCode=@ItemStatusCode,ItemConfirmationReference=@ItemConfirmationReference,CheckInDate=@CheckInDate,CheckOutDate=@CheckOutDate,HotelPaxRoomAdults=@HotelPaxRoomAdults,HotelPaxRoomChildren=@HotelPaxRoomChildren,HotelPaxRoomCots=@HotelPaxRoomCots,SupplierRoomCategoryID=@SupplierRoomCategoryID,RoomCategory=@RoomCategory,MealBasis=@MealBasis,SharingBedding=@SharingBedding,PAXRef=@PAXRef where HotelInformationID=@HotelInformationID and ItemReference=@ItemReference SELECT BookingRoomID  FROM tblBookingHotelRoomsInformation where HotelInformationID=@HotelInformationID and ItemReference=@ItemReference";
        return Dalref.ExeReader(cmd);
    }
    public int UpadteBookingHistoryDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ActionDate", BOOKDATE);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHistory";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable UpdateBookingHotelRoomPriceBreakup()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRoomID", BookingRoomID);
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@GrossCurrency", GrossCurrency);
        cmd.Parameters.AddWithValue("@NetValue", NetValue);
        cmd.Parameters.AddWithValue("@Commission", Commission);
        cmd.Parameters.AddWithValue("@Discount", Discount);
        cmd.Parameters.AddWithValue("@MarkUpType", MarkUpType);
        cmd.Parameters.AddWithValue("@MarkUpValue", MarkUpValue);
        cmd.Parameters.AddWithValue("@MarkUpCurrency", MarkUpCurrency);
        cmd.Parameters.AddWithValue("@SellingCurrency", SellingCurrency);
        cmd.Parameters.AddWithValue("@PriceForClient", PriceForClient);
        cmd.Parameters.AddWithValue("@AdditionalServiceFeebyClient", AdditionalServiceFeebyClient);
        cmd.Parameters.AddWithValue("@TotalSellAmount", TotalSellAmount);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingHotelRoomPriceBreakup set GrossValue=@GrossValue,GrossCurrency=@GrossCurrency,NetValue=@NetValue,Commission=@Commission,Discount=@Discount,MarkUpType=@MarkUpType,MarkUpValue=@MarkUpValue,MarkUpCurrency=@MarkUpCurrency,SellingCurrency=@SellingCurrency,PriceForClient=@PriceForClient,AdditionalServiceFeebyClient=@AdditionalServiceFeebyClient,TotalSellAmount=@TotalSellAmount where BookingRoomID=@BookingRoomID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetStatusFromHotelRoom()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select SupplierStatus from tblBookingHotelInformation where BookingRefID=@BookingReferenceID";
        return Dalref.ExeReader(cmd);
    }
    public string BookingStatusCoderoom { get; set; }
    public int UpdateStatusFromHotelRoom()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@SupplierStatus", BookingStatusCoderoom);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update  tblBookingHotelInformation set SupplierStatus=@SupplierStatus where BookingRefID=@BookingReferenceID";
        return Dalref.ExeNonQuery(cmd);
    }
    //update booking end
    public DataTable GetfullAgencydetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select * from tblAgency where AgencyID=@AgencyID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetMyBookHotelinfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@Today", System.DateTime.Now);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgetmyhotelbookinginfo";
        return Dalref.ExeReader(cmd);
    }
    public  string TravellID{ get;set;}

    public DataTable Gettravellerdetailstoedit()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procgetfrequentflyerdetails";
        return Dalref.ExeReader(cmd);
    }
    //srf
    public int Type { get; set; }
    public string Qstring { get; set; }
    public string EmailID { get; set; }
    public string ContactNo { get; set; }
    public Int64 CreatedBy { get; set; }
    public DateTime CreatedDate { get; set; }
    public int Status { get; set; }
    public DataTable insertQtndiv()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Type", Type);
        cmd.Parameters.AddWithValue("@Qstring", Qstring);
        cmd.Parameters.AddWithValue("@EmailID", EmailID);
        cmd.Parameters.AddWithValue("@ContactNo", ContactNo);
        cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
        cmd.Parameters.AddWithValue("@CreatedDate", CreatedDate);
        cmd.Parameters.AddWithValue("@Status", Status);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertqtjndivt";
        return Dalref.ExeReader(cmd);
    }    
    public DataTable GetGTAImageCountrycode()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CityCode", City);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select CountryCode from tblGTACity where Name=@CityCode";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetCountrycode()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CityCode", City);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select CountryCode from tblCity where CityCode=@CityCode";
        return Dalref.ExeReader(cmd);
    }
    public string DebitID { get; set; }
    public int InsertTblBookingDebitEntry()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@DebitID", DebitID);
        cmd.Parameters.AddWithValue("@BookingStatus", BookingStatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblBookingDebitEntry values(@BookingRefID,@DebitID,@BookingStatus)";
        return Dalref.ExeNonQuery(cmd);
    }
}