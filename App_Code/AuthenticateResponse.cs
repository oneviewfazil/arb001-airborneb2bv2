﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AuthenticateResponse
/// </summary>
public class AuthenticateResponse
{
    public User user { get; set; }
    public class Title
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class SolutionTypeList
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Tenant
    {
        public int id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public List<SolutionTypeList> solutionTypeList { get; set; }
    }

    public class LoginNode
    {
        public string code { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public Tenant tenant { get; set; }
    }

    public class RoleList
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string loginId { get; set; }
        public Title title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string status { get; set; }
        public LoginNode loginNode { get; set; }
        public List<RoleList> roleList { get; set; }
    }


}