﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for BALDBHotel
/// </summary>
public class BALDBHotel
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();
    public BALDBHotel()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Int64 AgencyID { get; set; }
    public Int64 USERID { get; set; }
    public DataTable GetSumDepoCreditPayment()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetSumDepoCreditPayment";
        return Dalref.ExeReader(cmd);
    }
    public int ServiceTypeId { get; set; }
    public DataTable GetBookingStatCount()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.Parameters.AddWithValue("@ServiceTypeId", ServiceTypeId);
        cmd.Parameters.AddWithValue("@UserID", USERID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "GetAllBookingStatusCountAgencyNew";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetActionReqFlight()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetActionReqFlight";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetActionReqFlightnew()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetActionReqFlightnew";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetActionReqHotel()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetActionReqHotelNew";
        return Dalref.ExeReader(cmd);
    }
}