﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmailV
/// </summary>
public class EmailV
{
   
        public int UserID { get; set; }
        public int AgencyID { get; set; }
        public int TitleID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Mobile { get; set; }
        public string EmailID { get; set; }
        public string Designation { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int LoginStatus { get; set; }
        public int CreatedBy { get; set; }
        public int AdminStatus { get; set; }
        public int AgencyID1 { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyName { get; set; }
        public int AgencyTypeID { get; set; }
        public string CityCode { get; set; }
        public string Address { get; set; }
        public int PostCode { get; set; }
        public string Fax { get; set; }
        public string Office { get; set; }
        public string Residence { get; set; }
        public string EmailID1 { get; set; }
        public string Website { get; set; }
        public int LoginStatus1 { get; set; }
        public int CreditAgentStatus { get; set; }
        public int CreatedBy1 { get; set; }
        public int lft { get; set; }
        public int rgt { get; set; }
        public int AgencyTypeID1 { get; set; }
        public string AgencyType { get; set; }
        public string CityCode1 { get; set; }
        public string CountryCode { get; set; }
        public string CityName { get; set; }
        public string GTAHotelCountryCode { get; set; }
        public string CountryCode1 { get; set; }
        public string CounryName { get; set; }
        public int AgencyLoginStat { get; set; }
        public int AgencyIDD { get; set; }
        public int AgencyTypeIDD { get; set; }
    
}