﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for BALBook
/// </summary>
public class BALBook
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();
    public string AgencyID = ConfigurationManager.AppSettings["AgencyID"];
    public string WebConGloUserID = ConfigurationManager.AppSettings["AgencyUserID"];
    public BALBook()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Bussiness Objects ...
    public Int64 UID { get; set; }
    public string PNR { get; set; }
    public DateTime BOOKDATE { get; set; }
    public DateTime CANCELDATE { get; set; }
    public DataTable InsertNewBookingDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Position", "1");
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", "1");
        cmd.Parameters.AddWithValue("@BookingDate", BOOKDATE);
        cmd.Parameters.AddWithValue("@SupplierID", "1");
        cmd.Parameters.AddWithValue("@SupplierBookingReference", "");
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", "RQ");
        cmd.Parameters.AddWithValue("@PaymentStatusID", "1");
        cmd.Parameters.AddWithValue("@AgencyRemarks", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertnewbooking";
        return Dalref.ExeReader(cmd);

    }
    public string PAXID { get; set; }
    public string TITLE { get; set; }
    public string BOOKINGREFID { get; set; }
    public string FNAME { get; set; }
    public string SNAME { get; set; }
    public string EMAIL { get; set; }
    public string TELEPHONE { get; set; }
    public string LOCATIONCODE { get; set; }
    public DateTime DOB { get; set; }

    public DataTable InsertPassengerBookingDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@TitleID", TITLE);
        cmd.Parameters.AddWithValue("@PaxTypeID", PAXID);
        cmd.Parameters.AddWithValue("@GivenName", FNAME);
        cmd.Parameters.AddWithValue("@Surname", SNAME);
        cmd.Parameters.AddWithValue("@EmailID", EMAIL);
        cmd.Parameters.AddWithValue("@TelPhoneType", TELEPHONE);
        cmd.Parameters.AddWithValue("@LocationCode", LOCATIONCODE);
        cmd.Parameters.AddWithValue("@DateofBirth", DOB);
        cmd.Parameters.AddWithValue("@PNR", "");
        cmd.Parameters.AddWithValue("@ETicketNo", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirPassengers";
        return Dalref.ExeReader(cmd);
    }

    public string DOCTYPE { get; set; }
    public string DOCNO { get; set; }
    public DateTime DOCEXPIRYDATE { get; set; }
    public string DOCISSUECUNTRY { get; set; }

    public DataTable InsertPassengerDocsDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@PaxID", PAXID);
        cmd.Parameters.AddWithValue("@DocTypeID", DOCTYPE);
        cmd.Parameters.AddWithValue("@DocumentNumber", DOCNO);
        cmd.Parameters.AddWithValue("@ExpiryDate", DOCEXPIRYDATE);
        cmd.Parameters.AddWithValue("@IssueLocation", "");
        cmd.Parameters.AddWithValue("@IssueCountry", DOCISSUECUNTRY);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirPassengerDocument";
        return Dalref.ExeReader(cmd);
    }
    public string BookingStatusCode { get; set; }
    public int InsertBookingHistoryDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ActionDate", BOOKDATE);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHistory";
        return Dalref.ExeNonQuery(cmd);
    }

    public int upadtepnrBooking()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@SupplierBookingReference", PNR);
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", "HK");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcupdateSupplierBookingReference";
        return Dalref.ExeNonQuery(cmd);
    }
    public int Upadtepnrairpassenger()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@SupplierBookingReference", PNR);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procupdatepnrinairpassengers";
        return Dalref.ExeNonQuery(cmd);
    }

    public string REFNO { get; set; }
    public string DIRECTIONID { get; set; }
    public string ELASPED { get; set; }
    public DataTable InsertAirOriginOptions()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@RefNumber", REFNO);
        cmd.Parameters.AddWithValue("@DirectionId", DIRECTIONID);
        cmd.Parameters.AddWithValue("@ElapsedTime", ELASPED);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirOriginDestinationOptions";
        return Dalref.ExeReader(cmd);
    }
    //Insert TblPayment
    public Int64 BookingRefID { get; set; }
    public string PaidAmount { get; set; }
    public string CurrencyCode { get; set; }
    public Int32 PaymentTypeID { get; set; }
    public int InsertTblPayment()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.Parameters.AddWithValue("@PaidAmount", PaidAmount);
        cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
        cmd.Parameters.AddWithValue("@PaidDate", DateTime.Now);
        cmd.Parameters.AddWithValue("@PaymentTypeID", PaymentTypeID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblPayment values(@BookingRefID,@PaidAmount,@CurrencyCode,@PaidDate,@PaymentTypeID) update tblBooking set PaymentStatusID=3 where BookingRefID=@BookingRefID";
        return Dalref.ExeNonQuery(cmd);
    }

    public Double TotalBaseNet { get; set; }
    public Double TotalFare { get; set; }
    public Double XmlMarkup { get; set; }
    public string NetCurrency { get; set; }
    public Double MarkupTypeID { get; set; }
    public Double MarkupValue { get; set; }
    public Double XMLMarkupValueDivision { get; set; }
    public string CurrenyCode { get; set; }
    public Double SellAmount { get; set; }
    public DataTable InsertAirBookingCost()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@TotalBaseNet", TotalBaseNet);
        cmd.Parameters.AddWithValue("@TotalTaxNet", "");
        cmd.Parameters.AddWithValue("@TotalNet", TotalFare);
        cmd.Parameters.AddWithValue("@NetCurrency", NetCurrency);
        cmd.Parameters.AddWithValue("@MarkupTypeID", MarkupTypeID);
        cmd.Parameters.AddWithValue("@MarkupValue", MarkupValue);
        cmd.Parameters.AddWithValue("@MarkupCurrency", CurrenyCode);
        cmd.Parameters.AddWithValue("@SellAmount", SellAmount);
        cmd.Parameters.AddWithValue("@SellCurrency", NetCurrency);
        cmd.Parameters.AddWithValue("@AdditionalServiceFee", XmlMarkup);
        cmd.Parameters.AddWithValue("@CancellationAmount", "");
        cmd.Parameters.AddWithValue("@CancellationCurrency", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirBookingCost";
        return Dalref.ExeReader(cmd);
    }
    public Int64 Costbreakup { get; set; }
    public Double Tax { get; set; }
    public Double BaseFare { get; set; }
    public string PassengerType { get; set; }
    public Int64 PassengerQuantity { get; set; }
    public int InsertAirBookingCostBreakup()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingCostID", Costbreakup);
        cmd.Parameters.AddWithValue("@BaseNet", BaseFare);
        cmd.Parameters.AddWithValue("@TaxNet", Tax);
        cmd.Parameters.AddWithValue("@TotalNet", TotalFare);
        cmd.Parameters.AddWithValue("@NetCurrency", NetCurrency);
        cmd.Parameters.AddWithValue("@MarkupTypeID", MarkupTypeID);
        cmd.Parameters.AddWithValue("@MarkupValue", MarkupValue);
        cmd.Parameters.AddWithValue("@MarkupCurrency", CurrenyCode);
        cmd.Parameters.AddWithValue("@SellAmount", SellAmount);
        cmd.Parameters.AddWithValue("@SellCurrency", NetCurrency);
        cmd.Parameters.AddWithValue("@AdditionalServiceFee", XMLMarkupValueDivision);
        cmd.Parameters.AddWithValue("@PaxType", PassengerType);
        cmd.Parameters.AddWithValue("@NoOfPAx", PassengerQuantity);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirBookingCostBreakup";
        return Dalref.ExeNonQuery(cmd);
    }
    public string OriginDestinationID { get; set; }
    public DateTime Depttime { get; set; }
    public DateTime Arritime { get; set; }
    public string Fno { get; set; }
    public string Fromloc { get; set; }
    public string Depterminal { get; set; }
    public string Toloc { get; set; }
    public string Arriterminal { get; set; }
    public string Operatingairline { get; set; }
    public string Equipment { get; set; }
    public string MarketingAirline { get; set; }
    public DataTable InsertAirSegment()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@OriginDestinationID", OriginDestinationID);
        cmd.Parameters.AddWithValue("@DepartureDateTime", Depttime);
        cmd.Parameters.AddWithValue("@ArrivalDateTime", Arritime);
        cmd.Parameters.AddWithValue("@FlightNumber", Fno);
        cmd.Parameters.AddWithValue("@Status", "1");
        cmd.Parameters.AddWithValue("@DepartureAirportLocationCode", Fromloc);
        cmd.Parameters.AddWithValue("@DepartureAirportTerminal", Depterminal);
        cmd.Parameters.AddWithValue("@ArrivalAirportLocationCode", Toloc);
        cmd.Parameters.AddWithValue("@ArrivalAirportTerminal", Arriterminal);
        cmd.Parameters.AddWithValue("@OperatingAirlineCode", Operatingairline);
        cmd.Parameters.AddWithValue("@EquipmentAirEquipType", Equipment);
        cmd.Parameters.AddWithValue("@MarketingAirlineCode", MarketingAirline);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinserttblAirSegment";
        return Dalref.ExeReader(cmd);
    }
    public Int64 SegmentID { get; set; }
    public DataTable GetSegmentID()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Frmloc", Fromloc);
        cmd.Parameters.AddWithValue("@Toloc", Toloc);
        cmd.Parameters.AddWithValue("@RefID", REFNO);
        cmd.Parameters.AddWithValue("@DirectionID", DIRECTIONID);
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgetsegmentidforbookavail";
        return Dalref.ExeReader(cmd);
    }
    public string LocationCode { get; set; }
    public DateTime DepartureDateTime { get; set; }
    public DateTime ArrivalDateTime { get; set; }
    public int InsertAirsegmentStopOver()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@SegmentID", SegmentID);
        cmd.Parameters.AddWithValue("@LoccationCode", LocationCode);
        cmd.Parameters.AddWithValue("@DepartureTime", DepartureDateTime);
        cmd.Parameters.AddWithValue("@ArrivalTime", ArrivalDateTime);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirSegmentStopOver";
        return Dalref.ExeNonQuery(cmd);
    }
    public string ResBookDesigCode { get; set; }
    public string AvailablePTC { get; set; }
    public string ResBookDesigCabinCode { get; set; }
    public string FareBasis { get; set; }
    public int InsertAirsegmentBookingAvail()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@SegmentID", SegmentID);
        cmd.Parameters.AddWithValue("@ResBookDesigCode", ResBookDesigCode);
        cmd.Parameters.AddWithValue("@AvailablePTC", AvailablePTC);
        cmd.Parameters.AddWithValue("@ResBookDesigCabinCode", ResBookDesigCabinCode);
        cmd.Parameters.AddWithValue("@FareBasis", FareBasis);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirSegmentBookingAvail";
        return Dalref.ExeNonQuery(cmd);
    }
    public int Quantity { get; set; }
    public string Unit { get; set; }
    public int insertAirbaggageDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PAXType", PassengerType);
        cmd.Parameters.AddWithValue("@CabinBaggageQuantity", 0);
        cmd.Parameters.AddWithValue("@CabinBaggageUnit", "");
        cmd.Parameters.AddWithValue("@CheckinBaggageQuantity", Quantity);
        cmd.Parameters.AddWithValue("@CheckinBaggageUnit", Unit);
        cmd.Parameters.AddWithValue("@FromSeg", Fromloc);
        cmd.Parameters.AddWithValue("@ToSeg", Toloc);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertAirbaggageDetails";

        return Dalref.ExeNonQuery(cmd);
    }
    public int DeletebookingInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procdeletebookingdetails";
        return Dalref.ExeNonQuery(cmd);
    }

    public string FareRule { get; set; }
    public string Segment { get; set; }
    public string FareRef { get; set; }

    public DataTable InsertFareRule()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@FareRule", FareRule);
        cmd.Parameters.AddWithValue("@Segment", Segment);
        cmd.Parameters.AddWithValue("@FareRef", FareRef);
        cmd.Parameters.AddWithValue("@FilingAirline", Operatingairline);
        cmd.Parameters.AddWithValue("@MarketingAirline", MarketingAirline);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procinsertfarerule";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Updatelasttktdate()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set CancellationDeadline=@CancellationDeadline where BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);

    }
    public DataTable Chechingcrntusrexsist()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@EmailID", EMAIL);
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.Text;
        //cmd.CommandText = "ProcgetuserIdforbooking";
        cmd.CommandText = "select UserID from tblUser where EmailID=@EmailID and AgencyID=@AgencyID";
        return Dalref.ExeReader(cmd);
    }
    public string PASSWORD { get; set; }
    public string PASSWORDEncrypt { get; set; }
    public DataTable InsertUserDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.Parameters.AddWithValue("@TitleID", TITLE);
        cmd.Parameters.AddWithValue("@FirstName", FNAME);
        cmd.Parameters.AddWithValue("@LastName", SNAME);
        cmd.Parameters.AddWithValue("@Contact", "");
        cmd.Parameters.AddWithValue("@Mobile", TELEPHONE);
        cmd.Parameters.AddWithValue("@EmailID", EMAIL);
        cmd.Parameters.AddWithValue("@Designation", "");
        cmd.Parameters.AddWithValue("@Username", EMAIL);
        cmd.Parameters.AddWithValue("@Password", PASSWORDEncrypt);
        cmd.Parameters.AddWithValue("@LoginStatus", "1");
        cmd.Parameters.AddWithValue("@CreatedBy", WebConGloUserID);
        cmd.Parameters.AddWithValue("@AdminStatus", "");
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Insert into tblUser Values ( @AgencyID, @TitleID, @FirstName, @LastName, @Contact, @Mobile, @EmailID, @Designation, @Username, @Password, @LoginStatus, @CreatedBy, @AdminStatus ) select UserID from tblUser where UserID =SCOPE_IDENTITY();";
        //cmd.CommandText = "ProcInsertAdduserbooking";
        return Dalref.ExeReader(cmd);
    }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string ZIPCode { get; set; }
    public string Country { get; set; }
    public DataTable insertpassaddress()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@Address1", Address1);
        cmd.Parameters.AddWithValue("@Address2", Address2);
        cmd.Parameters.AddWithValue("@City", City);
        cmd.Parameters.AddWithValue("@Postalcode", ZIPCode);
        cmd.Parameters.AddWithValue("@Country", Country);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertaddressdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetEmailData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UID", UID);
        cmd.Parameters.AddWithValue("@EmailID", EMAIL);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procnewemailchecking";
        return Dalref.ExeReader(cmd);
    }
    public string Gender { get; set; }
    public DataTable CheckTravellerexsist()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@PassportNo", DOCNO);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcCheckTravellerExist";
        return Dalref.ExeReader(cmd);
    }
    public int inserttravellerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@Title", TITLE);
        cmd.Parameters.AddWithValue("@FirstName", FNAME);
        cmd.Parameters.AddWithValue("@SurName", SNAME);
        cmd.Parameters.AddWithValue("@DOB", DOB);
        cmd.Parameters.AddWithValue("@PassportNo", DOCNO);
        cmd.Parameters.AddWithValue("@PassportExpiryDate", DOCEXPIRYDATE);
        cmd.Parameters.AddWithValue("@Nationality", DOCISSUECUNTRY);
        cmd.Parameters.AddWithValue("@Gender", Gender);
        cmd.Parameters.AddWithValue("@IssueCountry", DOCISSUECUNTRY);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinserttotraveller";
        return Dalref.ExeNonQuery(cmd);
    }

    //NEW 05-10-2017
    //B2C
    public DataTable Gettravellerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procgettravellerdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable UpdateEmailData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UID", UID);
        cmd.Parameters.AddWithValue("@EmailID", EMAIL);
        cmd.Parameters.AddWithValue("@Password", PASSWORDEncrypt);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procupdatenewemail";
        return Dalref.ExeReader(cmd);
    }
    public DataTable upadtepassengerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@TitleID", TITLE);
        cmd.Parameters.AddWithValue("@FirstName", FNAME);
        cmd.Parameters.AddWithValue("@LastName", SNAME);
        cmd.Parameters.AddWithValue("@Mobile", TELEPHONE);
        cmd.Parameters.AddWithValue("@Address1", Address1);
        cmd.Parameters.AddWithValue("@City", City);
        cmd.Parameters.AddWithValue("@Postalcode", ZIPCode);
        cmd.Parameters.AddWithValue("@Country", Country);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procupdatepassengeraddressdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetUserAddress()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procaddressdetails";
        return Dalref.ExeReader(cmd);
    }
    public string TravellID { get; set; }
    public DataTable Delatetravellerinfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procdeletetravellerdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Gettravellerdetailstoedit()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procgetfrequentflyerdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Edittravellerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@Title", TITLE);
        cmd.Parameters.AddWithValue("@FirstName", FNAME);
        cmd.Parameters.AddWithValue("@SurName", SNAME);
        cmd.Parameters.AddWithValue("@DOB", DOB);
        cmd.Parameters.AddWithValue("@PassportNo", DOCNO);
        cmd.Parameters.AddWithValue("@PassportExpiryDate", DOCEXPIRYDATE);
        cmd.Parameters.AddWithValue("@Nationality", DOCISSUECUNTRY);
        cmd.Parameters.AddWithValue("@Gender", Gender);
        cmd.Parameters.AddWithValue("@IssueCountry", DOCISSUECUNTRY);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procedittotraveller";
        return Dalref.ExeReader(cmd);
    }
    public string FFAirline { get; set; }
    public string FrequentFlyer { get; set; }
    public Int64 FFID { get; set; }
    public DataTable AddFrequentFlyerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.Parameters.AddWithValue("@AirLineCode", FFAirline);
        cmd.Parameters.AddWithValue("@FFNumber", FrequentFlyer);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procinsertb2ctravellerfrquentflyer";
        return Dalref.ExeReader(cmd);
    }
    public DataTable BindFrequentFlyerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblB2cTravellerFrequentFlyer where TravellerID=@TravellerID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable DeleteFrequentFlyerdetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@FFID", FFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "delete from tblB2cTravellerFrequentFlyer where FFID=@FFID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetMyBookingInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@Today", System.DateTime.Now);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgetmybookinginfo";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetFareRulesInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblAirFarerules where BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookingHistory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select C.FirstName,C.LastName,A.*,B.* from tblBookingHistory A inner join tblBookingStatus B on B.BookingStatusCode=A.BookingStatusCode inner join tbluser C on C.UserID=A.UserID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetFFDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TravellerID", TravellID);
        cmd.Parameters.AddWithValue("@AirLineCode", MarketingAirline);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblB2cTravellerFrequentFlyer where TravellerID=@TravellerID and AirLineCode=@AirLineCode";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetFullUserAddress()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcGetFullUserAddressdetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable upadtepassengerdetailsNew()
    {
        try
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@UserID", UID);
            cmd.Parameters.AddWithValue("@TitleID", TITLE);
            cmd.Parameters.AddWithValue("@FirstName", FNAME);
            cmd.Parameters.AddWithValue("@LastName", SNAME);
            cmd.Parameters.AddWithValue("@Mobile", TELEPHONE);
            cmd.Parameters.AddWithValue("@Address1", Address1);
            cmd.Parameters.AddWithValue("@Address2", Address2);
            cmd.Parameters.AddWithValue("@City", City);
            cmd.Parameters.AddWithValue("@Postalcode", ZIPCode);
            cmd.Parameters.AddWithValue("@Country", Country);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "begin IF EXISTS (SELECT * FROM tblPassengerAddress where UserID=@UserID) BEGIN update tblPassengerAddress set Address1=@Address1,Address2=@Address2,City=@City,Postalcode=@Postalcode,Country=@Country where UserID=@UserID END ELSE BEGIN insert into tblPassengerAddress values(@UserID,@Address1,@Address2,@City,@Postalcode,@Country) END update tblUser set TitleID=@TitleID,FirstName=@FirstName,LastName=@LastName,Mobile=@Mobile where UserID=@UserID end";
            return Dalref.ExeReader(cmd);
        }
        catch (Exception e)
        {
            throw;
        }
    }
}

