﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for BALDBTransfer
/// </summary>
public class BALDBTransfer
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();
    public BALDBTransfer()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Int64 AgencyID { get; set; }
    public DataTable GetActionReqTransfer()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetActionReqTransfer";
        return Dalref.ExeReader(cmd);
    }
    public Int64 BookingRefId { get; set; }
    public DataTable getallBookingValue()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookinRefId", BookingRefId);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetallTranferBookingDetails";
        return Dalref.ExeReader(cmd);
    }
}