﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for Pay
/// </summary>
public class Pay
{
    public Pay()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string sha256(string payfortaccesscode, string payfortlanguage, string payfortmerchantidentifier, string bookrefnum, string payfortcommand, double totpayamount, string payfortcurrency, string customeremail, string payfortreturl)
    {
        string SHARequestPhrase = ConfigurationManager.AppSettings["payfortSHARequestPhrase"];
        string HashString = SHARequestPhrase + "access_code=" + payfortaccesscode + "amount="
            + totpayamount + "command=" + payfortcommand + "currency=" + payfortcurrency + "customer_email="
            + customeremail + "language=" + payfortlanguage + "merchant_identifier="
            + payfortmerchantidentifier + "merchant_reference=" + bookrefnum + "return_url=" + payfortreturl + SHARequestPhrase;

        //HashString = "PASSaccess_code=SILgpo7pWbmzuURp2qriamount=1000command=PURCHASEcurrency=USDcustomer_email=test@gmail.comlanguage=enmerchant_identifier=MxvOupuGmerchant_reference=Test010PASS";
        //HashString = HashString.Trim();

        SHA256Managed crypt = new SHA256Managed();
        string hash = String.Empty;
        byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(HashString), 0, Encoding.ASCII.GetByteCount(HashString));
        foreach (byte theByte in crypto)
        {
            hash += theByte.ToString("x2");
        }
        return hash;
    }
    public string GetPayData(string totpayamount, string customeremail)
    {
        //double totalconvertedtoaed = Convert.ToDouble(totpayamount) * 9.54;
        string payfortcommand = "AUTHORIZATION";
        string payfortactlink = ConfigurationManager.AppSettings["payfortactlink"];
        string payfortaccesscode = ConfigurationManager.AppSettings["payfortaccesscode"];
        string payfortlanguage = ConfigurationManager.AppSettings["payfortlanguage"];
        string payfortmerchantidentifier = ConfigurationManager.AppSettings["payfortmerchantidentifier"];
        string payfortcurrency = ConfigurationManager.AppSettings["payfortcurrency"];
        string payfortreturl = ConfigurationManager.AppSettings["payfortReturnURL"];
        if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
        {
            payfortreturl = "http://" + HttpContext.Current.Request.Url.Authority + "/FLIGHT/pay.aspx";
        }
        string bookrefnum = "" + ConfigurationManager.AppSettings["AgencyRef"] + "-" + HttpContext.Current.Session["BookingRefID"].ToString();
        double AmountMultiplied = Convert.ToDouble(totpayamount) * Convert.ToDouble(ConfigurationManager.AppSettings["payfortCurrencyMultiplier"]);
        string signature = sha256(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, bookrefnum, payfortcommand, AmountMultiplied, payfortcurrency, customeremail, payfortreturl);


        string outform = "<form method=\"post\" action=" + payfortactlink +
            " id=\"form1\" name=\"formpay\">" +
            "<input type=\"hidden\" name=\"command\" value='" + payfortcommand + "' id=\"command\" />" +
            "<input type=\"hidden\" name=\"access_code\" value='" + payfortaccesscode + "' id=\"access_code\" />" +
            "<input type=\"hidden\" name=\"merchant_identifier\" value='" + payfortmerchantidentifier + "' id=\"merchant_identifier\" />" +
            "<input type=\"hidden\" name=\"amount\" value='" + AmountMultiplied + "' id=\"amount\" />" +
            "<input type=\"hidden\" name=\"currency\" value='" + payfortcurrency + "' id=\"currency\" />" +
            "<input type=\"hidden\" name=\"language\" value='" + payfortlanguage + "' id=\"language\" />" +
            "<input type=\"hidden\" name=\"customer_email\" value='" + customeremail + "' id=\"customeremail\" />" +
            "<input type=\"hidden\" name=\"signature\" value='" + signature + "' id=\"signature\" />" +
            "<input type=\"hidden\" name=\"merchant_reference\" value='" + bookrefnum + "' id=\"merchant_reference\" />" +
            "<input type=\"hidden\" name=\"return_url\" value='" + payfortreturl + "' id=\"return_url\" />" +
            "<input type=\"submit\" value=\"PAY\" id=\"btnPayFort\" style='display:none;' /> </form>";
        return outform;
    }
    public string GetPayDataHotel(string totpayamount, string customeremail)
    {
        string payfortcommand = "AUTHORIZATION";
        string payfortactlink = ConfigurationManager.AppSettings["payfortactlink"];
        string payfortaccesscode = ConfigurationManager.AppSettings["payfortaccesscode"];
        string payfortlanguage = ConfigurationManager.AppSettings["payfortlanguage"];
        string payfortmerchantidentifier = ConfigurationManager.AppSettings["payfortmerchantidentifier"];
        string payfortcurrency = ConfigurationManager.AppSettings["payfortcurrency"];
        string payfortreturl = ConfigurationManager.AppSettings["payfortReturnURLHotel"];
        string bookrefnum = "" + ConfigurationManager.AppSettings["AgencyRef"] + "-" + HttpContext.Current.Session["HotelBookingRefID"].ToString();
        double AmountMultiplied = Convert.ToDouble(totpayamount) * Convert.ToDouble(ConfigurationManager.AppSettings["payfortCurrencyMultiplier"]);
        string signature = sha256(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, bookrefnum, payfortcommand, AmountMultiplied, payfortcurrency, customeremail, payfortreturl);


        string outform = "<form method=\"post\" action=" + payfortactlink +
            " id=\"form1\" name=\"formpay\">" +
            "<input type=\"hidden\" name=\"command\" value='" + payfortcommand + "' id=\"command\" />" +
            "<input type=\"hidden\" name=\"access_code\" value='" + payfortaccesscode + "' id=\"access_code\" />" +
            "<input type=\"hidden\" name=\"merchant_identifier\" value='" + payfortmerchantidentifier + "' id=\"merchant_identifier\" />" +
            "<input type=\"hidden\" name=\"amount\" value='" + AmountMultiplied + "' id=\"amount\" />" +
            "<input type=\"hidden\" name=\"currency\" value='" + payfortcurrency + "' id=\"currency\" />" +
            "<input type=\"hidden\" name=\"language\" value='" + payfortlanguage + "' id=\"language\" />" +
            "<input type=\"hidden\" name=\"customer_email\" value='" + customeremail + "' id=\"customeremail\" />" +
            "<input type=\"hidden\" name=\"signature\" value='" + signature + "' id=\"signature\" />" +
            "<input type=\"hidden\" name=\"merchant_reference\" value='" + bookrefnum + "' id=\"merchant_reference\" />" +
            "<input type=\"hidden\" name=\"return_url\" value='" + payfortreturl + "' id=\"return_url\" />" +
            "<input type=\"submit\" value=\"PAY\" id=\"btnPayFort\" style='display:none;' /> </form>";

        return outform;
    }
    public string GetPayDataInsurance(string totpayamount, string customeremail)
    {
        string payfortcommand = "AUTHORIZATION";
        string payfortactlink = ConfigurationManager.AppSettings["payfortactlink"];
        string payfortaccesscode = ConfigurationManager.AppSettings["payfortaccesscode"];
        string payfortlanguage = ConfigurationManager.AppSettings["payfortlanguage"];
        string payfortmerchantidentifier = ConfigurationManager.AppSettings["payfortmerchantidentifier"];
        string payfortcurrency = ConfigurationManager.AppSettings["payfortcurrency"];
        string payfortreturl = ConfigurationManager.AppSettings["payfortReturnURLInsurance"];
        string bookrefnum = "" + ConfigurationManager.AppSettings["AgencyRef"] + "-" + HttpContext.Current.Session["InsuranceBookingRefID"].ToString();
        double AmountMultiplied = Convert.ToDouble(totpayamount) * Convert.ToDouble(ConfigurationManager.AppSettings["payfortCurrencyMultiplier"]);
        string signature = sha256(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, bookrefnum, payfortcommand, AmountMultiplied, payfortcurrency, customeremail, payfortreturl);


        string outform = "<form method=\"post\" action=" + payfortactlink +
            " id=\"form1\" name=\"formpay\">" +
            "<input type=\"hidden\" name=\"command\" value='" + payfortcommand + "' id=\"command\" />" +
            "<input type=\"hidden\" name=\"access_code\" value='" + payfortaccesscode + "' id=\"access_code\" />" +
            "<input type=\"hidden\" name=\"merchant_identifier\" value='" + payfortmerchantidentifier + "' id=\"merchant_identifier\" />" +
            "<input type=\"hidden\" name=\"amount\" value='" + AmountMultiplied + "' id=\"amount\" />" +
            "<input type=\"hidden\" name=\"currency\" value='" + payfortcurrency + "' id=\"currency\" />" +
            "<input type=\"hidden\" name=\"language\" value='" + payfortlanguage + "' id=\"language\" />" +
            "<input type=\"hidden\" name=\"customer_email\" value='" + customeremail + "' id=\"customeremail\" />" +
            "<input type=\"hidden\" name=\"signature\" value='" + signature + "' id=\"signature\" />" +
            "<input type=\"hidden\" name=\"merchant_reference\" value='" + bookrefnum + "' id=\"merchant_reference\" />" +
            "<input type=\"hidden\" name=\"return_url\" value='" + payfortreturl + "' id=\"return_url\" />" +
            "<input type=\"submit\" value=\"PAY\" id=\"btnPayFort\" style='display:none;' /> </form>";

        return outform;
    }
}