﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Net.Mail;
using System.Net.Security;
using System.Text;
using System.IO;
using System.Net;


/// <summary>
/// Summary description for commonclass
/// </summary>
public class commonclass
{

    public  string SelectedCountryCode2 = "";
    public  string SelectedMarketCode2 = "";
    public  string SelectedSubMarketCode2 = "";
    public  string SelectedCityCode2 = "";





    SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connectiontravel"].ConnectionString);
    
    DataSet ds = new DataSet();
    string[] str = new string[100];
    string[] str2 = new string[100];
    string[] typ = new string[100];
    int incr = 0;


    public string getcountrycode()
    {
        
        return SelectedCountryCode2;
    }

    public void setcountrycode()
    {
        SelectedCountryCode2 = "abc";
    }


	public commonclass()
	{
		
	}

    public SqlConnection con1()
    {
         return con;
       
    }


    

    public string Encryptdata(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }


    public String SENDSMS(string SMSSupplier, string tempmsg, string tempnumbers, string agencycode, string campaigncode, Int32 SmsType)
    {
        string tempreturn = "NOT SEND";
        if (SMSSupplier == "SINTIV001")
        {
            tempreturn = SendSMSTivre(tempmsg, tempnumbers, agencycode, campaigncode, SmsType, "intl.tivre.com",SMSSupplier);
        }
        else if (SMSSupplier == "ONETIV001")
        {
            tempreturn = SendSMSTivre(tempmsg, tempnumbers, agencycode, campaigncode, SmsType, "oneviewit.tivre.com", SMSSupplier);
        }

        return tempreturn;

    }


    private String SendSMSTivre(string tempmsg, string tempnumbers,string agencycode,string campaigncode,Int32 SmsType,string smssupurl,string SMSSupplier)
    {
        string tempusername = "NONE", temppassword = "NONE", tempgsm = "NONE";
        string tempreturn = "NOT SEND";
        DataTable dtSMS = getDatasToReptrUsingSQL("select * from tblSMSSettings where AgencyCode = '" + agencycode+"' and SupplierCode = '"+SMSSupplier+"'").Tables[0];
        if (dtSMS.Rows.Count > 0)
        {
            tempusername = Decryptdata(dtSMS.Rows[0]["UserName"].ToString());
            temppassword = Decryptdata(dtSMS.Rows[0]["Password"].ToString());
             tempgsm = Decryptdata(dtSMS.Rows[0]["GSMNAME"].ToString());

             string tempurl = "";
             string temppara = tempurl = "userid=" + tempusername + "&password=" + temppassword + "&msg=" + tempmsg + "&mobnum=" + tempnumbers + "&frommobilenoGSM=" + tempgsm + "&msgId=" + campaigncode + "&qrytype=impalert&TivreId=1";

             if (SmsType == 1)
             {
                 tempurl = "http://"+smssupurl+"/httppush/send_smsSch.asp?" + temppara;

                 if (tempmsg.Length > 160)
                 {
                     tempurl = tempurl + "&param3=1";
                 }

             }
             else
             {
                 tempurl = "http://" + smssupurl + "/httppush/send_smsSch_unicode.asp?" + temppara + "&param4=1";
                  if (tempmsg.Length > 70)
                  {
                      tempurl = tempurl + "&param3=1";
                  }
             }




            StringBuilder sb = new StringBuilder();
            byte[] buf = new byte[8192];

            //do get request
            HttpWebRequest request = (HttpWebRequest)
                WebRequest.Create(tempurl);


            HttpWebResponse response = (HttpWebResponse)
                request.GetResponse();


            Stream resStream = response.GetResponseStream();

            string tempString = null;
            int count = 0;
            //read the data and print it
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    sb.Append(tempString);
                }
            }
            while (count > 0);
            tempreturn =  sb.ToString();
        }

        return tempreturn;
    } 

    public string Decryptdata(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public string GeneratePassword()
    {
        string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789@$ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string strPwd = "";
        Random rnd = new Random();
        for (int i = 0; i <= 8; i++)
        {
            int iRandom = rnd.Next(0, strPwdchar.Length - 1);
            strPwd += strPwdchar.Substring(iRandom, 1);
        }
        return strPwd;
    }
    



  
   
    public void InsertDataUsingProcedure(string proName)
    {

       
        try
        {
            con.Open();
            SqlCommand cmdidup;
            cmdidup = new SqlCommand();
            cmdidup.Parameters.Clear();

            cmdidup.CommandType = CommandType.StoredProcedure;
            cmdidup.CommandText = proName;

            for (int i = 0; i < incr; i++)
            {

                sparam(str[i], str2[i], typ[i], cmdidup);
            }
            incr = 0;
            cmdidup.Connection = con;
            cmdidup.ExecuteNonQuery();

            cmdidup.Connection.Close();
            con.Close();
            
        }
        catch (Exception e)
        {
            e.Message.ToString();
        }
    }



    public void AddDropDownValuesUsingProcedure(string proName, DropDownList drp, string ColoumnName, string ColoumnIdName)
    {
        try
        {
            SqlCommand cmdaddvsp;
            cmdaddvsp = new SqlCommand();
            cmdaddvsp.Parameters.Clear();
            ds.Clear();
            cmdaddvsp.Connection = con;
            cmdaddvsp.CommandType = CommandType.StoredProcedure;
            cmdaddvsp.CommandText = proName;
            for (int i = 0; i < incr; i++)
            {

                sparam(str[i], str2[i], typ[i], cmdaddvsp);
            }
            incr = 0;
            SqlDataAdapter adapter = new SqlDataAdapter(cmdaddvsp);
            adapter.Fill(ds);
            drp.DataSource = ds;
            drp.DataTextField = ColoumnName;
            drp.DataValueField = ColoumnIdName;
            drp.DataBind();
            drp.Items.Insert(0, new ListItem("Select"));

            cmdaddvsp.Connection.Close();
            con.Close();
        }
        catch (Exception)
        {
        }

    }



    public void AddDropDownValues(string tblName, string ColoumnName, string ColoumnIdName, DropDownList drp)
    {
        try
        {

            SqlCommand cmdaddv;
            cmdaddv = new SqlCommand("SELECT " + ColoumnName + "," + ColoumnIdName + " FROM " + tblName, con);
            ds.Clear();
            cmdaddv.Parameters.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter(cmdaddv);
            adapter.Fill(ds);
            drp.DataSource = ds;
            drp.DataTextField = ColoumnName;
            drp.DataValueField = ColoumnIdName;
            drp.DataBind();
            drp.Items.Insert(0, new ListItem("Select"));

            cmdaddv.Connection.Close();
            con.Close();
        }
        catch (Exception)
        {
        }

    }

    public void AddDropDownValues(string tblName, string ColoumnName,string ColoumnIdName,string Condition,DropDownList drp)
    {
        try
        {

            SqlCommand cmdaddv;
            cmdaddv = new SqlCommand("SELECT " + ColoumnName + "," + ColoumnIdName + " FROM " + tblName + " WHERE " + Condition, con);
            ds.Clear();
            cmdaddv.Parameters.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter(cmdaddv);
            adapter.Fill(ds);
            drp.DataSource = ds;
            drp.DataTextField = ColoumnName;
            drp.DataValueField = ColoumnIdName;
            drp.DataBind();
            drp.Items.Insert(0, new ListItem("Select"));

            cmdaddv.Connection.Close();
            con.Close();
        }
        catch (Exception)
        {
        }

    }


    public void AddDropDownValues(string tblName, string ColoumnName, string ColoumnIdName, HtmlSelect drp)
    {

        try
        {
            SqlCommand cmdaddv;
            cmdaddv = new SqlCommand("SELECT " + ColoumnName + "," + ColoumnIdName + " FROM " + tblName, con);
            ds.Clear();
            cmdaddv.Parameters.Clear();
            SqlDataAdapter adapter = new SqlDataAdapter(cmdaddv);
            adapter.Fill(ds);
            drp.DataSource = ds;
            drp.DataTextField = ColoumnName;
            drp.DataValueField = ColoumnIdName;
            drp.DataBind();
            drp.Items.Insert(0, new ListItem("Select"));

            cmdaddv.Connection.Close();
            con.Close();
        }
        catch (Exception)
        {
        }

    }

    public string StoreImg(FileUpload flImg, int count)
    {
        string pathName = "";
        if (flImg.HasFile)
        {

            string fileName = flImg.FileName.ToString();
            string uploadFolderPath = "~/pckgimgs/";
            string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
            pathName = count + ".jpg";
            flImg.SaveAs(filePath + pathName);


        }
        return pathName;
    }



    public void ClearAllText(Control con)
    {
        try
        {
            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).Text = string.Empty;
                else if (c is DropDownList)
                    ((DropDownList)c).SelectedIndex = 0;
                else if (c is CheckBox)
                    ((CheckBox)c).Checked = false;
              
                else
                    ClearAllText(c);

            }
        }
        catch (Exception)
        {

        }
    }


    public void DisableAllFields(Control con)
    {

        try
        {

            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                    ((TextBox)c).Enabled = false;
              
                else if (c is DropDownList)
                    ((DropDownList)c).Enabled = false;
                else
                     DisableAllFields(c);
            }
        }
        catch (Exception)
        {

        }

    }


    public void VisitAllFields(Control con)
    {

        try
        {

            foreach (Control c in con.Controls)
            {
                if (c is TextBox)
                {
                    //((TextBox)c).Focus();
                    ((TextBox)c).Text = "TAWASOL TRAVEL";
                    
                }
                    //((TextBox)c).Enabled = false;

                else if (c is DropDownList)
                {
                }
                //((DropDownList)c).Enabled = false;
                else
                {
                    VisitAllFields(c);
                }
            }
        }
        catch (Exception)
        {

        }

    }

    protected void sparam(string paramName, string paramValue, string type,SqlCommand cmds)
    {
        try
        {
           
            if (type.StartsWith("datetime"))
            {
                cmds.Parameters.Add(paramName, Convert.ToDateTime(paramValue));
            }
            else if (type.StartsWith("string"))
            {
                cmds.Parameters.Add(paramName, paramValue);
            }
            else if (type.StartsWith("int"))
            {
                cmds.Parameters.Add(paramName, Convert.ToInt32(paramValue));
            }
            else if (type.StartsWith("bigint"))
            {
                cmds.Parameters.Add(paramName, Convert.ToInt64(paramValue));
            }
            else if (type.StartsWith("money"))
            {
                cmds.Parameters.Add(paramName, Convert.ToDouble(paramValue));
            }

            else if (type.StartsWith("float"))
            {
                cmds.Parameters.Add(paramName, Convert.ToDouble(paramValue));
            }


        }
        catch(Exception) { }
    }


    public void RemoveDirectories(string strpath)
    {
        if (Directory.Exists(strpath))
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
            {
                File.Delete(file);
            }
            DirectoryInfo deleteDirectory = new DirectoryInfo(strpath);
            deleteDirectory.Delete();


        }
    }

    public void RemoveFile(string strpath)
    {

        if (File.Exists(strpath))
        {
            System.IO.File.Delete(strpath);
        }
    }




    public void addProValues(string promVarNames, string proValues, string type)
    {
        try
        {

            str[incr] = promVarNames;
            str2[incr] = proValues;
            typ[incr] = type;
            incr++;
        }
        catch (Exception)
        {

           

        }



    }

    public string insertIntoTable(string tableName)
    {
        string values = "";
        for (int i = 0; i < incr; i++)
        {
            if ((i + 1) == incr)
            {
                values = values + str[i];
            }
            else
            {
                values = values + str[i] + ",";

            }
        }

        string sql = "insert into " + tableName + " values(" + values + ")";
        ins(sql);
        return values;

    }

  
    public Int64 InsertWithReturn(string procName)
    {

        SqlCommand cmdins;
        Int64 IDDs = 0;
        try
        {

            con.Open();
            cmdins = new SqlCommand();

            cmdins.CommandType = CommandType.StoredProcedure;
            cmdins.CommandText = procName;

            SqlParameter ID = new SqlParameter("@ID", SqlDbType.BigInt);
            ID.Direction = ParameterDirection.Output;
            cmdins.Parameters.Add(ID);
            for (int i = 0; i < incr; i++)
            {

                sparam(str[i], str2[i], typ[i], cmdins);
            }
            incr = 0;
            cmdins.Connection = con;
            cmdins.ExecuteNonQuery();
            cmdins.Connection.Close();

            IDDs = Convert.ToInt64(cmdins.Parameters["@ID"].Value);


        }
        catch (Exception e)
        {
            e.Message.ToString();
        }


        con.Close();

        return IDDs;
    }


    public void ins(string sql)
    {
        SqlCommand cmdins;
        try
        {

            con.Open();

            cmdins = new SqlCommand(sql, con);
            for (int i = 0; i < incr; i++)
            {

                sparam(str[i], str2[i], typ[i],cmdins);
            }
            incr = 0;
            cmdins.Connection = con;
            cmdins.ExecuteNonQuery();
            cmdins.Connection.Close();
            
         
        }
        catch (Exception e)
        {
            e.Message.ToString();
        }
                
        con.Close();
    }


   


    public void UpdateTable(string tableName, string coloumnValues, string codition)
    {
        string values = "";
        string sql = "";
        try
        {

            string[] strng = coloumnValues.Split(','); 
            int cnt = 0;


            for (int i = 0; i < incr; i++)
            {
                if ((i + 1) == incr)
                {
                    values = values + strng[i] + "=" + str[i];
                }
                else
                {
                    values = values + strng[i] + "=" + str[i] + ",";

                }
            }
            //incr = 0;

            sql = "UPDATE " + tableName + " SET " + values + " WHERE " + codition;
            ins(sql);



        }
        catch (SqlException)
        {

        }
        finally
        {
            
        }
      

    }

    public void DeleteRow(string tableName, string codition)
    {
        string sql = "DELETE FROM " + tableName + " WHERE " + codition;
        ins(sql);

    }

    public void getDataTypes()
    {
        //string sql = "SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME=@TblName ";
        //con.Open();
        //cmd = new SqlCommand(sql, con);

        //cmd.Connection = con;
        //cmd.ExecuteNonQuery();
        //cmd.Connection.Close();
        //con.Close();

    }




 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    public string getData(string TableName, string coloumnName, string condition)
    {


        string data = "";
        SqlCommand cmdgd;                
        con.Open();
        cmdgd = new SqlCommand("SELECT " + coloumnName + " FROM " + TableName + " WHERE " + condition, con);
        SqlDataReader drgd = cmdgd.ExecuteReader();
        if (drgd.Read())
        {

            data = drgd[coloumnName].ToString();

        }
        cmdgd.Parameters.Clear();
        cmdgd.Connection.Close();
        con.Close();
        return data;

      
    }
    
    
    
    
    protected void setCondition(string paramName, string paramValue, string type,SqlCommand cmdsc)
    {
        try
        {

            if (type.StartsWith("datetime"))
            {
                cmdsc.Parameters.Add(paramName, Convert.ToDateTime(paramValue));
            }
            else if (type.StartsWith("string"))
            {
                cmdsc.Parameters.AddWithValue(paramName, paramValue);
            }
            else if (type.StartsWith("int"))
            {
                cmdsc.Parameters.AddWithValue(paramName, Convert.ToInt32(paramValue));
            }
            else if (type.StartsWith("bigint"))
            {
                cmdsc.Parameters.AddWithValue(paramName, Convert.ToInt64(paramValue));
            }
            else if (type.StartsWith("money"))
            {
                cmdsc.Parameters.Add(paramName, Convert.ToDouble(paramValue));
            }
            else if (type.StartsWith("float"))
            {
                cmdsc.Parameters.Add(paramName, Convert.ToDouble(paramValue));
            }
          

        }
        catch
        {

        }


    }
    public void getdsdata(string tempproc, DataSet tempds, string templanguage)
    {
        tempds.Clear();
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.Clear();
        cmd.Connection = con;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = tempproc;
        System.Data.SqlClient.SqlParameter uid = cmd.Parameters.Add("@Language", SqlDbType.NVarChar);
        uid.Direction = System.Data.ParameterDirection.Input;
        uid.Value = templanguage;
        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        adapter.Fill(tempds);

    }

    public DataSet getDatasToRepeaterUsingProcedure(string proName)
    {
        SqlCommand cmdgdtrup;
        cmdgdtrup = new SqlCommand();
        cmdgdtrup.Parameters.Clear();
        ds.Clear();

        cmdgdtrup.Connection = con;
        cmdgdtrup.CommandType = CommandType.StoredProcedure;
        cmdgdtrup.CommandText = proName;

        for (int i = 0; i < incr; i++)
        {
            setCondition(str[i], str2[i], typ[i], cmdgdtrup);
        }
        incr = 0;
        SqlDataAdapter adapter = new SqlDataAdapter(cmdgdtrup);
        adapter.Fill(ds);

        int val = ds.Tables[0].Rows.Count;

        int tempval = val;

        return ds;

    }


    public string SendONEVIEWEMAIL(string toList, string cclist, string bcclist, string body, string subject, string agencycode, int emailtype,Attachment fileattachment)
    {


         MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;

        DataTable dtEmailSettings = getDatasToRptr("tblEmailSettings", "*", "AgencyCode = '" + agencycode + "' and EmailTypeID = " + emailtype).Tables[0];
        if (dtEmailSettings.Rows.Count > 0)
        {
            string fromid = Decryptdata(dtEmailSettings.Rows[0]["EmailID"].ToString());
            string Password = Decryptdata(dtEmailSettings.Rows[0]["Password"].ToString());

            MailAddress fromAddress = new MailAddress(fromid);
            message.From = fromAddress;

            string[] emailTO = toList.Split(';');
            foreach (string word in emailTO)
            {
                message.To.Add(word);
            }
            if (cclist.Length > 5)
            {
                string[] EmailCC = cclist.Split(';');
                foreach (string word in EmailCC)
                {
                    message.CC.Add(word);
                }
            }

            if (bcclist.Length > 5)
            {
                string[] EmailBCC = bcclist.Split(';');
                foreach (string word in EmailBCC)
                {
                    message.Bcc.Add(word);
                }
            }
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Attachments.Add(fileattachment);
            message.Body = body;

            smtpClient.Host = Decryptdata(dtEmailSettings.Rows[0]["SmtpDetail"].ToString());
            smtpClient.Port = Convert.ToInt32(dtEmailSettings.Rows[0]["PortNumber"]);

            int tempssl = Convert.ToInt32(dtEmailSettings.Rows[0]["EnableSSL"]);

            if (tempssl == 1)
            {
                smtpClient.EnableSsl = true;
            }
            else
            {
                smtpClient.EnableSsl = false;

            }

            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(fromid, Password);

            smtpClient.Send(message);
          
        }
        msg = "Successful<BR>";
        return msg;
    }



    public string SendONEVIEWEMAIL(string toList,string cclist,string bcclist,string body, string subject,string agencycode,int emailtype)
    {

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;

      //  string AgencyName = getData("tblAgency", "AgencyName", "AgencyCode = ' " + agencycode + "'");
        //string CompanyEmailID = getData("tblCompany", "EmailID", "AgencyCode = ' " + agencycode + "'");

        DataTable dtEmailSettings = getDatasToRptr("tblEmailSettings", "*", "AgencyCode = '" + agencycode + "' and EmailTypeID = " + emailtype).Tables[0];
        if (dtEmailSettings.Rows.Count > 0)
        {
            string fromid =Decryptdata(dtEmailSettings.Rows[0]["EmailID"].ToString());
            string Password = Decryptdata(dtEmailSettings.Rows[0]["Password"].ToString());
 
            MailAddress fromAddress = new MailAddress(fromid);
            message.From = fromAddress;
         //   message.Sender = new MailAddress(CompanyEmailID, AgencyName);
            string[] emailTO = toList.Split(';');
            foreach (string word in emailTO)
            {
                message.To.Add(word);
            }
            if (cclist.Length > 5)
            {
                string[] EmailCC = cclist.Split(';');
                foreach (string word in EmailCC)
                {
                    message.CC.Add(word);
                }
            }

            if (bcclist.Length > 5)
            {
                string[] EmailBCC = bcclist.Split(';');
                foreach (string word in EmailBCC)
                {
                    message.Bcc.Add(word);
                }
            }
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            message.Priority = MailPriority.High;

            smtpClient.Host = Decryptdata(dtEmailSettings.Rows[0]["SmtpDetail"].ToString());
            smtpClient.Port = Convert.ToInt32(dtEmailSettings.Rows[0]["PortNumber"]) ;

            int tempssl = Convert.ToInt32(dtEmailSettings.Rows[0]["EnableSSL"]);

            if (tempssl == 1)
            {
                smtpClient.EnableSsl = true;
            }
            else
            {
                smtpClient.EnableSsl = false;

            }
           
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(fromid, Password);
          

            smtpClient.Send(message);
            msg = "Successful<BR>";


        }


        return msg;
       
            
           
         
        

    }
















    public string SendMailSubject(string toList, string body, string subject)
    {


        string from = "uaecrt@gmail.com";
        string Subject = subject;
        string ccList = toList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential("uaecrt@gmail.com", "ambalath");

            smtpClient.Send(message);
            msg = "Successful<BR>";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }

        return msg;
    }








    public  void sendExceptionMail(string tempmsg)
    {
   
        try
        {

            string from = "workflow@amadeus.ae";


            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();

            //string toList = "info@oneviewit.com,products@amadeus.ae";
            string toList = "fazil@oneviewit.com";
            string subject = "Error Found";
            string body = tempmsg;


            MailAddress fromAddress = new MailAddress(from);
            //MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["smtpUser"]);



            smtpClient.Host = "remote.amadeus.ae";
            //smtpClient.Host = ConfigurationManager.AppSettings["smtpServer"];


            smtpClient.Port = 25;
            //smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);



            string msg = string.Empty;


            message.From = fromAddress;
            message.To.Add(toList);

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;


            smtpClient.EnableSsl = false;
            //smtpClient.EnableSsl = true;

            smtpClient.UseDefaultCredentials = true;

            smtpClient.Credentials = new System.Net.NetworkCredential("workflow@amadeus.ae", "@m@deus14");
            //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtpUser"], ConfigurationManager.AppSettings["smtpPass"]);

            smtpClient.Send(message);
            msg = "Successful<BR>";





           
           

        }
        catch (Exception)
        {

        }







        //string from = "uaecrt@gmail.com";
        //string subject = "Details";
        //string toList = "ah.fazil@gmail.com";            
        //string ccList = toList;
        //string body = tempmsg;

        //MailMessage message = new MailMessage();
        //SmtpClient smtpClient = new SmtpClient();
        //string msg = string.Empty;
        //try
        //{
        //    //MailAddress fromAddress = new MailAddress(from);
        //    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["smtpUser"]);

        //    message.From = fromAddress;
        //    message.To.Add(toList);

        //    if (ccList != null && ccList != string.Empty)
        //        message.CC.Add(ccList);
        //    message.Subject = subject;
        //    message.IsBodyHtml = true;
        //    message.Body = body;


        //    //smtpClient.Host = "smtp.gmail.com";
        //    smtpClient.Host = ConfigurationManager.AppSettings["smtpServer"];

        //    //smtpClient.Port = 587;
        //    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);


        //    smtpClient.EnableSsl = true;
        //    smtpClient.UseDefaultCredentials = true;

        //    //smtpClient.Credentials = new System.Net.NetworkCredential("uaecrt@gmail.com", "ambalath");
        //    smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["smtpUser"], ConfigurationManager.AppSettings["smtpPass"]);

        //    smtpClient.Send(message);
        //    msg = "Successful<BR>";

        //}
        //catch (Exception ex)
        //{
        //    msg = ex.Message;
        //}

        
        
        


    }

    public SqlDataReader getDatasUsingProcedure(string proName)
    {
        con.Open();
        SqlCommand cmd3;
        cmd3 = new SqlCommand();
        cmd3.Parameters.Clear();
        cmd3.Connection = con;
        cmd3.CommandType = CommandType.StoredProcedure;
        cmd3.CommandText = proName;        
        for (int i = 0; i < incr; i++)
        {
            setCondition(str[i], str2[i], typ[i], cmd3);
        }
        incr = 0;
        SqlDataReader dr1 = cmd3.ExecuteReader();        
        //cmd.Connection.Close();
        //
        con.Close();
        return dr1;
      
        
        
        

    }
    public SqlDataReader getDatasUsingProcedure2(string proName)
    {
        SqlCommand cmdgdup2;
        con.Open();
        cmdgdup2 = new SqlCommand();
        cmdgdup2.Parameters.Clear();
        cmdgdup2.Connection = con;
        cmdgdup2.CommandType = CommandType.StoredProcedure;
        cmdgdup2.CommandText = proName;
        for (int i = 0; i < incr; i++)
        {
            setCondition(str[i], str2[i], typ[i], cmdgdup2);
        }
        incr = 0;
        SqlDataReader dr = cmdgdup2.ExecuteReader();
        cmdgdup2.Connection.Close();
        con.Close();
        return dr;
       



    }


    public DataSet getDatasToReptrUsingSQL(string sql)
    {
        ds.Clear();
        SqlCommand cmdgdtrus;
        try
        {

            con.Open();
            
            cmdgdtrus = new SqlCommand(sql, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmdgdtrus);
            cmdgdtrus.Connection.Close();
            adapter.Fill(ds);
        }
        catch (Exception)
        {
        }
     
     
   
        con.Close();
     
        return ds;





    }



    public DataSet getDatasToRptr(string TableName, string coloumnName, string condition)
    {
        SqlCommand cmdgdtr;
        try
        {
            ds.Clear();
            con.Open();
            
          

            if (condition.StartsWith("NIL"))
            {
                cmdgdtr = new SqlCommand("SELECT " + coloumnName + " FROM " + TableName, con);
            }
            else
            {
                cmdgdtr = new SqlCommand("SELECT " + coloumnName + " FROM " + TableName + " WHERE " + condition, con);

            }
            SqlDataAdapter adapter = new SqlDataAdapter(cmdgdtr);
            adapter.Fill(ds);
            cmdgdtr.Parameters.Clear();
            
        }
        catch (Exception)
        {
        }

        
       
        con.Close();
  
        
        return ds;




    }
    public SqlDataReader getDatasUsingSql(string sql)
    {
        SqlCommand cmdgdus;
        SqlDataReader dr;


     
            con.Open();
            
            cmdgdus = new SqlCommand(sql, con);
            dr = cmdgdus.ExecuteReader();
            cmdgdus.Connection.Close();
            cmdgdus.Parameters.Clear();
         
      

         con.Close();
         return dr;
       


    }
    public SqlDataReader getDatas(string TableName, string coloumnName, string condition)
    {
        SqlCommand cmdgd;
       
        

     con.Open();
          



            if (condition.StartsWith("NIL"))
            {

                cmdgd = new SqlCommand("SELECT " + coloumnName + " FROM " + TableName, con);

            }
            else
            {
                cmdgd = new SqlCommand("SELECT " + coloumnName + " FROM " + TableName + " WHERE " + condition, con);


            }



            SqlDataReader drgd = cmdgd.ExecuteReader();
            cmdgd.Parameters.Clear();
            cmdgd.Connection.Close();
   


        //cmd.Parameters.Clear();    
      
            con.Close();
            return drgd;
        
        
      // drgd.Close();


    }


    public void setRepeater(Repeater rpt, DataSet ds)
    {
        rpt.DataSource = ds;
        rpt.DataBind();
        ds.Clear();
        con.Close();

    }


    public Int64 getRowCount(string tblName,string ID)
    {
        Int64 id = 0;

   
        SqlCommand command = new SqlCommand("SELECT " +ID+ " FROM " + tblName, con);
        con.Open();
        SqlDataReader reader11 = command.ExecuteReader();     
        if (reader11.HasRows)
        {
            while (reader11.Read())
            {
                id = Convert.ToInt64(reader11[ID].ToString());
                

            }
        }

        command.Connection.Close();
        con.Close();
        return id;

    }



    public Int64 GetCount(string tblName, string ID)
    {
        Int64 id = 0;


        SqlCommand command = new SqlCommand("SELECT " + ID + " FROM " + tblName, con);
        con.Open();
        SqlDataReader reader11 = command.ExecuteReader();
        if (reader11.HasRows)
        {
            while (reader11.Read())
            {
                id++;


            }
        }

        command.Connection.Close();
        con.Close();
        return id;

    }

    public Int64 GetCount(string tblName, string ID, string condition)
    {
        Int64 id = 0;


        SqlCommand command = new SqlCommand("SELECT " + ID + " FROM " + tblName + " WHERE " + condition, con);
        con.Open();
        SqlDataReader reader11 = command.ExecuteReader();
        if (reader11.HasRows)
        {
            while (reader11.Read())
            {
                id++;


            }
        }

        command.Connection.Close();
        con.Close();
        return id;

    }






































   // ---------------------------------------------------------------------------------------





    public void AddDropDownValuesUsingProcedure(string proName, HtmlSelect drp, string ColoumnName, string ColoumnIdName)
    {
        SqlCommand cmdaddvsp;
        cmdaddvsp = new SqlCommand();
        cmdaddvsp.Parameters.Clear();
        ds.Clear();
        cmdaddvsp.Connection = con;
        cmdaddvsp.CommandType = CommandType.StoredProcedure;
        cmdaddvsp.CommandText = proName;
        for (int i = 0; i < incr; i++)
        {

            sparam(str[i], str2[i], typ[i], cmdaddvsp);
        }
        incr = 0;
        SqlDataAdapter adapter = new SqlDataAdapter(cmdaddvsp);
        adapter.Fill(ds);
        drp.DataSource = ds;
        drp.DataTextField = ColoumnName;
        drp.DataValueField = ColoumnIdName;
        drp.DataBind();
        drp.Items.Insert(0, new ListItem("Select"));

        cmdaddvsp.Connection.Close();
        con.Close();


    }


    public string getEXT(FileUpload flupld)
    {
        string pathName = "";
        string ext1 = "";
        if (flupld.HasFile)
        {

            string fileName = flupld.FileName.ToString();
            ext1 = System.IO.Path.GetExtension(flupld.PostedFile.FileName);


            //string uploadFolderPath = "~/"+folderPath+"/";
            //string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
            //pathName = fname + ext1;
           // flupld.SaveAs(filePath + pathName);



        }
        return ext1;
    }



    public void FileUpload(int fname, FileUpload flupld, string folderPath)
    {
        string pathName = "";
        string ext1 = "";
        if (flupld.HasFile)
        {

            string fileName = flupld.FileName.ToString();
            ext1 = System.IO.Path.GetExtension(flupld.PostedFile.FileName);


            string uploadFolderPath = "~/" + folderPath + "/";
            string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
            pathName = fname + ext1;
            flupld.SaveAs(filePath + pathName);



        }
     //   return ext1;
    }



    public int EmailValidation(string tblName,string mailid)
    {
        int flg = 0;


        try
        {

            DataSet ds = new DataSet();           
            SqlCommand cmdev;
            con.Open();



            cmdev = new SqlCommand("SELECT * FROM " + tblName, con);
            SqlDataReader reader = cmdev.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    if (reader["EmailID"].ToString() == mailid)
                    {
                        flg = 1;

                    }
                }
            }


        }
        catch (Exception)
        {
        }





        return flg;
    }

    public void showMessage(string msg,HttpContext c)
    {

        c.Response.Write("<script>alert('"+msg+"');</script>");
    }

    public void Authetication(HttpContext context)
    {

        HttpCookie OneviewCookies = context.Request.Cookies["oneviewadmincaseinfo"];
        if (OneviewCookies == null)
        {

            context.Response.Redirect("Default.aspx");

        }

    }




}

public class ConfigSettings
{
    public static string GetAppSettingsValue(string key)
    {
        string retval = string.Empty;
        retval = ConfigurationManager.AppSettings[key];
        return retval;
    }
}