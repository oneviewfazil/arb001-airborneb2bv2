﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net.Mail;
using SelectPdf;
using System.Configuration;

/// <summary>
/// Summary description for BALSettings
/// </summary>
public class BALSettings1
{


    public BALSettings1()
    {
        //
        // TODO: Add constructor logic here
        //
    }



















    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject, string Attachment)
    {
        //DataTable DtEmailSettings = GetEmailSettings(7);
        // string virtualFilePath = path.Replace(HttpContext.Current.Server.MapPath("~/"), "~/").Replace(@"\", "/");
        string from = "servicenotifymail@gmail.com";
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);
            if (Attachment != null && Attachment != string.Empty)
            {
                //To PDF
                if (Attachment.Contains(".html"))
                {
                    try
                    {
                        HtmlToPdf converter = new HtmlToPdf();
                        converter.Options.WebPageWidth = 800;
                        string BookingReference = "";
                        if (Attachment.Contains("ticket_attachment_"))
                        {
                            BookingReference = "Your_Ticket_" + Attachment.Split('/')[3].ToString();
                        }
                        else
                        {
                            BookingReference = "Booking_Voucher";
                        }

                        PdfDocument doc = converter.ConvertUrl(HttpContext.Current.Server.MapPath(Attachment));
                        MemoryStream pdfStream = new MemoryStream();
                        doc.Save(pdfStream);
                        pdfStream.Position = 0;
                        message.Attachments.Add(new Attachment(pdfStream, BookingReference + ".pdf"));
                    }
                    catch (Exception)
                    {
                        message.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath(Attachment)));
                    }
                }
                else
                {
                    message.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath(Attachment)));
                }
            }
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential("servicenotifymail@gmail.com", "ambalath");
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }













}
