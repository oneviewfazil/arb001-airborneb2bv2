﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Data;
using Ionic.Zlib;
using System.Configuration;

/// <summary>
/// Summary description for SightseeingWebRequest
/// </summary>
public class SightseeingWebRequest
{
    String ClientID = ConfigurationManager.AppSettings["ActGTAClientID"];
    String EmailAddress = ConfigurationManager.AppSettings["ActGTAEmailAddress"];
    String ClientPassword = ConfigurationManager.AppSettings["ActGTAClientPassword"];
    String InterfaceURL = ConfigurationManager.AppSettings["ActGTAInterfaceURL"];
    //String ResponseURL = "https://10.100.21.131/receiveRequest.asp";
    String Language = ConfigurationManager.AppSettings["ActGTALanguage"];
    String Currency = ConfigurationManager.AppSettings["ActGTACurrency"];
    public string CreateXmlforSightseeingPriceRequest(string Country, string City, string Date, string Travellers)
    {
        int Adult = Convert.ToInt16(Travellers.Split('-')[0]);
        int Child = 0;
        if (Travellers.Length > 3)
        {
            Child = Convert.ToInt16(Travellers.Split('-')[1]);
        }
        List<int> TravellerChildAge = new List<int>();
        if (Child > 0)
        {
            TravellerChildAge = GetAllTravellersWithAge(Travellers);
        }


        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;
        XmlElement XMLFIRSTINNERCHILDNODE;


        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");

        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);

        //RequestDetails Node Start
        XMLBody = XmlDoc.CreateElement("RequestDetails");
        //    Node SearchSightseeingPriceRequest Start
        //      Node ItemDestination Start           
        XMLNODE = XmlDoc.CreateElement("SearchSightseeingPriceRequest");
        XMLCHILDNODE = XmlDoc.CreateElement("ItemDestination");
        XMLCHILDNODE.SetAttribute("DestinationType", "city");
        XMLCHILDNODE.SetAttribute("DestinationCode", City);
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node ItemDestination End
        //      Node ImmediateConfirmationOnly Start
        // XMLCHILDNODE = XmlDoc.CreateElement("ImmediateConfirmationOnly");
        //XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node ImmediateConfirmationOnly End
        XMLCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLCHILDNODE.InnerText = Date;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node NumberOfAdults Start
        XMLCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLCHILDNODE.InnerText = Adult.ToString();
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTINNERCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
            }
            XMLNODE.AppendChild(XMLCHILDNODE);
        }
        //      Node Children End
        //   Include Charge Condtions start
        XMLCHILDNODE = XmlDoc.CreateElement("IncludeChargeConditions");
        XMLNODE.AppendChild(XMLCHILDNODE);
        XMLBody.AppendChild(XMLNODE);
        //RequestDetails Node End
        XMLRequest.AppendChild(XMLBody);
        //Nodes Request End
        XmlDoc.AppendChild(XMLRequest);

        string Result = SendRequestForSightSeeing(XmlDoc, "Sighseeing-Request", "", "", "");
        return Result;
    }
    public string SendRequestForSightSeeing(XmlDocument XMLDoc, string SoapAction, string ItemCode, string City, string TourIndex)
    {
        string strResult = "NORESULT";
        try
        {
            City = City.ToUpper();
            //XML Request Start
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            Byte[] byte1 = encoding.GetBytes(XMLDoc.OuterXml);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebRequest HttpWReq = WebRequest.Create(InterfaceURL);
            HttpWReq.ContentType = "text/xml";
            HttpWReq.Headers["Accept-Encoding"] = "gzip";
            HttpWReq.ContentLength = XMLDoc.OuterXml.Length;
            HttpWReq.Method = "POST";
            Stream StreamData = HttpWReq.GetRequestStream();
            StreamData.Write(byte1, 0, byte1.Length);
            //XML Request End
            // XML Response Start
            WebResponse HttpWRes = HttpWReq.GetResponse();
            Stream receiveStream = HttpWRes.GetResponseStream();
            string Compression = HttpWRes.Headers.Get("Content-Encoding");
            if (Compression == "gzip")
            {
                receiveStream = new GZipStream(receiveStream, CompressionMode.Decompress);

            }
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();
            string strHeaders = HttpWRes.Headers.ToString();
            // XML Response End
            CreateXMLDoc(XMLDoc, strHeaders, strResult, SoapAction);
            if (SoapAction == "SightseeingItemInformation" || SoapAction == "Cancellation-Policy")
            {
                CreateXMLDocForData(XMLDoc, strHeaders, strResult, SoapAction, ItemCode, City, TourIndex);
            }
        }
        catch
        {

        }
        return strResult;

    }
    public void CreateXMLDocForData(XmlDocument XMLDATA, string XMLHEADER, string RESULT, string FileType, string ItemCode, string City, string TourIndex)
    {
        try
        {
            string UserID = HttpContext.Current.Session["UserID"].ToString();
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/SIGHSEEING-XML-DOCUMENTS/GTA/" + UserID + "/" + FileType + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string filename = City + ItemCode + ".xml";
            if (FileType == "Cancellation-Policy")
            {
                filename = City + ItemCode + "-" + TourIndex + ".xml";
            }
            string tempfilename = storePath + "/" + filename;
            XmlDocument XMLResult = new XmlDocument();
            XMLResult.LoadXml(RESULT);
            XMLResult.Save(tempfilename);
        }
        catch (XmlException xmlEx)
        {

        }
        finally
        {

        }
    }
    public void CreateXMLDoc(XmlDocument XMLDATA, string XMLHEADER, string RESULT, string FileType)
    {
        try
        {
            string finalsesID = Guid.NewGuid().ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/XmlFiles/" + DateTime.Now.ToString("yyyyMMdd") + "/" + "GTA" + "/" + "Sighseeing" + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string TimeStamp = DateTime.UtcNow.AddHours(4).ToString("HH-mm-ss");
            string filename = FileType + "-Request-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilename = storePath + "/" + filename;
            string filenameHeader = FileType + "-Header-" + finalsesID + "-" + TimeStamp + ".txt";
            string tempfilenameHeader = storePath + "/" + filenameHeader;
            string filenameResult = FileType + "-Response-" + finalsesID + "-" + TimeStamp + ".xml";
            string tempfilenameResult = storePath + "/" + filenameResult;

            XMLDATA.Save(tempfilename);
            File.WriteAllText(tempfilenameHeader, XMLHEADER);
            XmlDocument XMLResult = new XmlDocument();
            XMLResult.LoadXml(RESULT);
            XMLResult.Save(tempfilenameResult);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {

        }
    }
    public List<int> GetAllTravellersWithAge(string Travellerlist)
    {
        int Adults = Convert.ToInt16(Travellerlist.Split('-')[0]);
        int Child = Convert.ToInt16(Travellerlist.Split('-')[1]);
        List<int> Travellers = new List<int>();
        if (Convert.ToInt16(Child) > 0)
        {
            for (int i = 1; i <= Convert.ToInt16(Child); i++)
            {
                Travellers.Add(Convert.ToInt16(Travellerlist.Split('-')[i + 1]));
            }
        }
        return Travellers;
    }
    public SightseeingWebRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //Create XML for ItemInformationRequest
    public string CreateXMLforItemInformation(string ItemCode, string City, string Country)
    {
        string Results = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");

        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);

        //RequestDetails Node Start
        XMLBody = XmlDoc.CreateElement("RequestDetails");
        //  Node SearchItemInformationRequest
        XMLNODE = XmlDoc.CreateElement("SearchItemInformationRequest");
        XMLNODE.SetAttribute("ItemType", "sightseeing");
        //    Node ItemDestination
        XMLCHILDNODE = XmlDoc.CreateElement("ItemDestination");
        XMLCHILDNODE.SetAttribute("DestinationType", "city");
        XMLCHILDNODE.SetAttribute("DestinationCode", City);
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ItemDestination End
        //    Node ItemCode start
        XMLCHILDNODE = XmlDoc.CreateElement("ItemCode");
        XMLCHILDNODE.InnerText = ItemCode;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ItemCode End
        XMLBody.AppendChild(XMLNODE);
        //  Node SearchItemInformationRequest End
        XMLRequest.AppendChild(XMLBody);
        //RequestDetails Node End
        XmlDoc.AppendChild(XMLRequest);

        Results = SendRequestForSightSeeing(XmlDoc, "SightseeingItemInformation", ItemCode, City, "");

        return Results;
    }
    //Create XML for SearchChargeConditionsRequest
    public string CreteXMLforCancellationPolicy(string ItemCode, string City, string TourDate, string SpeacilCode, string LanguageCode, string LanguageListCode, string Country, string Travellers, string TourIndex)
    {
        int Adult = Convert.ToInt16(Travellers.Split('-')[0]);
        int Child = 0;
        if (Travellers.Length > 3)
        {
            Child = Convert.ToInt16(Travellers.Split('-')[1]);
        }
        List<int> TravellerChildAge = new List<int>();
        if (Child > 0)
        {
            TravellerChildAge = GetAllTravellersWithAge(Travellers);
        }


        string Results = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;
        XmlElement XMLFIRSTCHILDNODE;
        XmlElement XMLFIRSTINNERCHILDNODE;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");

        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);
        //RequestDetails Node Start
        XMLBody = XmlDoc.CreateElement("RequestDetails");
        //  Node SearchChargeConditionsRequest
        XMLNODE = XmlDoc.CreateElement("SearchChargeConditionsRequest");
        //    Node ChargeConditionsSightseeing
        XMLCHILDNODE = XmlDoc.CreateElement("ChargeConditionsSightseeing");
        //      Node City start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("City");
        XMLFIRSTCHILDNODE.InnerText = City;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node City end
        //      Node ItemCode start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Item");
        XMLFIRSTCHILDNODE.InnerText = ItemCode;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node ItemCode End
        //      Node TourDate start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLFIRSTCHILDNODE.InnerText = TourDate;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node TourDate End

        //  Policy Node Start
        if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourLanguageList");
            XMLFIRSTCHILDNODE.InnerText = LanguageListCode;
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            //      Node TourLanguage End
        }
        if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLFIRSTCHILDNODE.InnerText = LanguageCode;
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            //      Node TourLanguage End
        }
        if (SpeacilCode != "")
        {
            if (SpeacilCode != "0")
            {
                //      Node SpecialCode start
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("SpecialCode");
                XMLFIRSTCHILDNODE.InnerText = SpeacilCode;
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
                //      Node SpecialCode End
            }
        }

        //  Policy Node End

        //      Node NumberOfAdults Start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLFIRSTCHILDNODE.InnerText = Adult.ToString();
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTINNERCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
            }
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        }
        //      Node Children End
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ChargeConditionsSightseeing End
        XMLBody.AppendChild(XMLNODE);
        //  Node SearchChargeConditionsRequest End


        //  Node SightseeingPriceBreakdownRequest Start
        XMLNODE = XmlDoc.CreateElement("SightseeingPriceBreakdownRequest");
        //    Node City Start
        XMLCHILDNODE = XmlDoc.CreateElement("City");
        XMLCHILDNODE.InnerText = City;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node City End
        //    Node Item Start
        XMLCHILDNODE = XmlDoc.CreateElement("Item");
        XMLCHILDNODE.InnerText = ItemCode;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node Item End
        //    Node TourDate Start
        XMLCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLCHILDNODE.InnerText = TourDate;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node TourDate End
        //  Policy Node Start      
        if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("Code", LanguageCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("LanguageListCode", LanguageListCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        if (SpeacilCode != "")
        {
            if (SpeacilCode != "0")
            {
                //      Node SpecialCode start
                XMLCHILDNODE = XmlDoc.CreateElement("SpecialCode");
                XMLCHILDNODE.InnerText = SpeacilCode;
                XMLNODE.AppendChild(XMLCHILDNODE);
                //      Node SpecialCode End
            }
        }

        //  Policy Node End
        //      Node NumberOfAdults Start
        XMLCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLCHILDNODE.InnerText = Adult.ToString();
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            }
            XMLNODE.AppendChild(XMLCHILDNODE);
        }
        //      Node Children End
        XMLBody.AppendChild(XMLNODE);
        //  Node SightseeingPriceBreakdownRequest End


        XMLRequest.AppendChild(XMLBody);
        //RequestDetails Node End
        XmlDoc.AppendChild(XMLRequest);
        Results = SendRequestForSightSeeing(XmlDoc, "Cancellation-Policy", ItemCode, City, TourIndex);
        return Results;
    }
    public string CreteXMLforPriceBreakUp(string ItemCode, string City, string TourDate, string SpeacilCode, string LanguageCode, string LanguageListCode, string Country, string Travellers)
    {
        int Adult = Convert.ToInt16(Travellers.Split('-')[0]);
        int Child = 0;
        if (Travellers.Length > 3)
        {
            Child = Convert.ToInt16(Travellers.Split('-')[1]);
        }
        List<int> TravellerChildAge = new List<int>();
        if (Child > 0)
        {
            TravellerChildAge = GetAllTravellersWithAge(Travellers);
        }


        string Results = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;
        XmlElement XMLFIRSTCHILDNODE;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");

        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);
        //RequestDetails Node Start
        XMLBody = XmlDoc.CreateElement("RequestDetails");

        //  Node SightseeingPriceBreakdownRequest Start
        XMLNODE = XmlDoc.CreateElement("SightseeingPriceBreakdownRequest");
        //    Node City Start
        XMLCHILDNODE = XmlDoc.CreateElement("City");
        XMLCHILDNODE.InnerText = City;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node City End
        //    Node Item Start
        XMLCHILDNODE = XmlDoc.CreateElement("Item");
        XMLCHILDNODE.InnerText = ItemCode;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node Item End
        //    Node TourDate Start
        XMLCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLCHILDNODE.InnerText = TourDate;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node TourDate End
        //  Policy Node Start
        if (SpeacilCode != "")
        {
            if (SpeacilCode != "0")
            {
                //      Node SpecialCode start
                XMLCHILDNODE = XmlDoc.CreateElement("SpecialCode");
                XMLCHILDNODE.InnerText = SpeacilCode;
                XMLNODE.AppendChild(XMLCHILDNODE);
                //      Node SpecialCode End
            }
        }
        else if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("Code", LanguageCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        else if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("LanguageListCode", LanguageListCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        //  Policy Node End
        //      Node NumberOfAdults Start
        XMLCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLCHILDNODE.InnerText = Adult.ToString();
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            }
            XMLNODE.AppendChild(XMLCHILDNODE);
        }
        //      Node Children End
        XMLBody.AppendChild(XMLNODE);
        //  Node SightseeingPriceBreakdownRequest End


        XMLRequest.AppendChild(XMLBody);
        //RequestDetails Node End
        XmlDoc.AppendChild(XMLRequest);
        Results = SendRequestForSightSeeing(XmlDoc, "PriceBreakUp", "", "", "");
        return Results;
    }
    public string GetItemInformationwithCancellationPolicy(string ItemCode, string City, string TourDate, string SpeacilCode, string LanguageCode, string LanguageListCode, string Country, string Travellers)
    {
        int Adult = Convert.ToInt16(Travellers.Split('-')[0]);
        int Child = 0;
        if (Travellers.Length > 3)
        {
            Child = Convert.ToInt16(Travellers.Split('-')[1]);
        }
        List<int> TravellerChildAge = new List<int>();
        if (Child > 0)
        {
            TravellerChildAge = GetAllTravellersWithAge(Travellers);
        }

        string Results = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;
        XmlElement XMLFIRSTCHILDNODE;
        XmlElement XMLFIRSTINNERCHILDNODE;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");

        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);

        //RequestDetails Node Start
        XMLBody = XmlDoc.CreateElement("RequestDetails");
        //  Node SearchItemInformationRequest
        XMLNODE = XmlDoc.CreateElement("SearchItemInformationRequest");
        XMLNODE.SetAttribute("ItemType", "sightseeing");
        //    Node ItemDestination
        XMLCHILDNODE = XmlDoc.CreateElement("ItemDestination");
        XMLCHILDNODE.SetAttribute("DestinationType", "city");
        XMLCHILDNODE.SetAttribute("DestinationCode", City);
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ItemDestination End
        //    Node ItemCode start
        XMLCHILDNODE = XmlDoc.CreateElement("ItemCode");
        XMLCHILDNODE.InnerText = ItemCode;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ItemCode End
        XMLBody.AppendChild(XMLNODE);
        //  Node SearchItemInformationRequest End

        //  Node SearchChargeConditionsRequest
        XMLNODE = XmlDoc.CreateElement("SearchChargeConditionsRequest");
        //    Node ChargeConditionsSightseeing
        XMLCHILDNODE = XmlDoc.CreateElement("ChargeConditionsSightseeing");
        //      Node City start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("City");
        XMLFIRSTCHILDNODE.InnerText = City;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node City end
        //      Node ItemCode start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Item");
        XMLFIRSTCHILDNODE.InnerText = ItemCode;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node ItemCode End
        //      Node TourDate start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLFIRSTCHILDNODE.InnerText = TourDate;
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node TourDate End

        //  Policy Node Start

        if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLFIRSTCHILDNODE.InnerText = LanguageCode;
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            //      Node TourLanguage End
        }
        if (SpeacilCode != "")
        {
            if (SpeacilCode != "0")
            {
                //      Node SpecialCode start
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("SpecialCode");
                XMLFIRSTCHILDNODE.InnerText = SpeacilCode;
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
                //      Node SpecialCode End
            }
        }
        if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("TourLanguageList");
            XMLFIRSTCHILDNODE.InnerText = LanguageListCode;
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            //      Node TourLanguage End
        }
        //  Policy Node End

        //      Node NumberOfAdults Start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLFIRSTCHILDNODE.InnerText = Adult.ToString();
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTINNERCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
            }
            XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        }
        //      Node Children End
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node ChargeConditionsSightseeing End
        XMLBody.AppendChild(XMLNODE);
        //  Node SearchChargeConditionsRequest End


        //  Node SightseeingPriceBreakdownRequest Start
        XMLNODE = XmlDoc.CreateElement("SightseeingPriceBreakdownRequest");
        //    Node City Start
        XMLCHILDNODE = XmlDoc.CreateElement("City");
        XMLCHILDNODE.InnerText = City;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node City End
        //    Node Item Start
        XMLCHILDNODE = XmlDoc.CreateElement("Item");
        XMLCHILDNODE.InnerText = ItemCode;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node Item End
        //    Node TourDate Start
        XMLCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLCHILDNODE.InnerText = TourDate;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //    Node TourDate End
        //  Policy Node Start      
        if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("Code", LanguageCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        if (SpeacilCode != "")
        {
            if (SpeacilCode != "0")
            {
                //      Node SpecialCode start
                XMLCHILDNODE = XmlDoc.CreateElement("SpecialCode");
                XMLCHILDNODE.InnerText = SpeacilCode;
                XMLNODE.AppendChild(XMLCHILDNODE);
                //      Node SpecialCode End
            }
        }
        if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            XMLCHILDNODE.SetAttribute("LanguageListCode", LanguageListCode);
            XMLNODE.AppendChild(XMLCHILDNODE);
            //      Node TourLanguage End
        }
        //  Policy Node End
        //      Node NumberOfAdults Start
        XMLCHILDNODE = XmlDoc.CreateElement("NumberOfAdults");
        XMLCHILDNODE.InnerText = Adult.ToString();
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLCHILDNODE = XmlDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("Age");
                XMLFIRSTCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
            }
            XMLNODE.AppendChild(XMLCHILDNODE);
        }
        //      Node Children End
        XMLBody.AppendChild(XMLNODE);
        //  Node SightseeingPriceBreakdownRequest End

        XMLRequest.AppendChild(XMLBody);
        //RequestDetails Node End
        XmlDoc.AppendChild(XMLRequest);
        Results = SendRequestForSightSeeing(XmlDoc, "Sightseeing-single-Information", ItemCode, City, "");
        return Results;
    }
    public string CreateXMLforBookingRequest(string ReferenceID, string BookingDate, string Gest, string AdultPaxDetails, string ChildPaxDetails, string CityCode, string ActCode, string TourDate, string languagecode, string languagelistcode, string specialcode, string DepartureTime, string DepartureCode, string CountryCode)
    {
        string Results = "";
        DataTable DtChildPaxes = new DataTable();
        DataTable DtAdultPaxes = new DataTable();
        int TotAdult = Convert.ToInt16(Gest.Split('-')[0]);
        int TotChild = 0;
        if (Gest.Length > 3)
        {
            TotChild = Convert.ToInt16(Gest.Split('-')[1]);
        }
        if (TotChild > 0)
        {
            DtChildPaxes = SetChildPaxes(Gest, ChildPaxDetails);
        }
        if (TotAdult > 0)
        {
            DtAdultPaxes = SetAdultPaxes(TotAdult.ToString(), AdultPaxDetails);
        }


        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;
        XmlElement XMLFIRSTCHILDNODE;
        XmlElement XMLFIRSTINNERCHILDNODE;
        XmlElement XMLSECONDINNERCHILDNODE;
        XmlElement XMLTHIRDINNERCHILDNODE;
        XmlElement XMLPAXIDNODES;
        XmlElement XMLPAXIDNODECHILD;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");
        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", CountryCode);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);
        //RequestDetails Node Start

        XMLBody = XmlDoc.CreateElement("RequestDetails");
        //    AddBookingRequest Node Start
        XMLNODE = XmlDoc.CreateElement("AddBookingRequest");
        XMLNODE.SetAttribute("Currency", Currency);
        //      BookingReference strat
        XMLCHILDNODE = XmlDoc.CreateElement("BookingReference");
        XMLCHILDNODE.InnerText = ReferenceID.ToString();
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      BookingReference End
        //      BookingDepartureDate strat
        XMLCHILDNODE = XmlDoc.CreateElement("BookingDepartureDate");
        XMLCHILDNODE.InnerText = BookingDate;
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      BookingDepartureDate End
        //      PaxNames strat
        XMLCHILDNODE = XmlDoc.CreateElement("PaxNames");
        //          PaxName start
        int k = 1;
        XMLPAXIDNODES = XmlDoc.CreateElement("PaxIds");

        if (DtAdultPaxes.Rows.Count > 0)
        {
            for (int i = 0; i < DtAdultPaxes.Rows.Count; i++)
            {
                XMLFIRSTCHILDNODE = XmlDoc.CreateElement("PaxName");
                XMLFIRSTCHILDNODE.SetAttribute("PaxId", k.ToString());
                XMLFIRSTCHILDNODE.InnerText = DtAdultPaxes.Rows[i]["PaxName"].ToString();
                XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);

                XMLPAXIDNODECHILD = XmlDoc.CreateElement("PaxId");
                XMLPAXIDNODECHILD.InnerText = k.ToString();
                XMLPAXIDNODES.AppendChild(XMLPAXIDNODECHILD);
                ++k;
            }
        }
        if (TotChild > 0)
        {
            if (DtChildPaxes.Rows.Count > 0)
            {
                for (int i = 0; i < DtChildPaxes.Rows.Count; i++)
                {
                    XMLFIRSTCHILDNODE = XmlDoc.CreateElement("PaxName");
                    XMLFIRSTCHILDNODE.SetAttribute("PaxId", k.ToString());
                    XMLFIRSTCHILDNODE.SetAttribute("PaxType", "child");
                    XMLFIRSTCHILDNODE.SetAttribute("ChildAge", DtChildPaxes.Rows[i]["Age"].ToString());
                    XMLFIRSTCHILDNODE.InnerText = DtChildPaxes.Rows[i]["PaxName"].ToString();
                    XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);

                    XMLPAXIDNODECHILD = XmlDoc.CreateElement("PaxId");
                    XMLPAXIDNODECHILD.InnerText = k.ToString();
                    XMLPAXIDNODES.AppendChild(XMLPAXIDNODECHILD);
                    ++k;
                }
            }
        }
        //          PaxName End  
        XMLNODE.AppendChild(XMLCHILDNODE);
        //      PaxNames End

        //      Booking Items Start
        XMLCHILDNODE = XmlDoc.CreateElement("BookingItems");
        //          BookingItem start
        XMLFIRSTCHILDNODE = XmlDoc.CreateElement("BookingItem");
        XMLFIRSTCHILDNODE.SetAttribute("ItemType", "sightseeing");
        //              ItemReference start
        XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("ItemReference");
        XMLFIRSTINNERCHILDNODE.InnerText = "1";
        XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
        //              ItemReference End
        //              Item City Start
        XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("ItemCity");
        XMLFIRSTINNERCHILDNODE.SetAttribute("Code", CityCode);
        XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
        //              Item City End
        //              Item Code Start
        XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("Item");
        XMLFIRSTINNERCHILDNODE.SetAttribute("Code", ActCode);
        XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
        //              Item Code End
        XMLFIRSTINNERCHILDNODE = XmlDoc.CreateElement("SightseeingItem");
        // Inner Nodes
        //       Tour Date start 
        XMLSECONDINNERCHILDNODE = XmlDoc.CreateElement("TourDate");
        XMLSECONDINNERCHILDNODE.InnerText = TourDate;
        XMLFIRSTINNERCHILDNODE.AppendChild(XMLSECONDINNERCHILDNODE);
        //       Tour Date end
        // Tour Language Code start
        if (languagecode != "" || languagelistcode != "")
        {
            XMLSECONDINNERCHILDNODE = XmlDoc.CreateElement("TourLanguage");
            if (languagecode != "")
            {
                XMLSECONDINNERCHILDNODE.SetAttribute("Code", languagecode);
            }
            if (languagelistcode != "")
            {
                XMLSECONDINNERCHILDNODE.SetAttribute("LanguageListCode", languagelistcode);
            }
            XMLFIRSTINNERCHILDNODE.AppendChild(XMLSECONDINNERCHILDNODE);
        }
        XMLFIRSTINNERCHILDNODE.AppendChild(XMLSECONDINNERCHILDNODE);
        //Special Code
        if ((specialcode != "") && (specialcode != "0"))
        {
            XMLSECONDINNERCHILDNODE = XmlDoc.CreateElement("SpecialItem");
            XMLSECONDINNERCHILDNODE.SetAttribute("Code", specialcode);
            XMLFIRSTINNERCHILDNODE.AppendChild(XMLSECONDINNERCHILDNODE);
        }
        //Pax Idies Start
        XMLFIRSTINNERCHILDNODE.AppendChild(XMLPAXIDNODES);
        //Pax Idies End
        //TourDeparture Start
        if (DepartureTime != "")
        {
            //Time start
            XMLSECONDINNERCHILDNODE = XmlDoc.CreateElement("TourDeparture");
            XMLTHIRDINNERCHILDNODE = XmlDoc.CreateElement("Time");
            XMLTHIRDINNERCHILDNODE.InnerText = DepartureTime;
            XMLSECONDINNERCHILDNODE.AppendChild(XMLTHIRDINNERCHILDNODE);
            //Time end
        }
        if (DepartureCode != "")
        {
            //DeparturePoint start
            XMLTHIRDINNERCHILDNODE = XmlDoc.CreateElement("DeparturePoint");
            XMLTHIRDINNERCHILDNODE.SetAttribute("Code", DepartureCode);
            XMLSECONDINNERCHILDNODE.AppendChild(XMLTHIRDINNERCHILDNODE);
            XMLFIRSTINNERCHILDNODE.AppendChild(XMLSECONDINNERCHILDNODE);
            //DeparturePoint end       
        }
        //TourDeparture End     
        XMLFIRSTCHILDNODE.AppendChild(XMLFIRSTINNERCHILDNODE);
        XMLCHILDNODE.AppendChild(XMLFIRSTCHILDNODE);
        XMLNODE.AppendChild(XMLCHILDNODE);
        XMLBody.AppendChild(XMLNODE);
        XMLRequest.AppendChild(XMLBody);
        XmlDoc.AppendChild(XMLRequest);
        //          BookingItem start
        //      Booking Items Start
        Results = SendRequestForSightSeeing(XmlDoc, "Sightseeing-booking", ActCode, CityCode, "");
        return Results;
    }
    public Int64 Random()
    {
        Random generator = new Random();
        Int64 r = generator.Next(100000, 1000000);
        return r;
    }
    public DataTable SetChildPaxes(string Travellers, string ChildPaxDetails)
    {
        int Child = Convert.ToInt16(Travellers.Split('-')[1]);
        DataTable Dt = new DataTable();
        Dt.Columns.Add("Age");
        Dt.Columns.Add("PaxName");
        //List<int> TravellerList = new List<int>();
        if (Child > 0)
        {
            int k = 0;
            for (int i = 1; i <= Child; i++)
            {
                Dt.Rows.Add(Convert.ToInt16(Travellers.Split('-')[i + 1]), ChildPaxDetails.Split('-')[k]);
                ++k;
            }
        }
        return Dt;
    }
    public DataTable SetAdultPaxes(string TotAdult, string AdultPaxDetails)
    {
        DataTable Dt = new DataTable();
        Dt.Columns.Add("PaxName");
        if (Convert.ToInt16(TotAdult) > 0)
        {
            for (int i = 0; i < Convert.ToInt16(TotAdult); i++)
            {
                Dt.Rows.Add(AdultPaxDetails.Split('-')[i]);
            }
        }
        return Dt;
    }
    public string CreateXMLforHotelList(string City, string Country)
    {
        string Results = "";
        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XMLRequest;
        XmlElement XMLHeader;
        XmlElement XMLBody;
        XmlElement XMLNODE;
        XmlElement XMLCHILDNODE;

        //Node Request Start
        XMLRequest = XmlDoc.CreateElement("Request");
        //Source Node Start
        XMLHeader = XmlDoc.CreateElement("Source");
        //    Node Requester ID Start
        XMLNODE = XmlDoc.CreateElement("RequestorID");
        XMLNODE.SetAttribute("Client", ClientID);
        XMLNODE.SetAttribute("EMailAddress", EmailAddress);
        XMLNODE.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNODE);
        //    Node Requester ID End
        //    Node RequestorPreferences Start
        XMLNODE = XmlDoc.CreateElement("RequestorPreferences");
        XMLNODE.SetAttribute("Language", Language);
        XMLNODE.SetAttribute("Currency", Currency);
        XMLNODE.SetAttribute("Country", Country);
        //       Node RequestMode Start
        XMLCHILDNODE = XmlDoc.CreateElement("RequestMode");
        XMLCHILDNODE.InnerText = "SYNCHRONOUS";
        XMLNODE.AppendChild(XMLCHILDNODE);
        //       Node RequestMode End
        XMLHeader.AppendChild(XMLNODE);
        //    Node RequestorPreferences End
        //Source Node Start End
        XMLRequest.AppendChild(XMLHeader);
        //RequestDetails Node Start

        XMLBody = XmlDoc.CreateElement("RequestDetails");

        XMLNODE = XmlDoc.CreateElement("SearchItemRequest");
        XMLNODE.SetAttribute("ItemType", "hotel");

        XMLCHILDNODE = XmlDoc.CreateElement("ItemDestination");
        XMLCHILDNODE.SetAttribute("DestinationType", "city");
        XMLCHILDNODE.SetAttribute("DestinationCode", City);
        XMLNODE.AppendChild(XMLCHILDNODE);

        XMLBody.AppendChild(XMLNODE);

        XMLRequest.AppendChild(XMLBody);
        XmlDoc.AppendChild(XMLRequest);

        //      Booking Items Start
        Results = SendRequestForSightSeeing(XmlDoc, "Hotel-List", "", City, "");
        return Results;
    }
    public Int64 GetBookingRefID()
    {
        Int64 BookRefID = 0;
        DBSighseeingAccess BDS = new DBSighseeingAccess();
        BDS.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BDS.BOOKDATE = System.DateTime.Now;
        BDS.CANCELDATE = System.DateTime.Now.AddDays(-1);
        DataTable Dt = BDS.InsertNewBookingDetails();
        if (Dt.Rows.Count != 0)
        {
            BookRefID = Convert.ToInt64(Dt.Rows[0]["BookingRefID"]);
            HttpContext.Current.Session["ActBookRefID"] = BookRefID;
            BDS.BOOKINGREFID = BookRefID;
            BDS.BookingStatusCode = "RQ";
            BDS.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
            int Success = BDS.InsertBookingHistoryDetails();
        }
        return BookRefID;
    }
    public string GetGtaAOTNumbers(string CountryCode)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchAOTNumberRequest");
        XMLChildNode = XMLDoc.CreateElement("AssistanceLanguage");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("Destination");
        XMLChildNode.InnerText = "AU";
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("Nationality");
        XMLChildNode.InnerText = CountryCode;
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ServiceType");
        XMLChildNode.InnerText = "sightseeing";
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        string Results = SendRequestForSightSeeing(XMLDoc, "GetGtaAOTNumbers", "", "", "");
        return Results;
    }
    public string BookingActivityUpdate(string APIid)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchBookingItemRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "api");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequestForSightSeeing(XMLDoc, "UpdateBook", "", "", "");
    }
    public string BookingActivityUpdateClient(string APIid)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;

        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchBookingItemRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "client");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequestForSightSeeing(XMLDoc, "UpdateBook", "", "", "");
    }
    public string BookingActivityUpdateClientWithCancellationPolicy(string APIid, string bookingrefid, string ItemCode, string ItemCity, string TourDate, string LanguageCode, string LanguageListCode, string travellers, string specialCode)
    {
        System.Xml.XmlDocument XMLDoc = new System.Xml.XmlDocument();
        System.Xml.XmlElement XMLRequest;
        System.Xml.XmlElement XMLHeader;
        System.Xml.XmlElement XMLBody;
        System.Xml.XmlElement XMLNode;
        System.Xml.XmlElement XMLChildNode;
        System.Xml.XmlElement XMLFirstChildNode;
        System.Xml.XmlElement XMLFIRSTINNERCHILDNODE;

        //Set Paxes
        int Adult = Convert.ToInt16(travellers.Split('-')[0]);
        int Child = 0;
        if (travellers.Length > 3)
        {
            Child = Convert.ToInt16(travellers.Split('-')[1]);
        }
        List<int> TravellerChildAge = new List<int>();
        if (Child > 0)
        {
            TravellerChildAge = GetAllTravellersWithAge(travellers);
        }



        // create XML
        // create request
        XMLRequest = XMLDoc.CreateElement("Request");
        XMLHeader = XMLDoc.CreateElement("Source");
        // client details node
        XMLNode = XMLDoc.CreateElement("RequestorID");
        XMLNode.SetAttribute("Client", ClientID);
        XMLNode.SetAttribute("EMailAddress", EmailAddress);
        XMLNode.SetAttribute("Password", ClientPassword);
        XMLHeader.AppendChild(XMLNode);
        // client preference node
        XMLNode = XMLDoc.CreateElement("RequestorPreferences");
        XMLNode.SetAttribute("Language", Language);
        XMLChildNode = XMLDoc.CreateElement("RequestMode");
        XMLChildNode.InnerText = "SYNCHRONOUS";
        XMLNode.AppendChild(XMLChildNode);
        XMLHeader.AppendChild(XMLNode);
        // create request body
        XMLBody = XMLDoc.CreateElement("RequestDetails");
        XMLNode = XMLDoc.CreateElement("SearchBookingItemRequest");
        XMLChildNode = XMLDoc.CreateElement("BookingReference");
        XMLChildNode.SetAttribute("ReferenceSource", "client");
        XMLChildNode.InnerText = APIid;
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        //create cancellation policy request start
        XMLNode = XMLDoc.CreateElement("SearchChargeConditionsRequest");
        XMLChildNode = XMLDoc.CreateElement("DateFormatResponse");
        XMLNode.AppendChild(XMLChildNode);
        XMLChildNode = XMLDoc.CreateElement("ChargeConditionsSightseeing");
        XMLFirstChildNode = XMLDoc.CreateElement("City");
        XMLFirstChildNode.InnerText = ItemCity;
        XMLChildNode.AppendChild(XMLFirstChildNode);

        XMLFirstChildNode = XMLDoc.CreateElement("Item");
        XMLFirstChildNode.InnerText = ItemCode;
        XMLChildNode.AppendChild(XMLFirstChildNode);

        XMLFirstChildNode = XMLDoc.CreateElement("TourDate");
        XMLFirstChildNode.InnerText = TourDate;
        XMLChildNode.AppendChild(XMLFirstChildNode);
        //  Policy Node Start
        if (LanguageListCode != "")
        {
            //      Node TourLanguage start
            XMLFirstChildNode = XMLDoc.CreateElement("TourLanguageList");
            XMLFirstChildNode.InnerText = LanguageListCode;
            XMLChildNode.AppendChild(XMLFirstChildNode);
            //      Node TourLanguage End
        }
        if (LanguageCode != "")
        {
            //      Node TourLanguage start
            XMLFirstChildNode = XMLDoc.CreateElement("TourLanguage");
            XMLFirstChildNode.InnerText = LanguageCode;
            XMLChildNode.AppendChild(XMLFirstChildNode);
            //      Node TourLanguage End
        }
        if (specialCode != "")
        {
            if (specialCode != "0")
            {
                //      Node SpecialCode start
                XMLFirstChildNode = XMLDoc.CreateElement("SpecialCode");
                XMLFirstChildNode.InnerText = specialCode;
                XMLChildNode.AppendChild(XMLFirstChildNode);
                //      Node SpecialCode End
            }
        }
        //      Node NumberOfAdults Start
        XMLFirstChildNode = XMLDoc.CreateElement("NumberOfAdults");
        XMLFirstChildNode.InnerText = Adult.ToString();
        XMLChildNode.AppendChild(XMLFirstChildNode);
        //      Node NumberOfAdults End
        //      Node Children start
        if (Child > 0)
        {
            XMLFirstChildNode = XMLDoc.CreateElement("Children");
            for (int i = 0; i < TravellerChildAge.Count; i++)
            {
                XMLFIRSTINNERCHILDNODE = XMLDoc.CreateElement("Age");
                XMLFIRSTINNERCHILDNODE.InnerText = TravellerChildAge[i].ToString();
                XMLFirstChildNode.AppendChild(XMLFIRSTINNERCHILDNODE);
            }
            XMLChildNode.AppendChild(XMLFirstChildNode);
        }
        //      Node Children End
        XMLNode.AppendChild(XMLChildNode);
        XMLBody.AppendChild(XMLNode);
        //create cancellation policy request End   
        // create XML document
        XMLRequest.AppendChild(XMLHeader);
        XMLRequest.AppendChild(XMLBody);
        XMLDoc.AppendChild(XMLRequest);
        // post document 
        return SendRequestForSightSeeing(XMLDoc, "UpdateBookRQ", "", "", "");
    }
}
