﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// Summary description for BookEntry
/// </summary>
public partial class BookEntry
{
    [JsonProperty("booking")]
    public Booking Booking { get; set; }

    [JsonProperty("debitIdList")]
    public List<string> debitIdList { get; set; }
}

public partial class Booking
{
    [JsonProperty("hotels")]
    public List<Hotel> Hotels { get; set; }

    [JsonProperty("serviceId")]
    public long ServiceId { get; set; }

    [JsonProperty("supplierId")]
    public long SupplierId { get; set; }

    [JsonProperty("reference")]
    public string Reference { get; set; }

    [JsonProperty("comments")]
    public object Comments { get; set; }

    [JsonProperty("bookPosition")]
    public long BookPosition { get; set; }

    [JsonProperty("supplierRemarks")]
    public string SupplierRemarks { get; set; }

    [JsonProperty("otherDetails")]
    public string OtherDetails { get; set; }

    [JsonProperty("bookDate")]
    public string BookDate { get; set; }

    [JsonProperty("deadlineDate")]
    public string DeadlineDate { get; set; }
}

public partial class Hotel
{
    [JsonProperty("rooms")]
    public List<Room> Rooms { get; set; }

    [JsonProperty("serialNumber")]
    public long SerialNumber { get; set; }

    [JsonProperty("supplierCode")]
    public string SupplierCode { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("address")]
    public string Address { get; set; }

    [JsonProperty("cityCode")]
    public string CityCode { get; set; }

    [JsonProperty("telephone")]
    public string Telephone { get; set; }

    [JsonProperty("starCategory")]
    public long StarCategory { get; set; }

    [JsonProperty("payableBy")]
    public string PayableBy { get; set; }

    [JsonProperty("emergencyNumber")]
    public string EmergencyNumber { get; set; }

    [JsonProperty("minimumNights")]
    public long MinimumNights { get; set; }

    [JsonProperty("tariffRemarks")]
    public string TariffRemarks { get; set; }
}

public partial class Room
{
    [JsonProperty("roomQuotes")]
    public List<RoomQuote> RoomQuotes { get; set; }

    [JsonProperty("roomTypeId")]
    public Int32 RoomTypeId { get; set; }

    [JsonProperty("category")]
    public string Category { get; set; }

    [JsonProperty("meal")]
    public string Meal { get; set; }

    [JsonProperty("isExtraBed")]
    public bool IsExtraBed { get; set; }

    [JsonProperty("isBabyCot")]
    public bool IsBabyCot { get; set; }

    [JsonProperty("supplierRoomType")]
    public string SupplierRoomType { get; set; }

    [JsonProperty("roomDetail")]
    public object RoomDetail { get; set; }
}

public partial class RoomQuote
{
    //[JsonProperty("amendmentPolicy")]
    //public List<Policy> AmendmentPolicy { get; set; }

    [JsonProperty("cancellationPolicy")]
    public List<Policy> CancellationPolicy { get; set; }

    [JsonProperty("guests")]
    public List<Guest> Guests { get; set; }

    [JsonProperty("serialNumber")]
    public long SerialNumber { get; set; }

    [JsonProperty("date")]
    public string Date { get; set; }

    [JsonProperty("numberOfAdults")]
    public long NumberOfAdults { get; set; }

    [JsonProperty("status")]
    public string Status { get; set; }

    [JsonProperty("checkInDate")]
    public string CheckInDate { get; set; }

    [JsonProperty("checkOutDate")]
    public string CheckOutDate { get; set; }

    [JsonProperty("nights")]
    public long Nights { get; set; }

    [JsonProperty("costPerNight")]
    public double CostPerNight { get; set; }

    [JsonProperty("totalCost")]
    public double TotalCost { get; set; }
}

public partial class Policy
{
    [JsonProperty("fromDate")]
    public string FromDate { get; set; }

    [JsonProperty("toDate")]
    public string ToDate { get; set; }

    [JsonProperty("totalCost")]
    public double TotalCost { get; set; }

    [JsonProperty("remarks")]
    public object Remarks { get; set; }
}

public partial class Guest
{
    [JsonProperty("serialNumber")]
    public long SerialNumber { get; set; }

    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    [JsonProperty("surName")]
    public string SurName { get; set; }

    [JsonProperty("age")]
    public object Age { get; set; }

    [JsonProperty("isLead")]
    public string IsLead { get; set; }

    [JsonProperty("titleId")]
    public long TitleId { get; set; }

    [JsonProperty("paxTypeCode")]
    public string PaxTypeCode { get; set; }

    [JsonProperty("email")]
    public object Email { get; set; }

    [JsonProperty("phone")]
    public object Phone { get; set; }

    [JsonProperty("nationalityCode")]
    public string NationalityCode { get; set; }

    [JsonProperty("countryOfResidenceCode")]
    public string CountryOfResidenceCode { get; set; }
}
