﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for BALInsuranceBook
/// </summary>
public class BALInsuranceBook
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();
    public BALInsuranceBook()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public Int64 UID { get; set; }
    public string PNR { get; set; }
    public DateTime BOOKDATE { get; set; }
    public DateTime CANCELDATE { get; set; }
    public DataTable InsertNewBookingDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Position", "1");
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", "4");
        cmd.Parameters.AddWithValue("@BookingDate", BOOKDATE);
        cmd.Parameters.AddWithValue("@SupplierID", "3");
        cmd.Parameters.AddWithValue("@SupplierBookingReference", "");
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", "RQ");
        cmd.Parameters.AddWithValue("@PaymentStatusID", "1");
        cmd.Parameters.AddWithValue("@AgencyRemarks", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertnewbooking";
        return Dalref.ExeReader(cmd);

    }
    public string BOOKINGREFID { get; set; }
    public string BookingStatusCode { get; set; }
    public int InsertBookingHistoryDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@UserID", UID);
        cmd.Parameters.AddWithValue("@ActionDate", BOOKDATE);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHistory";
        return Dalref.ExeNonQuery(cmd);
    }

    public Int16 TitleCustomer { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string EmailID { get; set; }
    public string Mobile { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public DateTime DOB { get; set; }
    public string PassportNumber { get; set; }
    public int InserttblInsuranceCustomerInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@TitleCustomer", TitleCustomer);
        cmd.Parameters.AddWithValue("@FirstName", FirstName);
        cmd.Parameters.AddWithValue("@LastName", LastName);
        cmd.Parameters.AddWithValue("@EmailID", EmailID);
        cmd.Parameters.AddWithValue("@Mobile", Mobile);
        cmd.Parameters.AddWithValue("@Address1", Address1);
        cmd.Parameters.AddWithValue("@Address2", Address2);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsuranceCustomerInfo values(@BookingReferenceID,@TitleCustomer,@FirstName,@LastName,@EmailID,@Mobile,@Address1,@Address2)";
        return Dalref.ExeNonQuery(cmd);
    }
    public int inserttblInsuranceTravellers()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@TitleCustomer", TitleCustomer);
        cmd.Parameters.AddWithValue("@FirstName", FirstName);
        cmd.Parameters.AddWithValue("@LastName", LastName);
        cmd.Parameters.AddWithValue("@DateofBirth", DOB);
        cmd.Parameters.AddWithValue("@PassportNumber", PassportNumber);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsuranceTravellers values(@BookingReferenceID,@TitleCustomer,@FirstName,@LastName,@DateofBirth,@PassportNumber)";
        return Dalref.ExeNonQuery(cmd);
    }
    public int AddOnOptionID { get; set; }
    public string AddOnOption { get; set; }
    public string Price { get; set; }
    public string CurrencyCode { get; set; }
    public string Description { get; set; }

    public int inserttblInsuranceAddOn()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@AddOnOptionID", AddOnOptionID);
        cmd.Parameters.AddWithValue("@AddOnOption", AddOnOption);
        cmd.Parameters.AddWithValue("@Price", Price);
        cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
        cmd.Parameters.AddWithValue("@Description", Description);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsuranceAddOn values(@BookingReferenceID,@AddOnOptionID,@AddOnOption,@Price,@CurrencyCode,@Description)";
        return Dalref.ExeNonQuery(cmd);
    } 
    public int PremiumID { get; set; }
    public string Premium { get; set; }
    public string DestinationID { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public int inserttblInsurancePremium()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PremiumID", PremiumID);
        cmd.Parameters.AddWithValue("@Premium", Premium);
        cmd.Parameters.AddWithValue("@Price", Price);
        cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
        cmd.Parameters.AddWithValue("@Description", "");
        cmd.Parameters.AddWithValue("@PolicyID", "");
        cmd.Parameters.AddWithValue("@PolicyNumber", "");
        cmd.Parameters.AddWithValue("@StartDate", StartDate);
        cmd.Parameters.AddWithValue("@EndDate", EndDate);
        cmd.Parameters.AddWithValue("@DestinationID", DestinationID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsurancePremium values(@BookingReferenceID,@PremiumID,@Premium,@Price,@CurrencyCode,@Description,@PolicyID,@PolicyNumber,@StartDate,@EndDate,@DestinationID)";
        return Dalref.ExeNonQuery(cmd);
    }
    public string FullBenifits { get; set; }
    public int inserttblInsurancePremiumBenefits()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@FullBenifits", FullBenifits);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsurancePremiumBenefits values(@BookingReferenceID,@FullBenifits)";
        return Dalref.ExeNonQuery(cmd);
    }

    public string BasicAmount { get; set; }

    public string TotalAmount { get; set; }
    public Int64 MarkupValue { get; set; }
    public int inserttblInsuranceCost()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@TotalNet", BasicAmount);
        cmd.Parameters.AddWithValue("@TotalNetCurrency", CurrencyCode);
        cmd.Parameters.AddWithValue("@MarkupType", "1");
        cmd.Parameters.AddWithValue("@MarkupValue", MarkupValue);
        cmd.Parameters.AddWithValue("@MarkupCurrency", "OMR");
        cmd.Parameters.AddWithValue("@TotalSaleValue", TotalAmount);
        cmd.Parameters.AddWithValue("@TotalSaleCurrency", CurrencyCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblInsuranceCost values(@BookingReferenceID,@TotalNet,@TotalNetCurrency,@MarkupType,@MarkupValue,@MarkupCurrency,@TotalSaleValue,@TotalSaleCurrency)";
        return Dalref.ExeNonQuery(cmd);
    }
    public string PolicyID { get; set; }
    public string PolicyNumber { get; set; }
    public int UpdatetblBooking()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode = @BookingStatusCode  where BookingRefID = @BookingReferenceID";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdatetblInsurancePremium()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PolicyID", PolicyID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblInsurancePremium set PolicyID = @PolicyID  where BookingReferenceID = @BookingReferenceID";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdatetblBookingpolicynumber()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@SupplierBookingReference", PolicyNumber);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode = @BookingStatusCode,SupplierBookingReference=@SupplierBookingReference   where BookingRefID = @BookingReferenceID";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdatetblInsurancePremiumpolicynumber()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PolicyNumber", PolicyNumber);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblInsurancePremium set 	PolicyNumber = @PolicyNumber  where BookingReferenceID = @BookingReferenceID";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetInsuranceData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Progetinsuranceinfo";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetInsurancePaxData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProgetinsurancePaxinfo";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetInsuranceAddoninfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProgetinsuranceAddoninfo";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetpriceBreak()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select * from tblInsuranceCost where BookingReferenceID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public string BookingDateFrom { get; set; }
    public string BookingDateTo { get; set; }
    public string CheckinDateFrom { get; set; }
    public string CheckinDateTo { get; set; }
    public Int64 ddlBranch { get; set; }
    public string ddlStatus { get; set; }
    public static object DbNullIfNull(object obj)
    {

        if (obj.ToString() == "0")
        {
            return obj != null ? obj : DBNull.Value;

        }
        else
        {
            return obj.ToString() != "" ? obj : DBNull.Value;
        }


    }
    public DataTable GetHotelSearchData()
    {

        string tempbranch = "";
        if (ddlBranch != 0)
        {
            tempbranch = ddlBranch.ToString();
        }
       

        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ItemName", DbNullIfNull(Premium));
        cmd.Parameters.AddWithValue("@BookingDateFrom", DbNullIfNull(DbNullIfNull(BookingDateFrom)));
        cmd.Parameters.AddWithValue("@BookingDateTo", DbNullIfNull(BookingDateTo));
        cmd.Parameters.AddWithValue("@CheckinDateFrom", DbNullIfNull(CheckinDateFrom));
        cmd.Parameters.AddWithValue("@CheckinDateTo", DbNullIfNull(CheckinDateTo));
        cmd.Parameters.AddWithValue("@BookingRefNo", DbNullIfNull(BOOKINGREFID));
        cmd.Parameters.AddWithValue("@SupplierRefNo", DbNullIfNull(PolicyNumber));
        cmd.Parameters.AddWithValue("@ddlBranch", DbNullIfNull(tempbranch));
        cmd.Parameters.AddWithValue("@ddlStatus", DbNullIfNull(ddlStatus));
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsuraceSearchBooking";
        return Dalref.ExeReader(cmd);
    }
    public Int64 AgencyID { get; set; }
    public DataTable GetActionReqInsurance()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetActionReqInsurance";
        return Dalref.ExeReader(cmd);
    }
    public DataTable CheckVochurlinkavailable()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select VoucherUrl from tblVoucherDetails  where BookRefID=@BookRefID";
        return Dalref.ExeReader(cmd);
    }
    public string VoucherUrl { get; set; }
    public int insertVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@VoucherUrl", VoucherUrl);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertVoucherUrlinformation";
        return Dalref.ExeNonQuery(cmd);

    }
    public string PAXRef { get; set; }
    public DataTable GetHotelBookingHistory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", PAXRef);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select C.FirstName,C.LastName,A.*,B.* from tblBookingHistory A inner join tblBookingStatus B on B.BookingStatusCode=A.BookingStatusCode inner join tbluser C on C.UserID=A.UserID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject)
    {
        DataTable DtEmailSettings = GetEmailSettings(7);
        if (DtEmailSettings.Rows.Count > 0)
        {

        }
        string from = "info@oneviewit.com";
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential("servicenotifymail@gmail.com", "ambalath");
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }
    public DataTable GetEmailSettings(Int64 ESID)
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ESID", ESID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblMailSettings where ID=@ESID";
        return Dalref.ExeReader(cmd);
    }

}