﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;

/// <summary>
/// Summary description for DBSighseeingAccess
/// </summary>
public class DBSighseeingAccess
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();
    public DBSighseeingAccess()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Int64 UserID { get; set; }
    public DateTime BOOKDATE { get; set; }
    public DateTime CANCELDATE { get; set; }
    public DataTable InsertNewBookingDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Position", "1");
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", "3");
        cmd.Parameters.AddWithValue("@BookingDate", BOOKDATE);
        cmd.Parameters.AddWithValue("@SupplierID", "2");
        cmd.Parameters.AddWithValue("@SupplierBookingReference", "");
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", "RQ");
        cmd.Parameters.AddWithValue("@PaymentStatusID", "1");
        cmd.Parameters.AddWithValue("@AgencyRemarks", "");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "Procinsertnewbooking";
        return Dalref.ExeReader(cmd);

    }
    public Int64 BOOKINGREFID { get; set; }
    public string BookingStatusCode { get; set; }
    public int InsertBookingHistoryDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingReferenceID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@ActionDate", BOOKDATE);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingHistory";
        return Dalref.ExeNonQuery(cmd);
    }
    public string SupplierBookingReference { get; set; }
    public int updatetblbooking()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@SupplierBookingReference", SupplierBookingReference);
        cmd.Parameters.AddWithValue("@CancellationDeadline", CANCELDATE);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcupdateSupplierBookingReference";
        return Dalref.ExeNonQuery(cmd);
    }
    public string ClientRefID { get; set; }
    public string AppRefID { get; set; }
    public DateTime BookingCreationDate { get; set; }
    public DateTime BookingDepartureDate { get; set; }
    public string BookingName { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string ItemCityCode { get; set; }
    public string SupplierStatus { get; set; }
    public string ItemConfirmationReference { get; set; }
    public DateTime TourDate { get; set; }
    public string TourLanguageCode { get; set; }
    public string TourSupplierCode { get; set; }
    public string TourSupplierName { get; set; }
    public string SpecialItem { get; set; }
    public string TourSummary { get; set; }
    public string TourNote { get; set; }

    public DataTable Insertprocaddactivityinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@ClientRefID", ClientRefID);
        cmd.Parameters.AddWithValue("@APIRefID", AppRefID);
        cmd.Parameters.AddWithValue("@BookingCreationDate", BookingCreationDate);
        cmd.Parameters.AddWithValue("@BookingDepartureDate", BookingDepartureDate);
        cmd.Parameters.AddWithValue("@BookingName", BookingName);
        cmd.Parameters.AddWithValue("@ItemCode", ItemCode);
        cmd.Parameters.AddWithValue("@ItemName", ItemName);
        cmd.Parameters.AddWithValue("@ItemCityCode", ItemCityCode);
        cmd.Parameters.AddWithValue("@SupplierStatus", SupplierStatus);
        cmd.Parameters.AddWithValue("@ItemConfirmationReference", ItemConfirmationReference);
        cmd.Parameters.AddWithValue("@TourDate", TourDate);
        cmd.Parameters.AddWithValue("@TourLanguageCode", TourLanguageCode);
        cmd.Parameters.AddWithValue("@TourSupplierCode", TourSupplierCode);
        cmd.Parameters.AddWithValue("@TourSupplierName", TourSupplierName);
        cmd.Parameters.AddWithValue("@SpecialCode", SpecialItem);
        cmd.Parameters.AddWithValue("@TourSummary", TourSummary);
        cmd.Parameters.AddWithValue("@TourNote", TourNote);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procaddactivityinformation";
        return Dalref.ExeReader(cmd);
    }
    public double GrossValue { get; set; }
    public string GrossCurrency { get; set; }
    public double NetValue { get; set; }
    public double Discount { get; set; }
    public int MarkUpType { get; set; }
    public double MarkUpValue { get; set; }
    public string MarkUpCurrency { get; set; }
    public string SellingCurrency { get; set; }
    public double PriceForClient { get; set; }
    public double AdditionalServiceFeebyClient { get; set; }
    public double TotalSellAmount { get; set; }
    public double RateofChange { get; set; }
    public double Commission { get; set; }
    public Int64 ActivityID { get; set; }

    public int InsertBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@GrossCurrency", GrossCurrency);
        cmd.Parameters.AddWithValue("@NetValue", NetValue);
        cmd.Parameters.AddWithValue("@Discount", Discount);
        cmd.Parameters.AddWithValue("@MarkUpType", MarkUpType);
        cmd.Parameters.AddWithValue("@MarkUpValue", MarkUpValue);
        cmd.Parameters.AddWithValue("@MarkUpCurrency", MarkUpCurrency);
        cmd.Parameters.AddWithValue("@SellingCurrency", SellingCurrency);
        cmd.Parameters.AddWithValue("@PriceForClient", PriceForClient);
        cmd.Parameters.AddWithValue("@AdditionalServiceFeebyClient", AdditionalServiceFeebyClient);
        cmd.Parameters.AddWithValue("@TotalSellAmount", TotalSellAmount);
        cmd.Parameters.AddWithValue("@RateofChange", RateofChange);
        cmd.Parameters.AddWithValue("@Commission", Commission);
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingActivityPriceDetails";
        return Dalref.ExeNonQuery(cmd);
    }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public double ChargeAmount { get; set; }
    public string Currency { get; set; }
    public bool ConditionCharge { get; set; }
    public int InsertBookingCancellationCharges()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.Parameters.AddWithValue("@FromDate", FromDate);
        cmd.Parameters.AddWithValue("@ToDate", ToDate);
        cmd.Parameters.AddWithValue("@ChargeAmount", ChargeAmount);
        cmd.Parameters.AddWithValue("@Currency", Currency);
        cmd.Parameters.AddWithValue("@ConditionCharge", ConditionCharge);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procaddActivityCancellationCharges";
        return Dalref.ExeNonQuery(cmd);
    }
    public int PaxRefID { get; set; }
    public string PaxName { get; set; }
    public string PaxType { get; set; }
    public string Age { get; set; }
    public int TitleID { get; set; }

    public int InsertBookingActivityPax()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.Parameters.AddWithValue("@PaxRefID", PaxRefID);
        cmd.Parameters.AddWithValue("@PaxName", PaxName);
        cmd.Parameters.AddWithValue("@PaxType", PaxType);
        cmd.Parameters.AddWithValue("@Age", Age);
        cmd.Parameters.AddWithValue("@TitleID", TitleID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procaddActivityPaxTypes";
        return Dalref.ExeNonQuery(cmd);
    }
    public string DepartureAddress { get; set; }
    public string DeparturePoint { get; set; }
    public string DepartureName { get; set; }
    public string DepartureTime { get; set; }
    public string Telephone { get; set; }
    public string DepartureInfo { get; set; }
    public string HotelCode { get; set; }
    public int InsertBookingDeparture()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.Parameters.AddWithValue("@DepartureAddress", DepartureAddress);
        cmd.Parameters.AddWithValue("@DeparturePoint", DeparturePoint);
        cmd.Parameters.AddWithValue("@DepartureName", DepartureName);
        cmd.Parameters.AddWithValue("@DepartureTime", DepartureTime);
        cmd.Parameters.AddWithValue("@Telephone", Telephone);
        cmd.Parameters.AddWithValue("@DepartureInfo", DepartureInfo);
        cmd.Parameters.AddWithValue("@HotelCode", HotelCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingActivityDeparture";
        return Dalref.ExeNonQuery(cmd);
    }
    public string EssFromDate { get; set; }
    public string EssToDate { get; set; }
    public string Essentialinformation { get; set; }
    public int InsertBookingEssentialinfomation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.Parameters.AddWithValue("@FromDate", EssFromDate);
        cmd.Parameters.AddWithValue("@ToDate", EssToDate);
        cmd.Parameters.AddWithValue("@Information", Essentialinformation);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertBookingEssenatialinfo";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetSightBookInfoByREfID()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procselbookdetailsByRefID";
        return Dalref.ExeReader(cmd);
    }
    public string BookStatus { get; set; }
    public string ddlBranch { get; set; }
    public string RefNo { get; set; }
    public string SuppRefNo { get; set; }
    public string ItemCity { get; set; }
    public string ActivityName { get; set; }
    public string BookdateFrom { get; set; }
    public string BookdateTo { get; set; }
    public string TourDateFrom { get; set; }
    public string TourDateTo { get; set; }
    public DataTable GetActivitySearchData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookStatus", DbNullIfNull(BookStatus));
        cmd.Parameters.AddWithValue("@ddlBranch", DbNullIfNull(ddlBranch));
        cmd.Parameters.AddWithValue("@RefNo", DbNullIfNull(RefNo));
        cmd.Parameters.AddWithValue("@SuppRefNo", DbNullIfNull(SuppRefNo));
        cmd.Parameters.AddWithValue("@ItemCity", DbNullIfNull(ItemCity));
        cmd.Parameters.AddWithValue("@ActivityName", DbNullIfNull(ActivityName));
        cmd.Parameters.AddWithValue("@BookdateFrom", DbNullIfNull(BookdateFrom));
        cmd.Parameters.AddWithValue("@BookdateTo", DbNullIfNull(BookdateTo));
        cmd.Parameters.AddWithValue("@TourDateFrom", DbNullIfNull(TourDateFrom));
        cmd.Parameters.AddWithValue("@TourDateTo", DbNullIfNull(TourDateTo));
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcActivitySearchBookingNew";
        return Dalref.ExeReader(cmd);
    }
    public static object DbNullIfNull(object obj)
    {

        if (obj.ToString() == "0")
        {
            return obj != null ? obj : DBNull.Value;

        }
        else
        {
            return obj.ToString() != "" ? obj : DBNull.Value;
        }


    }
    public DataTable GetSightHistory()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select A.*,(C.Title+' '+B.FirstName+' '+B.LastName) as PersonName from tblBookingHistory A inner join tblUser B on B.UserID = A.UserID inner join tblTitle C on C.TitleID = B.TitleID where A.BookingRefID=" + BOOKINGREFID + "";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetPaxes()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select A.*,C.Title from tblActivityPax A inner join tblBookingActivityInformation B on  B.ActivityID=A.ActivityID inner join tblTitle C on C.TitleID = A.TitleID where B.BookingRefID=" + BOOKINGREFID + " order by A.PaxRefID asc";
        return Dalref.ExeReader(cmd);
    }
    public DataTable ActivityBookingPolicy()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select * from tblBookingActivityCancellationCharges where ActivityID=" + ActivityID + " order by ConditionCharge asc";
        return Dalref.ExeReader(cmd);
    }
    public int Quantity { get; set; }
    public int InsertBookingPaxBreakUp()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Quantity", Quantity);
        cmd.Parameters.AddWithValue("@GrossValue", GrossValue);
        cmd.Parameters.AddWithValue("@Age", Age);
        cmd.Parameters.AddWithValue("@PaxType", PaxType);
        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procaddActivivityPaxPriceBreakup";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetActivityPriceBreakUp()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select A.* from tblbookingActivityPaxPriceBreakUp A where A.ActivityID = " + ActivityID + "";
        return Dalref.ExeReader(cmd);
    }
    public double PaidAmount { get; set; }
    public string CurrencyCode { get; set; }
    public Int16 PaymentTypeID { get; set; }
    public int InsertTblPayment()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PaidAmount", PaidAmount);
        cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
        cmd.Parameters.AddWithValue("@PaidDate", DateTime.Now);
        cmd.Parameters.AddWithValue("@PaymentTypeID", PaymentTypeID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblPayment values(@BookingRefID,@PaidAmount,@CurrencyCode,@PaidDate,@PaymentTypeID) update tblBooking set PaymentStatusID=3 where BookingRefID=@BookingRefID";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdateTblBookingstatus()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode = '" + BookingStatusCode + "' where BookingRefID = " + BOOKINGREFID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdateTblBookingActivityinformationstatus()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingActivityInformation set SupplierStatus='" + BookingStatusCode + "' where  BookingRefID	= " + BOOKINGREFID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetSightCacelamount()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcGetActitvityCancelBookingPriceDetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetCancelInfo()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblbookingcancelpricedetails where  BookingRefID = " + BOOKINGREFID + "";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetEssInformation()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "Select * from tblBookingActivityEssentialInformation where ActivityID=" + ActivityID + "";
        return Dalref.ExeReader(cmd);
    }
    public Int64 AgencyID { get; set; }
    public DataTable GetDataActivityOnrequest()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procselgetrequesteddata";
        return Dalref.ExeReader(cmd);
    }
    public DataTable selectCornjobpendingconfirmation()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select top 1 SupplierBookingReference,BookingRefID from tblBooking where BookingStatusCode='RR' and BookingRefID not in(select BookRefID from tblAutoUpdateRequestlog) and BookingDate>= DATEADD(HOUR, -1, BookingDate) and ServiceTypeId=3";
        return Dalref.ExeReader(cmd);
    }
    public DataTable selectCornjobpendingconfirmationold()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select top 1 A.*,B.SupplierBookingReference from tblAutoUpdateRequestlog A inner join tblBooking B on B.BookingRefID=A.BookRefID and B.ServiceTypeId=3 and B.BookingStatusCode='RR' and datediff(hour,A.LastRequestedDate,'" + BOOKDATE + "')>=2";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetUserID()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select *  from tblBooking where BookingRefID=" + BOOKINGREFID + "";
        return Dalref.ExeReader(cmd);
    }
    public DataTable getactivityID()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select ActivityID from tblBookingActivityInformation where BookingRefID=" + BOOKINGREFID + "";
        return Dalref.ExeReader(cmd);
    }
    public int insertintoAutoUpdateRequestlog()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@LastRequestedDate", BOOKDATE);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblAutoUpdateRequestlog values(@BookRefID,@LastRequestedDate)";
        return Dalref.ExeNonQuery(cmd);
    }
    public int updateAutoUpdateRequestlog()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@LastRequestedDate", BOOKDATE);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblAutoUpdateRequestlog set LastRequestedDate=@LastRequestedDate where BookRefID=@BookRefID";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable SetUserDetailosByRefID()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BKRedID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "getalluserdetailsbyRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Titiles()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblTitle";
        return Dalref.ExeReader(cmd);
    }
    public int updatetblbookingactivityinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@APIRefID", AppRefID);
        cmd.Parameters.AddWithValue("@BookingCreationDate", BookingCreationDate);
        cmd.Parameters.AddWithValue("@BookingDepartureDate", BookingDepartureDate);
        cmd.Parameters.AddWithValue("@BookingName", BookingName);
        cmd.Parameters.AddWithValue("@ItemCode", ItemCode);
        cmd.Parameters.AddWithValue("@ItemName", ItemName);
        cmd.Parameters.AddWithValue("@ItemCityCode", ItemCityCode);
        cmd.Parameters.AddWithValue("@SupplierStatus", SupplierStatus);
        cmd.Parameters.AddWithValue("@ItemConfirmationReference", ItemConfirmationReference);
        cmd.Parameters.AddWithValue("@TourDate", TourDate);
        cmd.Parameters.AddWithValue("@TourLanguageCode", TourLanguageCode);
        cmd.Parameters.AddWithValue("@TourSupplierCode", TourSupplierCode);
        cmd.Parameters.AddWithValue("@TourSupplierName", TourSupplierName);
        cmd.Parameters.AddWithValue("@SpecialCode", SpecialItem);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingActivityInformation set APIRefID=@APIRefID," +
    "BookingCreationDate=@BookingCreationDate," +
    "BookingDepartureDate=@BookingDepartureDate," +
    "BookingName=@BookingName," +
    "ItemCode=@ItemCode," +
    "ItemName=@ItemName," +
    "ItemCityCode=@ItemCityCode," +
    "SupplierStatus=@SupplierStatus," +
    "ItemConfirmationReference=@ItemConfirmationReference," +
    "TourDate=@TourDate," +
    "TourLanguageCode=@TourLanguageCode," +
    "TourSupplierCode=@TourSupplierCode," +
    "TourSupplierName=@TourSupplierName," +
    "SpecialCode=@SpecialCode" +
    " where ActivityID = " + ActivityID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetActionRequiredActivity()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgetactivityactionrequired";
        return Dalref.ExeReader(cmd);
    }
    public DataTable Getusername()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select FirstName,LastName from  tblUser where UserID=@UserID";
        return Dalref.ExeReader(cmd);
    }
    public int Changestatusoftblbookingonrequest()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", BookingStatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode=@BookingStatusCode where BookingRefID=@BookingRefID";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable selectChornjobOnRequest()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procselgetonreqrefidforcronejob";
        return Dalref.ExeReader(cmd);
    }
    public DataTable getRefIDwithAgencyId()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select top 1 A.BookingRefID,C.AgencyID,A.UserID from tblBooking A inner join tblUser B on B.UserID=A.UserID inner join tblAgency C on C.AgencyID=B.AgencyID where BookingStatusCode='RQ' and BookingRefID not in(select BookRefID from tblAutoUpdateRequestlog) and BookingDate>= DATEADD(HOUR, -1, BookingDate) and  ServiceTypeId=3";
        return Dalref.ExeReader(cmd);
    }
    //New Methods
    public int PaymentStatusID { get; set; }
    public int UpdateTblBooking()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode='" + BookingStatusCode + "',SupplierBookingReference='" + SupplierBookingReference + "',PaymentStatusID=" + PaymentStatusID + " where BookingRefID=" + BOOKINGREFID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public int UpdateDepartureDetails()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingActivityDeparture set DepartureName='" + DepartureName + "',DepartureTime='" + DepartureTime + "' where ActivityID=" + ActivityID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public string DestinationCode { get; set; }
    public string OfficeLocation { get; set; }
    public string Language { get; set; }
    public string International { get; set; }
    public string Local { get; set; }
    public string National { get; set; }
    public string OutOfOffice { get; set; }
    public string OfficeHours { get; set; }
    public DataTable insertGtaAOTNumber()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@DestinationCode", DestinationCode);
        cmd.Parameters.AddWithValue("@OfficeLocation", OfficeLocation);
        cmd.Parameters.AddWithValue("@Language", Language);
        cmd.Parameters.AddWithValue("@OfficeHours", OfficeHours);
        cmd.Parameters.AddWithValue("@International", International);
        cmd.Parameters.AddWithValue("@Local", Local);
        cmd.Parameters.AddWithValue("@National", National);
        cmd.Parameters.AddWithValue("@OutOfOffice", OutOfOffice);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblGtaAOTNumbers values(@BookRefID,@DestinationCode,@OfficeLocation,@Language,@OfficeHours,@International,@National,@Local,@OutOfOffice)";
        return Dalref.ExeReader(cmd);
    }
    public string CatregoryCode { get; set; }
    public string CatregoryName { get; set; }
    public DataTable getDataByactCategory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CategoryCode", CatregoryCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblActivityCategory where CategoryCode=@CategoryCode";
        return Dalref.ExeReader(cmd);
    }
    public int AddActivityCategory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CategoryCode", CatregoryCode);
        cmd.Parameters.AddWithValue("@CategoryName", CatregoryName);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblActivityCategory values (@CategoryCode,@CategoryName)";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable getDataByactType()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CategoryCode", CatregoryCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblActivityTypes where CategoryCode=@CategoryCode";
        return Dalref.ExeReader(cmd);
    }
    public int AddActivityTypes()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CategoryCode", CatregoryCode);
        cmd.Parameters.AddWithValue("@CategoryName", CatregoryName);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblActivityTypes values (@CategoryCode,@CategoryName)";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetActCategories()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblActivityCategory";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetActType()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblActivityTypes";
        return Dalref.ExeReader(cmd);
    }
    public double CancelAmount { get; set; }
    public string CancelCurrency { get; set; }
    public double CustomerAmount { get; set; }
    public double Dbcancellamount { get; set; }
    public DataTable InsertCancelBookingPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@CancelNetAmount", CancelAmount);
        cmd.Parameters.AddWithValue("@CancelNetCurrency", CancelCurrency);
        cmd.Parameters.AddWithValue("@CustomerAmount", CustomerAmount);
        cmd.Parameters.AddWithValue("@CustomerCurrency", Currency);
        cmd.Parameters.AddWithValue("@DbCancelAmount", Dbcancellamount);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcInsertCancelBookingPriceDetails";
        return Dalref.ExeReader(cmd);
    }
    public DataTable SendMailid()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select EmailID from tblUser where UserID=@UserID ";
        return Dalref.ExeReader(cmd);
    }
    public string StatusCode { get; set; }
    public int upadteBookingCancellationstatus()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@BookingStatusCode", StatusCode);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcupdateHotelCancellationstatus";
        return Dalref.ExeNonQuery(cmd);
    }
    public string VoucherUrl { get; set; }
    public DataTable CheckVochurlinkavailable()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select VoucherUrl from tblVoucherDetails  where BookRefID=@BookRefID";
        return Dalref.ExeReader(cmd);
    }
    public int insertVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@VoucherUrl", VoucherUrl);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcinsertVoucherUrlinformation";
        return Dalref.ExeNonQuery(cmd);
    }
    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject)
    {
        DataTable DtEmailSettings = GetEmailSettings(7);
        if (DtEmailSettings.Rows.Count > 0)
        {

        }
        string from = "info@oneviewit.com";
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential("info@oneviewit.com", "farishamsa007");
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }
    public DataTable GetEmailSettings(Int64 ESID)
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ESID", ESID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblMailSettings where ID=@ESID";
        return Dalref.ExeReader(cmd);
    }
    public int UpdatetblPriceDetails()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBookingActivityPriceDetails set GrossValue='" + GrossValue + "',NetValue='" + NetValue + "',Discount='" + Discount + "',MarkUpValue='" + MarkUpValue + "' where ActivityID=" + ActivityID + "";
        return Dalref.ExeNonQuery(cmd);
    }
    public Int64 DebitID;
    public int InsertTblBookingDebitEntry()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@DebitID", DebitID);
        cmd.Parameters.AddWithValue("@BookingStatus", BookingStatusCode);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblBookingDebitEntry values(@BookingRefID,@DebitID,@BookingStatus)";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable getDetitEntry()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblBookingDebitEntry where BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
}