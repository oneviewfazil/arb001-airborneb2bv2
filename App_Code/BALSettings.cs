﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net.Mail;
using SelectPdf;
using System.Configuration;


/// <summary>
/// Summary description for BALSettings
/// </summary>
public class BALSettings
{
    SqlCommand cmd = new SqlCommand();
    Dalref Dalref = new Dalref();

    public BALSettings()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public Int64 AgencyID { get; set; }
    public int ServiceTypeID { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }

    public DataTable GetMarkupDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", ServiceTypeID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procSelectGeneralMarkupvalue";
        return Dalref.ExeReader(cmd);
    }
    public DataTable CheckLogin()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Username", Username);
        cmd.Parameters.AddWithValue("@Password", Password);
        cmd.CommandType = CommandType.Text;
        //cmd.CommandText = "select *,B.LoginStatus as AgencyLoginStat,B.AgencyID as AgencyIDD,B.AgencyTypeID as AgencyTypeIDD from tblUser A inner join tblAgency B on A.AgencyID = B.AgencyID inner join tblAgencyType C on B.AgencyTypeID = C.AgencyTypeID inner join tblCity D on D.CityCode = B.CityCode inner join tblCountry E on E.CountryCode = D.CountryCode where C.AgencyTypeID!=4 and A.EmailID=@Username and A.Password=@Password";
        cmd.CommandText = "select *,B.LoginStatus as AgencyLoginStat,B.AgencyID as AgencyIDD,B.AgencyTypeID as AgencyTypeIDD from tblUser A inner join tblAgency B on A.AgencyID = B.AgencyID inner join tblAgencyType C on B.AgencyTypeID = C.AgencyTypeID inner join tblCity D on D.CityCode = B.CityCode inner join tblCountry E on E.CountryCode = D.CountryCode where C.AgencyTypeID!=4 and A.Username=@Username and A.Password=@Password";

        return Dalref.ExeReader(cmd);
    }
    public string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    public string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText.Replace(" ", "+"));
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
    public DataTable GetCreditLimitAmount()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select dbo.getCreditLimitAmount(@AgencyID) as CreditLimitAmountt";
        return Dalref.ExeReader(cmd);
    }
    public Int64 UserID { get; set; }
    public DataTable GetUserRoleStatus()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UID", UserID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetUserRolesStatus";
        return Dalref.ExeReader(cmd);
    }
    public string selectdata { get; set; }
    public string tablename { get; set; }
    public string condition { get; set; }

    public DataTable GetTableDatas()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;

        if (condition != "NULL")
        {
            cmd.Parameters.AddWithValue("@condition", condition);
            cmd.CommandText = "select " + selectdata + " from " + tablename + " where " + condition;

        }
        else
        {
            cmd.Parameters.AddWithValue("@condition", condition);
            cmd.CommandText = "select " + selectdata + " from " + tablename;

        }
        return Dalref.ExeReader(cmd);
    }
    public string Title { get; set; }
    public string FName { get; set; }
    public string LName { get; set; }
    public string MobNo { get; set; }
    public int UpdateUserData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@Title", Title);
        cmd.Parameters.AddWithValue("@FName", FName);
        cmd.Parameters.AddWithValue("@LName", LName);
        cmd.Parameters.AddWithValue("@MobNo", MobNo);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblUser set TitleID=@Title,FirstName=@FName,LastName=@LName,Mobile=@MobNo where UserID=@UserID";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetUserData()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select *,B.LoginStatus as AgencyLoginStat,B.AgencyID as AgencyIDD,B.AgencyTypeID as AgencyTypeIDD from tblUser A inner join tblAgency B on A.AgencyID = B.AgencyID inner join tblAgencyType C on B.AgencyTypeID = C.AgencyTypeID where C.AgencyTypeID!=4 and A.UserID=@UserID";

        return Dalref.ExeReader(cmd);
    }
    public string NewPass { get; set; }
    public int UpdateUserPass()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@NewPass", NewPass);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblUser set Password=@NewPass where UserID=@UserID";
        return Dalref.ExeNonQuery(cmd);
    }
    public string EmailID { get; set; }

    public DataTable GetUserDataEmail()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@EmailID", EmailID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblUser A where A.EmailID=@EmailID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetEmailTemplate(Int64 ETID)
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@TemplateID", ETID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblEmailTemplate where TemplateID=@TemplateID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetEmailSettings(Int64 ESID)
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@ESID", ESID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblMailSettings where ID=@ESID";
        return Dalref.ExeReader(cmd);
    }
    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject, string Attachment)
    {
        DataTable DtEmailSettings = GetEmailSettings(7);
        if (DtEmailSettings.Rows.Count > 0)
        {

        }
        string from = ConfigurationManager.AppSettings["EmailUsername"];
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);
            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);
            if (Attachment != null && Attachment != string.Empty)
            {
                //To PDF
                if (Attachment.Contains(".html"))
                {
                    try
                    {
                        HtmlToPdf converter = new HtmlToPdf();
                        converter.Options.WebPageWidth = 800;
                        string BookingReference = "";
                        if (Attachment.Contains("ticket_attachment_"))
                        {
                            BookingReference = "Your_Ticket_" + Attachment.Split('/')[3].ToString();
                        }
                        else if (Attachment.Contains("Quotation_"))
                        {
                            BookingReference = "Your-Quotation";
                        }
                        else
                        {
                            BookingReference = "Booking_Voucher";
                        }

                        PdfDocument doc = converter.ConvertUrl(HttpContext.Current.Server.MapPath(Attachment));
                        MemoryStream pdfStream = new MemoryStream();
                        doc.Save(pdfStream);
                        pdfStream.Position = 0;
                        message.Attachments.Add(new Attachment(pdfStream, BookingReference + ".pdf"));
                    }
                    catch (Exception)
                    {
                        message.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath(Attachment)));
                    }
                }
                else
                {
                    message.Attachments.Add(new Attachment(HttpContext.Current.Server.MapPath(Attachment)));
                }
            }
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }
    public Int64 BookingRefID { get; set; }
    public DataTable GetBookingInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        //cmd.CommandText = "select * from tblBooking A inner join tblAirOriginDestinationOptions B on B.BookingRefID=@BookingRefID inner join tblAirSegment C on C.OriginDestinationID=B.OriginDestinationID inner join tblAirSegmentBookingAvail D on D.SegmentID=C.SegmentID left join tblAirSegmentStopOver E on E.SegmentID=C.SegmentID where A.BookingRefID=@BookingRefID";
        //cmd.CommandText = "select * from tblBooking A inner join tblAirOriginDestinationOptions B on B.BookingRefID=@BookingRefID inner join tblAirSegment C on C.OriginDestinationID=B.OriginDestinationID inner join tblAirSegmentBookingAvail D ON D.SegmentID=C.SegmentID left join tblAirSegmentStopOver F on F.SegmentID=C.SegmentID Where D.BookAvailID  in ( select top 1 E.BookAvailID from  tblAirSegmentBookingAvail E Where E.SegmentID=C.SegmentID ) and A.BookingRefID=@BookingRefID";
        cmd.CommandText = "select *,RIGHT(CONVERT(VARCHAR, C.DepartureDateTime, 100),7) as DTTimesplit,RIGHT(CONVERT(VARCHAR, C.ArrivalDateTime, 100),7) as ATTimesplit from tblBooking A inner join tblAirOriginDestinationOptions B on B.BookingRefID=@BookingRefID inner join tblAirSegment C on C.OriginDestinationID=B.OriginDestinationID inner join tblAirSegmentBookingAvail D ON D.SegmentID=C.SegmentID left join tblAirSegmentStopOver F on F.SegmentID=C.SegmentID Where D.BookAvailID  in ( select top 1 E.BookAvailID from  tblAirSegmentBookingAvail E Where E.SegmentID=C.SegmentID ) and A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookingPaxInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblAirPassengers A inner join tblTitle B on B.TitleID=A.TitleID inner join tblPassengerType C on C.PaxTypeID=A.PaxTypeID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookingTransInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select *,dbo.getTotalTax(B.BookingCostID) as GrandTotalTax from tblBooking A inner join tblAirBookingCost B on B.BookingRefID=@BookingRefID inner join tblPaymentStatus C on C.PaymentStatusID=A.PaymentStatusID inner join tblBookingStatus D on D.BookingStatusCode=A.BookingStatusCode where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookingBagInfo()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select distinct A.*,C.MarketingAirlineCode from tblAirbaggageDetails A inner join tblAirOriginDestinationOptions B on B.BookingRefID=A.BookingRefID inner join tblAirSegment C on C.OriginDestinationID=B.OriginDestinationID where A.BookingRefID=@BookingRefID and A.FromSeg=C.DepartureAirportLocationCode and A.ToSeg=C.ArrivalAirportLocationCode";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetIssueTktDataDB()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblAirPassengers A inner join tblBooking C on C.BookingRefID=@BookingRefID inner join tblAirPassengerDocument B on B.PaxID=A.PaxID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public string TicketNumber { get; set; }
    public string PersonName { get; set; }
    public string surname { get; set; }
    public string BirthDatepax { get; set; }

    public int UpdateTkt()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.Parameters.AddWithValue("@TicketNumber", TicketNumber);
        cmd.Parameters.AddWithValue("@PersonName", PersonName);
        cmd.Parameters.AddWithValue("@surname", surname);
        cmd.Parameters.AddWithValue("@BirthDatepax", BirthDatepax);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblAirPassengers set ETicketNo=@TicketNumber where BookingRefID=@BookingRefID and DateofBirth=CONVERT(datetime2,@BirthDatepax, 126) and GivenName=@PersonName and Surname=@surname";
        return Dalref.ExeNonQuery(cmd);
    }
    public string BookStatus { get; set; }
    public int UpdateBookStatus()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.Parameters.AddWithValue("@BookStatus", BookStatus);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@ActionDate", DateTime.Now);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set BookingStatusCode=@BookStatus where BookingRefID=@BookingRefID insert into tblBookingHistory values(@BookingRefID,@BookStatus,@UserID,@ActionDate)";
        return Dalref.ExeNonQuery(cmd);
    }
    public int InsertBookHistory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.Parameters.AddWithValue("@BookStatus", BookStatus);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@ActionDate", DateTime.Now);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "insert into tblBookingHistory values(@BookingRefID,@BookStatus,@UserID,@ActionDate)";
        return Dalref.ExeNonQuery(cmd);
    }

    public string DepartureCity { get; set; }
    public string ArrivalCity { get; set; }
    public string DepartureDateFrom { get; set; }
    public string DepartureDateTo { get; set; }
    public string ArrivalDateFrom { get; set; }
    public string ArrivalDateTo { get; set; }
    public string BookingDateFrom { get; set; }
    public string BookingDateTo { get; set; }
    public string DeadlineDateFrom { get; set; }
    public string DeadlineDateTo { get; set; }
    public string BookingRefNo { get; set; }
    public string SupplierRefNo { get; set; }
    public string TicketNo { get; set; }
    public string PassengerName { get; set; }
    public Int64 ddlBranch { get; set; }
    public string ddlStatus { get; set; }
    public int AgencyUserid { get; set; }
    public string agencyCode { get; set; }
    public int adminStatus { get; set; }


    public static object DbNullIfNull(object obj)
    {

        if (obj.ToString() == "0")
        {
            return obj != null ? obj : DBNull.Value;

        }
        else
        {
            return obj.ToString() != "" ? obj : DBNull.Value;
        }


    }
    public DataTable GetSearchData()
    {

        string tempbranch = "";
        if (ddlBranch != 0)
        {
            tempbranch = ddlBranch.ToString();
        }
        //string tempstatus = "";
        //if (ddlStatus == "0")
        //{
        //    tempstatus = ddlStatus.ToString();
        //}

        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@DepartureCity", DbNullIfNull(DepartureCity));
        cmd.Parameters.AddWithValue("@ArrivalCity", DbNullIfNull(ArrivalCity));
        cmd.Parameters.AddWithValue("@DepartureDateFrom", DbNullIfNull(DepartureDateFrom));
        cmd.Parameters.AddWithValue("@DepartureDateTo", DbNullIfNull(DepartureDateTo));
        cmd.Parameters.AddWithValue("@ArrivalDateFrom", DbNullIfNull(ArrivalDateFrom));
        cmd.Parameters.AddWithValue("@ArrivalDateTo", DbNullIfNull(ArrivalDateTo));
        cmd.Parameters.AddWithValue("@BookingDateFrom", DbNullIfNull(DbNullIfNull(BookingDateFrom)));
        cmd.Parameters.AddWithValue("@BookingDateTo", DbNullIfNull(BookingDateTo));
        cmd.Parameters.AddWithValue("@DeadlineDateFrom", DbNullIfNull(DeadlineDateFrom));
        cmd.Parameters.AddWithValue("@DeadlineDateTo", DbNullIfNull(DeadlineDateTo));
        cmd.Parameters.AddWithValue("@BookingRefNo", DbNullIfNull(BookingRefNo));
        cmd.Parameters.AddWithValue("@SupplierRefNo", DbNullIfNull(SupplierRefNo));
        cmd.Parameters.AddWithValue("@TicketNo", DbNullIfNull(TicketNo));
        cmd.Parameters.AddWithValue("@PassengerName", DbNullIfNull(PassengerName));
        cmd.Parameters.AddWithValue("@ddlBranch", DbNullIfNull(tempbranch));
        cmd.Parameters.AddWithValue("@ddlStatus", DbNullIfNull(ddlStatus));
        cmd.Parameters.AddWithValue("@userId", AgencyUserid);
        cmd.Parameters.AddWithValue("@agencyCode", agencyCode);
        cmd.Parameters.AddWithValue("@adminStatus", adminStatus);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "ProcSearchBookingbyUserid";
        return Dalref.ExeReader(cmd);
    }
    public DataTable SendMailTicketIssueCCList()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procGetTicketIssueEmailCC";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookingHistory()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select C.FirstName,C.LastName,A.*,B.* from tblBookingHistory A inner join tblBookingStatus B on B.BookingStatusCode=A.BookingStatusCode inner join tbluser C on C.UserID=A.UserID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetFareRules()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblAirFarerules where BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetFareBreak()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select B.* from tblAirBookingCost A inner join tblAirBookingCostBreakup B on B.BookingCostID=A.BookingCostID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetBookerAgencyDetails()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select C.AgencyTypeID from tblBooking A inner join tblUser B on B.UserID=A.UserID inner join tblAgency C on C.AgencyID=B.AgencyID where A.BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public string BOOKINGREFID { get; set; }
    public string VoucherUrl { get; set; }
    public DataTable CheckVochurlinkavailable()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select VoucherUrl from tblVoucherDetails  where BookRefID=@BookRefID";
        return Dalref.ExeReader(cmd);
    }
    public int insertVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@VoucherUrl", VoucherUrl);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "IF EXISTS (SELECT * FROM tblVoucherDetails WHERE BookRefID=@BookRefID) UPDATE tblVoucherDetails SET VoucherUrl=@VoucherUrl WHERE BookRefID=@BookRefID ELSE INSERT INTO tblVoucherDetails VALUES(@BookRefID,@VoucherUrl)";
        return Dalref.ExeNonQuery(cmd);
    }
    public int DeleteVoucherUrlinformation()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "delete from tblVoucherDetails WHERE BookRefID=@BookRefID";
        return Dalref.ExeNonQuery(cmd);
    }
    public string Operation { get; set; }
    public DateTime BlockDate { get; set; }
    public int CRUDBlockService()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", ServiceTypeID);
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.Parameters.AddWithValue("@Operation", Operation);
        cmd.Parameters.AddWithValue("@BlockDate", BlockDate);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procCRUDBlockService";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable checkServiceBlocked()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@AgencyID", AgencyID);
        cmd.Parameters.AddWithValue("@ServiceTypeID", ServiceTypeID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblBlockedList where AgencyID=@AgencyID and ServiceTypeID=@ServiceTypeID";
        return Dalref.ExeReader(cmd);
    }

    //New 03-10-2017 Payment Gateway
    public double PaidAmount { get; set; }
    public string CurrencyCode { get; set; }
    public int SuccessPaymentGatewayDBOps()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookRefID", BOOKINGREFID);
        cmd.Parameters.AddWithValue("@PaymentStatusID", "3");//Fully Paid
        cmd.Parameters.AddWithValue("@PaidAmount", PaidAmount);
        cmd.Parameters.AddWithValue("@CurrencyCode", CurrencyCode);
        cmd.Parameters.AddWithValue("@PaidDate", DateTime.Now);
        cmd.Parameters.AddWithValue("@PaymentTypeID", "2");//Credit Card
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "update tblBooking set PaymentStatusID=@PaymentStatusID where BookingRefID=@BookRefID insert into tblPayment values(@BookRefID,@PaidAmount,@CurrencyCode,@PaidDate,@PaymentTypeID)";
        return Dalref.ExeNonQuery(cmd);
    }
    public DataTable GetUserDataB2C()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@UserID", UserID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select *,B.LoginStatus as AgencyLoginStat,B.AgencyID as AgencyIDD,B.AgencyTypeID as AgencyTypeIDD from tblUser A inner join tblAgency B on A.AgencyID = B.AgencyID inner join tblAgencyType C on B.AgencyTypeID = C.AgencyTypeID where C.AgencyTypeID=4 and A.UserID=@UserID";
        return Dalref.ExeReader(cmd);
    }
    public string SendMailSubjectwithBcc(string toList, string CCtoList, string BCCtoList, string body, string subject)
    {
        DataTable DtEmailSettings = GetEmailSettings(7);
        if (DtEmailSettings.Rows.Count > 0)
        {

        }
        string from = ConfigurationManager.AppSettings["EmailUsername"];
        string Subject = subject;
        string ccList = CCtoList;
        string bccList = BCCtoList;

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from);
            message.From = fromAddress;
            message.To.Add(toList);
            if (ccList != null && ccList != string.Empty)
                message.CC.Add(ccList);

            if (bccList != null && bccList != string.Empty)
                message.Bcc.Add(bccList);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);

            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }

        return msg;
    }
    public DataTable Authpay()
    {
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@BookingRefID", BookingRefID);
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "select * from tblBooking where PaymentStatusID=3 and BookingRefID=@BookingRefID";
        return Dalref.ExeReader(cmd);
    }
    public string SendExceptionMail(string Exceptionstring)
    {
        string from = ConfigurationManager.AppSettings["EmailUsername"];
        string Subject = "Error Occured";

        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string msg = string.Empty;
        try
        {
            MailAddress fromAddress = new MailAddress(from, ConfigurationManager.AppSettings["EmailDisplayName"]);
            message.From = fromAddress;
            message.To.Add("shabeer@oneviewit.com");
            message.Subject = Subject;
            message.IsBodyHtml = true;
            message.Body = "Error : " + Exceptionstring;
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailUsername"], ConfigurationManager.AppSettings["EmailPassword"]);
            smtpClient.Send(message);
            msg = "1";

        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        return msg;
    }
    public DataTable GetSearchDataTransfer()
    {

        string tempbranch = "";
        if (ddlBranch != 0)
        {
            tempbranch = ddlBranch.ToString();
        }

        cmd.Parameters.Clear();

        cmd.Parameters.AddWithValue("@BookingDateFrom", DbNullIfNull(DbNullIfNull
(BookingDateFrom)));
        cmd.Parameters.AddWithValue("@BookingDateTo", DbNullIfNull(BookingDateTo));

        cmd.Parameters.AddWithValue("@BookingRefNo", DbNullIfNull(BookingRefNo));
        cmd.Parameters.AddWithValue("@SupplierRefNo", DbNullIfNull(SupplierRefNo));
        cmd.Parameters.AddWithValue("@TicketNo", DbNullIfNull(TicketNo));
        cmd.Parameters.AddWithValue("@PassengerName", DbNullIfNull(PassengerName));
        cmd.Parameters.AddWithValue("@ddlBranch", DbNullIfNull(tempbranch));
        cmd.Parameters.AddWithValue("@ddlStatus", DbNullIfNull(ddlStatus));
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procSearchTransferbooking";
        return Dalref.ExeReader(cmd);
    }
    public DataTable GetCurrencyDetails()
    {
        cmd.Parameters.Clear();
        cmd.CommandType = CommandType.Text;
        cmd.CommandText = "SELECT ReteofExchange as RateofExchange,CurrencyCode FROM tblRateofExchange where status=1";
        return Dalref.ExeReader(cmd);
    }
}