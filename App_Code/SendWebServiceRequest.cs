﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for SendWebServiceRequest
/// </summary>
public class SendWebServiceRequest
{
    string userName = ConfigurationManager.AppSettings["AMuserName"];
    string password = ConfigurationManager.AppSettings["AMpassword"];
    string Currency = ConfigurationManager.AppSettings["AMCurrency"];
    BALBook BALAD = new BALBook();

    public SendWebServiceRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    [WebMethod]
    public string Flightsearch(string[,] Segments, string QproviderType, string QRefundableType, Int32 AdultPax, Int32 ChildPax, Int32 InfantPax, Int32 NumSegment, string cc, string direct, ref string sessionIDFinal)
    {
        string qversion = "0";
        //string NumberOfRecommendation = "50";
        //string qPassengerTypeQuantity ="ADT";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append("<soap:Header>");
        sb.Append("<AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("<WSUserName>" + userName + "</WSUserName>");
        sb.Append("<WSPassword>" + password + "</WSPassword>");
        sb.Append("<Accept-Encoding>gzip</Accept-Encoding>");
        sb.Append("</AuthenticationSoapHeader>");
        sb.Append("</soap:Header>");
        sb.Append("<soap:Body>");

        sb.Append("<SearchFlight xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        //  sb.Append("<OTA_AirLowFareSearchRQ ProviderType=" + '"' + QproviderType + '"' + " RefundableType=" + '"' + QRefundableType + '"' + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Version=" + '"' + qversion + '"' + ">");
        sb.Append("<OTA_AirLowFareSearchRQ ProviderType=" + '"' + QproviderType + '"' + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"  Version=" + '"' + qversion + '"' + " DirectFlightsOnly=" + '"' + direct + '"' + ">");

        // for (int i = 0; i < Segments.Length; i++)
        for (int i = 0; i < NumSegment; i++)
        {
            sb.Append("<OriginDestinationInformation>");
            sb.Append("<DepartureDateTime>" + Segments[i, 2] + "</DepartureDateTime>");
            sb.Append("<OriginLocation LocationCode=" + '"' + Segments[i, 0] + '"' + "/>");
            sb.Append("<DestinationLocation LocationCode=" + '"' + Segments[i, 1] + '"' + "/>");
            sb.Append("<RadiusInformation FromValue=" + '"' + "0" + '"' + " ToValue=" + '"' + "250" + '"' + " />");
            sb.Append("</OriginDestinationInformation>");
        }

        sb.Append("<TravelerInfoSummary>");
        sb.Append("<AirTravelerAvail>");
        for (int i = 0; i < AdultPax; i++)
        {
            sb.Append("<PassengerTypeQuantity Code=" + '"' + "ADT" + '"' + " />");
        }
        for (int i = 0; i < ChildPax; i++)
        {
            sb.Append("<PassengerTypeQuantity Code=" + '"' + "CHD" + '"' + " />");
        }
        for (int i = 0; i < InfantPax; i++)
        {
            sb.Append("<PassengerTypeQuantity Code=" + '"' + "INF" + '"' + " />");
        }
        sb.Append("</AirTravelerAvail>");
        sb.Append("</TravelerInfoSummary>");

        sb.Append("<AdvanceSearchInfo>");
        //sb.Append("<NumberOfRecommendation>" + NumberOfRecommendation + "</NumberOfRecommendation>");
        sb.Append("<Currency>" + Currency + "</Currency>");
        sb.Append("</AdvanceSearchInfo>");

        if (cc != "All")
        {
            sb.Append("<TravelPreferences>");
            sb.Append("<CabinPref Cabin=" + '"' + cc + '"' + "/>");
            sb.Append("</TravelPreferences>");
        }

        sb.Append("</OTA_AirLowFareSearchRQ>");
        sb.Append("</SearchFlight>");
        sb.Append("</soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/SearchFlight";
        string msgf = sb.ToString();
        //SessionId in order to use next request. 
        string SessionId = sessionIDFinal;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        sessionIDFinal = SessionId;
        CreateXMLDoc(sb.ToString(), SessionId, "SEARCH-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "SEARCH-RESPONSE");

        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xmlResponse);

        //Save to Json File
        //string json = JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented);
        //CreateJSONDoc(json, SessionId);

        return xmlResponse;
    }
    public static string SendRequest(string RequestXml, string SoapAction, ref string SessionId)
    {
        bool signout = false;
        if (SoapAction.IndexOf("SignOut") != -1)
            signout = true;

        WebRequest req = WebRequest.Create(ConfigurationManager.AppSettings["AMurl"]);

        req.Timeout = 600000;
        req.Headers.Add("SOAPAction", SoapAction);
        req.ContentType = "text/xml; charset=utf-8";
        req.Method = "POST";

        //we add the SessionId to headers if we received before.
        if (!String.IsNullOrEmpty(SessionId))
            req.Headers.Add("Cookie", SessionId);

        Stream reqStr = req.GetRequestStream();
        StreamWriter sw = new StreamWriter(reqStr);
        sw.Write(RequestXml);
        sw.Close();
        string strResult = "";

        try
        {
            WebResponse res = req.GetResponse();
            Stream resStr = res.GetResponseStream();
            StreamReader sr = new StreamReader(resStr);
            strResult = sr.ReadToEnd();

            sr.Close();

            //Receive the SessionId from Headers.
            if (!String.IsNullOrEmpty(res.Headers["Set-Cookie"]))
                SessionId = res.Headers["Set-Cookie"];

            res.Close();
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();

                errMessage += Environment.NewLine + soapResponse;
                strResult = errMessage;

                ex.Response.Close();
            }
        }
        if (signout)
            SessionId = "";
        return strResult;
    }
    public void CreateXMLDoc(string XMLDATA, string SessionId, string FileType)
    {
        XmlDocument empDoc = new XmlDocument();
        try
        {
            string trimmedSessionId = SessionId.Substring(SessionId.IndexOf('=') + 1);
            string finalsesID = trimmedSessionId.Split(';')[0];
            empDoc.LoadXml(XMLDATA);
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/XmlFiles/" + DateTime.Now.ToString("yyyyMMdd") + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string filename = FileType + "-" + finalsesID + "-" + DateTime.UtcNow.AddHours(4).ToString("HHmmss") + ".xml";
            string tempfilename = storePath + "/" + filename;
            empDoc.Save(tempfilename);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {
        }
    }
    public void CreateJSONDoc(string JSONDATA, string SessionId)
    {
        try
        {
            string trimmedSessionId = SessionId.Substring(SessionId.IndexOf('=') + 1);
            string finalsesID = trimmedSessionId.Split(';')[0];



            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/JsonFiles/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            string filename = finalsesID + ".json";
            string tempfilename = storePath + "/" + filename;
            File.WriteAllText(tempfilename, JSONDATA);
        }
        catch (XmlException xmlEx)
        {
            // Response.Write("XmlException: " + xmlEx.Message);
        }
        finally
        {

        }
    }
    [WebMethod]
    public string PingTest()
    {
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <Ping xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <OTA_PingRQ>");
        sb.Append("             <EchoData> Are you there shabeer</EchoData>");
        sb.Append("         </OTA_PingRQ>");
        sb.Append("     </Ping>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/Ping";

        //SessionId in order to use next request. 
        string SessionId = CreateSessionID();

        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        return xmlResponse;

    }
    public string CreateSessionID()
    {
        return Guid.NewGuid().ToString();
    }
    public string BookFlight(int PassTypeCount, List<string> PassTypeDataArray, List<string> PassQuantDataArray, List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> BirthDateArray, string AreaCityCode, string PhoneNumber, string txtEmail, List<string> PassportnoArray, List<string> DocIssueCountry, List<string> ExpireDateArray, string hdSEQNO, string hdCID, string Country, string txtCity, string txtZipCode, string txtAdd1, string txtAdd2, string SessionID, List<string> FrequentFlyerAirlineeArray, List<string> FrequentFlyerNumberrArray)
    {
        //Insert tblBooking 01
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.BOOKDATE = System.DateTime.Now;
        BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
        DataTable dtbook = BALAD.InsertNewBookingDetails();
        //Insert tblBooking

        //inserting data to tblAirPassengers 02
        int ArrayNameCount1 = 0;
        if (dtbook.Rows.Count > 0)
        {
            BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            string BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            BALAD.BookingStatusCode = "RQ";
            HttpContext.Current.Session["BookingRefID"] = BOOKINGREFID;
            int success = BALAD.InsertBookingHistoryDetails();

            for (int i = 0; i < PassTypeDataArray.Count; i++)
            {
                int PassQuantity = Convert.ToInt32(PassQuantDataArray[i]);
                for (int j = 0; j < PassQuantity; j++)
                {
                    BALAD.TITLE = NamePrefixArray[ArrayNameCount1].ToString();
                    BALAD.PAXID = GetPassengerTypeID(PassTypeDataArray[i]);
                    BALAD.FNAME = FNamesArray[ArrayNameCount1].ToString();
                    BALAD.SNAME = SurnameArray[ArrayNameCount1].ToString();
                    BALAD.EMAIL = txtEmail;
                    BALAD.TELEPHONE = PhoneNumber.ToString();
                    BALAD.LOCATIONCODE = AreaCityCode.ToString();
                    BALAD.DOB = Convert.ToDateTime(BirthDateArray[ArrayNameCount1].ToString());
                    DataTable dtpassenger = BALAD.InsertPassengerBookingDetails();
                    if (dtpassenger.Rows.Count > 0)
                    {
                        BALAD.PAXID = dtpassenger.Rows[0]["PaxID"].ToString();
                        BALAD.DOCTYPE = "1";
                        BALAD.DOCNO = PassportnoArray[ArrayNameCount1].ToString();
                        BALAD.DOCEXPIRYDATE = Convert.ToDateTime(ExpireDateArray[ArrayNameCount1].ToString());
                        BALAD.DOCISSUECUNTRY = DocIssueCountry[ArrayNameCount1].ToString();
                        DataTable dtdocs = BALAD.InsertPassengerDocsDetails();
                    }
                    ArrayNameCount1++;
                }
            }
        }
        //---------------inserting data to tblAirPassengers-----------------------\\

        //Book Request
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <BookFlight xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <OTA_AirBookRQ RecommendationID=" + '"' + hdSEQNO + '"' + " CombinationID=" + '"' + hdCID + '"' + ">");
        sb.Append("         	<POS />");
        sb.Append("         	  <TravelerInfo>");

        int ArrayNameCount = 0;
        for (int i = 0; i < PassTypeDataArray.Count; i++)
        {
            int PassQuantity = Convert.ToInt32(PassQuantDataArray[i]);
            for (int j = 0; j < PassQuantity; j++)
            {
                sb.Append("<AirTraveler PassengerTypeCode=" + '"' + PassTypeDataArray[i] + '"' + ">");
                sb.Append("<PersonName><GivenName>" + FNamesArray[ArrayNameCount] + "</GivenName><NamePrefix>" + GetNamePrefixText(NamePrefixArray[ArrayNameCount]) + "</NamePrefix><Surname>" + SurnameArray[ArrayNameCount] + "</Surname></PersonName>");
                sb.Append("<BirthDate>" + BirthDateArray[ArrayNameCount] + "</BirthDate> <Telephone AreaCityCode=" + '"' + AreaCityCode + '"' + " PhoneNumber=" + '"' + PhoneNumber + '"' + " /> <Email EmailType=\"1\">" + txtEmail + "</Email>");
                sb.Append("<Document DocType=\"Passport\" DocID=" + '"' + PassportnoArray[ArrayNameCount] + '"' + " DocIssueCountry =" + '"' + DocIssueCountry[ArrayNameCount] + '"' + " ExpireDate=" + '"' + ExpireDateArray[ArrayNameCount] + '"' + " />");
                if (FrequentFlyerNumberrArray[ArrayNameCount] != "" && FrequentFlyerAirlineeArray[ArrayNameCount] != "")
                {
                    sb.Append("<Document DocType=\"FrequentFlyerCard\" DocID=" + '"' + FrequentFlyerNumberrArray[ArrayNameCount] + '"' + " DocIssueAuthority =" + '"' + FrequentFlyerAirlineeArray[ArrayNameCount] + '"' + "/>");
                }
                sb.Append("</AirTraveler>");
                ArrayNameCount++;
            }
        }

        sb.Append("</TravelerInfo>");
        sb.Append("<Fulfillment>");
        sb.Append("<DeliveryAddress>");
        sb.Append("<AddressLine>" + '"' + txtAdd1 + '"' + "</AddressLine>");
        sb.Append("<AddressLine>" + '"' + txtAdd2 + '"' + "</AddressLine>");
        sb.Append("<CityName>" + '"' + txtCity + '"' + "</CityName>");
        sb.Append("<CountryName Code=" + '"' + Country + '"' + " />");
        sb.Append("<PostalCode>" + txtZipCode + "</PostalCode>");
        sb.Append("</DeliveryAddress>");

        sb.Append("<PaymentDetails>");
        sb.Append("<PaymentDetail PaymentType=\"None\">");

        sb.Append("<BillingAddress>");
        sb.Append("<AddressLine>" + '"' + txtAdd1 + '"' + "</AddressLine>");
        sb.Append("<AddressLine>" + '"' + txtAdd2 + '"' + "</AddressLine>");
        sb.Append("<CityName>" + '"' + txtCity + '"' + "</CityName>");
        sb.Append("<CountryName Code=" + '"' + Country + '"' + " />");
        sb.Append("<PostalCode>" + txtZipCode + "</PostalCode>");
        sb.Append("</BillingAddress>");

        sb.Append("</PaymentDetail>");
        sb.Append("</PaymentDetails>");

        sb.Append("<PaymentText Name=\"TripName\" Text=\"Payment text\" />");
        sb.Append("<PaymentText Name=\"Notes\" Text=\"Payment notes\" />");

        sb.Append("</Fulfillment>");

        sb.Append("<Ticketing TicketType=\"BookingOnly\"></Ticketing>");
        sb.Append("</OTA_AirBookRQ>");
        sb.Append(" </BookFlight>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/BookFlight";

        //SessionId in order to use next request. 
        // string SessionId = "ASP.NET_SessionId=4gxc5sqfud504qvyibqzco20; path=/; HttpOnly";
        string SessionId = SessionID;
        // string SessionId = "ASP.NET_SessionId=kzgaihxqydfrkrt1kbkvcqp2; path=/; HttpOnly";
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);

        CreateXMLDoc(sb.ToString(), SessionId, "BOOKFLIGHT-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "BOOKFLIGHT-RESPONSE");

        //string xmlResponse = "sdf";
        return xmlResponse;
    }
    private string GetNamePrefixText(string prefValue)
    {
        string FinalPref = "";
        if (prefValue == "1")
        {
            FinalPref = "MR";
        }
        else if (prefValue == "3")
        {
            FinalPref = "MS";
        }
        else if (prefValue == "2")
        {
            FinalPref = "MRS";
        }
        else if (prefValue == "4")
        {
            FinalPref = "MISS";
        }
        else if (prefValue == "5")
        {
            FinalPref = "MSTR";
        }
        else if (prefValue == "6")
        {
            FinalPref = "DR";
        }
        else if (prefValue == "7")
        {
            FinalPref = "HE";
        }
        else if (prefValue == "8")
        {
            FinalPref = "HH";
        }
        return FinalPref;
    }
    public string Signout(string SessionId)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><SignOut xmlns=\"http://epowerv5.amadeus.com.tr/WS\" /></soap:Body></soap:Envelope>");
        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/SignOut";
        string signoutResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        return signoutResponse;
    }
    public string BookREquestPriceChange(string referenceno, string sha1, string SessionID)
    {
        //Book Request
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <BookFlight xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <OTA_AirBookRQ ReferenceNumber=" + '"' + referenceno + '"' + " ControlNumber=" + '"' + sha1 + '"' + ">");
        sb.Append("          </OTA_AirBookRQ>");
        sb.Append("          </BookFlight>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/BookFlight";

        //SessionId in order to use next request. 
        // string SessionId = "ASP.NET_SessionId=4gxc5sqfud504qvyibqzco20; path=/; HttpOnly";
        string SessionId = SessionID;
        //string SessionId = "ASP.NET_SessionId=kzgaihxqydfrkrt1kbkvcqp2; path=/; HttpOnly";
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "BOOK-REQUEST-PRICECHANGE");
        CreateXMLDoc(xmlResponse, SessionId, "BOOK-RESPONSE-PRICECHANGE");
        return xmlResponse;
    }
    public string FlightRules(string RecommendationID, string PassengerType, string CombinationID, string SessionID)
    {
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <GetFlightRules xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        //sb.Append("            <OTA_AirRulesRQ RecommendationID=" + '"' + RecommendationID + '"' + " PassengerType=" + '"' + PassengerType + '"' + " CombinationID=" + '"' + CombinationID + '"' + ">");
        sb.Append("            <OTA_AirRulesRQ RecommendationID=" + '"' + RecommendationID + '"' + " PassengerType=" + '"' + PassengerType + '"' + " CombinationID=" + '"' + CombinationID + '"' + " MiniRuleEnabled=\"1\" PriceMessageEnabled=\"1\" FlightRuleEnabled=\"1\" > ");
        sb.Append("          </OTA_AirRulesRQ>");
        sb.Append("          </GetFlightRules>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/GetFlightRules";

        //SessionId in order to use next request. 

        string SessionId = SessionID;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "FARERULES-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "FARERULES-RESPONSE");
        return xmlResponse;
    }
    public string BaggageInfo(string RecommendationID, string CombinationID, string SessionID)
    {
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");

        sb.Append("     <GetBaggageInfo xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <BaggageInfoRQ RecommendationID=" + '"' + RecommendationID + '"' + " CombinationID=" + '"' + CombinationID + '"' + ">");
        sb.Append("     <Passenger PassengerType =" + '"' + "ADT" + '"' + " PassengerIndex =" + '"' + "1" + '"' + ">");
        sb.Append("          </Passenger>");
        sb.Append("     <Passenger PassengerType =" + '"' + "CHD" + '"' + " PassengerIndex =" + '"' + "2" + '"' + ">");
        sb.Append("          </Passenger>");
        sb.Append("     <Passenger PassengerType =" + '"' + "INF" + '"' + " PassengerIndex =" + '"' + "3" + '"' + ">");
        sb.Append("          </Passenger>");
        sb.Append("          </BaggageInfoRQ>");
        sb.Append("          </GetBaggageInfo>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/GetBaggageInfo";

        //SessionId in order to use next request. 

        string SessionId = SessionID;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "BAGGAGE-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "BAGGAGE-RESPONSE");
        return xmlResponse;
    }
    public string CreateTicketResponse(int PassTypeCount, List<string> PassTypeDataArray, List<string> PassQuantDataArray, List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> BirthDateArray, List<string> PassportnoArray, List<string> DocIssueCountry, List<string> ExpireDateArray, string hdpnr, string SessionID)
    {
        //Book Request
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <CreateTicket xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <OTA_AirBookRQ>");
        sb.Append("          <BookingReferenceID ID_Context=" + '"' + hdpnr + '"' + "/>");
        sb.Append("            <TravelerInfo>");




        int ArrayNameCount = 0;
        for (int i = 0; i < PassTypeDataArray.Count; i++)
        {
            int PassQuantity = Convert.ToInt32(PassQuantDataArray[i]);
            for (int j = 0; j < PassQuantity; j++)
            {
                sb.Append("<AirTraveler PassengerTypeCode=" + '"' + PassTypeDataArray[i] + '"' + ">");
                sb.Append("<PersonName><GivenName>" + '"' + FNamesArray[ArrayNameCount] + '"' + "</GivenName><NamePrefix>" + '"' + GetNamePrefixText(NamePrefixArray[ArrayNameCount]) + '"' + "</NamePrefix><Surname>" + '"' + SurnameArray[ArrayNameCount] + '"' + "</Surname></PersonName>");
                sb.Append("<BirthDate>" + BirthDateArray[ArrayNameCount] + "</BirthDate>");
                sb.Append("<Document DocType=\"Passport\" DocID=" + '"' + PassportnoArray[ArrayNameCount] + '"' + " DocIssueCountry =" + '"' + DocIssueCountry[ArrayNameCount] + '"' + " ExpireDate=" + '"' + ExpireDateArray[ArrayNameCount] + '"' + " />");
                sb.Append("</AirTraveler>");
                ArrayNameCount++;
            }
        }
        sb.Append("</TravelerInfo>");
        sb.Append("</OTA_AirBookRQ>");
        sb.Append(" </CreateTicket>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/CreateTicket";

        //SessionId in order to use next request. 
        // string SessionId = "ASP.NET_SessionId=4gxc5sqfud504qvyibqzco20; path=/; HttpOnly";
        string SessionId = SessionID;
        // string SessionId = "ASP.NET_SessionId=kzgaihxqydfrkrt1kbkvcqp2; path=/; HttpOnly";

        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "TICKETISSUE-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "TICKETISSUE-RESPONSE");

        // string xmlResponse = sb.ToString();
        return xmlResponse;
    }
    public string CreateTicketResponseNew(int PassTypeCount, List<string> PassTypeDataArray, List<string> PassQuantDataArray, List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> BirthDateArray, List<string> PassportnoArray, List<string> DocIssueCountry, List<string> ExpireDateArray, string hdpnr, string SessionID)
    {
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <CreateTicket xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <OTA_AirBookRQ>");
        sb.Append("          <BookingReferenceID ID_Context=" + '"' + hdpnr + '"' + "/>");
        sb.Append("            <TravelerInfo>");

        int PassQuantity = Convert.ToInt32(PassTypeCount);
        for (int j = 0; j < PassQuantity; j++)
        {
            sb.Append("<AirTraveler PassengerTypeCode=" + '"' + PassTypeDataArray[j] + '"' + ">");
            sb.Append("<PersonName><GivenName>" + FNamesArray[j] + "</GivenName><NamePrefix>" + GetNamePrefixText(NamePrefixArray[j]) + "</NamePrefix><Surname>" + SurnameArray[j] + "</Surname></PersonName>");
            sb.Append("<BirthDate>" + BirthDateArray[j] + "</BirthDate>");
            sb.Append("<Document DocType=\"Passport\" DocID=" + '"' + PassportnoArray[j] + '"' + " DocIssueCountry =" + '"' + DocIssueCountry[j] + '"' + " ExpireDate=" + '"' + ExpireDateArray[j] + '"' + " />");
            sb.Append("</AirTraveler>");
        }

        sb.Append("</TravelerInfo>");
        sb.Append("</OTA_AirBookRQ>");
        sb.Append(" </CreateTicket>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/CreateTicket";
        string SessionId = SessionID;
        CreateXMLDoc(sb.ToString(), SessionId, "TICKETISSUE-REQUEST");
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(xmlResponse, SessionId, "TICKETISSUE-RESPONSE");

        return xmlResponse;
    }
    public string Checkmytrip(string pnr, string surname, string SessionID)
    {
        //string userName = "wswhitesands";
        //string password = "WhiteSands@epws07";
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");

        sb.Append("     <GetPNR  xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("            <OTA_ReadRQ>");
        sb.Append("     <ReadRequests>");
        sb.Append("          <ReadRequest>");
        sb.Append("     <BookingReferenceID Type=\"Flight\" ID_Context =" + '"' + pnr + '"' + " />");
        sb.Append("          <Verification>");
        sb.Append("     <PersonName>");
        sb.Append("<Surname>" + surname + "</Surname>");
        sb.Append("          </PersonName>");
        sb.Append("          </Verification>");
        sb.Append("          </ReadRequest>");
        sb.Append("          </ReadRequests>");
        sb.Append("          </OTA_ReadRQ>");
        sb.Append("          </GetPNR>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/GetPNR";

        //SessionId in order to use next request. 

        string SessionId = SessionID;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        return xmlResponse;
    }

    private string GetPassengerTypeID(string Value)
    {
        string FinalValue = "";
        if (Value == "ADT")
        {
            FinalValue = "1";
        }
        else if (Value == "CHD")
        {
            FinalValue = "2";
        }
        else if (Value == "INF")
        {
            FinalValue = "3";
        }
        return FinalValue;
    }

    public string CancelBooking(string PNRNO, string Surname, string SessionID)
    {
        //Book Request
        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <Cancel xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("<OTA_CancelRQ xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" Version=\"0\" CancelType=\"Initiate\">");
        sb.Append("            <BookingReferenceID Type=\"F\" ID_Context=" + '"' + PNRNO + '"' + " />");
        sb.Append("          <Verification>");
        sb.Append("          <PersonName>");
        sb.Append("         <Surname>" + Surname + "</Surname>");
        sb.Append("             </PersonName>");
        sb.Append("             </Verification>");
        sb.Append("              </OTA_CancelRQ>");
        sb.Append("          </Cancel>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");
        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/Cancel";
        string SessionId = SessionID;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "CANCEL_BOOKING-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "CANCEL_BOOKING-RESPONSE");
        return xmlResponse;
    }
    public string GetLastticketingDate(string PNRC, string SessionID)
    {

        //Create a request to send the e-Power WS
        StringBuilder sb = new StringBuilder();
        sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
        sb.Append(" <soap:Header>");
        sb.Append("     <AuthenticationSoapHeader xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <WSUserName>" + userName + "</WSUserName>");
        sb.Append("         <WSPassword>" + password + "</WSPassword>");
        sb.Append("     </AuthenticationSoapHeader>");
        sb.Append(" </soap:Header>");
        sb.Append(" <soap:Body>");
        sb.Append("     <GetLastTicketingDate  xmlns=\"http://epowerv5.amadeus.com.tr/WS\">");
        sb.Append("         <PnrNo>" + PNRC + "</PnrNo>");
        sb.Append("          </GetLastTicketingDate>");
        sb.Append(" </soap:Body>");
        sb.Append("</soap:Envelope>");

        string SoapAction = "http://epowerv5.amadeus.com.tr/WS/GetLastTicketingDate";

        //SessionId in order to use next request. 

        string SessionId = SessionID;
        string xmlResponse = SendRequest(sb.ToString(), SoapAction, ref SessionId);
        CreateXMLDoc(sb.ToString(), SessionId, "LAST_TKT-REQUEST");
        CreateXMLDoc(xmlResponse, SessionId, "LAST_TKT-RESPONSE");
        return xmlResponse;
    }
}