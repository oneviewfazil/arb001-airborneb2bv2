﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebMethodDbHotel : System.Web.UI.Page
{
    public static string HubURL = ConfigurationManager.AppSettings["HubURL"];
    public static string HubPort = ConfigurationManager.AppSettings["HubPort"];
    public static string Bookingmailpath = ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingmail.html";
    public static string Bookingpendingpath = ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingpendingsuppliermail.html";
    public static string Bookingcronjob = ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingmailcronjob.html";
    public static string Bookingmailpathbookingupdation = ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingmailupdation.html";
    public static string dbCurrency = "";
    public static string RateOfExchange = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetSessionData(string sesname)
    {
        Int64 UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]); ;
        GetUserData((UID));
        string SessionData = string.Empty;
        if (HttpContext.Current.Session[sesname] != null)
        {
            SessionData = HttpContext.Current.Session[sesname].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }
        return SessionData;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetUserData(Int64 UID)
    {
        BALSettings bals = new BALSettings();
        // bals.UserID = 50;
        bals.UserID = UID;
        DataTable dt = bals.GetUserData();
        if (dt.Rows.Count > 0)
        {
            HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
            Int64 UIDD = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);

        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelInformation(string ClinentID, string ApiID, DateTime CheckinDate, DateTime CheckOut, string StatusCode, string Commi, string Currency, string Gross, string Net, string TotalPrice, string MarkupValue, string MarkupTypeID)
    {

        dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
        RateOfExchange = HttpContext.Current.Session["dbRateOfExchange"].ToString();
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();

        // HttpContext.Current.Session["UserID"] = "50";
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        // BALAD.BOOKINGREFID = "00001239";
        BALAD.ClientID = ClinentID;
        BALAD.ApiID = ApiID;
        BALAD.CheckInDate = CheckinDate;
        BALAD.CheckOut = CheckOut;
        BALAD.StatusCode = StatusCode.Trim();
        DataTable dthotelinfo = BALAD.inserthotelinformation();
        if (dthotelinfo.Rows.Count > 0)
        {
            Out = dthotelinfo.Rows[0]["HotelInformationID"].ToString();
            HotelBookingPriceDetails(Out, Commi, Currency, Gross, Net, MarkupValue, MarkupTypeID);

        }
        else
        {
            Out = "NOINFO";
        }
        if (StatusCode == "C")
        {
            string status = "HK";
            UpdateSupplierStatus(status, ApiID);
            ChangeStatustoHK(status);

        }
        else if (StatusCode == "P" || StatusCode == "CP" || StatusCode == "CP")
        {
            string status = "RR";
            UpdateSupplierStatus(status, ApiID);
            ChangeStatustoHK(status);
        }
        else if (StatusCode == "RJ" || StatusCode == "ER" || StatusCode == "X ")
        {
            string status = "XX";
            UpdateSupplierStatus(status, ApiID);
            ChangeStatustoHK(status);

        }
        InsertTblPayment(TotalPrice, dbCurrency, 1);
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelRoomsInformation(string result, string ItemRef, string CityCode, string ItemCode, string HotelName, string StatusCode, string ItemReferenceCode, DateTime CheckinDate, DateTime CheckOut, string Noadult, string NoChild, string Cot, string RoomCatID, string RoomCat, string Brkfast, string Full, string Sharebedding, List<string> PaxDatA, string Type, string Allowble, string PassTypeAllow, string PassType, List<string> CancellData, string Commisssion, string ItemCurrency, string ItemGross, string ItemNet, string MarkupValue, string MarkupTypeID, string HotelAddress, List<string> EssnInfo, string numroom)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        BALAD.HotelInformationID = result;
        BALAD.ItemReference = ItemRef;
        BALAD.ItemCityCode = CityCode;
        BALAD.ItemCode = ItemCode;
        BALAD.ItemName = HotelName;
        BALAD.ItemStatusCode = StatusCode;
        BALAD.CheckInDate = CheckinDate;
        BALAD.CheckOut = CheckOut;
        BALAD.ItemConfirmationReference = ItemReferenceCode;
        BALAD.HotelPaxRoomAdults = Noadult;
        BALAD.HotelPaxRoomChildren = NoChild;
        BALAD.HotelPaxRoomCots = Cot;
        BALAD.SupplierRoomCategoryID = RoomCatID;
        BALAD.RoomCategory = RoomCat;
        BALAD.MealBasis = Brkfast + ',' + Full;
        BALAD.SharingBedding = Sharebedding;
        BALAD.PAXRef = string.Join(",", PaxDatA);
        BALAD.Address = HotelAddress;
        DataTable dthotelRoominfo = BALAD.insertBookingHotelRoomsInformation();
        if (dthotelRoominfo.Rows.Count > 0)
        {
            Out = dthotelRoominfo.Rows[0]["BookingRoomID"].ToString();
            BookingHotelRoomAmendmentStatus(Out, Type, Allowble, PassTypeAllow, PassType);
            BookingHotelRoomCancellationCharges(Out, CancellData);
            BookingEssentialinfo(Out, EssnInfo);
            BookingHotelRoomPriceBreakuproom(Out, Commisssion, ItemCurrency, ItemGross, ItemNet, MarkupValue, MarkupTypeID, numroom);

        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string HotelPax(string result, string PID, string PTitle, string PFName, string PLName, string PaxTypeAge)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.HotelInformationIDD = Convert.ToInt64(result);
        BALAD.PAXReff = Convert.ToInt16(PID);
        string title = GetTitle(PTitle);
        BALAD.PaxTitle = Convert.ToInt16(title);
        BALAD.PaxFirstName = PFName;
        BALAD.PaxLastName = PLName;
        BALAD.PaxTypeAge = PaxTypeAge;
        DataTable dthotelPAXinfo = BALAD.insertHotelPaxInformation();
        if (dthotelPAXinfo.Rows.Count > 0)
        {
            Out = dthotelPAXinfo.Rows[0]["PaxID"].ToString();
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    public static string GetTitle(string Title)
    {
        string Out = string.Empty;
        if (Title == "Mr")
        {
            Out = "1";
        }
        else if (Title == "Miss")
        {
            Out = "2";
        }
        else if (Title == "Mrs")
        {
            Out = "3";
        }
        else if (Title == "Ms")
        {
            Out = "4";
        }
        else if (Title == "Master")
        {
            Out = "5";
        }

        return Out;
    }

    public static string BookingHotelRoomAmendmentStatus(string HID, string Type, string Allowble, string PassTypeAllow, string PassType)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(HID);
        BALAD.AmendmentType = Type;
        string Allow = GetAllow(Allowble);
        BALAD.Allowable = Convert.ToInt16(Allow);
        DataTable dthotelAmendmentinfo = BALAD.insertHotelRoomAmendmentStatus();
        BALAD.AmendmentType = PassType;
        string Allow2 = GetAllow(PassTypeAllow);
        BALAD.Allowable = Convert.ToInt16(Allow2);
        DataTable dthotelAmendmentinfo2 = BALAD.insertHotelRoomAmendmentStatus();
        return Out;
    }
    public static string BookingEssentialinfo(string RoomID, List<string> EssnInfo)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(RoomID);
        for (int j = 0; j < EssnInfo.Count; j++)
        {
            string strVale = EssnInfo[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.FromDate = Convert.ToDateTime(arr[1].ToString());
            BALAD.ToDate = Convert.ToDateTime(arr[2].ToString());
            BALAD.EssInfo = arr[0].ToString();
            DataTable SegmentID = BALAD.insertBookingEssentialInformation();
        }

        return Out;
    }
    public static string GetAllow(string Allow)
    {
        string Out = string.Empty;
        if (Allow == "true")
        {
            Out = "1";
        }
        else if (Allow == "false")
        {
            Out = "0";
        }
        return Out;
    }

    public static string BookingHotelRoomCancellationCharges(string RoomID, List<string> CancellData)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(RoomID);
        for (int j = 0; j < CancellData.Count; j++)
        {
            string strVale = CancellData[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.FromDate = DateTime.ParseExact(arr[1].ToString(), "dd-MM-yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
            // BALAD.FromDate = Convert.ToDateTime(arr[1].ToString());
            BALAD.ToDate = DateTime.ParseExact(arr[0].ToString(), "dd-MM-yyyy hh:mm:ss tt", CultureInfo.InvariantCulture); ;
            //BALAD.ToDate = Convert.ToDateTime(arr[0].ToString());
            string val = GetAllow(arr[2].ToString());
            BALAD.Chrage = Convert.ToInt16(val);
            BALAD.ChargeAmount = Convert.ToDouble(arr[3].ToString());
            BALAD.Currency = arr[4].ToString();
            DataTable SegmentID = BALAD.insertBookingHotelRoomCancellationCharges();
        }

        return Out;
    }
    public static string HotelBookingPriceDetails(string HID, string Commi, string Currency, string Gross, string Net, string MarkupValue, string MarkupTypeID)
    {
        RateOfExchange = HttpContext.Current.Session["dbRateOfExchange"].ToString();
        dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(HID);
        BALAD.GrossValue = Convert.ToDouble(Gross);
        BALAD.GrossCurrency = Currency;
        BALAD.NetValue = Convert.ToDouble(Net);
        BALAD.Commission = Convert.ToDouble(Commi);
        BALAD.Discount = Convert.ToDouble(0);
        BALAD.MarkUpType = Convert.ToInt64(MarkupTypeID);
        BALAD.MarkUpValue = Convert.ToDouble(MarkupValue);
        BALAD.MarkUpCurrency = dbCurrency;
        BALAD.SellingCurrency = dbCurrency;
        BALAD.PriceForClient = (BALAD.GrossValue + BALAD.Commission + BALAD.MarkUpValue) - BALAD.Discount;
        BALAD.AdditionalServiceFeebyClient = Convert.ToDouble(0);
        BALAD.TotalSellAmount = (BALAD.GrossValue + BALAD.Commission + BALAD.MarkUpValue + BALAD.AdditionalServiceFeebyClient) - BALAD.Discount;
        BALAD.RateOfExchange = Convert.ToDouble(RateOfExchange);
        DataTable dtHotelBookingPriceDetails = BALAD.insertHotelBookingPriceDetails();
        return Out;
    }
    public static string BookingHotelRoomPriceBreakuproom(string RID, string Commisssion, string ItemCurrency, string ItemGross, string ItemNet, string MarkupValue, string MarkupTypeID, string rooms)
    {
        dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(RID);
        BALAD.GrossValue = Convert.ToDouble(ItemGross);
        BALAD.GrossCurrency = ItemCurrency;
        BALAD.NetValue = Convert.ToDouble(ItemNet);
        BALAD.Commission = Convert.ToDouble(Commisssion);
        BALAD.Discount = Convert.ToDouble(0);
        BALAD.MarkUpType = Convert.ToInt64(MarkupTypeID);
        if (BALAD.MarkUpType == 1)
        {
            Double MarkValue = Convert.ToDouble(MarkupValue);
            Double Sellamount = BALAD.GrossValue * (MarkValue / 100);
            Double Sellamounttotal = BALAD.GrossValue + Sellamount;
            BALAD.PriceForClient = (Sellamounttotal + BALAD.Commission) - BALAD.Discount;
        }
        if (BALAD.MarkUpType == 2)
        {
            Double MarkValue = Convert.ToDouble(MarkupValue);
            Double Sellamount = (Convert.ToDouble(MarkValue) / Convert.ToDouble(rooms));
            Double Sellamounttotal = BALAD.GrossValue + Sellamount;
            BALAD.PriceForClient = (Sellamounttotal + BALAD.Commission) - BALAD.Discount;
        }
        BALAD.MarkUpValue = Convert.ToDouble(MarkupValue);
        BALAD.MarkUpCurrency = dbCurrency;
        BALAD.SellingCurrency = dbCurrency;

        BALAD.AdditionalServiceFeebyClient = Convert.ToDouble(0);
        BALAD.TotalSellAmount = (BALAD.PriceForClient + BALAD.AdditionalServiceFeebyClient);
        DataTable dtHotelBookingPriceDetailsbrkup = BALAD.insertBookingHotelRoomPriceBreakup();
        return Out;
    }
    public static string BookingHotelRoomPriceBreakup(string RID, string Commisssion, string ItemCurrency, string ItemGross, string ItemNet, string MarkupValue, string MarkupTypeID)
    {
        dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(RID);
        BALAD.GrossValue = Convert.ToDouble(ItemGross);
        BALAD.GrossCurrency = ItemCurrency;
        BALAD.NetValue = Convert.ToDouble(ItemNet);
        BALAD.Commission = Convert.ToDouble(Commisssion);
        BALAD.Discount = Convert.ToDouble(0);
        BALAD.MarkUpType = Convert.ToInt64(MarkupTypeID);
        if (BALAD.MarkUpType == 1)
        {
            Double MarkValue = Convert.ToDouble(MarkupValue);
            Double Sellamount = BALAD.GrossValue * (MarkValue / 100);
            Double Sellamounttotal = BALAD.GrossValue + Sellamount;
            BALAD.PriceForClient = (Sellamounttotal + BALAD.Commission) - BALAD.Discount;
        }
        if (BALAD.MarkUpType == 2)
        {
            Double MarkValue = Convert.ToDouble(MarkupValue);
            Double Sellamount = BALAD.GrossValue + MarkValue;
            Double Sellamounttotal = BALAD.GrossValue + Sellamount;
            BALAD.PriceForClient = (Sellamounttotal + BALAD.Commission) - BALAD.Discount;
        }
        BALAD.MarkUpValue = Convert.ToDouble(MarkupValue);
        BALAD.MarkUpCurrency = dbCurrency;
        BALAD.SellingCurrency = dbCurrency;

        BALAD.AdditionalServiceFeebyClient = Convert.ToDouble(0);
        BALAD.TotalSellAmount = (BALAD.PriceForClient + BALAD.AdditionalServiceFeebyClient);
        DataTable dtHotelBookingPriceDetailsbrkup = BALAD.insertBookingHotelRoomPriceBreakup();
        return Out;
    }
    public static string ChangeStatustoHK(string status)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            BALAD.BookingStatusCode = status;
            BALAD.BOOKDATE = System.DateTime.Now;
            BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
            //  BALAD.UID = 50;
            int successss = BALAD.InsertBookingHistoryDetails();
            //SignOut();
        }
        return sucess;
    }

    public static string UpdateSupplierStatus(string status, string ApiID)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            BALAD.PNR = ApiID;
            BALAD.StatusCode = status;
            int Success = BALAD.upadteSupplierBooking();
        }
        return sucess;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotelinfo()
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
        DataTable dt = BALAD.GetBookInfoDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookPassengersHotelinfo()
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
        DataTable dt = BALAD.GetPassengerBookInfoDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelRoomPaxBrkup(string ItemRef)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.PAXRef = Convert.ToInt64(ItemRef).ToString();
        DataTable dt = BALAD.GetPassengerBrkup();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetFarebrkup(string hotelId)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.PAXRef = Convert.ToInt64(hotelId).ToString();
        DataTable dt = BALAD.GetfareBrkup();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Getcancellationpolicy(string hotelId)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.PAXRef = Convert.ToInt64(hotelId).ToString();
        DataTable dt = BALAD.Getcancellationpolicy();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetHotelBookingHistory(string BookingID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.PAXRef = Convert.ToInt64(BookingID).ToString();
        DataTable dt = BALAD.GetHotelBookingHistory();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetSearchDataHotel(string CityName, string HotelName, string BookingDateFrom, string BookingDateTo, string CheckinDateFrom, string CheckinDateTo, string BookingRefNo, string SupplierRefNo, string ddlBranch, string ddlStatus)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.City = CityName;
        BALAD.Hotel = HotelName;
        BALAD.BookingDateFrom = BookingDateFrom;
        BALAD.BookingDateTo = BookingDateTo;
        BALAD.CheckinDateFrom = CheckinDateFrom;
        BALAD.CheckinDateTo = CheckinDateTo;
        BALAD.BookingRefNo = BookingRefNo;
        BALAD.SupplierRefNo = SupplierRefNo;
        BALAD.ddlBranch = Convert.ToInt64(ddlBranch);
        BALAD.ddlStatus = ddlStatus;
        DataTable dtroles = BALAD.GetHotelSearchData();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string SendMailBookhotel(string EUName, string ETripID, string EPaxData, string UserMailID, string ETripName, string ETotalCharge, string bstatus)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString(Bookingmailpath);
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EUName#", EUName);
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                EmailBody = EmailBody.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EPaxData#", EPaxData);
                EmailBody = EmailBody.Replace("#TRIPNAME#", ETripName);
                EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Information");
                EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Information");
                EmailBody = EmailBody.Replace("#STATUSBOOK#", bstatus);
                EmailBody = EmailBody.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
                EmailBody = EmailBody.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
                EmailBody = EmailBody.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
                EmailBody = EmailBody.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyNumber"]);

                Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string SendMailCancelbooking(string EUName, string ETripID, string EPaxData, string UserMailID, string ETripName, string ETotalCharge, string bstatus)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString(Bookingmailpath);
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EUName#", EUName);
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                EmailBody = EmailBody.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EPaxData#", EPaxData);
                EmailBody = EmailBody.Replace("#TRIPNAME#", ETripName);
                EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                EmailSub = EmailSub.Replace("#SUBTEXT#", "Cancel Booking");
                EmailBody = EmailBody.Replace("#MAILHEADER#", "Cancel Booking");
                EmailBody = EmailBody.Replace("#STATUSBOOK#", bstatus);
                EmailBody = EmailBody.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
                EmailBody = EmailBody.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
                EmailBody = EmailBody.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
                EmailBody = EmailBody.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyNumber"]);
                Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }

    public static string InsertTblPayment(string PaidAmount, string CurrencyCode, Int32 PaymentTypeID)
    {
        BALBook BALAD = new BALBook();
        BALAD.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["HotelBookingRefID"]);
        BALAD.PaidAmount = PaidAmount;
        BALAD.CurrencyCode = CurrencyCode;
        BALAD.PaymentTypeID = PaymentTypeID;
        int AirSegment = BALAD.InsertTblPayment();
        return AirSegment.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelCancel(string APIid)
    {
        string status = "XR";
        ChangeStatustoHK(status);
        ChangecancellStatus(status);
        SendHotelWebService hws = new SendHotelWebService();
        return hws.BookHotelCancel(APIid);

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Changehotelbookingstatus(string Statuscancel)
    {
        string status = string.Empty;
        if (Statuscancel == "C ")
        {
            status = "HK";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }
        else if (Statuscancel == "P " || Statuscancel == "CP ")
        {
            status = "RR";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);
        }
        else if (Statuscancel == "X ")
        {
            status = "XX";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }
        else if (Statuscancel == "RC")
        {
            status = "RC";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }

        return status;
    }
    public static string ChangecancellStatus(string status)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            BALAD.StatusCode = status;
            int Success = BALAD.upadteBookingCancellationstatus();
        }
        return sucess;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Changehotelbookingstatusgetcanceldata(string Statuscancellation)
    {
        string status = string.Empty;
        if (Statuscancellation == "C ")
        {
            status = "HK";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }
        else if (Statuscancellation == "P " || Statuscancellation == "CP")
        {
            status = "RR";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);
        }
        else if (Statuscancellation == "X ")
        {
            status = "XX";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }
        else if (Statuscancellation == "RC")
        {
            status = "RC";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);

        }
        else if (Statuscancellation == "XP")
        {

            status = "XR";
            ChangeStatustoHK(status);
            ChangecancellStatus(status);
        }
        BALHotelBook BALAD = new BALHotelBook();

        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            DataTable dtprice = BALAD.GetCancelBookingPriceDetails();
            if (dtprice.Rows.Count > 0)
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dtprice.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtprice.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                status = serializer.Serialize(rows);
            }
            else
            {
                status = "NOINFO";
            }
        }
        return status;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string InsertCancelBookingPriceDetails(string cancelAmount, string CancelCurrency, string CustomerAmount, string dbcancellationcharge)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            BALAD.CancelAmount = Convert.ToDouble(cancelAmount);
            BALAD.CancelCurrency = CancelCurrency;
            BALAD.CustomerAmount = Convert.ToDouble(CustomerAmount);
            BALAD.Currency = dbCurrency;
            BALAD.Dbcancellamount = Convert.ToDouble(dbcancellationcharge);
            DataTable dtprice = BALAD.InsertCancelBookingPriceDetails();
            if (dtprice.Rows.Count > 0)
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dtprice.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtprice.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                sucess = serializer.Serialize(rows);
            }
            else
            {
                sucess = "NOINFO";
            }
        }
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelUpdate(string APIid)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.BookingHotelUpdate(APIid);

    }

    [System.Web.Services.WebMethod]
    public static string VoucherBookhotel(string AgencyName, string AgencyAddress, string AgencyContact, string gettoday, string ETripName, string HotelAddress, string ETripID, string voucherbookingdate, string APIid, string chin, string chout, string VoucherPax, string GtaAotNumber, string grandtotal)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        //DataTable dtCheck = BALAD.CheckVochurlinkavailable();
        //if (dtCheck.Rows.Count <= 0)
        //{
        using (WebClient client = new WebClient())
        {

            string ticket_attachment = client.DownloadString(ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingvochur.html");

            ticket_attachment = ticket_attachment.Replace("#AgencyName#", AgencyName);
            ticket_attachment = ticket_attachment.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
            ticket_attachment = ticket_attachment.Replace("#AgencyAddress#", AgencyAddress);
            ticket_attachment = ticket_attachment.Replace("#Contact#", AgencyContact);
            ticket_attachment = ticket_attachment.Replace("#Voucherissuedate#", gettoday);
            ticket_attachment = ticket_attachment.Replace("#GTARef#", APIid);
            ticket_attachment = ticket_attachment.Replace("#HotelName#", ETripName);
            ticket_attachment = ticket_attachment.Replace("#bookingdate#", voucherbookingdate);
            ticket_attachment = ticket_attachment.Replace("#BookingRef#", ETripID);
            ticket_attachment = ticket_attachment.Replace("#Address#", HotelAddress);
            ticket_attachment = ticket_attachment.Replace("#Checkin#", chin);
            ticket_attachment = ticket_attachment.Replace("#Checkout#", chout);
            ticket_attachment = ticket_attachment.Replace("#paxdetails#", VoucherPax);
            ticket_attachment = ticket_attachment.Replace("#GtaAotNumber#", GtaAotNumber);
            ticket_attachment = ticket_attachment.Replace("#Amount#", grandtotal);
            ticket_attachment = ticket_attachment.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
            ticket_attachment = ticket_attachment.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyLoc#", ConfigurationManager.AppSettings["AgencyNumber"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyLoc"]);

            string ticks = DateTime.Now.Ticks.ToString();
            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/BookingVoucher/" + ETripID;
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);

            TextWriter attachhtml = new StreamWriter(storePath + "/Booking_voucher_" + ETripID + "_" + ticks + ".html");
            attachhtml.WriteLine(ticket_attachment);
            attachhtml.Flush();
            attachhtml.Close();
            Out = "Documents/BookingVoucher/" + ETripID + "/Booking_voucher_" + ETripID + "_" + ticks + ".html";
            // Out = "http://karnakb2cdemo.oneviewitsolutions.com/Documents/BookingVoucher/" + ETripID + "/Booking_vochuer_" + ETripID + "_" + ticks + ".html";

            //BALAD.VoucherUrl = Out;
            //string host = HttpContext.Current.Request.Url.Host.ToLower();
            //if (host != "localhost")
            //{
            //    int dtVoucher = BALAD.insertVoucherUrlinformation();
            //}
        }

        //}
        //else
        //{
        //    string VoucherUrl = dtCheck.Rows[0]["VoucherUrl"].ToString();
        //    Out = VoucherUrl;
        //}
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string EmailVoucherBookhotel(string AgencyName, string AgencyAddress, string AgencyContact, string gettoday, string ETripName, string HotelAddress, string ETripID, string voucherbookingdate, string APIid, string chin, string chout, string VoucherPax, string Email, string mailname, string GtaAotNumber, string grandtotal)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        // DataTable dtCheck = BALAD.CheckVochurlinkavailable();


        using (WebClient client = new WebClient())
        {

            string ticket_attachment = client.DownloadString(ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/hotelbookingvochurmail.html");
            string htmlCode = client.DownloadString(ConfigurationManager.AppSettings["PortalURL"] + "/HOTEL/hotel/EmailTemplates/Voucheremail.html");
            string ticks = DateTime.Now.Ticks.ToString();
            //  if (dtCheck.Rows.Count > 0)
            //  {
            //      string VoucherUrl = dtCheck.Rows[0]["VoucherUrl"].ToString();
            //
            //      string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
            //       string EmailBody = htmlCode;
            //       EmailSub = EmailSub.Replace("#ETripID#", ETripID);
            //       EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Voucher");
            //       EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Voucher");
            //       EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
            //       EmailBody = EmailBody.Replace("#EUName#", mailname);
            //       EmailBody = EmailBody.Replace("#ETripID#", ETripID);
            //       EmailBody = EmailBody.Replace("#ATTACHDET#", "Your voucher is attached along with this email");
            //       Out = bals.SendMailSubjectwithBcc(Email, "", "", EmailBody, EmailSub, VoucherUrl);
            //   }
            //    else
            //   {
            ticket_attachment = ticket_attachment.Replace("#AgencyName#", AgencyName);
            ticket_attachment = ticket_attachment.Replace("#AgencyAddress#", AgencyAddress);
            ticket_attachment = ticket_attachment.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
            ticket_attachment = ticket_attachment.Replace("#Contact#", AgencyContact);
            ticket_attachment = ticket_attachment.Replace("#Voucherissuedate#", gettoday);
            ticket_attachment = ticket_attachment.Replace("#GTARef#", APIid);
            ticket_attachment = ticket_attachment.Replace("#HotelName#", ETripName);
            ticket_attachment = ticket_attachment.Replace("#bookingdate#", voucherbookingdate);
            ticket_attachment = ticket_attachment.Replace("#BookingRef#", ETripID);
            ticket_attachment = ticket_attachment.Replace("#Address#", HotelAddress);
            ticket_attachment = ticket_attachment.Replace("#Checkin#", chin);
            ticket_attachment = ticket_attachment.Replace("#Checkout#", chout);
            ticket_attachment = ticket_attachment.Replace("#paxdetails#", VoucherPax);
            ticket_attachment = ticket_attachment.Replace("#GtaAotNumber#", GtaAotNumber);
            ticket_attachment = ticket_attachment.Replace("#Amount#", grandtotal);
            ticket_attachment = ticket_attachment.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
            ticket_attachment = ticket_attachment.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyLoc#", ConfigurationManager.AppSettings["AgencyNumber"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyLoc"]);

            string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/BookingVoucher/" + ETripID + "/";
            if (!Directory.Exists(storePath))
                Directory.CreateDirectory(storePath);
            //if (File.Exists(filepathee))
            //{
            //    File.Delete(filepathee);
            //}
            TextWriter attachhtml = new StreamWriter(storePath + "/Booking_Vochurattachment_" + ETripID + "_" + ticks + ".html");
            attachhtml.WriteLine(ticket_attachment);
            attachhtml.Flush();
            attachhtml.Close();

            string EmailSub = "Hotel #SUBTEXT# - Trip ID #ETripID#";
            string EmailBody = htmlCode;
            EmailSub = EmailSub.Replace("#ETripID#", ETripID);
            EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Voucher");
            EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Voucher");
            EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
            EmailBody = EmailBody.Replace("#EUName#", mailname);
            EmailBody = EmailBody.Replace("#ETripID#", ETripID);
            EmailBody = EmailBody.Replace("#ATTACHDET#", "Your voucher is attached along with this email");
            EmailBody = EmailBody.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
            EmailBody = EmailBody.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);

            Out = bals.SendMailSubjectwithBcc(Email, "", "", EmailBody, EmailSub, "Documents/BookingVoucher/" + ETripID + "/Booking_Vochurattachment_" + ETripID + "_" + ticks + ".html");
            string result = "/Documents/BookingVoucher/" + ETripID + "/Booking_Vochurattachment_" + ETripID + "_" + ticks + ".html";
            BALAD.VoucherUrl = result;
            string host = HttpContext.Current.Request.Url.Host.ToLower();
            //       if (host != "localhost")
            //       {
            //           int dtVoucher = BALAD.insertVoucherUrlinformation();
            //       }
            //    }

        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string Getbookingsession()
    {
        string Out = string.Empty;
        Out = HttpContext.Current.Session["HotelBookingRefID"].ToString();
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string SendMailbookingnotpossiable(string data, string UserMailID, string EUName, string MailHotelName, string cin, string cout, string MailCityName, string night, string moredetails)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString(Bookingpendingpath);
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking pending with the supplier");
                EmailSub = EmailSub.Replace("#ETripID#", data);
                EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking pending");
                EmailBody = EmailBody.Replace("#EUName#", EUName);
                EmailBody = EmailBody.Replace("#ETripID#", data);
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                EmailBody = EmailBody.Replace("#HotelName#", MailHotelName);
                EmailBody = EmailBody.Replace("#Checkin#", cin);
                EmailBody = EmailBody.Replace("#Checkout#", cout);
                EmailBody = EmailBody.Replace("#Nights#", night);
                EmailBody = EmailBody.Replace("#CityName#", MailCityName);
                EmailBody = EmailBody.Replace("#MoreDetails#", moredetails);
                BALAD.Emailbody = EmailBody;
                BALAD.BOOKINGREFID = data;
                DataTable dtinspending = BALAD.insertpendinginfo();
                Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelEssentialInfo(string RoomId)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BookingRoomID = Convert.ToInt64(RoomId.ToString());
        DataTable dtInfo = BALAD.GetHotelEssentialInfo();
        if (dtInfo.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtInfo.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtInfo.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetGtaAOTNumbers(string CountryCode)
    {

        SendHotelWebService hws = new SendHotelWebService();
        return hws.GetGtaAOTNumbers(CountryCode);

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertGtaAOTNumber(string DestCode, string OfficeHours, string OfficeLocation, string Language, string International, string Local, string National, string OutOfOffice)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        BALAD.DestinationCode = DestCode;
        BALAD.OfficeLocation = OfficeLocation;
        BALAD.Language = Language;
        BALAD.OfficeHours = OfficeHours;
        BALAD.International = International;
        BALAD.Local = Local;
        BALAD.National = National;
        BALAD.OutOfOffice = OutOfOffice;
        DataTable dtchrome = BALAD.insertGtaAOTNumber();
        if (dtchrome.Rows.Count > 0)
        {
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetSearchDataHotelOnrequest(string AgencyID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.AgencyID = AgencyID;
        DataTable dtonrequest = BALAD.GetHotelSearchDataOnrequest();
        if (dtonrequest.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtonrequest.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtonrequest.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string RejectHotelbooking(string valueses)
    {
        string Out = string.Empty;
        string result = string.Empty;
        string UserMailID = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = valueses;
        BALAD.BookingStatusCode = "RJ";
        BALAD.BOOKDATE = System.DateTime.Now;
        BALSettings bals = new BALSettings();

        BALAD.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);

        DataTable dtinfo = BALAD.Getusername();
        if (dtinfo.Rows.Count > 0)
        {
            string info = dtinfo.Rows[0]["FirstName"].ToString();
            string data = dtinfo.Rows[0]["LastName"].ToString();
            result = info + " " + data;
        }
        int successss = BALAD.Changestatusoftblbookingonrequest();
        int success = BALAD.InsertBookingHistoryDetails();
        DataTable dtpending = BALAD.selectpendinghotelbookinginformation();
        if (dtpending.Rows.Count > 0)
        {
            Out = dtpending.Rows[0]["PendingEmailBody"].ToString();

            DataTable dtmailID = BALAD.SendMailid();
            if (dtmailID.Rows.Count > 0)
            {
                UserMailID = dtmailID.Rows[0]["EmailID"].ToString();
            }
            DataTable dt = bals.SendMailTicketIssueCCList();
            if (dt.Rows.Count > 0)
            {
                String CCLIST = dt.AsEnumerable()
                         .Select(row => row["EmailAddress"].ToString())
                         .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
                using (WebClient client = new WebClient())
                {

                    string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                    string EmailBody = Out;
                    EmailSub = EmailSub.Replace("#ETripID#", valueses);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Rejected by " + result);

                    Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

                }
            }
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelUpdateClient(string APIid)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.BookingHotelUpdateClient(APIid);

    }
    [System.Web.Services.WebMethod]
    public static string OnrequestBookingHotelRoomsInformation(string result, string ItemRef, string CityCode, string ItemCode, string HotelName, string StatusCode, string ItemReferenceCode, DateTime CheckinDate, DateTime CheckOut, string Noadult, string NoChild, string Cot, string RoomCatID, string RoomCat, string Brkfast, string Full, string Sharebedding, List<string> PaxDatA, string Commisssion, string ItemCurrency, string ItemGross, string ItemNet, string MarkupValue, string MarkupTypeID, string HotelAddress)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        BALAD.HotelInformationID = result;
        BALAD.ItemReference = ItemRef;
        BALAD.ItemCityCode = CityCode;
        BALAD.ItemCode = ItemCode;
        BALAD.ItemName = HotelName;
        BALAD.ItemStatusCode = StatusCode;
        BALAD.CheckInDate = CheckinDate;
        BALAD.CheckOut = CheckOut;
        BALAD.ItemConfirmationReference = ItemReferenceCode;
        BALAD.HotelPaxRoomAdults = Noadult;
        BALAD.HotelPaxRoomChildren = NoChild;
        BALAD.HotelPaxRoomCots = Cot;
        BALAD.SupplierRoomCategoryID = RoomCatID;
        BALAD.RoomCategory = RoomCat;
        BALAD.MealBasis = Brkfast + ',' + Full;
        BALAD.SharingBedding = Sharebedding;
        BALAD.PAXRef = string.Join(",", PaxDatA);
        BALAD.Address = HotelAddress;
        DataTable dthotelRoominfo = BALAD.insertBookingHotelRoomsInformation();
        if (dthotelRoominfo.Rows.Count > 0)
        {
            Out = dthotelRoominfo.Rows[0]["BookingRoomID"].ToString();
            BookingHotelRoomPriceBreakup(Out, Commisssion, ItemCurrency, ItemGross, ItemNet, MarkupValue, MarkupTypeID);

        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string OnrequestBookingCancellationpolicies(string ItemReff)
    {
        string BOOKINGREFID = (HttpContext.Current.Session["HotelBookingRefID"]).ToString();
        SendHotelWebService hws = new SendHotelWebService();
        return hws.OnrequestBookingCancellationpolicies(ItemReff, BOOKINGREFID);
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string OnrequestBookingCancellationpoliciesinsertion(string HID, string Type, string Allowble, string PassTypeAllow, string PassType, List<string> CancellData)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BookingHotelRoomAmendmentStatus(HID, Type, Allowble, PassTypeAllow, PassType);
        BookingHotelRoomCancellationCharges(HID, CancellData);
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string UpdateHotelAddress(string HID, string HotelAddress)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.Address = HotelAddress;
        BALAD.HotelInformationID = HID;
        DataTable dt = BALAD.UpdateHotelAdress();
        if (dt.Rows.Count > 0)
        {
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetMoreInfoAboutOnrequest(string RefID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = RefID;
        DataTable dtmoreinfo = BALAD.selectpendinghotelbookinginformation();
        if (dtmoreinfo.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtmoreinfo.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtmoreinfo.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertintoAutoUpdateRequestlog(string APIid)
    {
        string Out = string.Empty;
        string result = string.Empty;
        string UserMailID = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = APIid;
        BALAD.BOOKDATE = DateTime.Now;
        BALSettings bals = new BALSettings();

        BALAD.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);

        DataTable dtinfo = BALAD.Getusername();
        if (dtinfo.Rows.Count > 0)
        {
            string info = dtinfo.Rows[0]["FirstName"].ToString();
            string data = dtinfo.Rows[0]["LastName"].ToString();
            result = info + " " + data;
        }
        DataTable dtchrome = BALAD.insertintoAutoUpdateRequestlog();
        DataTable dtpending = BALAD.selectpendinghotelbookinginformation();
        if (dtpending.Rows.Count > 0)
        {
            Out = dtpending.Rows[0]["PendingEmailBody"].ToString();

            DataTable dtmailID = BALAD.SendMailid();
            if (dtmailID.Rows.Count > 0)
            {
                UserMailID = dtmailID.Rows[0]["EmailID"].ToString();
            }
            DataTable dt = bals.SendMailTicketIssueCCList();
            if (dt.Rows.Count > 0)
            {
                String CCLIST = dt.AsEnumerable()
                         .Select(row => row["EmailAddress"].ToString())
                         .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
                using (WebClient client = new WebClient())
                {
                    string htmlCode = client.DownloadString(Bookingcronjob);
                    string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                    string EmailBody = Out;
                    EmailSub = EmailSub.Replace("#ETripID#", APIid);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking not found");


                    Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

                }
            }
        }

        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelInformationcronjob(string cronresult, string ClinentID, string ApiID, DateTime CheckinDate, DateTime CheckOut, string StatusCode, string Commi, string Currency, string Gross, string Net, string TotalPrice, string MarkupValue, string MarkupID)
    {
        dbCurrency = HttpContext.Current.Session["dbCurrencyCode"].ToString();
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();

        BALAD.BOOKINGREFID = cronresult;
        BALAD.ClientID = ClinentID;
        BALAD.ApiID = ApiID;
        BALAD.CheckInDate = CheckinDate;
        BALAD.CheckOut = CheckOut;
        BALAD.StatusCode = StatusCode;
        DataTable dthotelinfo = BALAD.inserthotelinformation();
        if (dthotelinfo.Rows.Count > 0)
        {
            Out = dthotelinfo.Rows[0]["HotelInformationID"].ToString();
            HotelBookingPriceDetails(Out, Commi, Currency, Gross, Net, MarkupValue, MarkupID);

        }
        else
        {
            Out = "NOINFO";
        }
        if (StatusCode == "C ")
        {
            string status = "HK";
            UpdateSupplierStatuscronjob(status, ApiID, cronresult);
            ChangeStatustoHKcronjob(status, cronresult);

        }
        else if (StatusCode == "P " || StatusCode == "CP " || StatusCode == "CP")
        {
            string status = "RR";
            UpdateSupplierStatuscronjob(status, ApiID, cronresult);
            ChangeStatustoHKcronjob(status, cronresult);
        }
        else if (StatusCode == "RJ" || StatusCode == "ER" || StatusCode == "X ")
        {
            string status = "XX";
            UpdateSupplierStatuscronjob(status, ApiID, cronresult);
            ChangeStatustoHKcronjob(status, cronresult);

        }
        InsertTblPaymentcronjob(TotalPrice, dbCurrency, 1, cronresult);
        return Out;
    }
    public static string UpdateSupplierStatuscronjob(string status, string ApiID, string cronjob)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = cronjob;
            BALAD.PNR = ApiID;
            BALAD.StatusCode = status;
            int Success = BALAD.upadteSupplierBooking();
        }
        return sucess;
    }

    public static string ChangeStatustoHKcronjob(string status, string cronjob)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = cronjob;
            BALAD.BookingStatusCode = status;
            BALAD.BOOKDATE = System.DateTime.Now;
            BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
            //  BALAD.UID = 50;
            int successss = BALAD.InsertBookingHistoryDetails();
            //SignOut();
        }
        return sucess;
    }
    public static string InsertTblPaymentcronjob(string PaidAmount, string CurrencyCode, Int32 PaymentTypeID, string cronjob)
    {
        BALBook BALAD = new BALBook();
        BALAD.BookingRefID = Convert.ToInt64(cronjob);
        BALAD.PaidAmount = PaidAmount;
        BALAD.CurrencyCode = CurrencyCode;
        BALAD.PaymentTypeID = PaymentTypeID;
        int AirSegment = BALAD.InsertTblPayment();
        return AirSegment.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SendMailbookingcronjob(string cronresult, string HotelName)
    {

        string Out = string.Empty;
        string UserMailID = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dtmailID = BALAD.SendMailid();
        if (dtmailID.Rows.Count > 0)
        {
            UserMailID = dtmailID.Rows[0]["EmailID"].ToString();
        }
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString(Bookingcronjob);
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", cronresult);
                EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Information");
                EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Information");
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                EmailBody = EmailBody.Replace("#STATUSBOOK#", "Confirmed");
                EmailBody = EmailBody.Replace("#ETripID#", cronresult);
                EmailBody = EmailBody.Replace("#TRIPNAME#", HotelName);
                Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Cornjobpendingconfirmation()
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtchrome = BALAD.selectCornjobpendingconfirmation();
        if (dtchrome.Rows.Count > 0)
        {
            Out = dtchrome.Rows[0]["BookingRefID"].ToString();
            // Out = "1992";

        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string CornjobpendingconfirmationOld()
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtchrome = BALAD.selectCornjobpendingconfirmationOld();
        if (dtchrome.Rows.Count > 0)
        {
            Out = dtchrome.Rows[0]["BookingRefID"].ToString();
            //Out = "1985";

        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string ChornjobOnRequest()
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        //  DataTable dtchrome = BALAD.selectChornjobOnRequest();
        //if (dtchrome.Rows.Count > 0)
        //{
        //    Out = dtchrome.Rows[0]["BookingRefID"].ToString();
        //    //Out = "1985";

        //}
        //else
        //{
        //    Out = "NOINFO";
        //}
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookingHotelUpdatecronjob(string APIid)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.BookingHotelUpdateClient(APIid);

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Changehotelbookingstatusgetcanceldatacronjob(string Statuscancellation, string APIid)
    {
        string status = string.Empty;
        if (Statuscancellation == "C ")
        {
            status = "HK";
            ChangeStatustoHKcronjob(status, APIid);

            ChangecancellStatuscronjob(status, APIid);

        }
        else if (Statuscancellation == "P " || Statuscancellation == "CP")
        {
            status = "RR";
            ChangeStatustoHKcronjob(status, APIid);
            ChangecancellStatuscronjob(status, APIid);
        }
        else if (Statuscancellation == "X ")
        {
            status = "XX";
            ChangeStatustoHKcronjob(status, APIid);
            ChangecancellStatuscronjob(status, APIid);

        }
        else if (Statuscancellation == "RC")
        {
            status = "RC";
            ChangeStatustoHKcronjob(status, APIid);
            ChangecancellStatuscronjob(status, APIid);

        }
        else if (Statuscancellation == "XP")
        {

            status = "XR";
            ChangeStatustoHKcronjob(status, APIid);
            ChangecancellStatuscronjob(status, APIid);
        }
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKDATE = System.DateTime.Now;
        if (HttpContext.Current.Session["HotelBookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["HotelBookingRefID"].ToString();
            DataTable dtchromeupdatelog = BALAD.insertintoAutoUpdateRequestlog();
            DataTable dtprice = BALAD.GetCancelBookingPriceDetails();
            if (dtprice.Rows.Count > 0)
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dtprice.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dtprice.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                status = serializer.Serialize(rows);
            }
            else
            {
                status = "NOINFO";
            }
        }
        return status;
    }
    public static string ChangecancellStatuscronjob(string status, string Apid)
    {
        BALHotelBook BALAD = new BALHotelBook();
        string sucess = "";
        BALAD.BOOKINGREFID = Apid;
        BALAD.StatusCode = status;
        int Success = BALAD.upadteBookingCancellationstatus();

        return sucess;
    }

    [System.Web.Services.WebMethod]
    public static string SendMailBookhotelpendingConfirmation(string Statuscancellation, string ClinentID, string CheckinDate, string HotelName, string Night, string ItemCity)
    {
        string Out = string.Empty;
        string UserMailID = string.Empty;
        string result = string.Empty;
        BALSettings bals = new BALSettings();
        BALHotelBook BALAD = new BALHotelBook();
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);

        DataTable dtinfo = BALAD.Getusername();
        if (dtinfo.Rows.Count > 0)
        {
            string info = dtinfo.Rows[0]["FirstName"].ToString();
            string data = dtinfo.Rows[0]["LastName"].ToString();
            result = info + " " + data;
        }
        DataTable dtmailID = BALAD.SendMailid();
        if (dtmailID.Rows.Count > 0)
        {
            UserMailID = dtmailID.Rows[0]["EmailID"].ToString();
        }
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            using (WebClient client = new WebClient())
            {
                string htmlCode = client.DownloadString(Bookingmailpathbookingupdation);
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", ClinentID);
                EmailBody = EmailBody.Replace("#ETripID#", ClinentID);
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                EmailBody = EmailBody.Replace("#EUName#", result);
                EmailBody = EmailBody.Replace("#Checkin#", CheckinDate);
                EmailBody = EmailBody.Replace("#HotelName#", HotelName);
                EmailBody = EmailBody.Replace("#Nights#", Night);
                EmailBody = EmailBody.Replace("#CityName#", ItemCity);
                EmailSub = EmailSub.Replace("#SUBTEXT#", "Booking Information");
                EmailBody = EmailBody.Replace("#MAILHEADER#", "Booking Information");
                EmailBody = EmailBody.Replace("#STATUSBOOK#", Statuscancellation);
                BALAD.Emailbody = EmailBody;
                BALAD.BOOKINGREFID = ClinentID;
                DataTable dtinspending = BALAD.insertpendinginfo();
                Out = BALAD.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub);

            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetAgencyData(string AgencyIDD)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.AgencyID = AgencyIDD;
        DataTable dtgetagency = BALAD.GetfullAgencydetails();
        if (dtgetagency.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtgetagency.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtgetagency.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string MyBookHotelinfo(string User)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.UID = Convert.ToInt64(User.ToString());
        DataTable dt = BALAD.GetMyBookHotelinfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string MyBookHotelinformation(string bookingID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = bookingID.ToString();
        HttpContext.Current.Session["HotelBookingRefID"] = bookingID;
        DataTable dt = BALAD.GetBookInfoDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string MyBookPassengersHotelinfo(string bookingID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.BOOKINGREFID = bookingID.ToString();
        DataTable dt = BALAD.GetPassengerBookInfoDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetCotravellerdetails()
    {
        string Out = string.Empty;
        BALBook BALAD = new BALBook();
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dttravel = BALAD.Gettravellerdetails();
        if (dttravel.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dttravel.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dttravel.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BindCotravellerdetails(string TravelID)
    {
        string Out = string.Empty;
        BALHotelBook BALAD = new BALHotelBook();
        BALAD.TravellID = TravelID;
        DataTable dttraveller = BALAD.Gettravellerdetailstoedit();
        if (dttraveller.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dttraveller.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dttraveller.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string inserthoteldetailstobookingtables(string FirstName, string SurName, string Email, string PhoneNumber, string Title)
    {

        BALHotelBook BALAD = new BALHotelBook();
        BALSettings bals = new BALSettings();
        BALBook BALA = new BALBook();
        BALGeneral BALG = new BALGeneral();
        DataTable dtbook = new DataTable();
        string BOOKINGREFID = string.Empty;
        Int64 user = 0;
        //---------Checking that if the user exsist or not--START----------------\\
        if (HttpContext.Current.Session["UserID"] != null)
        {
            user = Convert.ToInt64(HttpContext.Current.Session["UserID"].ToString());

        }
        else
        {
            BALA.EMAIL = Email;
            DataTable dtCheckuser = BALA.Chechingcrntusrexsist();
            if (dtCheckuser.Rows.Count > 0)
            {
                user = Convert.ToInt64(dtCheckuser.Rows[0]["UserID"].ToString());
                HttpContext.Current.Session["UserID"] = user;



            }
            else
            {

                BALA.TITLE = Title;
                BALA.FNAME = FirstName;
                BALA.SNAME = SurName;
                BALA.EMAIL = Email;
                BALA.TELEPHONE = PhoneNumber;
                BALA.PASSWORD = Guid.NewGuid().ToString("n").Substring(1, 5);
                string PlainPass = BALA.PASSWORD;
                BALA.PASSWORDEncrypt = BALG.Encrypt(PlainPass);

                DataTable dtinsertuserdetails = BALA.InsertUserDetails();
                if (dtinsertuserdetails.Rows.Count > 0)
                {
                    user = Convert.ToInt64(dtinsertuserdetails.Rows[0]["UserID"].ToString());
                    // Sendregistrationemail(Email, FirstName, SurName, PlainPass);
                    HttpContext.Current.Session["UserID"] = user;
                }
            }
        }
        BALAD.UID = user;
        BALAD.BOOKDATE = System.DateTime.Now;
        BALAD.BookingStatusCode = "RQ";
        BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
        dtbook = BALAD.InsertNewBookingDetails();


        //---------------inserting data to tblbooking-----------------------\\
        //---------------inserting data to tblbookinghistory-----------------------\\
        if (dtbook.Rows.Count > 0)
        {
            BALAD.UID = user;
            BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            HttpContext.Current.Session["HotelBookingRefID"] = BOOKINGREFID;
            int success = BALAD.InsertBookingHistoryDetails();
        }
        return BOOKINGREFID;
        //---------------inserting data to tblbookinghistory-----------------------\\
    }
    [System.Web.Services.WebMethod]
    public static string CancelbookinghotelMail(string APIid, string ETripIDD, string EUNamee, string ECommentt)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt2 = bals.GetEmailTemplate(6);
        if (dt2.Rows.Count > 0)
        {
            string EmailSub = dt2.Rows[0]["Subject"].ToString();
            string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
            string EmailTo = dt2.Rows[0]["Recipient"].ToString();

            EmailSub = EmailSub.Replace("#BOOKINGREFID#", ETripIDD);
            EmailSub = EmailSub.Replace("#PNRNUMBER#", APIid);

            EmailBody = EmailBody.Replace("#USERNAME#", EUNamee);
            EmailBody = EmailBody.Replace("#BOOKINGREFID#", ETripIDD);
            EmailBody = EmailBody.Replace("#PNRNUMBER#", APIid);
            EmailBody = EmailBody.Replace("#COMMENT#", ECommentt);
            bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string inserthoteltobookingtables()
    {

        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtbook = new DataTable();
        string BOOKINGREFID = string.Empty;
        string BOOKCLIENTID = string.Empty;
        // BALAD.UID = 50;
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.BOOKDATE = System.DateTime.Now;
        BALAD.BookingStatusCode = "RQ";
        BALAD.CANCELDATE = System.DateTime.Now.AddDays(-1);// for backing oneday(previous date)
        dtbook = BALAD.InsertNewBookingDetails();
        //---------------inserting data to tblbooking-----------------------\\
        //---------------inserting data to tblbookinghistory-----------------------\\
        if (dtbook.Rows.Count > 0)
        {
            BALAD.BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();

            BOOKINGREFID = dtbook.Rows[0]["BookingRefID"].ToString();
            BOOKCLIENTID = dtbook.Rows[0]["BookingRefID"].ToString();
            HttpContext.Current.Session["HotelBookingRefID"] = BOOKINGREFID;
            int success = BALAD.InsertBookingHistoryDetails();
        }
        //---------------inserting data to tblbookinghistory-----------------------\\
        return BOOKINGREFID;
    }
    [System.Web.Services.WebMethod]
    public static string GetGTAImageCountrycode(string Citycode)
    {
        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtcode = new DataTable();
        BALAD.City = Citycode;
        dtcode = BALAD.GetGTAImageCountrycode();
        string Countrycode = "";
        if (dtcode.Rows.Count > 0)
        {
            Countrycode = dtcode.Rows[0]["CountryCode"].ToString();
        }
        return Countrycode;
    }
    [System.Web.Services.WebMethod]
    public static string GetCountrycode(string Citycode)
    {
        BALHotelBook BALAD = new BALHotelBook();
        DataTable dtcode = new DataTable();
        BALAD.City = Citycode;
        dtcode = BALAD.GetCountrycode();
        string Countrycode = "";
        if (dtcode.Rows.Count > 0)
        {
            Countrycode = dtcode.Rows[0]["CountryCode"].ToString();
        }
        return Countrycode;
    }
    public enum TokenType
    {
        Bearer
    }

    public enum RequestType
    {
        GET, POST, PUT, DELETE
    }
    public class DebitEntryResposne
    {
        [JsonProperty("id")]
        public List<string> Id { get; set; }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetServiceMarkupfordebit(string Currency, string Servicetype) //advancedMarkup
    {

        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = "{  \"criteria\": {            \"service\": \"Hotel\"  }   } ";
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "markup/fetchapplicablemarkups"; ;
        string strResult = "NOMARKUP";
        string tempResultJSON = "[";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", TokenType.Bearer.ToString() + " " + SessionData);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType.POST.ToString();

            string request = JsonConvert.SerializeObject(XMLData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                //streamWriter.Flush();
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];
            Stream receiveStream = httpResponse.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();
        }
        catch (Exception ex)
        {
        }
        return strResult;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string inserthotelDebitentrydetails(string requestentry) //debit entry
    {
        BALHotelBook BALAD = new BALHotelBook();
        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        //string XMLData = requestentry;
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "transaction/debitentry";
        string strResult = "NORESULT";
        string tempResultJSON = "[";
        JObject reqEntry = JObject.Parse(requestentry);
        var markUps = (JArray)reqEntry["debitEntryList"];

        foreach (var markUp in markUps)
        {
            markUp["totalAmount"] = Math.Round((double)markUp["totalAmount"], 4);
        }
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + SessionData);
            using (
                var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(reqEntry.ToString(Formatting.None));
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            SessionData = httpResponse.Headers["access_token"].ToString();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                HttpContext.Current.Session["Access_Token"] = SessionData;
                strResult = responseText;


            }
        }
        catch (Exception ex)
        {
        }
        return strResult;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string cancelDevitEntry(string DebitID)
    {
        string strResult = "NORESULT";
        string accessToken = "";
        try
        {
            accessToken = HttpContext.Current.Session["Access_Token"].ToString();
        }
        catch
        {
            accessToken = "";
        }
        if (accessToken != "")
        {
            try
            {

                string baseUri = "https://67.209.127.116:9443/";
                string reqeusturl = baseUri + "transaction/cancel/" + DebitID + "";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(reqeusturl);
                httpWebRequest.Method = "DELETE";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                accessToken = httpResponse.Headers["access_token"].ToString();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    HttpContext.Current.Session["Access_Token"] = accessToken;
                    strResult = responseText;
                }
            }
            catch
            {
                strResult = "NORESULT";
            }
        }
        else
        {
            strResult = "NORESULT";
        }
        return strResult;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string issueDevitEntry(string DebitID)
    {
        string strResult = "NORESULT";
        string accessToken = "";
        try
        {
            accessToken = HttpContext.Current.Session["Access_Token"].ToString();
        }
        catch
        {
            accessToken = "";
        }
        if (accessToken != "")
        {
            try
            {

                string baseUri = "https://67.209.127.116:9443/";
                string reqeusturl = baseUri + "hotel/issue/" + DebitID + "";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(reqeusturl);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "PUT";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                accessToken = httpResponse.Headers["access_token"].ToString();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    HttpContext.Current.Session["Access_Token"] = accessToken;
                    strResult = responseText;
                }
            }
            catch (Exception ex)
            {
                strResult = "NORESULT";
            }
        }
        else
        {
            strResult = "NORESULT";
        }
        return strResult;
    }
    [System.Web.Services.WebMethod]
    public static string AddQuatationtoHub(string Request)
    {
        // String InterfaceURL = "https://192.168.1.133:9443/quote";
        string InterfaceURL = HubURL + ":" + HubPort + "/quote";
        string strResult = "NORESULT";
        try
        {

            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(Request);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                HttpContext.Current.Session["Access_Token"] = accesstocken;
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();
            }

        }
        return strResult;
    }
    [System.Web.Services.WebMethod]
    public static string GetAllQuatation()
    {
        String InterfaceURL = HubURL + ":" + HubPort + "/quote/pendingQuoteDetails";
        string strResult = "NORESULT";
        try
        {
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];
            HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;

            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }

        return strResult;
    }
    [System.Web.Services.WebMethod]
    public static string RemoveQuatation(string requst)
    {
        // String InterfaceURL = "https://192.168.1.133:9443/quote";
        string InterfaceURL = HubURL + ":" + HubPort + "/quote/remove";
        string strResult = "NORESULT";
        try
        {

            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(requst);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                HttpContext.Current.Session["Access_Token"] = accesstocken;
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();
            }

        }
        return strResult;

    }
    [System.Web.Services.WebMethod]
    public static string saveSendQuote(string Request)
    {
        // String InterfaceURL = "https://192.168.1.133:9443/quote";
        string InterfaceURL = HubURL + ":" + HubPort + "/quote/saveSendQuote";
        string strResult = "NORESULT";
        try
        {

            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(Request);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                HttpContext.Current.Session["Access_Token"] = accesstocken;
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();
            }

        }
        return strResult;

    }
}
