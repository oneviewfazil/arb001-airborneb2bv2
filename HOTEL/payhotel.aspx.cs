﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class payhotel : System.Web.UI.Page
{
    BALSettings bals = new BALSettings();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["status"] != null)
            {
                if (Request.QueryString["status"] == "02")
                {
                    //Authorization Success
                    if (Request.QueryString["response_code"] != null)
                    {
                        if (Request.QueryString["response_code"] == "02000")
                        {
                            //Success
                            if (Request.QueryString["merchant_reference"] != null)
                            {
                               // hdcheck.Value = "1";
                               ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "MyFun1", "confirm();", true);
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "MyFunction()", true);
                                //Changing payment Status to 3 - Fully Paid and payment type credit card
                                string BOOKINGREFID = Session["HotelBookingRefID"].ToString();
                                double AmountMultiplier = Convert.ToInt64(ConfigurationManager.AppSettings["payfortCurrencyMultiplier"]);
                                bals.PaidAmount = Convert.ToDouble(Request.QueryString["amount"].ToString()) / AmountMultiplier;
                                bals.CurrencyCode = ConfigurationManager.AppSettings["dbCurrencyCode"];
                                bals.BOOKINGREFID = BOOKINGREFID;
                                bals.SuccessPaymentGatewayDBOps();
                               // Response.Redirect("~/hotelbookinginfo.html");

                            }
                            else
                            {
                                //Failed
                                hdcheck.Value = "0";
                                Response.Redirect("~/Error.html?error=Payment%20Failed");
                            }
                        }
                    }
                }
                else
                {
                    //Failed
                    Response.Redirect("~/Error.html?error=Payment%20Failed");
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}