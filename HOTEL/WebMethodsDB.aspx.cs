﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

public partial class WebMethodsDB : System.Web.UI.Page
{

    public static string dbGTACurrency = ConfigurationManager.AppSettings["dbGTACurrency"];
    public static string RateOfExchange = ConfigurationManager.AppSettings["GTARateOfExchange"];
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static string GetMarkupData(Int64 AgencyID, int ServiceTypeID)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.AgencyID = AgencyID;
        bals.ServiceTypeID = ServiceTypeID;
        DataTable dt = bals.GetMarkupDetails();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            //Out = "[{" + '"' + "MarkupTypeID" + '"' + ":0," + '"' + "MarkupValue" + '"' + ":0," + '"' + "CurrenyCode" + '"' + ":" + '"' + "AED" + '"' + "}]";
            Out = "NOMARKUP";
        }
        HttpContext.Current.Session["MarkupData"] = Out;
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CheckLogin(string uname, string pwd)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.Username = uname;
        bals.Password = bals.Encrypt(pwd);
        DataTable dt = bals.CheckLogin();
        if (dt.Rows.Count > 0)
        {
            string UserStat = dt.Rows[0]["LoginStatus"].ToString();
            string AgencyStat = dt.Rows[0]["AgencyLoginStat"].ToString();
            if (UserStat == "1" && AgencyStat == "1")
            {
                Out = "SUCCESS";
                HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                Int64 UID = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);

                GetRoleStatus(UID);
            }
            else
            {
                Out = "LOCKED";
            }

        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetSessionData(string sesname)
    {
        string SessionData = string.Empty;
        if (HttpContext.Current.Session[sesname] != null)
        {
            SessionData = HttpContext.Current.Session[sesname].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }
        return SessionData;
    }

    //Kelvin
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static Dictionary<string, string> GetSessionDataNew(string sesname)
    {
        JObject jObject = JObject.Parse(sesname);
        JToken jUser = jObject["userSession"];

        Dictionary<string, string> SessionData = new Dictionary<string, string>();

        foreach (var sessionKey in jUser.Values())
        {
            string sessionKeyStr = (string)sessionKey;

            if (HttpContext.Current.Session[sessionKeyStr] != null)
            {
                SessionData.Add(sessionKeyStr, HttpContext.Current.Session[sessionKeyStr].ToString());
            }
            else
            {
                SessionData.Add(sessionKeyStr, "NOSESSION");
            }
        }
        return SessionData;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string[] GetSessionDatadet()
    {
        string[] webdet = new string[11];
        string baseUrl = ConfigurationManager.AppSettings["HotelUrl"] + ":" + ConfigurationManager.AppSettings["HotelPort"];

        webdet[0] = ConfigurationManager.AppSettings["HotelAgencyCode"];
        webdet[1] = baseUrl + ConfigurationManager.AppSettings["HotelFindURI"];
        webdet[2] = baseUrl + ConfigurationManager.AppSettings["HotelDetailURI"];
        webdet[3] = baseUrl + ConfigurationManager.AppSettings["HotelPrebookURI"];
        webdet[4] = baseUrl + ConfigurationManager.AppSettings["HotelBookURI"];
        webdet[5] = baseUrl + ConfigurationManager.AppSettings["HotelPrecancelURI"];
        webdet[6] = baseUrl + ConfigurationManager.AppSettings["HotelCancelURI"];
        webdet[7] = baseUrl + ConfigurationManager.AppSettings["HotelAmenitiesURI"];
        webdet[8] = baseUrl + ConfigurationManager.AppSettings["HotelRoomAmenitiesURI"];
        webdet[9] = baseUrl + ConfigurationManager.AppSettings["HotelFindMoreURI"];
        webdet[10] = baseUrl + ConfigurationManager.AppSettings["HotelPOIFilterURI"];
        return webdet;
    }

    //[System.Web.Services.WebMethod(EnableSession = true)]
    //public static string[] BindHotelSuppliers()
    //{
    //    Int32 i = 0;
    //    string[] webdet = new string[10];
    //    if (HttpContext.Current.Session["Loginuser"] != null)
    //    {
    //        var SessionData = HttpContext.Current.Session["Loginuser"].ToString();
    //        JObject jObject = JObject.Parse(SessionData);
    //        if (jObject["user"]["loginNode"]["servicesList"] == null)
    //        {
    //            webdet[i] = "NOVALUE";
    //        }
    //        else
    //        {
    //            foreach (var nodeItem in jObject["user"]["loginNode"]["servicesList"])
    //            {
    //                if ((string)nodeItem["name"] == "Hotel")
    //                {
    //                    foreach (var indSupplier in nodeItem["provider"])
    //                    {
    //                        webdet[i] = (string)indSupplier["name"];
    //                        i++;
    //                    }
    //                }
    //            }
    //            webdet[i] = "NOVALUE";
    //        }
    //    }
    //    return webdet;
    //}

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static List<string> BindHotelSuppliers()
    {
        //Int32 i = 0;
        //string[] webdet = new string[10];
        var webdet = new List<string>();
        if (HttpContext.Current.Session["Loginuser"] != null)
        {
            var SessionData = HttpContext.Current.Session["Loginuser"].ToString();
            JObject jObject = JObject.Parse(SessionData);
            if (jObject["user"]["loginNode"]["servicesList"] == null)
            {
                webdet.Add("NOVALUE");
            }
            else
            {
                foreach (var nodeItem in jObject["user"]["loginNode"]["servicesList"])
                {
                    if ((string)nodeItem["name"] == "Hotel")
                    {
                        foreach (var indSupplier in nodeItem["provider"])
                        {
                            webdet.Add((string)indSupplier["name"]);
                            //i++;
                        }
                    }
                }
                //webdet[i] = "NOVALUE";
            }
        }
        return webdet;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void ClearSessionData(string sesname)
    {
        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.Signout(SessionIDFinal);
        HttpContext.Current.Session.Clear();
    }
    [System.Web.Services.WebMethod]
    public static string GetCreditLimitAmount(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.AgencyID = AgencyID;
        DataTable dt = bals.GetCreditLimitAmount();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            //Out = "[{" + '"' + "MarkupTypeID" + '"' + ":0," + '"' + "MarkupValue" + '"' + ":0," + '"' + "CurrenyCode" + '"' + ":" + '"' + "AED" + '"' + "}]";
            Out = "NOCREDITLIMIT";
        }
        //HttpContext.Current.Session["MarkupData"] = Out;
        return Out;
    }


    public static void GetRoleStatus(Int64 UID)
    {
        //Get Roles
        BALSettings bals = new BALSettings();
        bals.UserID = UID;
        DataTable dtroles = bals.GetUserRoleStatus();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            HttpContext.Current.Session["UserRoleStatus"] = serializer1.Serialize(rows1);
        }
    }
    [System.Web.Services.WebMethod]
    public static string InsertTblPayment(string PaidAmount, string CurrencyCode, Int32 PaymentTypeID)
    {
        BALBook BALAD = new BALBook();
        BALAD.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        BALAD.PaidAmount = PaidAmount;
        BALAD.CurrencyCode = CurrencyCode;
        BALAD.PaymentTypeID = PaymentTypeID;
        int AirSegment = BALAD.InsertTblPayment();
        return AirSegment.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string GetTableData(string selectdata, string tablename, string condition)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.selectdata = selectdata;
        bals.tablename = tablename;
        bals.condition = condition;
        DataTable dtroles = bals.GetTableDatas();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string UpdateUserData(string Title, string FName, string LName, string MobNo)
    {
        BALSettings BALSET = new BALSettings();
        BALSET.Title = Title;
        BALSET.FName = FName;
        BALSET.LName = LName;
        BALSET.MobNo = MobNo;
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int AirSegment = BALSET.UpdateUserData();
        GetUserData(Convert.ToInt64(HttpContext.Current.Session["UserID"]));
        return AirSegment.ToString();
    }
    public static void GetUserData(Int64 UID)
    {
        BALSettings bals = new BALSettings();
        bals.UserID = UID;
        DataTable dt = bals.GetUserData();
        if (dt.Rows.Count > 0)
        {
            HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
            Int64 UIDD = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);
            GetRoleStatus(UIDD);
        }
    }
    [System.Web.Services.WebMethod]
    public static string UpdateUserPassword(string CurrentPass, string NewPass, string ConfPass)
    {
        string OUT = string.Empty;
        BALSettings BALSET = new BALSettings();
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = BALSET.GetUserData();
        if (dt.Rows.Count > 0)
        {
            string DBPass = dt.Rows[0]["Password"].ToString();
            string DBPassDecrypt = BALSET.Decrypt(DBPass);
            if (DBPassDecrypt == CurrentPass)
            {
                BALSET.NewPass = BALSET.Encrypt(NewPass);
                int AirSegment = BALSET.UpdateUserPass();
                OUT = "PASSCHANGESUCCESS";
            }
            else
            {
                OUT = "PASSCHANGEFAILED";
            }
        }
        return OUT;
    }

    [System.Web.Services.WebMethod]
    public static string UpdateUserPasswordNew(string CurrentPass, string NewPass)
    {
        string HubURL = ConfigurationManager.AppSettings["HubURL"];
        string HubPort = ConfigurationManager.AppSettings["HubPort"];
        // String InterfaceURL = "https://192.168.1.133:9443/quote";
        string InterfaceURL = HubURL + ":" + HubPort + "/profile/password";
        string strResult = "NORESULT";
        string request = "{\"oldPassword\": \"" + CurrentPass + "\", \"newPassword\": \"" + NewPass + "\"}";
        try
        {

            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PUT";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(request);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                HttpContext.Current.Session["Access_Token"] = accesstocken;
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();
            }

        }
        return strResult;

    }

    //Book
    public class objectName
    {
        public Int64 MarkupTypeID { get; set; }
        public Int64 MarkupValue { get; set; }
        public string CurrenyCode { get; set; }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirBookingCost(Double TotalBaseNett, Double TotalFaree, string NetCurrencyy, double XmlMarkupp)
    {

        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        if (HttpContext.Current.Session["MarkupData"] != null)
        {
            string markupdata = HttpContext.Current.Session["MarkupData"].ToString();
            List<objectName> obj = new List<objectName>(JsonConvert.DeserializeObject<objectName[]>(markupdata));
            Double MarkupID = Convert.ToDouble(obj[0].MarkupTypeID);
            if (MarkupID == 0)
            {
                BALAD.MarkupTypeID = 2;
                BALAD.MarkupValue = 0;
                BALAD.CurrenyCode = ConfigurationManager.AppSettings["dbCurrencyCode"];
            }
            else
            {
                BALAD.MarkupTypeID = Convert.ToDouble(obj[0].MarkupTypeID);
                BALAD.MarkupValue = Convert.ToDouble(obj[0].MarkupValue);
                BALAD.CurrenyCode = obj[0].CurrenyCode;
            }

        }

        if (BALAD.MarkupTypeID == 1)
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamount = TotalFaree * (MarkValue / 100);
            Double Sellamounttotal = TotalFaree + Sellamount;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        else
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamounttotal = TotalFaree + MarkValue;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        BALAD.TotalBaseNet = Convert.ToDouble(TotalBaseNett);
        BALAD.TotalFare = Convert.ToDouble(TotalFaree);
        BALAD.XmlMarkup = Convert.ToDouble(XmlMarkupp);
        BALAD.NetCurrency = NetCurrencyy;
        string sucess = "";
        DataTable AirBookingCost = BALAD.InsertAirBookingCost();
        if (AirBookingCost.Rows.Count > 0)
        {
            sucess = AirBookingCost.Rows[0]["BookingCostID"].ToString();
        }

        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirBookingCostBreakup(Int64 Costbreakupp, Double TotalFaree, Double Taxx, Double BaseFaree, string PassengerTypee, Int64 PassengerQuantityy, string NetCurrencyy, int GrandTotalPax, double xmlfarebreakupp, Double TotalOgFare)
    {
        BALBook BALAD = new BALBook();
        BALAD.Costbreakup = Convert.ToInt64(Costbreakupp);
        BALAD.TotalFare = Convert.ToDouble(TotalFaree);
        BALAD.BaseFare = Convert.ToDouble(BaseFaree);
        BALAD.Tax = Convert.ToDouble(Taxx);
        BALAD.NetCurrency = NetCurrencyy;
        Double OGMarkupValue = 0;
        if (HttpContext.Current.Session["MarkupData"] != null)
        {
            string markupdata = HttpContext.Current.Session["MarkupData"].ToString();
            List<objectName> obj = new List<objectName>(JsonConvert.DeserializeObject<objectName[]>(markupdata));
            Double MarkupID = Convert.ToDouble(obj[0].MarkupTypeID);
            OGMarkupValue = Convert.ToDouble(obj[0].MarkupValue);
            if (MarkupID == 0)
            {
                BALAD.MarkupTypeID = 2;
                BALAD.MarkupValue = 0;
                BALAD.XMLMarkupValueDivision = xmlfarebreakupp / GrandTotalPax;
                BALAD.CurrenyCode = ConfigurationManager.AppSettings["dbCurrencyCode"];
            }
            else
            {
                BALAD.MarkupTypeID = Convert.ToDouble(obj[0].MarkupTypeID);
                BALAD.MarkupValue = Convert.ToDouble(obj[0].MarkupValue) / GrandTotalPax;
                BALAD.XMLMarkupValueDivision = xmlfarebreakupp / GrandTotalPax;
                BALAD.CurrenyCode = obj[0].CurrenyCode;
            }
        }
        if (BALAD.MarkupTypeID == 1)
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamount = (TotalOgFare * (OGMarkupValue / 100)) / GrandTotalPax;
            BALAD.MarkupValue = Sellamount;
            Double Sellamounttotal = TotalFaree + Sellamount;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        else
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamounttotal = TotalFaree + MarkValue;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        BALAD.PassengerType = PassengerTypee;
        BALAD.PassengerQuantity = Convert.ToInt64(PassengerQuantityy);
        int AirBookingCostBreakup = BALAD.InsertAirBookingCostBreakup();
        string sucess = AirBookingCostBreakup.ToString();
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirsegmentBookingAvail(List<string> FareBasisArrayy)
    {
        BALBook BALAD = new BALBook();
        string sucess = "0";
        HttpContext.Current.Session["FareBasisArray"] = FareBasisArrayy;
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertAirOriginDestinationOptions(List<string> RefNumberArryyy, List<string> DirectionIdArryyy, List<string> ElapsedTimeArryyy, List<string> FromlocArryyyy, List<string> TolocArryyy, List<string> depttimeArryyy, List<string> arritimeArryyy, List<string> FnoArryyy, List<string> depterminalArryyy, List<string> arriterminalArryyy, List<string> operatingairlineArryyy, List<string> EquipmentArryyy, List<string> MarketingAirlineArryyy, string PN, string CANCEL, List<string> FlightSegCount, List<string> FareBasisArrayy, List<string> DepartureDateTimeArrayy)
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        BALAD.PNR = PN;
        string cancel = CANCEL;
        BALAD.CANCELDATE = Convert.ToDateTime(CANCEL);
        int Success = BALAD.upadtepnrBooking();
        int Updateair = BALAD.Upadtepnrairpassenger();

        for (int i = 0; i < RefNumberArryyy.Count; i++)
        {
            BALAD.REFNO = RefNumberArryyy[i].ToString();
            BALAD.DIRECTIONID = DirectionIdArryyy[i].ToString();
            BALAD.ELASPED = ElapsedTimeArryyy[i].ToString();
            DataTable UpdateAirOrigin = BALAD.InsertAirOriginOptions();
            if (UpdateAirOrigin.Rows.Count > 0)
            {
                BALAD.OriginDestinationID = UpdateAirOrigin.Rows[0]["OriginDestinationID"].ToString();
                for (int j = 0; j < FromlocArryyyy.Count; j++)
                {
                    BALAD.Depttime = Convert.ToDateTime(depttimeArryyy[j].ToString());
                    BALAD.Arritime = Convert.ToDateTime(arritimeArryyy[j].ToString());
                    BALAD.Fno = FnoArryyy[j].ToString();
                    BALAD.Fromloc = FromlocArryyyy[j].ToString();
                    BALAD.Depterminal = depterminalArryyy[j].ToString();
                    BALAD.Toloc = TolocArryyy[j].ToString();
                    BALAD.Arriterminal = arriterminalArryyy[j].ToString();
                    BALAD.Operatingairline = operatingairlineArryyy[j].ToString();
                    BALAD.Equipment = EquipmentArryyy[j].ToString();
                    BALAD.MarketingAirline = MarketingAirlineArryyy[j].ToString();

                    DataTable AirSegment = BALAD.InsertAirSegment();
                    if (AirSegment.Rows.Count > 0)
                    {
                        sucess = AirSegment.Rows[0]["SegmentID"].ToString();
                    }
                }

            }

        }
        BookingAvailableinsertion(FareBasisArrayy);
        StopOverInsertion(DepartureDateTimeArrayy);
        return sucess;
    }
    public static string BookingAvailableinsertion(List<string> FareBasisArray)
    {
        List<string> FareBasisArrayy = new List<string>();
        string sucess;
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        //if (HttpContext.Current.Session["FareBasisArray"] != null)
        //{
        //    FareBasisArrayy = (List<string>)HttpContext.Current.Session["FareBasisArray"];
        FareBasisArrayy = FareBasisArray;

        for (int j = 0; j < FareBasisArrayy.Count; j++)
        {
            string strVale = FareBasisArrayy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.Fromloc = arr[4].ToString();
            BALAD.Toloc = arr[5].ToString();
            BALAD.REFNO = arr[6].ToString();
            BALAD.DIRECTIONID = arr[7].ToString();
            DataTable SegmentID = BALAD.GetSegmentID();
            if (SegmentID.Rows.Count > 0)
            {
                BALAD.SegmentID = Convert.ToInt64(SegmentID.Rows[0]["SegmentID"].ToString());
                BALAD.ResBookDesigCabinCode = arr[0].ToString();
                BALAD.ResBookDesigCode = arr[1].ToString();
                BALAD.AvailablePTC = arr[2].ToString();
                BALAD.FareBasis = arr[3].ToString();
                int AirsegmentBookingAvail = BALAD.InsertAirsegmentBookingAvail();
            }
        }
        //}
        sucess = "0";
        return sucess;
    }
    public static string StopOverInsertion(List<string> DepartureDateTimeArrayy)
    {
        List<string> DepartureDateTimeArrayyy = new List<string>();
        string sucess;
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        DepartureDateTimeArrayyy = DepartureDateTimeArrayy;
        for (int j = 0; j < DepartureDateTimeArrayyy.Count; j++)
        {
            string strVale = DepartureDateTimeArrayyy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.Fromloc = arr[3].ToString();
            BALAD.Toloc = arr[4].ToString();
            BALAD.REFNO = arr[5].ToString();
            BALAD.DIRECTIONID = arr[6].ToString();
            DataTable SegmentID = BALAD.GetSegmentID();
            if (SegmentID.Rows.Count > 0)
            {
                BALAD.SegmentID = Convert.ToInt64(SegmentID.Rows[0]["SegmentID"].ToString());
                BALAD.LocationCode = arr[0].ToString();
                BALAD.DepartureDateTime = Convert.ToDateTime(arr[1].ToString());
                BALAD.ArrivalDateTime = Convert.ToDateTime(arr[2].ToString());
                int AirsegmentStopOver = BALAD.InsertAirsegmentStopOver();
            }
        }
        sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirbaggageDetails(string PassengerTypee, int Quantityy, string Unitt, string frombaggsegg, string Tobaggsegg)
    {
        BALBook BALAD = new BALBook();

        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        string success = "";
        BALAD.PassengerType = PassengerTypee;
        BALAD.Quantity = Quantityy;
        BALAD.Unit = Unitt;
        BALAD.Fromloc = frombaggsegg;
        BALAD.Toloc = Tobaggsegg;
        int AirbaggageDetails = BALAD.insertAirbaggageDetails();
        success = "0";
        return success;

    }
    [System.Web.Services.WebMethod]
    public static string ForgotPass(string Email)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.EmailID = Email;
        DataTable dt = bals.GetUserDataEmail();
        if (dt.Rows.Count > 0)
        {
            string PassDecry = bals.Decrypt(dt.Rows[0]["Password"].ToString());
            string FName = dt.Rows[0]["FirstName"].ToString();
            string LName = dt.Rows[0]["LastName"].ToString();
            string EmailTo = dt.Rows[0]["EmailID"].ToString();

            DataTable dt2 = bals.GetEmailTemplate(2);
            if (dt2.Rows.Count > 0)
            {
                string EmailSub = dt2.Rows[0]["Subject"].ToString();
                string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
                EmailBody = EmailBody.Replace("#PersonName#", FName + ' ' + LName);
                EmailBody = EmailBody.Replace("#Password#", PassDecry);
                bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
                Out = "SUCCESS";
            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingPaxInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingPaxInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string GetBookingTransInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingTransInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingBaggageInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingBagInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string Getbaggageinfo(string sequencenu, string combinationi)
    {
        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.BaggageInfo(sequencenu, combinationi, SessionIDFinal);
        return xmlResponse;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string DeletebookingInfo()
    {
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        int DeletebookingInfo = BALAD.DeletebookingInfo();
        string sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod]
    public static string GetIssueTktDataDB()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetIssueTktDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string UpdateTkt(string TicketNumber, string PersonName, string surname, string BirthDate)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);

        List<string> TicketNumberArray = TicketNumber.Split(',').ToList<string>();
        List<string> PersonNameArray = PersonName.Split(',').ToList<string>();
        List<string> surnameArray = surname.Split(',').ToList<string>();
        List<string> BirthDateArray = BirthDate.Split(',').ToList<string>();
        for (int itn = 0; itn < TicketNumberArray.Count; itn++)
        {
            bals.TicketNumber = TicketNumberArray[itn];
            bals.PersonName = PersonNameArray[itn];
            bals.surname = surnameArray[itn];
            bals.BirthDatepax = BirthDateArray[itn];
            int Tkt = bals.UpdateTkt();
        }
        return 1.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string UpdateBookStatus(string bstat)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        bals.BookStatus = bstat;
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int Tkt = bals.UpdateBookStatus();
        return Tkt.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string InsertBookHistory(string bstat)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        bals.BookStatus = bstat;
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int Tkt = bals.InsertBookHistory();
        return Tkt.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string GetSearchData(string DepartureCity, string ArrivalCity, string DepartureDateFrom, string DepartureDateTo, string ArrivalDateFrom, string ArrivalDateTo, string BookingDateFrom, string BookingDateTo, string DeadlineDateFrom, string DeadlineDateTo, string BookingRefNo, string SupplierRefNo, string TicketNo, string PassengerName, string ddlBranch, string ddlStatus)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.DepartureCity = DepartureCity;
        bals.ArrivalCity = ArrivalCity;
        bals.DepartureDateFrom = DepartureDateFrom;
        bals.DepartureDateTo = DepartureDateTo;
        bals.ArrivalDateFrom = ArrivalDateFrom;
        bals.ArrivalDateTo = ArrivalDateTo;
        bals.BookingDateFrom = BookingDateFrom;
        bals.BookingDateTo = BookingDateTo;
        bals.DeadlineDateFrom = DeadlineDateFrom;
        bals.DeadlineDateTo = DeadlineDateTo;
        bals.BookingRefNo = BookingRefNo;
        bals.SupplierRefNo = SupplierRefNo;
        bals.TicketNo = TicketNo;
        bals.PassengerName = PassengerName;
        bals.ddlBranch = Convert.ToInt64(ddlBranch);
        bals.ddlStatus = ddlStatus;

        DataTable dtroles = bals.GetSearchData();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static void SaveDataToSession(string sessionname, string valueses)
    {
        HttpContext.Current.Session[sessionname] = valueses;
    }
    [System.Web.Services.WebMethod]
    public static string SendMailTicketIssue(string EUName, string ETripID, string EPaxData, string UID, string UserMailID, string ETripName, string EFlightLeg, string ETotalCharge, string Type, string ETktAttachMainLeg, string ETktAttachTraveller, string ETktAttachFareBreakup, string ETktBaggageInfo, string DeadlineDiv)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.UserID = Convert.ToInt64(UID);
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            string host2 = HttpContext.Current.Request.Url.Host.ToLower();
            if (host2 == "localhost")
            {
                CCLIST = "shabeerm@oneviewit.com";
            }
            //Checking Voucher Link Availability
            string AttachLink = string.Empty;
            bool voucherlinkavailable = false;
            BALSettings BALSET = new BALSettings();
            if (HttpContext.Current.Session["BookingRefID"] != null)
            {
                BALSET.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
                DataTable dtcheckvou = BALSET.CheckVochurlinkavailable();
                if (dtcheckvou.Rows.Count > 0)
                {//Link Available
                    voucherlinkavailable = true;
                    AttachLink = dtcheckvou.Rows[0]["VoucherUrl"].ToString();
                }
                else
                {
                    voucherlinkavailable = false;
                }
            }

            using (WebClient client = new WebClient())
            {
                string ticks = DateTime.Now.Ticks.ToString();
                string htmlCode = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/bookingmail.html");
                if (voucherlinkavailable == false)
                {
                    string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/ticket_attachment_template.html");
                    ticket_attachment = ticket_attachment.Replace("#ETripID#", ETripID);
                    ticket_attachment = ticket_attachment.Replace("#FLIGHTDETAILS#", ETktAttachMainLeg);
                    ticket_attachment = ticket_attachment.Replace("#PAXDET#", ETktAttachTraveller);
                    ticket_attachment = ticket_attachment.Replace("#FAREBREAKUPS#", ETktAttachFareBreakup);
                    ticket_attachment = ticket_attachment.Replace("#BAGGAGEDATA#", ETktBaggageInfo);


                    string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/TicketAttachments/" + ETripID + "/";
                    if (!Directory.Exists(storePath))
                        Directory.CreateDirectory(storePath);
                    //if (File.Exists(filepathee))
                    //{
                    //    File.Delete(filepathee);
                    //}
                    TextWriter attachhtml = new StreamWriter(HttpContext.Current.Server.MapPath("~/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html"));
                    attachhtml.WriteLine(ticket_attachment);
                    attachhtml.Flush();
                    attachhtml.Close();
                    attachhtml.Dispose();

                    string vouurl = "/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html";
                    BALSET.VoucherUrl = vouurl;
                    string host = HttpContext.Current.Request.Url.Host.ToLower();
                    if (host != "localhost")
                    {
                        int dtVoucher = BALSET.insertVoucherUrlinformation();
                    }
                    AttachLink = vouurl;
                }

                //DataTable dt2 = bals.GetEmailTemplate(1);
                //if (dt2.Rows.Count > 0)
                //{
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EUName#", EUName);
                EmailBody = EmailBody.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EFlightLeg#", EFlightLeg);
                EmailBody = EmailBody.Replace("#EPaxData#", EPaxData);
                EmailBody = EmailBody.Replace("#TRIPNAME#", ETripName);
                if (Type == "TKT")//TKT
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Ticket Issued");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Ticket Attached");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "confirmed");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Your itinerary is attached along with this email");
                }
                else if (Type == "PNR")//PNR
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Trip is Booked");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Trip Information");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "booked");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Please find the trip details below");
                }
                else if (Type == "TKTTOMAIL")//TKTTOMAIL
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", "");
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Itinerary");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Trip Information");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "confirmed");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Your itinerary is attached along with this email");
                    CCLIST = "";
                }
                EmailBody = EmailBody.Replace("#EDeadline#", DeadlineDiv);
                //Out = bals.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub, "~/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html");
                Out = bals.SendMailSubjectwithBcc(UserMailID, CCLIST, "", EmailBody, EmailSub, AttachLink);
                //}
                //Delete Files
                //if (Directory.Exists(storePath))
                //{
                //    //Delete all files from the Directory
                //    foreach (string file in Directory.GetFiles(storePath))
                //    {
                //        File.Delete(file);
                //    }
                //    //Delete a Directory
                //    Directory.Delete(storePath);
                //}
            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string ChangeStatustoHK()
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
            BALAD.BookingStatusCode = "HK";
            BALAD.BOOKDATE = System.DateTime.Now;
            BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
            int successss = BALAD.InsertBookingHistoryDetails();
        }
        return sucess;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingHistory()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingHistory();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CancelBookingMail(string PNRC, string ETripIDD, string EUNamee, string ECommentt)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt2 = bals.GetEmailTemplate(4);
        if (dt2.Rows.Count > 0)
        {
            string EmailSub = dt2.Rows[0]["Subject"].ToString();
            string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
            string EmailTo = dt2.Rows[0]["Recipient"].ToString();

            EmailSub = EmailSub.Replace("#BOOKINGREFID#", ETripIDD);
            EmailSub = EmailSub.Replace("#PNRNUMBER#", PNRC);

            EmailBody = EmailBody.Replace("#USERNAME#", EUNamee);
            EmailBody = EmailBody.Replace("#BOOKINGREFID#", ETripIDD);
            EmailBody = EmailBody.Replace("#PNRNUMBER#", PNRC);
            EmailBody = EmailBody.Replace("#COMMENT#", ECommentt);
            bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string ChangeTravelDateMail(string PNRC, string ETripIDD, string EUNamee, string ECommentt)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt2 = bals.GetEmailTemplate(5);
        if (dt2.Rows.Count > 0)
        {
            string EmailSub = dt2.Rows[0]["Subject"].ToString();
            string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
            string EmailTo = dt2.Rows[0]["Recipient"].ToString();

            EmailSub = EmailSub.Replace("#BOOKINGREFID#", ETripIDD);
            EmailSub = EmailSub.Replace("#PNRNUMBER#", PNRC);

            EmailBody = EmailBody.Replace("#USERNAME#", EUNamee);
            EmailBody = EmailBody.Replace("#BOOKINGREFID#", ETripIDD);
            EmailBody = EmailBody.Replace("#PNRNUMBER#", PNRC);
            EmailBody = EmailBody.Replace("#COMMENT#", ECommentt);
            bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetFareRules()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetFareRules();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string GetFareBreak()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetFareBreak();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertfarerules(List<string> farerulee)
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        //List<string> FareRuleArrayy = new List<string>();
        //FareRuleArrayy = fullFareDataa;
        for (int j = 0; j < farerulee.Count; j++)
        {
            string strVale = farerulee[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.FareRule = arr[0].ToString();
            BALAD.Segment = arr[1].ToString() + "-" + arr[2].ToString();
            BALAD.FareRef = arr[3].ToString(); ;
            BALAD.Operatingairline = arr[4].ToString();
            BALAD.MarketingAirline = arr[5].ToString();
            DataTable FareRule = BALAD.InsertFareRule();
            if (FareRule.Rows.Count > 0)
            {
                sucess = "Success";

            }
            else
            {
                sucess = "Failed";
            }
        }



        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirbaggageDetails(List<string> Airbaggagee)
    {
        BALBook BALAD = new BALBook();
        string success = "";
        List<string> AirbaggageeArrayy = new List<string>();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        AirbaggageeArrayy = Airbaggagee;

        for (int j = 0; j < AirbaggageeArrayy.Count; j++)
        {
            string strVale = AirbaggageeArrayy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.PassengerType = arr[0].ToString();
            BALAD.Quantity = Convert.ToInt32(arr[1].ToString());
            BALAD.Unit = arr[2].ToString();
            BALAD.Fromloc = arr[3].ToString();
            BALAD.Toloc = arr[4].ToString();
            int AirbaggageDetails = BALAD.insertAirbaggageDetails();
        }
        success = "0";
        return success;

    }
    [System.Web.Services.WebMethod]
    public static string GetBookerAgencyDetails()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookerAgencyDetails();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string updatelastticketingdate(string bookingIDD, string CancelDatee)
    {
        string Out = string.Empty;
        BALBook BALAD = new BALBook();
        BALAD.BOOKINGREFID = bookingIDD.ToString();
        BALAD.CANCELDATE = Convert.ToDateTime(CancelDatee.ToString());
        DataTable dtcanceldate = BALAD.Updatelasttktdate();
        if (dtcanceldate.Rows.Count > 0)
        {
            Out = "Success";
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirsegmentStopOver(List<string> DepartureDateTimeArrayy)
    {
        HttpContext.Current.Session["DepartureDateTimeArrayy"] = DepartureDateTimeArrayy;
        string sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string DeleteVoucherUrlinformation()
    {
        BALSettings BALSET = new BALSettings();
        BALSET.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        int Success = BALSET.DeleteVoucherUrlinformation();
        return Success.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string PrintTicket(string ETripID, string ETktAttachMainLeg, string ETktAttachTraveller, string ETktAttachFareBreakup, string ETktBaggageInfo)
    {
        string Out = "NODATA";
        using (WebClient client = new WebClient())
        {
            string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/ticket_attachment_template.html");
            ticket_attachment = ticket_attachment.Replace("#ETripID#", ETripID);
            ticket_attachment = ticket_attachment.Replace("#FLIGHTDETAILS#", ETktAttachMainLeg);
            ticket_attachment = ticket_attachment.Replace("#PAXDET#", ETktAttachTraveller);
            ticket_attachment = ticket_attachment.Replace("#FAREBREAKUPS#", ETktAttachFareBreakup);
            ticket_attachment = ticket_attachment.Replace("#BAGGAGEDATA#", ETktBaggageInfo);
            Out = ticket_attachment;
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CRUDBlockService(string AgencyId, string ServiceId, string Operation)
    {
        BALSettings BALSET = new BALSettings();
        BALSET.AgencyID = Convert.ToInt64(AgencyId);
        BALSET.ServiceTypeID = Convert.ToInt32(ServiceId);
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALSET.Operation = Operation;
        BALSET.BlockDate = DateTime.Now;
        int CrudSuccess = BALSET.CRUDBlockService();
        return CrudSuccess.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string checkServiceBlocked(string AgencyId, string ServiceTypeID)
    {
        int BlockedStat = 1;
        BALSettings BALSET = new BALSettings();
        BALSET.AgencyID = Convert.ToInt64(0);
        BALSET.ServiceTypeID = Convert.ToInt32(ServiceTypeID);
        DataTable dtCheckServiceBlockAll = BALSET.checkServiceBlocked();
        if (dtCheckServiceBlockAll.Rows.Count > 0)
        {
            BlockedStat = 1;
        }
        else
        {
            BALSET.AgencyID = Convert.ToInt64(AgencyId);
            DataTable dtCheckServiceBlockAgency = BALSET.checkServiceBlocked();
            if (dtCheckServiceBlockAgency.Rows.Count > 0)
            {
                BlockedStat = 1;
            }
            else
            {
                BlockedStat = 0;
            }
        }
        return BlockedStat.ToString();
    }
    //NEW 05-10-2017 B2C
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Gettravellerdetails(string User)
    {
        string Out = string.Empty;
        BALBook BALAD = new BALBook();
        BALAD.UID = Convert.ToInt64(User);
        DataTable dttravel = BALAD.Gettravellerdetails();
        if (dttravel.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dttravel.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dttravel.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string CheckUserNewEmail(string NewEmaill)
    {
        string OUT = string.Empty;
        BALBook BALAD = new BALBook();

        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.EMAIL = NewEmaill;
        DataTable dt = BALAD.GetEmailData();
        if (dt.Rows.Count > 0)
        {
            OUT = "SUCCESS";
        }
        else
        {
            OUT = "FAILED";
        }

        return OUT;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string InsertUserNewEmail(string NewEmaill, string Newpasss)
    {
        string OUT = string.Empty;
        BALBook BALAD = new BALBook();
        BALGeneral BALG = new BALGeneral();
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.EMAIL = NewEmaill;
        string PlainPass = Newpasss;
        BALAD.PASSWORDEncrypt = BALG.Encrypt(PlainPass);
        DataTable dt = BALAD.UpdateEmailData();
        if (dt.Rows.Count > 0)
        {

            OUT = "EmailCHANGESUCCESS";
        }
        else
        {
            OUT = "EmailCHANGEFAILED";
        }

        return OUT;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string UpdateUserPasswordB2C(string CurrentPass, string NewPass, string ConfPass)
    {
        string OUT = string.Empty;
        BALSettings BALSET = new BALSettings();
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = BALSET.GetUserDataB2C();
        if (dt.Rows.Count > 0)
        {
            string DBPass = dt.Rows[0]["Password"].ToString();
            string DBPassDecrypt = BALSET.Decrypt(DBPass);
            if (DBPassDecrypt == CurrentPass)
            {
                BALSET.NewPass = BALSET.Encrypt(NewPass);
                int AirSegment = BALSET.UpdateUserPass();
                OUT = "PASSCHANGESUCCESS";
            }
            else
            {
                OUT = "PASSCHANGEFAILED";
            }
        }
        return OUT;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AddTravellerdetails(string NamePrefixx, string FNames, string Surnamee, string BirthDatee, string Passportnoo, string ExpireDatee, string IssueCountryy)
    {
        string Out = string.Empty;
        BALBook BALAD = new BALBook();
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.TITLE = NamePrefixx;
        BALAD.Gender = Getgender(BALAD.TITLE);
        BALAD.FNAME = FNames;
        BALAD.SNAME = Surnamee;
        BALAD.DOB = Convert.ToDateTime(BirthDatee);
        BALAD.DOCNO = Passportnoo;
        BALAD.DOCEXPIRYDATE = Convert.ToDateTime(ExpireDatee);
        BALAD.DOCISSUECUNTRY = IssueCountryy;
        int dttraveller = BALAD.inserttravellerdetails();
        if (dttraveller == 1)
        {
            Out = "Success";
        }
        return Out;
    }
    public static string Getgender(string TITLE)
    {
        string Out = string.Empty;
        if (TITLE == "1" || TITLE == "5")
        {
            Out = "1";
        }
        else
        {
            Out = "0";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]

    public static string Checkemailexsist(string Emaill)
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        BALAD.EMAIL = Emaill;

        DataTable dtCheckuser = BALAD.Chechingcrntusrexsist();
        if (dtCheckuser.Rows.Count > 0)
        {
            string user = dtCheckuser.Rows[0]["UserID"].ToString();
            sucess = "ExsistUser";
        }
        else
        {
            sucess = "Success";
        }
        return sucess;

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Updatepassengeruserdetails(string Title, string FName, string LName, string MobNo, string address1, string city, string postal, string country)
    {
        BALBook BALAD = new BALBook();
        string Out = string.Empty;
        BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALAD.TITLE = Title;
        BALAD.FNAME = FName;
        BALAD.SNAME = LName;
        BALAD.TELEPHONE = MobNo;
        BALAD.Address1 = address1;
        BALAD.City = city;
        BALAD.ZIPCode = postal;
        BALAD.Country = country;
        DataTable dtupdate = BALAD.upadtepassengerdetails();
        GetUserData(Convert.ToInt64(HttpContext.Current.Session["UserID"]));
        if (dtupdate.Rows.Count > 0)
        {
            Out = "Success";
        }
        return Out;

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Getuserdataforeditprofile(string UID)
    {
        BALBook BALAD = new BALBook();
        string Out = string.Empty;
        BALAD.UID = Convert.ToInt64(UID.ToString());
        DataTable dtuseradd = BALAD.GetUserAddress();
        if (dtuseradd.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtuseradd.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtuseradd.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }

        return Out;

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SendQtationMail(string Subject, string UserMailID, string CClist, string Bcclist, int SType, string SelectQtnDiv, string UID)
    {
        string Out = string.Empty;
        Int64 QID = 0;
        string EmailSub = Subject;
        string EmailBody = "";
        string qtnURL = "";
        BALSettings bals = new BALSettings();

        BALHotelBook BALAD = new BALHotelBook();
        string[] MailAddress = UserMailID.Split(';');
        foreach (string ToEMailId in MailAddress)
        {

            BALAD.Type = Convert.ToInt16("2");
            BALAD.Qstring = SelectQtnDiv;
            BALAD.EmailID = ToEMailId;
            BALAD.ContactNo = "";
            BALAD.CreatedBy = Convert.ToInt64("1");
            BALAD.CreatedDate = DateTime.Now;
            BALAD.Status = Convert.ToInt16("1");
            DataTable qtndata = BALAD.insertQtndiv();

            if (qtndata.Rows.Count > 0)
            {

                QID = Convert.ToInt64(qtndata.Rows[0][0].ToString());

            }


            string AttachLink = string.Empty;
            bool voucherlinkavailable = false;
            //  BALSettingsamaweb BALSET = new BALSettingsamaweb();

            using (WebClient client = new WebClient())
            {
                string ticks = DateTime.Now.Ticks.ToString();
                string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/hotel/emailtemplates/Quotation/pdfconversion.html");
                string htmlCode = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/hotel/emailtemplates/Quotation/Quatation-Emailtemplate.html");


                EmailBody = htmlCode;

                EmailBody = EmailBody.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
                EmailBody = EmailBody.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
                EmailBody = EmailBody.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
                EmailBody = EmailBody.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyNumber"]);

                //EmailBody = EmailBody.Replace("#quataiondata#", SelectQtnDiv);
                // EmailBody = EmailBody.Replace("#ATTACHDET#", "Your Quatation is attached along with this email");

                ticket_attachment = ticket_attachment.Replace("#divdesign#", SelectQtnDiv);
                ticket_attachment = ticket_attachment.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyNumber"]);

                string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/Quatation/";
                if (!Directory.Exists(storePath))
                    Directory.CreateDirectory(storePath);

                TextWriter attachhtml = new StreamWriter(storePath + "Quotation_" + QID + ".html");
                qtnURL = "/HOTEL/Documents/Quatation/" + "Quotation_" + QID + ".html";
                // string qtnURL = "http://websites.oneviewitsolutions.com/PSD/booking.html";

                attachhtml.WriteLine(ticket_attachment);
                attachhtml.Flush();
                attachhtml.Close();
            }
        }
        BALAD.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dtmailID = BALAD.SendMailid();
        string clientEmail = "uaecrt@gmail.com";
        if (dtmailID.Rows.Count > 0)
        {
            clientEmail = dtmailID.Rows[0]["EmailID"].ToString();
        }

        string toaddress = UserMailID.Replace(';', ',');

        if (CClist != "NoData")
        {
            CClist = CClist.Replace(';', ',');
            Bcclist = Bcclist.Replace(';', ',');
        }
        else
        {
            CClist = "";
        }
        if (Bcclist != "NoData")
        {
            Bcclist = Bcclist.Replace(';', ',');
        }
        else
        {
            Bcclist = "";
        }





        Out = bals.SendMailSubjectwithBcc(toaddress, CClist, Bcclist, EmailBody, EmailSub, qtnURL);




        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetCurrencyDetailsbkup()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt = bals.GetCurrencyDetails();
        if (dt.Rows.Count > 0)
        {
            HttpContext.Current.Session["dbCurrencyCode"] = dt.Rows[0]["CurrencyCode"].ToString();
            HttpContext.Current.Session["dbRateOfExchange"] = dt.Rows[0]["RateofExchange"].ToString();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "ERROR";
        }
        return Out;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetCurrencyDetails()
    {
        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = "{\"currencyConversions\":[ { \"nodeId\":74,\"fromCurrency\":\"USD\",\"toCurrency\":\"AED\"} ]}";
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "currency/rates";
        string strResult = "NORESULT";
        string tempResultJSON = "[";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + SessionData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            SessionData = httpResponse.Headers["access_token"].ToString();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                HttpContext.Current.Session["Access_Token"] = SessionData;
                strResult = responseText;
            }
        }
        catch (Exception ex)
        {
        }
        JObject jObject = JObject.Parse(strResult);
        string tempres = "[{\"RateofExchange\":" + jObject["currencyRates"][0]["sellRate"] + ",\"CurrencyCode\":\"AED\"}]";
        return tempres;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string RefreshSessionToken()
    {
        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = "{\"currencyConversions\":[ { \"nodeId\":74,\"fromCurrency\":\"USD\",\"toCurrency\":\"AED\"} ]}";
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "currency/rates";
        string strResult = "NOSESSION";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + SessionData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            SessionData = httpResponse.Headers["access_token"].ToString();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                HttpContext.Current.Session["Access_Token"] = SessionData;
                strResult = SessionData;
            }
        }
        catch (Exception ex)
        { }
        return strResult;
    }

}
