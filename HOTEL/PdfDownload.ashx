﻿<%@ WebHandler Language="C#" Class="PdfDownload" %>

using System;
using System.Configuration;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using SelectPdf;
using System.Net;
using Newtonsoft.Json;

public class PdfDownload : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        //try
        //{
        string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

        //deserialize the object
        userInfo objUsr = Deserialize<userInfo>(strJson);
        if (objUsr != null)
        {
            string fullName = objUsr.Data;
            string ticks = DateTime.Now.Ticks.ToString();
            using (WebClient client = new WebClient())
            {
                string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/hotel/emailtemplates/Quotation/pdfconversion.html");
                ticket_attachment = ticket_attachment.Replace("#divdesign#", fullName);
                ticket_attachment = ticket_attachment.Replace("#BackgroundImg#", ConfigurationManager.AppSettings["BackgroundImg"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyLogo#", ConfigurationManager.AppSettings["AgencyLogo"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyTitle#", ConfigurationManager.AppSettings["AgencyTitle"]);
                ticket_attachment = ticket_attachment.Replace("#AgencyNumber#", ConfigurationManager.AppSettings["AgencyNumber"]);



                string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/QuatationPDF";
                if (!Directory.Exists(storePath))
                    Directory.CreateDirectory(storePath);
                TextWriter attachhtml = new StreamWriter(storePath + "/Quatation_pdf_" + ticks + ".html");
                attachhtml.WriteLine(ticket_attachment);
                attachhtml.Flush();
                attachhtml.Close();
            }
            string url = "Documents/QuatationPDF/Quatation_pdf_" + ticks + ".html";
            string rturnurl = "Documents/QuatationPDF/Quatation_pdf_" + ticks + ".pdf";
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;

            converter.Options.WebPageWidth = 1024;
            converter.Options.WebPageHeight = 0;
            converter.Options.WebPageFixedSize = false;
            PdfDocument doc = converter.ConvertUrl(HttpContext.Current.Server.MapPath(url));

            string path = HttpContext.Current.Server.MapPath("") + @"\Documents\QuatationPDF\Quatation_pdf_" + ticks + ".pdf";
            doc.Save(path);



            context.Response.Write(JsonConvert.SerializeObject(new { rturnurl = rturnurl}));


            context.Response.End();
        }
        else
        {
            context.Response.Write("No Data");
        }
        //}
        //    catch (Exception ex)
        //    {
        //        context.Response.Write("Error :" + ex.Message);
        //    }
    }
    public class userInfo
    {
        public string Data { get; set; }

    }
    public T Deserialize<T>(string context)
    {
        string jsonData = context;

        //cast to specified objectType
        var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
        return obj;
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}