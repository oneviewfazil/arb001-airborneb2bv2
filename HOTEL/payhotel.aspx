﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="payhotel.aspx.cs" Inherits="payhotel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="/COMMON/css/responsive.css" rel="stylesheet" type="text/css">
     <script type="text/javascript" src="http://code.jquery.com/jquery-2.2.0.js"></script>
          <script src="/COMMON/js/tabs.js"></script>
          <script src="/COMMON/JS/pagecontents.js"></script>
          <script src="/COMMON/JS/Sessionjs.js"></script>
          <script src="/COMMON/JS/alertify.min.js?QVer=1904171100"></script>

    <link href="/COMMON/JS/alertify.min.css" rel="stylesheet" />
    <link href="/COMMON/JS/default.min.css" rel="stylesheet" />

          <script src="/COMMON/JS/moment.min.js"></script>
          <script src="/COMMON/JS/xml2json.js?QVer=1904171100" defer></script>
          <script src="/COMMON/JS/underscore-min.js?QVer=1904171100" defer></script>
          <%--<script src="oneconnect/hotel/jshotel/hotelpax.js?qver=30072017"></script>--%>
<script>
    var DateFormate = 'dd/mm/yy';
    var DateFormate = 'mm/dd/yy';
    var cin = '';
    var cout = '';
    var night = '';
    var numroom = '';
    var rooms = '';
    var roomcategoryid = '';
    var CityCode = '';
    var HotelCode = '';
    var JBookRes = '';
    var jsonseq = '';
    var CinMDY = "";
    var CurrencyStat = 'USD';
    var CancellationGPeriod = parseInt(2);
    //var MarkupValue = parseFloat(0);
    var RoomCatType = '';
    var GMapKey = 'AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58';
    var Thumbimage = '';
    var TotalPrice = '';
    var RoomDes = '';
    var HotelAddress = '';
    var AgencyTypeIDD = '';
    var CreditLimittAmount = parseFloat(0);
    var extracot = '';
    var cotdaetails = '';
    var typeofroom = '';
    var EmailID = '';
    var EUName = '';
    var MailHotelName = '';
    var MailCityName = '';
    var MailPax = [];
    var EssntalInfo = '';
    var CountryCode = '';
    var ExpectedPrice = '';
    var OnRequest = '';
    var FirstName = '';
    var Email = '';
    var SurName = '';
    var PhoneNumber = '';
    var Title = '';
    var FNames = '';
    var NamePrefixx = '';
    var Surnamee = '';
    //$(document).ready(function () {
    //    if ($("#hdcheck").val() == "1")
    //    {
    //        confirm();
    //    }
        
    //});
    function confirm()
    {
        SessionCheckingPrice('HotelPrice');
      
       
    }
    function SessionCheckingPrice(session) {

        var Dastasseq = {
            sesname: session
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/GetSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SessionSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
        function SessionSuccess(data) {
            if (data.d == 'NOSESSION') {
                window.location.href = "/searchhotel.html";
            }
            else {
                var userdata = $.parseJSON(data.d);
                TotalPrice = userdata;
                cin = window.localStorage.getItem('cin');
                cout = window.localStorage.getItem('cout');
                night = window.localStorage.getItem('night');
                numroom = window.localStorage.getItem('numroom');
                rooms = window.localStorage.getItem('rooms');
                roomcategoryid = window.localStorage.getItem("roomcatid");
                CityCode = window.localStorage.getItem("CityCode");
                HotelCode = window.localStorage.getItem("HotelCode");
                RoomCatType = window.localStorage.getItem("RoomCatType");
                Thumbimage = window.localStorage.getItem("HotelImage");
                //TotalPrice = window.localStorage.getItem("HotelPrice");
                RoomDes = window.localStorage.getItem("roomdescription");
                typeofroom = window.localStorage.getItem("selectedrooms");
                CountryCode = window.localStorage.getItem("nation");
                OnRequest = window.localStorage.getItem("roomStatus");
                FirstName = window.localStorage.getItem("FirstName");
                Email = window.localStorage.getItem("Email");
                SurName = window.localStorage.getItem("SurName");
                PhoneNumber = window.localStorage.getItem("PhoneNumber");
                Title = window.localStorage.getItem("Title");
                FNames = window.localStorage.getItem("FNames");
                NamePrefixx = window.localStorage.getItem("NamePrefixx");
                Surnamee = window.localStorage.getItem("Surnamee");
                ExpectedPrice = window.localStorage.getItem("ExpectedPrice");
                HotelAddress = window.localStorage.getItem("HotelAddress");
                hotelbookconfirm();
            }
        }
    }
    function hotelbookconfirm()
    {
       
        var Datas = {
            cin: cin,
            cout: cout,
            night: night,
            numroom: numroom,
            rooms: rooms,
            roomcategoryid: roomcategoryid,
            CityCode: CityCode,
            HotelCode: HotelCode,
            FNames: FNames,
            NamePrefixx: NamePrefixx,
            Surnamee: Surnamee,
            CountryCode: CountryCode,
            ExpectedPrice: ExpectedPrice,
            FirstName: FirstName,
            SurName: SurName,
            Email: Email,
            PhoneNumber: PhoneNumber,
            Title: Title


        }
        $.ajax({
            type: "POST",
            url: "WebserviceHotel.aspx/BookHotel",
            data: JSON.stringify(Datas),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: populateBookInfo,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    function populateBookInfo(data) {
        var xmlbookresp = data.d;
        var x2js2 = new X2JS();
        var jsonHotelRes = x2js2.xml_str2json(xmlbookresp);
        console.log(jsonHotelRes);
        if (jsonHotelRes != '' && jsonHotelRes != null) {
            JBookRes = jsonHotelRes.Response.ResponseDetails.BookingResponse;
            console.log(JBookRes);
            var Error = JBookRes.Errors;
            if (Error == undefined) {
                GetgtaNumber();
                setTimeout(function () {
                    var bookpasscount = JBookRes.BookingItems.BookingItem.length;

                    if (bookpasscount == undefined) {
                        var Paxname = JBookRes.BookingName;
                        var RefNo = JBookRes.BookingItems.BookingItem.ItemConfirmationReference;
                        var ClinentID = JBookRes.BookingReferences.BookingReference["0"].__cdata;
                        var ApiID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                        var BookingDate = JBookRes.BookingCreationDate
                        var CheckinDate = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckInDate;
                        var StatusCode = JBookRes.BookingStatus._Code;
                        var Status = JBookRes.BookingItems.BookingItem.ItemStatus.__cdata;
                        var CheckOut = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckOutDate;
                        var gross = JBookRes.BookingPrice._Gross;
                        var Net = JBookRes.BookingPrice._Nett;
                        var Currency = JBookRes.BookingPrice._Currency;
                        var HotelName = JBookRes.BookingItems.BookingItem.Item.__cdata;
                        HotelName = HotelName.toUpperCase();
                        var SupplyID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                        var Checkin = moment(CheckinDate);
                        var CheckOutt = moment(CheckOut);
                        var DifferenceMilli = CheckOutt - Checkin;
                        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                        var Night = DifferenceMilliDuration.days();
                        // var Totel = parseFloat(parseFloat(gross) * parseFloat(Night));
                        var Totel = parseFloat(parseFloat(gross));
                        var RoomType = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.Description;
                        var Adult = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Adults;
                        var Child = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Children;
                        var Brkfast = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Basis.__cdata;
                        if (Brkfast != "None") {
                            var Full = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Breakfast.__cdata;
                        }
                        else {
                            var Full = "";
                            var Brkfast = "Rooms Only";
                        }

                        var ItemRef = JBookRes.BookingItems.BookingItem.ItemReference;

                    }
                    else {
                        var Paxname = JBookRes.BookingName;
                        var RefNo = JBookRes.BookingItems.BookingItem[0].ItemConfirmationReference;
                        var ClinentID = JBookRes.BookingReferences.BookingReference["0"].__cdata;
                        var ApiID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                        var BookingDate = JBookRes.BookingCreationDate
                        var StatusCode = JBookRes.BookingStatus._Code;
                        var CheckinDate = JBookRes.BookingItems.BookingItem[0].HotelItem.PeriodOfStay.CheckInDate;
                        var Status = JBookRes.BookingItems.BookingItem[0].ItemStatus.__cdata;
                        var CheckOut = JBookRes.BookingItems.BookingItem[0].HotelItem.PeriodOfStay.CheckOutDate;
                        var gross = JBookRes.BookingPrice._Gross;
                        var Net = JBookRes.BookingPrice._Nett;
                        var Currency = JBookRes.BookingPrice._Currency;
                        var HotelName = JBookRes.BookingItems.BookingItem[0].Item.__cdata;
                        HotelName = HotelName.toUpperCase();
                        var SupplyID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                        var Checkin = moment(CheckinDate);
                        var CheckOutt = moment(CheckOut);
                        var DifferenceMilli = CheckOutt - Checkin;
                        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                        var Night = DifferenceMilliDuration.days();
                        var Totel = parseFloat(parseFloat(gross));

                        for (var ic = 0; ic < bookpasscount; ic++) {
                            var val = ic;
                            val = parseFloat(parseFloat(val) + parseFloat(1));
                            var RoomType = val + ". " + JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.Description;
                            var Adult = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Adults;
                            var Child = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Children;
                            var Brkfast = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Basis.__cdata;
                            if (Brkfast != "None") {
                                var Full = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Breakfast.__cdata;
                            }
                            else {
                                var Full = "";
                                var Brkfast = "Rooms Only";
                            }

                            var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId["0"];
                            var PNameCount = JBookRes.PaxNames.PaxName;
                            var PaxN = _.where(PNameCount, { "_PaxId": PaxID });
                            var Name = PaxN[0].__cdata;
                            var ItemRef = JBookRes.BookingItems.BookingItem[ic].ItemReference;

                        }
                    }
                    var Commi = JBookRes.BookingPrice._Commission;
                    var Currency = JBookRes.BookingPrice._Currency;
                    var Gross = JBookRes.BookingPrice._Gross;
                    var Net = JBookRes.BookingPrice._Nett;
                    setTimeout(function () {
                        inserttblBookingHotelInformation(ClinentID, ApiID, CheckinDate, CheckOut, StatusCode, Commi, Currency, Gross, Net)

                    }, 3000);
                }, 3000);
            }
            else {
                $(".loadingGIF").hide();
                var errdata = JBookRes.Errors.Error.ErrorText;
                alertify.alert("Please note", "" + errdata + "! please select another room category",
    function () {
        //window.location.href = "searchhotel.html";
        window.location.href = "hotelbookinginfo.html";
        
    });
            }
        }
        else {
            $.ajax({
                type: "POST",
                url: "WebMethodDbHotel.aspx/Getbookingsession",
                contentType: "application/json; charset=utf-8",
                dataType: "json",

                success: GetbookingsessionSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }

    }

    function GetgtaNumber() {
        var Dastasseq = {
            CountryCode: CountryCode

        };
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/GetGtaAOTNumbers",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: GetGtaAOTNumbersSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //alert('500');
                }
            }
        });

    }
    function GetGtaAOTNumbersSuccess(data) {
        var xmlbookresp = data.d;
        var x2js2 = new X2JS();
        var jsonGtaAOTNumbers = x2js2.xml_str2json(xmlbookresp);
        console.log(jsonGtaAOTNumbers);
        if (jsonGtaAOTNumbers != null && jsonGtaAOTNumbers != '') {
            var Desti = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.length;
            if (Desti != undefined) {
                for (var iDes = 0; iDes < Desti; iDes++) {
                    var DestCode = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes]._Code;
                    var AssistanceLanguage = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.length;
                    if (AssistanceLanguage == undefined) {
                        var OfficeHours = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.OfficeHours
                        var OfficeLocation = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.OfficeLocation;
                        var Language = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.Language.__cdata;
                        var International = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.International;
                        var Local = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.Local;
                        var National = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.National;
                        var OutOfOffice = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.OutOfOffice;
                        insertGtaAOTNumber(DestCode, OfficeHours, OfficeLocation, Language, International, Local, National, OutOfOffice);
                    }
                    else {
                        for (var iAssi = 0; iAssi < AssistanceLanguage; iAssi++) {
                            var OfficeHours = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].OfficeHours
                            var OfficeLocation = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.OfficeLocation;
                            var Language = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].Language.__cdata;
                            var International = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.International;
                            var Local = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.Local;
                            var National = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.National;
                            var OutOfOffice = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination[iDes].Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.OutOfOffice;
                            insertGtaAOTNumber(DestCode, OfficeHours, OfficeLocation, Language, International, Local, National, OutOfOffice);
                        }
                    }


                }
            }
            else {
                var DestCode = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination._Code;
                var AssistanceLanguage = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.length;
                if (AssistanceLanguage == undefined) {
                    var OfficeHours = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.OfficeHours
                    var OfficeLocation = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.OfficeLocation;
                    var Language = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.Language.__cdata;
                    var International = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.International;
                    var Local = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.Local;
                    var National = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.National;
                    var OutOfOffice = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage.AOTNumbers.OutOfOffice;
                    insertGtaAOTNumber(DestCode, OfficeHours, OfficeLocation, Language, International, Local, National, OutOfOffice);
                }
                else {
                    for (var iAssi = 0; iAssi < AssistanceLanguage; iAssi++) {
                        var OfficeHours = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].OfficeHours
                        var OfficeLocation = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.OfficeLocation;
                        var Language = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].Language.__cdata;
                        var International = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.International;
                        var Local = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.Local;
                        var National = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.National;
                        var OutOfOffice = jsonGtaAOTNumbers.Response.ResponseDetails.SearchAOTNumberResponse.ContactDetails.Destination.Offices.Office.AssistanceLanguages.AssistanceLanguage[iAssi].AOTNumbers.OutOfOffice;
                        insertGtaAOTNumber(DestCode, OfficeHours, OfficeLocation, Language, International, Local, National, OutOfOffice);
                    }
                }
            }
        }


    }
    function insertGtaAOTNumber(DestCode, OfficeHours, OfficeLocation, Language, International, Local, National, OutOfOffice) {
        var Dastasseq = {
            DestCode: DestCode,
            OfficeHours: OfficeHours,
            OfficeLocation: OfficeLocation,
            Language: Language,
            International: International,
            Local: Local,
            National: National,
            OutOfOffice: OutOfOffice
        };
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/insertGtaAOTNumber",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: insertGtaAOTNumberSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //alert('500');
                }
            }
        });

    }
    function insertGtaAOTNumberSuccess() {

    }
    function inserttblBookingHotelInformation(ClinentID, ApiID, CheckinDate, CheckOut, StatusCode, Commi, Currency, Gross, Net) {
        var Datass = {

            ClinentID: ClinentID,
            ApiID: ApiID,
            CheckinDate: CheckinDate,
            CheckOut: CheckOut,
            StatusCode: StatusCode,
            Commi: Commi,
            Currency: Currency,
            Gross: Gross,
            Net: Net,
            TotalPrice: TotalPrice,
            MarkupValue: MarkupValueHotel
        }
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/BookingHotelInformation",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: BookingHotelInformationSuccess,
            failure: function (response) {
                alert(response.d);

            }
        });

    }
    function BookingHotelInformationSuccess(data) {
        var result = data.d;
        console.log(result);

        //JBookRes = jsonHotelRes.Response.ResponseDetails.BookingResponse;
        var bookpasscount = JBookRes.BookingItems.BookingItem.length;
        var PaxData = JBookRes.PaxNames.PaxName.length;
        if (PaxData != undefined) {
            for (var iP = 0; iP < PaxData; iP++) {
                var PName = JBookRes.PaxNames.PaxName[iP].__cdata;
                var PID = JBookRes.PaxNames.PaxName[iP]._PaxId;
                var strArr = PName.split(" ");
                var PTitle = strArr[0];
                var PFName = strArr[1];
                var PLName = strArr[2];
                var PType = JBookRes.PaxNames.PaxName[iP]._PaxType;
                if (PType != undefined) {
                    if (PType == "child") {
                        var PType = "CHD"
                        var PAge = JBookRes.PaxNames.PaxName[iP]._ChildAge;
                        var PaxTypeAge = PType + "," + PAge;

                    }
                    else if (PType == "infant") {
                        var PaxTypeAge = "INF"

                    }
                    else {
                        var PaxTypeAge = "ADT";
                    }



                }
                else {
                    var PaxTypeAge = "ADT";

                }
                insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge);
            }


        }
        else {
            var PName = JBookRes.PaxNames.PaxName.__cdata;
            var PID = JBookRes.PaxNames.PaxName._PaxId;
            var strArr = PName.split(" ");
            var PTitle = strArr[0];
            var PFName = strArr[1];
            var PLName = strArr[2];
            var PType = JBookRes.PaxNames.PaxName._PaxType;
            if (PType != undefined) {
                if (PType == "child") {
                    var PType = "CHD"
                    var PAge = JBookRes.PaxNames.PaxName._ChildAge;
                    var PaxTypeAge = PType + "," + PAge;

                }
                else if (PType == "infant") {
                    var PaxTypeAge = "INF"

                }
                else {
                    var PaxTypeAge = "ADT";
                }



            }
            else {
                var PaxTypeAge = "ADT";

            }
            insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge);
        }


        if (bookpasscount == undefined) {
            var ItemRef = JBookRes.BookingItems.BookingItem.ItemReference;
            var CityCode = JBookRes.BookingItems.BookingItem.ItemCity._Code;
            var ItemCode = JBookRes.BookingItems.BookingItem.Item._Code;
            var HotelName = JBookRes.BookingItems.BookingItem.Item.__cdata;
            var StatusCode = JBookRes.BookingStatus._Code;
            var ItemReferenceCode = JBookRes.BookingItems.BookingItem.ItemConfirmationReference;
            var CheckinDate = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckInDate;
            var CheckOut = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckOutDate;
            var Noadult = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Adults;
            var NoChild = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Children;
            var Cot = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Cots;
            var RoomCatID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Id;
            var RoomCat = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.Description;
            var Brkfast = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Basis.__cdata;
            if (Brkfast != "None") {
                var Full = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Breakfast.__cdata;
            }
            else {
                var Full = "";
                var Brkfast = "Rooms Only";
            }

            var Sharebedding = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._SharingBedding;
            if (Sharebedding == undefined) {

                Sharebedding = "0";
            }
            var PaxData = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId.length;
            var PaxDatA = [];
            var CancellData = [];
            if (PaxData != undefined) {
                for (var iP = 0; iP < PaxData; iP++) {
                    var PaxID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId[iP];
                    PaxDatA.push(PaxID);

                }

            }
            else {
                var PaxID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId[0];
                PaxDatA.push(PaxID);
            }
            EssntalInfo = '';
            var EssnInfo = [];
            var ESSINFO = JBookRes.BookingItems.BookingItem.EssentialInformation;
            if (ESSINFO != undefined) {
                var EInfo = JBookRes.BookingItems.BookingItem.EssentialInformation.Information.length;

                if (EInfo == undefined) {
                    EssntalInfo = JBookRes.BookingItems.BookingItem.EssentialInformation.Information.Text;
                    var essfrm = JBookRes.BookingItems.BookingItem.EssentialInformation.Information.DateRange.FromDate;
                    var essTo = JBookRes.BookingItems.BookingItem.EssentialInformation.Information.DateRange.ToDate;
                }
                else {
                    for (var ESSi = 0; ESSi < EInfo; ESSi++) {
                        EssntalInfo += JBookRes.BookingItems.BookingItem.EssentialInformation.Information[ESSi].Text;
                    }
                    var essfrm = JBookRes.BookingItems.BookingItem.EssentialInformation.Information[0].DateRange.FromDate;
                    var essTo = JBookRes.BookingItems.BookingItem.EssentialInformation.Information[0].DateRange.ToDate;
                }

                var Ess = EssntalInfo + "," + essfrm + "," + essTo;
                EssnInfo.push(Ess);
            }

            var Allowble = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[1]._Allowable;
            var Type = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[1]._Type;
            var PassTypeAllow = JBookRes.BookingItems.BookingItem.ChargeConditions.PassengerNameChange._Allowable;
            var PassType = "PassengerNameChange";
            var cancell = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition
            for (var ij = 0; ij < cancell.length; ij++) {
                var Condition1 = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition;

                if (Condition1 != undefined) {
                    if (Condition1.length == undefined) {
                        var FromDate = Condition1._FromDate;
                        var Charge = Condition1._Charge;
                        var ConCurrency = Condition1._Currency;
                        var ToDate = Condition1._ToDate;
                        var ChargeAmount = Condition1._ChargeAmount;
                        if (ToDate == undefined) {
                            //FromDate = new Date();
                            //FromDate = moment(FromDate).format();
                            //ToDate = Condition1._FromDate;
                            ToDate = new Date();
                            ToDate = moment(ToDate).format();
                        }
                        var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                        CancellData.push(FullInfo);
                    }
                    else {
                        for (var ih = 0; ih < Condition1.length; ih++) {
                            var FromDate = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._FromDate;
                            var ToDate = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;
                            var Charge = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._Charge
                            var ConCurrency = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._Currency;
                            if (ToDate == undefined) {
                                //FromDate = new Date();
                                //FromDate = moment(FromDate).format();
                                //ToDate = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._FromDate;;
                                ToDate = new Date();
                                ToDate = moment(ToDate).format();
                            }
                            else {
                                ToDate = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;

                            }
                            var ChargeAmount = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                            if (ChargeAmount == undefined) {
                                var ChargeAmount = "0.000";

                            }
                            else {
                                var ChargeAmount = JBookRes.BookingItems.BookingItem.ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                            }

                            var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                            CancellData.push(FullInfo);
                        }
                    }

                }

            }
            var Commisssion = JBookRes.BookingItems.BookingItem.ItemPrice._Commission;
            var ItemCurrency = JBookRes.BookingItems.BookingItem.ItemPrice._Currency;
            var ItemGross = JBookRes.BookingItems.BookingItem.ItemPrice._Gross;
            var ItemNet = JBookRes.BookingItems.BookingItem.ItemPrice._Nett;
            insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA, Type, Allowble, PassTypeAllow, PassType, CancellData, Commisssion, ItemCurrency, ItemGross, ItemNet, EssnInfo);


        }
        else {

            for (var ic = 0; ic < bookpasscount; ic++) {
                var ItemRef = JBookRes.BookingItems.BookingItem[ic].ItemReference;
                var CityCode = JBookRes.BookingItems.BookingItem[ic].ItemCity._Code;
                var ItemCode = JBookRes.BookingItems.BookingItem[ic].Item._Code;
                var HotelName = JBookRes.BookingItems.BookingItem[ic].Item.__cdata;
                var StatusCode = JBookRes.BookingStatus._Code;
                var ItemReferenceCode = JBookRes.BookingItems.BookingItem[ic].ItemConfirmationReference;
                var CheckinDate = JBookRes.BookingItems.BookingItem[ic].HotelItem.PeriodOfStay.CheckInDate;
                var CheckOut = JBookRes.BookingItems.BookingItem[ic].HotelItem.PeriodOfStay.CheckOutDate;
                var Noadult = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Adults;
                var NoChild = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Children;
                var Cot = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Cots;
                var RoomCatID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Id;
                var RoomCat = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.Description;
                var Brkfast = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Basis.__cdata;
                if (Brkfast != "None") {
                    var Full = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Breakfast.__cdata;
                }
                else {
                    var Full = "";
                    var Brkfast = "Rooms Only";
                }

                var Sharebedding = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._SharingBedding;
                if (Sharebedding == undefined) {

                    Sharebedding = "0";
                }
                var PaxData = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId.length;
                var PaxDatA = [];
                var CancellData = [];
                if (PaxData != undefined) {
                    for (var iP = 0; iP < PaxData; iP++) {
                        var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId[iP];
                        PaxDatA.push(PaxID);

                    }

                }
                else {
                    var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId[0];
                    PaxDatA.push(PaxID);
                }
                EssntalInfo = '';
                var EssnInfo = [];
                var ESSINFO = JBookRes.BookingItems.BookingItem[ic].EssentialInformation;
                if (ESSINFO != undefined) {
                    var EInfo = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information.length;

                    if (EInfo == undefined) {
                        EssntalInfo = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information.Text;
                        var essfrm = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information.DateRange.FromDate;
                        var essTo = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information.DateRange.ToDate;
                    }
                    else {
                        for (var ESSi = 0; ESSi < EInfo; ESSi++) {
                            EssntalInfo += JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information[ESSi].Text;
                        }
                        var essfrm = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information[0].DateRange.FromDate;
                        var essTo = JBookRes.BookingItems.BookingItem[ic].EssentialInformation.Information[0].DateRange.ToDate;
                    }

                    var Ess = EssntalInfo + "," + essfrm + "," + essTo;
                    EssnInfo.push(Ess);
                }
                var Allowble = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[1]._Allowable;
                var Type = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[1]._Type;
                var PassTypeAllow = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.PassengerNameChange._Allowable;
                var PassType = "PassengerNameChange";
                var cancell = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition
                for (var ij = 0; ij < cancell.length; ij++) {
                    var Condition1 = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition;

                    if (Condition1 != undefined) {
                        if (Condition1.length == undefined) {
                            var FromDate = Condition1._FromDate;
                            var Charge = Condition1._Charge;
                            var ConCurrency = Condition1._Currency;
                            var ToDate = Condition1._ToDate;
                            var ChargeAmount = Condition1._ChargeAmount;
                            if (ToDate == undefined) {
                                //  ToDate = Condition1._FromDate;
                                ToDate = new Date();
                                ToDate = moment(ToDate).format();
                            }
                            var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                            CancellData.push(FullInfo);
                        }
                        else {
                            for (var ih = 0; ih < Condition1.length; ih++) {


                                var FromDate = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._FromDate;
                                var ToDate = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;
                                var Charge = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._Charge
                                var ConCurrency = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._Currency;
                                if (ToDate == undefined) {
                                    //ToDate = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._FromDate;
                                    //FromDate = new Date();
                                    // FromDate = moment(FromDate).format();
                                    ToDate = new Date();
                                    ToDate = moment(ToDate).format();
                                }
                                else {
                                    ToDate = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;

                                }
                                var ChargeAmount = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                                if (ChargeAmount == undefined) {
                                    var ChargeAmount = "0.000";

                                }
                                else {
                                    var ChargeAmount = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                                }
                                if (ConCurrency == undefined) {
                                    var ConCurrency = JBookRes.BookingItems.BookingItem[ic].ChargeConditions.ChargeCondition[ij].Condition[0]._Currency;

                                }
                                var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                                CancellData.push(FullInfo);
                            }
                        }

                    }

                }
                var Commisssion = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Commission;
                var ItemCurrency = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Currency;
                var ItemGross = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Gross;
                var ItemNet = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Nett;

                insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA, Type, Allowble, PassTypeAllow, PassType, CancellData, Commisssion, ItemCurrency, ItemGross, ItemNet, EssnInfo);


            }
        }
    }
    function insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA, Type, Allowble, PassTypeAllow, PassType, CancellData, Commisssion, ItemCurrency, ItemGross, ItemNet, EssnInfo) {
        var Datass = {

            result: result,
            ItemRef: ItemRef,
            CityCode: CityCode,
            ItemCode: ItemCode,
            HotelName: HotelName,
            StatusCode: StatusCode,
            ItemReferenceCode: ItemReferenceCode,
            CheckinDate: CheckinDate,
            CheckOut: CheckOut,
            Noadult: Noadult,
            NoChild: NoChild,
            Cot: Cot,
            RoomCatID: RoomCatID,
            RoomCat: RoomCat,
            Brkfast: Brkfast,
            Full: Full,
            Sharebedding: Sharebedding,
            PaxDatA: PaxDatA,
            Type: Type,
            Allowble: Allowble,
            PassTypeAllow: PassTypeAllow,
            PassType: PassType,
            CancellData: CancellData,
            Commisssion: Commisssion,
            ItemCurrency: ItemCurrency,
            ItemGross: ItemGross,
            ItemNet: ItemNet,
            MarkupValue: MarkupValueHotel,
            HotelAddress: HotelAddress,
            EssnInfo: EssnInfo

        }
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/BookingHotelRoomsInformation",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: BookingHotelRoomsInformation,
            failure: function (response) {
                alert(response.d);

            }
        });
    }
    function BookingHotelRoomsInformation(data) {
        var HID = data.d;
        console.log(HID);
        setTimeout(function () {
            window.localStorage.setItem("HotelPax", "true");

            window.location.href = "hotelbookinginfo.html";
        }, 3000);
    }
    function insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge) {
        var Datass = {
            result: result,
            PID: PID,
            PTitle: PTitle,
            PFName: PFName,
            PLName: PLName,
            PaxTypeAge: PaxTypeAge

        }
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/HotelPax",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: HotelPax,
            failure: function (response) {
                alert(response.d);

            }
        });

    }
    function HotelPax() {

    }
    function insertBookingHotelRoomAmendmentStatus(HID, Type, Allowble, PassTypeAllow, PassType) {
        var Datass = {

            HID: HID,
            Type: Type,
            Allowble: Allowble,
            PassTypeAllow: PassTypeAllow,
            PassType: PassType

        }
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/BookingHotelRoomAmendmentStatus",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: BookingHotelRoomAmendmentStatus,
            failure: function (response) {
                alert(response.d);

            }
        });

    }
    function BookingHotelRoomAmendmentStatus() {

    }

    function GetbookingsessionSuccess(data) {
        var data = data.d;
        if (data != '') {
            $(".loadingGIF").hide();
            sendmailifnotbook(data);
            alertify.alert("Please note", "Please note that the booking might be pending with the supplier.Please do not do duplicated booking.Please contact admin to make sure about the booking.And the reference ID is" + data + "",
             function () {
                 window.location.href = "searchhotel.html";
             });
        }
        else {
            $(".loadingGIF").hide();
            alertify.alert("Please note", "Please note that the booking might be pending with the supplier.Please do not do duplicated booking.Please contact admin to make sure about the booking.",
             function () {
                 window.location.href = "searchhotel.html";
             });
        }

    }
    function sendmailifnotbook(data) {
        var moredetails = '';
        var splitRoomDes = RoomDes.split(',');
        var splitadult = rooms.split('/');
        alert(splitadult);
        EMailID = EMail;
        var MailPax = window.localStorage.getItem("MailPax");
        EUName = EMailID.substring(0, EMailID.lastIndexOf("@"));
        for (var iDes = 0; iDes < splitRoomDes.length; iDes++) {
            var splitadt = splitadult[iDes].split(',');
            var adult = splitadt[0].split('_');
            var child = splitadt[1].split('_');
            moredetails += '<tr><td>' + splitRoomDes[iDes] + '</td> <td>' + adult[1] + '</td> <td>' + child[1] + '</td> <td>' + MailPax[iDes] + '</td></tr>';
            alert(moredetails);
        }
        var MailCityName = window.localStorage.getItem("MailCityName");
       
        var Dastasseq = {
            data: data,
            UserMailID: EmailID,
            EUName: EUName,
            MailHotelName: HotelName,
            cin: cin,
            cout: cout,
            MailCityName: MailCityName,
            night: night,
            moredetails: moredetails,
            TotalPrice: TotalPrice
        };
        console.log(Dastasseq);
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/SendMailbookingnotpossiable",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SendMailbookingnotpossiableSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //alert('500');
                }
            }
        });
        console.log(Dastasseq);
    }
    function SendMailbookingnotpossiableSuccess() {

    }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <input id="hdcheck"  runat="server" type="hidden" />
          <!--loader-->
    

        <!--load-->
        <div id="pageloader" class="popcont06 loadingGIF">
        <div style="width: 100%; height: 100%; background: #FFF; position: fixed; left: 0;"></div>
        <div class="pop_cont">
            <div class="mainpageloaderfull02">
                <h2 style="margin-bottom: 7px;"><img src="/activity/sightseeing/images/hotelbookgif.gif" alt="loading_hotels" width="125px" /></h2>
                <h3>PLEASE WAIT...</h3>
                <p>We are processing your booking request!</p>
               <p>Do not close or refresh the page !</p>
            </div>
        </div>
    </div>
 <!--load-->




    <!--Booking loader-->
    </div>
    </form>
</body>
</html>
