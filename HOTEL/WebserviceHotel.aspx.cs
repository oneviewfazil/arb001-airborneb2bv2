﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class WebserviceHotel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    //Shabeer
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string HotelSearchResponse(string nation, string city, string cin, string cout, string night, string numroom, string rooms)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.HotelSearch(nation, city, cin, cout, night, numroom, rooms);
    }
    [System.Web.Services.WebMethod]
    public static string GetStaticXmlHotel(string CityCode, string HotelCode)
    {
        string StaticData = "NODATA";
        try
        {
            StaticData = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/oneconnect/hotel/StaticData/GTAHotels/" + CityCode + "_" + HotelCode + ".xml"));
        }
        catch (Exception ex)
        {
            StaticData = "NODATA";
        }
        return StaticData;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetHotelInfoGta(string CityCode, string HotelCode)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.GetHotelInfoGtaa(CityCode, HotelCode);
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveHotelSequence(string SelHotel)
    {
        HttpContext.Current.Session["SelectedHotel"] = SelHotel;
    }
    [System.Web.Services.WebMethod]
    public static string GetMarkupDataHotel(Int64 AgencyID, int ServiceTypeID)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.AgencyID = AgencyID;
        bals.ServiceTypeID = ServiceTypeID;
        DataTable dt = bals.GetMarkupDetails();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOMARKUP";
        }
        HttpContext.Current.Session["MarkupDataHotel"] = Out;
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetSumDepoCreditPayment(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        DataTable dt = baldbhot.GetSumDepoCreditPayment();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingStatCount(Int64 AgencyID, int ServiceTypeId)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        baldbhot.ServiceTypeId = ServiceTypeId;
        DataTable dt = baldbhot.GetBookingStatCount();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetActionReqFlight(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        DataTable dt = baldbhot.GetActionReqFlight();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetActionReqHotel(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        DataTable dt = baldbhot.GetActionReqHotel();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void ClearAllSession()
    {
        HttpContext.Current.Session.Clear();
    }
    //Durga
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetHotelSequence()
    {
        string Selectflight = HttpContext.Current.Session["SelectedHotel"].ToString();
        return Selectflight;
    }

    //public static string BookHotel(string cin, string cout, string night, string numroom, string rooms, string roomcategoryid, string CityCode, string HotelCode, string MName, string MLName, string Prefix, string FNames, string NamePrefixx, string Surnamee, string AreaCityCodeArra, string PhoneNumberArra, string EmailArra)
    //{
    //    SendHotelWebService hws = new SendHotelWebService();
    //    List<string> FNamesArray = FNames.Split(',').ToList<string>();
    //    List<string> NamePrefixArray = NamePrefixx.Split(',').ToList<string>();
    //    List<string> SurnameArray = Surnamee.Split(',').ToList<string>();
    //    List<string> RoomsArray = rooms.Split('/').ToList<string>();
    //    List<string> roomcategoryidArray = roomcategoryid.Split(',').ToList<string>();
    //    string CityCodee = CityCode;
    //    string HotelCodee = HotelCode;
    //    string Checkin = cin;
    //    string Checkout = cout;
    //    string NoofNights = night;
    //    string NoofRoom = numroom;
    //    string Name = MName;
    //    string LName = MLName;
    //    string MPrefix = Prefix;
    //    string AreaCityCodeArray = AreaCityCodeArra;
    //    string PhoneNumberArray = PhoneNumberArra;
    //    string EmailArray = EmailArra;
    //    return hws.HotelBook(FNamesArray, NamePrefixArray, SurnameArray, RoomsArray, roomcategoryidArray, CityCodee, HotelCodee, Checkin, Checkout, NoofNights, NoofRoom, Name, LName, MPrefix, AreaCityCodeArray, PhoneNumberArray, EmailArray);
    //}

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotel(string cin, string cout, string night, string numroom, string rooms, string roomcategoryid, string CityCode, string HotelCode, string FNames, string NamePrefixx, string Surnamee, string CountryCode, List<string> ExpectedPrice)
    {
        SendHotelWebService hws = new SendHotelWebService();
        List<string> FNamesArray = FNames.Split(',').ToList<string>();
        List<string> NamePrefixArray = NamePrefixx.Split(',').ToList<string>();
        List<string> SurnameArray = Surnamee.Split(',').ToList<string>();
        List<string> RoomsArray = rooms.Split('/').ToList<string>();
        List<string> roomcategoryidArray = roomcategoryid.Split(',').ToList<string>();
        List<string> ExpectedPriceArray = ExpectedPrice;
        string CityCodee = CityCode;
        string HotelCodee = HotelCode;
        string Checkin = cin;
        string Checkout = cout;
        string NoofNights = night;
        string NoofRoom = numroom;
        return hws.HotelBook(FNamesArray, NamePrefixArray, SurnameArray, RoomsArray, roomcategoryidArray, CityCodee, HotelCodee, Checkin, Checkout, NoofNights, NoofRoom, CountryCode, ExpectedPriceArray);

    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotelAvailabiltycheck(string cin, string night, string numroom, string rooms, string roomcategoryid, string CityCode, string HotelCode)
    {
        SendHotelWebService hws = new SendHotelWebService();
        return hws.HotelFinalAvailabilityCheck(cin, night, numroom, rooms, roomcategoryid, CityCode, HotelCode);
    }

}