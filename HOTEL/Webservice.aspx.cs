﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Webservice : System.Web.UI.Page
{
    SendWebServiceRequest SWSR = new SendWebServiceRequest();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static string Checkfarerules(string sequencenu, string PassTyp, string combinationi, string session)
    {
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();

        string xmlResponse = SWSR.FlightRules(sequencenu, PassTyp, combinationi, session);

        return xmlResponse;
    }
    [System.Web.Services.WebMethod]
    public static string Getbaggageinfo(string sequencenu, string combinationi, string session)
    {
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.BaggageInfo(sequencenu, combinationi, session);
        return xmlResponse;
    }
    [System.Web.Services.WebMethod]
    public static string Createticket(string PN, int passcoun, string passtyp, string passdat, string fnam, string nameprefi, string surnam, string birthdate, string passpo, string passexpir, string issuedat, string session)
    {
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        int PassTypeCount = passcoun;
        string PassTypeData = passtyp;
        List<string> PassTypeDataArray = PassTypeData.Split(',').ToList<string>();
        string PassQuantData = passdat;
        List<string> PassQuantDataArray = PassQuantData.Split(',').ToList<string>();
        string FNames = fnam;
        List<string> FNamesArray = FNames.Split(',').ToList<string>();
        string NamePrefix = nameprefi;
        List<string> NamePrefixArray = NamePrefix.Split(',').ToList<string>();
        string Surname = surnam;
        List<string> SurnameArray = Surname.Split(',').ToList<string>();
        string BirthDate = birthdate;
        List<string> BirthDateArray = BirthDate.Split(',').ToList<string>();
        string Passportno = passpo;
        List<string> PassportnoArray = Passportno.Split(',').ToList<string>();
        string ExpireDate = passexpir;
        List<string> ExpireDateArray = ExpireDate.Split(',').ToList<string>();
        string IssueCountry = issuedat;
        List<string> IssueCountryArray = IssueCountry.Split(',').ToList<string>();
        string xmlResponse = SWSR.CreateTicketResponseNew(PassTypeCount, PassTypeDataArray, PassQuantDataArray, FNamesArray, NamePrefixArray, SurnameArray, BirthDateArray, PassportnoArray, IssueCountryArray, ExpireDateArray, PN, session);
        return xmlResponse;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string FlightSearchResponse(string triptype, string from, string to, string depdate, string retdate, string ad, string ch, string inf, string leg1, string leg2, string leg3, string leg4, string leg5, string leg6, string cc, string direct)
    {
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string QproviderType = "OnlyAmadeus";
        //string QproviderType = "AmadeusAndExtProviders";
        //string QproviderType = "OnlyExtProviders";
        string TripType = triptype;
        //string QRefundableType = "OnlyRefundable";
        //string QRefundableType = "OnlyNonRefundable";
        string QRefundableType = "AllFlights";

        if (direct != "true")
        {
            direct = "false";
        }

        Int32 NumSegment = 0;
        Int32 AdultPax = Convert.ToInt32(ad);
        Int32 ChildPax = Convert.ToInt32(ch); ;
        Int32 InfantPax = Convert.ToInt32(inf);

        string[,] Segments = new string[6, 3];

        if (TripType == "O" || TripType == "R")
        {
            string qfrom = from;
            string qto = to;
            string deptDate = depdate;
            string retDate = retdate;

            Segments[0, 0] = qfrom;
            Segments[0, 1] = qto;
            Segments[0, 2] = Convert.ToDateTime(deptDate).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
            NumSegment++;

            if (TripType == "R")
            {
                Segments[1, 0] = qto;
                Segments[1, 1] = qfrom;
                Segments[1, 2] = Convert.ToDateTime(retDate).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
        }

        if (TripType == "M")
        {
            if (leg1 != "NODATA")
            {
                Segments[NumSegment, 0] = leg1.Split('-')[0];
                Segments[NumSegment, 1] = leg1.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg1.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
            if (leg2 != "NODATA")
            {
                Segments[NumSegment, 0] = leg2.Split('-')[0];
                Segments[NumSegment, 1] = leg2.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg2.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
            if (leg3 != "NODATA")
            {
                Segments[NumSegment, 0] = leg3.Split('-')[0];
                Segments[NumSegment, 1] = leg3.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg3.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
            if (leg4 != "NODATA")
            {
                Segments[NumSegment, 0] = leg4.Split('-')[0];
                Segments[NumSegment, 1] = leg4.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg4.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
            if (leg5 != "NODATA")
            {
                Segments[NumSegment, 0] = leg5.Split('-')[0];
                Segments[NumSegment, 1] = leg5.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg5.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
            if (leg6 != "NODATA")
            {
                Segments[NumSegment, 0] = leg6.Split('-')[0];
                Segments[NumSegment, 1] = leg6.Split('-')[1];
                Segments[NumSegment, 2] = Convert.ToDateTime(leg6.Split('-')[2]).AddMinutes(1).ToString("yyyy-MM-ddTHH:mm:ss");
                NumSegment++;
            }
        }

        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }

        string xmlResponse = SWSR.Flightsearch(Segments, QproviderType, QRefundableType, AdultPax, ChildPax, InfantPax, NumSegment, cc, direct, ref SessionIDFinal);
        //string xmlResponse = "";
        //HttpContext.Current.Session["FLSearchResults"] = xmlResponse;
        HttpContext.Current.Session["GlobelSession"] = SessionIDFinal;
        return xmlResponse;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string Book(string hdPassCount, string hdPassType, string hdPassQuantity, string hdfrisnamarry, string NamePrefix, string Surname, string BirthDate, string txtAreaCityCode, string txtPhoneNumber, string txtEmail, string Passportno, string ExpireDate, string IssueCountry, string txtAdd1, string txtAdd2, string txtCity, string txtZipCode, string ddlCountry, string hdSEQNO, string hdCID, string FrequentFlyerAirlinee, string FrequentFlyerNumberr)
    {
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        int PassTypeCount = Convert.ToInt32(hdPassCount);

        List<string> PassTypeDataArray = hdPassType.Split(',').ToList<string>();
        List<string> PassQuantDataArray = hdPassQuantity.Split(',').ToList<string>();
        List<string> FNamesArray = hdfrisnamarry.Split(',').ToList<string>();
        List<string> NamePrefixArray = NamePrefix.Split(',').ToList<string>();
        List<string> SurnameArray = Surname.Split(',').ToList<string>();
        List<string> BirthDateArray = BirthDate.Split(',').ToList<string>();

        string AreaCityCodeArray = txtAreaCityCode;
        string PhoneNumberArray = txtPhoneNumber;
        string EmailArray = txtEmail;

        List<string> PassportnoArray = Passportno.Split(',').ToList<string>();
        List<string> ExpireDateArray = ExpireDate.Split(',').ToList<string>();
        List<string> IssueCountryArray = IssueCountry.Split(',').ToList<string>();

        string Addr1 = txtAdd1;
        string Addr2 = txtAdd2;
        string City = txtCity;
        string PostalCode = txtZipCode;
        string Country = ddlCountry;

        List<string> FrequentFlyerAirlineeArray = FrequentFlyerAirlinee.Split(',').ToList<string>();
        List<string> FrequentFlyerNumberrArray = FrequentFlyerNumberr.Split(',').ToList<string>();

        string xmlResponse = SWSR.BookFlight(PassTypeCount, PassTypeDataArray, PassQuantDataArray, FNamesArray, NamePrefixArray, SurnameArray, BirthDateArray, AreaCityCodeArray, PhoneNumberArray, EmailArray, PassportnoArray, IssueCountryArray, ExpireDateArray, hdSEQNO, hdCID, Country, City, PostalCode, Addr1, Addr2, SessionIDFinal, FrequentFlyerAirlineeArray, FrequentFlyerNumberrArray);

        return xmlResponse;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveFlightSequence(string SelectedFlightSEQ)
    {
        HttpContext.Current.Session["SelectedFlight"] = SelectedFlightSEQ;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetFlightSequence()
    {
        string Selectflight = HttpContext.Current.Session["SelectedFlight"].ToString();
        return Selectflight;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string btnReBook(string refno)
    {
        SendWebServiceRequest SWSR = new SendWebServiceRequest();

        string referenceno = refno;
        //string Amadeussecuritynumber = "842638110";
        //string Amadeussecuritynumber = "123787444";
        string Amadeussecuritynumber = ConfigurationManager.AppSettings["AMWSSecNumber"];
        string Concatenatedstring = referenceno + Amadeussecuritynumber;
        string sha1 = CalculateSha1(Concatenatedstring, Encoding.Default);

        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }

        string xmlResponse = SWSR.BookREquestPriceChange(referenceno, sha1, SessionIDFinal);
        return xmlResponse;

    }
    public static string CalculateSha1(string text, Encoding enc)
    {
        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(text);
        System.Security.Cryptography.SHA1Managed sha1hashstring = new System.Security.Cryptography.SHA1Managed();
        byte[] hash = sha1hashstring.ComputeHash(buffer);
        return HexStringFromBytes(hash);
    }
    public static string HexStringFromBytes(byte[] bytes)
    {
        var sb = new StringBuilder();
        foreach (byte b in bytes)
        {
            var hex = b.ToString("x2");
            sb.Append(hex);
        }
        return sb.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveMarkupData(string MarkupData)
    {
        HttpContext.Current.Session["MarkupData"] = MarkupData;
    }
    [System.Web.Services.WebMethod]
    public static string Cancel(string PNRC, string SURNAMEC)
    {
        string session = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.CancelBooking(PNRC, SURNAMEC, session);
        return xmlResponse;
    }
    [System.Web.Services.WebMethod]
    public static string GetLastticketingDate(string PNRC)
    {
        string session = string.Empty;
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        string xmlResponse = SWSR.GetLastticketingDate(PNRC, session);
        return xmlResponse;

    }
    [System.Web.Services.WebMethod]
    public static string GetPNR(string PNRC, string CSurname)
    {
        string session = string.Empty;
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            session = HttpContext.Current.Session["GlobelSession"].ToString();
        }


        string xmlResponse = SWSR.Checkmytrip(PNRC, CSurname, session);

        return xmlResponse;

    }

    //New 04/10/2017
    //[System.Web.Services.WebMethod]
    //public static string GetPayData(string totpayamount, string customeremail)
    //{
    //    Pay payclass = new Pay();
    //    return payclass.GetPayData(totpayamount, customeremail);
    //}
    //[System.Web.Services.WebMethod]
    //public static string GetPayDataHotel(string totpayamount, string customeremail)
    //{
    //    Pay payclass = new Pay();
    //    return payclass.GetPayDataHotel(totpayamount, customeremail);
    //}
}