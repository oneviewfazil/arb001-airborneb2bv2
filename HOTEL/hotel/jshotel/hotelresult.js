﻿var noResultAvailable = true;
var isPaused = false;
var isPausedonGTACCode = true;
var isWaitingForFirstResult = true;
var DateFormate = 'dd/mm/yy';
var CurrencyStat = 'USD';
var Hotel = "";
//var CinMDY = "";
//var CoutMDY = "";
var SessionHotel = [];
var CancellationGPeriod = parseInt(0);
//var MarkupValue = parseFloat(0);
var CreditLimittAmount = parseFloat(0);
var AgencyTypeIDD = '';
var GMapKey = 'AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58';
var AllLocationCodeArray = [];
var cin = '';
var cout = '';
var html = [];
var hotellocations = [];
var rooms = '';
var MarkupValueHotel = parseFloat(0);
var imgonload = "HOTEL/hotel/images/noimage.gif";
var domain = $(location).attr('host');
var Countrycode = '';
var GTAImageCountrycode = '';
var city = '';
var HotelAgencyCode = '';
var HotelFindURL = '';
var HotelFindMoreURL = '';
var HotelDetailURL = '';
var HotelPrebookURL = '';
var HotelBookURL = '';
var HotelPrecancelURL = '';
var HotelCancelURL = '';
var HotelAmenitiesURL = '';
var HotelRoomAmenitiesURL = '';
var HotelPOIFilterURL = '';
var suppliercode = '';
var remainingsuppliercode = '';
var findsuppliercode = '';
var currentsuppliercode = [];
var nation = '';
var countryofresidence = '';
var night = '';
var numroom = '';
var AgencyCurrencyCode = '';
var AgencyCurrencyExRate = '';
var pageIndex = '';
var currentIndex = '';
var hotelListFullStatus = '';
var pageCount = '';
var sjs = '0';
var nousliderstatus = '';
var directionbindFrom = [];
var directionbindTo = [];
var publicgeocodeatti = '';
var publicgeocodelongi = '';
var FilefetchTimeout = 5000;
var GrncSearchID = '';
var GrncCanstatus = '';
var SearchRefId = "";
var progressbarwidth = 0;
var progressBarTimer = undefined;
var progressBarTimerLimit = 70;
var timoutset = false;
var Quateid = "0";
var outputcurrency = 'AED';
var Agycode = '';
var Supplierelements = '';
var SupplierNum = '';
var PBUpperLimit = 10;
var PBLimit1 = 0;
var CurrSuppliercnt = 1;
var NewPBLimit = 100;
var accesstocken = '';
var parameterUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
var getQuotationOnce = false;
var isPausedOnGtaImage = false;
var isGetMapClicked = false;
//function BindAllFlightsProgressBarTimer() {
//    progressbarwidth += 2.5;
//    $('.filter-Bar').attr('style', 'width: ' + progressbarwidth + '%;');
//    if (progressbarwidth >= progressBarTimerLimit) {
//        window.clearInterval(progressBarTimer);
//    }
//}
//function BindResultsProgressBarTimer() {
//    progressbarwidth += 0.15;
//    if (progressbarwidth >= 99) {
//        window.clearInterval(progressBindBarTimer);
//    }
//    else
//    {
//        $('.filter-Bar').attr('style', 'width: ' + progressbarwidth + '%;');    
//    }
//}
function BindResultsProgressBarTimerWithWidth(hotelWidth, PBLimit, setToTop) {
    //  window.clearInterval(progressBarTimer);

    if (setToTop == true) {
        progressbarwidth = PBLimit;
        $('.filter-Bar').attr('style', 'width: ' + progressbarwidth + '%;');
        window.clearInterval(progressBarTimer);
    }
    else {
        progressbarwidth += hotelWidth;
        if (progressbarwidth >= PBLimit || progressbarwidth >= 100) {
            window.clearInterval(progressBarTimer);
        }
        else {
            $('.filter-Bar').attr('style', 'width: ' + progressbarwidth + '%;');
        }

    }
}
var Cart = [];
$(document).ready(function () {
    suppliercode = GetParameterValues('suplr');
    Supplierelements = suppliercode.split(',');
    SupplierNum = Supplierelements.length;
    NewPBLimit = 80 / SupplierNum;
    $('#lblloadername').text("we are finding the best results for you...");
    progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(0.25, ' + PBUpperLimit + ', false)', 250); //ok

    //accesstocken = window.localStorage.getItem("accesstocken");

    CityCode = GetParameterValues('city');
    window.localStorage.setItem("CityCode", CityCode);

    //if (!accesstocken) {
    //	var LoginuserDetails = {
    //		userSession: {
    //			accessToken: Setup.Session.AccessToken
    //		}
    //	};
    //} else {
    //	waitforaccesstocken = false;
    //}

    Agycode = window.localStorage.getItem("Agycode");
    CancellationGPeriod = parseInt(0);
    CreditLimittAmount = parseFloat(0);
    pageIndex = parseFloat(0);
    currentIndex = parseFloat(0);
    hotelListFullStatus = parseFloat(0);
    pageCount = parseFloat(1);
    sjs = parseInt(0);
    nousliderstatus = parseFloat(0);
    MarkupValueHotel = parseFloat(0);
    //    $(".loadingGIF").show();
    city = GetParameterValues('city');
    //GetGTAImageCountrycode();
    AgencyCurrencyCode = window.localStorage.getItem(Setup.Session.AgencyCurrencyCode);
    CurrencyStat = AgencyCurrencyCode;
    AgencyCurrencyExRate = window.localStorage.getItem(Setup.Session.AgencyCurrencyExRate);
    SessiongetAgencyCode("SaveAgencyCode");
    setmodifyfield();
    $(".chdage, #r2Div, #r3Div, #r4Div, #r5Div, #r6Div, #r7Div, #r8Div, #r9Div").hide();

    var LoginuserDetails = {
        userSession: {
            accessToken: Setup.Session.AccessToken,
            //userDetails: Setup.Session.UserDetails,
            loginUser: Setup.Session.LoginUser
        }
    };

    SessionCheckDetails(JSON.stringify(LoginuserDetails));

    if (!window.localStorage.getItem("HotelSuppliers")) {
        SessionGetHotelSuppliers();
    } else {
        SessionBindHotelSuppliers(JSON.parse(window.localStorage.getItem("HotelSuppliers")));
    }
    //GetCountrycode();
    //SearchCookies();
    GetGTAImageCountrycode();

    nation = GetParameterValues('nat');
    countryofresidence = GetParameterValues('countryofres');
    suppliercode = GetParameterValues('suplr');
    remainingsuppliercode = suppliercode;
    findsuppliercode = suppliercode;
    cin = GetParameterValues('cin');
    cout = GetParameterValues('cout');
    //if (DateFormate == 'dd/mm/yy') {
    //    CinMDY = new Date(cin.split('/')[1] + '-' + cin.split('/')[0] + '-' + cin.split('/')[2]);
    //    CoutMDY = new Date(cout.split('/')[1] + '-' + cout.split('/')[0] + '-' + cout.split('/')[2]);
    //}
    //else {
    //    CinMDY = new Date(cin);
    //    CoutMDY = new Date(cout);
    //}
    night = GetParameterValues('night');
    //night = '1';
    numroom = GetParameterValues('numroom');
    rooms = '';
    for (var irt = 1; irt <= parseInt(numroom); irt++) {
        if (rooms == '') {
            rooms += GetParameterValues('room' + irt + '');
        }
        else {
            rooms += '/' + GetParameterValues('room' + irt + '');
        }
    }

    //Getallquatation();
    //  GetHotelResults();
});


//function SearchCookies() {
//    var searchCriteria = getSearchCriteria();
//    var JsonSearch = {
//        HotelSearch: {
//            searchCriteria: searchCriteria
//        }
//    }
//    var DataReq = JSON.stringify(JsonSearch);
//    $.cookie('HotelSearch', DataReq, { expires: 50 });
//}
//function getSearchCriteria() {
//    nation = GetParameterValues('nat');
//    countryofresidence = GetParameterValues('countryofres');
//    suppliercode = GetParameterValues('suplr');
//    remainingsuppliercode = suppliercode;
//    findsuppliercode = suppliercode;
//    cin = GetParameterValues('cin');
//    cout = GetParameterValues('cout');
//    //if (DateFormate == 'dd/mm/yy') {
//    //    CinMDY = new Date(cin.split('/')[1] + '-' + cin.split('/')[0] + '-' + cin.split('/')[2]);
//    //    CoutMDY = new Date(cout.split('/')[1] + '-' + cout.split('/')[0] + '-' + cout.split('/')[2]);
//    //}
//    //else {
//    //    CinMDY = new Date(cin);
//    //    CoutMDY = new Date(cout);
//    //}
//    // night = GetParameterValues('night');
//    night = '1';
//    numroom = GetParameterValues('numroom');
//    rooms = '';
//    for (var irt = 1; irt <= parseInt(numroom); irt++) {
//        if (rooms == '') {
//            rooms += GetParameterValues('room' + irt + '');
//        }
//        else {
//            rooms += '/' + GetParameterValues('room' + irt + '');
//        }
//    }

//    var qstrings = {
//        nation: nation,
//        countryofresidence: countryofresidence,
//        city: city,
//        cin: cin,
//        cout: cout,
//        night: night,
//        numroom: numroom,
//        rooms: rooms,
//        supp_code: suppliercode,
//        s_curr: "USD",
//        lang: "ENG",
//        AgencyCode: HotelAgencyCode
//    }
//    return qstrings;
//}


function GetParameterValues(param) {

    var QData = 'NODATA';

    if (!Array.prototype.find) {
        for (var i = 0; i < parameterUrl.length; i++) {
            var urlparam = parameterUrl[i].split('=');
            if (urlparam[0] == param) {
                QData = urlparam[1];
                return decodeURIComponent(QData);
            }
        }
        return QData;
    } else {
        QData = parameterUrl.find(function (e) {
            return e.split('=')[0] == param;
        });

        return QData ? decodeURIComponent(QData.split('=')[1]) : 'NODATA';
    }

}
function ajaxHelper(successCallback, failureCallback, errorCallback, requestData) {

    var data = {};

    data.type = "POST";
    data.url = requestData.url;

    if (requestData.data) {
        data.data = JSON.stringify(requestData.data);
    }
    data.crossDomain = true;
    data.contentType = "application/json; charset=utf-8";
    data.dataType = "json";
    data.success = successCallback;

    if (failureCallback) {
        data.failure = failureCallback;
    } else {
        data.failure = function (response) {
            console.log(response.d);
        }
    }

    if (errorCallback) {
        data.error = errorCallback;
    }


    if (navigator.onLine) {
        $.ajax(data);
    } else {
        alertify.alert('No Connection', 'There seems to be a network issue. Please try again later.', function () {
            window.location.href = "/HOTEL/searchhotel.html";
        });
    }
}

////Check user login
//function SessionChecking(session) {
// //   $(".loadingGIF").show();
//	var Dastasseq = {
//		sesname: session
//	};

//	function SessionSuccess(data) {
//		if (data.d == 'NOSESSION') {
//			window.location.href = "/index.html";
//		}
//		else {
//			var userdata = $.parseJSON(data.d);
//			//console.log(userdata);
//			$('.username').html(userdata[0].FirstName);
//			$('.AgencyName').html(userdata[0].AgencyName);
//			AgencyTypeIDD = userdata[0].AgencyTypeIDD;
//			if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
//				$('.CreditLimitAmount').hide();
//				$('.lesscreditlimit').hide();
//				$('.nocreditlimit').hide();
//			}
//			GetMarkupDataa('MarkupDataHotel');
//		}
//	}

//    ajaxHelper(SessionSuccess, null, null, { url: Setup.Session.Url.GetSessionData, data: Dastasseq });

//}


////Get Markup Data
//function GetMarkupDataa(session) {
//	var Dastasseq = {
//		sesname: session
//	};

//	function GetMarkupSuccess(data) {
//		if (data.d != Setup.Session.NoSession) {
//			if (data.d != Setup.Session.NoMarkup) {
//				var markupdata = $.parseJSON(data.d);
//				//console.log(markupdata);
//				MarkupValue = markupdata[0].MarkupValue;
//				//console.log(MarkupValue);
//				CreditLimittAmount = parseFloat(markupdata[0].CreditLimitAmount);
//				if (CreditLimittAmount < parseFloat(500) && CreditLimittAmount > parseFloat(100))
//				{ $('.nocreditlimit').hide(); }
//				else if (CreditLimittAmount <= parseFloat(100))
//				{ $('.lesscreditlimit').hide(); }
//				else { $('.nocreditlimit').hide(); $('.lesscreditlimit').hide(); }

//				$('.CreditLimitAmount').html('Credit Limit - ' + markupdata[0].CreditLimitAmount + ' ' + markupdata[0].CurrenyCode);
//			}
//			else {//No Markup Data
//				MarkupValue = 0;
//			}
////            GetHotelResults();
//		}
//		else {//No Session
//			window.location.href = "/index.html";
//		}
//	}

//	ajaxHelper(GetMarkupSuccess, null, null, { url: Setup.Session.Url.GetSessionData, data: Dastasseq });
//}

function GetHotelResults() {
    if (isPausedOnMarkup == true || waitforaccesstocken == true) {
        setTimeout(function () { GetHotelResults() }, 100);
    }
    else {
        nation = GetParameterValues('nat');
        suppliercode = GetParameterValues('suplr');
        cin = GetParameterValues('cin');
        cout = GetParameterValues('cout');
        //if (DateFormate == 'dd/mm/yy') {
        //	CinMDY = new Date(cin.split('/')[1] + '-' + cin.split('/')[0] + '-' + cin.split('/')[2]);
        //	CoutMDY = new Date(cout.split('/')[1] + '-' + cout.split('/')[0] + '-' + cout.split('/')[2]);
        //}
        //else {
        //	CinMDY = new Date(cin);
        //	CoutMDY = new Date(cout);
        //}
        night = GetParameterValues('night');
        numroom = GetParameterValues('numroom');
        rooms = '';
        for (var irt = 1; irt <= parseInt(numroom); irt++) {
            if (rooms == '') {
                rooms += GetParameterValues('room' + irt + '');
            }
            else {
                rooms += '/' + GetParameterValues('room' + irt + '');
            }
        }
        Showsearchsummary();

        var qstrings = {
            nation: nation,
            countryofresidence: countryofresidence,
            city: city,
            cin: cin,
            cout: cout,
            night: night,
            numroom: numroom,
            rooms: rooms,
            supp_code: findsuppliercode,
            s_curr: "USD",
            lang: "ENG",
            requeststatus: true,
            index: 0,
            AgencyCode: HotelAgencyCode,
            token: accesstocken
        }

        var errorFunc = function (response) {
            if (response.Response == 'REQUESTSEND') {
                ShowHotelResultsTwice(response);
            }
            else {
                alertify.alert('No Results', 'There are no results. Please try for another combination.', function () {
                    window.location.href = "/HOTEL/searchhotel.html";
                });
            }
        };

        ajaxHelper(ShowHotelResultsTwice, null, errorFunc, { url: HotelFindURL, data: qstrings });

        BindResultsProgressBarTimerWithWidth(1, PBUpperLimit, true); //ok
        CurrSuppliercnt = 1;
        PBUpperLimit = PBUpperLimit + NewPBLimit;
        setModifySearch();
        $('#lblloadername').text("FETCHING RESULTS FROM SUPPLIER " + CurrSuppliercnt);
        // PBLimit1 = 90;

        // progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(1,PBLimit1,false)', 250);   
    }
}
function ShowHotelResultsTwice(data) {
    if (isPausedOnMarkup == "true") {
        setTimeout(function () { ShowHotelResultsTwice(data) }, 100);
    }
    else {
        if (data.Response == 'REQUESTSEND') {
            progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(0.25,"' + PBUpperLimit + '",false)', 250);

            setTimeout(function () { GetInitialRecords() }, FilefetchTimeout);
        }
        else {

            //PBLimit1 = 90;
            progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(2,"' + PBUpperLimit + '",false)', 250);
            ShowHotelResults(data);
        }

    }
}
function RequestFiles(data) {
    GetInitialRecords();
}

function ShowHotelResults(data) {
    // PBLimit1 = 90;
    // progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(1,PBLimit1,false)', 250);
    //progressBindBarTimer = window.setInterval('BindResultsProgressBarTimer()', 1500);
    //var xml = JSON.parse(data.d);
    //var xml = data;
    if (data.Response == 'REQUESTSEND') {
        if (timoutset == false) {
            timoutset = true;
            setTimeout(function () { GetTimeout() }, 40000);
        }
        RequestFiles(data);
    }
    else if (data != 'NORESULT') {
        if (!getQuotationOnce) {
            Getallquatation();
            getQuotationOnce = true;
        }
        noResultAvailable = false;
        //var x2js = new X2JS();
        //var jsonRes = x2js.xml_str2json(xml);
        //var jsonRes = xml;
        //console.log(data);
        if (!isPausedOnGtaImage && data.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel[0].SupplierCode == "GTA") {
            setTimeout(function () { ShowHotelResults(data) }, 100);
        } else {
            if (data.Response.ResponseDetails.Errors == undefined) {
                if (data.Response.ResponseDetails.SearchHotelPricePaxResponse.Errors == undefined) {
                    if (data.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails != "") {
                        Hotel = data.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel;
                        SearchRefId = data.Response._ResponseReference;
                        if (GrncSearchID == '' && data.Response.Grnc_ResponseReference != null) {
                            GrncSearchID = data.Response.Grnc_ResponseReference;
                        }
                        if (Hotel.length == null) {
                            //for single results
                            var arr = [];
                            arr.push(Hotel)
                            Hotel = arr;
                            $(".hotelcount").html('<p>1 Hotel Found</p>');
                        }
                        //console.log(Hotel);
                        //multiple results
                        for (var index = 0; index < Hotel.length; index++) {
                            var myActiveSupplierList = new Set(currentsuppliercode);
                            if (!myActiveSupplierList.has(Hotel[index].SupplierCode)) {
                                findsuppliercode = Hotel[index].SupplierCode;
                                currentsuppliercode.push(Hotel[index].SupplierCode);
                            }
                            var TopRoomPrice = parseFloat(0);
                            SessionHotel.push(Hotel[index]);
                            if (Hotel[index].SupplierCode == "GTA") {
                                var MainThumbImg = "http://images.gta-travel.com/HH/Images/" + GTAImageCountrycode + "/" + Hotel[index].City._Code + "/" + Hotel[index].City._Code + "-" + Hotel[index].Item._Code + "-1.jpg";
                                var MainImgGeo = "http://images.gta-travel.com/HH/Images/" + GTAImageCountrycode + "/" + Hotel[index].City._Code + "/" + Hotel[index].City._Code + "-" + Hotel[index].Item._Code + "-1.jpg";
                            }
                            else {
                                var MainThumbImg = Hotel[index].ThumbnailImage;
                                var MainImgGeo = Hotel[index].ThumbnailImage;
                            }

                            if (Hotel[index].PaxRoomSearchResults.PaxRoom.length != undefined) {
                                //Multiple Rooms
                                var PaxRooms = "";
                                var PaxRoomSearchResults = Hotel[index].PaxRoomSearchResults;
                                for (var ro = 0; ro < PaxRoomSearchResults.PaxRoom.length; ro++) {

                                    PaxRooms += '<div class="hotel_room_details"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_bg_color01">Room '
                                        + PaxRoomSearchResults.PaxRoom[ro]._RoomIndex + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_bg_color01">Status</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_bg_color01">Cost</div> <div class="col-lg-2 col-md-2 col-sm-2 hotel_bg_color01">&nbsp;</div> </div>';

                                    var RoomCategories = PaxRoomSearchResults.PaxRoom[ro].RoomCategories;
                                    if (RoomCategories.RoomCategory.length != undefined) {
                                        //Multiple Room Category
                                        TopRoomPrice += parseFloat(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[0].ItemPrice.__text);
                                        var TopXMLCurrency = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[0].ItemPrice._Currency;
                                        var TopConfirmation = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[0].Confirmation.__cdata;
                                        var ConfirmationCode = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[0].Confirmation._Code;

                                        var popid = sjs + '_' + ro + '_0';
                                        var SupplierCode = Hotel[index].SupplierCode;

                                        for (var roCat = 0; roCat < RoomCategories.RoomCategory.length; roCat++) {

                                            var canValID = sjs + '_' + ro + '_' + roCat;
                                            if (roCat == 0) {
                                                var BtnSelect = '<a sel-stat="true" pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                                    '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                                    '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                                    '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                                    '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                                    '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                                    '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                                    '"  pr-sid ="' + GrncSearchID +
                                                    '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                                    '" id="selecticon_' + sjs + '_' + ro + '_' + roCat + '" class="btn_selectedroom" onclick="SelectRoom(this,\'' + SupplierCode + '\')">Selected</a>';
                                                var roomselbool = 'true';
                                            }
                                            else {
                                                var BtnSelect = '<a sel-stat="false" pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                                    '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                                    '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                                    '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                                    '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                                    '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                                    '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                                    '"  pr-sid ="' + GrncSearchID +
                                                    '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                                    '" id="selecticon_' + sjs + '_' + ro + '_' + roCat + '" onclick="SelectRoom(this,\'' + SupplierCode + '\')">Select</a>';
                                                var roomselbool = 'false';
                                            }

                                            var CancPolicyString = GetCancellationPolicy(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ChargeConditions.ChargeCondition, SupplierCode);
                                            var PriceRangesString = GetPriceRanges(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].HotelRoomPrices, RoomCategories.RoomCategory[roCat].Description.__cdata, PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Meals, SupplierCode);
                                            var Sharebeddet = '';
                                            if (Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed != null) {
                                                Sharebeddet = Getextrabeddetails(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed.__cdata)
                                            }

                                            //PaxRooms += '<div id="r_' + sjs + '_' + ro + '_' + roCat + '" sel-stat="' + roomselbool + '" r-desc="' + RoomCategories.RoomCategory[roCat].Description.__cdata + ' (' + GetMeals(PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Meals) + ')"  room-i="' + RoomCategories.RoomCategory[roCat]._Id + '" r-stat="' + RoomCategories.RoomCategory[roCat].Confirmation._Code + '"  class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><i class="fa fa-bed" aria-hidden="true"></i>'
                                            PaxRooms += '<div id="r_' + sjs + '_' + ro + '_' + roCat + '" sel-stat="' + roomselbool +
                                                '" pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                                '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                                '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                                '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                                '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                                '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                                '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                                '"  pr-sid ="' + GrncSearchID +
                                                '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                                '" r-desc="' + RoomCategories.RoomCategory[roCat].Description.__cdata + '"room-i="' + RoomCategories.RoomCategory[roCat]._Id + '" r-stat="' + RoomCategories.RoomCategory[roCat].Confirmation._Code + '"  class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type">' +
                                                '<div class="pretty_check" style=" top: 1px !important; display: block !important;"><input type="checkbox" data-active="0" class="htlRoomcheckbox_' + sjs + ' htlRoomCHKActive" id="HtlRoom_' + sjs + '_' + ro + '_' + roCat + '" data-img="' + MainThumbImg + '" data-type="' + RoomCategories.RoomCategory[roCat].Description.__cdata + '" data-rmstaus="' + RoomCategories.RoomCategory[roCat].Confirmation.__cdata + '" data-address="' + GetFullLocationTitlenew(Hotel[index].LocationDetails.Location["0"]._address, Hotel[index].LocationDetails) + '" data-price="' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '" data-star="' + parseInt(Hotel[index].StarRating) + '" data-hotelName="' + Hotel[index].Item.__cdata + '" data-hotelCode="' + Hotel[index].Item._Code + '" data-sharebed="' + RoomCategories.RoomCategory[roCat].Sharebed.__cdata + '"  data-RoomID="' + RoomCategories.RoomCategory[roCat]._Id + '" data-meals="' + RoomCategories.RoomCategory[roCat].Meals.Basis.__cdata + '" data-supplier="' + SupplierCode + '" data-rsno="' + (ro + 1) + '" data-hid="' + sjs + '"  onChange="RoomCheckChanged(this)"> <label></label></div>'
                                                + RoomCategories.RoomCategory[roCat].Description.__cdata +
                                                //' (' + GetMeals(PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Meals) +
                                                //')
                                                '</span></div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding status_area">' +
                                                GetConfirmation(RoomCategories.RoomCategory[roCat].Confirmation.__cdata) + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding cost_area"><span>'
                                                + CurrencyStat + ' ' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '</span>' +
                                                '<a style="cursor:pointer" data-open="0" onclick="GetJACCancelplcyfromPrebook(\'' + SupplierCode + '\',\'' + popid + '\',\'' + RoomCategories.RoomCategory[roCat].cpStatus + '\',\'' + Hotel[index].Item._Code + '\',\'' + canValID + '\',\'' + RoomCategories.RoomCategory[roCat]._Id + '\',\'' + PaxRoomSearchResults.PaxRoom[ro]._RoomIndex + '\',this)"><i id="icon_' + sjs + '_' + ro + '_' + roCat + '" class="fa fa-plus-circle" title="show cancellation policy" aria-hidden="true"></i>' + GetOffers(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Offer) + '</a></div>' +
                                                '<div class="col-lg-2 col-md-2 col-sm-2 hotel_sm_padding book_button">' + BtnSelect + '</div> </div> </div>' +
                                                '<!-- cancellation popup start --> <div class="modal fade" id="cmod_' + sjs + '_' + ro + '_' + roCat +
                                                '" role="dialog" style="background: rgba(0, 0, 0, 0.57);"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title"><i class="fa fa-bed hotel_icon5" aria-hidden="true"></i>' + Hotel[index].Item.__cdata +
                                                '</h4> </div> <div id="candiv_' + sjs + '_' + ro + '_' + roCat + '" class="modal-body sp_bg_hotel cancpolicydiv">' +
                                                '' + CancPolicyString + '</div>' + GetEssentialInfo(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].EssentialInfo) + ' <div class="modal-body hotel_type">' +

                                                "<h4>" + Sharebeddet +
                                                "</h4>" +

                                                PriceRangesString +
                                                '</div> <div class="modal-footer"> <h3>' + GetDiscountAmount(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ItemPrice._IncludedOfferDiscount, RoomCategories.RoomCategory[roCat].ItemPrice.__text, SupplierCode) + '</h3> </div> </div> </div> </div> <!-- cancellation popup end-->';

                                        }
                                    }
                                    else {
                                        //Single Room Category
                                        TopRoomPrice += parseFloat(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.ItemPrice.__text);
                                        var TopXMLCurrency = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.ItemPrice._Currency;
                                        var TopConfirmation = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Confirmation.__cdata;

                                        var canValID = sjs + '_' + ro + '_' + roCat;
                                        var ConfirmationCode = Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Confirmation._Code;
                                        var popid = sjs + '_' + ro + '_0';
                                        var SupplierCode = Hotel[index].SupplierCode;
                                        var CancPolicyString = GetCancellationPolicy(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition, SupplierCode);
                                        var PriceRangesString = GetPriceRanges(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.HotelRoomPrices, RoomCategories.RoomCategory.Description.__cdata, PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Meals, SupplierCode);
                                        var Sharebeddet = '';
                                        if (Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed != null) {
                                            Sharebeddet = Getextrabeddetails(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed.__cdata)
                                        }
                                        //PaxRooms += '<div id="r_' + sjs + '_' + ro + '_0" sel-stat="true" r-desc="' + RoomCategories.RoomCategory.Description.__cdata + ' (' + GetMeals(PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Meals) + ')" room-i="' + RoomCategories.RoomCategory._Id + '" r-stat="' + RoomCategories.RoomCategory.Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><i class="fa fa-bed" aria-hidden="true"></i>'
                                        PaxRooms += '<div id="r_' + sjs + '_' + ro + '_0" sel-stat="true" ' +
                                            'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                            '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                            '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                            '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                            '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                            '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                            '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                            '"  pr-sid ="' + GrncSearchID +
                                            '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                            '" r-desc="' + RoomCategories.RoomCategory.Description.__cdata + '"room-i="' + RoomCategories.RoomCategory._Id + '" r-stat="' + RoomCategories.RoomCategory.Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><div class="pretty_check" style=" top: 1px !important;display: block !important; "><input type="checkbox" data-active="0" class="htlRoomcheckbox_' + sjs + '" id="HtlRoom_' + sjs + '_' + ro + '_0" data-img="' + MainThumbImg + '"  data-type="' + RoomCategories.RoomCategory[roCat].Description.__cdata + '" data-address="' + GetFullLocationTitlenew(Hotel[index].LocationDetails.Location["0"]._address, Hotel[index].LocationDetails) + '"  data-price="' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '" data-star="' + parseInt(Hotel[index].StarRating) + '" data-hotelName="' + Hotel[index].Item.__cdata + '" data-hotelCode="' + Hotel[index].Item._Code + '" data-rmstaus="' + RoomCategories.RoomCategory[roCat].Confirmation.__cdata + '" data-sharebed="' + RoomCategories.RoomCategory[roCat].Sharebed.__cdata + '" data-RoomID="' + RoomCategories.RoomCategory[roCat]._Id + '" data-meals="' + RoomCategories.RoomCategory[roCat].Meals.Basis.__cdata + '" data-supplier="' + SupplierCode + '" data-hid="' + sjs + '" data-rsno="' + (ro + 1) + '"  onChange="RoomCheckChanged(this)"> <label></label></div>'
                                            + RoomCategories.RoomCategory.Description.__cdata +
                                            //  ' (' + GetMeals(PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Meals) +
                                            ')</span></div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding status_area">' +
                                            '' + GetConfirmation(RoomCategories.RoomCategory.Confirmation.__cdata) + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding cost_area"><span>'
                                            + CurrencyStat + ' ' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '</span>' +
                                            '<a style="cursor=pointer" data-open="0" onclick="GetJACCancelplcyfromPrebook(\'' + SupplierCode + '\',\'' + popid + '\',\'' + RoomCategories.RoomCategory[roCat].cpStatus + '\',\'' + Hotel[index].Item._Code + '\',\'' + canValID + '\',\'' + RoomCategories.RoomCategory[roCat]._Id + '\',\'' + PaxRoomSearchResults.PaxRoom[ro]._RoomIndex + '\',this)"><i id="icon_' + sjs + '_' + ro + '_0" class="fa fa-plus-circle" title="show cancellation policy" aria-hidden="true"></i>' + GetOffers(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.Offer) + '</a></div>' +
                                            '<div class="col-lg-2 col-md-2 col-sm-2 hotel_sm_padding book_button"><a sel-stat="true" class="btn_selectedroom"' +
                                            'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                            '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                            '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                            '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                            '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                            '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                            '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                            '"  pr-sid ="' + GrncSearchID +
                                            '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                            '" id="selecticon_' + sjs + '_' + ro + '_0" onclick="SelectRoom(this,\'' + Hotel[index].SupplierCode + '\')">Selected</a></div> </div> </div>' +
                                            '<!-- cancellation popup start --> <div class="modal fade" id="cmod_' + sjs + '_' + ro + '_0' +
                                            '" role="dialog" style="background: rgba(0, 0, 0, 0.57);"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title"><i class="fa fa-bed hotel_icon5" aria-hidden="true"></i>' + Hotel[index].Item.__cdata +
                                            '</h4> </div> <div id="candiv_' + sjs + '_' + ro + '_' + roCat + '" class="modal-body sp_bg_hotel cancpolicydiv">' +
                                            '' + CancPolicyString + '</div>' + GetEssentialInfo(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.EssentialInfo) + ' <div class="modal-body hotel_type">' +
                                            "<h4>" + Sharebeddet +
                                            "</h4>" +
                                            PriceRangesString + ''
                                            + '</div><div class="modal-footer"> <h3>' + GetDiscountAmount(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory.ItemPrice._IncludedOfferDiscount, RoomCategories.RoomCategory.ItemPrice.__text, SupplierCode) + '</h3> </div> </div> </div> </div> <!-- cancellation popup end-->';
                                    }
                                }
                            }
                            else {
                                //Single Rooms
                                var PaxRooms = "";
                                var PaxRoomSearchResults = Hotel[index].PaxRoomSearchResults;

                                PaxRooms += '<div class="hotel_room_details"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_bg_color01">Room '
                                    + PaxRoomSearchResults.PaxRoom._RoomIndex + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_bg_color01">Status</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_bg_color01">Cost</div> <div class="col-lg-2 col-md-2 col-sm-2 hotel_bg_color01">&nbsp;</div> </div>';
                                var RoomCategories = PaxRoomSearchResults.PaxRoom.RoomCategories;
                                if (RoomCategories.RoomCategory.length != undefined) {
                                    //Multiple Room Category
                                    var TopRoomPrice = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[0].ItemPrice.__text;
                                    var TopXMLCurrency = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[0].ItemPrice._Currency;
                                    var TopConfirmation = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[0].Confirmation.__cdata;
                                    var ConfirmationCode = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[0].Confirmation._Code;
                                    var popid = sjs + '_' + ro + '_0';
                                    var SupplierCode = Hotel[index].SupplierCode;
                                    for (var roCat = 0; roCat < RoomCategories.RoomCategory.length; roCat++) {
                                        var canValID = sjs + '_' + ro + '_' + roCat;
                                        if (roCat == 0) {
                                            var BtnSelect = '<a class="btn_selectedroom" sel-stat="true"' +
                                                'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                                '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                                '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                                '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                                '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                                '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                                '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                                '"  pr-sid ="' + GrncSearchID +
                                                '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                                '" id="selecticon_' + sjs + '_' + ro + '_' + roCat + '" onclick="SelectRoom(this,\'' + SupplierCode + '\')">Selected</a>';
                                            var roomselbool = 'true';
                                        }
                                        else {
                                            var BtnSelect = '<a sel-stat="false"' +
                                                'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                                '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                                '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                                '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                                '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                                '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                                '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                                '"  pr-sid ="' + GrncSearchID +
                                                '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                                '"  id="selecticon_' + sjs + '_' + ro + '_' + roCat + '" onclick="SelectRoom(this,\'' + SupplierCode + '\')">Select</a>';
                                            var roomselbool = 'false';
                                        }

                                        var CancPolicyString = GetCancellationPolicy(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].ChargeConditions.ChargeCondition, SupplierCode);
                                        var PriceRangesString = GetPriceRanges(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].HotelRoomPrices, RoomCategories.RoomCategory[roCat].Description.__cdata, PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].Meals, SupplierCode);
                                        var Sharebeddet = '';
                                        if (Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed != null) {
                                            Sharebeddet = Getextrabeddetails(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed.__cdata)
                                        }
                                        //PaxRooms += '<div id="r_' + sjs + '_0_' + roCat + '" sel-stat="' + roomselbool + '" r-desc="' + RoomCategories.RoomCategory[roCat].Description.__cdata + ' (' + GetMeals(PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].Meals) + ')" room-i="' + RoomCategories.RoomCategory[roCat]._Id + '" r-stat="' + RoomCategories.RoomCategory[roCat].Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><i class="fa fa-bed" aria-hidden="true"></i>'
                                        PaxRooms += '<div id="r_' + sjs + '_0_' + roCat + '" sel-stat="' + roomselbool +
                                            '" pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                            '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                            '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                            '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                            '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                            '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                            '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                            '"  pr-sid ="' + GrncSearchID +
                                            '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                            '" r-desc="' + RoomCategories.RoomCategory[roCat].Description.__cdata + '"room-i="' + RoomCategories.RoomCategory[roCat]._Id + '" r-stat="' + RoomCategories.RoomCategory[roCat].Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><div class="pretty_check" style=" top: 1px !important;display: block !important; "><input type="checkbox" class="htlcheckbox"> <label></label></div>'
                                        PaxRooms += '<div id="r_' + sjs + '_0_' + roCat + '" sel-stat="' + roomselbool + '" r-desc="' + RoomCategories.RoomCategory[roCat].Description.__cdata + '"room-i="' + RoomCategories.RoomCategory[roCat]._Id + '" r-stat="' + RoomCategories.RoomCategory[roCat].Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><div class="pretty_check" style=" top: 1px !important;display: block !important; "><input type="checkbox" class="htlcheckbox"> <label></label></div>'
                                            + RoomCategories.RoomCategory[roCat].Description.__cdata +
                                            // ' (' + GetMeals(PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].Meals) +
                                            ')</span></div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding status_area">'
                                            + GetConfirmation(RoomCategories.RoomCategory[roCat].Confirmation.__cdata) + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding cost_area"><span>'
                                            + CurrencyStat + ' ' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '</span>' +
                                            '<a style="cursor:pointer" data-open="0" onclick="GetJACCancelplcyfromPrebook(\'' + SupplierCode + '\',\'' + popid + '\',\'' + RoomCategories.RoomCategory[roCat].cpStatus + '\',\'' + Hotel[index].Item._Code + '\',\'' + canValID + '\',\'' + RoomCategories.RoomCategory[roCat]._Id + '\',\'' + PaxRoomSearchResults.PaxRoom[ro]._RoomIndex + '\',this)"><i id="icon_' + sjs + '_' + ro + '_' + roCat + '" class="fa fa-plus-circle" title="show cancellation policy" aria-hidden="true"></i>' + GetOffers(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].Offer) + '</a></div> <div class="col-lg-2 col-md-2 col-sm-2 hotel_sm_padding book_button">' + BtnSelect + '</div> </div> </div>' +
                                            '<!-- cancellation popup start --> <div class="modal fade" id="cmod_' + sjs + '_0_' + roCat +
                                            '" role="dialog" style="background: rgba(0, 0, 0, 0.57);"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title"><i class="fa fa-bed hotel_icon5" aria-hidden="true"></i>' + Hotel[index].Item.__cdata +
                                            '</h4> </div> <div id="candiv_' + sjs + '_' + ro + '_' + roCat + '" class="modal-body sp_bg_hotel cancpolicydiv">' +
                                            '' + CancPolicyString + '</div>' + GetEssentialInfo(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].EssentialInfo) + ' <div class="modal-body hotel_type">' +
                                            "<h4>" + Sharebeddet +
                                            "</h4>" +
                                            PriceRangesString +
                                            '</div> <div class="modal-footer"> <h3>' + GetDiscountAmount(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory[roCat].ItemPrice._IncludedOfferDiscount, RoomCategories.RoomCategory[roCat].ItemPrice.__text, SupplierCode) + '</h3> </div> </div> </div> </div> <!-- cancellation popup end-->';
                                    }
                                }
                                else {
                                    //Single Room Category
                                    var TopRoomPrice = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.ItemPrice.__text;
                                    var TopXMLCurrency = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.ItemPrice._Currency;
                                    var TopConfirmation = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Confirmation.__cdata;
                                    var ConfirmationCode = Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Confirmation._Code;
                                    var canValID = sjs + '_' + ro + '_0';
                                    var popid = sjs + '_' + ro + '_0';
                                    var SupplierCode = Hotel[index].SupplierCode;
                                    var CancPolicyString = GetCancellationPolicy(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.ChargeConditions.ChargeCondition, SupplierCode);
                                    var PriceRangesString = GetPriceRanges(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.HotelRoomPrices, RoomCategories.RoomCategory.Description.__cdata, PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Meals, SupplierCode);
                                    var Sharebeddet = '';
                                    if (Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed != null) {
                                        Sharebeddet = Getextrabeddetails(Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].Sharebed.__cdata)
                                    }
                                    //PaxRooms += '<div id="r_' + sjs + '_0_0" sel-stat="true" r-desc="' + RoomCategories.RoomCategory.Description.__cdata + ' (' + GetMeals(PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Meals) + ')" room-i="' + RoomCategories.RoomCategory._Id + '" r-stat="' + RoomCategories.RoomCategory.Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><i class="fa fa-bed" aria-hidden="true"></i>'
                                    PaxRooms += '<div id="r_' + sjs + '_0_0" sel-stat="true"' +
                                        'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                        '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                        '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                        '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                        '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                        '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                        '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                        '"  pr-sid ="' + GrncSearchID +
                                        '"  pr-bundlestatus ="' + Hotel[index].SupplierCode +
                                        '"r-desc="' + RoomCategories.RoomCategory.Description.__cdata + '"room-i="' + RoomCategories.RoomCategory._Id + '" r-stat="' + RoomCategories.RoomCategory.Confirmation._Code + '" class="hotel_room_details01"> <div class="hotel_room_details02"> <div class="col-lg-4 col-md-4 col-sm-4 hotel_sm_padding room_type"><div class="pretty_check" style=" top: 1px !important;display: block !important; "><input type="checkbox" class="htlcheckbox"> <label></label></div>'
                                        + RoomCategories.RoomCategory.Description.__cdata +
                                        // ' (' + GetMeals(PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Meals) +
                                        ')</span></div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding status_area">'
                                        + GetConfirmation(RoomCategories.RoomCategory.Confirmation.__cdata) + '</div> <div class="col-lg-3 col-md-3 col-sm-3 hotel_sm_padding cost_area"><span>'
                                        + CurrencyStat + ' ' + GetRoomPriceConvMark(RoomCategories.RoomCategory[roCat].ItemPrice.__text, 0, numroom, SupplierCode) + '</span>' +

                                        '<a style="cursor:pointer" data-open="0" onclick="GetJACCancelplcyfromPrebook(\'' + SupplierCode + '\',\'' + popid + '\',\'' + RoomCategories.RoomCategory[roCat].cpStatus + '\',\'' + Hotel[index].Item._Code + '\',\'' + canValID + '\',\'' + RoomCategories.RoomCategory[roCat]._Id + '\',\'' + PaxRoomSearchResults.PaxRoom[ro]._RoomIndex + '\',this)"><i id="icon_' + sjs + '_' + ro + '_' + roCat + '" class="fa fa-plus-circle" title="show cancellation policy" aria-hidden="true"></i>' + GetOffers(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.Offer) + '</a></div> <div class="col-lg-2 col-md-2 col-sm-2 hotel_sm_padding book_button"><a sel-stat="true"' +
                                        'pr-groupcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].groupcode +
                                        '"  pr-citycode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].citycode +
                                        '"  pr-cpcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].cancellationcode +
                                        '"  pr-ratetype ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratetype +
                                        '"  pr-roomref ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomreference +
                                        '"  pr-roomcode ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].roomcode +
                                        '"  pr-ratekey ="' + Hotel[index].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ratekey +
                                        '" id="selecticon_' + sjs + '_' + ro + '_' + roCat + '" class="btn_selectedroom" onclick="SelectRoom(this,\'' + SupplierCode + '\')">Selected</a></div> </div> </div>' +
                                        '<!-- cancellation popup start --> <div class="modal fade" id="cmod_' + sjs + '_0_0' +
                                        '" role="dialog" style="background: rgba(0, 0, 0, 0.57);"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title"><i class="fa fa-bed hotel_icon5" aria-hidden="true"></i>' + Hotel[index].Item.__cdata +
                                        '</h4> </div> <div id="candiv_' + sjs + '_' + ro + '_0" class="modal-body sp_bg_hotel cancpolicydiv">' +
                                        '' + CancPolicyString + '</div> ' + GetEssentialInfo(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.EssentialInfo) + '<div class="modal-body hotel_type">' +
                                        "<h4>" + Sharebeddet +
                                        "</h4>" +
                                        PriceRangesString + ''
                                        + '</div><div class="modal-footer"> <h3>' + GetDiscountAmount(Hotel[index].PaxRoomSearchResults.PaxRoom.RoomCategories.RoomCategory.ItemPrice._IncludedOfferDiscount, RoomCategories.RoomCategory.ItemPrice.__text, SupplierCode) + '</h3> </div> </div> </div> </div> <!-- cancellation popup end-->';
                                }
                            }

                            if (ConfirmationCode == 'IM') {
                                ConfirmationCode = 'available';
                            }
                            else if (ConfirmationCode == 'OR') {
                                ConfirmationCode = 'hotelstatus_OR';
                            }
                            //  var MainThumbImg = "http://images.gta-travel.com/HH/Images/EM/" + Hotel[index].City._Code + "th/" + Hotel[index].City._Code + "-" + Hotel[index].Item._Code + "-1.jpg";
                            $("#itemContainer").append('<li id="hotel_' + sjs + '" data-show="0" d-loc="' + GetFullLocationFilter(Hotel[index].LocationDetails) + '"d-Ham="' + GetFullHotelAmenitiFilter(Hotel[index].HotelAminities) + '"d-Ram="' + GetFullRoomAmenitiFilter(Hotel[index].CommonRoomAminities) + '"d-POI="' + GetFullPOIFilter(Hotel[index].POI) + '" class="hotel_view_area" data-price="' + GetRoomPriceConvMark(TopRoomPrice, 0, 1, Hotel[index].SupplierCode) + '" data-name="' + Hotel[index].Item.__cdata + ' " data-star="' + parseInt(Hotel[index].StarRating) + '">' +
                                '</span> </div>  <!--hide sec--> <div class="view_hotel_sec view_area01"> <div class="search_details sp07" id="more_info_popup_' + sjs +
                                '"> <div class="hotel_left_area">' +
                                ' <div class="hotel_img" id="mainthumb_' + sjs + '"><img src="' + MainThumbImg + '" onerror="imgError(this);" alt="noimage"></div> <div class="hotel_dis">' +
                                '<h3>' + Hotel[index].Item.__cdata + '    (<b>' + Hotel[index].SupplierCode + '</b>) </h3>' +
                                '<h4>' + GetStarRatingSpan(Hotel[index].StarRating) + '</h4>' +
                                '<div class="hotel_location"> <i class="fa fa-map-marker sp01" aria-hidden="true"></i> <span>'
                                //+ GetFullLocationTitle(Hotel[index].LocationDetails) + '</span> </div> <div class="available_icons" id="hotRoomAmenitiesHori_' + sjs + '"></div> <div class="hotel_mobile_info"></div> </div> </div> ' +
                                + GetFullLocationTitlenew(Hotel[index].LocationDetails.Location["0"]._address, Hotel[index].LocationDetails) + '</span> </div> <div class="available_icons" id="hotRoomAmenitiesHori_' + sjs + '"></div> <div class="hotel_mobile_info"></div> </div> </div> ' +
                                '<div class="flight_rate_area"> <div class="rate_area01"><h4>' + CurrencyStat + ' <span class="hotpricetot_' + sjs + '">' + GetRoomPriceConvMark(TopRoomPrice, 0, 1, Hotel[index].SupplierCode) + '</span></h4> </div>' +
                                '<div class="' + ConfirmationCode + '">' + TopConfirmation +
                                '</div>' +
                                '<div class="more_view_click01 icon_click02" title="hide more details" id="HotMinus_' + sjs +
                                '">Room Info<i class="fa fa-minus" aria-hidden="true"></i></div>' +
                                '<div class="more_view_click icon_click01" title="show more details" data-open="0" onclick="GetStaticData(\'' + Hotel[index].SupplierCode + '\',\'' + Hotel[index].City._Code + '\',\'' + Hotel[index].sid + '\',\'' + Hotel[index].Item._Code + '\',' + sjs + ',this)"  id="HotPlus_' + sjs +
                                '">Room Info<i class="fa fa-plus" aria-hidden="true"></i></div>' +
                                '<div class="more_view_click icon_click01" id="HotBook_' + sjs + '" type="submit" class="more_search" style="display:none;" onclick="BookHotelClick(\'' + Hotel[index].SupplierCode + '\',\'' + Hotel[index].City._Code + '\',\'' + Hotel[index].Item._Code + '\',' + sjs + ')" >Book Now<i id="HotBookWait_' + sjs + '"class="" aria-hidden="true"></i></div></div> <div id="HViewArea_' + sjs +
                                '" class="hotel_details">' +
                                '<div class="hotel_cont">' +
                                '<p id="hotcat_' + sjs + '"></p><p id="hotgen_' + sjs + '"></p></div>' +
                                '<div class="hotel_detailsbtn"><a class="MoreDetHotClick" data-toggle="modal" data-target="#hotel_more_' + sjs +
                                '">More details</a></div> <div class="row">' +
                                '' + PaxRooms + '</div></div>' +
                                '<!-- Modal --> <div class="modal fade tabstyle" id="hotel_more_' + sjs + '" role="dialog" style="background: rgba(0, 0, 0, 0.68);"> <div class="modal-dialog"> <!-- Modal content--> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">×</button> <h4 class="modal-title"><i class="fa fa-bed hotel_icon5" aria-hidden="true"></i>Informations</h4> </div> <div class="tabs animated-fade"> <ul class="tab-links"> <li class="active"><a href="#tab11"><p><i class="fa fa-info-circle" aria-hidden="true"></i></p>Description</a></li> <li><a href="#tab22"><p><i class="fa fa-camera" aria-hidden="true"></i></p>Photos</a></li> <li><a href="#tab23"><p><i class="fa fa-map-marker" aria-hidden="true"></i></p>Location</a></li> </ul> <div class="tab-content">' +
                                '<div id="tab11" class="tab active"> <div class="hotel_description"> <div class="row"> <div class="col-lg-4 hotel_ar">' +
                                '<div class="hotel_address"><h3>' + Hotel[index].Item.__cdata + '</h3><span>' + GetStarRatingSpan(Hotel[index].StarRating) +
                                '</span><p id="hotAddress_' + sjs + '"></p>' +
                                '<div class="hotel_contact"> <h4 id="hotTelephone_' + sjs + '"></h4><h4 id="hotFax_' + sjs + '"></h4><h4 id="hotEmail_' + sjs + '"></h4><h4 id="hotWeb_' + sjs + '"></h4> </div> </div>' +
                                '<div class="point_or_interest"><div id="hotAreaDet_' + sjs + '"></div>' +
                                '<div class="map" id="HotMiniMap_' + sjs + '"> </div> </div>' +
                                '</div> <div class="col-lg-8 hotel_ar">' +
                                '<div class="hotel_dis_area" id="hotReports_' + sjs + '"></div> ' +
                                '<div class="hotel_aminities"> <h4><i class="fa fa-building-o" aria-hidden="true"></i>Hotel Amenities</h4><div><ul id="hotAmenities_' + sjs + '"></ul></div>' +
                                '<div id="hotRoomAmenities_' + sjs + '"></div>' +
                                '</div> </div> </div> </div> </div>' +
                                '<div id="tab22" class="tab"><div class="photoview padding10" id="divSlidePics_' + sjs + '"></div></div>' +
                                '<div id="tab23" class="tab"> <div class="photoview padding10" id="HotLargeMap_' + sjs + '">  </div> </div> </div> </div> </div> </div> </div> <!-- Modal -->' +
                                '</div> </div> <!--hide sec--> </li>');

                            hotellocations.push(Hotel[index].Item.__cdata + "," + GetStarRatingSpan(Hotel[index].StarRating) + "," + Hotel[index].City.__cdata + "," + MainImgGeo + "," + GetRoomPriceConvMark(TopRoomPrice, 0, 1, Hotel[index].SupplierCode) + "," + Hotel[index].LocationDetails.Location["0"]._latti + "," + Hotel[index].LocationDetails.Location["0"]._longi + "," + CurrencyStat + "," + [sjs + 1]);
                            sjs++;
                        }

                        if (isGetMapClicked) {
                            GetGmap();
                        }

                        //   $("#loader").css("display", "none");
                    }
                    else {//NO RESULTS
                        $('.mainpageloaderfull02').html('<h2 style="margin-bottom: 7px;"><img src="/HOTEL/hotel/images/nohotel.png" style="max-height:500px;" alt="loading_hotels"></h2><h3 style="color:#797979;">It seems high traffic during this check in dates and no rooms found for your search criteria. Please search later to get last minute offers or explore our deals for other dates! <a href="/HOTEL/searchhotel.html">Go Back</a></h3>')
                    }
                }
                else {
                    //Has Error Tag, Server Busy
                    $('.mainpageloaderfull02').html('<h2 style="margin-bottom: 7px;"><img src="/HOTEL/hotel/images/nohotel.png" alt="loading_hotels"></h2><h3 style="color:#ec4230;">' + data.Response.ResponseDetails.SearchHotelPricePaxResponse.Errors.Error.ErrorText + '  <i class="fa fa-refresh" onclick="location.reload();" aria-hidden="true"></i> <a href="/HOTEL/searchhotel.html">Go Back</a></h3>')
                }
            }
            else {
                $('.mainpageloaderfull02').html('<h2 style="margin-bottom: 7px;"><img src="/HOTEL/hotel/images/nohotel.png" alt="loading_hotels"></h2><h3 style="color:#ec4230;">' + data.Response.ResponseDetails.Errors.Error.ErrorText + '  <i class="fa fa-refresh" onclick="location.reload();" aria-hidden="true"></i> <a href="/HOTEL/searchhotel.html">Go Back</a></h3>')
            }
            $("#show_dummy_hotels").hide();
            setTimeout(function () { GetRecords(data); $(".hotelcount").html(''); }, 100);
        }
    }
    else {//NO RESULTS
        $('.mainpageloaderfull02').html('<h2 style="margin-bottom: 7px;"><img src="/HOTEL/hotel/images/nohotel.png" style="max-height:500px;" alt="loading_hotels"></h2><h3 style="color:#797979;">It seems high traffic during this check in dates and no rooms found for your search criteria. Please search later to get last minute offers or explore our deals for other dates! <a href="/HOTEL/searchhotel.html">Go Back</a></h3>')
    }
    //Events after result bind

    //price sorting
    tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-price' });

    //Minus Click
    $('body').on('click', '.more_view_click01', function () {
        //  debugger;
        var idMinus = $(this).attr("id");
        var idPlus = idMinus.replace("HotMinus", "HotPlus");
        $("#hotel_" + $(this).attr("id").split("_")[1]).css("border", "1px solid #cccccc");
        $("#hotel_" + $(this).attr("id").split("_")[1]).css("border-bottom", "none");

        $('#' + idPlus + ' i').removeClass('fa-spin');
        var idViewArea = idMinus.replace("HotMinus", "HViewArea");
        $("#" + idViewArea).hide();
        $('#' + idPlus).css("display", "block");
        $('#' + idMinus).css("display", "none");

        //$(".view_area01").slideToggle();
        //$('.icon_click01').css("display", "block");
        //$('.more_view_click01').css("display", "none");
    });

    //tabs start
    // Standard
    jQuery('.tabs.standard .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    // Animated Fade
    jQuery('.tabs.animated-fade .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    // Animated Slide 1
    jQuery('.tabs.animated-slide-1 .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
        jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    // Animated Slide 2
    jQuery('.tabs.animated-slide-2 .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    //tabs close

    //pagination start
    /* initiate plugin */
    $("div.holder").jPages({
        containerID: "itemContainer",
        callback: function (pages, items) {
            $("#legend1").html("Page " + pages.current + " of " + pages.count);
            $("#legend2").html(items.range.start + " - " + items.range.end + " of " + items.count);
        }
    });
    //pagination close

    //More info tab click
    $(".MoreDetHotClick").click(function () {
        $('.tab-links li').eq(0).addClass('active');
        $('.tab-links li').eq(1).removeClass('active');
        $('.tab-links li').eq(2).removeClass('active');

        $('.tab-content #tab11').css({ display: "block" });
        $('.tab-content #tab22').css({ display: "none" });
        $('.tab-content #tab23').css({ display: "none" });
    });

    //Filters Start

    //Price Slider Start
    $('.currency_name_bind').html(CurrencyStat + ' ');
    var mypricelimit = parseFloat(0);
    var MinAmount = parseFloat($("#itemContainer > li:first").data("price"));
    if (MinAmount === 'NaN' || isNaN(MinAmount)) {
        MinAmount = parseFloat(0);
    }
    var MaxAmount = parseFloat($("#itemContainer > li:last").data("price"));
    if (MaxAmount === 'NaN' || isNaN(MaxAmount)) {
        MaxAmount = parseFloat(1);
    }
    if (MinAmount == MaxAmount) {
        MaxAmount = MaxAmount + 1;
    }

    if (GetParameterValues('mprice') != 'NODATA') {
        var UserMaxPrice = parseFloat(GetParameterValues('mprice'));
        if (UserMaxPrice >= 0 && UserMaxPrice < 100) {
            mypricelimit = UserMaxPrice + (UserMaxPrice * 0.15)
        }
        else if (UserMaxPrice >= 100 && UserMaxPrice < 300) {
            mypricelimit = UserMaxPrice + (UserMaxPrice * 0.10)
        }
        else {
            mypricelimit = UserMaxPrice + (UserMaxPrice * 0.5)
        }

    }
    else {
        mypricelimit = MaxAmount;
    }


    if ($('#property-price-range')[0]) {
        var propertyPriceRange = document.getElementById('property-price-range');
        var propertyPriceRangeValues = [
            document.getElementById('property-price-upper'),
            document.getElementById('property-price-lower')
        ]
        if (nousliderstatus != 0) { propertyPriceRange.noUiSlider.destroy() }
        noUiSlider.create(propertyPriceRange, {
            start: [parseFloat(MinAmount), parseFloat(mypricelimit)],
            connect: true,
            range: {
                'min': parseFloat(MinAmount),
                'max': parseFloat(MaxAmount)
            }
        });

        propertyPriceRange.noUiSlider.on('update', function (values, handle) {
            propertyPriceRangeValues[handle].innerHTML = values[handle];
        });
        propertyPriceRange.noUiSlider.on('change.one', function (values, handle) {
            propertyPriceRangeValues[handle].innerHTML = values[handle];
            showProducts(values[0], values[1]);
        });
        nousliderstatus = parseInt(1);
    }

    //Price Slider End

    //Star Filter
    //Kelvin Abella - Quotations 6/23/2018
    $(".starcbox").unbind().click(function () {
        //$(".starcbox").click(function () {
        //          $("#loadingGIFFilter").show();
        var IDCHECK = $(this).attr("id");
        //IDCHK = IDCHK.charAt(0);
        CallStarFilter(IDCHECK);
        ShowLIBlocks();
    });

    //Show All
    //Kelvin Abella - Quotations 6/23/2018
    $(".showall").click(function () {
        $(this).parent().find(" .checkBox_area input").each(function () {
            if ($(this).is(":checked")) {
            }
            else {
                $(this).click();
            }
        });
    });

    $(".hotel_room_details01").each(function () {
        $(this).attr("data-show", 0);
    });
    var dealstatus = parseInt(0);
    //var systemfarestatus = parseInt(0);
    $(".hotel_room_details01").each(function () {
        if ($(this).find('i.fa-tag').length !== 0) {
            dealstatus = parseInt(1);
            $(this).attr("data-offer", "1");
            var temphotelidfordeals = $(this).attr("id").split("_")[1];
            $("#hotel_" + temphotelidfordeals).attr("data-offer", "1");
            if ($("#hotel_" + temphotelidfordeals).find('div.hoteldealhighlightag').length == 0) {
                $("#hotel_" + temphotelidfordeals).append('<div data-toggle="tooltip" data-placement="top" data-original-title="Deals available" class="hoteldealhighlightag"><i class="fa fa-tags sp01" aria-hidden="true"></i></div>');
            }
        }
        else {
            //systemfarestatus = parseInt(1);
            $(this).attr("data-offer", "0");
        }

        //if ($(this).find('i.fa-money').length === 0) {
        //    var temphotelidfordeals = $(this).attr("id").split("_")[1];
        //    if ($("#hotel_" + temphotelidfordeals).find('div.hoteldealhighlightag').length == 0) {
        //        $("#hotel_" + temphotelidfordeals).append('<div data-toggle="tooltip" data-placement="top" data-original-title="Free Cancellation" class="hoteldealhighlightag"><i class="fa fa-dollar sp01" aria-hidden="true"></i></div>');
        //    }
        //}



    });
    //if (dealstatus == 0 || systemfarestatus == 0) {
    if (dealstatus == 0) {
        $("#DealFilterBox").hide();
    } else {
        $("#DealFilterBox").show();
    }

    //GenerateStarFilter();
    //GenerateLocationFilter();
    //GenerateHotelNameFilter();
    //GenerateHotelAmenitiesFilter();
    //GenerateRoomAmenitiesFilter();
    //GeneratePOIFilter();


    //GenerateHotelPoiFilter();
    //Advance filters
    if (GetParameterValues('name') != 'NODATA') {
        $('#txtHotelnameFilter').val(GetParameterValues('name'));
        FinalizeHotelNameFIlter();
    }

    var parastar = GetParameterValues('star');
    if (parastar != 'NODATA') {
        for (var i = 1; i < 6; i++) { if (parastar != i.toString()) { $('#' + i.toString() + 'S').attr("checked", false); } }
        for (var i = 1; i < 6; i++) { CallStarFilter(i + 'S'); }
        ShowLIBlocks();
    }


    //  $(".hotel_room_details #big-mage img:not(:first)").hide();

    //Filter End

    //user sorting
    finalizeinitialsorting();

    //Kelvin Abella - Quotations 6/23/2018
    $(".XlBox").unbind().click(function () {
        var buttonName = $(this).attr("id");
        var chkCharges = "";
        var chkBool = "";
        if (buttonName == "WithXLCharges") {
            chkCharges = "WithXLCharges";
            chkBool = "true";
        }
        else {
            chkCharges = "WithoutXLCharges";
            chkBool = "false";
        }

        var numroom = GetParameterValues('numroom');
        $(".hotel_view_area").each(function () {
            var HotelIndex = $(this).attr("id").replace("hotel_", "");

            var datashowchangestatus = parseInt(0);
            $("#hotel_" + HotelIndex + " .hotel_room_details01").each(function () {
                var SelRoomID = $(this).attr("id");
                var PopID = SelRoomID.replace('r', 'cmod');
                if ($('#' + PopID + ' .modal-dialog .modal-content .sp_bg_hotel h3').attr("xlcharge") == chkBool) {
                    var datashow = parseInt($(this).attr("data-show"));
                    if (document.getElementById(chkCharges).checked == true) {
                        if (datashow > 0) {
                            datashowchangestatus = 1;
                            $(this).attr("data-show", (datashow - parseInt(1)));
                        }
                    }
                    else {
                        datashowchangestatus = 1;
                        $(this).attr("data-show", (datashow + parseInt(1)));
                    }
                }
            });

            if (datashowchangestatus == 1) {
                rearrangehotelinnerrates(numroom, HotelIndex);
            }


        });


        ShowLIBlocks();
    });

    $(".Statusbox").unbind().click(function () {
        var buttonName = $(this).attr("id");
        var rStat = "";

        if (ButtonName == "ChkStatusAvailable") {
            rStat = "IM";
        }
        else {
            rStat = "OR";
        }

        var numroom = GetParameterValues('numroom');
        $("#itemContainer").find(".hotel_view_area").each(function () {
            var HotelIndex = $(this).attr("id").replace("hotel_", "");
            var datashowchangestatus = parseInt(0);
            $("#hotel_" + HotelIndex + " .hotel_room_details01").each(function () {
                var SelRoomID = $(this).attr("id");
                var PopID = SelRoomID.replace('r', 'cmod');
                if ($('#' + SelRoomID).attr("r-stat") == rStat) {
                    var datashow = parseInt($(this).attr("data-show"));
                    if (document.getElementById(buttonName).checked == true) {
                        if (datashow > 0) {
                            datashowchangestatus = 1;
                            $(this).attr("data-show", (datashow - parseInt(1)));
                        }
                    }
                    else {
                        datashowchangestatus = 1;
                        $(this).attr("data-show", (datashow + parseInt(1)));
                    }
                }
            });
            if (datashowchangestatus == 1) {
                rearrangehotelinnerrates(numroom, HotelIndex);
            }
        });

        ShowLIBlocks();
    });

    //Kelvin Abella - Quotations 6/24/2018
    $(".Dealsbox").unbind().click(function () {
        var buttonName = $(this).attr("id");
        var dataOfer = "";

        if (buttonName == "ChkAirborneDeals") {
            dataOfer = "1";
        }
        else {
            dataOfer = "0";
        }

        var numroom = GetParameterValues('numroom');
        $("#itemContainer").find(".hotel_view_area").each(function () {
            var HotelIndex = $(this).attr("id").replace("hotel_", "");
            var datashowchangestatus = parseInt(0);

            $("#hotel_" + HotelIndex).find(".hotel_room_details01").each(function () {
                var $this = $(this);
                var SelRoomID = $this.attr("id");
                if ($('#' + SelRoomID).attr("data-offer") == dataOfer) {
                    var datashow = parseInt($this.attr("data-show"));
                    if (document.getElementById(buttonName).checked == true) {
                        if (datashow > 0) {
                            datashowchangestatus = 1;
                            $this.attr("data-show", (datashow - parseInt(1)));
                        }
                    }
                    else {
                        datashowchangestatus = 1;
                        $this.attr("data-show", (datashow + parseInt(1)));
                    }
                }
            });
            if (datashowchangestatus == 1) {
                rearrangehotelinnerrates(numroom, HotelIndex);
            }
        });
        ShowLIBlocks();

    });

    $('[data-toggle="tooltip"]').tooltip();


    updatetotalnumberofhotelshownbycategory();



    //      $(".loadingGIF").hide();



}

function finalizeinitialsorting() {
    if (GetParameterValues('sort') != 'NODATA') {
        var sorttype = GetParameterValues('sort');
        $('#topFilterUL li').removeClass('active');
        $('#sor-data-' + sorttype.split('-')[0] + '').addClass('active');

        var FilterText = '';
        var FilterValue = '';
        if (sorttype == "price-a") {
            tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-price' });
            FilterText = 'Price (Lowest)';
            FilterValue = '1';
        }
        else if (sorttype == "price-de") {
            tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-price' });
            FilterText = 'Price (Highest)';
            FilterValue = '2';
        }
        else if (sorttype == "name-a") {
            tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-name' });
            FilterText = 'Hotel (A to Z)';
            FilterValue = '3';
        }
        else if (sorttype == "name-de") {
            tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-name' });
            FilterText = 'Hotel (Z to A)';
            FilterValue = '4';
        }
        else if (sorttype == "star-a") {
            tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-star' });
            FilterText = 'Star (Lowest)';
            FilterValue = '5';
        }
        else if (sorttype == "star-de") {
            tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-star' });
            FilterText = 'Star (Highest)';
            FilterValue = '6';
        }
        $("#mobddlfilterHotelselected").html(FilterText);
        $("#ddlMobFilterHotel").val(FilterValue);
    }

}
function ShowCancellationChargeIcon() {
    $(".cancpolicydiv h3").each(function () {
        var Xlstatus = $(this).attr("xlcharge");
        if (Xlstatus == "true") {
            var tempid = $(this).parent().parent().parent().parent().attr("id").replace("cmod", "r");
            $("#" + tempid).find("div.cost_area").find("a").append('<i class="fa fa-money red" data-toggle="tooltip" data-placement="top" data-original-title="Under cancellation fare" aria-hidden="true"></i>');
        }
    });
}
function CallStarFilter(IDCHK) {
    $("#itemContainer li").each(function () {
        var DataStar = $(this).attr("data-star");
        var ID = $(this).attr("id");
        var DataShowValue = parseInt($("#" + ID).attr("data-show"));
        var DataShowValueNew = parseInt(0);

        if (IDCHK.charAt(0) == DataStar) {
            if (document.getElementById(IDCHK).checked == true) {
                if (DataShowValue == 0) {
                    DataShowValueNew = 0;
                }
                else {
                    DataShowValueNew = DataShowValue - 1;
                }
            }
            else {
                DataShowValueNew = DataShowValue + 1;
            }
            $("#" + ID).attr("data-show", DataShowValueNew);
        }
    });

}
function GenerateHotelNameFilter() {
    $("#txtHotelnameFilter").keyup(function () {
        FinalizeHotelNameFIlter();
    });

}
function GenerateHotelPoiFilter() {
    $("#selectPOIFilter").change(function () {
        FinalizePOIFIlter();
    });
}
function FinalizeHotelNameFIlter() {
    var tempval = $('#txtHotelnameFilter').val().toLowerCase();
    $("#itemContainer .hotel_view_area").each(function () {
        var DataShowValue = parseInt($(this).attr("data-show"));
        var temphotelname = $(this).attr("data-name").toLowerCase();
        var DataShowValueNew = parseInt(0);
        if (DataShowValue == 0) { DataShowValueNew = 0; }
        else { DataShowValue = DataShowValue - 1; }
        if (temphotelname.indexOf(tempval) == -1) {
            if (DataShowValue == 0) { DataShowValueNew = 1; }
            else { DataShowValueNew = DataShowValue + 1; }
        }
        else {
            if (DataShowValue == 0) { DataShowValueNew = 0; }
            else { DataShowValueNew = DataShowValue + 1; }
        }
        $(this).attr("data-show", DataShowValueNew);
    });
    ShowLIBlocks();
}
function FinalizePOIFIlter() {
    var tempval = $('#selectPOIFilter option:selected').val().toLowerCase();
    $("#itemContainer .hotel_view_area").each(function () {
        var DataShowValue = parseInt($(this).attr("data-show"));
        var temphotelname = $(this).attr("d-poi").toLowerCase();
        var DataShowValueNew = parseInt(0);
        if (DataShowValue == 0) { DataShowValueNew = 0; }
        else { DataShowValue = DataShowValue - 1; }
        if (temphotelname.indexOf(tempval) == -1 && tempval != 'none') {
            if (DataShowValue == 0) { DataShowValueNew = 1; }
            else { DataShowValueNew = DataShowValue + 1; }
        }
        else {
            if (DataShowValue == 0) { DataShowValueNew = 0; }
            else { DataShowValueNew = DataShowValue + 1; }
        }
        $(this).attr("data-show", DataShowValueNew);
    });
    ShowLIBlocks();
}

function GenerateLocationFilter() {
    var UlLocations = '';
    for (var it7 = 0; it7 < AllLocationCodeArray.length; it7++) {
        if (UlLocations.indexOf("loc_" + AllLocationCodeArray[it7].locode + '"') == -1) {
            if (AllLocationCodeArray[it7].locode != "?") {
                var tolnohotelsbylocation = $("#itemContainer .hotel_view_area").filter(function () { return $(this).attr('d-loc').indexOf(AllLocationCodeArray[it7].locode) != -1; }).length;
                UlLocations += '<div class="checkBox_area"> <input name="HotelLocations" class="loccbox" type="checkbox" value="" checked="" id="loc_' + AllLocationCodeArray[it7].locode + '"> <label for="web" class="formsfont">' + AllLocationCodeArray[it7].locname + ' (' + tolnohotelsbylocation + ' hotels)</label> <span>Only</span></div>';
            }
            else {
                UlLocations += '<div class="checkBox_area"> <input name="HotelLocations" class="loccbox" type="checkbox" value="" checked="" id="loc_' + AllLocationCodeArray[it7].locode + '"> <label for="web" class="formsfont">Other</label> <span>Only</span></div>';
            }
        }
    }
    $("#ulLocFilter").html(UlLocations);
    //Kelvin Abella - Quotations 6/24/2018
    $(".loccbox").unbind().click(function () {
        //$(".loccbox").click(function () {
        var temploccode = $(this).attr("id").split("_")[1];
        var checkedstatus = 0;
        if (document.getElementById($(this).attr("id")).checked == true) {
            checkedstatus = 1;
        }
        $("#itemContainer .hotel_view_area").each(function () {
            var DataLoc = $(this).attr('d-loc');
            var DataShowValue = parseInt($(this).attr("data-show"));
            var DataShowValueNew = parseInt(0);
            if (DataLoc.indexOf(temploccode) != -1) {
                var datalocarray = DataLoc.split(",");
                var tempstatus = 0;
                for (var i645 = 0; i645 < datalocarray.length; i645++) {
                    var templocval = datalocarray[i645];
                    if (templocval != temploccode) {
                        if (document.getElementById("loc_" + templocval).checked == true) {
                            tempstatus = 1;
                        }
                    }
                }

                if (checkedstatus == 1) {
                    if (tempstatus == 0) {
                        if (DataShowValue == 0) { DataShowValueNew = 0; }
                        else { DataShowValueNew = DataShowValue - 1; }
                    }
                    else { DataShowValueNew = DataShowValue; }
                }
                else {
                    if (tempstatus == 0) { DataShowValueNew = DataShowValue + 1; }
                    else { DataShowValueNew = DataShowValue; }
                }
                $(this).attr("data-show", DataShowValueNew);
            }
        });

        ShowLIBlocks();
    });
}

function GenerateStarFilter() {

    var container = $("#itemContainer .hotel_view_area");
    var tot5starhotels = container.filter(function () { return $(this).attr('data-star') == '5'; }).length;
    var tot4starhotels = container.filter(function () { return $(this).attr('data-star') == '4'; }).length;
    var tot3starhotels = container.filter(function () { return $(this).attr('data-star') == '3'; }).length;
    var tot2starhotels = container.filter(function () { return $(this).attr('data-star') == '2'; }).length;
    var tot1starhotels = container.filter(function () { return $(this).attr('data-star') == '1'; }).length;

    showFilter(5, tot5starhotels);
    showFilter(4, tot4starhotels);
    showFilter(3, tot3starhotels);
    showFilter(2, tot2starhotels);
    showFilter(1, tot1starhotels);

}
function showFilter(Star, count) {
    if (count > 0) {
        $("#chk_starfilter_" + Star).show();
        // $("#chk_starfilter_" + Star + " label").html($("#chk_starfilter_" + Star + " label").html() + "  (" + count + " hotels)");
        $("#chk_starfilter_" + Star + " label").html($("#chk_starfilter_" + Star + " label").html());
        $("#lblcount_" + Star).html("  (" + count + " hotels)");
    }
    else {
        $("#chk_starfilter_" + Star).hide();
    }
}
function showProducts(minPrice, maxPrice) {
    $("#itemContainer .hotel_view_area").hide().filter(function () {
        var price = parseInt($(this).data("price"), 10);
        var dshow = $(this).attr("data-show");
        // alert(dshow);
        if (dshow == '0') {
            return price >= minPrice - 1 && price <= maxPrice + 1;
        }
        else {
            return false;
        }

    }).show();
    //  $(".hotelcount").html('<p>' + $("#itemContainer .hotel_view_area").filter(function () { return $(this).css('display') !== 'none'; }).length + ' Hotels Found</p>');

    //Kelvin Abella - Quotations 6/25/2018
    var numres = $("#itemContainer .hotel_view_area").filter(function () { return $(this).css('display') !== 'none'; }).length;
    //console.log(numres);
    if (parseInt(numres) == 0) {
        $("#divnores").show();
    }
    else {
        $("#divnores").hide();
    }
    $(".hotelcount").html('<p>' + numres + ' Hotels Found out of ' + $("#itemContainer .hotel_view_area").filter(function () { return $(this) }).length + ' results. </p>');
}

function ShowLIBlocks() {

    var MinAmount = parseFloat($("#property-price-upper").html());
    if (MinAmount === 'NaN') { MinAmount = parseFloat(0); }
    var MaxAmount = parseFloat($("#property-price-lower").html());
    if (MaxAmount === 'NaN') { MaxAmount = parseFloat(1); }


    $(".hotel_view_area").each(function () {
        var DataShowValue = $(this).attr("data-show");
        var tempprice = parseFloat($(this).data("price"), 10);
        var ID = $(this).attr("id");
        if (ID != "-1" && isNaN(tempprice) == false) {
            if (tempprice >= MinAmount && tempprice <= MaxAmount) {
                if (DataShowValue == 0) { $('#' + ID).show(); }
                else { $('#' + ID).hide(); }
            }
            else { $('#' + ID).hide(); }
        }
    });
    var numres = $("#itemContainer .hotel_view_area").filter(function () { return $(this).css('display') !== 'none'; }).length;
    $(".hotelcount").html('<p>' + numres + ' Hotels Found out of ' + $("#itemContainer .hotel_view_area").filter(function () { return $(this) }).length + ' results. </p>');
    //Kelvin Abella - Quotations 6/25/2018
    if (parseInt(numres) == 0) {
        $("#divnores").show();
    }
    //if (numres == '0') { $("#divnores").show(); }
    else {
        $("#divnores").hide();
    }
    //        $("#loadingGIFFilter").hide();

    //Price Slider Start 

    var SliderMinAmount = parseFloat(0);
    var SliderMaxAmount = parseFloat(0);
    try {
        var listtotal = [];
        listtotal = jQuery.grep($("#itemContainer > li"), function (x) { return x.dataset.show == "0"; });
        SliderMinAmount = parseFloat($(listtotal[0]).data("price"));
        SliderMaxAmount = parseFloat($(listtotal[listtotal.length - 1]).data("price"));

    } catch (err) {
        SliderMinAmount = parseFloat(0);
        SliderMaxAmount = parseFloat(0);
        //console.log(err)
    }


    var mypricelimit = parseFloat(0);
    if (isNaN(SliderMinAmount)) {
        SliderMinAmount = parseFloat(0);
    }
    if (isNaN(SliderMaxAmount)) {
        SliderMaxAmount = parseFloat(1);
    }
    if (SliderMinAmount == SliderMaxAmount) {
        SliderMaxAmount = SliderMaxAmount + 1;
    }

    //if (GetParameterValues('mprice') != 'NODATA') {
    //    var UserMaxPrice = parseFloat(GetParameterValues('mprice'));
    //    if (UserMaxPrice >= 0 && UserMaxPrice < 100) {
    //        mypricelimit = UserMaxPrice + (UserMaxPrice * 0.15)
    //    }
    //    else if (UserMaxPrice >= 100 && UserMaxPrice < 300) {
    //        mypricelimit = UserMaxPrice + (UserMaxPrice * 0.10)
    //    }
    //    else {
    //        mypricelimit = UserMaxPrice + (UserMaxPrice * 0.5)
    //    }

    //}
    //else {
    //    mypricelimit = SliderMaxAmount;
    //}


    if ($('#property-price-range')[0]) {
        var propertyPriceRange = document.getElementById('property-price-range');
        var propertyPriceRangeValues = [
            document.getElementById('property-price-upper'),
            document.getElementById('property-price-lower')
        ]
        if (nousliderstatus != 0) {

            try {
                propertyPriceRange.noUiSlider.destroy();
            }
            catch (err) {
                nousliderstatus = parseInt(0);
            }


        }
        noUiSlider.create(propertyPriceRange, {
            start: [parseFloat(SliderMinAmount), parseFloat(SliderMaxAmount)],
            connect: true,
            range: {
                'min': parseFloat(SliderMinAmount),
                'max': parseFloat(SliderMaxAmount)
            }
        });

        propertyPriceRange.noUiSlider.on('update', function (values, handle) {
            propertyPriceRangeValues[handle].innerHTML = values[handle];
            showProducts(values[0], values[1]);
        });
        propertyPriceRange.noUiSlider.on('change.one', function (values, handle) {
            propertyPriceRangeValues[handle].innerHTML = values[handle];
            showProducts(values[0], values[1]);
        });

        nousliderstatus = parseInt(1);
    }

    //Price Slider End

}

function updatetotalnumberofhotelshownbycategory() {

    var totalsystemrates = $(".hotel_room_details01").filter(function () { return $(this).attr('data-offer') == '0'; }).length;
    var totairbornedeals = $(".hotel_room_details01").filter(function () { return $(this).attr('data-offer') == '1'; }).length;
    $("#ChkAirborneDeals").parent().find("label").html(Setup.Agency.AgencyShortName + " Deals (" + totairbornedeals + " rooms)");
    $("#ChkSystemRates").parent().find("label").html("System Fares (" + totalsystemrates + " rooms)");

    if (totairbornedeals == 0 || totalsystemrates == 0) {
        $("#DealFilterBox").hide();
    }


    var totalavailablerates = $(".hotel_room_details01").filter(function () { return $(this).attr('r-stat') == 'IM'; }).length;
    var totalonrequestrates = $(".hotel_room_details01").filter(function () { return $(this).attr('r-stat') == 'OR'; }).length;
    $("#ChkStatusAvailable").parent().find("label").html("Available (" + totalavailablerates + " rooms)");
    $("#ChkStatusOnRequest").parent().find("label").html("On Request (" + totalonrequestrates + " rooms)");

    var totalunderxlpolicy = $(".cancpolicydiv").filter(function () { return $(this).find("h3").attr('xlcharge') == 'true'; }).length;
    var totalnotunderxlpolicy = $(".cancpolicydiv").filter(function () { return $(this).find("h3").attr('xlcharge') == 'false'; }).length;
    $("#WithXLCharges").parent().find("label").html("With Cancellation charges (" + totalunderxlpolicy + " rooms)");
    $("#WithoutXLCharges").parent().find("label").html("Free Cancellation * (" + totalnotunderxlpolicy + " rooms)");

}

function GetStarRatingSpan(star) {
    var StarSpan = '';
    if (star != undefined) {
        for (var is = 0; is < 5; is++) {
            if (is < parseInt(star)) {
                StarSpan += '<i class="fa fa-star" aria-hidden="true"></i>';
            }
            else {
                StarSpan += '<i class="fa fa-star-o" aria-hidden="true"></i>';
            }
        }
    }
    else {
        StarSpan = '<i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i>';
    }
    return StarSpan;
}
function GetFullLocation(LocationDetails) {
    var Location = '';
    if (LocationDetails.Location.length != null) {
        Location = LocationDetails.Location[LocationDetails.Location.length - 1].__cdata;
        //for (var i2 = 0; i2 < LocationDetails.Location.length; i2++) {
        //    if (Location == '') {
        //        Location += LocationDetails.Location[i2].__cdata;
        //    }
        //    else {
        //        Location += ',' + LocationDetails.Location[i2].__cdata;
        //    }
        //}
    }
    else {
        Location = LocationDetails.Location.__cdata;
    }
    if (Location.length > 12) {
        Location = Location.slice(0, 11);
        Location += '..';
    }
    if (Location == '?') {
        Location = 'Other';
    }
    return Location;
}
function GetFullLocationFull(LocationDetails) {
    var Location = '';
    if (LocationDetails.Location.length != null) {
        Location = LocationDetails.Location[LocationDetails.Location.length - 1].__cdata;
    }
    else {
        Location = LocationDetails.Location.__cdata;
    }
    if (Location == '?') {
        Location = 'Other';
    }
    return Location;
}
function GetFullLocationFilter(LocationDetails) {
    var Location = '';
    if (LocationDetails.Location.length != null) {
        for (var iff = 0; iff < LocationDetails.Location.length; iff++) {
            if (Location == '') {
                Location += LocationDetails.Location[iff]._Code;
            }
            else {
                Location += ',' + LocationDetails.Location[iff]._Code;
            }
            AllLocationCodeArray.push({ "locode": LocationDetails.Location[iff]._Code, "locname": LocationDetails.Location[iff].__cdata });
        }
    }
    else {
        Location = LocationDetails.Location._Code;
        AllLocationCodeArray.push({ "locode": LocationDetails.Location._Code, "locname": LocationDetails.Location.__cdata });
    }
    return Location;
}
function GetFullLocationTitle(LocationDetails) {
    var Location = '';
    if (LocationDetails.Location.length != null) {
        for (var iff = 0; iff < LocationDetails.Location.length; iff++) {
            if (Location == '') {
                Location += LocationDetails.Location[iff].__cdata;
            }
            else {
                Location += ',' + LocationDetails.Location[iff].__cdata;
            }
        }
    }
    else {
        Location = LocationDetails.Location.__cdata;
    }
    return Location;
}
function GetFullLocationTitlenew(locationadd, LocationDetails) {
    var Location = '';
    if (locationadd != "") {
        if (LocationDetails.Location.length != null) {
            for (var iff = 0; iff < LocationDetails.Location.length; iff++) {
                if (Location == '') {
                    Location += LocationDetails.Location[iff].__cdata;
                }
                else {
                    Location += ',' + LocationDetails.Location[iff].__cdata;
                }
            }
        }
        else {
            Location = LocationDetails.Location.__cdata;
        }

        Location = locationadd + "," + Location;


    }

    else {
        if (LocationDetails.Location.length != null) {
            for (var iff = 0; iff < LocationDetails.Location.length; iff++) {
                if (Location == '') {
                    Location += LocationDetails.Location[iff].__cdata;
                }
                else {
                    Location += ',' + LocationDetails.Location[iff].__cdata;
                }
            }
        }
        else {
            Location = LocationDetails.Location.__cdata;
        }
    }
    return Location;
}

function GetRoomPriceConvMark(ogprice, cancelation, rooms, supplier) {
    var OGPricee = parseFloat(ogprice);
    var SellingRateUSD = parseFloat(AgencyCurrencyExRate);
    var BuyyingRateCurr = parseFloat(1);
    NewPrice = (ogprice * SellingRateUSD) / BuyyingRateCurr;
    var NewPriceMarkup = NewPrice;
    // if (cancelation != 1) {
    NewPriceMarkup = getMarkupDetails(NewPrice, rooms, supplier);
    //  }
    return Math.ceil(NewPriceMarkup * 100) / 100;
}
//function GetMeals(Meals) {
//    var MealData = '';
//    if (Meals.Basis != null) {
//        if (Meals.Basis.__cdata == "None") {
//            MealData = 'Room Only';
//        }
//        else if (Meals.Basis.__cdata == "Breakfast") {
//            MealData = Meals.Breakfast.__cdata + ' ' + Meals.Basis.__cdata;
//        }
//        else {
//            MealData = Meals.Basis.__cdata;
//        }
//    }
//    else {
//        MealData = 'Room Only';
//    }
//    return MealData;
//}
function GetMeals(Meals) {
    var MealData = '';
    if (Meals != null && Meals != undefined) {
        if (Meals.Basis != null) {
            if (Meals.Basis.__cdata == "None") {
                MealData = 'Room Only';
            }
            else if (Meals.Basis.__cdata == "Breakfast") {
                //if (Meals.Breakfast != null && Meals.Breakfast != undefined) {
                //    MealData = Meals.Breakfast.__cdata + ' ' + Meals.Basis.__cdata;

                //}
                //else {
                MealData = Meals.Basis.__cdata;
                // }
            }
            else {
                MealData = Meals.Basis.__cdata;
            }
        }
        else {
            MealData = 'Room Only';
        }
    }
    else {
        MealData = 'Room Only';

    }
    return MealData;
}
function GetConfirmation(Confirmation) {
    var ConfData = '';
    if (Confirmation == "AVAILABLE") {
        ConfData = '<i class="fa fa-check-circle htl_sp" aria-hidden="true"></i><span>Available</span>';
    }
    else if (Confirmation == "On Request") {
        ConfData = '<i class="fa fa-question-circle on_request_clr" aria-hidden="true"></i><span class="status_area_clr">On Request</span>';
    }

    return ConfData;
}
function GetCancellationPolicy(ChargeConditions, supplier) {
    window.localStorage.setItem("ChargeConditions", JSON.stringify(ChargeConditions));
    var hidedpolicy = false;
    var xlcharge = true;
    var Today = moment().format('MMM DD YYYY, dddd');
    //ChargeConditions
    var CancPolicyString = '<h3 #PASTALERT# #XLXHARGE#>Cancellation Policy #SHOWALLCPOLICYBTN#</h3>';
    if (ChargeConditions.length != null) {
        //Multiple Charge condi
        for (var icc = 0; icc < ChargeConditions.length; icc++) {
            var Conditype = ChargeConditions[icc]._Type;
            //if (Conditype == "cancellation") {
            var Condi = ChargeConditions[icc].Condition;
            if (Condi.length != null) {
                //Mul Condi
                for (var iccc = (Condi.length - 1); iccc >= 0; iccc--) {
                    if (Condi[iccc]._Charge == "true") {
                        if (Condi[iccc]._FromDay == "0" && Condi[iccc]._ToDay != undefined) {

                            var DayToSubTo = parseInt(CancellationGPeriod) + parseInt(Condi[iccc]._ToDay);
                            //  var NewDateTo = moment(CinMDY, DateFormate).subtract(DayToSubTo, "days").format("MMM DD YYYY, dddd");
                            var NewDateTo = moment(Condi[iccc]._ToDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                            CancPolicyString += '<h4>Cancellation on or after <span>' + NewDateTo + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                        }
                        //else if (Condi[iccc]._FromDay == "0" && Condi[iccc]._ToDay == undefined) {
                        else if (Condi[iccc]._FromDay == Condi[iccc]._ToDay) {
                            CancPolicyString += '<h4  style="color:red;font-weight:bolder;">Non Refundable Booking with Cancellation Charges <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                            xlcharge = true;
                        }
                        else if (Condi[iccc]._FromDay != "0" && Condi[iccc]._ToDay != undefined) {
                            var DayToSubFrom = parseInt(CancellationGPeriod) + parseInt(Condi[iccc]._FromDay);
                            var NewDateFrom = moment(Condi[iccc]._FromDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                            //var NewDateFrom = moment(CinMDY, DateFormate).subtract(DayToSubFrom, "days").format("MMM DD YYYY, dddd");

                            var DayToSubTo = parseInt(CancellationGPeriod) + parseInt(Condi[iccc]._ToDay);
                            var NewDateTo = moment(Condi[iccc]._ToDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                            //var NewDateTo = moment(CinMDY, DateFormate).subtract(DayToSubTo, "days").format("MMM DD YYYY, dddd");

                            var iscurrentDate = moment(NewDateTo, 'MMM DD YYYY, dddd').isSameOrBefore(new Date(), "day");
                            if (iscurrentDate) {
                                hidedpolicy = true;
                                CancPolicyString += '<h4 style="display:none;">Cancellation between <span>' + NewDateTo +
                                    '</span> and <span>' + NewDateFrom + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                            }
                            else {
                                CancPolicyString += '<h4>Cancellation between <span>' + NewDateTo +
                                    '</span> and <span>' + NewDateFrom + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                            }
                        }
                        else if (Condi[iccc]._FromDay != "0" && Condi[iccc]._ToDay == undefined) {

                            var DayToSubFrom = parseInt(CancellationGPeriod) + parseInt(Condi[iccc]._FromDay);

                            //var NewDateFrom = moment(CinMDY, DateFormate).subtract(DayToSubFrom, "days").format("MMM DD YYYY, dddd");
                            var NewDateFrom = moment(Condi[iccc]._FromDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                            var today = new Date();
                            var NewDateTo = today.toDateString();
                            CancPolicyString += '<h4>Cancellation between <span>' + NewDateTo +
                                '</span> and <span>' + NewDateFrom + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                        }

                    }
                    else if (Condi[iccc]._Charge == "false") {
                        var DayToSubFree = parseInt(CancellationGPeriod) + parseInt(Condi[iccc]._FromDay);
                        //var NewDateFree = moment(CinMDY, DateFormate).subtract(DayToSubFree, "days").format("MMM DD YYYY, dddd");
                        var NewDateFree = moment(Condi[iccc]._FromDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                        var iscurrentDate = moment(NewDateFree, 'MMM DD YYYY, dddd').isSameOrBefore(new Date(), "day");
                        if (iscurrentDate) {
                            hidedpolicy = true;
                            CancPolicyString += '<h4 style="color:green;font-weight:bolder;display:none;">Free cancellation on or before <span>' + NewDateFree + '</span></h4>';
                        }
                        else {
                            CancPolicyString += '<h4 style="color:green;font-weight:bolder;">Free cancellation on or before <span>' + NewDateFree + '</span></h4>';
                            xlcharge = false;
                        }
                    }
                }
            }
            else {
                //Single Condi
                if (Condi._Charge == "true") {
                    if (Condi._FromDay == "0" && Condi._ToDay != undefined) {

                        var DayToSubTo = parseInt(CancellationGPeriod) + parseInt(Condi._ToDay);
                        //var NewDateTo = moment(CinMDY, DateFormate).subtract(DayToSubTo, "days").format("MMM DD YYYY, dddd");
                        var NewDateTo = moment(Condi._ToDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                        CancPolicyString += '<h4>Cancellation on or after <span>' + NewDateTo + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                    }
                    //else if (Condi._FromDay == "0" && Condi._ToDay == undefined) {
                    else if (Condi._FromDay == Condi._ToDay) {
                        CancPolicyString += '<h4  style="color:red;font-weight:bolder;">Non Refundable Booking with Cancellation Charges <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                    }
                    else if (Condi._FromDay != "0" && Condi._ToDay != undefined) {
                        var DayToSubFrom = parseInt(CancellationGPeriod) + parseInt(Condi._FromDay);

                        //var NewDateFrom = moment(CinMDY, DateFormate).subtract(DayToSubFrom, "days").format("MMM DD YYYY, dddd");
                        var NewDateFrom = moment(Condi._FromDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                        var DayToSubTo = parseInt(CancellationGPeriod) + parseInt(Condi._ToDay);

                        //var NewDateTo = moment(CinMDY, DateFormate).subtract(DayToSubTo, "days").format("MMM DD YYYY, dddd");
                        var NewDateTo = moment(Condi._ToDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                        CancPolicyString += '<h4>Cancellation between <span>' + NewDateTo +
                            '</span> and <span>' + NewDateFrom + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                    }
                    else if (Condi._FromDay != "0" && Condi._ToDay == undefined) {

                        var DayToSubFrom = parseInt(CancellationGPeriod) + parseInt(Condi._FromDay);

                        //var NewDateFrom = moment(CinMDY, DateFormate).subtract(DayToSubFrom, "days").format("MMM DD YYYY, dddd");
                        var NewDateFrom = moment(Condi._FromDay, "DD-MM-YYYY HH:mm:ss a").format("MMM DD YYYY, dddd");
                        var today = new Date();
                        var NewDateTo = today.toDateString();

                        CancPolicyString += '<h4>Cancellation between <span>' + NewDateTo +
                            '</span> and <span>' + NewDateFrom + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                    }
                    else {
                        CancPolicyString += '<h4>Cancellation between <span>' + Condi._FromDay +
                            '</span> to <span>' + Condi._ToDay + '</span> days before checkin will be charge <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                    }
                }
                else if (Condi._Charge == "false") {
                    CancPolicyString += '<h4>No Cancellation charge before <span>' + Condi._FromDay + '</span> days</h4>';
                    xlcharge = false;
                }
            }
            //}
        }
    }
    else {
        //Single Charge condi
        var Conditype = ChargeConditions._Type;
        if (Conditype == "cancellation") {
            var Condi = ChargeConditions.Condition;
            if (Condi.length != null) {
                //Mul Condi
                for (var iccc = 0; iccc < Condi.length; iccc++) {
                    CancPolicyString += '<h4>Cancellation between <span>' + Condi[iccc]._FromDay +
                        '</span> to <span>' + Condi[iccc]._ToDay + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi[iccc]._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
                }
            }
            else {
                //Single Condi
                CancPolicyString += '<h4>Cancellation between <span>' + Condi._FromDay +
                    '</span> to <span>' + Condi._ToDay + '</span> will be charged <span>' + GetRoomPriceConvMark(Condi._ChargeAmount, 1, numroom, supplier) + ' ' + CurrencyStat + '</span></h4>';
            }
        }
    }
    if (hidedpolicy == true) {
        CancPolicyString = CancPolicyString.replace('#SHOWALLCPOLICYBTN#', '<a data-toggle="tooltip" data-placement="top" data-original-title="Show past policies" onclick="ShowAllCancPolicy(this)"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>');
        CancPolicyString = CancPolicyString.replace('#PASTALERT#', 'd-past="true"');
    }
    else {
        CancPolicyString = CancPolicyString.replace('#SHOWALLCPOLICYBTN#', '');
        CancPolicyString = CancPolicyString.replace('#PASTALERT#', '');
    }
    CancPolicyString = CancPolicyString.replace('#XLXHARGE#', 'xlcharge="' + xlcharge + '"');

    return CancPolicyString;
}
function GetPriceRanges(HotelRoom, Descriptions, Meals, supplier) {
    var PriceRangesString = '';
    HotelRoom = HotelRoom.HotelRoom;
    //console.log(HotelRoom)
    if (HotelRoom.length != null) {
        for (var hlh = 0; hlh < HotelRoom.length; hlh++) {
            if (HotelRoom[hlh].PriceRanges.PriceRange.length != null) {
                for (var ihppp = 0; ihppp < HotelRoom[hlh].PriceRanges.PriceRange.length; ihppp++) {
                    if (GetRoomPriceConvMark(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].Price._Gross, supplier) > 0) {
                        var PriceRangeDate = '';
                        if (HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.FromDate == HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.ToDate) {
                            //PriceRangeDate = moment(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.FromDate).format("DD MMM YYYY");
                            PriceRangeDate = HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.FromDate;
                        }
                        else {
                            //PriceRangeDate = moment(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.FromDate).format("DD MMM YYYY") + ' to ' + moment(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.ToDate).format("DD MMM YYYY");
                            PriceRangeDate = HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.FromDate + ' to ' + HotelRoom[hlh].PriceRanges.PriceRange[ihppp].DateRange.ToDate;
                        }
                        var perroomamount = parseFloat(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].Price._Gross) / parseFloat(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].Price._Nights)
                        //PriceRangesString += '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + ' (' + GetMeals(Meals) + ')</h5>' +
                        PriceRangesString += '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + '</h5>' +
                            '<div class="row"> <div class="col-lg-12"> <div class="hotel_type_area"> <div class="col-lg-4 col-md-4 col-xs-12">' + PriceRangeDate +
                            '</div> <div class="col-xs-3">@' + GetRoomPriceConvMark(perroomamount, 0, numroom, supplier) + '</div> <div class="col-xs-1">X</div> <div class="col-xs-2">' + HotelRoom[hlh].PriceRanges.PriceRange[ihppp].Price._Nights + ' NIGHTS</div> <div class="col-xs-2">' + GetRoomPriceConvMark((parseFloat(HotelRoom[hlh].PriceRanges.PriceRange[ihppp].Price._Nights) * parseFloat(perroomamount)), 0, numroom, supplier) + '</div> </div> </div> </div> ';
                    }
                }
            }
            else {
                // if (GetRoomPriceConvMark(HotelRoom[hlh].PriceRanges.PriceRange.Price._Gross) > 0) {
                var PriceRangeDate = '';
                if (HotelRoom[hlh].PriceRanges.PriceRange.DateRange.FromDate == HotelRoom[hlh].PriceRanges.PriceRange.DateRange.ToDate) {
                    //PriceRangeDate = moment(HotelRoom[hlh].PriceRanges.PriceRange.DateRange.FromDate).format("DD MMM YYYY");
                    PriceRangeDate = HotelRoom[hlh].PriceRanges.PriceRange.DateRange.FromDate;
                }
                else {
                    //PriceRangeDate = moment(HotelRoom[hlh].PriceRanges.PriceRange.DateRange.FromDate).format("DD MMM YYYY") + ' to ' + moment(HotelRoom[hlh].PriceRanges.PriceRange.DateRange.ToDate).format("DD MMM YYYY");
                    PriceRangeDate = HotelRoom[hlh].PriceRanges.PriceRange.DateRange.FromDate + ' to ' + HotelRoom[hlh].PriceRanges.PriceRange.DateRange.ToDate;
                }
                var perroomamount = parseFloat(HotelRoom[hlh].PriceRanges.PriceRange.Price._Gross) / parseFloat(HotelRoom[hlh].PriceRanges.PriceRange.Price._Nights)
                //PriceRangesString += '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + ' (' + GetMeals(Meals) + ')</h5>' +
                PriceRangesString += '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + '</h5>' +
                    '<div class="row"> <div class="col-lg-12"> <div class="hotel_type_area"> <div class="col-lg-4 col-md-4 col-xs-12">' + PriceRangeDate +
                    '</div> <div class="col-xs-3">@' + GetRoomPriceConvMark(perroomamount, 0, numroom, supplier) + '</div> <div class="col-xs-1">X</div> <div class="col-xs-2">' + HotelRoom[hlh].PriceRanges.PriceRange.Price._Nights + ' NIGHTS</div> <div class="col-xs-2">' + GetRoomPriceConvMark((parseFloat(HotelRoom[hlh].PriceRanges.PriceRange.Price._Nights) * parseFloat(perroomamount)), 0, numroom, supplier) + '</div> </div> </div> </div> ';
                //  }
            }
        }
    }
    else {
        //if (HotelRoom.PriceRanges.PriceRange.DateRange == undefined) {
        //    console.log(HotelRoom)
        //}
        if (HotelRoom.PriceRanges.PriceRange.length != null) {
            for (var ihpp = 0; ihpp < HotelRoom.PriceRanges.PriceRange.length; ihpp++) {
                var PriceRangeDate = '';
                if (HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.FromDate == HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.ToDate) {
                    //PriceRangeDate = moment(HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.FromDate).format("DD MMM YYYY");
                    PriceRangeDate = HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.FromDate;
                }
                else {
                    //PriceRangeDate = moment(HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.FromDate).format("DD MMM YYYY") + ' to ' + moment(HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.ToDate).format("DD MMM YYYY");
                    PriceRangeDate = HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.FromDate + ' to ' + HotelRoom.PriceRanges.PriceRange[ihpp].DateRange.ToDate;
                }
                var perroomamount = parseFloat(HotelRoom.PriceRanges.PriceRange[ihpp].Price._Gross) / parseFloat(HotelRoom.PriceRanges.PriceRange[ihpp].Price._Nights)
                //PriceRangesString = '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + ' (' + GetMeals(Meals) + ')</h5>' +
                PriceRangesString = '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + '</h5>' +
                    '<div class="row"> <div class="col-lg-12"> <div class="hotel_type_area"> <div class="col-lg-4 col-md-4 col-xs-12">' + PriceRangeDate +
                    '</div> <div class="col-xs-3">@' + GetRoomPriceConvMark(perroomamount, 0, numroom, supplier) + '</div> <div class="col-xs-1">X</div> <div class="col-xs-2">' + HotelRoom.PriceRanges.PriceRange[ihpp].Price._Nights + ' NIGHTS</div> <div class="col-xs-2">' + GetRoomPriceConvMark((parseFloat(HotelRoom.PriceRanges.PriceRange[ihpp].Price._Nights) * parseFloat(perroomamount)), 0, numroom, supplier) + '</div> </div> </div> </div> ';
            }
        }
        else {
            var PriceRangeDate = '';
            if (HotelRoom.PriceRanges.PriceRange.DateRange.FromDate == HotelRoom.PriceRanges.PriceRange.DateRange.ToDate) {
                //PriceRangeDate = moment(HotelRoom.PriceRanges.PriceRange.DateRange.FromDate).format("DD MMM YYYY");
                PriceRangeDate = HotelRoom.PriceRanges.PriceRange.DateRange.FromDate;
            }
            else {
                //PriceRangeDate = moment(HotelRoom.PriceRanges.PriceRange.DateRange.FromDate).format("DD MMM YYYY") + ' to ' + moment(HotelRoom.PriceRanges.PriceRange.DateRange.ToDate).format("DD MMM YYYY");
                PriceRangeDate = HotelRoom.PriceRanges.PriceRange.DateRange.FromDate + ' to ' + HotelRoom.PriceRanges.PriceRange.DateRange.ToDate;
            }
            var perroomamount = parseFloat(HotelRoom.PriceRanges.PriceRange.Price._Gross) / parseFloat(HotelRoom.PriceRanges.PriceRange.Price._Nights)
            //PriceRangesString = '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + ' (' + GetMeals(Meals) + ')</h5>' +
            PriceRangesString = '<h5><i class="fa fa-bed" aria-hidden="true"></i> Room Type : ' + Descriptions + '</h5>' +
                '<div class="row"> <div class="col-lg-12"> <div class="hotel_type_area"> <div class="col-lg-4 col-md-4 col-xs-12">' + PriceRangeDate +
                '</div> <div class="col-xs-3">@' + GetRoomPriceConvMark(perroomamount, 0, numroom, supplier) + '</div> <div class="col-xs-1">X</div> <div class="col-xs-2">' + HotelRoom.PriceRanges.PriceRange.Price._Nights + ' NIGHTS</div> <div class="col-xs-2">' + GetRoomPriceConvMark((parseFloat(HotelRoom.PriceRanges.PriceRange.Price._Nights) * parseFloat(perroomamount)), 0, numroom, supplier) + '</div> </div> </div> </div> ';
        }
    }
    return PriceRangesString;
}
function GetEssentialInfo(EssentialInformation) {
    var Essstring = '';
    if (EssentialInformation != null) {
        Essstring = '<div class="modal-body sp_bg_hotel"><h3>Essential Informations</h3>';
        if (EssentialInformation.length != null) {
            for (var ied = 0; ied < EssentialInformation.length; ied++) {
                Essstring += '<h4>' + EssentialInformation[ied]._EssentialInfo + '</h4>';
            }
        }
        else {
            Essstring += '<h4>' + EssentialInformation._EssentialInfo + '</h4>';
        }
        Essstring += '</div>';
    }
    return Essstring;
}
function GetOffers(Offer) {
    var Offerstring = '';
    if (Offer != null) {
        if (Offer.__cdata != null) {
            Offerstring = '<i data-toggle="tooltip" data-placement="top" title="' + Offer.__cdata + '" class="fa fa-tag" aria-hidden="true"></i>';
        }
    }
    return Offerstring;
}
function Getextrabeddetails(Extrabed) {
    var Extrabedtring = '';
    if (Extrabed != null) {
        if (Extrabed != null) {
            if (Extrabed == "True") {
                Extrabedtring = "Extra bed is not provided for children";
            }
            else if (Extrabed == "False") {
                Extrabedtring = "Extra bed is provided for children";
            }
        }
    }
    return Extrabedtring;
}
function GetDiscountAmount(IncludedOfferDiscount, ItemPrice, supplier) {
    var IncludedOfferDiscountstring = '';
    if (IncludedOfferDiscount != null) {
        IncludedOfferDiscountstring = '<span style="float: left;">Discount Amount : ' + CurrencyStat + ' ' + GetRoomPriceConvMark(IncludedOfferDiscount, 0, 1, supplier) + '</span>' +
            '<span style="float: right;">Total Cost : ' + CurrencyStat + ' ' + GetRoomPriceConvMark(ItemPrice, 0, 1, supplier) + '</span>';
    }
    else {
        IncludedOfferDiscountstring = '<span>Total Cost : ' + CurrencyStat + ' ' + GetRoomPriceConvMark(ItemPrice, 0, 1, supplier) + '</span>';
    }
    return IncludedOfferDiscountstring;
}
function Showsearchsummary() {
    var Cityfull = window.localStorage.getItem("cityname");
    if (Cityfull != null) {
        $('#sumcity').html('<i class="fa fa-map-marker" aria-hidden="true"></i> ' + Cityfull);
        document.title = Setup.Agency.AgencyShortName + ' - Hotels in ' + Cityfull;
    }
    else {
        $('#sumcity').html(city);
        document.title = Setup.Agency.AgencyShortName + ' - Hotels in ' + city;
    }
    $('#sumcin').html(moment(GetParameterValues('cin'), "DD/MM/YYYY").format("DD MMM YY"));
    $('#sumcout').html(moment(GetParameterValues('cout'), "DD/MM/YYYY").format("DD MMM YY"));
    //$('#sumn8s').html(night);
    //$('#sumnro').html(numroom);
    $('#sumpax').html('<span>' + window.localStorage.getItem("totpax") + '</span> Pax in <span>' + numroom + '</span> room(s)');
    $("#loadingdata h5").append(Setup.Agency.AgencyShortName + ' - Hotels in ' + window.localStorage.getItem("cityname") + ' <span>Checkin ' + moment(GetParameterValues('cin'), "DD/MM/YYYY").format("DD MMM'YY") + " Checkout " + moment(GetParameterValues('cout'), "DD/MM/YYYY").format("DD MMM'YY") + " " + window.localStorage.getItem("totpax") + '</span> Pax in <span>' + numroom + '</span> room(s)');

}


//------------------HotelAmenites------------------------\\
function GenerateHotelAmenitiesFilter() {
    ajaxHelper(GenerateHotelAmenities, null, null, { url: HotelAmenitiesURL, data: null });
}
function GenerateHotelAmenities(data) {
    if (data != "NOINFO") {

        var AllHotelAmenitiArray = data;
        var UlLocations = '';
        for (var it7 = 0; it7 < AllHotelAmenitiArray.length; it7++) {
            if (UlLocations.indexOf("HotAm_" + AllHotelAmenitiArray[it7].code + '"') == -1) {
                if (AllHotelAmenitiArray[it7].code != "") {
                    var tolnohotelsbyAmeniti = $("#itemContainer .hotel_view_area").filter(function () { return $(this).attr('d-Ham').indexOf(AllHotelAmenitiArray[it7].code) != -1; }).length;
                    UlLocations += '<div class="checkBox_area"> <input name="HotelAmenities" class="HAmbox" type="checkbox" value="" checked="" id="HotAm_' + AllHotelAmenitiArray[it7].code + '"> <label for="web" class="formsfont">' + AllHotelAmenitiArray[it7].name + '</label> <span>Only</span></div>';
                }
                else {
                    UlLocations += '<div class="checkBox_area"> <input name="HotelAmenities" class="HAmbox" type="checkbox" value="" checked="" id="HotAm_' + AllHotelAmenitiArray[it7].code + '"> <label for="web" class="formsfont">Other</label> <span>Only</span></div>';
                }
            }
        }
        $("#ulHaminitiFilter").html(UlLocations);
        $("#ulHaminitiFilter").mCustomScrollbar({
            autoHideScrollbar: true,
            theme: "rounded"
        });
        finalizeonlyshowclick();

        //Kelvin Abella - Quotations 6/24/2018
        $(".HAmbox").unbind().click(function (e) {
            //$(".HAmbox").click(function (e) {
            var tempHamcode = $(this).attr("id").split("_")[1];
            var checkedstatus = 0;
            if (document.getElementById($(this).attr("id")).checked == true) {
                checkedstatus = 1;
            }
            $("#itemContainer .hotel_view_area").each(function () {
                var DataLoc = $(this).attr('d-Ham');
                var DataShowValue = parseInt($(this).attr("data-show"));
                var DataShowValueNew = parseInt(0);
                if (DataLoc.indexOf(tempHamcode) != -1) {
                    var datalocarray = DataLoc.split(",");
                    var tempstatus = 0;
                    for (var i645 = 0; i645 < datalocarray.length; i645++) {
                        var templocval = datalocarray[i645];
                        if (templocval != tempHamcode) {
                            if (document.getElementById("HotAm_" + templocval).checked == true) {
                                tempstatus = 1;
                            }
                        }
                    }

                    if (checkedstatus == 1) {
                        if (tempstatus == 0) {
                            if (DataShowValue == 0) { DataShowValueNew = 0; }
                            else { DataShowValueNew = DataShowValue - 1; }
                        }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    else {
                        if (tempstatus == 0) { DataShowValueNew = DataShowValue + 1; }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    $(this).attr("data-show", DataShowValueNew);
                }
            });

            ShowLIBlocks();
        });

    }
}

function GetFullHotelAmenitiFilter(HotelAmenitiDetails) {
    var Ameniti = '';
    if (HotelAmenitiDetails.length != null) {
        for (var iff = 0; iff < HotelAmenitiDetails.length; iff++) {
            if (Ameniti == '') {
                Ameniti += HotelAmenitiDetails[iff]._Code;
            }
            else {
                Ameniti += ',' + HotelAmenitiDetails[iff]._Code;
            }
            //AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location[iff]._Code, "locname": HotelAmenitiDetails.Location[iff].__cdata });
        }
    }
    else {
        Ameniti = HotelAmenitiDetails[0]._Code;
        //  AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location._Code, "locname": HotelAmenitiDetails.Location.__cdata });
    }
    return Ameniti;
}
//------------------HotelAmenites------------------------\\
//------------------RoomAmenites------------------------\\
function GenerateRoomAmenitiesFilter() {

    ajaxHelper(GenerateRoomAmenities, null, null, { url: HotelRoomAmenitiesURL, data: null });
}
function GenerateRoomAmenities(data) {
    if (data != "NOINFO") {

        var AllRoomAmenitiArray = data;
        var UlLocations = '';
        for (var it7 = 0; it7 < AllRoomAmenitiArray.length; it7++) {
            if (UlLocations.indexOf("RoomAm_" + AllRoomAmenitiArray[it7].code + '"') == -1) {
                if (AllRoomAmenitiArray[it7].code != "") {
                    //  var tolnohotelsbyAmeniti = $("#itemContainer .hotel_view_area").filter(function () { return $(this).attr('d-Ham').indexOf(AllHotelAmenitiArray[it7].code) != -1; }).length;
                    UlLocations += '<div class="checkBox_area"> <input name="RoomAmenities" class="RAmbox" type="checkbox" value="" checked="" id="RoomAm_' + AllRoomAmenitiArray[it7].code + '"> <label for="web" class="formsfont">' + AllRoomAmenitiArray[it7].name + '</label> <span>Only</span></div>';
                }
                else {
                    UlLocations += '<div class="checkBox_area"> <input name="RoomAmenities" class="RAmbox" type="checkbox" value="" checked="" id="RoomAm_' + AllRoomAmenitiArray[it7].code + '"> <label for="web" class="formsfont">Other</label> <span>Only</span></div>';
                }
            }
        }
        $("#ulHRoomaminitiFilter").html(UlLocations);
        $("#ulHRoomaminitiFilter").mCustomScrollbar({
            autoHideScrollbar: true,
            theme: "rounded"
        });
        finalizeonlyshowclick();
        //Kelvin Abella - Quotations 6/24/2018
        $(".RAmbox").unbind().click(function () {
            //$(".RAmbox").click(function () {
            var tempHamcode = $(this).attr("id").split("_")[1];
            var checkedstatus = 0;
            if (document.getElementById($(this).attr("id")).checked == true) {
                checkedstatus = 1;
            }
            $("#itemContainer .hotel_view_area").each(function () {
                var DataLoc = $(this).attr('d-Ram');
                var DataShowValue = parseInt($(this).attr("data-show"));
                var DataShowValueNew = parseInt(0);
                if (DataLoc.indexOf(tempHamcode) != -1) {
                    var datalocarray = DataLoc.split(",");
                    var tempstatus = 0;
                    for (var i645 = 0; i645 < datalocarray.length; i645++) {
                        var templocval = datalocarray[i645];
                        if (templocval != tempHamcode) {
                            if (document.getElementById("RoomAm_" + templocval).checked == true) {
                                tempstatus = 1;
                            }
                        }
                    }

                    if (checkedstatus == 1) {
                        if (tempstatus == 0) {
                            if (DataShowValue == 0) { DataShowValueNew = 0; }
                            else { DataShowValueNew = DataShowValue - 1; }
                        }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    else {
                        if (tempstatus == 0) { DataShowValueNew = DataShowValue + 1; }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    $(this).attr("data-show", DataShowValueNew);
                }
            });

            ShowLIBlocks();
        });

    }
}
function GetFullRoomAmenitiFilter(RoomAmenitiDetails) {
    var Ameniti = '';
    if (RoomAmenitiDetails.length != null) {
        for (var iff = 0; iff < RoomAmenitiDetails.length; iff++) {
            if (Ameniti == '') {
                Ameniti += RoomAmenitiDetails[iff]._Code;
            }
            else {
                Ameniti += ',' + RoomAmenitiDetails[iff]._Code;
            }
            //AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location[iff]._Code, "locname": HotelAmenitiDetails.Location[iff].__cdata });
        }
    }
    else {
        Ameniti = RoomAmenitiDetails[0]._Code;
        //  AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location._Code, "locname": HotelAmenitiDetails.Location.__cdata });
    }
    return Ameniti;
}
//------------------RoomAmenites------------------------\\
//}

//------------------POIFilter------------------------\\
function GeneratePOIFilter() {

    ajaxHelper(GeneratePOI, null, null, { url: HotelPOIFilterURL, data: null });
}
function GeneratePOI(data) {
    if (data != "NOINFO") {

        var x = document.getElementById("selectPOIFilter");
        x.innerHTML = '';
        var option = document.createElement('option');
        option.value = 'none';
        option.innerHTML = 'none';
        x.appendChild(option);
        var AllHotelAmenitiArray = data;
        var UlLocations = '';
        for (var it7 = 0; it7 < AllHotelAmenitiArray.length; it7++) {
            if (UlLocations.indexOf("HotAm_" + AllHotelAmenitiArray[it7].code + '"') == -1) {
                if (AllHotelAmenitiArray[it7].text != "") {
                    var opt = document.createElement('option');
                    opt.value = AllHotelAmenitiArray[it7].text;
                    opt.innerHTML = AllHotelAmenitiArray[it7].text;
                    x.appendChild(opt);

                    //var option = document.createElement(AllHotelAmenitiArray[it7].code);
                    //option.text = AllHotelAmenitiArray[it7].text;
                    //x.add(option);
                }
                else {
                    UlLocations += '<div class="checkBox_area"> <input name="HotelAmenities" class="HAmbox" type="checkbox" value="" checked="" id="HotAm_' + AllHotelAmenitiArray[it7].code + '"> <label for="web" class="formsfont">Other</label> <span>Only</span></div>';
                }
            }
        }
        //   $("#ulHaminitiFilter").html(UlLocations);
        //    $("#ulHaminitiFilter").mCustomScrollbar({
        //        autoHideScrollbar: true,
        //        theme: "rounded"
        //    });
        finalizeonlyshowclick();
        //Kelvin Abella - Quotations 6/24/2018
        $(".HAmbox").unbind().click(function () {
            //$(".HAmbox").click(function () {
            var tempHamcode = $(this).attr("id").split("_")[1];
            var checkedstatus = 0;
            if (document.getElementById($(this).attr("id")).checked == true) {
                checkedstatus = 1;
            }
            $("#itemContainer .hotel_view_area").each(function () {
                var DataLoc = $(this).attr('d-Ham');
                var DataShowValue = parseInt($(this).attr("data-show"));
                var DataShowValueNew = parseInt(0);
                if (DataLoc.indexOf(tempHamcode) != -1) {
                    var datalocarray = DataLoc.split(",");
                    var tempstatus = 0;
                    for (var i645 = 0; i645 < datalocarray.length; i645++) {
                        var templocval = datalocarray[i645];
                        if (templocval != tempHamcode) {
                            if (document.getElementById("HotAm_" + templocval).checked == true) {
                                tempstatus = 1;
                            }
                        }
                    }

                    if (checkedstatus == 1) {
                        if (tempstatus == 0) {
                            if (DataShowValue == 0) { DataShowValueNew = 0; }
                            else { DataShowValueNew = DataShowValue - 1; }
                        }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    else {
                        if (tempstatus == 0) { DataShowValueNew = DataShowValue + 1; }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    $(this).attr("data-show", DataShowValueNew);
                }
            });

            ShowLIBlocks();
        });
        $("#selectPOIFilter").click(function () {
            var tempHamcode = $(this).attr("id").split("_")[1];
            var checkedstatus = 0;
            if (document.getElementById($(this).attr("id")).checked == true) {
                checkedstatus = 1;
            }
            $("#itemContainer .hotel_view_area").each(function () {
                var DataLoc = $(this).attr('d-poi');
                var DataShowValue = parseInt($(this).attr("data-show"));
                var DataShowValueNew = parseInt(0);
                if (DataLoc.indexOf(tempHamcode) != -1) {
                    var datalocarray = DataLoc.split(",");
                    var tempstatus = 0;
                    for (var i645 = 0; i645 < datalocarray.length; i645++) {
                        var templocval = datalocarray[i645];
                        if (templocval != tempHamcode) {
                            if (document.getElementById("RoomAm_" + templocval).checked == true) {
                                tempstatus = 1;
                            }
                        }
                    }

                    if (checkedstatus == 1) {
                        if (tempstatus == 0) {
                            if (DataShowValue == 0) { DataShowValueNew = 0; }
                            else { DataShowValueNew = DataShowValue - 1; }
                        }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    else {
                        if (tempstatus == 0) { DataShowValueNew = DataShowValue + 1; }
                        else { DataShowValueNew = DataShowValue; }
                    }
                    $(this).attr("data-show", DataShowValueNew);
                }
            });

            ShowLIBlocks();
        });

    }
}
function GetFullPOIFilter(RoomAmenitiDetails) {
    var Ameniti = '';
    if (RoomAmenitiDetails != null && RoomAmenitiDetails.length != null) {
        for (var iff = 0; iff < RoomAmenitiDetails.length; iff++) {
            if (Ameniti == '') {
                Ameniti += RoomAmenitiDetails[iff]._text;
            }
            else {
                Ameniti += ',' + RoomAmenitiDetails[iff]._text;
            }
            //AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location[iff]._Code, "locname": HotelAmenitiDetails.Location[iff].__cdata });
        }
    }
    else {
        //  Ameniti = RoomAmenitiDetails[0].test;
        //  AllLocationCodeArray.push({ "locode": HotelAmenitiDetails.Location._Code, "locname": HotelAmenitiDetails.Location.__cdata });
    }
    return Ameniti;
}

//------------------POIFilter------------------------\\


//function ShowHideUnderXLRates() {
//	var numroom = GetParameterValues('numroom');
//	$(".hotel_view_area").each(function () {
//		var HotelIndex = $(this).attr("id").replace("hotel_", "");

//		var datashowchangestatus = parseInt(0);
//		$("#hotel_" + HotelIndex + " .hotel_room_details01").each(function () {
//			var SelRoomID = $(this).attr("id");
//			var PopID = SelRoomID.replace('r', 'cmod');
//			if ($('#' + PopID + ' .modal-dialog .modal-content .sp_bg_hotel h3').attr("xlcharge") == 'true') {
//				var datashow = parseInt($(this).attr("data-show"));
//				if (document.getElementById("WithXLCharges").checked == true) { if (datashow > 0) { datashowchangestatus = 1; $(this).attr("data-show", (datashow - parseInt(1))); } }
//				else { datashowchangestatus = 1; $(this).attr("data-show", (datashow + parseInt(1))); }
//			}
//		});

//		if (datashowchangestatus == 1) {
//			rearrangehotelinnerrates(numroom, HotelIndex);
//		}


//	});

//}
//function ShowHideNotUnderXLRates() {
//	var numroom = GetParameterValues('numroom');
//	$(".hotel_view_area").each(function () {
//		var HotelIndex = $(this).attr("id").replace("hotel_", "");

//		var datashowchangestatus = parseInt(0);
//		$("#hotel_" + HotelIndex + " .hotel_room_details01").each(function () {
//			var SelRoomID = $(this).attr("id");
//			var PopID = SelRoomID.replace('r', 'cmod');
//			if ($('#' + PopID + ' .modal-dialog .modal-content .sp_bg_hotel h3').attr("xlcharge") == 'false') {
//				var datashow = parseInt($(this).attr("data-show"));
//				if (document.getElementById("WithoutXLCharges").checked == true) { if (datashow > 0) { datashowchangestatus = 1; $(this).attr("data-show", (datashow - parseInt(1))); } }
//				else { datashowchangestatus = 1; $(this).attr("data-show", (datashow + parseInt(1))); }
//			}
//		});
//		if (datashowchangestatus == 1) {
//			rearrangehotelinnerrates(numroom, HotelIndex);
//		}

//	});
//}

function rearrangehotelinnerrates(numroom, HotelIndex) {

    //var temphotelshowstatus = parseInt(1);
    for (var tempi = 0; tempi < numroom; tempi++) {
        //console.log($("div[id^='r_" + HotelIndex + "_" + tempi + "']").filter(function () { return $(this).attr('data-show') == '0'; }).length);
        //console.log($("#hotel_" + HotelIndex + " div.hotel_room_details01"));
        var temproomcatcount = $("#hotel_" + HotelIndex + " div.hotel_room_details01").filter(function () {
            return this.getAttribute('data-show') == '0';
        }).length;

        var temphotelshowstatus = parseInt(temproomcatcount) == 0 ? 0 : 1

        //if (parseInt(temproomcatcount) == 0) {
        //    temphotelshowstatus = 0;
        //}
        //else {
        //    temphotelshowstatus = 1;
        //}
    }

    var DataShowValue = parseInt($("#hotel_" + HotelIndex).attr("data-show"));

    var DataShowValueNew = temphotelshowstatus == 1 ?
        DataShowValue == 0 ? 0 : DataShowValue - 1 :
        DataShowValue + 1;

    //if (temphotelshowstatus == 1) {
    //    DataShowValueNew = DataShowValue == 0 ? 0 : DataShowValue - 1;
    //    if (DataShowValue == 0) {
    //        DataShowValueNew = 0;
    //    }
    //    else {
    //        DataShowValueNew = DataShowValue - 1;
    //    }
    //}
    //else {
    //    DataShowValueNew = DataShowValue + 1;
    //}

    $("#hotel_" + HotelIndex).attr("data-show", DataShowValueNew);

    $("#hotel_" + HotelIndex).find(".hotel_room_details01").each(function () {
        var $this = $(this);
        var DataShowValue = $this.attr("data-show");
        //var ID = $this.attr("id");
        if (DataShowValue == 0) {
            $this.show();
        }
        else {
            $this.hide();
        }
    });


    // $("div[id^='r_" + HotelIndex + "_" + 0 + "']").filter(function () { return $(this).attr('data-show') == '0'; }).first().find("div.book_button a").click();

    for (var tempi = 0; tempi < numroom; tempi++) {

        var button = $("#hotel_" + HotelIndex + " div.hotel_room_details01").filter(function () {
            return $(this).attr('data-show') == '0';
        }).first().find("div.book_button a");

        var SuplierCode = $("#hotel_" + HotelIndex + " div.hotel_room_details01").filter(function () {
            return $(this).attr('data-show') == '0';
        }).parent().parent().parent().find("div.hotel_dis").find("b").text();

        var buttonAttr = button.attr("sel-stat");

        if (typeof buttonAttr !== "undefined") {
            if (DataShowValueNew == 0) {
                if (buttonAttr == true || buttonAttr == "true") {
                } else {
                    SelectRoom(button, SuplierCode);
                }
            }
        }
    }

}

function GetStaticData(SupplierCode, CityCode, sid, HotelCode, HotelIndex, thiss, roomfullcode) {
    window.localStorage.setItem("grncsearchid", sid);
    var ssid = '';
    var numroom = GetParameterValues('numroom');
    var roomcategoryid = GetParameterValues('roomcatid');
    var dopen = $(thiss).attr("data-open");
    if (dopen == 0) {
        $('#HotPlus_' + HotelIndex + ' i').addClass('fa-spin');
        if (SupplierCode == 'R24') {
            ssid = SearchRefId;
        }
        else {
            ssid = sid;
        }
        // if (datas.d == 'NODATA') {//Get Hotel Data from GTA
        var qstrings = {
            CityCode: CityCode,
            HotelCode: HotelCode,
            RateKey: ssid,
            SupplierCode: SupplierCode,
            Curr: "USD",
            Lang: "EN",
            cin: cin,
            cout: cout,
            Rooms: rooms,
            numroom: numroom,
            AgencyCode: HotelAgencyCode,
            token: accesstocken
        }

        var errorFunc = function (response) {
            //  alertify.alert('Session', 'Session expired please login to continue.', function () {
            //       window.location.href = "/index.html";
            // });
            alertify.alert('Error', 'Sorry We found some issue with this room selection...', function () {
                //       window.location.href = "/index.html";
                $('#HotPlus_' + HotelIndex + ' i').removeClass('fa-spin');
            });
            //alert('Server Busy!!Please try later.');
        };

        function GetHotelInfoSuccess(datag) {


            var x2js = new X2JS();
            //  var jsonDataHotelGta = x2js.xml_str2json(datag.d);
            var jsonDataHotelGta = JSON.parse(datag);
            GenerateHotelInfo(jsonDataHotelGta);
        }

        ajaxHelper(GetHotelInfoSuccess, null, errorFunc, { url: HotelDetailURL, data: qstrings });

        // }
        //else {
        //    var x2js = new X2JS();
        //    var jsonStatDataHotel = x2js.xml_str2json(datas.d);
        //    GenerateHotelInfo(jsonStatDataHotel);
        //}
        function GenerateHotelInfo(jsonStatDataHotel) {
            //console.log(jsonStatDataHotel);

            var AddressLines = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.AddressLines;
            if (AddressLines != null) {
                var AddressString = '';

                if (AddressLines.AddressLine1.__cdata && AddressLines.AddressLine1.__cdata != "") {
                    AddressString += AddressLines.AddressLine1.__cdata + ",";
                }
                if (AddressLines.AddressLine2.__cdata && AddressLines.AddressLine2.__cdata != "") {
                    AddressString += AddressLines.AddressLine2.__cdata + ",";
                }
                if (AddressLines.AddressLine3.__cdata && AddressLines.AddressLine3.__cdata != "") {
                    AddressString += AddressLines.AddressLine3.__cdata + ",";
                }
                if (AddressLines.AddressLine4.__cdata && AddressLines.AddressLine4.__cdata != "") {
                    AddressString += AddressLines.AddressLine4.__cdata + ".";
                }
                //if (AddressLines.AddressLine5.__cdata && AddressLines.AddressLine5.__cdata != "") {
                //    AddressString += AddressLines.AddressLine5.__cdata + ".";
                //}

                $("#hotAddress_" + HotelIndex).html('<b>Address</b><br> ' + AddressString.substr(0, AddressString.length - 1) + '.');
            }
            if (AddressLines.Telephone.__cdata && AddressLines.Telephone.__cdata != "") {
                $("#hotTelephone_" + HotelIndex).html('Ph: ' + AddressLines.Telephone.__cdata);
            }
            if (AddressLines.Fax.__cdata && AddressLines.Fax.__cdata != "") {
                $("#hotFax_" + HotelIndex).html('Fax: ' + AddressLines.Fax.__cdata);
            }
            if (AddressLines.EmailAddress.__cdata && AddressLines.EmailAddress.__cdata != "") {
                $("#hotEmail_" + HotelIndex).html('Email: ' + AddressLines.EmailAddress.__cdata);
            }
            if (AddressLines.WebSite.__cdata && AddressLines.WebSite.__cdata != "") {
                var website = AddressLines.WebSite.__cdata;
                $("#hotWeb_" + HotelIndex).html('Website: ' + website.replace('http://', ''));
            }
            var Category = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Category.__cdata;
            //jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Category.__cdata
            if (Category != null) {
                $("#hotcat_" + HotelIndex).html('<b>' + Category + '</b>');
            }
            else {
                $("#hotcat_" + HotelIndex).html("");
            }

            var AreaDetails = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.AreaDetails;
            if (AreaDetails != "") {
                var AreaDetailsString = "<h3>Point of Interests</h3>";
                if (_.isArray(AreaDetails.AreaDetail) != false) {
                    for (var i3 = 0; i3 < AreaDetails.AreaDetail.length; i3++) {
                        AreaDetailsString += "<h4>" + AreaDetails.AreaDetail[i3].__cdata + "</h4>";
                    }
                }
                else {
                    AreaDetailsString += "<h4>" + AreaDetails.AreaDetail + "</h4>";
                }
                $("#hotAreaDet_" + HotelIndex).html(AreaDetailsString);
            }

            //Room Facilities
            var RoomFacilitiesStringHorizontal = "";
            var RoomFacilities = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.RoomFacilities;
            if (RoomFacilities != null) {
                var RoomFacilitiesString = "";
                for (var i4 = 0; i4 < RoomFacilities.Facility.length; i4++) {
                    if (RoomFacilities.Facility[i4]._Code != "") {
                        RoomFacilitiesString += '<li><i class="' + GetFaClass(RoomFacilities.Facility[i4]._Code) + '" aria-hidden="true"></i>' + RoomFacilities.Facility[i4].__cdata + '</li>';
                        RoomFacilitiesStringHorizontal += '<i data-toggle="tooltip" data-placement="top" data-original-title="' + RoomFacilities.Facility[i4].__cdata + '" class="' + GetFaClass(RoomFacilities.Facility[i4]._Code) + '" aria-hidden="true"></i>';
                    }
                }

                var limitFacilitiesRm = $.parseHTML(RoomFacilitiesString);
                var hotRoomAmenitiesId = $("#hotRoomAmenities_" + HotelIndex);
                hotRoomAmenitiesId.html('<h4><i class="fa fa-key" aria-hidden="true"></i>Room Amenities</h4><ul></ul>');
                if (RoomFacilitiesString) {
                    if (limitFacilitiesRm.length > 15) {
                        hotRoomAmenitiesId.find('ul').html(limitFacilitiesRm.slice(0, 15));
                        hotRoomAmenitiesId.append("<a id='show-more-button-rm" + HotelIndex + "' data-show-more-rm" + HotelIndex + " = '0' style = 'cursor:pointer;font-family:\"Open Sans\", sans-serif;font-size:13px;'>Show more</a>");

                    } else {
                        hotRoomAmenitiesId.find('ul').html(limitFacilitiesRm);
                    }

                    $("#show-more-button-rm" + HotelIndex).on("click", function () {
                        var $this = $(this);
                        if ($this.attr('data-show-more-rm' + HotelIndex) == 0) {
                            hotRoomAmenitiesId.find('ul').html(limitFacilitiesRm);
                            $this.attr('data-show-more-rm' + HotelIndex, 1);
                            $this.text('Show less');
                        } else if ($this.attr('data-show-more-rm' + HotelIndex) == 1) {
                            hotRoomAmenitiesId.find('ul').html(limitFacilitiesRm.slice(0, 15));
                            $this.attr('data-show-more-rm' + HotelIndex, 0);
                            $this.text('Show more');
                        }
                    });
                } else {
                    hotRoomAmenitiesId.find('ul').html(RoomFacilitiesString);
                }

            }
            var Facilities = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Facilities;
            if (Facilities != null) {
                var FacilitiesString = "";
                var HFacHomeItems = '*EC,*HP,*CP,*GY,*TE,*GF,*BS,*DF,*PT,*OP,*SA';
                if (Facilities.Facility.length != undefined) {
                    for (var i4 = 0; i4 < Facilities.Facility.length; i4++) {
                        FacilitiesString += '<li><i class="' + GetFaClass(Facilities.Facility[i4]._Code) + '" aria-hidden="true"></i>' + Facilities.Facility[i4].__cdata + '</li>';

                        if (HFacHomeItems.indexOf(Facilities.Facility[i4]._Code) != -1) {
                            RoomFacilitiesStringHorizontal += '<i data-toggle="tooltip" data-placement="top" data-original-title="' + Facilities.Facility[i4].__cdata + '" class="' + GetFaClass(Facilities.Facility[i4]._Code) + '" aria-hidden="true"></i>';
                        }
                    }
                }
                else {
                    FacilitiesString += '<li><i class="' + GetFaClass(Facilities.Facility._Code) + '" aria-hidden="true"></i>' + Facilities.Facility.__cdata + '</li>';

                    if (HFacHomeItems.indexOf(Facilities.Facility._Code) != -1) {
                        RoomFacilitiesStringHorizontal += '<i data-toggle="tooltip" data-placement="top" data-original-title="' + Facilities.Facility.__cdata + '" class="' + GetFaClass(Facilities.Facility._Code) + '" aria-hidden="true"></i>';
                    }
                }
                var limitFacilitiesHot = $.parseHTML(FacilitiesString);
                var hotAmenitiesId = $("#hotAmenities_" + HotelIndex);
                if (limitFacilitiesHot) {
                    if (limitFacilitiesHot.length > 15) {
                        hotAmenitiesId.html(limitFacilitiesHot.slice(0, 15));
                        hotAmenitiesId.parent().append("<a id='show-more-button-hot" + HotelIndex + "' data-show-more-hot" + HotelIndex + "='0' style='cursor:pointer;font-family:\"Open Sans\", sans-serif;font-size:13px;'>Show more</a>");
                    } else {
                        hotAmenitiesId.html(limitFacilitiesHot);
                    }

                    $("#show-more-button-hot" + HotelIndex).on("click", function () {
                        var $this = $(this);
                        if ($this.attr('data-show-more-hot' + HotelIndex) == 0) {
                            hotAmenitiesId.html(limitFacilitiesHot);
                            $this.attr('data-show-more-hot' + HotelIndex, 1);
                            $this.text('Show less');
                        } else if ($this.attr('data-show-more-hot' + HotelIndex) == 1) {
                            hotAmenitiesId.html(limitFacilitiesHot.slice(0, 15));
                            $this.attr('data-show-more-hot' + HotelIndex, 0);
                            $this.text('Show more');
                        }
                    })
                } else {
                    hotAmenitiesId.html(limitFacilitiesHot);
                }

            }
            if (RoomFacilitiesStringHorizontal != "") {
                RoomFacilitiesStringHorizontal = $.parseHTML(RoomFacilitiesStringHorizontal).slice(0, 10);
            }
            $("#hotRoomAmenitiesHori_" + HotelIndex).html(RoomFacilitiesStringHorizontal);
            $('[data-toggle="tooltip"]').tooltip();
            var MapLinkString = "";

            var HotNamee = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.Item.__cdata + ' ' + jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.City.__cdata;
            HotNamee = HotNamee.replace('&', '%26');
            MapLinkString = '<iframe src="https://www.google.com/maps/embed/v1/place?q=' + HotNamee + '&amp;zoom=10&amp;key=' + GMapKey + '" frameborder="0" style="border:0; width:100%;height:200px;" allowfullscreen></iframe>';
            $("#HotMiniMap_" + HotelIndex).html(MapLinkString);
            MapLinkString = '<iframe src="https://www.google.com/maps/embed/v1/place?q=' + HotNamee + '&amp;zoom=17&amp;key=' + GMapKey + '" style="width:100%; height:417px; border:0;" frameborder="0"></iframe>';
            $("#HotLargeMap_" + HotelIndex).html(MapLinkString);


            var Reports = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Reports;
            if (Reports != "") {
                var ReportsString = "";
                var hotelgeninfo = "";
                var hotellocinfo = "";
                for (var i5 = 0; i5 < Reports.Report.length; i5++) {
                    ReportsString += "<h3>" + Reports.Report[i5]._Type + "</h3><p>" + Reports.Report[i5].__cdata + "</p>";
                    if (Reports.Report[i5]._Type == 'general') {
                        hotelgeninfo = Reports.Report[i5].__cdata;
                    }
                    else if (Reports.Report[i5]._Type == 'general' && Reports.Report[i5]._Type == 'location') {
                        hotellocinfo = Reports.Report[i5].__cdata;
                    }
                }
                $("#hotReports_" + HotelIndex).html(ReportsString);
            }

            var shortText = "";
            var longText = "";
            var textDots = "";
            var showMore = "";
            var textLenBool = "";
            var noReportsBool = "";

            if (hotelgeninfo != undefined && hotelgeninfo != "") {
                textLenBool = hotelgeninfo.length < 200 ? true : false;
                shortText = "<div><span class='short-text'>" + hotelgeninfo.substring(0, 200) + "</span>";
                longText = "<span class='long-text' style='display: none;'>" + hotelgeninfo.substring(201, hotelgeninfo.length) + "</span>";
            }
            else if (hotellocinfo != undefined && hotellocinfo != "") {
                textLenBool = hotellocinfo.length < 200 ? true : false;
                shortText = "<div><span class='short-text'>" + hotellocinfo.substring(0, 200) + "</span>";
                longText = "<span class='long-text' style='display: none;'>" +
                    hotellocinfo.substring(201, hotellocinfo.length) +
                    "</span>";

            } else {
                noReportsBool = true;
            }

            textDots = "<span class='text-dots'>.. </span>";
            showMore = "<a class='read-more-button' data-more-" + HotelIndex + "='0' style='cursor:pointer'>Read more</a></div>";
            if ((textLenBool || textLenBool == "true") || (noReportsBool || noReportsBool == "true")) {
                $("#hotgen_" + HotelIndex).html(shortText);

            }
            else {
                $("#hotgen_" + HotelIndex).html(shortText + longText + textDots + showMore);

                $(".read-more-button").on('click', function () {
                    var $this = $(this)
                    if ($this.attr('data-more-' + HotelIndex) == 0) {
                        $this.attr('data-more-' + HotelIndex, 1);
                        $this.text(' Read less');

                        $this.prev().css('display', 'none');
                        $this.prev().prev().css('display', 'inline');
                    }
                    else if ($this.attr('data-more-' + HotelIndex) == 1) {
                        $this.attr('data-more-' + HotelIndex, 0);
                        $this.text(' Read more');

                        $this.prev().css('display', 'inline');
                        $this.prev().prev().css('display', 'none');
                    }
                });
            }

            var ImageLinksString = "";
            var MainThumbImageLinksString = "";
            if (jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Links != null) {
                var ImageLinks = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.Links.ImageLinks;

                var imgSrcLink = $("#mainthumb_" + HotelIndex).find("img").attr("src");
                if (ImageLinks.ImageLink.length != null) {
                    if (imgSrcLink == "hotel/images/noimage.gif") {
                        MainThumbImageLinksString = '<img src="' + ImageLinks.ImageLink[0].Image.__cdata + '" onerror="imgError(this);" alt="' + ImageLinks.ImageLink[0].Text.__cdata + '">';
                        $("#mainthumb_" + HotelIndex).html(MainThumbImageLinksString);
                    }
                    for (var i6 = 0; i6 < ImageLinks.ImageLink.length; i6++) {
                        ImageLinksString += '<div class="col-sm-3 padding_sm"><a class="fancybox" data-fancybox-group="gallery" title="(Provided by VFM Leonardo Inc.)" href="' + ImageLinks.ImageLink[i6].Image.__cdata + '"><img src="' + ImageLinks.ImageLink[i6].Image.__cdata + '" onerror="imgError(this);" alt="' + ImageLinks.ImageLink[i6].Text.__cdata + '" style="width:100%;height:200px;"></a></div>';
                    }
                }
                else {
                    if (imgSrcLink == "hotel/images/noimage.gif") {
                        MainThumbImageLinksString = '<img src="' + ImageLinks.ImageLink.Image.__cdata + '" onerror="imgError(this);" alt="' + ImageLinks.ImageLink.Text.__cdata + '">';
                        $("#mainthumb_" + HotelIndex).html(MainThumbImageLinksString);
                    }
                    ImageLinksString += '<div class="col-sm-3 padding_sm"><a class="fancybox" data-fancybox-group="gallery" title="(Provided by VFM Leonardo Inc.)" href="' + ImageLinks.ImageLink.Image.__cdata + '"><img src="' + ImageLinks.ImageLink.Image.__cdata + '" onerror="imgError(this);" alt="' + ImageLinks.ImageLink.Text.__cdata + '" style="width:100%;height:200px;"></a></div>';
                }
                $("#divSlidePics_" + HotelIndex).html(ImageLinksString + '<div style="float:left;width:100%;"><p>Images provided by VFM Leonardo Inc.</p></div>');

            }
            else {
                $("#mainthumb_" + HotelIndex).html('<img src="HOTEL/hotel/images/noimage.gif" onerror="imgError(this);" alt="noimage">');
            }

            if (ImageLinksString == "") {
                $("#divSlidePics_" + HotelIndex).html('<div style="float:left;width:100%;"><p>No images available.</p></div>');
            }

            //Room Categories
            var RoomCategories = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.RoomCategories;
            if (RoomCategories != null) {
                if (RoomCategories.RoomCategory.length != null) {
                    for (var ircal = 0; ircal < RoomCategories.RoomCategory.length; ircal++) {
                        if (RoomCategories.RoomCategory[ircal]._Id != null) {
                            var rcid = RoomCategories.RoomCategory[ircal]._Id;
                            rcid = rcid.split(':')[0] + ":" + rcid.split(':')[1];
                            $("div.hotel_room_details01[room-i^='" + rcid + "']").each(function () {
                                var temproomid = $(this).attr("id");
                                if (temproomid != undefined) {
                                    temproomid = temproomid.replace("r", "cmod");
                                    var checkstatus = $("#" + temproomid).find("div.hotel_type").find("h4").length;
                                    if (parseInt(checkstatus) == 0) {
                                        // if (RoomCategories.RoomCategory[ircal].RoomDescription != undefined) {
                                        // $("<h4>" + RoomCategories.RoomCategory[ircal].RoomDescription.__cdata + "</h4>").insertAfter($("#" + temproomid).find("div.hotel_type").find("h5"));
                                        //$("<h4>" + RoomCategories.RoomCategory[ircal].Sharebed.__cdata + "</h4>").insertAfter($("#" + temproomid).find("div.hotel_type").find("h5"));
                                        //}
                                    }
                                }
                            });
                        }
                    }
                }
            }

            $(thiss).attr("data-open", 1);
            $("#HViewArea_" + HotelIndex).slideToggle();
            $('#HotMinus_' + HotelIndex).css("display", "block");
            $('#HotPlus_' + HotelIndex).css("display", "none");
            $("#hotel_" + HotelIndex).css("border", "2px solid #ffb84e");
            $('#HotBook_' + HotelIndex).css("display", "block");

        }
        // }
    }
    else {
        $("#HViewArea_" + HotelIndex).show();
        $('#HotMinus_' + HotelIndex).css("display", "block");
        $('#HotPlus_' + HotelIndex).css("display", "none");
        $("#hotel_" + HotelIndex).css("border", "2px solid #ffb84e");
    }


}

function GetFaClass(codee) {

    var faClassRoomFacilities = [
        { code: '*IN', fa: 'fa fa-wifi' },
        { code: '*TV', fa: 'fa fa-tv' },
        { code: '*SV', fa: 'satellite_icon' },
        { code: '*AC', fa: 'fa fa-snowflake-o' },
        { code: '*VL', fa: 'fa fa-plug' },
        { code: '*LT', fa: 'fa fa-laptop' },
        { code: '*FI', fa: 'fa fa-film' },
        { code: '*HD', fa: 'hairdryer_icon' },
        { code: '*RA', fa: 'fa fa-music' },
        { code: '*DD', fa: 'fa fa-phone' },
        { code: '*AW', fa: 'fa fa-volume-control-phone' },
        { code: '*MB', fa: 'fa fa-glass' },
        { code: '*FL', fa: 'fa fa-building-o' }
    ];

    var faClassHotelFacilities = [
        { code: '*EC', fa: 'fa fa-clock-o' },
        { code: '*HP', fa: 'fa fa-bus' },
        { code: '*CP', fa: 'fa fa-car' },
        { code: '*GY', fa: 'gym_icon' },
        { code: '*TE', fa: 'tennis_icon' },
        { code: '*GF', fa: 'golf_icon' },
        { code: '*BS', fa: 'fa fa-child' },
        { code: '*DF', fa: 'fa fa-wheelchair-alt' },
        { code: '*PT', fa: 'fa fa-suitcase' },
        { code: '*OP', fa: 'poolh_icon' },
        { code: '*SA', fa: 'sauna_icon' },
        { code: '', fa: 'fa fa-check' }
    ];

    var outclass = '';
    for (var i = 0; i < faClassRoomFacilities.length; i++) {
        if (faClassRoomFacilities[i].code == codee) {
            outclass += faClassRoomFacilities[i].fa;
            break;
        }
    }
    //for (var i in faClassRoomFacilities) {
    //    if (i.code == codee) {
    //        outclass += i.fa;
    //        break;
    //    }
    //}

    for (var j = 0; j < faClassHotelFacilities.length; j++) {
        if (faClassHotelFacilities[j].code == codee) {
            outclass += faClassHotelFacilities[j].fa;
            break;
        }
    }

    if (outclass.length == 0) {
        outclass += 'fa fa-check';
    }
    return outclass;
}

function finalizeonlyshowclick() {

    //Kelvin Abella - Quotations 6/24/2018
    $(".checkBox_area span").unbind().click(function () {
        var buttonName = $(this).parent().find("input").attr("id");

        $("#" + $(this).parent().parent().attr("id") + " .checkBox_area input").each(function () {
            if (buttonName != $(this).attr("id")) {
                if ($(this).is(":checked")) {
                    $(this).click();
                    //console.log($(this).attr("id"));
                }
            } else {
                if ($(this).is(":checked")) {
                } else {
                    $(this).click();
                }
            }
        });

    });

    //original code for Only link
    //$(".checkBox_area span").click(function () {
    //    $("#" + $(this).parent().parent().attr("id") + " .checkBox_area input").each(function () {
    //        if ($(this).is(":checked")) {
    //            $(this).click();
    //        }
    //    });

    //    if ($(this).parent().find("input").is(":checked")) { }
    //    else { $(this).parent().find("input").click(); }
    //});
}

function imgError(image) {
    if ($(image).parent().attr("Class") == "fancybox") {
        var temparentid = $(image).parent().parent().parent().attr("id");
        $(image).parent().parent().remove();
        var imagecount = $("#" + temparentid).find("a.fancybox").length;
        if (parseInt(imagecount) > 0) {
            var mainimageurl = $("#" + temparentid).find("a.fancybox").first().find("img").attr("src");
            if (mainimageurl == "hotel/images/noimage.gif") {
                $("#" + temparentid.replace("divSlidePics_", "mainthumb_")).find("img").attr("src", mainimageurl);
            }
        }
    }
    $(image).attr("src", "hotel/images/noimage.gif");
}

//Sort
function Sortof(sortitem, thiss) {
    $('#topFilterUL li').removeClass('active');
    $('#' + thiss).addClass('active');
    tinysort('ul#itemContainer>li', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: sortitem });
    return false;
}

//Mob Filter Change
function MobFilterHotelChange() {
    var SelectedText = $('option:selected', '#ddlMobFilterHotel').text();
    var SelectedValue = $('option:selected', '#ddlMobFilterHotel').val();
    if (SelectedValue == "1") {
        tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-price' });
    }
    else if (SelectedValue == "2") {
        tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-price' });
    }
    else if (SelectedValue == "3") {
        tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-name' });
    }
    else if (SelectedValue == "4") {
        tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-name' });
    }
    else if (SelectedValue == "5") {
        tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-star' });
    }
    else if (SelectedValue == "6") {
        tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-star' });
    }
    else if (SelectedValue == "7") {
        tinysort('ul#itemContainer>li', { order: 'asc', attr: 'data-offer' });
    }
    else if (SelectedValue == "8") {
        tinysort('ul#itemContainer>li', { order: 'desc', attr: 'data-offer' });
    }
    $("#mobddlfilterHotelselected").html(SelectedText);
}

//Show hidden cancel policies
function ShowAllCancPolicy(thiss) {
    $(thiss).hide();
    $($(thiss).parent().parent().find("h4")).each(function () {
        $(this).show();
    });
}

//Select Room Click
function SelectRoombkup(thisss) {



    if ($(thisss).attr("sel-stat") != "true") {
        var tempavailstatus = parseInt(1);
        var temphotelroomid = $(thisss).parent().parent().parent().attr("id");
        var temphotelid = temphotelroomid.split('_')[1];
        var temphotelroomgroupid = temphotelroomid.split('_')[2];
        $("#hotel_" + temphotelid + " .hotel_room_details01").each(function () {
            var temproomid = $(this).attr("id");
            //alert(temproomid);
            if (temproomid.indexOf("r_" + temphotelid + "_" + temphotelroomgroupid + "_") != -1) {
                $("#" + temproomid + " .book_button a").each(function () {
                    $(this).removeClass("btn_selectedroom");
                    $(this).html("Select");
                    $(this).attr("sel-stat", "false");
                    $("#" + temproomid).attr("sel-stat", "false");
                });
            }
            // alert("all buttons cleared")
        });

        $(thisss).addClass("btn_selectedroom");
        $(thisss).html("Selected");
        $(thisss).attr("sel-stat", "true");
        $("#" + temphotelroomid).attr("sel-stat", "true");
        var tempnewprice = parseFloat(0);

        $("#hotel_" + temphotelid + " .hotel_room_details01").each(function () {
            if ($(this).attr("sel-stat") == "true") {
                tempnewprice += parseFloat($(this).find("div.cost_area").find("span").html().split(" ")[1]);
                if ($(this).attr("r-stat") == "OR") { tempavailstatus = parseInt(0); }
            }
        });

        if (tempavailstatus == 0) {
            $("#hotel_" + temphotelid).find("div.available").html("On Request");
            $("#hotel_" + temphotelid).find("div.available").addClass("hotelstatus_OR");
            $("#hotel_" + temphotelid).find("div.available").removeClass("available");
        }
        else {
            $("#hotel_" + temphotelid).find("div.hotelstatus_OR").html("AVAILABLE");
            $("#hotel_" + temphotelid).find("div.hotelstatus_OR").addClass("available");
            $("#hotel_" + temphotelid).find("div.hotelstatus_OR").removeClass("hotelstatus_OR");
        }

        //$('.hotpricetot_' + temphotelid).html(Math.ceil(tempnewprice * 100) / 100);
        $('#hotel_' + temphotelid).attr("data-price", (Math.ceil(tempnewprice * 100) / 100));

        $('.hotpricetot_' + temphotelid).prop('Counter', $('.hotpricetot_' + temphotelid).html()).animate({ Counter: tempnewprice }, { duration: 500, easing: 'swing', step: function (tempnewprice) { $('.hotpricetot_' + temphotelid).text(Math.ceil(tempnewprice * 100) / 100); } });
    }
}
//Book button click
function SelectRoom(thisss, tmpSupplierCode) {
    if (tmpSupplierCode == "R24" || tmpSupplierCode == "GRNC") {
        var numroom = GetParameterValues('numroom')
        for (var ix = 0; ix < numroom; ix++) {

            var tempavailstatus = parseInt(1);
            var temphotelroomid = $(thisss).parent().parent().parent().attr("id");

            //r_36_0_2
            //var currentIndex = temphotelroomid.lastIndexOf("_"); //6 
            //var len = temphotelroomid.length - 3;
            //temphotelroomid = temphotelroomid.substring(0, len) + ix + temphotelroomid.substring(len + 1); // r_36_02  
            //var newtemphotelroomid = temphotelroomid.substring(1); //_36_02
            var tempArr = temphotelroomid.split("_");
            temphotelroomid = tempArr[0] + "_" + tempArr[1] + "_" + ix + "_" + tempArr[3];
            var newtemphotelroomid = "_" + tempArr[1] + "_" + ix + "_" + tempArr[3];

            //console.log(newtemphotelroomid);

            var selectIcon = $("#selecticon" + newtemphotelroomid);
            if (selectIcon.attr("sel-stat") != "true" || selectIcon.attr("sel-stat") != true) {
                var temphotelid = temphotelroomid.split('_')[1];
                var temphotelroomgroupid = temphotelroomid.split('_')[2];

                var hotelDetails = $("#hotel_" + temphotelid + " .hotel_room_details01");

                hotelDetails.each(function () {
                    var temproomid = $(this).attr("id");
                    var selectId = temproomid.substring(1);
                    //alert(temproomid);
                    if (temproomid.indexOf("r_" + temphotelid + "_" + temphotelroomgroupid + "_") != -1) {
                        selectId = $("#selecticon" + selectId);
                        //console.log(selectId);
                        if (selectId.attr("sel-stat") == "true") {
                            selectId.removeClass("btn_selectedroom").html("Select").attr("sel-stat", "false");
                            $("#" + temproomid).attr("sel-stat", "false");
                        }
                    }
                    // alert("all buttons cleared")
                });
                $("#selecticon" + newtemphotelroomid).addClass("btn_selectedroom").html("Selected").attr("sel-stat", "true");
                //$selectIcon.addClass("btn_selectedroom").html("Selected").attr("sel-stat", "true");
                // $("#selecticon" + newtemphotelroomid).attr("sel-stat", "true");
                $("#" + temphotelroomid).attr("sel-stat", "true");



                var tempnewprice = parseFloat(0);

                hotelDetails.each(function () {
                    var $this = $(this);
                    if ($this.attr("sel-stat") == "true") {
                        tempnewprice += parseFloat($this.find("div.cost_area").find("span").html().split(" ")[1]);
                        //  tempnewprice += parseFloat($(this).find("div.hotel_room_details01").firstElementChild.children[2].children["0"].innerHTML.split(" ")[1]);
                        if ($this.attr("r-stat") == "OR") {
                            tempavailstatus = parseInt(0);
                        }
                    }
                });

                var hotelId = $("#hotel_" + temphotelid);
                if (tempavailstatus == 0) {
                    hotelId.find("div.available").html("On Request").addClass("hotelstatus_OR").removeClass("available");
                }
                else {
                    hotelId.find("div.hotelstatus_OR").html("AVAILABLE").addClass("available").removeClass("hotelstatus_OR");
                }

                //$('.hotpricetot_' + temphotelid).html(Math.ceil(tempnewprice * 100) / 100);
                hotelId.attr("data-price", (Math.ceil(tempnewprice * 100) / 100));

                $('.hotpricetot_' + temphotelid).prop('Counter', $('.hotpricetot_' + temphotelid).html()).animate({ Counter: tempnewprice }, { duration: 500, easing: 'swing', step: function (tempnewprice) { $('.hotpricetot_' + temphotelid).text(Math.ceil(tempnewprice * 100) / 100); } });
            }
        }
    }
    else {
        if ($(thisss).attr("sel-stat") != "true") {
            var tempavailstatus = parseInt(1);
            var temphotelroomid = $(thisss).parent().parent().parent().attr("id");
            var temphotelid = temphotelroomid.split('_')[1];
            var temphotelroomgroupid = temphotelroomid.split('_')[2];

            var hotelDetails = $("#hotel_" + temphotelid + " .hotel_room_details01");
            //var tmain0 = performance.now();
            hotelDetails.each(function () {
                var temproomid = $(this).attr("id");
                //alert(temproomid);
                var selectId = temproomid.substring(1);

                if (temproomid.indexOf("r_" + temphotelid + "_" + temphotelroomgroupid + "_") != -1) {
                    selectId = $("#selecticon" + selectId);
                    if (selectId.attr("sel-stat") == "true" || selectId.attr("sel-stat") == true) {
                        //console.log(selectId);
                        selectId.removeClass("btn_selectedroom").html("Select").attr("sel-stat", "false");
                        $("#" + temproomid).attr("sel-stat", "false");
                    }
                }
                // alert("all buttons cleared")
            });


            $(thisss).addClass("btn_selectedroom").html("Selected").attr("sel-stat", "true");
            $("#" + temphotelroomid).attr("sel-stat", "true");
            var tempnewprice = parseFloat(0);

            hotelDetails.each(function () {
                var $this = $(this);
                if ($this.attr("sel-stat") == "true") {
                    tempnewprice += parseFloat($this.find("div.cost_area").find("span").html().split(" ")[1]);
                    if ($this.attr("r-stat") == "OR") {
                        tempavailstatus = parseInt(0);
                    }
                }
            });


            var hotelId = $("#hotel_" + temphotelid);
            if (tempavailstatus == 0) {
                hotelId.find("div.available").html("On Request").addClass("hotelstatus_OR").removeClass("available");
            }
            else {
                hotelId.find("div.hotelstatus_OR").html("AVAILABLE").addClass("available").removeClass("hotelstatus_OR");
            }

            //$('.hotpricetot_' + temphotelid).html(Math.ceil(tempnewprice * 100) / 100);
            hotelId.attr("data-price", (Math.ceil(tempnewprice * 100) / 100));

            $('.hotpricetot_' + temphotelid).prop('Counter', $('.hotpricetot_' + temphotelid).html()).animate({ Counter: tempnewprice }, { duration: 500, easing: 'swing', step: function (tempnewprice) { $('.hotpricetot_' + temphotelid).text(Math.ceil(tempnewprice * 100) / 100); } });
            //console.group("button");
            //var tmain1 = performance.now();
            //console.log("Call to select room took " + (tmain1 - tmain0) + " milliseconds.", "loop count" + count);
            //console.groupEnd();
        }
    }
}
function BookHotelClick(SupplierCode, CityCode, HotelCode, HotelIndex) {
    $('#HotBookWait_' + HotelIndex).addClass('fa');
    $('#HotBookWait_' + HotelIndex).addClass('fa-hourglass-o');
    $('#HotBookWait_' + HotelIndex).addClass('fa-spin');
    document.getElementById('HotBook_' + HotelIndex).style.cursor = "not-allowed";
    $('#sort_popup').css("z-index", "99");
    //if (RoleBooksHotel != '1' && RoleBooksHotel != '-1') {
    //    alertify.alert('No permission', 'Please note ! You dont have permission to proceed with this booking.');
    //    return false;
    //}
    //var TotalPrice = $('#hotel_' + HotelIndex).attr("data-price");
    //if ((parseFloat(TotalPrice) > parseFloat(CreditLimittAmount)) && (AgencyTypeIDD != '1')) {// && (AgencyTypeIDD != '1' && AgencyTypeIDD != '2')) {
    //    alertify.alert('Credit balance', 'Please note ! Not enough credit balance to proceed with this booking.');
    //    return false;
    //}

    var cin = GetParameterValues('cin');
    var cout = GetParameterValues('cout');
    var night = GetParameterValues('night');
    var numroom = GetParameterValues('numroom');
    var rooms = '';
    var roomcategoryid = '';
    var roomcategoryval = '';
    //id="selecticon_' + sjs + '_' + ro + '_' + roCat +'"
    var roomDescription = '';
    var roomStatus = '';
    var cotexist = false;
    for (var irt = 1; irt <= parseInt(numroom); irt++) {
        if (rooms == '') {
            rooms += GetParameterValues('room' + irt + '');
        }
        else {
            rooms += '/' + GetParameterValues('room' + irt + '');
        }

        if (cotexist == false) {
            var roomdata = GetParameterValues('room' + irt + '');
            var roomPaxes = roomdata.split(',');
            for (var irp = 0; irp < roomPaxes.length; irp++) {
                var PaxTypeCount = roomPaxes[irp].split('_');
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0") {
                    for (var iptc = 0; iptc < parseInt(PaxTypeCount[1]); iptc++) {
                        var ChdAge = parseInt(PaxTypeCount[iptc + 2]);
                        if (ChdAge == 1) {
                            cotexist = true;
                        }
                    }
                }
            }
        }
    }
    window.localStorage.setItem("cin", cin);
    window.localStorage.setItem("cout", cout);
    window.localStorage.setItem("night", night);
    window.localStorage.setItem("numroom", numroom);
    window.localStorage.setItem("rooms", rooms);
    window.localStorage.setItem("SupplierCode", SupplierCode);
    var selectedhotelrooms = [];
    var selectedhotelgroupcode = [];
    var selectedhotelroomcode = [];
    var selectedhotelcpcode = [];
    var selectedhotelroomref = [];
    var selectedhotelratetype = [];
    var selectedhotelratekey = [];
    var selectedhotelcitycode = [];
    var selectedhotelsid = [];
    var selectedbundlestatus = [];
    var pastdatestat = false;
    var availablestat = false;
    $("#hotel_" + HotelIndex + " .hotel_room_details01").each(function () {
        if ($(this).attr("sel-stat") == "true") {

            selectedhotelrooms.push($(this).attr("id").replace("r_" + HotelIndex + "_", ""));
            selectedhotelgroupcode.push($(this).attr("pr-groupcode"));
            selectedhotelroomcode.push($(this).attr("pr-roomcode"));
            selectedhotelratekey.push($(this).attr("pr-ratekey"));
            selectedhotelcitycode.push($(this).attr("pr-citycode"));
            selectedhotelcpcode.push($(this).attr("pr-cpcode"));
            selectedhotelroomref.push($(this).attr("pr-roomref"));
            selectedhotelratetype.push($(this).attr("pr-ratetype"));
            selectedhotelsid.push($(this).attr("pr-sid"));
            selectedbundlestatus.push($(this).attr("pr-bundlestatus"));

            var selectedhotelroomscode = [];
            if (roomcategoryid == '') {
                roomcategoryid += $(this).attr("room-i");
                roomcategoryval += $(this).attr("id");

            }
            else {
                roomcategoryid += ',' + $(this).attr("room-i");
                roomcategoryval += ',' + $(this).attr("id");
            }

            //Room desc
            if (roomDescription == '') {
                roomDescription += $(this).attr("r-desc");
            }
            else {
                roomDescription += ',' + $(this).attr("r-desc");
            }

            //past policy alert
            var SelRoomID = $(this).attr("id");
            var PopID = SelRoomID.replace('r', 'cmod');
            if ($('#' + PopID + ' .modal-dialog .modal-content .sp_bg_hotel h3').attr("xlcharge") == 'true') {
                pastdatestat = true;
            }
            if ($(this).attr("r-stat") == 'OR') {
                availablestat = true;
            }

            //availability
            if (roomStatus == '') {
                roomStatus += $(this).attr("r-stat");
            }
            else {
                roomStatus += ',' + $(this).attr("r-stat");
            }
        }
    });
    night = GetParameterValues('night');
    numroom = GetParameterValues('numroom');
    CityCode = GetParameterValues('city');
    window.localStorage.setItem("ratekey", selectedhotelratekey);
    window.localStorage.setItem("roomcode", selectedhotelroomcode);
    window.localStorage.setItem("roomref", selectedhotelroomref);
    window.localStorage.setItem("cpcode", selectedhotelcpcode);
    window.localStorage.setItem("citycodes", selectedhotelcitycode);
    window.localStorage.setItem("ratetype", selectedhotelratetype);
    window.localStorage.setItem("sid", selectedhotelsid);
    window.localStorage.setItem("bundlestatus", selectedbundlestatus);
    window.localStorage.setItem("selectedrooms", selectedhotelrooms);
    window.localStorage.setItem("groupcode", selectedhotelgroupcode);
    window.localStorage.setItem("roomcatid", roomcategoryid);
    window.localStorage.setItem("CityCode", CityCode);
    window.localStorage.setItem("HotelCode", HotelCode);
    window.localStorage.setItem("HotelImage", $("#mainthumb_" + HotelIndex + " img").attr("src"));
    if (SupplierCode == 'JAC' || SupplierCode == 'R24' || SupplierCode == 'GRNC') {
        var night = GetParameterValues('night');
        var numroom = GetParameterValues('numroom');
        var CityCode = GetParameterValues('city');
        var Datas = {
            cin: cin,
            night: night,
            numroom: numroom,
            rooms: rooms,
            Roomcategoryid: roomcategoryid,
            CityCode: CityCode,
            HotelCode: HotelCode,
            Curr: "USD",
            Lang: "en",
            cout: cout,
            SupplierCode: SupplierCode,
            AgencyCode: HotelAgencyCode,
            token: accesstocken

        }

        var failureFunc = function (response) {
            //   alert(response.d);
            alertify.alert('Cancellation Policy', 'Unable to show cancellation policy.Please try again!!');
            $('#icon_' + canValID).removeClass('fa-spin');

        };

        var errorFunc = function (response) {
            //                 alert(response.d);
            alertify.alert('Cancellation Policy', 'Unable to show cancellation policy.Please try again!!');
            $('#icon_' + canValID).removeClass('fa-spin');
            //    window.location.href = "/HOTEL/hotelpax.html";
        };

        function AddJACCanplcy(response) {
            // alert(response);
            var xml = response;
            if (xml != 'NORESULT') {
                var x2js2 = new X2JS();
                var jsonRes = JSON.parse(xml);
                for (var iRoom = 0; iRoom < jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom.length; iRoom++) {
                    var fields = roomcategoryval.split(',');

                    var tempposition = fields[iRoom].split('_');
                    var temppos = tempposition[3];
                    var CancPolicyString = GetCancellationPolicy(jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[iRoom].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition, SupplierCode);
                    //  var detai = canValID.split('_');


                    //Hotel[HotelIndex].PaxRoomSearchResults.PaxRoom[iRoom].RoomCategories.RoomCategory[temppos].ChargeConditions.ChargeCondition = jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[iRoom].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition;



                    if (SupplierCode != 'R24' && SupplierCode != 'GRNC') {
                        SessionHotel[HotelIndex].PaxRoomSearchResults.PaxRoom[iRoom].RoomCategories.RoomCategory[temppos].ChargeConditions.ChargeCondition = jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[iRoom].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition;
                    }
                }


                BookHotelClickFilled(SupplierCode, CityCode, HotelCode, HotelIndex);
            }
        }

        ajaxHelper(AddJACCanplcy, failureFunc, errorFunc, { url: HotelPrebookURL, data: Datas });

    }
    else {
        BookHotelClickFilled(SupplierCode, CityCode, HotelCode, HotelIndex);
    }

    function BookHotelClickFilled(SupplierCode, CityCode, HotelCode, HotelIndex) {

        // window.localStorage.setItem("HotelPrice", $('#hotel_' + HotelIndex).attr("data-price"));
        var HPrice = $('#hotel_' + HotelIndex).attr("data-price");
        var Datatable = {
            sessionname: 'HotelPrice',
            valueses: HPrice
        };

        ajaxHelper(Savesessionsuccess, null, null, { url: Setup.Session.Url.SaveDataToSession, data: Datatable });

        window.localStorage.setItem("roomdescription", roomDescription);
        window.localStorage.setItem("roomStatus", roomStatus);
        window.localStorage.setItem("HotelAddress", $("#hotAddress_" + HotelIndex).html().split("<br>")[1]);
        //console.log($("#hotAddress_" + HotelIndex).html().split("<br>")[1]);

        //console.log(roomcategoryid);
        if (Hotel.length != null) {
            Hotel = SessionHotel[HotelIndex];
        }
        else {
            Hotel = SessionHotel;
        }
        //console.log(Hotel)

        var Dastasseq = {
            SelHotel: JSON.stringify(Hotel)
        }

        function SaveHotelSuccess(data) {
            //console.log(cotexist)

            var alertmsg = '';
            if (pastdatestat == true) {
                alertmsg += '* Please note the booking is under cancellation period and you here by agree the cancellation policies.</br></br>';
            }
            if (availablestat == true) {
                alertmsg += '* On Request hotel, we will contact the hotel and you will receive an update with in 48 hours, if we cannot confirm your request you will not be charged.</br></br>';
            }
            if (cotexist == true) {
                alertmsg += '* Cot(s) will be requested at the hotel, however cots are not guaranteed and are subject to availability at check-in.Also, maximum age for cot is 2 years old.</br></br>';
            }
            alertmsg += 'Please click OK if you agree the above terms or click cancel to select another hotel';

            if (availablestat == true || pastdatestat == true || cotexist == true) {
                alertify.confirm("Please note", alertmsg,
                    function () {
                        window.location.href = "/HOTEL/hotelpax.html";
                    },
                    function () {
                        $('#sort_popup').css("z-index", "99999");
                        $('#HotBookWait_' + HotelIndex).removeClass('fa');
                        $('#HotBookWait_' + HotelIndex).removeClass('fa-hourglass-o');
                        $('#HotBookWait_' + HotelIndex).removeClass('fa-spin');
                        document.getElementById('HotBook_' + HotelIndex).style.cursor = "pointer";
                    });
            }
            else {
                window.location.href = "/HOTEL/hotelpax.html";
            }
        }

        ajaxHelper(SaveHotelSuccess, null, null, { url: Setup.Session.Url.SaveHotelSequence, data: Dastasseq });

    }
}
function Savesessionsuccess() {

}
function GetFaimages(codee) {

    var imgClassRoomFacilities = [
        { code: '*IN', fa: 'fa-wifi.png' },
        { code: '*TV', fa: 'fa-tv.png' },
        { code: '*SV', fa: 'satellite.png' },
        { code: '*AC', fa: 'fa-snowflake-o.png' },
        { code: '*VL', fa: 'fa-plug.png' },
        { code: '*LT', fa: 'fa-laptop.png' },
        { code: '*FI', fa: 'fa-film.png' },
        { code: '*HD', fa: 'hairdryer.png' },
        { code: '*RA', fa: 'fa-music.png' },
        { code: '*DD', fa: 'fa-phone.png' },
        { code: '*AW', fa: 'fa-volume-control-phone.png' },
        { code: '*MB', fa: 'fa-glass.png' },
        { code: '*FL', fa: 'fa-building-o.png' }
    ];

    var imgClassHotelFacilities = [
        { code: '*EC', fa: 'fa-clock-o.png' },
        { code: '*HP', fa: 'fa-bus.png' },
        { code: '*CP', fa: 'fa-car.png' },
        { code: '*GY', fa: 'gym.png' },
        { code: '*TE', fa: 'tennis.png' },
        { code: '*GF', fa: 'golf.png' },
        { code: '*BS', fa: 'fa-child' },
        { code: '*DF', fa: 'fa-wheelchair-alt.png' },
        { code: '*PT', fa: 'fa-suitcase.png' },
        { code: '*OP', fa: 'pool-heated.png' },
        { code: '*SA', fa: 'sauna.png' }
    ];

    var outclassimg = '';

    for (var i = 0; i < imgClassRoomFacilities.length; i++) {
        if (imgClassRoomFacilities[i].code == codee) {
            outclassimg += imgClassRoomFacilities[i].fa;
            break;
        }
    }

    //for (var j of imgClassHotelFacilities) {
    //    if (j.code == codee) {
    //        outclassimg += j.fa;
    //        break;
    //    }
    //}

    for (var j = 0; j < imgClassHotelFacilities.length; j++) {
        if (imgClassHotelFacilities[j].code == codee) {
            outclassimg += imgClassHotelFacilities[j].fa;
            break;
        }
    }

    if (outclassimg.length == 0) {
        outclassimg += 'fa-check.png';
    }
    return outclassimg;
}
function GetJACCancelplcyfromPrebook(SupplierCode, popid, GrncCanstatus, HotelCode, canValID, rmcatid, rmindex, thisdv) {

    var fields = rooms.split('/');

    var tmprooms = fields[rmindex - 1];

    var dopen = $(thisdv).attr("data-open");
    //   if (dopen == 0 && dopen == 1) {
    $('#icon_' + canValID).addClass('fa-spin');
    var night = GetParameterValues('night');
    var numroom = GetParameterValues('numroom');
    var CityCode = GetParameterValues('city');
    var Datas = {
        cin: cin,
        night: night,
        numroom: 1,
        rooms: tmprooms,
        Roomcategoryid: rmcatid,
        CityCode: CityCode,
        HotelCode: HotelCode,
        Curr: "USD",
        Lang: "en",
        cout: cout,
        SupplierCode: SupplierCode,
        AgencyCode: HotelAgencyCode,
        token: accesstocken

    }

    var failureFunc = function (response) {
        alertify.alert('Cancellation Policy', 'Unable to show cancellation policy. Please try again.', function () {
            $('#icon_' + canValID).removeClass('fa-spin');
        });

    };

    var errorFunc = function (response) {
        alertify.alert('Cancellation Policy', 'Unable to show cancellation policy. Please try again.', function () {
            $('#icon_' + canValID).removeClass('fa-spin');
        });
    };

    function AddJACCanplcy(response) {
        // alert(response);
        var xml = response;
        if (xml != 'NORESULT') {
            var x2js2 = new X2JS();
            // var jsonHotelAvail = x2js2.xml_str2json(xmlbookresp);
            var jsonRes = JSON.parse(xml);
            //   var x2js = new X2JS();
            //var jsonRes = x2js.xml_str2json(xml);
            //   jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[0].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition
            //  var jsonRes = xml;
            //var CancPolicyString = GetCancellationPolicy(Hotel[h].PaxRoomSearchResults.PaxRoom[ro].RoomCategories.RoomCategory[roCat].ChargeConditions.ChargeCondition);
            var CancPolicyString = GetCancellationPolicy(jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[0].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition, SupplierCode);
            $("#candiv_" + canValID).html(CancPolicyString);
            $("#cmod_" + canValID).modal('show');
            $('#icon_' + canValID).removeClass('fa-spin');
            $(thisdv).attr("data-open", 1);
            //       $("#hotel_" + temphotelid).find("div.available").html("On Request");

            var hotelObjInfo = jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel;
            var rInfoComments = (!("Comments" in hotelObjInfo)) ? "null" : hotelObjInfo.Comments;

            var essentialInfoText = $("#candiv_" + canValID).next().find("h4").html();
            if (essentialInfoText == "" || essentialInfoText == "null") {
                $("#candiv_" + canValID).next().find("h4").html(rInfoComments == "null" ? "No infomation to display." : rInfoComments);
            }

            if (SupplierCode != 'R24') {
                var detai = canValID.split('_');
                SessionHotel[detai[0]].PaxRoomSearchResults.PaxRoom[detai[1]].RoomCategories.RoomCategory[detai[2]].ChargeConditions.ChargeCondition = jsonRes.Response.ResponseDetails.SearchHotelPricePaxResponse.HotelDetails.Hotel.PaxRoomSearchResults.PaxRoom[0].RoomCategories.RoomCategory.ChargeConditions.ChargeCondition;
            }

            //      Hotel.find("#candiv_" + canValID).html(CancPolicyString);
            //       Hotel.$("#candiv_" + canValID).html(CancPolicyString);
            //   console.log(jsonRes);
        }
        else {
            alertify.alert('Cancellation Policy', 'Unable to show cancellation policy. Please try again.', function () {
                $('#icon_' + canValID).removeClass('fa-spin');
            });
        }
    }

    if (dopen == 0 && SupplierCode == 'GRNC') {
        if (GrncCanstatus == 'false' && SupplierCode == 'GRNC') {

            ajaxHelper(AddJACCanplcy, failureFunc, errorFunc, { url: HotelPrebookURL, data: Datas });
        }
        else if (GrncCanstatus == 'true' && SupplierCode == 'GRNC') {
            $('#icon_' + canValID).addClass('fa-spin');
            $('#shijiyas').html('<h4>Success</h4>');
            //   $("#cmod_" + canValID).addClass('fade in');
            $("#cmod_" + canValID).modal('show');//slideToggle();
            $('#icon_' + canValID).removeClass('fa-spin');
        }
    }
    else if ((dopen == 0 && SupplierCode == 'JAC') || (dopen == 0 && SupplierCode == 'R24')) {

        ajaxHelper(AddJACCanplcy, failureFunc, errorFunc, { url: HotelPrebookURL, data: Datas });

    }
    else {
        $('#icon_' + canValID).addClass('fa-spin');
        $('#shijiyas').html('<h4>Success</h4>');
        //   $("#cmod_" + canValID).addClass('fade in');
        $("#cmod_" + canValID).modal('show');//slideToggle();
        $('#icon_' + canValID).removeClass('fa-spin');
        //    $('#HotMinus_' + HotelIndex).css("display", "block");
        //    $('#HotPlus_' + HotelIndex).css("display", "none");
        //  $("#hotel_" + HotelIndex).css("border", "2px solid #ffb84e");
        ////   //   id = "cmod_' + h + '_' + ro + '_' + roCat +
        ////   $("#cmod_" + popid).removeClass("fade");//.slideToggle(); class
        ////   $("#cmod_" + popid).css("display", "block");
        //////   $('#HotPlus_' + HotelIndex).css("display", "none");
        ////   $("#cmod_" + popid).css("border", "2px solid #ffb84e");
    }

}

//================ Map  FancyBox Popup================

function GetGmap() {
    isGetMapClicked = true;
    $("#map").html('');
    $(".view_map").show('');
    $(".hotel_banner").hide('');
    $(".main_cont_area").hide('');
    $('footer').css("margin-top", "0px");
    $('header').css("z-index", "99");
    $("#idmapfilter").css("display", "block");
    $(".point_int").hide('');
    $(".hotel_map_list").show('');

    var locations = [];
    locations = hotellocations;
    var mapset = false;
    for (i = 0; i < locations.length; i++) {
        var geoloc = hotellocations[i].split(",");
        if (geoloc[5] != "" && geoloc[6] != "") {
            mapset = true;
            i = locations.length;
            publicgeocodeatti = geoloc[5];
            publicgeocodelongi = geoloc[6];
            //console.log(locations);
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: new google.maps.LatLng(geoloc[5], geoloc[6]),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
    }
    if (mapset == false) {
        var geoloc = hotellocations[0].split(",");

        mapset = true;
        i = locations.length;
        publicgeocodeatti = geoloc[5];
        publicgeocodelongi = geoloc[6];
        //console.log(locations);
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: new google.maps.LatLng(geoloc[5], geoloc[6]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

    }

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    $("#idgmapprint").css("display", "none");
    $("#idgmapview").css("display", "block");
    $("#getmaplistinfo").html("");
    for (i = 0; i < locations.length; i++) {
        var locationss = hotellocations[i].split(",");
        if ((locationss[5] != "") && (locationss[6] != "")) {


            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locationss[5], locationss[6]),
                icon: "../HOTEL/hotel/images/marker_icon.png",
                map: map
            });
            html[i] = '<div class="map_details" id="viewmap_' + i + '"><div class="hotel_img"><img src="' + locationss[3] + '" onerror="imgError(this);"  alt="noimage" style="height: 82px;"></div> <div class="hotel_dis"> <h3 id="hotlnme" style="padding-right:95px;">' + locationss[0] + '</h3><h4>' + locationss[1] + '</h4> <div class="hotel_location"><i class="fa fa-map-marker sp01" aria-hidden="true"></i> <span>' + locationss[2] + '</span> </div> </div><div class="map_rate"><span>' + locationss[7] + '</span> ' + locationss[4] + '</div> <div class="direction_btn"><input type="submit" class="direction_btn1" data_hotelname="' + locationss[0] + '" data_latt="' + locationss[5] + '" data_longi="' + locationss[6] + '" onclick="directionviewfrom(this)" value="Directions From"><input type="submit" class="direction_btn2" data_hotelname="' + locationss[0] + '" data_latt="' + locationss[5] + '" data_longi="' + locationss[6] + '" onclick="directionviewto(this)" value="Directions To"></div> </div>';
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(html[i]);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            $("#getmaplistinfo").append('<li class="hotel_view_area_map" "data-show="0" "data-name="' + locationss[0] + '" id="mapbindid_' + i + '" onclick="Getviewmap(this,' + locationss[5] + ',' + locationss[6] + ')"> <div class="list_htlImg"><img src="' + locationss[3] + '" onerror="imgError(this);" alt="hotels"/></div> <h2>' + locationss[0] + '</h2> <h3><span>' + locationss[7] + '</span> ' + locationss[4] + '</h3></li>');

        }
    }

    $("#map").show();
    $("#itemContainer").hide();

    //=========================================SearchHotelNameinMapstart==========================================================//
    $("#txtHotelnameFilterinmap").keyup(function () {


        FinalizeHotelNameFIlterMap();
    });
    function FinalizeHotelNameFIlterMap() {
        var tempval = $('#txtHotelnameFilterinmap').val().toLowerCase();

        $("#getmaplistinfo li").each(function () {

            var DataShowValue = $(this).attr('"data-show');
            var temphotelname = $(this).attr('"data-name').toLowerCase();
            var DataShowValueNew = parseInt(0);
            if (DataShowValue == 0) { DataShowValueNew = 0; }
            else { DataShowValue = DataShowValue - 1; }
            if (temphotelname.indexOf(tempval) == -1) {
                if (DataShowValue == 0) { DataShowValueNew = 1; }
                else { DataShowValueNew = DataShowValue + 1; }
            }
            else {
                if (DataShowValue == 0) { DataShowValueNew = 0; }
                else { DataShowValueNew = DataShowValue + 1; }
            }
            $(this).attr("data-show", DataShowValueNew);
        });
        ShowLIBlocksMaps();
    }
    function ShowLIBlocksMaps() {


        $("#getmaplistinfo li").each(function () {
            var DataShowValue = $(this).attr("data-show");
            var ID = $(this).attr("id");
            if (ID != "-1") {

                if (DataShowValue == 0) { $('#' + ID).show(); }
                else { $('#' + ID).hide(); }

            }
        });

    }
    //=========================================SearchHotelNameinMapEnd==========================================================//


}


function Getviewmap(mapidd, locationsslatt, locationsslong) {

    var marker;
    var mapid = $(mapidd).attr("id");
    var id = mapid.split("_");
    var idinfo = id[1];
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: new google.maps.LatLng(locationsslatt, locationsslong),

        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(locationsslatt, locationsslong),
        icon: "../HOTEL/hotel/images/marker_icon.png",
        map: map
    });
    var infowindow = new google.maps.InfoWindow({
        content: html[idinfo],

    });
    infowindow.open(map, marker);
    google.maps.event.addListener(marker, "click", function () {
        infowindow.setContent(this.html);
        infowindow.open(map, this);
    });
    //GetGmapfull();
}


function Getlist() {
    isGetMapClicked = false;
    $("#map").hide();
    $("#itemContainer").show();
}
function GetCountrycode() {
    var Datatable = {
        Citycode: city
    };

    function GetCountrycodesuccess(data) {
        Countrycode = data.d
        //console.log(Countrycode);

    }
    ajaxHelper(GetCountrycodesuccess, null, null, { url: "WebMethodDbHotel.aspx/GetCountrycode", data: Datatable });

}

function GetGTAImageCountrycode() {
    Datatable = {
        Citycode: city
    };

    function GetGTACountrycodesuccess(data) {
        GTAImageCountrycode = data.d;
        isPausedOnGtaImage = true;
        //console.log(Countrycode);
        //SessiongetAgencyCode("SaveAgencyCode");
        //setmodifyfield();

    }
    ajaxHelper(GetGTACountrycodesuccess, null, null, { url: "WebMethodDbHotel.aspx/GetGTAImageCountrycode", data: Datatable });
}

//=============End map=========================
/////////////////////////////////////////


//===============modify search start==============//
///////////////////////////////////////////////////
function setmodifyfield() {
    //Hotel City
    $(".hotelcity").autocomplete({
        autoFocus: true,
        source: AreaCityList, minLength: 3, position: { my: "left top", at: "left bottom" },
        change: function (event, ui) { if (!ui.item) { $(this).val(''); $(this).prop('placeholder', 'No City Found !'); } },
        select: function (event, ui) {
            if (ui.item.id != 0) {
                if (ui.item.value.indexOf('No City Found !') < 0) {
                    this.value = ui.item.label;
                    $('#hdcitycode').val(ui.item.C);
                    // $('#hdloctype').val(ui.item.T);
                }
            } return false;
        },

        focus: function () { return false; }
    });
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
        var matcher1 = new RegExp("^" + "C", "i");
        return ($.grep(array, function (value) { return ((matcher1.test(value.T) && (matcher.test(value.C) || matcher.test(value.label)))); })).slice(0, 15);
    };

    var tempnumberofmonths = parseInt(1);
    if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
        tempnumberofmonths = parseInt(2);
    }
    else if (parseInt($(window).width()) > 999) {
        tempnumberofmonths = parseInt(3);
    }

    $("#txtCheckInDate").datepicker({
        dateFormat: DateFormate,
        minDate: 0,
        maxDate: "360d",
        numberOfMonths: tempnumberofmonths,
        onSelect: function (date) {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            startDate.setDate(startDate.getDate() + 1);
            dt2.datepicker('option', 'minDate', startDate);
            dt2.datepicker('setDate', minDate);
            //$(this).datepicker('option', 'minDate', minDate);
            $('#txtNights').val("1");
        },
        onClose: function (dateText, inst) {
            $('#txtCheckOutDate').focus();
        }
    });
    $("#txtCheckInDate").datepicker({ dateFormat: DateFormate, }).datepicker("setDate", new Date().getDay + 1);
    //Checkout Date
    $('#txtCheckOutDate').datepicker({
        dateFormat: DateFormate,
        minDate: +2,
        maxDate: "360d",
        numberOfMonths: tempnumberofmonths,
        onSelect: function (date) {
            var a = $("#txtCheckInDate").datepicker('getDate').getTime(),
                b = $("#txtCheckOutDate").datepicker('getDate').getTime(),
                c = 24 * 60 * 60 * 1000,
                diffDays = Math.round(Math.abs((a - b) / (c)));
            $('#txtNights').val(diffDays);
        }
    });
    $("#txtCheckOutDate").datepicker({ dateFormat: DateFormate, }).datepicker("setDate", new Date().getDay + 2);
    $("#txtNights").val(1);
    $("#txtNights").keyup(function () {
        if ($(this).val() != "") {
            var numnights = parseInt(1);
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var minDate = $('#txtCheckInDate').datepicker('getDate');
            var checkstatus = parseInt(1);

            $(this).val($(this).val().replace(/[^0-9\.]/g, function () {
                var a = $("#txtCheckInDate").datepicker('getDate').getTime(),
                    b = $("#txtCheckOutDate").datepicker('getDate').getTime(),
                    c = 24 * 60 * 60 * 1000,
                    diffDays = Math.round(Math.abs((a - b) / (c)));
                numnights = parseInt(diffDays)
                startDate.setDate(startDate.getDate() + parseInt(numnights));
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('setDate', minDate);
                checkstatus = parseInt(0);
                alertify.alert('Validate', 'Only numbers allowed !');
            }));

            if (parseInt(checkstatus) == 1) {
                numnights = parseInt($('#txtNights').val());
                if (numnights > 90) {
                    numnights = parseInt(90);
                    alertify.alert('Maximum Nights', 'Maximum 90 Nights allowed !');
                }
                startDate.setDate(startDate.getDate() + numnights);
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('setDate', minDate);
            }
            $('#txtNights').val(numnights.toString());
        }
    });

    //CHD Change
    $(".ChildCount").change(function () {
        var CHDCount = $(this).find("option:selected").text();
        var ThisID = $(this).attr("id").split('_');
        var RoomID = ThisID[1];
        if (CHDCount == 0) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").hide();
            $("#DivRoom_" + RoomID + "_CHDAge_2").hide();
        }
        if (CHDCount == 1) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").show();
            $("#DivRoom_" + RoomID + "_CHDAge_2").hide();
        }
        if (CHDCount == 2) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").show();
            $("#DivRoom_" + RoomID + "_CHDAge_2").show();
        }
    });
    //Room Count Change
    $("#selNumRoom").change(function () {
        var RoomCount = $(this).find("option:selected").text();
        for (var irt = 2; irt <= parseInt(RoomCount); irt++) {
            $("#r" + irt + "Div").show();
        }
        for (var irt = 9; irt > parseInt(RoomCount); irt--) {
            $("#r" + irt + "Div").hide();
        }
    });
    //max price
    $("#txtMaxPrice").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}
function setModifySearch() {
    $('#txtCity').val(getAreaName(city));
    $('#hdcitycode').val(city);
    $('#selSupplier').val(suppliercode);
    $('#selCoutryOfRes').val(countryofresidence);
    $('#selNationality').val(nation);
    $('#txtCheckInDate').val(moment(GetParameterValues('cin'), "DD/MM/YYYY").format("DD/MM/YYYY"));
    $('#txtCheckOutDate').val(moment(GetParameterValues('cout'), "DD/MM/YYYY").format("DD/MM/YYYY"));



    $('#txtNights').val(night);
    $('#selNumRoom').val(numroom);
    var RoomCount = rooms.split('/');
    var Paxes = [];
    var ir = parseInt(1)
    for (var irt = 0; irt < parseInt(numroom); irt++) {
        $("#r" + ir + "Div").show();
        var roomEach = RoomCount[irt];
        Paxes = roomEach.split(',');
        for (var ipax = 0; ipax < Paxes.length; ipax++) {
            var PaxTypeCount = []
            PaxTypeCount = Paxes[ipax].split('_');
            if (PaxTypeCount[0] == "ADT") {
                $('#r' + ir + 'ADTCount').val(PaxTypeCount[1])
                $('#r' + ir + 'ADTCount').show();
            }
            if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0") {
                $('#Room_' + ir + '_CHDCount').val(PaxTypeCount[1])
                var ich = parseInt(1);
                for (var ichcount = 0; ichcount < parseInt(PaxTypeCount[1]); ichcount++) {
                    $('#r' + ir + 'CHD' + ich + 'Age').val(PaxTypeCount[ichcount + 2]);
                    $('#r' + ir + 'CHD' + ich + 'Age').show();
                    $('#DivRoom_' + ir + '_CHDAge_' + ich).show();
                    ich++;
                }

                $('#Room_' + ir + '_CHDCount').show();

            }
        }

        ir++;
    }



}
function modifySearch() {
    var countryofresidence = $('#selCoutryOfRes').find(":selected").val();
    var Nation = $('#selNationality').find(":selected").val();
    var Supplier = $('#selSupplier').find(":selected").val();
    var CheckinD = $('#txtCheckInDate').val();
    var cityname = $('#txtCity').val();
    var CheckoutD = $('#txtCheckOutDate').val();
    var NumNights = $('#txtNights').val();
    var NumRooms = $('#selNumRoom').find(":selected").val();
    var TotalPax = 0;
    var City = $('#hdcitycode').val();
    //var loctype = $('#hdloctype').val();


    if (CheckinD == "" && CheckoutD == "") {
        alertify.alert('Validation', 'Please fill checkin date and checkout date!');
        return false;
    }

    for (var irt = 1; irt <= parseInt(NumRooms); irt++) {
        TotalPax += parseInt($('#r' + irt + 'ADTCount').find(":selected").val()) + parseInt($('#Room_' + irt + '_CHDCount').find(":selected").val());
    }

    if (City == "NA" && CheckinD == "" && CheckoutD == "") {
        alertify.alert('Validation', 'Please select a city,checkin and checkout dates!');
        return false;
    }

    if (cityname.trim() == "") {
        alertify.alert('Validation', 'Please select a city!');
        return false;
    }

    if (City == "NA") {
        alertify.alert('Validation', 'Please select a city!');
        return false;
    }

    if (NumNights > 90) {
        alertify.alert('Validation', 'Maximum nights allowed in a single booking is 90!');
        return false;
    }

    if (NumNights == "") {
        alertify.alert('Validation', 'Number of nights must not be blank!');
        return false;
    }

    if (NumRooms == "") {
        alertify.alert('Validation', 'Number of rooms must not be blank!');
        return false;
    }

    if (TotalPax > 9) {
        alertify.alert('Validation', 'Maximum Passenger in a single booking is 9!');
        return false;
    }

    if (City != "NA" && NumRooms != "" && NumNights != "" && cityname != "") {
        window.localStorage.setItem("totpax", TotalPax);
        window.localStorage.setItem("cityname", $('#txtCity').val());
        window.localStorage.setItem("nation", Nation);
        window.localStorage.setItem("countryofresidence", countryofresidence);

        var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        if (format.test($('#txtHotelName').val()) == true) {
            alertify.alert('Validation', 'Avoid special characters!');
            return false;
        }
        var Rooms = '';
        for (var inr = 1; inr <= parseInt(NumRooms); inr++) {
            var CHDCount = parseInt($('#Room_' + inr + '_CHDCount').find(":selected").val());
            Rooms += '&room' + inr + '=ADT_' + $('#r' + inr + 'ADTCount').find(":selected").val() +
                ',CHD_' + CHDCount + '';
            for (var inr2 = 1; inr2 <= CHDCount; inr2++) {
                Rooms += '_' + $('#r' + inr + 'CHD' + inr2 + 'Age').find(":selected").val() + '';
            }
        }

        window.location.href = "hotels.html?nat=" + Nation + "&countryofres=" + countryofresidence + "&city=" + City + "&cin=" + CheckinD + "&cout="
            + CheckoutD + "&night=" + NumNights + "&numroom=" + NumRooms + "" + Rooms + "&sort=price-a" + "&suplr=" + Supplier + "";

    }
}
function getAreaName(AreaCode) {
    var AirportName = _.where(AreaCityList, { C: AreaCode });
    if (AirportName == "") {
        AirportName = AreaCode;
    }
    else {
        AirportName = AirportName[0].label;
    }
    return AirportName;
}
//=============================modify search end==========================================//
//--Quotation roome check srf--//

function RoomCheckChanged(room) {
    if ($(room).prop('checked')) {
        var HotelID;
        var id = $(room).attr("id");
        var hid = $(room).attr("data-hid");
        var Htlname = $(room).attr("data-hotelName");
        //var img = $(room).attr("data-img");
        var img = $(room).closest(".search_details").find("img").attr("src");
        var price = parseFloat($(room).attr("data-price"));
        var RoomId = $(room).attr("data-RoomID");
        var Meals = $(room).attr("data-meals");
        var sharebed = Boolean($(room).attr("data-sharebed"));
        var Hcode = $(room).attr("data-hotelCode");
        var address = $(room).attr("data-address");
        var type = $(room).attr("data-type");
        var star = $(room).attr("data-star");
        var Suplier = $(room).attr("data-supplier");
        var Roomstatus = $(room).attr("data-rmstaus");
        var Paxes = [];
        var Adt = [];
        var children = [];
        var numroomD = GetParameterValues('numroom');
        var roomsD = '';

        for (var irt = 1; irt <= parseInt(numroomD); irt++) {
            if (roomsD == '') {
                roomsD += GetParameterValues('room' + irt + '');
            }
            else {
                roomsD += '/' + GetParameterValues('room' + irt + '');
            }
        }

        var clickedChkBox = $(room).attr("id").split('_')[2];
        Paxes = roomsD.split('/');
        //["ADT_1,CHD_0", "ADT_1,CHD_0"]
        var temp = Paxes[clickedChkBox].split(',');
        //[ADT_1,CHD_0]
        for (var j = 0; j < temp.length; j++) {
            var temp1 = temp[j].split('_');
            //[ADT, 1]
            if (temp1[0] == "ADT") {
                //console.log(temp1[1]);
                Adt.push(temp1[1]);
            }

            if (temp1[0] == "CHD" && temp1[1] != "0") {
                for (var sd = 0; sd < parseInt(temp1[1]); sd++) {
                    children.push({ "serialNumber": (sd + 1), "age": parseInt(temp1[(1 + (sd + 1))]) });
                }

            }
        }
        //console.log(JSON.stringify(children));
        var Hcat = $('#hotcat_' + hid).text();
        var Hdes = $('#hotReports_' + hid + ' > p').text();
        var Hemail = $('#hotEmail_' + hid).html();
        var Htelphone = $('#hotTelephone_' + hid).html();
        var indate = moment(cin, "DD/MM/YYYY").format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
        var outdate = moment(cout, "DD/MM/YYYY").format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
        var today = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
        var nonight = night;
        var pernight = price / nonight;
        var obj = Cart.find(function (o) { return o.hotelItem.hotelCode == Hcode; });
        //console.log(obj);
        if (obj != null) {
            HotelID = obj.hotelItem.id;
            var objroom = obj.hotelItem.rooms;

            var found = objroom.find(function (oj) { return oj.roomQuotes[0].roomCode === RoomId });
            //console.log(found);
        }
        else {
            HotelID = null;
        }

        var qutastrig = '{"quote": {"quoteHotelItemList": [' +
            '{"serialNumber": 1,"hotelItem": {"rooms": [' +
            '{"roomQuotes": [' +
            '{"amendmentPolicy": [' +
            '{' +
            ' "fromDate": "2017-04-06T08:36:12.000+0000",' +
            ' "toDate": "2017-04-06T08:36:12.000+0000",' +
            '"totalCost": 100,' +
            '"remarks": null' +
            '}' +
            '],' +
            '"cancellationPolicy": [' +
            '{' +
            '"fromDate": "2017-04-06T08:36:12.000+0000",' +
            '"toDate": "2017-04-06T08:36:12.000+0000",' +
            ' "totalCost": 100,' +
            '"remarks": null' +
            '}' +
            '],' +
            '"children": ' + JSON.stringify(children) +
            ',"serialNumber": 1,' +
            //'"roomCode":"' + RoomId+'",'+
            '"date":"' + today + '" ,' +
            '"numberOfAdults":' + Adt[0] + ' ,' +
            '"status": "' + Roomstatus + '",' +
            '"checkInDate": "' + indate + '",' +
            '"checkOutDate": "' + outdate + '",' +
            '"nights": ' + nonight + ',' +
            '"costPerNight": ' + pernight + ',' +
            '"totalCost": ' + price +
            '}' +

            '],' +
            '"roomTypeId": 1,' +
            '"category": "' + Hcat.substring(0, 300) + ".." + '",' +
            '"meal": "' + Meals + '",' +
            '"isExtraBed": ' + sharebed + ',' +
            '"isBabyCot": false,' +
            '"supplierRoomType": "' + type + '",' +
            '"roomDetail": null' +
            '}' +
            ' ],' +
            '"id":' + HotelID + ',' +
            '"supplierCode": "' + Suplier + '",' +
            '"hotelCode":"' + Hcode + '",' +
            '"description":' + JSON.stringify(Hdes) + ',' +
            '"thumbNailImageUrl":"' + img + '",' +
            '"imageUrl":"' + img + '",' +
            ' "name": "' + Htlname + '",' +
            '"address":"' + address + '",' +
            '"cityCode": "' + city + '",' +
            '"telephone": "' + Htelphone + '",' +
            '"starCategory": ' + star + ',' +
            '"payableBy": "HOTELBEDS SPAIN, S.L.U",' +
            '"emergencyNumber": "270",' +
            '"minimumNights": 1,' +
            '"tariffRemarks": "wwwww"' +
            '}}]}}';
        var lines = qutastrig.split('\n');
        var quostng = '';
        for (var i = 0; i < lines.length; i++) {
            quostng += lines[i];
        }
        var dataquery = JSON.parse(quostng);
        var Data = {
            Request: JSON.stringify(dataquery)
        };
        //console.log(JSON.stringify(dataquery));
        $(".lds-css").show();
        $(".bckgrnd").show();

        ajaxHelper(Quatationaddedsuccesfully, null, null, { url: "WebMethodDbHotel.aspx/AddQuatationtoHub", data: Data });
    }
}
function Quatationaddedsuccesfully(data) {
    if (data.d != "NORESULT") {
        //console.log(data.d);
        alertify.alert('Quotation!', 'Quotation Added Successfully!');
        Getallquatation();
    }
    else {
        alertify.alert('Quotation!', 'Unable to append the Quotation!');
    }
    $(".lds-css").hide();
    $(".bckgrnd").hide();
}
function Getallquatation() {

    ajaxHelper(GetQuatationsuccesfully, null, null, { url: "WebMethodDbHotel.aspx/GetAllQuatation", data: null });

}
function GetQuatationsuccesfully(response) {

    if (response.d != "NORESULT") {

        $('#quatul').html('');
        var QResult = JSON.parse(response.d);
        //console.log(QResult);
        Quateid = QResult.id;
        var quoteHotelItemList = QResult.quoteHotelItemList;
        Cart = quoteHotelItemList;
        //console.log(JSON.stringify(Cart));
        var Qul = "";
        for (var i = 0; i < quoteHotelItemList.length; i++) {

            var QHName = quoteHotelItemList[i].hotelItem.name;
            var QHId = quoteHotelItemList[i].hotelItem.id;
            var Qrooms = quoteHotelItemList[i].hotelItem.rooms;
            var address = quoteHotelItemList[i].hotelItem.address;
            var roomli = "";
            for (var j = 0; j < Qrooms.length; j++) {
                var QRName = Qrooms[j].supplierRoomType;
                var QRID = Qrooms[j].id;
                // roomli = roomli + '<li data-toggle="tooltip" title="'+Qrooms[j].meal+'">' + QRName + ' <i class="fa fa-times-circle" onclick="Removequta(\'Room\',' + QRID+')"></i></li>';
                roomli = roomli + '<li data-toggle="tooltip" title="">' + QRName + ' <i class="fa fa-times-circle" onclick="Removequta(\'Room\',' + QRID + ')"></i></li>';
            }
            Qul = Qul + '<li><span data-toggle="tooltip" title="' + address + '">' + QHName + '</span > <i  class="fa fa-times-circle" onclick="Removequta(\'Hotel\',' + QHId + ')"></i><ul>' + roomli + ' </ul></li >';

        }
        Bindquatationdesgin();
        $("#quatul").append(Qul);
    }
    else {
        Cart = [];
        $("#quatul").empty();
        $("#quatul").append('<li><span> No Item found</span></li>');
        Bindquatationdesgin();
    }
    $("#quotloadingoff").show();
    $("#quotloading").hide();
}
function Removequta(item, id) {
    var deletedata = "";
    if (item == "Hotel") {
        //console.log("delete hotel" + id);
        deletedata = "hotelItemId";
    }
    if (item == "Room") {
        //console.log("delete Room" + id);
        deletedata = "hotelRoomId";

    }


    var request = '{"' + deletedata + '": ' + id + '}';
    var qury = JSON.parse(request);
    var Data = {
        requst: JSON.stringify(qury)
    }
    $(".lds-css").show();
    $(".bckgrnd").show();
    ajaxHelper(RemoveQuatationsuccesfully, null, null, { url: "WebMethodDbHotel.aspx/RemoveQuatation", data: Data });

}
function RemoveQuatationsuccesfully(response) {
    if (response.d != "NORESULT") {
        alertify.alert('Quotation!', 'Quotation Removed Successfully!');
        //console.log(response.d);
        // setTimeout(function () {   //calls click event after a certain time
        Getallquatation();
        $(".lds-css").hide();
        $(".bckgrnd").hide();
        //  }, 1000);

    }
}
function search(nameKey, prop, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i][prop] === nameKey) {
            return myArray[i];
        }
    }
}
function Bindquatationdesgin() {
    if (Cart.length > 0) {
        var divdetails = '<div style="width:100%; margin: auto; margin - top:100px;">';
        $.each(Cart, function () {
            var Rooomtable = "";
            var arrprice = [];
            $.each(this.hotelItem.rooms, function () {
                var Qchl = 0;
                arrprice.push(this.roomQuotes[0].totalCost);
                var QInDate = this.roomQuotes[0].checkInDate;
                var QOutDate = this.roomQuotes[0].checkOutDate;
                var QcheckInDate = moment(QInDate, "YYYY-MM-DDTHH:mm:ss.SSSZZ").format("MMM DD");
                var QcheckOutDate = moment(QOutDate, "YYYY-MM-DDTHH:mm:ss.SSSZZ").format("MMM DD");
                if (this.roomQuotes[0].hasOwnProperty("children")) {
                    Qchl = this.roomQuotes[0].children.length;
                }
                var mealinfo = (this.meal != "undefined" && !!this.meal && this.meal != "null") ? this.meal : 'Information not available';
                var rTypeInfo = (this.supplierRoomType != "undefined" && !!this.supplierRoomType && this.supplierRoomType != "null") ? this.supplierRoomType : 'Information not available';
                var rQInfo = (this.roomQuotes[0].status != "undefined" && !!this.roomQuotes[0].status && this.roomQuotes[0].status != "null") ? this.roomQuotes[0].status : 'Information not available';

                Rooomtable = Rooomtable + '<tr style="page-break-inside: avoid"><td style= "font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171; padding: 6px; font-weight: normal;" > ' + rTypeInfo + '</td >' +
                    '<td style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171;  font-weight: normal;">' + rQInfo + '</td>' +
                    '<td style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171;  font-weight: normal;">' + QcheckInDate + '-' + QcheckOutDate + ' (' + this.roomQuotes[0].nights + ' Nights)</td>' +
                    '<td style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171;  font-weight: 500;">AED ' + this.roomQuotes[0].totalCost.toFixed(2) + '(' + this.roomQuotes[0].costPerNight.toFixed(2) + '/night)</td>' +
                    '<td style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171;  font-weight: normal;">Adults:' + this.roomQuotes[0].numberOfAdults + ', Child:' + Qchl + '</td>' +
                    '<td style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #717171;  font-weight: normal;">' + mealinfo + '</td>' +
                    '</tr>';
            });
            var qroommprice = Math.min.apply(Math, arrprice);


            var rDescInfo = (this.hotelItem.description != "undefined" && !!this.hotelItem.description && this.hotelItem.description != "null") ? this.hotelItem.description : 'Information not available';
            divdetails = divdetails + '<div style="width:96%;background:#FFF;padding:2%;float:left;border-bottom: 1px dotted #000;page-break-inside: avoid;">' +
                // '<div style="width:100%;font-family: \'Open Sans\', sans-serif; font-size: 13px; color: #2c96d1; padding: 6px;font-weight: 700;border-bottom: 1px dashed #d2d2d2;padding: 4px 0px 4px 0px;margin - bottom: 7px;">OPTION 1</div>'+
                '<div style="width:222px;float:left;margin-right:20px;margin-bottom: 9px;"><img src="' + this.hotelItem.imageUrl + '" alt="hotel" style="width:100%;" /></div>' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 17px; color: #565454; font-weight: 700;">' + this.hotelItem.name + '</div>' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 21px; color: #2c96d1; font-weight: 700;margin-top: 3px; margin-bottom: 3px;">AED &nbsp;<div style="color:#dd0015;display: -webkit-inline-box;"> ' + qroommprice + '</div></div>' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 15px; color: #565454;margin-bottom: 4px;">Hotel rating : <div style="color:#2c96d1;display: -webkit-inline-box;">&nbsp;' + this.hotelItem.starCategory + ' star</div></div>' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 15px; color: #565454;">Address : <div style="color:#2c96d1;display: -webkit-inline-box;">&nbsp;' + this.hotelItem.address + '</div></div>' +
                '<div style="width:100%;float:left;page-break-inside: avoid;">' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 16px; color: #2c96d1; font-weight: 700;border-bottom:1px dashed #d2d2d2;padding: 4px 0px 4px 0px;margin-bottom:7px;">Description</div>' +
                '<div style="font-family: \'Open Sans\', sans-serif; font-size: 13px; color: #717171; text-align: justify; line-height: 18px"> ' + this.hotelItem.description + '</div>' +
                '</div>' +
                '<div style="width:100%;float:left;padding-top:20px;page-break-inside: avoid">' +
                '<table style="width:100%; border-collapse:collapse;font-family:\'Open Sans\', sans-serif;" cellpadding="5" cellspacing="0" border="1" bordercolor="#00739c;">' +
                '<tr style="page-break-inside: avoid">' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Room Type</th>' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Room Status</th>' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Check in Date and Check out Date ( Nights )</th>' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Cost</th>' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Pax</th>' +
                '<th style="font-family: \'Open Sans\', sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Meals Type</th>' +
                '</tr>' + Rooomtable + '</table > ' +
                '</div></div>';


        });
        divdetails = divdetails + '</div>';
        CKEDITOR.instances['editor1'].setData(divdetails);
    }
}
function saveSendQuote(qsubject, email, ccmail) {
    var body = CKEDITOR.instances['editor1'].getData();
    var senddate = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
    var Datas = {
        quoteId: Quateid,
        mailFrom: "",
        mailTo: email,
        mailCC: ccmail,
        subject: qsubject,
        body: body,
        sendDate: senddate
    };
    var Data = {
        Request: JSON.stringify(Datas)
    };

    ajaxHelper(saveSendQuotesuccesfylly, null, null, { url: "WebMethodDbHotel.aspx/saveSendQuote", data: Data });

}
function saveSendQuotesuccesfylly(response) {
    //console.log(response.d);
}
function Removeallquataions() {
    var request = '{"quoteId": ' + Quateid + '}';
    var qury = JSON.parse(request);
    var Data = {
        requst: JSON.stringify(qury)
    }

    ajaxHelper(RemoveQuatationsuccesfully, null, null, { url: "WebMethodDbHotel.aspx/RemoveQuatation", data: Data });

}
//new end

$(document).ready(function () {
    var headerInit = "";
    var dataUrl = "";
    $(".chksend").click(function () {
        $("#CartPDF").attr("data-open", "0");
        $("#QuatationPopup").modal('show');
        headerInit = CKEDITOR.instances['editor1'].getData();

    });
    $(".clearall").click(function () {
        alertify.confirm('Clear', 'Are you sure you want to clear all quotation?',
            function () {
                $(".lds-css").show();
                $(".bckgrnd").show();
                Removeallquataions();
            },
            function () {
            });


    });

    $("#sendquatation").click(function () {
        var desc = CKEDITOR.instances['editor1'].getData();
        var qsubject = $("#txtqtsubject").val();
        if ($("#txtqtsubject").val() == "") {
            alertify.alert("Validation", "Please enter the mail subject!");
            $("#txtqtsubject").focus();
            return false;
        }
        var email = $("#txtsendqtnemail").val();
        if (email == "") {

            alertify.alert("Validation", "Please enter the mail address!");
            $("#txtsendqtnemail").focus();
            return false;
        }
        var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var emaillist = email.split(';');
        for (var i = 0; i < emaillist.length; i++) {
            if (emaillist[i] != '') {
                var matchArray = emaillist[i].match(emailPat);
                if (matchArray == null) {

                    alertify.alert("Validation", "Your email address seems incorrect. Please try again.");
                    $("#txtsendqtnemail").focus();
                    return false;
                }
            }
        }
        var ccmail = $("#txtqtcc").val();
        if ($("#txtqtcc").val() == "") {
            ccmail = "NoData";
        }

        else {
            var ccemaillist = ccmail.split(';');
            for (var i = 0; i < ccemaillist.length; i++) {
                if (ccemaillist[i] != '') {
                    var matchArray = ccemaillist[i].match(emailPat);
                    if (matchArray == null) {

                        alertify.alert("Validation", "Your Cc email address seems incorrect. Please try again.");
                        $("#txtqtcc").focus();
                        return false;
                    }
                }
            }
        }
        var bccmail = $("#txtqtbcc").val();
        if ($("#txtqtbcc").val() == "") {
            bccmail = "NoData";
        }

        else {
            var bccemaillist = bccmail.split(';');
            for (var i = 0; i < bccemaillist.length; i++) {
                if (bccemaillist[i] != '') {
                    var matchArray = bccemaillist[i].match(emailPat);
                    if (matchArray == null) {

                        alertify.alert("Validation", "Your Bcc email address seems incorrect. Please try again.");
                        $("#txtqtbcc").focus();
                        return false;
                    }
                }
            }
        }

        var lastChar = email.slice(-1);
        if (lastChar == ';') {
            email = email.slice(0, -1);
        }
        var lastChar1 = ccmail.slice(-1);
        if (lastChar1 == ';') {
            ccmail = ccmail.slice(0, -1);
        }
        var lastChar2 = bccmail.slice(-1);
        if (lastChar2 == ';') {
            bccmail = bccmail.slice(0, -1);
        }

        $("#sendquatation").attr("disabled", true);
        $(".loadbtn").css("display", "block");
        var name = Setup.Agency.AgencyShortName;// $('#txtsendqtnname').val();
        var contact = Setup.Agency.Helpline;//$('#txtsendqtncont').val();
        var Type = 2;
        var userID = 3;
        var qtdata = {
            Subject: qsubject,
            UserMailID: email,
            CClist: ccmail,
            Bcclist: bccmail,
            SType: Type,
            SelectQtnDiv: desc,
            UID: userID
        }

        $(".lds-css").show();
        $(".bckgrnd").show();

        ajaxHelper(Quatsuccess, null, null, { url: "WebMethodsDB.aspx/SendQtationMail", data: qtdata });


        function Quatsuccess(response) {
            saveSendQuote(qsubject, email, ccmail);
            $('.htlcheckbox').removeAttr('checked');
            $('.htlRoomCHKActive').removeAttr('checked');
            $('.htlRoomCHKActive').attr('data-active', "0");
            $('#txtsendqtnemail').val("");
            $('#txtqtsubject').val("");
            $('#txtqtcc').val("");
            $('#txtqtbcc').val("");
            $('#QuatationPopup').modal('hide');
            $('#sendquatation').removeAttr("disabled");
            $(".lds-css").hide();
            $(".bckgrnd").hide();
            alertify.alert('Quotation Sent Successfully', 'Kindly check your email for the quotation.');
            Getallquatation();
        }

    });

    $("#CartPDF").click(function () {
        var header = CKEDITOR.instances['editor1'].getData();

        var jsonData = {
            Data: header
        }

        var errorFunc = function (errorText) {
            alertify.alert("Error", "Please try to download again.");
        };

        function download(data) {
            //$("#CartPDF").text("Download PDF");
            //$("#CartPDF").attr("href", data);
            //$("#CartPDF").attr("download", "Hotel Quotation");
            $("#CartPDF").attr("data-open", "1");
            dataUrl = data.rturnurl;
            $(".lds-css").hide();
            $(".bckgrnd").hide();
            window.open(data.rturnurl, "_blank");


        }
        $(".lds-css").show();
        $(".bckgrnd").show();
        if ($("#CartPDF").attr("data-open") == "0") {

            ajaxHelper(download, null, errorFunc, { url: "PdfDownload.ashx", data: jsonData });

        } else {
            if (headerInit == header) {
                $(".lds-css").hide();
                $(".bckgrnd").hide();
                window.open(dataUrl, "_blank");
            } else {
                headerInit = header;
                ajaxHelper(download, null, errorFunc, { url: "PdfDownload.ashx", data: jsonData });
            }
        }
    });
});
//srf end
//=============GetAgencyCodeStart=========================
/////////////////////////////////////////
function SessiongetAgencyCode(session) {
    isPaused = true;
    //   $(".loadingGIF").show();   

    function SessionSuccessdet(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            HotelAgencyCode = data.d[0];
            HotelFindURL = data.d[1];
            HotelDetailURL = data.d[2];
            HotelPrebookURL = data.d[3];
            HotelBookURL = data.d[4];
            HotelPrecancelURL = data.d[5];
            HotelCancelURL = data.d[6];
            HotelAmenitiesURL = data.d[7];
            HotelRoomAmenitiesURL = data.d[8];
            HotelFindMoreURL = data.d[9];
            HotelPOIFilterURL = data.d[10];
            window.localStorage.setItem("HotelAPI", JSON.stringify(data.d))
            //console.log(data.d);
            isPaused = false;
            GetHotelResults();
        }
    }

    ajaxHelper(SessionSuccessdet, null, null, { url: Setup.Session.Url.GetSessionDatadet, data: null });

}
//=============GetAgencyCodeEnd=========================
//=============GetAgencyCodeStart=========================
/////////////////////////////////////////
function SessionGetSupplierlist() {
    isPaused = true;
    //   $(".loadingGIF").show();   

    function SessionSuccessdet(data) {
        if (data.d == 'NOSESSION') {
            //         window.location.href = "/index.html";
        }
        else {
            //HotelAgencyCode = data.d[0];
            //HotelFindURL = data.d[1];
            //HotelDetailURL = data.d[2];
            //HotelPrebookURL = data.d[3];
            //HotelBookURL = data.d[4];
            //HotelPrecancelURL = data.d[5];
            //HotelCancelURL = data.d[6];
            //HotelAmenitiesURL = data.d[7];
            //HotelRoomAmenitiesURL = data.d[8];
            //HotelFindMoreURL = data.d[9];
            //console.log(data.d);
            //isPaused = false;
            //GetHotelResults();
        }
    }

    ajaxHelper(SessionSuccessdet, null, null, { url: "/HOTEL/WebMethodsDB.aspx/GetSupplierlist", data: null });

}
//=============GetAgencyCodeEnd=========================

//======================================scroll start=========================================================//

//$(window).scroll(function () {
//    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
//        GetRecords();
//        $(".hotelcount").html('');
//    }
//});
function GetRecords() {


    if (hotelListFullStatus == 0 && pageIndex < pageCount) {
        currentIndex++;
        pageIndex++;
        var qstrings = {
            nation: nation,
            countryofresidence: countryofresidence,
            city: city,
            cin: cin,
            cout: cout,
            night: night,
            numroom: numroom,
            rooms: rooms,
            supp_code: findsuppliercode,
            s_curr: "USD",
            requeststatus: false,
            lang: "ENG",
            AgencyCode: HotelAgencyCode,
            index: currentIndex,
            token: accesstocken
        }
        $("#loader").show();

        var failureFunc = function (response) {
            hotelListFullStatus = 1;
            $("#loader").css("display", "none");
            pageCount++;
        };

        var errorFunc = function (response) {
            CurrSuppliercnt = CurrSuppliercnt + 1;
            $('#lblloadername').text("Fetching Results from supplier " + CurrSuppliercnt);
            BindResultsProgressBarTimerWithWidth(1, PBUpperLimit, true);
            //        progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(5,"' + PBUpperLimit + '",true)', 1000);   
            PBUpperLimit = PBUpperLimit + NewPBLimit;
            progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(0.25,' + PBUpperLimit + ',false)', 250);
            //progressBarTimer = BindResultsProgressBarTimerWithWidth(1, PBLimit1, true);
            if (remainingsuppliercode.length > 3) {
                for (i = 0; i < currentsuppliercode.length; i++) {
                    var indcurrentsuppliercode = currentsuppliercode[i];
                    remainingsuppliercode = remainingsuppliercode.replace("," + indcurrentsuppliercode, "");
                    remainingsuppliercode = remainingsuppliercode.replace(indcurrentsuppliercode + ",", "");
                    remainingsuppliercode = remainingsuppliercode.replace(indcurrentsuppliercode, "");
                    findsuppliercode = remainingsuppliercode;
                }
                if (remainingsuppliercode.length > 0) {
                    pageCount++;
                    currentIndex = parseFloat(0);
                    GetInitialRecords();
                }
                else {
                    hotelListFullStatus = 1;
                    $("#loader").css("display", "none");
                    pageCount++;
                    $(".filter-Progress").hide();
                    ShowCancellationChargeIcon();
                    GenerateStarFilter();
                    GenerateLocationFilter();
                    GenerateHotelNameFilter();
                    GenerateHotelAmenitiesFilter();
                    GenerateRoomAmenitiesFilter();
                    GeneratePOIFilter();
                    GenerateHotelPoiFilter();

                    // Filter full display start//
                    $('.resultprice_load').css('display', 'none');
                    $('.resultstar_load').css('display', 'none');
                    $('.currdisplay').css('display', 'block');
                    $('#SecFilterbyStar').css('display', 'block');
                    $('#SecDealFilterBox').css('display', 'block');
                    $('#DealXlChargesFilterBox').css('display', 'block');
                    $('#SecAvailableFilterBox').css('display', 'block');
                    $('#ulLocFilter').css('display', 'block');
                    $('#ulHaminitiFilter').css('display', 'block');
                    $('#ulHRoomaminitiFilter').css('display', 'block');
                }
            }
            else {
                remainingsuppliercode = "";
                hotelListFullStatus = 1;
                $("#loader").css("display", "none");
                pageCount++;
                $(".filter-Progress").hide();
                ShowCancellationChargeIcon();
                GenerateStarFilter();
                GenerateLocationFilter();
                GenerateHotelNameFilter();
                GenerateHotelAmenitiesFilter();
                GenerateRoomAmenitiesFilter();
                GeneratePOIFilter();
                GenerateHotelPoiFilter();

                // Filter full display start//
                $('.resultprice_load').css('display', 'none');
                $('.resultstar_load').css('display', 'none');
                $('.currdisplay').css('display', 'block');
                $('#SecFilterbyStar').css('display', 'block');
                $('#SecDealFilterBox').css('display', 'block');
                $('#DealXlChargesFilterBox').css('display', 'block');
                $('#SecAvailableFilterBox').css('display', 'block');
                $('#ulLocFilter').css('display', 'block');
                $('#ulHaminitiFilter').css('display', 'block');
                $('#ulHRoomaminitiFilter').css('display', 'block');
                // Filter full display end//
            }
        }

        function ShowRemainingHotelResults(data) {
            var resXML = data;
            if (resXML != 'NORESULT') {
                pageCount++;
                ShowHotelResults(data);
            }
            else {
                hotelListFullStatus = 1;
                $("#loader").css("display", "none");
                pageCount++;
            }
        }

        ajaxHelper(ShowRemainingHotelResults, failureFunc, errorFunc, { url: HotelFindURL, data: qstrings });

    }
}
function GetTimeout() {
    if (noResultAvailable == true) {
        alertify.alert('No Results', 'There are no results. Please try for another combination.', function () {
            window.location.href = "/HOTEL/searchhotel.html";
        });

    }
    else {
        remainingsuppliercode = "";
        hotelListFullStatus = 1;
        $("#loader").css("display", "none");
        pageCount++;
        $(".filter-Progress").hide();
        ShowCancellationChargeIcon();
        GenerateStarFilter();
        GenerateLocationFilter();
        GenerateHotelNameFilter();
        GenerateHotelAmenitiesFilter();
        GenerateRoomAmenitiesFilter();
        GeneratePOIFilter();
        GenerateHotelPoiFilter();

        // Filter full display start//
        progressBarTimer = window.setInterval('BindResultsProgressBarTimerWithWidth(1,100,false)', 50);
        $('.resultprice_load').css('display', 'none');
        $('.resultstar_load').css('display', 'none');
        $('.currdisplay').css('display', 'block');
        $('#SecFilterbyStar').css('display', 'block');
        $('#SecDealFilterBox').css('display', 'block');
        $('#DealXlChargesFilterBox').css('display', 'block');
        $('#SecAvailableFilterBox').css('display', 'block');
        $('#ulLocFilter').css('display', 'block');
        $('#ulHaminitiFilter').css('display', 'block');
        $('#ulHRoomaminitiFilter').css('display', 'block');
        BindResultsProgressBarTimerWithWidth(1, 100, true); //ok

    }
    // Filter full display end//
}
function GetInitialRecords() {
    if (hotelListFullStatus == 0 && pageIndex < pageCount) {
        //    pageIndex++;
        var qstrings = {
            nation: nation,
            countryofresidence: countryofresidence,
            city: city,
            cin: cin,
            cout: cout,
            night: night,
            numroom: numroom,
            rooms: rooms,
            supp_code: findsuppliercode,
            s_curr: "USD",
            lang: "ENG",
            AgencyCode: HotelAgencyCode,
            requeststatus: false,
            index: 0,
            token: accesstocken

        }
        $("#loader").show();

        var failureFunc = function (response) {
            hotelListFullStatus = 1;
            $("#loader").css("display", "none");
            pageCount++;
        };


        var errorFunc = function (response) {
            if (response.Response == 'REQUESTSEND') {
                setTimeout(function () { GetInitialRecords() }, 100);
            }
            else {
                hotelListFullStatus = 1;
                $("#loader").css("display", "none");
                pageCount++;
            }
        };

        function ShowInitialHotelResults(data) {
            var resXML = data;
            if (resXML != 'NORESULT' && resXML != 'REQUESTED' && resXML.Response != "REQUESTSEND") {
                ShowHotelResults(data);
                $("#loader").css("display", "none");
                pageCount++;
            }
            else {
                if (timoutset == false) {
                    timoutset = true;
                    setTimeout(function () { GetTimeout() }, 40000);
                }
                setTimeout(function () { GetInitialRecords() }, 100);
            }
        }

        ajaxHelper(ShowInitialHotelResults, failureFunc, errorFunc, { url: HotelFindURL, data: qstrings });

    }
}


//=====================================scroll end==========================================================//
/////////////////////////////////////////
/////////////////////////////////////////
//========slide left========//

function directionviewfrom(thiss) {
    directionbindFrom = [];
    $("#dirctionfrm").val('')
    $(".get_direction").slideDown("slow");
    var hotelname = $(thiss).attr("data_hotelname");
    var lattitude = $(thiss).attr("data_latt");
    var longitude = $(thiss).attr("data_longi");
    var fullloc = lattitude + ',' + longitude;
    $("#dirctionfrm").val(hotelname)
    directionbindFrom.push(fullloc);
}

function directionviewto(thiss) {
    directionbindTo = [];
    $("#dirctionto").val('')
    $(".get_direction").slideDown("slow");
    var hotelname = $(thiss).attr("data_hotelname");
    var lattitude = $(thiss).attr("data_latt");
    var longitude = $(thiss).attr("data_longi");
    var fullloc = lattitude + ',' + longitude;
    $("#dirctionto").val(hotelname)
    directionbindTo.push(fullloc);
}
//========slide left close========//
//===================================Getdirection start==============================================//
function Getdirection() {

    var directionsDisplay = new google.maps.DirectionsRenderer();
    var directionsService = new google.maps.DirectionsService;
    var geolocstart = directionbindFrom[0].split(",");
    var geolocend = directionbindTo[0].split(",");
    var start = geolocstart[0] + "," + geolocstart[1];
    var end = geolocend[0] + "," + geolocend[1];

    var mapOptions = {
        zoom: 14,
        center: start
    }
    var selectedMode = $('input[name=radio-btns]:checked').val();

    $("#idgmapprint").css("display", "block");
    $("#idgmapview").css("display", "block");
    $("#idmapfilter").css("display", "none");
    $("#getmaplistinfo").html("");
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('getmaplistinfo'));

    var request = {
        origin: start,
        destination: end,
        // Note that Javascript allows us to access the constant
        // using square brackets and a string value as its
        // "property."
        travelMode: google.maps.TravelMode[selectedMode]
    };
    directionsService.route(request, function (response, status) {
        if (status == 'OK') {
            directionsDisplay.setDirections(response);
        }
    });


}
//===================================Getdirection end================================================//
//================================================PrintMapDivStart=====================================//
function PrintGmapdiv() {
    const $body = $('body');
    const $mapContainer = $('.map-container');
    var divToPrint = document.getElementById('getmaplistinfo');
    const $mapContainerParent = $mapContainer.parent();
    const $printContainer = $('<div style="position:relative;">' + divToPrint.innerHTML);

    $printContainer
        .height($mapContainer.height())
        .append($mapContainer)
        .prependTo($body);

    const $content = $body
        .children()
        .not($printContainer)
        .not('script')
        .detach();

    /**
     * Needed for those who use Bootstrap 3.x, because some of
     * its `@media print` styles ain't play nicely when printing.
     */
    const $patchedStyle = $('<style media="print">')
        .text('img { max-width: none !important; } a[href]:after { content: ""; }')
        .appendTo('head');

    window.print();

    $body.prepend($content);
    $mapContainerParent.prepend($mapContainer);

    $printContainer.remove();
    $patchedStyle.remove();

}
//================================================PrintMapDivEnd=====================================//
//===============================================GlobeViewStart=========================================================//
function GlobeGmapdiv() {
    $(".point_int").show('');
    $(".hotel_map_list").hide('');
    $("#ulairports").html("");
    $("#ulloality").html("");
    $("#ulshoppingmalls").html("");
    $("#ulpremise").html("");
    var map;
    var service;
    var infowindow;
    var locations = ['airport', '(cities)', 'shopping_mall', 'natural_feature', 'administrative_area_level_1'
    ]
    for (var j = 0; j < locations.length; j++) {

        var pyrmont = new google.maps.LatLng(publicgeocodeatti, publicgeocodelongi);

        map = new google.maps.Map(document.getElementById('mapdummy'), {
            center: pyrmont,
            zoom: 15
        });

        var request = {
            location: pyrmont,
            radius: '50000',
            type: [locations[j]]
            //type: 'airport'//'point_of_interest'
        };

        service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, callback);

    }
}

function callback(results, status) {
    var flag = 0;
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            var place = results[i];
            //console.log(place);
            var airportlist = '';
            var locality = '';
            var shoppingmalls = '';
            var Islands = '';
            var premise = '';
            if (place.types["0"] == 'airport' && flag == 0) {
                var name = place.name;
                //console.log(name);
                flag = 1;
                airportlist = '<li><a>' + name + '</a></li>';
                $("#ulairports").append(airportlist);
            }
            if (place.types["0"] == 'locality') {
                var name = place.name;
                //console.log(name);

                locality = '<li><a>' + name + '</a></li>';
                $("#ulloality").append(locality);
            }
            if (place.types["0"] == 'shopping_mall') {
                var name = place.name;
                //console.log(name);

                shoppingmalls = '<li><a>' + name + '</a></li>';
                $("#ulshoppingmalls").append(shoppingmalls);
            }
            if (place.types["0"] == 'premise') {
                var name = place.name;
                //console.log(name);

                premise = '<li><a>' + name + '</a></li>';
                $("#ulpremise").append(premise);
            }

            if (place.types["0"] == 'natural_feature') {
                var name = place.name;
                //console.log(name);

                Islands = '<li><a>' + name + '</a></li>';
                $("#ulIslands").append(Islands);
            }


        }

    }

}
//================================================GlobeViewEnd========================================================//


function SessionCheckDetails(session) {
    //   $(".loadingGIF").show();
    var Dastasseq = {
        sesname: session
    };

    function SessionCheckingSuccess(data) {

        if ("Access_Token" in data.d) {
            if (data.d.Access_Token == 'NOSESSION') {
                window.location.href = "/index.html";
            }
            else {
                accesstocken = data.d.Access_Token;
                //console.log(data.d.Access_Token);
                waitforaccesstocken = false;
                window.localStorage.setItem("accesstocken", accesstocken);
            }
        }

        if ("Loginuser" in data.d) {
            if (data.d.Loginuser == 'NOSESSION') {
                window.location.href = "/index.html";
            }
            else {
                var logininfo = $.parseJSON(data.d.Loginuser);
                var agcode = logininfo.user.loginNode.code;
                var agycodesplit = agcode.match(/\d+/);
                var Agycode = agycodesplit[0];
                window.localStorage.setItem("Agycode", Agycode);
                //console.log(logininfo);
                $('.username').html(logininfo.user.firstName);
                $('.AgencyName').html(logininfo.user.loginNode.name);

            }

            //if (data.d.UserDetails == 'NOSESSION') {
            //    window.location.href = "/index.html";
            //}
            //else {
            //    var userdata = $.parseJSON(data.d.UserDetails);
            //    console.log(userdata);
            //    $('.username').html(userdata[0].FirstName);
            //    $('.AgencyName').html(userdata[0].AgencyName);
            //    AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            //    if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            //        $('.CreditLimitAmount').hide();
            //        $('.lesscreditlimit').hide();
            //        $('.nocreditlimit').hide();
            //    }
            //    GetMarkupDataa('MarkupDataHotel');
            //}
        }
    }

    ajaxHelper(SessionCheckingSuccess, null, null, { url: Setup.Session.Url.GetSessionDataNew, data: Dastasseq });

}