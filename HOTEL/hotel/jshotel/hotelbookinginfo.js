﻿var Paxname = '';
var hotelId = '';
var MarkupValue = parseFloat(0);
var markupType = 1;
var CancellationGPeriod = parseInt(0);
var EmailID = '';
var EmailId1 = '';
//var EUName = '';
var ETripID = '';
var ETripName = '';
var EPaxData = '';
var grandtotal = '';
var APIid = '';
var Rateofchange = parseFloat(1);
var cancelAmount = '';
var CancelCurrency = '';
var CustomerAmount = '';
var Statuscancellation = '';
var Time = '';
var CheckinDate = '';
var cancelpriceamount = '';
var CancelNetAmount = '';
var checkval = '';
var Cstatus = '';
//var AgencyName = '';
var AgencyAddress = 'Albadie';
var AgencyContact = '+3645445958';
var gettoday = '';
var voucherbookingdate = '';
var HotelAddress = '';
var chin = '';
var chout = '';
var VoucherPax = '';
var RoleUpdate = -1;
var RoleVoucher = -1;
var RoleCancel = -1;
var RoleBookingHis = -1;
var RoleNetcostbrkup = -1;
var Voucherdet = '';
var GtaAotNumber = 'Not Available';
var updatestatus = '';
var Status = '';
var SupplierCode = '';
var JacClientID = '';
var BookingReferenceID = '';
var HotelAgencyCode = '';
var HotelFindURL = '';
var HotelDetailURL = '';
var HotelPrebookURL = '';
var HotelBookURL = '';
var HotelPrecancelURL = '';
var HotelCancelURL = '';
var HotelAmenitiesURL = '';
var HotelRoomAmenitiesURL = '';
var AgencyCurrencyCode = '';
var AgencyCurrencyExRate = '';
var HotelPaxData = '';
var HotelbookinfoData = '';
var supprf = '';
var accesstocken = '';

$(document).ready(function () {

    var loginUserDetails = {
        userSession: {
            accessToken: "Access_Token",
            //   userDetails: "UserDetails",
            loginUser: "Loginuser"
        }
    };

    SessiongetAgencyCode("SaveAgencyCode");
    SessionCheckDetails(JSON.stringify(loginUserDetails));

    //SessionCheckingaccesstocken('Access_Token');
    //SessionCheckingLoginuser('Loginuser');

    AgencyCurrencyCode = window.localStorage.getItem("AgencyCurrencyCode");
    AgencyCurrencyExRate = window.localStorage.getItem("AgencyCurrencyExRate");
    Rateofchange = parseFloat(AgencyCurrencyExRate);
    var FrmHotelPax = window.localStorage.getItem("HotelPax");
    SupplierCode = window.localStorage.getItem("SupplierCode");
    //SessiongetAgencyCode("SaveAgencyCode");
    //SessionChecking('UserDetails');
    $("#divbuttonappend").html(' ');
    //if (FrmHotelPax == "true") {
    //  Reconfirmed();
    //}
    Bookinfo();

    if (FrmHotelPax == "true") {
        //  Reconfirmed();
        window.localStorage.setItem("HotelPax", "false");
        setTimeout(function () {
            var bstatus = "confirmed"
            SendMailBookingInfo(Status);
        }, 3000);
    }

});
//Check user login
function SessionChecking(session) {

    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SessionSuccess(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var userdata = $.parseJSON(data.d);
            $('.username').html(userdata[0].FirstName);
            $('.AgencyName').html(userdata[0].AgencyName);
            EUName = userdata[0].FirstName;
            EmailID = userdata[0].EmailID;
            AgencyName = userdata[0].AgencyName;
            AgencyAddress = userdata[0].Address;
            AgencyContact = userdata[0].Contact;
            gettoday = new Date();
            gettoday = moment(gettoday).format("DD MMM YYYY")
            var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
            }
            // GetMarkupDataa('MarkupDataHotel');
            // GetmarkupBookinfo();
        }
    }
}
function Bookinfo() {
    $.ajax({
        type: "POST",
        //url: "WebMethodDbHotel.aspx/BookHotelinfo",
        url: "../Common.aspx/BookHotelinfonew",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: BookHotelResults,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BookHotelResults(data) {
    HotelbookinfoData = JSON.parse(data.d);
    if (HotelbookinfoData == undefined || HotelbookinfoData == null) {

        window.location.href = "/HOTEL/searchhotel.html";
    }
    else {

        ETripName = (HotelbookinfoData.booking.hotels["0"].name).toUpperCase();

        var BookingDate = HotelbookinfoData.booking.bookDate;
        CheckinDate = HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].checkInDate;
        Cstatus = HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].status;
        Time = moment(HotelbookinfoData.booking.deadlineDate).subtract(CancellationGPeriod, 'day');

        Status = HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].status;
        var CheckOut = HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].checkOutDate;
        HotelAddress = HotelbookinfoData.booking.hotels["0"].address;
        ETripID = HotelbookinfoData.booking.refId;
        Rateofchange = parseFloat(AgencyCurrencyExRate);
        $('#StayIn').html('YOUR BOOKING AT ' + (HotelbookinfoData.booking.hotels["0"].name).toUpperCase() + ' HAS BEEN ' + Status.toUpperCase());
        BookingReferenceID = HotelbookinfoData.booking.refId;

        GtaAotNumber = "61 - 2 - 89058770";
        if (GtaAotNumber == undefined) {
            GtaAotNumber = 'Not Available';
        }
        voucherbookingdate = moment(BookingDate).format("DD MMM YYYY");
        $("#BookedOn").html('Booked on ' + (moment(BookingDate).format("DD MMM YYYY")));
        //   $('#RefernceNo').html(HotelbookinfoData[0].BookingRefID);
        $('#RefernceNo').html(HotelbookinfoData.booking.refId);
        document.title = '' + travelagencyname + ' - Hotel booked at ' + (HotelbookinfoData.booking.hotels["0"].name).toLowerCase() + ' - ' + HotelbookinfoData.booking.refId;
        $("#BookingDate").html((moment(BookingDate).format("DD MMM YYYY")));
        $('#CheckinDate').html(moment(CheckinDate).format("DD MMM YYYY"));
        $('#Status').html(Status + '<span id="divhistory"></span><a><i id="history" data-open="0" onclick="GetHistory()" class="fa fa-plus-square" data-toggle="modal" data-target="#Status_History" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');
        //$('#Status').html(Status);
        GetStatus(Status);
        $('#CheckOut').html(moment(CheckOut).format("DD MMM YYYY"));
        var Checkin = moment(CheckinDate);
        var CheckOutt = moment(CheckOut);
        chin = moment(CheckinDate).format("DD MMM YYYY");
        chout = moment(CheckOut).format("DD MMM YYYY");
        gettoday = moment().format("DD MMM YYYY");
        var DifferenceMilli = CheckOutt - Checkin;
        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
        var Night = DifferenceMilliDuration.days();
        $('#Nights').html(Night);
        var totalprice = parseFloat(0);

        if (Time._d == 'Invalid Date') {
            $('#TimeLimit').html("Not Available");
        }
        else {
            $('#TimeLimit').html(moment(Time).format("DD MMM YYYY"));
        }
        $('#Accomedation').html(HotelbookinfoData.booking.hotels["0"].name);
        $('#divaddress').html(HotelAddress);
        $('#SupplyRef').html(HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].supplierReferenceNumber);
        $('#SuppCode').html(HotelbookinfoData.booking.supplierName);

        $('#Total').html((HotelbookinfoData.booking.totalCost).toFixed(3) + " " + HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].debitDetails.sellCurrency + '<span id="divFarebrkup"></span><a><i id="farebreakup" data-open="0" onclick="GetpriceBreak()" class="fa fa-plus-square" data-toggle="modal" data-target="#Price_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');
        grandtotal = (HotelbookinfoData.booking.totalCost).toFixed(3) + " " + HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].debitDetails.sellCurrency;
        APIid = BookingReferenceID;
        supprf = HotelbookinfoData.booking.hotels["0"].rooms["0"].roomQuotes["0"].supplierReferenceNumber;
    }
    if (RoleBookingHis == '0') {
        $("#history").remove();
    }
    if (RoleNetcostbrkup == '0') {
        $("#farebreakup").remove();
    }

    $('#ROOPAX').html('');
    EPaxData = '';
    VoucherPax = '';
    HotelPaxData = HotelbookinfoData.booking.hotels["0"].rooms;

    for (var iPax = 0; iPax < HotelbookinfoData.booking.hotels["0"].rooms.length; iPax++) {
        for (var iQ = 0; iQ < HotelbookinfoData.booking.hotels["0"].rooms[iPax].roomQuotes.length; iQ++) {


            Paxname = HotelPaxData[iPax].roomQuotes[iQ].guests["0"].firstName;
            var val = parseInt(iPax) + parseInt(1);
            var Passname = Paxname;
            var rooms = HotelPaxData[iPax].category;
            var roomcots = HotelPaxData[iPax].isBabyCot;
            var brkfst = HotelPaxData[iPax].meal;
            var Sharebed = HotelPaxData[iPax].isExtraBed;

            var vochurage = parseInt(0);
            for (var ichd = 0; ichd < HotelPaxData[iPax].roomQuotes[iQ].guests.length; ichd++) {
                var chdsplit = HotelPaxData[iPax].roomQuotes[iQ].guests[ichd].paxTypeCode;
                if (chdsplit == "CHD") {
                    vochurage = vochurage + 1;
                }
            }



            if (brkfst == "Breakfast,") {
                brkfst = "Full Breakfast";
            }
            var Adult = HotelPaxData[iPax].roomQuotes[iQ].numberOfAdults;

            var Child = vochurage;
            var Sharebedresult = '';
            if (Child != 0) {
                if (Sharebed == true) {
                    Sharebedresult = " (extra bed is not provided)";
                }
                else if (Sharebed == false) {
                    Sharebedresult = " (extra bed is provided)";
                }

            }
            var RoomId = HotelPaxData[iPax].roomTypeId;
            if (roomcots != 0) {
                var cotstodisplay = " ," + roomcots + " x Cot(s)";
                $("#divcots").show();
            }
            else {
                var cotstodisplay = "";
            }

            $('#ROOPAX').append('<tr><td>' + rooms + cotstodisplay + '</td> <td>' + Adult + '</td> <td>' + Child + '</td> <td>' + Passname + '<span id="divTotal"></span><a><i id="ifbreakup" data-open="0" onclick="GetPaxBreak(' + iPax + ',' + iQ + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#PAX_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td></tr>');
            EPaxData += '<tr><td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + rooms + cotstodisplay + '</td> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Adult + '</td> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px;text-align:center;">' + Child + '</td> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px;text-align:center;">' + Passname + '</td></tr>';
            VoucherPax += '<tr> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + val +
                '</td> <td colspan="2" style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + rooms +
                '</td> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Adult +
                '</td> <td style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Child +
                '</td> <td colspan="1" style="border: 1px solid #cecece; border-collapse: collapse; padding: 10px; text-align:center;">' + Passname +
                '</td> </tr>'


        }
    }


}

function GetStatus(status) {
    var Checkiffinished = false;
    var today = new Date();
    today = moment(today);
    var up = '0';
    var Chdatee = moment(CheckinDate);
    //  var c = 24 * 60 * 60 * 1000, diffDays = Math.round(Math.abs((Chdatee - today) / (c)));
    var diffDays = Math.floor((((Chdatee - today) % 31536000000) % 2628000000) / 86400000);
    if (diffDays <= 0) {
        $("#divbuttonappend").html(' ');
        Checkiffinished = true;
    }
    var CurrentDate = new Date();
    CurrentDate = moment(CurrentDate);
    var limit = moment(Time);
    var F = 24 * 60 * 60 * 1000
    var days = Math.floor((((limit - CurrentDate) % 31536000000) % 2628000000) / 86400000);
    var diff = Math.floor((((Chdatee - CurrentDate) % 31536000000) % 2628000000) / 86400000);
    if (days <= 0 && diff >= 0) {
        $('#Divcancellationalert').html("Please note: Booking is under cancellation period")
    }
    var Sat = "";
    if (status == "Confirmation pending") {
        Sat = "Confirmation pending";
        $("#divbuttonappend").html(' ');
        $("#divbuttonappend").append('<a id="btnUpdate" onclick="Update(' + up + ')"><i class="greatclass"></i>UPDATE</a>');
        if (Checkiffinished == false) {
            // $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
            $("#divbuttonappend").append('<a id="btnCancelPnr" onclick="CancelBooking()"><i class="newclass"></i>CANCEL</a>');
        }

    }
    else if (status == "Reconfirmed") {
        Sat = "Reconfirmed";
        $("#divbuttonappend").html(' ');
        if (Checkiffinished == false) {
            // $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
            $("#divbuttonappend").append('<a id="btnCancelPnr" onclick="CancelBooking()"><i class="newclass"></i>CANCEL</a>');
        }
        $("#divbuttonappend").append('<a id="btnDownloadVochuer" download data-open="0" onclick="DownloadVochuer()">DOWNLOAD VOUCHER</a>');
        $("#divbuttonappend").append('<a data-toggle="modal" data-target="#modalCancelMail" title="Email Voucher" id="btnEmailVochuer">EMAIL VOUCHER</a>');

    }
    else if (status == "Booking Success/Confirmed") {
        Sat = "Booking Success/Confirmed";
        $("#divbuttonappend").html(' ');
        if (Checkiffinished == false) {
            // $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
            $("#divbuttonappend").append('<a id="btnCancelPnr" onclick="CancelBooking()"><i class="newclass"></i>CANCEL</a>');
        }
        $("#divbuttonappend").append('<a id="btnDownloadVochuer" download data-open="0" onclick="DownloadVochuer()">DOWNLOAD VOUCHER</a>');
        $("#divbuttonappend").append('<a data-toggle="modal" data-target="#modalCancelMail" title="Email Voucher" id="btnEmailVochuer">EMAIL VOUCHER</a>');

    }
    else if (status == "Confirmed") {
        Sat = "Confirmed";
        $("#divbuttonappend").html(' ');
        if (Checkiffinished == false) {
            // $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
            $("#divbuttonappend").append('<a id="btnCancelPnr" onclick="CancelBooking()"><i class="newclass"></i>CANCEL</a>');
        }
        $("#divbuttonappend").append('<a id="btnDownloadVochuer" download data-open="0" onclick="DownloadVochuer()">DOWNLOAD VOUCHER</a>');
        $("#divbuttonappend").append('<a data-toggle="modal" data-target="#modalCancelMail" title="Email Voucher" id="btnEmailVochuer">EMAIL VOUCHER</a>');

    }
    else if (status == "Requested") {
        Sat = "Requested";
        $("#divbuttonappend").html(' ');
        $("#divbuttonappend").append('<a id="btnUpdate" onclick="Update(' + up + ')"><i class="greatclass"></i>UPDATE</a>');
        if (Checkiffinished == false) {
            // $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
            $("#divbuttonappend").append('<a id="btnCancelPnr" onclick="CancelBooking()"><i class="newclass"></i>CANCEL</a>');
        }
        else if (status == "Cancelled") {
            Sat = "Cancelled";
            $("#divbuttonappend").html(' ');
            $('#Divcancellationalert').html('');
        }
    }
    if (RoleUpdate == '0') {
        $("#btnUpdate").remove();
    }
    if (RoleVoucher == '0') {
        $("#btnDownloadVochuer").remove();
        $("#btnEmailVochuer").remove();
    }
    if (RoleCancel == '0') {
        $("#btnCancelPnr").remove();
    }

    return Sat;
}

function GetPaxBreak(iPax, iQ) {
    $("#tablePAXBreakUps").html('');


    HotelPaxDatabrkup = HotelbookinfoData.booking.hotels["0"].rooms[iPax].roomQuotes[iQ].guests;
    for (var ifb = 0; ifb < HotelPaxDatabrkup.length; ifb++) {
        var val = ifb;
        val = parseFloat(parseFloat(val) + parseFloat(1));
        var Fname = HotelPaxDatabrkup[ifb].firstName;
        var LName = HotelPaxDatabrkup[ifb].surName;
        var Title = HotelPaxDatabrkup[ifb].titleName;

        $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Title + " " + Fname + " " + LName + '</tr></td>');


    }
    $(".loadingGIF").hide();
}
function BookingHotelRoomPaxBrkup(data) {
    $("#tablePAXBreakUps").html('');
    var HotelPaxDatabrkup = JSON.parse(data.d);

    for (var ifb = 0; ifb < HotelPaxDatabrkup.length; ifb++) {
        var val = ifb;
        val = parseFloat(parseFloat(val) + parseFloat(1));
        var Fname = HotelPaxDatabrkup[ifb].PaxFirstName;
        var LName = HotelPaxDatabrkup[ifb].PaxLastName;
        var Title = HotelPaxDatabrkup[ifb].PaxTitle;
        var PaxAge = HotelPaxDatabrkup[ifb].PaxTypeAge;
        if (Title == 2 || Title == 5) {
            var agesplit = PaxAge.split(",");

            $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Gettitle(Title) + " " + Fname + " " + LName + " ( Age " + agesplit[1] + ")" + '</tr></td>');
        }
        else {
            $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Gettitle(Title) + " " + Fname + " " + LName + '</tr></td>');
        }

    }
    $(".loadingGIF").hide();
}



function GetFareBreak() {
    $(".loadingGIF").show();
    $("#tableFareBreakUps").html('');
    var HotelFareDatabrkup = HotelPaxData;
    var total = parseFloat(0);
    $('#tableFareBreakUps').append('<thead> <tr> <th class="sp_table">Rooms</th> <th class="sp_table">Room Cost</th> </tr> </thead> <tbody>')
    for (var ifb = 0; ifb < HotelFareDatabrkup.length; ifb++) {
        for (var ix = 0; ix < HotelFareDatabrkup[ifb].roomQuotes.length; ix++) {

            var val = parseFloat(parseFloat(ifb) + parseFloat(1));
            var trroom = val;
            var trBaseFare = HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellAmount;
            var fare = HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellAmount;;
            total += parseFloat(fare);
            var mealtype = '';
            if (HotelFareDatabrkup[ifb].meal == "Breakfast") {
                mealtype = 'Full Breakfast'
            }
            else {
                mealtype = HotelFareDatabrkup[ifb].meal;
            }
            $('#tableFareBreakUps').append('<tr><td>' + val + ". " + HotelFareDatabrkup[ifb].category + '</td> <td>' + Math.ceil(fare * 100) / 100 + " " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellCurrency + '</td></tr>');

        }


    }
    $('#tableFareBreakUps').append('</tbody>');
    $("#TotalCost").html('Total Cost : ' + Math.ceil(total * 100) / 100 + " " + HotelFareDatabrkup["0"].roomQuotes["0"].debitDetails.sellCurrency);
    $(".loadingGIF").hide();


}
function GetpriceBreak() {
    $(".loadingGIF").show();
    var HotelFareDatabrkup = HotelPaxData;
    var netamount = parseFloat(0);
    var mvalue = parseFloat(0);
    var Total = parseFloat(0);
    var baseamount = parseFloat(0);
    for (var ifb = 0; ifb < HotelFareDatabrkup.length; ifb++) {
        for (var ix = 0; ix < HotelFareDatabrkup[ifb].roomQuotes.length; ix++) {


            $("#tablePriceBreakUps").html('');


            netamount += '<tr> <td class="fare_head_area">Net Amount:Room ' + HotelFareDatabrkup[ifb].category + '</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.netAmount + '</td></tr>';
            var netCurrency = '<tr> <td class="fare_head_area">Net Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellCurrency + '</td></tr>';
            var baseCurrency = '<tr> <td class="fare_head_area">Net Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellCurrency + '</td></tr>';
            var RateofChange = '<tr> <td class="fare_head_area">Rate Of Exchange</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.conversionRate + '</td></tr>';

            mvalue += HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.totalMarkup;
            // var mtypevalue = HotelFareDatabrkup[0].MarkUpType; 
            var farecur = HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellCurrency;
            var Markup = '<tr> <td class="fare_head_area">Total Markup </td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + mvalue + '</td></tr>';
            //  var fare = GetRoomPriceConvMark(totalnet, mvalue, mtypevalue, 0, RofExvalue);
            Total += '<tr> <td class="fare_head_area">Sell Amount:Room ' + HotelFareDatabrkup[ifb].category + '</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + +HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.sellAmount + '</td></tr>';
            var FareCurrency = '<tr> <td class="fare_head_area">Sell Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + farecur + '</td></tr>';

            var baseCurrency = '<tr> <td class="fare_head_area">Base Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.netCurrency + '</td></tr>';
            baseamount += '<tr> <td class="fare_head_area">Base Amount:Room ' + HotelFareDatabrkup[ifb].category + '</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[ifb].roomQuotes[ix].debitDetails.baseAmount + '</td></tr>';

        }
    }
    $("#tablePriceBreakUps").append(Total + FareCurrency + netamount + netCurrency + baseCurrency + baseamount + RateofChange + Markup);
    $(".loadingGIF").hide();
}
function CancellationPolicy(tthis) {
    $(".loadingGIF").show();
    var dataopen = $(tthis).attr("data-open");
    //alert(dataopen);
    if (dataopen == 0) {

        $("#divcancellationpolicy").html('');

        //console.log(Hotelcancellationpolicy);
        var CancPolicyString = '';
        var bookidold = '';
        var roomcount = parseInt(0);
        for (var iP = 0; iP < HotelbookinfoData.booking.hotels["0"].rooms.length; iP++) {
            var Hotelcancellationpolicy = HotelbookinfoData.booking.hotels["0"].rooms[iP].roomQuotes;
            for (var iarr = 0; iarr < Hotelcancellationpolicy.length; iarr++) {

                for (var i = 0; i < Hotelcancellationpolicy[iarr].cancellationPolicyList.length; i++) {
                    CancPolicyString = '#RoomCategory#</button> <div class="panel"> <div class="accodion_cont"><p>';
                    var RCat = HotelbookinfoData.booking.hotels["0"].rooms[iP].category;
                    CancPolicyString = CancPolicyString.replace("#RoomCategory#", RCat);
                    var cancelfrmdate = Hotelcancellationpolicy[iarr].cancellationPolicyList[i].fromDate;
                    var canceltodate = Hotelcancellationpolicy[iarr].cancellationPolicyList[i].toDate;
                    var cancelamount = Hotelcancellationpolicy[iarr].cancellationPolicyList[i].totalCost;
                    cancelfrmdate = moment(cancelfrmdate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                    if (cancelamount == "0") {
                        var cancelfrmdate = Hotelcancellationpolicy[iarr].cancellationPolicyList[i].fromDate;
                        var canceltodate = Hotelcancellationpolicy[iarr].cancellationPolicyList[i].toDate;
                        canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        CancPolicyString += '<li style="color:#0c5b9a;">Free cancellation on or before <span>' + canceltodate + '</span></li>';


                    }
                    else if (moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].toDate).format("DD MMM YYYY") == moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].fromDate).format("DD MMM YYYY")) {
                        CancPolicyString += '<li style="color:red;font-weight:bolder;">Non Refundable Booking with Cancellation Charges <span>' + +GetRoomPriceConvMark(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].totalCost, Hotelcancellationpolicy[iarr].debitDetails.conversionRate) + ' ' + " " + Hotelcancellationpolicy[iarr].debitDetails.sellCurrency + '</span></li>';
                    }
                    else if (moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].toDate).format("DD MMM YYYY") == moment(CheckinDate).format("DD MMM YYYY")) {
                        var canceltodate = moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].fromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        //canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        CancPolicyString += '<li style="color:#0c5b9a;">Cancellation on or after <span>' + canceltodate +
                            '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].totalCost, Hotelcancellationpolicy[iarr].debitDetails.conversionRate) + ' ' + " " + Hotelcancellationpolicy[iarr].debitDetails.sellCurrency + '</span></li>';
                    }
                    else {
                        var cancelfrmdate = moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].fromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        var canceltodate = moment(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].toDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        CancPolicyString += '<li style="color:#0c5b9a;">Cancellation between <span>' + cancelfrmdate +
                            '</span> and <span>' + canceltodate + '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[iarr].cancellationPolicyList[i].totalCost, Hotelcancellationpolicy[iarr].debitDetails.conversionRate) + ' ' + " " + Hotelcancellationpolicy[iarr].debitDetails.sellCurrency + '</span></li>';
                    }
                    $("#divcancellationpolicy").append('<button class="accordion">' + CancPolicyString + '</p></div></div>');
                }
            }
        }

        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            }
        }
    }
    $(tthis).attr("data-open", "1");
    //  $("#divcancellationpolicy").append('</ul>');

    $(".loadingGIF").hide();
}
function GetRoomPriceConvMark(ogprice, rateofexchange) {//, cancelation, rooms) {
    var NewPriceMarkup = ogprice;
    var SellingRateUSD = parseFloat(rateofexchange);
    var BuyyingRateCurr = parseFloat(1);
    NewPrice = (ogprice * SellingRateUSD) / BuyyingRateCurr;
    var NewPriceMarkup = NewPrice;

    return Math.ceil(NewPriceMarkup * 100) / 100;
}
function GetHistory() {
    $(".loadingGIF").show();
    $('#tablehistory').html('');
    $("loadingGIFFareBr").show();
    Hotelhistory = HotelbookinfoData.booking.bookingStatusList;
    if (Hotelhistory.length != undefined) {
        for (var iH = 0; iH < Hotelhistory.length; iH++) {

            $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].entryDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Hotelhistory[iH].bookingStatus + '</b> - ' + Hotelhistory[iH].userName + '</li>');

        }
    }
    else {
        $('#tablehistory').html('<tr><td>' + "No history available ! " + '</td></tr>');
    }


    $("loadingGIFFareBr").hide();
    $(".loadingGIF").hide();
}
function SendMailBookingInfo(bstatus) {
    var mailid = window.localStorage.getItem("EMail");
    var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
    var Dastasseq = {
        EUName: mailname,
        ETripID: ETripID,
        EPaxData: EPaxData,
        UserMailID: mailid,
        ETripName: ETripName,
        ETotalCharge: grandtotal,
        bstatus: bstatus

    };
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/SendMailBookhotel",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SendMailTicketIssueSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                //alert('500');
            }
        }
    });
}
function SendMailTicketIssueSuccess() {

}

function CancelBooking() {
    // sendmailbookcancelb2c();
    alertify.confirm("Please note", "Are you sure to cancel this booking?",
        function () {

            $(".newclass").addClass("fa fa-circle-o-notch fa-spin");
            //if (Cstatus == "RR") {
            Update("XX");

            //  }
            // else {

            // cancelfrmupdate();

            // }
        },
        function () {
            $('#sort_popup').css("z-index", "99999");
        });
}

function sendmailbookcancelb2c() {
    var parass = {
        APIid: APIid,
        ETripIDD: ETripID,
        EUNamee: EUName,
        ECommentt: $("#txtChangeTravelDateComment").val()
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/CancelbookinghotelMail",
        data: JSON.stringify(parass),
        contentType: "application/json; charset=utf-8",
        success: CancelbookingmaillSuccess,
        failure: function (response) {
            alert(response.d);
        },
        dataType: "json"
    });
}
function CancelbookingmaillSuccess() {
    alertify.alert('Cancel', 'Admin is notified about your cancellation request by mail.');

}
function Update(Check) {
    checkval = Check;
    $(".greatclass").addClass("fa fa-circle-o-notch fa-spin");

    if (SupplierCode == 'R24') {
        supprf = BookingReferenceID;
    }
    var Datass = {

        APIid: supprf,
        SuppCode: SupplierCode,
        AgencyCode: HotelAgencyCode,
        token: accesstocken

    }
    $.ajax({
        type: "POST",
        url: HotelPrecancelURL,
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelUpdate,
        failure: function (response) {
            alert(response.d);

        }
    });

}

function BookingHotelUpdate(data) {
    // var HotelBookingHotelUpdateinfo = data.d;
    // var x2js2 = new X2JS();
    //var jsonHotelUpdateRes = x2js2.xml_str2json(HotelBookingHotelUpdateinfo);
    var Supplicancellationcharges = "0";
    var PreCancelToken = "xxx";
    if (data != "NORESULT") {
        var jsonHotelUpdateRes = JSON.parse(data);
        if (jsonHotelUpdateRes != '' && jsonHotelUpdateRes != undefined) {
            var JBookUpdateRes = jsonHotelUpdateRes.Response.ResponseDetails.SearchBookingItemResponse;
            //   if (JBookUpdateRes != '' && JBookUpdateRes != undefined) {
            updatestatus = jsonHotelUpdateRes.Response.ResponseDetails.SearchBookingItemResponse.BookingStatus._Code;
            Supplicancellationcharges = jsonHotelUpdateRes.Response.ResponseDetails.SearchBookingItemResponse.BookingPrice._Nett;
            PreCancelToken = jsonHotelUpdateRes.Response._ResponseReference;
            if (Supplicancellationcharges == null) {
                Supplicancellationcharges = "0";
            }
            if (PreCancelToken == null) {
                PreCancelToken = "xxx"
            }
            if (updatestatus != undefined && updatestatus != '') {

                if (checkval == "XX") {

                    cancelfrmupdate(Supplicancellationcharges, PreCancelToken);

                }
                else {
                    if (updatestatus == "C ") {

                        alertify.alert('Confirmed', 'Your booking got confirmed!');
                        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
                        itemrq = {}
                        itemrq["referenceNumber"] = BookingReferenceID;
                        itemrq["code"] = "HK";

                        var jsonstatusupsdterq = JSON.stringify(itemrq);
                        var statusupdate = {

                            requestentry: jsonstatusupsdterq,
                            url: "hotel/booking/changeStatus"

                        }

                        $.ajax({
                            type: "POST",
                            url: "../Common.aspx/BookHotelStatusUpdate",
                            data: JSON.stringify(statusupdate),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: BookingHotelupdatesuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });

                        return true;
                    }
                    else if (updatestatus == "CP") {
                        alertify.alert('Confirmation Pending', 'Your booking is not confirmed yet!');
                        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
                        itemrq = {}
                        itemrq["referenceNumber"] = BookingReferenceID;
                        itemrq["code"] = "RP";

                        var jsonstatusupsdterq = JSON.stringify(itemrq);
                        var statusupdate = {

                            requestentry: jsonstatusupsdterq,
                            url: "hotel/booking/changeStatus"

                        }

                        $.ajax({
                            type: "POST",
                            url: "../Common.aspx/BookHotelStatusUpdate",
                            data: JSON.stringify(statusupdate),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: BookingHotelupdatesuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                        return true;
                    }
                }
            }

            else {
                alertify.alert('Update', 'Error occured! Please contact admin.');
                $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
                return true;
            }

        }
        else {
            alertify.alert('Update', 'Error occured!Please contact admin.');
            $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
            return true;

        }
        setTimeout(function () {
            $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);
    }
    else {
        cancelfrmupdate(Supplicancellationcharges, PreCancelToken);
    }
}
function BookingHotelupdatesuccess() {
    Bookinfo();

}
function ShowCancellationChargeIcon() {
    $(".cancpolicydiv h3").each(function () {
        var Xlstatus = $(this).attr("xlcharge");
        if (Xlstatus == "true") {
            var tempid = $(this).parent().parent().parent().parent().attr("id").replace("cmod", "r");
            $("#" + tempid).find("div.cost_area").find("a").append('<i class="fa fa-money red" data-toggle="tooltip" data-placement="top" data-original-title="Under cancellation fare" aria-hidden="true"></i>');
        }
    });
}

//Show hidden cancel policies
function ShowAllCancPolicy(thiss) {
    $(thiss).hide();
    $($(thiss).parent().parent().find("h4")).each(function () {
        $(this).show();
    });
}


function cancelfrmupdate(Supplicancellationcharges, PreCancelToken) {
    var CurrentDate = new Date();
    CurrentDate = moment(CurrentDate);
    var limit = moment(Time);
    var F = 24 * 60 * 60 * 1000, Daysdiff = Math.round(Math.abs((limit - CurrentDate) / (F)));
    // if (Daysdiff <= 0) {
    // alertify.confirm('Note', 'You are under cancellation period!', function () {
    if (SupplierCode == 'R24') {
        supprf = BookingReferenceID;
    }
    var Datass = {

        APIid: supprf,
        SuppCode: SupplierCode,
        PreCancellationToken: PreCancelToken,
        CancellationCharge: Supplicancellationcharges,
        AgencyCode: HotelAgencyCode,
        token: accesstocken
    }
    $.ajax({
        type: "POST",
        url: HotelCancelURL,
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelCancelupdate,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BookingHotelCancelupdate(data) {
    var Hotelbookcancelinfo = data.d;
    var x2js2 = new X2JS();
    if (data != "NORESULT") {
        // var jsonHotelCancelRes = x2js2.xml_str2json(Hotelbookcancelinfo);
        var jsonHotelCancelRes = JSON.parse(data);
        // var jsonHotelCancelRes = data;
        JBookCancelRes = jsonHotelCancelRes.Response.ResponseDetails.BookingResponse;
        var Error = JBookCancelRes.Errors;
        if (Error == undefined) {
            Statuscancellation = JBookCancelRes.BookingStatus._Code;

            SendMailCancelbooking("Cancelled")
            itemrq = {}
            itemrq["referenceNumber"] = BookingReferenceID;
            itemrq["code"] = "XX";

            var jsonstatusupsdterq = JSON.stringify(itemrq);
            var statusupdate = {

                requestentry: jsonstatusupsdterq,
                url: "hotel/booking/changeStatus"

            }

            $.ajax({
                type: "POST",
                url: "../Common.aspx/BookHotelStatusUpdate",
                data: JSON.stringify(statusupdate),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: statusupdatesuccesss,
                failure: function (response) {
                    alert(response.d);
                }
            });

        }
        else {
            alertify.alert('Cancel', 'You cannot cancel this booking,its already exceed the limit of cancellation!');

            setTimeout(function () {
                $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
            }, 3000);

            return true;
        }
    }
    else {
        alertify.alert('Cancel', 'Error occured! Please contact admin.');

        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);

        return true;
    }
}
function statusupdatesuccesss(data) {
    var result = data.d;
    if (result != "NORESULT") {
        alertify.alert('Cancel', 'Your booking got cancelled successfully!');
        Bookinfo();
        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);
        window.location.href = "/HOTEL/hotelbookinginfo.html";
    }
    else {
        alertify.alert('Cancel', 'You cannot cancel this booking,its already exceed the limit of cancellation!');

        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);
    }
}
function DownloadVochuer() {
    if ($("#btnDownloadVochuer").attr("data-open") == "0") {
        var Dastasseq = {
            AgencyName: AgencyName,
            AgencyAddress: AgencyAddress,
            AgencyContact: AgencyContact,
            gettoday: gettoday,
            ETripName: ETripName,
            HotelAddress: HotelAddress,
            ETripID: ETripID,
            voucherbookingdate: voucherbookingdate,
            APIid: supprf,
            chin: chin,
            chout: chout,
            VoucherPax: VoucherPax,
            GtaAotNumber: GtaAotNumber,
            grandtotal: grandtotal

        };
        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/VoucherBookhotel",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: VoucherBookhotelSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //  alert('500');
                }
            }
        });
    } else {
        window.open(dataUrlVoucher, "_blank");
    }


}

var dataUrlVoucher = "";
function VoucherBookhotelSuccess(data) {

    //var link = data.d
    dataUrlVoucher = data.d;
    //$("#btnDownloadVochuer").attr("href", link);
    $("#btnDownloadVochuer").attr("data-open", "1");
    // $("#btnDownloadVochuer").prop("download");
    //$("#btnDownloadVochuer").removeAttr("onclick");
    //console.log(data);
    //$("#btnDownloadVochuer").click();
    window.open(dataUrlVoucher, "_blank");

}

function EmailVochuerTo() {
    var mailid = $("#txtTktToMail").val();
    if (mailid != '' || mailid != null || mailid != undefined) {
        var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
        var Dastasseq = {
            AgencyName: AgencyName,
            AgencyAddress: AgencyAddress,
            AgencyContact: AgencyContact,
            gettoday: gettoday,
            ETripName: ETripName,
            HotelAddress: HotelAddress,
            ETripID: ETripID,
            voucherbookingdate: voucherbookingdate,
            APIid: supprf,
            chin: chin,
            chout: chout,
            VoucherPax: VoucherPax,
            Email: mailid,
            mailname: mailname,
            GtaAotNumber: GtaAotNumber,
            grandtotal: grandtotal
        };

        $.ajax({
            type: "POST",
            url: "WebMethodDbHotel.aspx/EmailVoucherBookhotel",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: EmailVoucherBookhotelSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //  alert('500');
                }
            }
        });
    }
    else {
        alertify.alert('Validation', 'Please fill the emailid.');
    }
}

function EmailVoucherBookhotelSuccess() {

    alertify.alert('Email', 'Email sent successfully');
}


function SendMailCancelbooking(bstatus) {
    var mailid = window.localStorage.getItem("EMail");
    if (mailid == '') {
        mailid = EMailID;
    }
    var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
    var Dastasseq = {
        EUName: mailname,
        ETripID: ETripID,
        EPaxData: EPaxData,
        UserMailID: mailid,
        ETripName: ETripName,
        ETotalCharge: grandtotal,
        bstatus: bstatus

    };
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/SendMailCancelbooking",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SendMailCancelbookingSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                //alert('500');
            }
        }
    });
}
function SendMailCancelbookingSuccess() {

}
//=============GetAgencyCodeStart=========================
/////////////////////////////////////////
function SessiongetAgencyCode(session) {
    $(".loadingGIF").show();
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetSessionDatadet",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccessdet,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SessionSuccessdet(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            HotelAgencyCode = data.d[0];
            HotelFindURL = data.d[1];
            HotelDetailURL = data.d[2];
            HotelPrebookURL = data.d[3];
            HotelBookURL = data.d[4];
            HotelPrecancelURL = data.d[5];
            HotelCancelURL = data.d[6];
            HotelAmenitiesURL = data.d[7];
            HotelRoomAmenitiesURL = data.d[8];
        }
    }
}
//=============GetAgencyCodeEnd=========================
/////////////////////////////////////////


function SessionCheckDetails(session) {
    //   $(".loadingGIF").show();
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetSessionDataNew",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionCheckingSuccess,
        failure: function (data) {
            console.log(data);
        }
    });

    function SessionCheckingSuccess(data) {

        if (data.d.Access_Token == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            accesstocken = data.d.Access_Token;
            window.localStorage.setItem("accesstocken", accesstocken);
        }

        if (data.d.Loginuser == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var logininfo = $.parseJSON(data.d.Loginuser);
            var agcode = logininfo.user.loginNode.code;
            var agycodesplit = agcode.match(/\d+/);
            var Agycode = agycodesplit[0];
            window.localStorage.setItem("Agycode", Agycode);
            $('.username').html(logininfo.user.firstName);
            $('.AgencyName').html(logininfo.user.loginNode.name);
            EUName = logininfo.user.firstName;
            EmailID = logininfo.user.emailId;
            AgencyName = logininfo.user.loginNode.name;
            AgencyAddress = logininfo.user.loginNode.name;
            AgencyContact = logininfo.user.contactNumber;
            gettoday = new Date();
            gettoday = moment(gettoday).format("DD MMM YYYY");



        }

    }
}
