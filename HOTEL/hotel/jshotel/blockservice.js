﻿$(document).ready(function () {
    SessionChecking('UserDetails');
    BindAgency();
    BindService();
});
function SessionChecking(session) {
    $(this).attr("disabled", true);
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        //console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        $('.AgencyName').html(userdata[0].AgencyName);
        $('#txtFName').val(userdata[0].FirstName);
        $('#txtLastName').val(userdata[0].LastName);
        $('#ddlTitle').val(userdata[0].TitleID);
        $('#txtEmail').val(userdata[0].EmailID);
        $('#txtPhone').val(userdata[0].Mobile);

        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }
        ShowBlockedServices();
        //Get Markup
        var Dastasseqq = {
            AgencyID: userdata[0].AgencyIDD,
            ServiceTypeID: '1'
        };
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodsDB.aspx/GetMarkupData",
            data: JSON.stringify(Dastasseqq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: MarkupSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function MarkupSuccess(datamark) {
    $(this).attr("disabled", false);
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No markup !');
    }
}
function BindAgency() {
    var Datatable = {
        selectdata: 'AgencyID,AgencyName',
        tablename: 'tblAgency',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: AgencySuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function AgencySuccess(dataa) {
        var Agencydata = $.parseJSON(dataa.d);
        console.log(Agencydata);
        for (var i = 0; i < Agencydata.length; i++) {
            $("#ddlAgency").append($("<option></option>").val(Agencydata[i].AgencyID).html(Agencydata[i].AgencyName));
        }
    }
}
function BindService() {
    var Datatablea = {
        selectdata: 'TypeID,ServiceType',
        tablename: 'tblServicetype',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatablea),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ServiceSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function ServiceSuccess(dataaa) {
        var Servicedata = $.parseJSON(dataaa.d);
        console.log(Servicedata);
        for (var i2 = 0; i2 < Servicedata.length; i2++) {
            $("#ddlService").append($("<option></option>").val(Servicedata[i2].TypeID).html(Servicedata[i2].ServiceType));
        }
    }
}
function BlockService() {
    if ($('#ddlService').val() != '0') {
        $(this).attr("disabled", true);
        var DataBlock = {
            AgencyId: $('#ddlAgency').val(),
            ServiceId: $('#ddlService').val(),
            Operation: 'INSERT'
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/CRUDBlockService",
            data: JSON.stringify(DataBlock),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SaveBlockDataSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
        function SaveBlockDataSuccess(dblo) {
            if (dblo.d == '-1') {
                ShowBlockedServices();
                alertify.alert('Blocked', 'Successfully Blocked');
            }
        }
    }
    else {
        alertify.alert('Validate', 'Please select !');
    }
}

function ShowBlockedServices() {
    $("#loadergifht").show();

    var Datatable = {
        selectdata: 'A.*,dbo.getAgencyNameBlocked(A.AgencyID) as AgencyName,B.FirstName,D.ServiceType',
        tablename: 'tblBlockedList A inner join tblUser B on B.UserID=A.UserId inner join tblServicetype D on D.TypeID=A.ServiceTypeID',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ShowBlockedSerSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function ShowBlockedSerSuccess(datacreeee) {
        if (datacreeee.d != 'NODATA' && datacreeee.d != "") {
            var Searchdata = $.parseJSON(datacreeee.d);
            console.log(Searchdata);
            $('#divBlockedSer').show();
            var searchresh = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                searchresh += '<tr><td>' + Searchdata[isd].ServiceType +
                '</td><td>' + Searchdata[isd].AgencyName + '</td><td>'
                + Searchdata[isd].FirstName + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BlockedDate).format("ddd-MMM-YYYY h:mm:ss a") + '">'
                + moment(Searchdata[isd].BlockedDate).format("DD-MM-YY h:mm:ss a") +
                '</td><td><button class="OnrequestUpdate" onclick="UnblockService(this,' + Searchdata[isd].AgencyID + ',' + Searchdata[isd].ServiceTypeID + ')">Unblock</button></td></tr>';
            }
            $("#tbodyBlockedSer").html(searchresh);
            $('[data-toggle="tooltip"]').tooltip();
            $('#tableBlockedSer').DataTable({
                "order": [[1, "asc"]]
            });
        }
        else {
            $('#divBlockedSer').hide();
        }
        $("#loadergifht").hide();
    }
}
function UnblockService(thiss, AgencyIDE, ServiceTypeIDE) {
    var DataBlock = {
        AgencyId: AgencyIDE,
        ServiceId: ServiceTypeIDE,
        Operation: 'DELETE'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/CRUDBlockService",
        data: JSON.stringify(DataBlock),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: UnBlockDataSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function UnBlockDataSuccess(dbloun) {
        if (dbloun.d == '-1') {
            $(thiss).parent().parent().remove();
            alertify.alert('Unblocked', 'Successfully Unblocked');
        }
    }
}