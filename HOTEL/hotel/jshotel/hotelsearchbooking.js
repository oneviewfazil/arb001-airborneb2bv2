﻿//var DateFormate = 'dd/mm/yy';
var DateFormate = 'mm/dd/yy';
var AgencyCurrencyCode = '';
var AgencyCurrencyExRate = '';

$(document).ready(function () {
    SessionChecking('Loginuser');
    GetCurrencyDetails();
    SearchHotelwithStatus();	
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
   

    $('#BookingDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#BookingDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#BookingDateTo').datepicker({
        defaultDate: "0d",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#BookingDateTo').datepicker("widget").css({ "z-index": 10000 });

    $('#CheckinDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#CheckinDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#CheckinDateTo').datepicker({
        defaultDate: "0d",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#CheckinDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#CheckinDateTo').datepicker("widget").css({ "z-index": 10000 });
});
function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {

        var logininfo = $.parseJSON(data.d);
        //var agcode = logininfo.user.loginNode.code;
        //var agycodesplit = agcode.match(/\d+/);
        //var Agycode = agycodesplit[0];
        //window.localStorage.setItem("Agycode", Agycode);
        $('.username').html(logininfo.user.firstName);
        $('.AgencyName').html(logininfo.user.loginNode.name);
        var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);

        var AgencyTypeIDD = 1;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }

        //var AdminStatus = userdata[0].AdminStatus;
        //if (AgencyTypeIDD == '1' && AdminStatus == '1') {
        //    $('#divddlBranch').show();
        //    BindBranch();
        //}
        //else {
        //    $("#ddlBranch").html($("<option></option>").val(userdata[0].AgencyID).html(userdata[0].AgencyName));
        //    $('#divddlBranch').hide();
        //}
        ////Get Markup
        //var Dastasseqq = {
        //    AgencyID: userdata[0].AgencyIDD,
        //    ServiceTypeID: '2'
        //};
        //$.ajax({
        //    type: "POST",
        //    url: "/HOTEL/WebserviceHotel.aspx/GetMarkupDataHotel",
        //    data: JSON.stringify(Dastasseqq),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: MarkupSuccess,
        //    failure: function (response) {
        //        alert(response.d);
        //    }
        //});
    }
}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No credit !');
    }
}
function BindBranch() {
    var Datatable = {
        selectdata: '*',
        tablename: 'tblAgency',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BindBranchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BindBranchSuccess(data) {
    var Branchdata = $.parseJSON(data.d);
    //console.log(Branchdata);
    for (var i = 0; i < Branchdata.length; i++) {
        $("#ddlBranch").append($("<option></option>").val(Branchdata[i].AgencyID).html(Branchdata[i].AgencyName));
    }
}

//function SearchHotelwithStatus() {
//    var status;
//    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
//    for (var i = 0; i < hashes.length; i++) {
//        hash = hashes[i].split('=');
//        status = hash[1];
//    }
//    if (status != undefined) {      
//        $('#divSearch').hide();
//        $('#divSearchRes').show();
//        $('#loadingGIFFlSearchDet').show(); 
//        $('#tableSearch').hide();
//        var BkStatus;
//        if (status != "all") {
//            BkStatus = status;
//        }
//        else {
//            BkStatus = $('#ddlStatus').val();
//        }
//        var Datatable = {
//            CityName: $('#CityName').val(),
//            HotelName: $('#HotelName').val(),
//            BookingDateFrom: $('#BookingDateFrom').val(),
//            BookingDateTo: $('#BookingDateTo').val(),
//            CheckinDateFrom: $('#CheckinDateFrom').val(),
//            CheckinDateTo: $('#CheckinDateTo').val(),
//            BookingRefNo: $('#BookingRefNo').val(),
//            SupplierRefNo: $('#SupplierRefNo').val(),
//            ddlBranch: $('#ddlBranch').val(),
//            ddlStatus: BkStatus

//        };
//        $.ajax({
//            type: "POST",
//            url: "/HOTEL/WebMethodDbHotel.aspx/GetSearchDataHotel",
//            data: JSON.stringify(Datatable),
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: SearchSuccess,
//            failure: function (response) {
//                alert(response.d);
//            }
//        });
//    }
//    else {
//        $('#divSearch').show();
//    }
//}
//function SearchSuccess(datab) {
//    console.log(datab.d);
//    if (datab.d != "NODATA") {
//        $('#divNoData').hide();
//        var Searchdata = $.parseJSON(datab.d);
//        console.log(Searchdata);
//        var searchres = '';
//        for (var isd = 0; isd < Searchdata.length; isd++) {
//            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
//                '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].ItemCityCode) + '">'
//                + Searchdata[isd].ItemCityCode + '</td><td>'
//                + Searchdata[isd].ItemName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].CheckInDate).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].CheckInDate).format("DD-MM-YY") + '</td><td>' + Searchdata[isd].Nights +
//                '</td><td  data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].timelimit).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].timelimit).format("DD-MM-YY") + '</td><td data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
//                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
//                + Searchdata[isd].BookingStatusCode + '</td></tr>';

//        }
//        $("#SearchResult").html(searchres);
//        $('[data-toggle="tooltip"]').tooltip();
//        $('#tableSearch').DataTable({
//            "order": [[0, "desc"]]
//        });
//        $('#tableSearch').show();
//    }
//    else {
//        $('#divNoData').show();
//    }

//    $('#loadingGIFFlSearchDet').hide();
//    $('#divSearch').hide();
//}

function SearchHotelwithStatus() {
    $('#divSearchRes').show();
       
        $.ajax({
            type: "POST",
            url: "/FLIGHT/Testlogrequest.aspx/GetSearchDataHotel",
           
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SearchSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    
    
}
function SearchSuccess(datab) {
    //console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divNoData').hide();
        var Searchdata = $.parseJSON(datab.d);
        //console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClick(\'' + Searchdata[isd].referenceNumber + '\')"><td>' + Searchdata[isd].referenceNumber +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].cityCode) + '">'
                + Searchdata[isd].cityCode + '</td><td>'
                + Searchdata[isd].hotelName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].checkInDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].checkInDate).format("DD-MM-YY") + '</td><td>' + Searchdata[isd].numOfNights +
                '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].deadlineDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].deadlineDate).format("DD-MM-YY") + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].bookDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].bookDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].status) + '">'
                + Searchdata[isd].status + '</td></tr>';

        }
        $("#SearchResult").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableSearch').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableSearch').show();
    }
    else {
        $('#divNoData').show();
    }

    $('#loadingGIFFlSearchDet').hide();
    $('#divSearch').hide();
}


//function SearchHotel() {
//    $('#divSearch').hide();
//    $('#divSearchRes').show();
//    $('#loadingGIFFlSearchDet').show();
//    $('#tableSearch').hide();
//    var Datatable = {
//       CityName: $('#CityName').val(),
//       HotelName: $('#HotelName').val(),
//        BookingDateFrom: $('#BookingDateFrom').val(),
//        BookingDateTo: $('#BookingDateTo').val(),
//        CheckinDateFrom: $('#CheckinDateFrom').val(),
//        CheckinDateTo: $('#CheckinDateTo').val(),
//        BookingRefNo: $('#BookingRefNo').val(),
//        SupplierRefNo: $('#SupplierRefNo').val(),
//        ddlBranch: $('#ddlBranch').val(),
//         ddlStatus: $('#ddlStatus').val()
      
//    };
//    $.ajax({
//        type: "POST",
//        url: "/HOTEL/WebMethodDbHotel.aspx/GetSearchDataHotel",
//        data: JSON.stringify(Datatable),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: SearchSuccessres,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}
//function SearchSuccessres(datab) {
//    console.log(datab.d);
//    if (datab.d != "NODATA") {
//        $('#divNoData').hide();
//        var Searchdata = $.parseJSON(datab.d);
//        console.log(Searchdata);
//        var searchres = '';
//        for (var isd = 0; isd < Searchdata.length; isd++) {
//            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
//                '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].ItemCityCode) + '">'
//                + Searchdata[isd].ItemCityCode + '</td><td>'
//                + Searchdata[isd].ItemName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].CheckInDate).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].CheckInDate).format("DD-MM-YY") + '</td><td>' + Searchdata[isd].Nights +
//                '</td><td  data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].timelimit).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].timelimit).format("DD-MM-YY") + '</td><td data-toggle="tooltip" data-placement="top" title="'
//                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
//                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
//                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
//                + Searchdata[isd].BookingStatusCode + '</td></tr>';

//        }
//        $("#SearchResult").html(searchres);
//        $('[data-toggle="tooltip"]').tooltip();
//        $('#tableSearch').DataTable({
//            "order": [[0, "desc"]]
//        });
//        $('#tableSearch').show();
//    }
//    else {
//        $('#divNoData').show();
//    }

//    $('#loadingGIFFlSearchDet').hide();
//    $('#divSearch').hide();
//}
function SearchDetailsClick(sid) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess() {
    window.location.href = "/HOTEL/hotelbookinginfo.html";
}

function GetStatus(status) {
    var Sat = "";
    if (status == "HK") {
        Sat = "Confirmed";
       
    }
    else if (status == "RR") {
        Sat = "Confirmation Pending";
       
    }
    else if (status == "XX") {
        Sat = "Cancelled";
    }
    else if (status == "RC") {
        Sat = "Reconfirmed";
       
    }
    else if (status == "RQ") {
        Sat = "On request";
       
    }
    else if (status == "XR") {
        Sat = "Cancellation On request";
        
    }
    return Sat;
}

function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}

function GetCurrencyDetails() {
   
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetCurrencyDetails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetCurrencyDetailsSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetCurrencyDetailsSuccess(datacur) {
        if (datacur.d != 'ERROR') {
            var currencydata = $.parseJSON(datacur.d);
			AgencyCurrencyExRate=currencydata[0].RateofExchange;
			AgencyCurrencyCode=currencydata[0].CurrencyCode;
        window.localStorage.setItem("AgencyCurrencyExRate", AgencyCurrencyExRate);
        window.localStorage.setItem("AgencyCurrencyCode", AgencyCurrencyCode);
            //console.log(currencydata);
        }
    }
}
function Savesessionsuccess() {

}