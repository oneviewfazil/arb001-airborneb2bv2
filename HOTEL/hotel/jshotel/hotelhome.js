﻿var serviceblocked = false;
var searchblocked = true;
var AgencyCurrencyCode = '';
var AgencyCurrencyExRate = '';
$(document).ready(function () {

    //--------Auto fill cookies for search boxes in search page start------------//
    GetHotelSearchHistory();
    //--------Auto fill cookies for search boxes in search page start------------// 

    if (!window.localStorage.getItem("HotelSuppliers")) {
        SessionGetHotelSuppliers();
    } else {
        SessionBindHotelSuppliers(JSON.parse(window.localStorage.getItem("HotelSuppliers")));
    }
    //Checkin Date
    var DateFormate = 'dd/mm/yy';

    var userDetails = {
        userSession: {
            accessToken: Setup.Session.AccessToken,
            //          userDetails: Setup.Session.UserDetails,
            loginUser: Setup.Session.LoginUser
        }
    };

    SessionCheckDetails(JSON.stringify(userDetails));
    SetMarkupDetails();

    var AgencyCurrencyExRateLocStrg = window.localStorage.getItem(Setup.Session.AgencyCurrencyExRate);
    var AgencyCurrencyCodeLocStrg = window.localStorage.getItem(Setup.Session.AgencyCurrencyCode);

    if (!AgencyCurrencyExRateLocStrg && !AgencyCurrencyCodeLocStrg) {
        GetCurrencyDetails();
    }

    //Hotel City
    $(".hotelcity").autocomplete({
        autoFocus: true,
        source: AreaCityList, minLength: 3, position: { my: "left top", at: "left bottom" },
        change: function (event, ui) { if (!ui.item) { $(this).val(''); $(this).prop('placeholder', 'No City Found !'); } },
        select: function (event, ui) {
            if (ui.item.id != 0) {
                if (ui.item.value.indexOf('No City Found !') < 0) {
                    this.value = ui.item.label;
                    $('#hdcitycode').val(ui.item.C);
                    $('#hdloctype').val(ui.item.T);
                }
            } return false;
        },
        //response: function (event, ui) {
        //    if (ui.content.length == 1) {
        //        console.log(ui)
        //        $(this).val(ui.content[0].value);
        //        $('#hdcitycode').val(ui.content[0].C);
        //        $('#hdloctype').val(ui.content[0].T);
        //        selectautotype = "single";
        //        $(this).autocomplete("close");              
        //    } 
        //},
        focus: function () { return false; }
    });
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
        var matcher1 = new RegExp("^" + "C", "i");
        var arraylist = array;
        var arraylist2 = array;
        var list1 = $.grep(arraylist, function (value) {
            return ((matcher1.test(value.T) && matcher.test(value.C)));
        });
        var list2 = $.grep(arraylist2, function (value) {
            return ((matcher1.test(value.T) && matcher.test(value.label)));
        });
        var list3 = list1.concat(list2);

        function removeDuplicates(arr) {
            var unique_array = []
            for (var i = 0; i < arr.length; i++) {
                if (unique_array.indexOf(arr[i]) == -1) {
                    unique_array.push(arr[i])
                }
            }
            return unique_array
        }
        var list4 = removeDuplicates(list3);
        return (list4);
        //return ($.grep(array, function (value) { return ((matcher1.test(value.T) && (matcher.test(value.C) || matcher.test(value.label)))); }));
    };

    ////Hotel City Mapped
    //$(".hotelcity").autocomplete({
    //    autoFocus: true,
    //    source: AreaCityListMapped, minLength: 3, position: { my: "left top", at: "left bottom" },
    //    change: function (event, ui) { if (!ui.item) { $(this).val(''); $(this).prop('placeholder', 'No City Found !'); } },
    //    select: function (event, ui) {
    //        if (ui.item.id != 0) {
    //            if (ui.item.value.indexOf('No City Found !') < 0) {
    //                console.log(ui)
    //                this.value = ui.item.lb;
    //                $('#hdcitycode').val(ui.item.CC);
    //                $('#hdloctype').val(ui.item.AW);
    //            }
    //        } return false;
    //    },
    //    focus: function () { return false; }
    //});
    //$.ui.autocomplete.filter = function (array, term) {
    //    var matcher = new RegExp($.ui.autocomplete.escapeRegex(term), "i");
    //    var matcher1 = new RegExp("^" + "CC", "i");
    //    return ($.grep(array, function (value) { return ((matcher1.test(value.AW) && (matcher.test(value.CC) || matcher.test(value.lb)))); })).slice(0, 15);
    //};

    //Date 1,2
    var tempnumberofmonths = parseInt(1);
    if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
        tempnumberofmonths = parseInt(2);
    }
    else if (parseInt($(window).width()) > 999) {
        tempnumberofmonths = parseInt(3);
    }

    $("#txtCheckInDate").datepicker({
        dateFormat: DateFormate,
        minDate: 0,
        maxDate: "360d",
        numberOfMonths: tempnumberofmonths,
        onSelect: function (date) {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
            startDate.setDate(startDate.getDate() + 1);
            dt2.datepicker('option', 'minDate', startDate);
            dt2.datepicker('setDate', minDate);
            //$(this).datepicker('option', 'minDate', minDate);
            //  $('#txtNights').val("1");
        },
        onClose: function (dateText, inst) {
            $('#txtCheckOutDate').focus();
        }
    });
    $("#txtCheckInDate").datepicker({ dateFormat: DateFormate, }).datepicker("setDate", new Date().getDay + 1);
    //Checkout Date
    $('#txtCheckOutDate').datepicker({
        dateFormat: DateFormate,
        minDate: +2,
        maxDate: "360d",
        numberOfMonths: tempnumberofmonths,
        onSelect: function (date) {
            var a = $("#txtCheckInDate").datepicker('getDate').getTime(),
                b = $("#txtCheckOutDate").datepicker('getDate').getTime(),
                c = 24 * 60 * 60 * 1000,
                diffDays = Math.round(Math.abs((a - b) / (c)));
            $('#txtNights').val(diffDays);
        }
    });
    $("#txtCheckOutDate").datepicker({ dateFormat: DateFormate, }).datepicker("setDate", new Date().getDay + 2);
    // $("#txtNights").val(1);
    $("#txtNights").keyup(function () {
        if ($(this).val() != "") {
            var numnights = parseInt(1);
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var minDate = $('#txtCheckInDate').datepicker('getDate');
            var checkstatus = parseInt(1);

            $(this).val($(this).val().replace(/[^0-9\.]/g, function () {
                var a = $("#txtCheckInDate").datepicker('getDate').getTime(),
                    b = $("#txtCheckOutDate").datepicker('getDate').getTime(),
                    c = 24 * 60 * 60 * 1000,
                    diffDays = Math.round(Math.abs((a - b) / (c)));
                numnights = parseInt(diffDays)
                startDate.setDate(startDate.getDate() + parseInt(numnights));
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('setDate', minDate);
                checkstatus = parseInt(0);
                alertify.alert('Validate', 'Only numbers allowed !');
            }));

            if (parseInt(checkstatus) == 1) {
                numnights = parseInt($('#txtNights').val());
                if (numnights > 90) {
                    numnights = parseInt(90);
                    alertify.alert('Maximum Nights', 'Maximum 90 Nights allowed!');
                }
                startDate.setDate(startDate.getDate() + numnights);
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('setDate', minDate);
            }
            $('#txtNights').val(numnights.toString());
        }
    });

    //CHD Change
    $(".ChildCount").change(function () {
        var CHDCount = $(this).find("option:selected").text();
        var ThisID = $(this).attr("id").split('_');
        var RoomID = ThisID[1];
        if (CHDCount == 0) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").hide();
            $("#DivRoom_" + RoomID + "_CHDAge_2").hide();
        }
        if (CHDCount == 1) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").show();
            $("#DivRoom_" + RoomID + "_CHDAge_2").hide();
        }
        if (CHDCount == 2) {
            $("#DivRoom_" + RoomID + "_CHDAge_1").show();
            $("#DivRoom_" + RoomID + "_CHDAge_2").show();
        }
    });
    //Room Count Change
    $("#selNumRoom").change(function () {
        var RoomCount = $(this).find("option:selected").text();
        for (var irt = 2; irt <= parseInt(RoomCount); irt++) {
            $("#r" + irt + "Div").show();
        }
        for (var irt = 5; irt > parseInt(RoomCount); irt--) {
            $("#r" + irt + "Div").hide();
        }
    });
    //max price
    $("#txtMaxPrice").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });



});

$(document).ajaxStop(function () {
    searchblocked = false;
});

function SessionChecking(session) {
    $(".loadingGIF").show();
    let Dastasseq = {
        sesname: session
    };

    function SessionSuccess(data) {
        if (data.d == Setup.Session.NoSession) {
            window.location.href = "/index.html";
        }
        else {
            var userdata = $.parseJSON(data.d);
            $('.username').html(userdata[0].FirstName);
            $('.AgencyName').html(userdata[0].AgencyName);
            var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
            $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
            var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
            }
            var AgencyID = userdata[0].AgencyID;
            GetMarkupValue(AgencyID);
            checkServiceBlocked(AgencyID);
            GetActionReqHotel(AgencyID);

        }
    }

    ajaxHelper(SessionSuccess, null, null, { url: Setup.Session.Url.GetSessionData, data: Dastasseq });

}
function GetMarkupValue(AgencyID) {
    //Get Markup
    let Dastasseq = {
        AgencyID: AgencyID,
        ServiceTypeID: '2'
    };

    function MarkupSuccess(datamark) {
        if (datamark.d != Setup.Session.NoMarkup) {
            var userdata = $.parseJSON(datamark.d);
            $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
        }
        else {
            $('.CreditLimitAmount').html('Credit Limit - No Credit !');
        }
    }

    ajaxHelper(MarkupSuccess, null, null, { url: Setup.Session.Url.GetMarkupDataHotel, data: Dastasseq });

}

function Search() {
    if (searchblocked) {
        setTimeout(function () { Search() }, 500);
        $("#searchspinner").show();
        $(".search").css("cursor", "not-allowed");
    }
    else {
        if (supplierstatus == true) {
            var countryofresidence = $('#selCoutryOfRes').find(":selected").val();
            var Nation = $('#selNationality').find(":selected").val();
            var Supplier = $('#selSupplier').find(":selected").val();
            var CheckinD = $('#txtCheckInDate').val();
            var cityname = $('#txtCity').val();
            var CheckoutD = $('#txtCheckOutDate').val();
            var NumNights = $('#txtNights').val();
            var NumRooms = $('#selNumRoom').find(":selected").val();
            var TotalPax = 0;
            var City = $('#hdcitycode').val();
            //var loctype = $('#hdloctype').val();

            if (CheckinD == "" && CheckoutD == "") {
                alertify.alert('Validation', 'Please fill checkin date and checkout date!');
                return false;
            }

            //if (CheckinD != '' && CheckoutD != '') {
            for (var irt = 1; irt <= parseInt(NumRooms); irt++) {
                TotalPax += parseInt($('#r' + irt + 'ADTCount').find(":selected").val()) + parseInt($('#Room_' + irt + '_CHDCount').find(":selected").val());
            }

            if (City == "NA" && CheckinD == "" && CheckoutD == "") {
                alertify.alert('Validation', 'Please select a city,checkin and checkout dates!');
                return false;
            }

            if (cityname.trim() == "") {
                alertify.alert('Validation', 'Please select a city!');
                return false;
            }

            if (City == "NA") {
                alertify.alert('Validation', 'Please select a city!');
                return false;
            }

            if (NumNights > 90) {
                alertify.alert('Validation', 'Maximum nights allowed in a single booking is 90!');
                return false;
            }

            if (NumNights == "") {
                alertify.alert('Validation', 'Number of nights must not be blank!');
                return false;
            }

            if (NumRooms == "") {
                alertify.alert('Validation', 'Number of rooms must not be blank!');
                return false;
            }

            if (TotalPax > 9) {
                alertify.alert('Validation', 'Maximum Passenger in a single booking is 9!');
                return false;
            }

            if (City != "NA" && NumRooms != "" && NumNights != "" && cityname != "") {
                window.localStorage.setItem("totpax", TotalPax);
                window.localStorage.setItem("cityname", $('#txtCity').val());
                window.localStorage.setItem("nation", Nation);
                window.localStorage.setItem("countryofresidence", countryofresidence);

                var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
                if (format.test($('#txtHotelName').val()) == true) {
                    alertify.alert('Invalid!', 'Avoid special characters!');
                    return false;
                }
                var Rooms = '';
                var roomForCookie = '';
                for (var inr = 1; inr <= parseInt(NumRooms); inr++) {
                    var CHDCount = parseInt($('#Room_' + inr + '_CHDCount').find(":selected").val());
                    roomForCookie += '=ADT_' + $('#r' + inr + 'ADTCount').find(":selected").val() +
                        ',CHD_' + CHDCount + '';
                    Rooms += '&room' + inr + '=ADT_' + $('#r' + inr + 'ADTCount').find(":selected").val() +
                        ',CHD_' + CHDCount + '';
                    for (var inr2 = 1; inr2 <= CHDCount; inr2++) {
                        Rooms += '_' + $('#r' + inr + 'CHD' + inr2 + 'Age').find(":selected").val() + '';
                        roomForCookie += '_' + $('#r' + inr + 'CHD' + inr2 + 'Age').find(":selected").val() + '';
                    }
                }
                //adv search paras
                var advpara = '';
                if ($('#txtHotelName').val().trim() != '') {
                    advpara += '&name=' + $('#txtHotelName').val();
                }
                if ($('#selStar').find(":selected").val() != 'ALL') {
                    advpara += '&star=' + $('#selStar').find(":selected").val();
                }
                if ($('#txtMaxPrice').val().trim() != '') {
                    advpara += '&mprice=' + $('#txtMaxPrice').val().trim().replace(/ /g, '');
                }
                if ($('#selSort').find(":selected").val() != '') {
                    advpara += '&sort=' + $('#selSort').find(":selected").val();
                }
                if (serviceblocked == false) {
                    window.location.href = "hotels.html?nat=" + Nation + "&countryofres=" + countryofresidence + "&city=" + City + "&cin=" + CheckinD + "&cout="
                        + CheckoutD + "&night=" + NumNights + "&numroom=" + NumRooms + "" + Rooms + advpara + "&suplr=" + Supplier + "";

                    //--------Auto fill cookies for search boxes in search page start------------//
                    var roomsTemp = '';
                    roomForCookie = roomForCookie.split('=').slice(1);
                    for (var irt = 0; irt < roomForCookie.length; irt++) {
                        if (roomsTemp == '') {
                            roomsTemp += roomForCookie[irt];
                        }
                        else {
                            roomsTemp += '/' + roomForCookie[irt];
                        }
                    }


                    var JsonSearch = {
                        HotelSearch: {
                            searchCriteria: {
                                nation: Nation,
                                countryofresidence: countryofresidence,
                                city: City,
                                cin: CheckinD,
                                cout: CheckoutD,
                                night: NumNights,
                                numroom: NumRooms,
                                rooms: roomsTemp,
                                supp_code: Supplier,
                                s_curr: "USD",
                                lang: "ENG",
                                AgencyCode: ""
                            }
                        }
                    }
                    $.cookie('HotelSearch', JSON.stringify(JsonSearch), { expires: 50 });

                }
                else {
                    alertify.alert('Blocked', 'Hotel Service Blocked!');
                }
            }

            //}
            //else {
            //  alertify.alert('Please fill!', 'Please fill checkin date and checkout date!');
            //}
        }
        else {
            alertify.alert('Please contact your admin!', 'You do not have any active suppliers!!');
        }
        $("#searchspinner").hide();
        $(".search").css("cursor", "pointer");
    }
}
function checkServiceBlocked(AgencyIDD) {
    //Get Markup
    let Dastasseq = {
        AgencyId: AgencyIDD,
        ServiceTypeID: '2'
    };

    function checkServiceBlockedSuccess(datablock) {
        if (datablock.d == '1') {
            serviceblocked = true;
            $('#hotel').remove();
            $('#hotelnoservice').show();
        }
        else {
            serviceblocked = false;
            $('#hotelnoservice').hide();
        }
    }

    ajaxHelper(checkServiceBlockedSuccess, null, null, { url: Setup.Session.Url.CheckServiceBlocked, data: Dastasseq });

}

//Action Required Hotel
function GetActionReqHotel(AgencyID) {
    $("#loadergifht").show();
    let Dastasseq = {
        AgencyID: AgencyID
    };

    function GetActionReqHotelSuccess(datacreeee) {
        if (datacreeee.d != Setup.Session.NoData) {
            var Searchdata = $.parseJSON(datacreeee.d);
            $('#divactHot').show();
            var searchresh = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                var BookStatName = '';
                if (Searchdata[isd].BookingStatusCode == 'HK') {
                    BookStatName = 'Confirmed';
                }
                else {
                    BookStatName = Searchdata[isd].BokingStatName;
                }
                searchresh += '<tr onclick="ActionReqHotelClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                    '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                    + Searchdata[isd].AgencyName + '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].ItemCityCode) + '">'
                    + Searchdata[isd].ItemCityCode + '</td><td>'
                    + Searchdata[isd].ItemName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].CheckInDate).format("ddd-MMM-YYYY") + ' - ' + Searchdata[isd].Nights + ' Nights">'
                    + moment(Searchdata[isd].CheckInDate).format("DD-MM-YY") + ' (' + Searchdata[isd].Nights + 'N)</td><td  data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].timelimit).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].timelimit).format("DD-MM-YY") + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                    '</td><td data-toggle="tooltip" data-placement="top" title="' + BookStatName + '">'
                    + Searchdata[isd].BookingStatusCode + '</td></tr>';
            }
            $("#ActReq2").html(searchresh);
            $('[data-toggle="tooltip"]').tooltip();
            $('#tableActHot').DataTable({
                "order": [[0, "desc"]]
            });
        }
        else {
            $('#divactHot').hide();
        }
        $("#loadergifht").hide();
        //$("#tableActHot").show();
    }

    ajaxHelper(GetActionReqHotelSuccess, null, null, { url: Setup.Session.Url.GetActionReqHotel, data: Dastasseq });

}
function ActionReqHotelClick(sid) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };

    ajaxHelper(ActionReqHotelClickSuccess, null, null, { url: Setup.Session.Url.SaveDataToSession, data: Datatable });

}
function ActionReqHotelClickSuccess() {
    window.location.href = Setup.Session.Pages.HotelBookingInfo;
}

function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}

function GetCurrencyDetails() {

    function GetCurrencyDetailsSuccess(datacur) {
        if (datacur.d != 'ERROR') {
            var currencydata = $.parseJSON(datacur.d);
            AgencyCurrencyExRate = currencydata[0].RateofExchange;
            AgencyCurrencyCode = currencydata[0].CurrencyCode;
            window.localStorage.setItem(Setup.Session.AgencyCurrencyExRate, AgencyCurrencyExRate);
            window.localStorage.setItem(Setup.Session.AgencyCurrencyCode, AgencyCurrencyCode);
        }
    }

    ajaxHelper(GetCurrencyDetailsSuccess, null, null, { url: Setup.Session.Url.GetCurrencyDetails, data: null });
}
function Postbooking() {

    if ($('#chkpostbook').is(':checked')) {
        $("#usernametb").hide();
        BindAgencydetails();


    }
    else {
        $("#divpostbookdropdown").hide();
        $("#usernametb").hide();
    }
}

function BindAgencydetails() {

    function BindAgencydetailsSuccess(data) {
        //if (data.d == 'NORESULT') {
        //    alert("Your session has expired");
        //    window.location.href = "/index.html?lout=true";
        //}
        //else {
        $("#divpostbookdropdown").show();
        var postbookingdata = $.parseJSON(data.d);
        var childnodelength = postbookingdata.children.length
        var Agencyiid = '';
        var option = '<option value="">--select--</option >'
        for (var i = 0; i < childnodelength; i++) {
            option += '<option value="' + postbookingdata.children[i].id + '">' + postbookingdata.children[i].name + '</option>';
            //Agencyiid = postbookingdata.children[i].id
        }
        $('#slagency').html(option);
    }
    ajaxHelper(BindAgencydetailsSuccess, null, null, { url: Setup.Session.Url.PostBooking, data: null });

}


function BindAgencyUser() {
    var Agencyiid = $('#slagency').val();
    let Dastasseq = {
        AgencyID: Agencyiid,

    };

    function BindAgencyUserdetailsSuccess(data) {
        //if (data.d == 'NORESULT') {
        //    alert("Your session has expired");
        //    window.location.href = "/index.html?lout=true";
        //}
        //else {
        // $("#divpostbookdropdown").show();
        $("#usernametb").show();
        var postbookinguserdata = $.parseJSON(data.d);
        var userlength = postbookinguserdata.length;
        var option2 = '<option value="">--select--</option >'
        for (var i = 0; i < userlength; i++) {

            option2 += '<option value="' + postbookinguserdata[i].id + '">' + postbookinguserdata[i].name + '</option>';

        }

        $('#slusername').html(option2);


    }

    ajaxHelper(BindAgencyUserdetailsSuccess, null, null, { url: Setup.Session.Url.PostBookingByUserId, data: Dastasseq });
}
//--------Auto fill cookies for search boxes in search page start------------//

function GetHotelSearchHistory() {
    $(".chdage, #r2Div, #r3Div, #r4Div, #r5Div, #r6Div, #r7Div, #r8Div, #r9Div").hide();
    var history = '';
    try {
        history = $.cookie('HotelSearch');
    }
    catch (errr) { history = ''; }
    if (history != undefined && history != '') {
        history = JSON.parse(history).HotelSearch.searchCriteria;
        $('#txtCity').val(getAreaName(history.city));
        $('#hdcitycode').val(history.city);
        //    $('#selSupplier').val(history.supp_code);
        //$('#selCoutryOfRes').val(history.countryofresidence);
        //$('#selNationality').val(history.nation);

        // $('#txtCheckInDate').val(moment(history.CinMDY, DateFormate).format("DD/MM/YYYY"));
        // $('#txtCheckOutDate').val(moment(history.CoutMDY, DateFormate).format("DD/MM/YYYY"));

        $('#txtNights').val('1');
        $('#selNumRoom').val(history.numroom);
        var RoomCount = history.rooms.split('/');
        var Paxes = [];
        var ir = parseInt(1)
        for (var irt = 0; irt < parseInt(history.numroom); irt++) {
            $("#r" + ir + "Div").show();
            var roomEach = RoomCount[irt];
            Paxes = roomEach.split(',');
            for (var ipax = 0; ipax < Paxes.length; ipax++) {
                var PaxTypeCount = []
                PaxTypeCount = Paxes[ipax].split('_');
                if (PaxTypeCount[0] == "ADT") {
                    $('#r' + ir + 'ADTCount').val(PaxTypeCount[1])
                    $('#r' + ir + 'ADTCount').show();
                }
                if (PaxTypeCount[0] == "CHD" && PaxTypeCount[1] != "0") {
                    $('#Room_' + ir + '_CHDCount').val(PaxTypeCount[1])
                    var ich = parseInt(1);
                    for (var ichcount = 0; ichcount < parseInt(PaxTypeCount[1]); ichcount++) {
                        $('#r' + ir + 'CHD' + ich + 'Age').val(PaxTypeCount[ichcount + 2]);
                        $('#r' + ir + 'CHD' + ich + 'Age').show();
                        $('#DivRoom_' + ir + '_CHDAge_' + ich).show();
                        ich++;
                    }

                    $('#Room_' + ir + '_CHDCount').show();

                }
            }

            ir++;
        }
    }
    else {
        $('#txtNights').val("1");
        $(".chdage, #r2Div, #r3Div, #r4Div, #r5Div, #r6Div, #r7Div, #r8Div, #r9Div").hide();
    }
}
function getAreaName(AreaCode) {
    var AirportName = _.where(AreaCityList, { C: AreaCode });
    if (AirportName == "") {
        AirportName = AreaCode;
    }
    else {
        AirportName = AirportName[0].label;
    }
    return AirportName;
}
//--------Auto fill cookies for search boxes in search page end------------//

function SessionCheckDetails(session) {
    let Dastasseq = {
        sesname: session
    };

    function SessionCheckingSuccess(data) {

        if (data.d.Access_Token == Setup.Session.NoSession) {
            window.location.href = "/index.html";
        }
        else {
            var accesstocken = data.d.Access_Token;
            window.localStorage.setItem("accesstocken", accesstocken);
        }

        if (data.d.Loginuser == Setup.Session.NoSession) {
            window.location.href = "/index.html";
        }
        else {
            var logininfo = $.parseJSON(data.d.Loginuser);
            var agcode = logininfo.user.loginNode.code;
            var agycodesplit = agcode.match(/\d+/);
            var Agycode = agycodesplit[0];
            window.localStorage.setItem("Agycode", Agycode);
            $('.username').html(logininfo.user.firstName);
            $('.AgencyName').html(logininfo.user.loginNode.name);
            var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
            $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
            //var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            //if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            //    $('.CreditLimitAmount').hide();
            //}
            //var AgencyID = userdata[0].AgencyID;
            //GetMarkupValue(AgencyID);
        }


    }

    ajaxHelper(SessionCheckingSuccess, null, null, { url: Setup.Session.Url.GetSessionDataNew, data: Dastasseq });
}


function ajaxHelper(successCallback, failureCallback, errorCallback, requestData) {

    var data = {};

    data.type = "POST";
    data.url = requestData.url;

    if (requestData.data) {
        data.data = JSON.stringify(requestData.data);
    }

    data.contentType = "application/json; charset=utf-8";
    data.dataType = "json";
    data.success = successCallback;

    if (failureCallback) {
        data.failure = failureCallback;
    } else {
        data.failure = function (response) {
        }
    }

    if (errorCallback) {
        data.error = errorCallback;
    }


    if (navigator.onLine) {
        $.ajax(data);
    } else {
        alertify.alert('No Connection', 'There seems to be a network issue. Please try again later.', function () {
            window.location.href = "/HOTEL/searchhotel.html";
        });
    }
}
