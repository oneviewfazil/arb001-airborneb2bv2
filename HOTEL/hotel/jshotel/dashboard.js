﻿var Currency = 'USD';
var TotBookFlight = parseFloat(-1);
var TotBookHotel = parseFloat(-1);
var TotBookinsurance = parseFloat(-1);
$(document).ready(function () {
    SessionChecking('UserDetails');
});
function SessionChecking(session) {
    $(".loadingGIF").show();
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SessionSuccess(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var userdata = $.parseJSON(data.d);
            console.log(userdata);
            $('.username').html(userdata[0].FirstName);
            $('.AgencyName').html(userdata[0].AgencyName);
            var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
            $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
            var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
                $('.AvaCreLmtAmt , .onlysubagency').hide();
            }

            var AgencyID = userdata[0].AgencyID;
            GetMarkupValue(AgencyID);
            GetCreditInfo(AgencyID);
            GetBookingStatCount(AgencyID, 1);
            GetBookingStatCount(AgencyID, 2);
            GetBookingStatCount(AgencyID, 4);
            GetActionReqFlight(AgencyID, AgencyTypeIDD);
            GetActionReqHotel(AgencyID);
            GetActionReqInsurance(AgencyID);
        }
    }
}
function GetMarkupValue(AgencyID) {
    //Get Markup
    var Dastasseqq = {
        AgencyID: AgencyID,
        ServiceTypeID: '2'
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetMarkupDataHotel",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: MarkupSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function MarkupSuccess(datamark) {
        if (datamark.d != 'NOMARKUP') {
            var userdata = $.parseJSON(datamark.d);
            console.log(userdata);
            $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
            $('.AvaCreLmtAmt').html('Available Credit : ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);

        }
        else {
            $('.CreditLimitAmount').html('Credit Limit - No Credit !');
        }
    }
}

//Get Credit info
function GetCreditInfo(AgencyID) {
    //Get Markup
    var Dastasseqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetSumDepoCreditPayment",
        data: JSON.stringify(Dastasseqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetCreditInfoSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetCreditInfoSuccess(datacre) {
        if (datacre.d != 'NODATA') {
            var datacree = $.parseJSON(datacre.d);
            console.log(datacree);
            $('#totDeposit').html('Total Deposit : ' + datacree[0].TotalDeposit + ' ' + Currency);
            $('#CrLimit').html('Credit Limit : ' + datacree[0].TotalCredit + ' ' + Currency);
            $('#util').html('Utilized : ' + datacree[0].TotalPayment + ' ' + Currency);
        }
    }
}
//Get Hotel Booking Info
function GetBookingStatCount(AgencyID, ServiceTypeId) {
    //Get Markup
    var Dastasseqqqq = {
        AgencyID: AgencyID,
        ServiceTypeId: ServiceTypeId
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetBookingStatCount",
        data: JSON.stringify(Dastasseqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetBookingStatCountSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetBookingStatCountSuccess(datacree) {
        if (datacree.d != 'NODATA') {
            var datacreee = $.parseJSON(datacree.d);
            console.log(datacreee);
            $('#RQ' + ServiceTypeId).html(datacreee[0].StatusRequested);
            $('#RR' + ServiceTypeId).html(datacreee[0].PendingConfirmation);
            $('#HK' + ServiceTypeId).html(datacreee[0].ConfirmedNotTicketed);
            $('#XR' + ServiceTypeId).html(datacreee[0].CancellationRequest);
            $('#XX' + ServiceTypeId).html(datacreee[0].Cancelled);
            $('#TOT' + ServiceTypeId).html(datacreee[0].TotalBooking);
            if (ServiceTypeId == '1') {
                $('#RC' + ServiceTypeId).html(datacreee[0].TicketIssued);
                TotBookFlight = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '2') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                $('#RJ' + ServiceTypeId).html(datacreee[0].Rejected);
                TotBookHotel = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '4') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                TotBookinsurance = datacreee[0].TotalBooking;
            }
            if (TotBookFlight == 0 && TotBookHotel == 0) {
                $('#divWaiting').show();
            }
        }
    }
}
//Action Required Flight
function GetActionReqFlight(AgencyID, AgencyTypeIDDD) {
    $("#loadergiffl").show();
    //$("#tableActFl").hide();
    var Dastasseqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetActionReqFlight",
        data: JSON.stringify(Dastasseqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqFlightSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqFlightSuccess(datacreee) {
        if (datacreee.d != 'NODATA') {
            var Searchdata = $.parseJSON(datacreee.d);
            console.log(Searchdata);
            $('#divactFl').show();
            var searchresf = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                searchresf += '<tr onclick="ActionReqFlightClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                    '</td><td data-toggle="tooltip" data-placement="top" title="Agency Name:' + Searchdata[isd].AgencyName + '">'
                    + Searchdata[isd].FirstName + '</td><td class="onlyhq">' + Searchdata[isd].AgencyName + '</td><td class="no_mobile">' + Searchdata[isd].SupplierBookingReference +
                    '</td><td class="no_mobile">' + Searchdata[isd].IstPax + '</td><td data-toggle="tooltip" data-placement="top" title="'
                    + getAirportNameUTC(Searchdata[isd].DepartureAirportLocationCode) + ' to ' + getAirportNameUTC(Searchdata[isd].ArrivalAirportLocationCode) + '">'
                    + Searchdata[isd].DepartureAirportLocationCode + ' to ' + Searchdata[isd].ArrivalAirportLocationCode + '</td><td data-toggle="tooltip" data-placement="top" title="' + Searchdata[isd].BokingStatName + '">'
                    + Searchdata[isd].BookingStatusCode + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                    '</td><td  data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].CancellationDeadline).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].CancellationDeadline).format("DD-MM-YY") + '</td></tr>';
            }
            $("#ActReq1").html(searchresf);
            $('[data-toggle="tooltip"]').tooltip();
            $('#tableActFl').DataTable({
                "order": [[0, "desc"]]
            });
            if (AgencyTypeIDDD != '1') {
                $('.onlyhq').remove();
            }
        }
        else {
            $('#divactFl').hide();
        }
        $("#loadergiffl").hide();
        //$("#tableActFl").show();
    }
}
function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}
function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}
function ActionReqFlightClick(sid) {
    var Datatable = {
        sessionname: 'BookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqFlightClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function ActionReqFlightClickSuccess() {
    window.location.href = "/bookinginfo.html";
}
//Action Required Hotel
function GetActionReqHotel(AgencyID) {
    $("#loadergifht").show();
    //$("#tableActHot").hide();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetActionReqHotel",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqHotelSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqHotelSuccess(datacreeee) {
        if (datacreeee.d != 'NODATA') {
            var Searchdata = $.parseJSON(datacreeee.d);
            console.log(Searchdata);
            $('#divactHot').show();
            var searchresh = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                var BookStatName = '';
                if (Searchdata[isd].BookingStatusCode == 'HK') {
                    BookStatName = 'Confirmed';
                }
                else {
                    BookStatName = Searchdata[isd].BokingStatName;
                }
                searchresh += '<tr onclick="ActionReqHotelClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                + Searchdata[isd].AgencyName + '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].ItemCityCode) + '">'
                + Searchdata[isd].ItemCityCode + '</td><td>'
                + Searchdata[isd].ItemName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].CheckInDate).format("ddd-MMM-YYYY") + ' - ' + Searchdata[isd].Nights + ' Nights">'
                + moment(Searchdata[isd].CheckInDate).format("DD-MM-YY") + ' (' + Searchdata[isd].Nights + 'N)</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].timelimit).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].timelimit).format("DD-MM-YY") + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + BookStatName + '">'
                + Searchdata[isd].BookingStatusCode + '</td></tr>';
            }
            $("#ActReq2").html(searchresh);
            $('[data-toggle="tooltip"]').tooltip();
            $('#tableActHot').DataTable({
                "order": [[0, "desc"]]
            });
        }
        else {
            $('#divactHot').hide();
        }
        $("#loadergifht").hide();
        //$("#tableActHot").show();
    }
}
function ActionReqHotelClick(sid) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqHotelClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function ActionReqHotelClickSuccess() {
    window.location.href = "/hotelbookinginfo.html";
}

function GetActionReqInsurance(AgencyID) {
    $("#loadergifht").show();
    //$("#tableActHot").hide();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "WebMethodDbInsurance.aspx/GetActionReqInsurance",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchSuccess(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divactInsu').show();
        var Searchdata = $.parseJSON(datab.d);
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                + Searchdata[isd].AgencyName + '</td><td>'
                + Searchdata[isd].Premium + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].StartDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].StartDate).format("DD-MM-YY") + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].EndDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].EndDate).format("DD-MM-YY") + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
                + Searchdata[isd].BookingStatusCode + '</td></tr>';

        }
        $("#ActReq4").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableActInsu').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableActInsu').show();

    }
    else {
        $('#divactInsu').hide();
    }
    $("#loadergifht").hide();

}
function GetStatus(status) {
    var Sat = "";
    if (status == "HK") {
        Sat = "Confirmed";

    }
    else if (status == "RR") {
        Sat = "Confirmation Pending";

    }
    else if (status == "XX") {
        Sat = "Cancelled";
    }
    else if (status == "RC") {
        Sat = "Reconfirmed";

    }
    else if (status == "RQ") {
        Sat = "On request";

    }
    else if (status == "XR") {
        Sat = "Cancellation On request";

    }
    return Sat;
}

function SearchDetailsClick(sid) {
    var Datatable = {
        sessionname: 'InsuranceBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess() {
    window.location.href = "/Policyinfo.html";
}