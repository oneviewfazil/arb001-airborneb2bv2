﻿var travelagencyname = Setup.Agency.AgencyName;
var AgencyShortName = Setup.Agency.AgencyShortName;
var Address = Setup.Agency.AgencyAddress;
var Website = Setup.Agency.AgencyWebsite;
var Helpline = Setup.Agency.Helpline;
var Email = Setup.Agency.Email;

var MarkupTypeID = parseFloat(0);
var MarkupValue = parseFloat(0);
var MarkupTypeIDHotel = parseFloat(0);
var MarkupValueHotel = parseFloat(0);
var EUName = '';
var EUserID = '';
var EMailID = '';
var RoleConfirm = parseFloat(0);
var RoleAutoTkt = parseFloat(0);
var UserID = '';
var AgencyName = '';
var AgencyAddress = '';
var AgencyContact = '';
//var AgencyIDD = '<%=ConfigurationManager.AppSettings["AgencyID"].ToString() %>';
var AgencyIDD = '118';

var Header = '<div class="container"><div class="main_header ">' +
    '<div class="logo_area"><a href="index.html"><img src="/COMMON/images/logo.png" alt=""></a></div>' +
    '<div class="header"><a href="#menu"><span></span></a></div>';
Header += '<nav id="menu"><div class="left_menu_sec"><div class="users_info"><h2 class="username"></h2>' +
    '<h3 class="AgencyName"></h3><h3 class="CreditLimitAmount"></h3></div><ul>' +
    //Mobile Side Menu
    '<li><a href="index.html"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</a></li>' +
    '<li><a href="profileinformation.html"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>' +
    '<li class="rleflight"><a href="index.html"><i class="fa fa-plane" aria-hidden="true"></i>Book a seat</a></li>' +
    '<li class="rlehotel"><a href="/HOTEL/searchhotel.html"><i class="fa fa-bed" aria-hidden="true"></i>Book a pillow</a></li>' +
    '<li><a href="Signin.html"><i class="fa fa-power-off" aria-hidden="true"></i>Login</a></li>' +
    //Activity Start 
    //'<li class="rlesight"><a href="searchsightseeing.html"><i class="fa fa-binoculars" aria-hidden="true"></i>Book an Activity</a></li>' +
    //Activity End
    //insurance Start 
    //'<li class="rleinsurance"><a href="searchinsurance.html"><i class="fa fa-handshake-o" aria-hidden="true"></i>Book insurance</a></li>' +
    //insurance End
    '<li class="rlereportflight"><a href="searchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Seat Bookings</a></li>' +
    '<li class="rlereporthotel"><a href="hotelsearchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Pillow Bookings</a></li>' +
    '<li class="rleonrequest"><a href="hotelonrequest.html"><i class="fa fa-search" aria-hidden="true"></i>On Request Pillow Bookings</a></li>' +
    '<li class="rlereportinsurance"><a href="Insurancesearchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Insurance Bookings</a></li>' +
    '<li class="cliklogout"><a href=""><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>' +
    '<!--<li><a class="left_padding icon02" href="">Hotels</a></li><li><a class="left_padding icon03" href="">Cars</a></li>-->' +
    '</ul></div></nav>' +
    '<div class="header_menu"><ul>' +
    //Header Menu
    '<li id="headerlinkflight" class="flight_icon rleflight"><i class="fa fa-plane hdr_ico" aria-hidden="true"></i><a href="index.html">Seats</a></li>' +
    '<li id="headerlinkhotel" class="hotel_icon rlehotel"><i class="fa fa-bed hdr_ico01" aria-hidden="true"></i><a href="/HOTEL/searchhotel.html">Pillows</a></li>' +
    //Activity Start 
    //'<li id="brdsightseeing" class="hotel_icon rlesight"><i class="fa fa-binoculars hdr_ico01" aria-hidden="true" style="font-size: 15px !important; margin-left: -21px; margin-top: 3px;"></i><a href="searchsightseeing.html">Activities</a></li>' +
    //Activity End
    //insurance Start 
    //'<li id="headerlinkinsurance" class="hotel_icon rleinsurance"><i class="fa fa-handshake-o hdr_ico01" aria-hidden="true" style="font-size: 15px !important; margin-left: -21px; margin-top: 3px;"></i><a href="searchinsurance.html">Insurance</a></li>' +
    //insurance End
    '<!--<li class="car_icon"><a href="">Cars</a></li>--></ul></div>';
Header += '<div id="divlogin"><div id="divuser" class="user-menu pull-right profile_view"><div class="user-info"><div class="profile-img" style="background-image:url(/COMMON/images/userpic.png)"></div>' +
    '<div class="top_info_padding"><h3 class="name">Hello! <span class="username"></span></h3><h4 class="name">Your Account</h4></div></div>' +
    '<div class="profile_view_toggle"><div class="list_first"><ul>' +
    //Profile Links
    '<li><b class="CreditLimitAmount"></b></li>' +
    '<li><a href="profileinformation.html">My Profile</a></li>' +
    '<!--<li><a href="">My Bookings</a></li>--><!--<li><a href="searchbooking.html">Search Bookings</a></li>--></ul></div>' +
    '<div class="list_first01"><ul><!--<li><a href="">View E-Ticket</a></li>' +
    '<li><a href="">Cancel Bookings</a></li><li><a href="">Change Travel Date</a></li>--><!--<li><a href="">Make Payment</a></li>-->' +
    '<li class="cliklogout"><a>Logout</a></li></ul></div></div></div></div></div></div>';

var Footer = '<section class="thirdbelt"> <p class="light">' + Address + '</p> <hr class="last"> <p>© ' + (new Date()).getFullYear() + ' ' + Website + '. All Rights Reserved.</p> <p class="semibold">' + travelagencyname + '</p> </section>';

var Sidebar = '<div class="common-box"> <div class="admin_menu"> <ul class="accordion">' +
    //Sidebar Admin
    '<li><div class="link sp10 clrchange">Admin<i class="fa fa-chevron-down"></i></div><ul class="submenu">' +
    '<div class="links_area"> <a id="SideMenuDashboard" href="index.html">Dashboard</a> </div>' +
    '<div class="links_area rleonrequest"> <a id="searchpillowonrequest" href="hotelonrequest.html">On Request Pillow Bookings</a></div>' +
    '<div class="links_area rleblockservice"> <a id="SideMenuBlockService" href="BlockService.html">Block Service</a> </div>' +
    '</ul> </li>' +
    //Sidebar Booking
    '<li> <div class="link sp10 clrchange">Booking<i class="fa fa-chevron-down"></i></div> <ul class="submenu">' +
    '<div class="links_area rleflight"> <a id="SideMenuBookFlight" href="index.html">Book a seat</a> </div>' +
    '<div class="links_area rlehotel"> <a id="SideMenuBookHotel" href="/HOTEL/searchhotel.html">Book a pillow</a> </div>' +
    //'<div class="links_area rlesight"> <a id="SideMenusighthotel" href="searchsightseeing.html">Book an activity</a> </div>' +
    //'<div class="links_area rleinsurance"> <a id="SideMenuBookInsurance" href="searchinsurance.html">Book insurance</a> </div>' +
    ' </ul> </li>' +
    //Sidebar Search
    '<li> <div class="link sp10 clrchange">Search<i class="fa fa-chevron-down"></i></div> <ul class="submenu">' +
    '<div class="links_area rlereportflight"> <a id="SideMenuSearchBookings"  href="searchbooking.html">Search Seat Bookings</a> </div>' +
    '<div class="links_area rlereporthotel"> <a id="searchpillow" href="hotelsearchbooking.html">Search Pillow Bookings</a> </div>' +
    '<div class="links_area rlereportinsurance"> <a id="searchinsurance" href="Insurancesearchbooking.html">Search insurance Bookings</a> </div> ' +
    '</ul> </li></ul></div></div>' +
    '<div class="admin_details"> <h1 class="username"></h1> <h2 class="AgencyName"></h2>' +
    '<h2 class="CreditLimitAmount"></h2> <h2 id="datetimenow"></h2> </div>';

var RoleBooksHotel = -1;
var RoleBooksSighntseeing = -1;
var DisableDev = true;

$(document).ready(function () {
    document.title = travelagencyname;
    $('header').html(Header);
    $('footer').html(Footer);
    $('.admin_control_area').html(Sidebar);
    FetchMarkupFlight();
    FetchMarkupHotel();
    SessionChecking('UserDetails');
    //RoleCheckingg();

    $('.help_number').html('<i class="fa fa-phone-square" aria-hidden="true"></i>' + Helpline);
    $('.help_email').html('<i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:' + Email + '">' + Email + '</a>');
    //logout
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                var pageURL = $(location).attr("href");
                window.location.href = pageURL;
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
});
// login session checking

function SessionChecking(session) {
    $(this).attr("disabled", true);
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function loginback() {
    var pageURL = $(location).attr("href");
    window.localStorage.setItem("returl", pageURL)
}

function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        $('#divlogin').append('<a href="Signin.html" onclick="loginback()"><div class="user_login pull-right profile_view"><div class="user-info"><i class="fa fa-lock" aria-hidden="true"></i>LOGIN</div></div></a>');
        $('#divloginmobile').append('<ul><li><a href="index.html" onclick="loginback()"><i class="fa fa-user" aria-hidden="true"></i>Login</a></li> <li><a href="register.html"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign up</a></li></ul>');
        $('#divloginmobile').show();
        $('#divloginmobileuser').hide();
        $('#divuser').hide();
        //$('#divlogin').hide();
        $('#loadLoginGXV').show();
        var Dastasseq = {
            AgencyIDD: AgencyIDD
        };
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodDbHotel.aspx/GetAgencyData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: AgencySuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
        function AgencySuccess(data) {
            var agencydata = JSON.parse(data.d);
            //AgencyName = agencydata[0].AgencyName;
            //AgencyAddress = agencydata[0].Address;
            //AgencyContact = agencydata[0].Office;
            AgencyName = "Karnak";
            AgencyAddress = "Karnak, Egypt";
            AgencyContact = "9895820499";
        }
        if (window.location.pathname === '/m_hotelmybookinginfo.html' || window.location.pathname === '/hotelmybookinginfo.html' || window.location.pathname === '/profileinformation.html') {
            window.location.href = "//HOTEL/searchhotel.html";
        }

    }
    else {
        $('#loadLoginGXV').hide();
        if (window.location.pathname === '/register.html') {
            window.location.href = "/search.html";
        }
        console.log(JSON.parse(data.d));
        var userdata = JSON.parse(data.d);
        UserID = userdata[0].UserID;
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        EUName = userdata[0].FirstName + ' ' + userdata[0].LastName;
        EUserID = userdata[0].UserID;
        EMailID = userdata[0].EmailID;
        var userdata = $.parseJSON(data.d);
        $('#txtFirstName').val(userdata[0].FirstName);
        $('#txtSurName').val(userdata[0].LastName);
        $('#ddlTitle').val(userdata[0].TitleID);
        $('#txtAdd1').val(userdata[0].Address1);
        $('#txtAdd2').val(userdata[0].Address2);
        $('#txtEmail').val(userdata[0].EmailID);
        $('#txtZipCode').val(userdata[0].Postalcode);
        $('#txtCity').val(userdata[0].City);
        $('#ddlCountry').val(userdata[0].Country);
        var mobile = userdata[0].Mobile;
        var arr = mobile.split(',');
        $('#txtAreaCityCode').val(arr[0]);
        $('#txtPhoneNumber').val(arr[1]);
        $('#txtPhoneNumber').val(userdata[0].Mobile);
        if ($(window).width() < 990 && $(window).width() > 200) {
            $('#divuser').hide();

        }
        else {
            $('#divuser').show();

        }
        $('.AgencyName').html(userdata[0].AgencyName);
        AgencyName = userdata[0].AgencyName;
        AgencyAddress = userdata[0].Address;
        AgencyContact = userdata[0].Office;
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }




        checkServiceBlocked();
        checkServiceBlockedHotel();
    }
}
function FetchMarkupFlight() {    //Get Markup
    var Dastasseqq = {
        AgencyID: AgencyIDD,
        ServiceTypeID: '1'
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetMarkupData",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: MarkupSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });

}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);


        MarkupTypeID = userdata[0].MarkupTypeID;
        MarkupValue = userdata[0].MarkupValue;

    }
}

//Hotel Markup Fetching
function FetchMarkupHotel() {
    //Get Markup
    var Dastasseqq = {
        AgencyID: AgencyIDD,
        ServiceTypeID: '2'
    };
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetMarkupDataHotel",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: FetchMarkupHotelSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function FetchMarkupHotelSuccess(datamark) {
        if (datamark.d != 'NOMARKUP') {
            var userdata = $.parseJSON(datamark.d);
            console.log(userdata);
            MarkupTypeIDHotel = userdata[0].MarkupTypeID;
            MarkupValueHotel = userdata[0].MarkupValue;
        }

    }
}
//Hotel Markup Fetching
//login session checking 
//Prevent dev option
$(document).keydown(function (event) {
    if (location.hostname !== "localhost" && DisableDev == true) {
        if (event.keyCode == 123) {
            return false;
        }
        else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
            return false;
        }
    }
});
//Prevent dev option
$(document).on("contextmenu", function (e) {
    if (location.hostname !== "localhost" && DisableDev == true) {
        e.preventDefault();
    }
});

function RoleCheckingg() {

    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetRoleStatus",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: RoleSuccesss,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function RoleSuccesss(datarole) {
    var Roledata = $.parseJSON(datarole.d);
    console.log(Roledata);

    //Hide service contents from dashboard
    //Flight
    var RoleServiceFlight = _.find(Roledata, function (item) {
        return item.RoleID == "1";
    }).RoleStatus;
    if (RoleServiceFlight == 0) {
        $(".rleserflight").remove();
    }
    else {
        $(".rleserflight").show();
    }

    //Hotel
    var RoleServiceHotel = _.find(Roledata, function (item) {
        return item.RoleID == "2";
    }).RoleStatus;
    if (RoleServiceHotel == 0) {
        $(".rleserhotel").remove();
    }
    else {
        $(".rleserhotel").show();
    }

    //Sightseeing
    var RoleServiceSight = _.find(Roledata, function (item) {
        return item.RoleID == "26";
    }).RoleStatus;
    if (RoleServiceSight == 0) {
        $(".rlesight").remove();
    }
    else {
        $(".rlesight").show();
    }

    //insurance
    var RoleServiceInsurance = _.find(Roledata, function (item) {
        return item.RoleID == "44";
    }).RoleStatus;
    if (RoleServiceInsurance == 0) {
        $(".rleinsurance").remove();
    }
    else {
        $(".rleinsurance").show();
    }

    //Onrequestbookingrole
    var RoleServiceonrequest = _.find(Roledata, function (item) {
        return item.RoleID == "21";
    }).RoleStatus;
    if (RoleServiceonrequest == 0) {
        $(".rleonrequest").remove();
    }
    else {
        $(".rleonrequest").show();
    }


    //Hide menu and footer contents
    var RoleFlight = _.find(Roledata, function (item) {
        return item.RoleID == "3";
    }).RoleStatus;
    if (RoleFlight == 0) {
        $(".rleflight").remove();
        if (window.location.pathname === '/index.html' || window.location.pathname === '/travellerdetails.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rleflight").show();
    }

    var RoleHotel = _.find(Roledata, function (item) {
        return item.RoleID == "10";
    }).RoleStatus;
    if (RoleHotel == 0) {
        $(".rlehotel").remove();
        if (window.location.pathname === '//HOTEL/searchhotel.html' || window.location.pathname === '/hotels.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rlehotel").show();
    }
    //Sighseeing
    var RoleSighseeing = _.find(Roledata, function (item) {
        return item.RoleID == "27";
    }).RoleStatus;
    if (RoleSighseeing == 0) {
        $(".rlesight").remove();
        if (window.location.pathname === '/searchsightseeing.html' || window.location.pathname === '/sightseeing.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rlesight").show();
    }

    //insurance
    var Roleinsurance = _.find(Roledata, function (item) {
        return item.RoleID == "44";
    }).RoleStatus;
    if (Roleinsurance == 0) {
        $(".rleinsurance").remove();
        if (window.location.pathname === '/searchinsurance.html' || window.location.pathname === '/insurance.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rleinsurance").show();
    }

    //Reports flight
    var RoleReportsFlight = _.find(Roledata, function (item) {
        return item.RoleID == "17";
    }).RoleStatus;
    if (RoleReportsFlight == 0) {
        $(".rlereportflight").remove();
        if (window.location.pathname === '/searchbooking.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rlereportflight").show();
    }

    //Reports hotel
    var RoleReportsHotel = _.find(Roledata, function (item) {
        return item.RoleID == "18";
    }).RoleStatus;
    if (RoleReportsHotel == 0) {
        $(".rlereporthotel").remove();
        if (window.location.pathname === '/hotelsearchbooking.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rlereporthotel").show();
    }

    //Reports insurance
    var RoleReportsInsurance = _.find(Roledata, function (item) {
        return item.RoleID == "45";
    }).RoleStatus;
    if (RoleReportsInsurance == 0) {
        $(".rlereportinsurance").remove();
        if (window.location.pathname === '/Insurancesearchbooking.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $(".rlereportinsurance").show();
    }
    //Book Hotel
    RoleBooksHotel = _.find(Roledata, function (item) {
        return item.RoleID == "11";
    }).RoleStatus;
    //Book Sightseeing
    RoleBooksSighntseeing = _.find(Roledata, function (item) {
        return item.RoleID == "28";
    }).RoleStatus;

    //Book Flight

    RoleConfirm = _.find(Roledata, function (item) {
        return item.RoleID == "4";
    }).RoleStatus;
    if (RoleConfirm == 0) {
        //$("#btnBook").hide();
        $("#btnBook").remove();
    }
    else {
        $("#btnBook").show();
    }

    RoleAutoTkt = _.find(Roledata, function (item) {
        return item.RoleID == "7";
    }).RoleStatus;
    //Book Flight
    //Block Service
    var RoleBookStatusHotel = _.find(Roledata, function (item) {
        return item.RoleID == "25";
    }).RoleStatus;
    if (RoleBookStatusHotel == 0) {
        $('.rleblockservice').remove();
        if (window.location.pathname === '/BlockService.html') {
            window.location.href = "/index.html";
        }
    }
    else {
        $('.rleblockservice').show();
    }

}
function checkServiceBlockedHotel() {
    //Get Markup
    var Dastasseqq = {
        AgencyId: AgencyIDD,
        ServiceTypeID: '2'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/checkServiceBlocked",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: checkServiceBlockedHotelSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function checkServiceBlockedHotelSuccess(datablock) {
        //alert(datablock.d)
        if (datablock.d == '1') {
            serviceblocked = true;
            $('#hotel').remove();
            $('#hotelnoservice').show();
        }
        else {
            serviceblocked = false;
            $('#hotelnoservice').hide();
        }
    }
}
function checkServiceBlocked() {
    //Get Markup
    var Dastasseqq = {
        AgencyId: AgencyIDD,
        ServiceTypeID: '1'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/checkServiceBlocked",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: checkServiceBlockedSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function checkServiceBlockedSuccess(datablock) {
        //alert(datablock.d)
        if (datablock.d == '1') {
            serviceblocked = true;
            $('#flight').remove();
            $('#hotelnoservice').show();
        }
        else {
            serviceblocked = false;
            $('#hotelnoservice').hide();
        }
    }
}