﻿//var DateFormate = 'dd/mm/yy';
var DateFormate = 'mm/dd/yy';
var MarkupValue = parseFloat(0);
var HotelAddress = 'Not Available';
var AgencyTypeIDD = '';
var CreditLimittAmount = parseFloat(0);
var extracot = '';
var cotdaetails = '';
var CityCode = '';
var ItemCode = '';
var TotalPrice = '';
var HID = '';
var RHID = [];
var updatestatus = '';
var flag = 0;
var ItemRef = '';
var ONItemRef = [];
var ArrayCount = parseFloat(0);
var HoID = '';
var SessID = '';
var AgencyCheck = '';
var ItemReferenceCode = '';
var ArrayC = parseFloat(0);
var tthis;
$(document).ready(function () {
    
    SessionChecking('UserDetails');

    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
   


});
function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        $('.AgencyName').html(userdata[0].AgencyName);
        var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }

        var AdminStatus = userdata[0].AdminStatus;
        if (AgencyTypeIDD == '1' && AdminStatus == '1') {
            $('#divddlBranch').show();
            BindBranch();
        }
        else {
            $("#ddlBranch").html($("<option></option>").val(userdata[0].AgencyID).html(userdata[0].AgencyName));
            $('#divddlBranch').hide();
        }

        AgencyCheck = userdata[0].AgencyID;
        //Get Markup
        var Dastasseqq = {
            AgencyID: userdata[0].AgencyIDD,
            ServiceTypeID: '2'
        };
        $.ajax({
            type: "POST",
            url: "WebserviceHotel.aspx/GetMarkupDataHotel",
            data: JSON.stringify(Dastasseqq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: MarkupSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        MarkupValue = userdata[0].MarkupValue;
        CreditLimittAmount = parseFloat(userdata[0].CreditLimitAmount);
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        MarkupValue = 0;
        $('.CreditLimitAmount').html('Credit Limit - No credit !');
    }
    SearchHotel();
}
function BindBranch() {
    var Datatable = {
        selectdata: '*',
        tablename: 'tblAgency',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BindBranchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BindBranchSuccess(data) {
    var Branchdata = $.parseJSON(data.d);
    console.log(Branchdata);
    for (var i = 0; i < Branchdata.length; i++) {
        $("#ddlBranch").append($("<option></option>").val(Branchdata[i].AgencyID).html(Branchdata[i].AgencyName));
    }
}

function SearchHotel() {
    $('#loadingGIFFlSearchDet').show();
    $('#divSearchRes').show();
    var DataSeq = {
        AgencyID: AgencyCheck
    };
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/GetSearchDataHotelOnrequest",
        data: JSON.stringify(DataSeq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchSuccess(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divNoData').hide();
        var Searchdata = $.parseJSON(datab.d);
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr><td>' + Searchdata[isd].BookingRefID +
                ' <a><i  title="More Information"  id="idMoreInfo" data-open="0" onclick="GetMoreInfo(' + Searchdata[isd].BookingRefID + ')" class="fa fa-info-circle" data-toggle="modal" data-target="#More_info" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td>'
                + Searchdata[isd].FirstName + " " + Searchdata[isd].LastName + '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
                + Searchdata[isd].BookingStatusCode + '</td><td><button id="btnOnrequest_' + isd + '" class="OnrequestUpdate" onclick="SearchDetailsClick(this,' + Searchdata[isd].BookingRefID + ')"><i class="newclass05 fa fa-circle-o-notch fa-spin"></i>Update</button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button id="btnOnrequestReject_' + isd + '" class="OnrequestUpdate" onclick="RejectClick(this,' + Searchdata[isd].BookingRefID + ')"><i class="newclass05  fa fa-circle-o-notch fa-spin"></i>Reject</button></td></tr>';
            
        }
        $("#SearchResult").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableSearch').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableSearch').show();
    }
    else {
        $('#divNoData').show();
    }

    $('#loadingGIFFlSearchDet').hide();

}

function SearchDetailsClick(tthis,sid) {
    
    $(tthis).find("i").removeClass("newclass05");

    SessID = sid;
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: Savesessionsuccess,
        failure: function (response) {
            alert(response.d);
        }
    });

    setTimeout(function () {
        $(tthis).find("i").addClass("newclass05");
    }, 2000);
  
}
function RejectClick(tthis,sid)
{
    $(tthis).find("i").removeClass("newclass05");
    alertify.confirm("Please note", "Are you sure to reject this booking?<br/><br/>Please make sure the booking is not done with supplier system",
   function () {

       SessID = sid;
       var Datatable = {

           valueses: sid
       };
       $.ajax({
           type: "POST",
           url: "WebMethodDbHotel.aspx/RejectHotelbooking",
           data: JSON.stringify(Datatable),
           contentType: "application/json; charset=utf-8",
           dataType: "json",
           success: RejectHotelbookingsuccess,
           failure: function (response) {
               alert(response.d);
           }
       });
   },
   function () {
       $('#sort_popup').css("z-index", "99999");
   });
    setTimeout(function () {
        $(tthis).find("i").addClass("newclass05");
    }, 2000);
}
function RejectHotelbookingsuccess(data)
{
   
    alertify.alert("Success", "Rejected successfully");
    window.location.href = "hotelOnRequest.html";
}
function Savesessionsuccess()
{
    var Datass = {

        APIid: SessID

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/BookingHotelUpdateClient",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function SearchDetailsClickSuccess(data) {
    if (data.d == null) {
        alertify.alert('Please Note', 'We cannot find any booking in supplier system with this reference id ' + SessID + '. Please contact admin to make sure about the booking and reject the booking accordingly.');
        $(tthis).removeClass("fa fa-circle-o-notch fa-spin");
        return true;
    }
    else {
        var HotelBookingHotelUpdateinfo = data.d;
        var x2js2 = new X2JS();
        var jsonHotelRes = x2js2.xml_str2json(HotelBookingHotelUpdateinfo);
        console.log(jsonHotelRes);
        if (jsonHotelRes != '' && jsonHotelRes !=null) {
            var Respone = jsonHotelRes.Response.ResponseDetails.SearchBookingItemResponse.BookingStatus;
            if (Respone != "" && Respone != undefined) {
                updatestatus = jsonHotelRes.Response.ResponseDetails.SearchBookingItemResponse.BookingStatus._Code;

                if (jsonHotelRes != '' || jsonHotelRes != null) {
                    JBookRes = jsonHotelRes.Response.ResponseDetails.SearchBookingItemResponse;
                    var Error = JBookRes.Errors;
                    if (Error == undefined) {
                        var bookpasscount = JBookRes.BookingItems.BookingItem.length;

                        if (bookpasscount == undefined) {
                            var Paxname = JBookRes.BookingName;
                            var RefNo = JBookRes.BookingItems.BookingItem.ItemConfirmationReference;
                            var ClinentID = JBookRes.BookingReferences.BookingReference["0"].__cdata;
                            var ApiID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                            var BookingDate = JBookRes.BookingCreationDate
                            var CheckinDate = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckInDate;
                            var StatusCode = JBookRes.BookingStatus._Code;
                            var Status = JBookRes.BookingItems.BookingItem.ItemStatus.__cdata;
                            var CheckOut = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckOutDate;
                            var gross = JBookRes.BookingPrice._Gross;
                            var Net = JBookRes.BookingPrice._Nett;
                            var Currency = JBookRes.BookingPrice._Currency;
                            var HotelName = JBookRes.BookingItems.BookingItem.Item.__cdata;
                            HotelName = HotelName.toUpperCase();
                            var SupplyID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                            var Checkin = moment(CheckinDate);
                            var CheckOutt = moment(CheckOut);
                            var DifferenceMilli = CheckOutt - Checkin;
                            var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                            var Night = DifferenceMilliDuration.days();
                            // var Totel = parseFloat(parseFloat(gross) * parseFloat(Night));
                            var Totel = parseFloat(parseFloat(gross));
                            TotalPrice = GetRoomPriceConvMark(Totel);
                            var RoomType = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.Description;
                            var Adult = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Adults;
                            var Child = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Children;
                            var Brkfast = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Basis.__cdata;
                            if (Brkfast != "None") {
                                var Full = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Breakfast.__cdata;
                            }
                            else {
                                var Full = "";
                                var Brkfast = "Rooms Only";
                            }

                            ItemRef = JBookRes.BookingItems.BookingItem.ItemReference;

                        }
                        else {
                            var Paxname = JBookRes.BookingName;
                            var RefNo = JBookRes.BookingItems.BookingItem[0].ItemConfirmationReference;
                            var ClinentID = JBookRes.BookingReferences.BookingReference["0"].__cdata;
                            var ApiID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                            var BookingDate = JBookRes.BookingCreationDate
                            var StatusCode = JBookRes.BookingStatus._Code;
                            var CheckinDate = JBookRes.BookingItems.BookingItem[0].HotelItem.PeriodOfStay.CheckInDate;
                            var Status = JBookRes.BookingItems.BookingItem[0].ItemStatus.__cdata;
                            var CheckOut = JBookRes.BookingItems.BookingItem[0].HotelItem.PeriodOfStay.CheckOutDate;
                            var gross = JBookRes.BookingPrice._Gross;
                            var Net = JBookRes.BookingPrice._Nett;
                            var Currency = JBookRes.BookingPrice._Currency;
                            var HotelName = JBookRes.BookingItems.BookingItem[0].Item.__cdata;
                            HotelName = HotelName.toUpperCase();
                            var SupplyID = JBookRes.BookingReferences.BookingReference[1].__cdata;
                            var Checkin = moment(CheckinDate);
                            var CheckOutt = moment(CheckOut);
                            var DifferenceMilli = CheckOutt - Checkin;
                            var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                            var Night = DifferenceMilliDuration.days();
                            var Totel = parseFloat(parseFloat(gross));
                            TotalPrice = GetRoomPriceConvMark(Totel);
                            for (var ic = 0; ic < bookpasscount; ic++) {
                                var val = ic;
                                val = parseFloat(parseFloat(val) + parseFloat(1));
                                var RoomType = val + ". " + JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.Description;
                                var Adult = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Adults;
                                var Child = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Children;
                                var Brkfast = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Basis.__cdata;
                                if (Brkfast != "None") {
                                    var Full = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Breakfast.__cdata;
                                }
                                else {
                                    var Full = "";
                                    var Brkfast = "Rooms Only";
                                }

                                var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId["0"];
                                var PNameCount = JBookRes.PaxNames.PaxName;
                                var PaxN = _.where(PNameCount, { "_PaxId": PaxID });
                                var Name = PaxN[0].__cdata;
                                ItemRef = JBookRes.BookingItems.BookingItem[ic].ItemReference;

                            }
                        }
                        var Commi = JBookRes.BookingPrice._Commission;
                        var Currency = JBookRes.BookingPrice._Currency;
                        var Gross = JBookRes.BookingPrice._Gross;
                        var Net = JBookRes.BookingPrice._Nett;
                        inserttblBookingHotelInformation(ClinentID, ApiID, CheckinDate, CheckOut, StatusCode, Commi, Currency, Gross, Net)

                    }

                }
                else {
                    alertify.alert('Update', 'Error occured! Please contact admin');
                    $(this).removeClass("fa fa-circle-o-notch fa-spin");
                    return true;
                }




            }
            else {
                alertify.alert('Please Note', 'We cannot find any booking in supplier system with this reference id ' + SessID + '. Please contact admin to make sure about the booking and reject the booking accordingly.');
                $(this).removeClass("fa fa-circle-o-notch fa-spin");
                return true;
            }
        }


        else {

            alertify.alert('Please Note', 'We cannot find any booking in supplier system with this reference id ' + SessID + '. Please contact admin to make sure about the booking and reject the booking accordingly.');
            $(this).removeClass("fa fa-circle-o-notch fa-spin");
            return true;

        }
      
    }

    
}

function GetStatus(status) {
    var Sat = "";
    if (status == "HK") {
        Sat = "Confirmed";
       
    }
    else if (status == "RR") {
        Sat = "Confirmation Pending";
       
    }
    else if (status == "XX") {
        Sat = "Cancelled";
    }
    else if (status == "RC") {
        Sat = "Reconfirmed";
       
    }
    else if (status == "RQ") {
        Sat = "On request";
       
    }
    else if (status == "XR") {
        Sat = "Cancellation On request";
        
    }
    return Sat;
}

function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}

function inserttblBookingHotelInformation(ClinentID, ApiID, CheckinDate, CheckOut, StatusCode, Commi, Currency, Gross, Net) {
    var Datass = {

        ClinentID: ClinentID,
        ApiID: ApiID,
        CheckinDate: CheckinDate,
        CheckOut: CheckOut,
        StatusCode: StatusCode,
        Commi: Commi,
        Currency: Currency,
        Gross: Gross,
        Net: Net,
        TotalPrice: TotalPrice,
        MarkupValue: MarkupValue
    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/BookingHotelInformation",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelInformationSuccess,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function BookingHotelInformationSuccess(data) {
    var result = data.d;
    HoID = result;
    console.log(result);

    //JBookRes = jsonHotelRes.Response.ResponseDetails.BookingResponse;
    var bookpasscount = JBookRes.BookingItems.BookingItem.length;
    var PaxData = JBookRes.PaxNames.PaxName.length;
    if (PaxData != undefined) {
        for (var iP = 0; iP < PaxData; iP++) {
            var PName = JBookRes.PaxNames.PaxName[iP].__cdata;
            var PID = JBookRes.PaxNames.PaxName[iP]._PaxId;
            var strArr = PName.split(" ");
            var PTitle = strArr[0];
            var PFName = strArr[1];
            var PLName = strArr[2];
            var PType = JBookRes.PaxNames.PaxName[iP]._PaxType;
            if (PType != undefined) {
                if (PType == "child") {
                    var PType = "CHD"
                    var PAge = JBookRes.PaxNames.PaxName[iP]._ChildAge;
                    var PaxTypeAge = PType + "," + PAge;

                }
                else if (PType == "infant") {
                    var PaxTypeAge = "INF"

                }
                else {
                    var PaxTypeAge = "ADT";
                }



            }
            else {
                var PaxTypeAge = "ADT";

            }
            insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge);
        }


    }
    else {
        var PName = JBookRes.PaxNames.PaxName.__cdata;
        var PID = JBookRes.PaxNames.PaxName._PaxId;
        var strArr = PName.split(" ");
        var PTitle = strArr[0];
        var PFName = strArr[1];
        var PLName = strArr[2];
        var PType = JBookRes.PaxNames.PaxName._PaxType;
        if (PType != undefined) {
            if (PType == "child") {
                var PType = "CHD"
                var PAge = JBookRes.PaxNames.PaxName._ChildAge;
                var PaxTypeAge = PType + "," + PAge;

            }
            else if (PType == "infant") {
                var PaxTypeAge = "INF"

            }
            else {
                var PaxTypeAge = "ADT";
            }



        }
        else {
            var PaxTypeAge = "ADT";

        }
        insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge);
    }


    if (bookpasscount == undefined) {
        
        CityCode = JBookRes.BookingItems.BookingItem.ItemCity._Code;
         ItemCode = JBookRes.BookingItems.BookingItem.Item._Code;
        var HotelName = JBookRes.BookingItems.BookingItem.Item.__cdata;
        var StatusCode = JBookRes.BookingStatus._Code;
      
        var CheckinDate = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckInDate;
        var CheckOut = JBookRes.BookingItems.BookingItem.HotelItem.PeriodOfStay.CheckOutDate;
        var Noadult = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Adults;
        var NoChild = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Children;
        var Cot = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Cots;
        var RoomCatID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._Id;
        var RoomCat = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.Description;
        var Brkfast = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Basis.__cdata;
        if (Brkfast != "None") {
            var Full = JBookRes.BookingItems.BookingItem.HotelItem.Meals.Breakfast.__cdata;
        }
        else {
            var Full = "";
            var Brkfast = "Rooms Only";
        }

        var Sharebedding = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom._SharingBedding;
        if (Sharebedding == undefined) {
            
            Sharebedding = "0";
        }
        var PaxData = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId.length;
        var PaxDatA = [];
        var CancellData = [];
        if (PaxData != undefined) {
            for (var iP = 0; iP < PaxData; iP++) {
                var PaxID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId[iP];
                PaxDatA.push(PaxID);

            }

        }
        else {
            var PaxID = JBookRes.BookingItems.BookingItem.HotelItem.HotelPaxRoom.PaxIds.PaxId[0];
            PaxDatA.push(PaxID);
        }
        var Commisssion = JBookRes.BookingItems.BookingItem.ItemPrice._Commission;
        var ItemCurrency = JBookRes.BookingItems.BookingItem.ItemPrice._Currency;
        var ItemGross = JBookRes.BookingItems.BookingItem.ItemPrice._Gross;
        var ItemNet = JBookRes.BookingItems.BookingItem.ItemPrice._Nett;
        ItemRef = JBookRes.BookingItems.BookingItem.ItemReference;
        ItemReferenceCode = JBookRes.BookingItems.BookingItem.ItemConfirmationReference;
        ONItemRef.push(ItemRef);
        console.log(ONItemRef);
        insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA, Commisssion, ItemCurrency, ItemGross, ItemNet);

    }
    else {

        for (var ic = 0; ic < bookpasscount; ic++) {
             CityCode = JBookRes.BookingItems.BookingItem[ic].ItemCity._Code;
             ItemCode = JBookRes.BookingItems.BookingItem[ic].Item._Code;
            var HotelName = JBookRes.BookingItems.BookingItem[ic].Item.__cdata;
            var StatusCode = JBookRes.BookingStatus._Code; 
            var CheckinDate = JBookRes.BookingItems.BookingItem[ic].HotelItem.PeriodOfStay.CheckInDate;
            var CheckOut = JBookRes.BookingItems.BookingItem[ic].HotelItem.PeriodOfStay.CheckOutDate;
            var Noadult = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Adults;
            var NoChild = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Children;
            var Cot = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Cots;
            var RoomCatID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._Id;
            var RoomCat = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.Description;
            var Brkfast = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Basis.__cdata;
            if (Brkfast != "None") {
                var Full = JBookRes.BookingItems.BookingItem[ic].HotelItem.Meals.Breakfast.__cdata;
            }
            else {
                var Full = "";
                var Brkfast = "Rooms Only";
            }

            var Sharebedding = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom._SharingBedding;
            if (Sharebedding == undefined) {
               
                Sharebedding = "0";
            }
            var PaxData = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId.length;
            var PaxDatA = [];
            var CancellData = [];
            if (PaxData != undefined) {
                for (var iP = 0; iP < PaxData; iP++) {
                    var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId[iP];
                    PaxDatA.push(PaxID);

                }

            }
            else {
                var PaxID = JBookRes.BookingItems.BookingItem[ic].HotelItem.HotelPaxRoom.PaxIds.PaxId[0];
                PaxDatA.push(PaxID);
            }

           
            var Commisssion = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Commission;
            var ItemCurrency = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Currency;
            var ItemGross = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Gross;
            var ItemNet = JBookRes.BookingItems.BookingItem[ic].ItemPrice._Nett;
           
               ItemReferenceCode = JBookRes.BookingItems.BookingItem[ic].ItemConfirmationReference;
           
               ItemRef = JBookRes.BookingItems.BookingItem[ic].ItemReference;
               ONItemRef.push(ItemRef);
                insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA, Commisssion, ItemCurrency, ItemGross, ItemNet);

          


        }
        console.log(ONItemRef);
    }
}
function insertBookingHotelRoomsInformation(result, ItemRef, CityCode, ItemCode, HotelName, StatusCode, ItemReferenceCode, CheckinDate, CheckOut, Noadult, NoChild, Cot, RoomCatID, RoomCat, Brkfast, Full, Sharebedding, PaxDatA,  Commisssion, ItemCurrency, ItemGross, ItemNet) {
    var Datass = {

        result: result,
        ItemRef: ItemRef,
        CityCode: CityCode,
        ItemCode: ItemCode,
        HotelName: HotelName,
        StatusCode: StatusCode,
        ItemReferenceCode: ItemReferenceCode,
        CheckinDate: CheckinDate,
        CheckOut: CheckOut,
        Noadult: Noadult,
        NoChild: NoChild,
        Cot: Cot,
        RoomCatID: RoomCatID,
        RoomCat: RoomCat,
        Brkfast: Brkfast,
        Full: Full,
        Sharebedding: Sharebedding,
        PaxDatA: PaxDatA,
        Commisssion: Commisssion,
        ItemCurrency: ItemCurrency,
        ItemGross: ItemGross,
        ItemNet: ItemNet,
        MarkupValue: MarkupValue,
        HotelAddress: HotelAddress

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/OnrequestBookingHotelRoomsInformation",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelRoomsInformation,
        failure: function (response) {
            alert(response.d);

        }
    });
}
function BookingHotelRoomsInformation(data) {
    HID = data.d;
    RHID.push(HID);
    console.log(HID);
    var ref = ONItemRef[ArrayC];
    var Datass = {
        ItemReff: ref
    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/OnrequestBookingCancellationpolicies",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnrequestBookingCancellationpoliciessuccesss,
        failure: function (response) {
            alert(response.d);

        }
    });
    ArrayC = parseInt(ArrayC) + parseInt(1);
}
function OnrequestBookingCancellationpoliciessuccesss(data)
{
  
    var HotelBookingCancellation = data.d;
    var x2js2 = new X2JS();
    var jsonHotelRescancel = x2js2.xml_str2json(HotelBookingCancellation);
    console.log(jsonHotelRescancel);
    var CancellData = [];
    var Allowble = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[1]._Allowable;
    var Type = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[1]._Type;
    var PassTypeAllow = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.PassengerNameChange._Allowable;
    var PassType = "PassengerNameChange";
    var cancell = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition
    for (var ij = 0; ij < cancell.length; ij++) {
        var Condition1 = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition;

        if (Condition1 != undefined) {
            if (Condition1.length == undefined) {
                var FromDate = Condition1._FromDate;
                var Charge = Condition1._Charge;
                var ConCurrency = Condition1._Currency;
                var ToDate = Condition1._ToDate;
                var ChargeAmount = Condition1._ChargeAmount;
                if (ToDate == undefined) {
                    ToDate = new Date();
                    ToDate = moment(ToDate).format();
                }
                var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                CancellData.push(FullInfo);
            }
            else {
                for (var ih = 0; ih < Condition1.length; ih++) {
                    var FromDate = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._FromDate;
                    var ToDate = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;
                    var Charge = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._Charge

                    var ConCurrency = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._Currency;
                    if (ConCurrency == undefined) {
                        var ConCurrency = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[0]._Currency;

                    }
                    else {
                        var ConCurrency = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._Currency;

                    }
                    if (ToDate == undefined) {

                        ToDate = new Date();
                        ToDate = moment(ToDate).format();
                    }
                    else {
                        ToDate = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._ToDate;

                    }
                    var ChargeAmount = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                    if (ChargeAmount == undefined) {
                        var ChargeAmount = "0.000";

                    }
                    else {
                        var ChargeAmount = jsonHotelRescancel.Response.ResponseDetails.SearchChargeConditionsResponse.ChargeConditions.ChargeCondition[ij].Condition[ih]._ChargeAmount;
                    }

                    var FullInfo = FromDate + "," + ToDate + "," + Charge + "," + ChargeAmount + "," + ConCurrency;
                    CancellData.push(FullInfo);
                }
            }

        }

    }
    var ID = RHID[ArrayCount];
    var Datass = {
        HID: ID,
        Type: Type,
        Allowble: Allowble,
        PassTypeAllow: PassTypeAllow,
        PassType: PassType,
        CancellData: CancellData
    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/OnrequestBookingCancellationpoliciesinsertion",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnrequestBookingCancellationpoliciesinsertionsuccesss,
        failure: function (response) {
            alert(response.d);

        }
    });
    ArrayCount = ArrayCount + parseInt(1);
}
function OnrequestBookingCancellationpoliciesinsertionsuccesss()
{
    
    hotelimage();
}
function insertHotelPax(result, PID, PTitle, PFName, PLName, PaxTypeAge) {
    var Datass = {
        result: result,
        PID: PID,
        PTitle: PTitle,
        PFName: PFName,
        PLName: PLName,
        PaxTypeAge: PaxTypeAge

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/HotelPax",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: HotelPax,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function HotelPax() {

}

function hotelimage() {
    HotelAddress = '';
    var qstrings = {
        CityCode: CityCode,
        HotelCode: ItemCode
    }
    $.ajax({
        type: "POST",
        url: "WebserviceHotel.aspx/GetStaticXmlHotel",
        data: JSON.stringify(qstrings),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetStaticDataSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetStaticDataSuccess(datas) {
        if (datas.d == 'NODATA') {//Get Hotel Data from GTA
            var qstrings = {
                CityCode: CityCode,
                HotelCode: ItemCode
            }
            $.ajax({
                type: "POST",
                url: "WebserviceHotel.aspx/GetHotelInfoGta",
                data: JSON.stringify(qstrings),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: GetHotelInfoSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
            function GetHotelInfoSuccess(datag) {
                var x2js = new X2JS();
                var jsonDataHotelGta = x2js.xml_str2json(datag.d);
                GenerateHotelInfo(jsonDataHotelGta);
            }
        }
        else {
            var x2js = new X2JS();
            var jsonStatDataHotel = x2js.xml_str2json(datas.d);
            GenerateHotelInfo(jsonStatDataHotel);
        }
        function GenerateHotelInfo(jsonStatDataHotel) {
            console.log(jsonStatDataHotel);
            var AddressLines = jsonStatDataHotel.Response.ResponseDetails.SearchItemInformationResponse.ItemDetails.ItemDetail.HotelInformation.AddressLines;

            if (AddressLines.AddressLine1 != undefined) {
                var address1 = AddressLines.AddressLine1;
                HotelAddress += AddressLines.AddressLine1 + ",";
            }
            if (AddressLines.AddressLine2 != undefined) {
                var address2 = AddressLines.AddressLine2;
                HotelAddress += AddressLines.AddressLine2 + ",";
            }
            if (AddressLines.AddressLine3 != undefined) {
                var address3 = AddressLines.AddressLine3;
                HotelAddress += AddressLines.AddressLine3 + ",";
            }
            if (AddressLines.AddressLine4 != undefined) {
                var address4 = AddressLines.AddressLine4;
                HotelAddress += AddressLines.AddressLine4 + ".";
            }
            if (_.isUndefined(AddressLines.Telephone) == false) {
                var telephone = ', Ph: ' + AddressLines.Telephone;
            }
            else {
                var telephone = '';
            }

            var Add = HotelAddress.substr(0, HotelAddress.length - 1) + '.';
            HotelAddress = HotelAddress.substr(0, HotelAddress.length - 1) + telephone + '.';
            $("#hotAddress").append('<b>Address</b><br>' + Add + '');
            console.log(HotelAddress);
            var HIDstrings = {
                HID: HoID,
                HotelAddress: HotelAddress
            }
            $.ajax({
                type: "POST",
                url: "WebMethodDbHotel.aspx/UpdateHotelAddress",
                data: JSON.stringify(HIDstrings),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: UpdateHotelAddressSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
      
    }
}
function UpdateHotelAddressSuccess()
{
   
    if (updatestatus == "C " && flag == 0) {
       
        flag == 1
        alertify.alert('Confirmed', 'Congratulations!! your booking got confirmed with booking reference id ' + SessID + ' ');
        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
       
    }
    else if (updatestatus == "X " && flag == 0) {
        
        flag == 1
        alertify.alert('Cancel', 'Oops...your booking got cancelled with booking reference id ' + SessID + ' ');
        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
    }
    else if (updatestatus == "CP" && flag == 0) {
      
        flag == 1
        alertify.alert('Pending Confirmation', 'Your booking is pending for supplier confirmation with booking reference id ' + SessID + '.');
        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
    }
   
    setTimeout(function () {
        window.location.href = "hotelOnRequest.html";
    }, 3000);
}

function GetMoreInfo(RefID) {
    $("#tableMoreinfo").html('<iframe height="400px" width="100%" src="iframeInfo.html?qur=' + RefID + '" name="iframe_a"></iframe>');
}
 function GetRoomPriceConvMark(ogprice) {
    var OGPricee = parseFloat(ogprice);
    var NewPrice = parseFloat(0);
    var NewPriceMarkup = parseFloat(0);
    var MarkupValuePerc = parseFloat(MarkupValue);
    //var SellingRateUSD = parseFloat(0.388);
    var SellingRateUSD = parseFloat(1);
    var BuyyingRateOMR = parseFloat(1);
    NewPrice = (OGPricee * SellingRateUSD) / BuyyingRateOMR;
    NewPriceMarkup = NewPrice + (NewPrice * (MarkupValuePerc / 100));
    return Math.ceil(NewPriceMarkup * 100) / 100;
    //return ogprice;
}

