﻿var Paxname = '';
var hotelId = '';
var MarkupValue = parseFloat(0);
var CancellationGPeriod = parseInt(2);
var Quotationid=''
$(document).ready(function () {
    SessionCheckingBookingRef('HotelBookingRefID');
  
});
//Check user login
function SessionChecking(session) {

    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SessionSuccess(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var userdata = $.parseJSON(data.d);
            console.log(userdata);
            $('.username').html(userdata[0].FirstName);
            $('.AgencyName').html(userdata[0].AgencyName);
            var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
            }
            GetMarkupDataa('MarkupDataHotel');
           
        }
    }
}
//Load
function SessionCheckingBookingRef(session) {
    var Dastasseqq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionRefSuccess,
        failure: function (response) {
            alert(response.d);
            window.location.href = "/hotelbookingsearchinfo.html";
        }
    });
} //01

function SessionRefSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        var result = data.d
        GetQuotationDetails(result)
    }
} //02
function GetQuotationDetails(quoteid)
{
    Quotationid = quoteid;
    var Request = "{\"quoteId\": " + quoteid + "}";
    var Datatable = {
        Request: Request
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/Testlogrequest.aspx/GetsentHotelQuoteDetails",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess(data)
{
    var quotedetails = $.parseJSON(data.d);
    $("#RefernceNo").append(Quotationid);
   // $("#Mailfrom").append(quotedetails.mailFrom);
    $("#Mailto").append(quotedetails.mailTo);
    $("#Subject").append(quotedetails.subject);
    $("#hoteldetails").append(quotedetails.body);

}
function Bookinfo() {
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/BooksearchHotelinfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: BookHotelResults,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BookHotelResults(data) {
    var HotelbookinfoData = JSON.parse(data.d);
    console.log(HotelbookinfoData);
    $('#StayIn').append((HotelbookinfoData[0].ItemName).toUpperCase());
    var BookingDate = HotelbookinfoData[0].BookingDate;
    var CheckinDate = HotelbookinfoData[0].CheckInDate;
    var Status = GetStatus(HotelbookinfoData[0].ItemStatusCode);
    var CheckOut = HotelbookinfoData[0].CheckOutDate;
    var HotelAddress = HotelbookinfoData[0].HotelAddress;
    hotelId = HotelbookinfoData[0].HotelInformationID
    $("#BookedOn").append('Booked on ' + (moment(BookingDate).format("DD MMM YYYY")));
    $('#RefernceNo').append(HotelbookinfoData[0].BookingRefID);
    $("#BookingDate").append((moment(BookingDate).format("DD MMM YYYY")));
    $('#CheckinDate').append(moment(CheckinDate).format("DD MMM YYYY"));
    $('#Status').html(Status + '<span id="divhistory"></span><a><i id="history" data-open="0" onclick="GetHistory(' + HotelbookinfoData[0].BookingRefID + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Status_History" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');
    $('#CheckOut').append(moment(CheckOut).format("DD MMM YYYY"));
    var Checkin = moment(CheckinDate);
    var CheckOutt = moment(CheckOut);
    var DifferenceMilli = CheckOutt - Checkin;
    var DifferenceMilliDuration = moment.duration(DifferenceMilli);
    var Night = DifferenceMilliDuration.days();
    $('#Nights').append(Night);
    var Time = moment(HotelbookinfoData[0].fromdate).subtract(2, 'day');
    var totalprice = parseFloat(0);
    for (var iRoom = 0; iRoom < HotelbookinfoData.length; iRoom++) {
        totalprice += parseFloat(HotelbookinfoData[iRoom].price);
    }
   // var SellingRateUSD = parseFloat(0.388);
    //var NewPrice = (totalprice * SellingRateUSD)
   // NewPrice = NewPrice + (NewPrice * (MarkupValue / 100));
    $('#TimeLimit').append(moment(Time).format("DD MMM YYYY"));
    $('#Accomedation').append(HotelbookinfoData[0].ItemName);
    $('#divaddress').append(HotelAddress);
    $('#SupplyRef').append(HotelbookinfoData[0].APIRefID);
    $('#Total').html((GetRoomPriceConvMark(totalprice, MarkupValue)) + " USD" + '<span id="divFarebrkup"></span><a><i id="farebreakup" data-open="0" onclick="GetpriceBreak(' + hotelId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Price_breakup" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');

}
function GetStatus(status) {
    var Sat = "";
    if (status == "C " || status == "C") {
        Sat = "Confirmed";
    }
    else if (status == "CP") {
        Sat = "Confirmation Pending";
    }
    return Sat;
}
function BookPaxinfo() {
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/BookSearchPassengersHotelinfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: BookpaxHotelResults,
        failure: function (response) {
            alert(response.d);
        }
    });

}
function BookpaxHotelResults(data) {
    var HotelPaxData = JSON.parse(data.d);
    console.log(HotelPaxData);

    for (var iPax = 0; iPax < HotelPaxData.length; iPax++) {
        Paxname = HotelPaxData[iPax].Column1;
        var Passname = HotelPaxData[iPax].Column1;
        var rooms = HotelPaxData[iPax].RoomCategory;
        var brkfst = HotelPaxData[iPax].MealBasis;
        console.log(brkfst)
        if (brkfst == "Breakfast,") {
            brkfst = "Full Breakfast";
        }
        var Adult = HotelPaxData[iPax].HotelPaxRoomAdults;
        var Child = HotelPaxData[iPax].HotelPaxRoomChildren;
        var RoomId = HotelPaxData[iPax].BookingRoomID;
        $('#ROOPAX').append('<tr><td>' + rooms + '(' + brkfst + ')' + '</td> <td>' + Adult + '</td> <td>' + Child + '</td> <td>' + Passname + '<span id="divTotal"></span><a><i id="ifbreakup" data-open="0" onclick="GetPaxBreak(' + RoomId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#PAX_breakup" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td></tr>');

    }

}
function GetPaxBreak(ItemRef) {

    var Datass = {

        ItemRef: ItemRef

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/BookingHotelRoomPaxBrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelRoomPaxBrkup,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function BookingHotelRoomPaxBrkup(data) {
    $("#tablePAXBreakUps").html('');
    var HotelPaxDatabrkup = JSON.parse(data.d);
    console.log(HotelPaxDatabrkup);

    for (var ifb = 0; ifb < HotelPaxDatabrkup.length; ifb++) {
        var val = ifb;
        val = parseFloat(parseFloat(val) + parseFloat(1));
        var Fname = HotelPaxDatabrkup[ifb].PaxFirstName;
        var LName = HotelPaxDatabrkup[ifb].PaxLastName;
        var Title = HotelPaxDatabrkup[ifb].PaxTitle;
        $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Gettitle(Title) + " " + Fname + " " + LName + '</tr></td>');
    }
}

function Gettitle(Title) {
    var Out = '';
    if (Title == "1") {
        Out = "Mr";
    }
    else if (Title == "2") {
        Out = "Miss";
    }
    else if (Title == "3") {
        Out = "Mrs";
    }
    else if (Title == "4") {
        Out = "Ms";
    }
    else if (Title == "5") {
        Out = "Master";
    }

    return Out;

}

function GetFareBreak() {
    var Datass = {

        hotelId: hotelId

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/GetFarebrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelFarebrkupResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetHotelFarebrkupResults(data) {
        $("#tableFareBreakUps").html('');
        var HotelFareDatabrkup = JSON.parse(data.d);
        console.log(HotelFareDatabrkup);
        var total = parseFloat(0);
        $('#tableFareBreakUps').append('<thead> <tr> <th class="sp_table">Rooms</th> <th class="sp_table">Room Cost</th> </tr> </thead> <tbody>')
        for (var ifb = 0; ifb < HotelFareDatabrkup.length; ifb++) {
            var val = parseFloat(parseFloat(ifb) + parseFloat(1));
            var trroom = val;
            var trBaseFare = +((parseFloat(HotelFareDatabrkup[ifb].TotalSellAmount) * (parseFloat(0.388)))) + " " + "USD" + '';
            var fare = (parseFloat(HotelFareDatabrkup[ifb].TotalSellAmount) * (parseFloat(0.388)));
            total += parseFloat(fare);
            var mealtype = '';
            if (HotelFareDatabrkup[ifb].MealBasis == "Breakfast,") {
                mealtype = 'Full Breakfast'
            }
            else {
                mealtype = HotelFareDatabrkup[ifb].MealBasis;
            }
            $('#tableFareBreakUps').append('<tr><td>' + val + ". " + HotelFareDatabrkup[ifb].RoomCategory + "(" + mealtype + ")" + '</td> <td>' + Math.ceil(fare * 100) / 100 + " " + "USD" + '</td></tr>');

        }
        $('#tableFareBreakUps').append('</tbody>');
        $("#TotalCost").html('Total Cost : ' + Math.ceil(total * 100) / 100 + " USD");
    }
}
function GetpriceBreak(hotelId) {
    var Datass = {

        hotelId: hotelId

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/GetFarebrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelpricebrkupResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetHotelpricebrkupResults(data) {
        $("#tablePriceBreakUps").html('');
        var HotelFareDatabrkup = JSON.parse(data.d);
        console.log(HotelFareDatabrkup);
        var totalnet = HotelFareDatabrkup[0].NetVale;
        var netamount = '<tr> <td class="fare_head_area">Net Amount</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].NetVale + '</td></tr>';
        var netCurrency = '<tr> <td class="fare_head_area">Net Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].NetCurrency + '</td></tr>';
        var RateofChange = '<tr> <td class="fare_head_area">Rate Of Exchange</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].RateOfExchange + '</td></tr>';
        var mvalue = HotelFareDatabrkup[0].MarkUpValue;
        var Markup = '<tr> <td class="fare_head_area">Markup %</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + mvalue + '</td></tr>';
        var fare = GetRoomPriceConvMark(totalnet, mvalue);
        var Total = '<tr> <td class="fare_head_area">Sell Amount</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + fare + '</td></tr>';
        var FareCurrency = '<tr> <td class="fare_head_area">Sell Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + "USD" + '</td></tr>';
        $("#tablePriceBreakUps").append(Total + FareCurrency + netamount + netCurrency + RateofChange + Markup);
    }
}
function GetRoomPriceConvMark(ogprice, markup) {
    var OGPricee = parseFloat(ogprice);
    var NewPrice = parseFloat(0);
    var NewPriceMarkup = parseFloat(0);
    var MarkupValuePerc = parseFloat(markup);
    //var SellingRateUSD = parseFloat(0.388);
    var SellingRateUSD = parseFloat(1);
    var BuyyingRateOMR = parseFloat(1);
    NewPrice = (OGPricee * SellingRateUSD) / BuyyingRateOMR;
    NewPriceMarkup = NewPrice + (NewPrice * (MarkupValuePerc / 100));
    return Math.ceil(NewPriceMarkup * 100) / 100;
    //return ogprice;
}

function CancellationPolicy() {
    var Datass = {

        hotelId: hotelId

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/Getcancellationpolicy",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelcancellationpolicyResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetHotelcancellationpolicyResults(data) {
        $("#divcancellationpolicy").html('');
        var Hotelcancellationpolicy = JSON.parse(data.d);
        console.log(Hotelcancellationpolicy);
        var CancPolicyString = '';
        var bookidold = '';
        var roomcount = parseInt(0);

        var arr = [];
        for (var iarr = 0; iarr < Hotelcancellationpolicy.length; iarr++) {
            arr.push(Hotelcancellationpolicy[iarr].BookingRoomID)
        }
        console.log(arr)
        var uniarr = _.uniq(arr)
        console.log(uniarr)

        for (var i = 0; i < uniarr.length; i++) {
            CancPolicyString = '#RoomCategory#</button> <div class="panel"> <div class="accodion_cont"><p>';

            for (var ifc = (Hotelcancellationpolicy.length - 1) ; ifc >= 0; ifc--) {

                if (uniarr[i] == Hotelcancellationpolicy[ifc].BookingRoomID) {
                    if (Hotelcancellationpolicy.length < 3) {
                        var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                        if (Meal == "Breakfast,") {
                            Meal = "Full Breakfast";
                        }
                        else {
                            var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                        }
                        var RCat = Hotelcancellationpolicy[ifc].RoomCategory + "(" + Meal + ")";
                        CancPolicyString = CancPolicyString.replace("#RoomCategory#", RCat);
                        var cancelfrmdate = Hotelcancellationpolicy[ifc].FromDate;
                        var canceltodate = Hotelcancellationpolicy[ifc].ToDate;
                        cancelfrmdate = moment(cancelfrmdate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                        CancPolicyString += '<li style="color:red;font-weight:bolder;">Non Refundable Booking with Cancellation Charges <span>' + +GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue) + ' ' + " USD" + '</span></li>';

                    }
                    else {
                        var RCat = Hotelcancellationpolicy[ifc].RoomCategory;
                        var cancelamount = Hotelcancellationpolicy[ifc].ChargeAmount;

                        CancPolicyString = CancPolicyString.replace("#RoomCategory#", RCat);
                        if (cancelamount == "0") {
                            var cancelfrmdate = Hotelcancellationpolicy[ifc].FromDate;
                            var canceltodate = Hotelcancellationpolicy[ifc].ToDate;
                            cancelfrmdate = moment(cancelfrmdate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                            CancPolicyString += '<li style="color:green;font-weight:bolder;">Free cancellation on or before <span>' + cancelfrmdate + '</span></li>';


                        }
                        else {
                            var cancelfrmdate = moment(Hotelcancellationpolicy[ifc].FromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                            var canceltodate = moment(Hotelcancellationpolicy[ifc].ToDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                            CancPolicyString += '<li>Cancellation between <span>' + canceltodate +
                                                                    '</span> and <span>' + cancelfrmdate + '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue) + ' ' + " USD" + '</span></li>';
                        }

                    }
                }


            }
            $("#divcancellationpolicy").append('<button class="accordion">' + CancPolicyString + '</p></div></div>');

        }




        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            }
        }
    }

    //  $("#divcancellationpolicy").append('</ul>');

}

function GetMarkupDataa(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: MarkSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function MarkSuccess(data) {
        if (data.d != 'NOSESSION') {
            if (data.d != 'NOMARKUP') {
                var markupdata = $.parseJSON(data.d);
                console.log(markupdata);
                MarkupValue = markupdata[0].MarkupValue;

                //console.log(MarkupValue);
                //$('.CreditLimitAmount').html('Credit Limit - ' + markupdata[0].CreditLimitAmount + ' ' + markupdata[0].CurrenyCode);
            }
            else {//No Markup Data
                MarkupValue = 0;

            }
            BookPaxinfo();
            Bookinfo();

        }
        else {//No Session
        // window.location.href = "/index.html";
          BookPaxinfo();
          Bookinfo();
        }
    }
}
function GetHistory(BookingID) {
    var Datass = {

        BookingID: BookingID

    }
    $.ajax({
        type: "POST",
        url: "WebMethodDbHotel.aspx/GetHotelBookingHistory",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelBookingHistory,
        failure: function (response) {
            alert(response.d);
        }
    });

    function GetHotelBookingHistory(data) {
        $("#tablehistory").html('');
        var Hotelhistory = JSON.parse(data.d);
        console.log(Hotelhistory);
        if (data.d != 'NOINFO') {
            for (var iH = 0; iH < Hotelhistory.length; iH++) {
                var Status = Hotelhistory[iH].BookingStatusCode;
                if (Status == "RQ") {
                    Status = "On Requested";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }
                else if (Status == "HK") {
                    Status = "Confirmed";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');


                }
                else if (Status == "RR") {
                    Status = "Confirmation Pending";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }

            }

        }
        else {
            $('#tablehistory').html('<tr><td>' + "No history available ! " + '</td></tr>');

        }
    }


}