﻿//var DateFormate = 'dd/mm/yy';
var DateFormate = 'mm/dd/yy';
var AgencyCurrencyCode = '';
var AgencyCurrencyExRate = '';

$(document).ready(function () {
    SessionChecking('Loginuser');
   // GetCurrencyDetails();
    SearchHotelquotes();
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });


    $('#BookingDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#BookingDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#BookingDateTo').datepicker({
        defaultDate: "0d",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#BookingDateTo').datepicker("widget").css({ "z-index": 10000 });

    $('#CheckinDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#CheckinDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#CheckinDateTo').datepicker({
        defaultDate: "0d",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#CheckinDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#CheckinDateTo').datepicker("widget").css({ "z-index": 10000 });
});
function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {


        var logininfo = $.parseJSON(data.d);

        $('.username').html(logininfo.user.firstName);
        $('.AgencyName').html(logininfo.user.loginNode.name);
        var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);


        var AgencyTypeIDD = '1';
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }

        //var AdminStatus = userdata[0].AdminStatus;
        //if (AgencyTypeIDD == '1' && AdminStatus == '1') {
        //    $('#divddlBranch').show();
        //    //BindBranch();
        //}
        //else {
        //    $("#ddlBranch").html($("<option></option>").val(userdata[0].AgencyID).html(userdata[0].AgencyName));
        //    $('#divddlBranch').hide();
        //}
        ////Get Markup
        //var Dastasseqq = {
        //    AgencyID: userdata[0].AgencyIDD,
        //    ServiceTypeID: '2'
        //};
        //$.ajax({
        //    type: "POST",
        //    url: "/HOTEL/WebserviceHotel.aspx/GetMarkupDataHotel",
        //    data: JSON.stringify(Dastasseqq),
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: MarkupSuccess,
        //    failure: function (response) {
        //        alert(response.d);
        //    }
        //});
    }
}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No credit !');
    }
}




function SearchHotelquotes() {
    $('#divSearchRes').show();

    $.ajax({
        type: "POST",
        url: "/FLIGHT/Testlogrequest.aspx/GetAllSentQuotes",

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });


}
function SearchSuccess(datab) {
    //console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divNoData').hide();
        var Searchdata = $.parseJSON(datab.d);
        //console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
			searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].quoteId + ')"><td>' + Searchdata[isd].quoteId +
                '</td>'
                //< td data - toggle="tooltip" data - placement="top" title = "' + Searchdata[isd].mailFrom+ '" > '
                //    + Searchdata[isd].mailFrom + '</td> 
                    + '<td>'+ Searchdata[isd].mailTo + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + Searchdata[isd].subject + '">' + Searchdata[isd].subject + '</td>' +
                '<td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].sendDate).format("DD-MM-YYYY hh:mm A") + '">'
                + moment(Searchdata[isd].sendDate).format("DD-MM-YYYY") + '</td>' +'</tr>';			
        }
        $("#SearchResult").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableSearch').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableSearch').show();
    }
    else {
        $('#divNoData').show();
    }

    $('#loadingGIFFlSearchDet').hide();
    $('#divSearch').hide();
}

function SearchDetailsClick(sid) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess(data) {
    if (data.d != "NORESULT" || data.d != "") {
        window.location.href = "/HOTEL/hotelquotationinfo.html";
    }
}


function sendQuoteid() {
    var quoteid = $('#quoteid').val();
    if (quoteid != "")
    {
        searchQuoteid(quoteid);
    }
}
function searchQuoteid(quoteid) {
    SearchDetailsClick(quoteid);
}
function Savesessionsuccess() {

}