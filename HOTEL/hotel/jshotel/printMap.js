﻿/// <reference path="../Scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../typings/google.maps.d.ts" />
function printMapControl(map) {
   
    var controlDiv = printMapButton("Print", "printMap");
    google.maps.event.addDomListener(controlDiv, "click", function () {
        printMap(map);
    });
    return controlDiv;
}

function printMap(map) {
        const $body = $('body');
        const $mapContainer = $('.map-container');
        const $mapContainerParent = $mapContainer.parent();
        const $printContainer = $('<div style="position:relative;">');

        $printContainer
          .height($mapContainer.height())
          .append($mapContainer)
          .prependTo($body);

        const $content = $body
          .children()
          .not($printContainer)
          .not('script')
          .detach();

        /**
         * Needed for those who use Bootstrap 3.x, because some of
         * its `@media print` styles ain't play nicely when printing.
         */
        const $patchedStyle = $('<style media="print">')
          .text(`img { max-width: none !important; }
      a[href]:after { content: ""; }
    `)
          .appendTo('head');

        window.print();

        $body.prepend($content);
        $mapContainerParent.prepend($mapContainer);

        $printContainer.remove();
        $patchedStyle.remove();
   

}
function printMapButton(text, className) {
    "use strict";
    var controlDiv = document.createElement("div");
    controlDiv.className = className;
    controlDiv.index = 1;
    controlDiv.style.padding = "10px";
    // set CSS for the control border.
    var controlUi = document.createElement("div");
    controlUi.style.backgroundColor = "rgb(255, 255, 255)";
    controlUi.style.color = "#565656";
    controlUi.style.cursor = "pointer";
    controlUi.style.textAlign = "center";
    controlUi.style.boxShadow = "rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px";
    controlDiv.appendChild(controlUi);
    // set CSS for the control interior.
    var controlText = document.createElement("div");
    controlText.style.fontFamily = "Roboto,Arial,sans-serif";
    controlText.style.fontSize = "11px";
    controlText.style.paddingTop = "8px";
    controlText.style.paddingBottom = "8px";
    controlText.style.paddingLeft = "8px";
    controlText.style.paddingRight = "8px";
    controlText.innerHTML = text;
    controlUi.appendChild(controlText);
    $(controlUi).on("mouseenter", function () {
        controlUi.style.backgroundColor = "rgb(235, 235, 235)";
        controlUi.style.color = "#000";
    });
    $(controlUi).on("mouseleave", function () {
        controlUi.style.backgroundColor = "rgb(255, 255, 255)";
        controlUi.style.color = "#565656";
    });
    return controlDiv;
}