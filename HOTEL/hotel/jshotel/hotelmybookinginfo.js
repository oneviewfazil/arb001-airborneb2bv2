﻿var Paxname = '';
var hotelId = '';
var MarkupValue = parseFloat(1);
var CancellationGPeriod = parseInt(2);
var EmailID = '';
var EmailId1 = '';
//var EUName = '';
var ETripID = '';
var ETripName = '';
var EPaxData = '';
var grandtotal = '';
var APIid = '';
var Rateofchange = parseFloat(1);
var cancelAmount = '';
var CancelCurrency = '';
var CustomerAmount = '';
var Statuscancellation = '';
var Time = '';
var CheckinDate = '';
var cancelpriceamount = '';
var CancelNetAmount = '';
var checkval = '';
var Cstatus = '';
//var AgencyName = '';
//var AgencyAddress = '';
//var AgencyContact = '';
var gettoday = '';
var voucherbookingdate = '';
var HotelAddress = '';
var chin = '';
var chout = '';
var VoucherPax = '';
var RoleUpdate = -1;
var RoleVoucher = -1;
var RoleCancel = -1;
var RoleBookingHis = -1;
var RoleNetcostbrkup = -1;
var Voucherdet = '';
var GtaAotNumber = 'Not Available';
var updatestatus = '';
var Status = '';
$(document).ready(function () {

   
    var FrmHotelPax = window.localStorage.getItem("HotelPax");

    $("#divbuttonappend").html(' ');
    if (FrmHotelPax == "true") {
        Reconfirmed();
    }
   
    if (FrmHotelPax == "true") {
        //  Reconfirmed();
        window.localStorage.setItem("HotelPax", "false");
        setTimeout(function () {
            var bstatus = "confirmed"
            SendMailBookingInfo(Status);
        }, 3000);
    }
    setTimeout(function () {
        BindMyUserBookingInfo();

    }, 3000);
});
function BindMyUserBookingInfo() {
    $('#divmybookinginfo').html('');
    var DataBooking = {
        User: UserID
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/MyBookHotelinfo",
        data: JSON.stringify(DataBooking),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingInformationn,
        failure: function (response) {
            alert(response.d);
        }
    });


    function BookingInformationn(data) {

        if (data.d == "NOINFO") {

            alertify.alert('Hi user', 'We are still waiting for you to book your first booking with us', function () { window.location.href = "//HOTEL/searchhotel.html"; });


        }
        else {
            console.log(JSON.parse(data.d));
            var userdetails = $.parseJSON(data.d);
            var Book = userdetails[0].BookingRefID;
             Bookinfo(Book);
            console.log(userdetails);
            var flag = '0';
            var active = '';
            for (var ittt = 0; ittt < userdetails.length; ittt++) {
                var bookingID = userdetails[ittt].BookingRefID;
                ETripName = (userdetails[ittt].ItemName).toUpperCase();
                var Depttime = userdetails[ittt].CheckInDate;
                var Day = moment(Depttime).format("ddd");
                var Date = moment(Depttime).format("DD");
                var Month = moment(Depttime).format("MMM");
                var Year = moment(Depttime).format("YYYY");
                var Time = userdetails[ittt].Timesplit;
                Time == Time.substring(3, Time.length);

                console.log(Time);
                $('#divmybookinginfo').append('<li onclick="Bookinfo(' + bookingID + ')" class=""> <a> <div class="flicon-h"></div> <div class="date pull-left"> <span class="size14 bold">' + Day + " " + Date + '</span> <span class="size12 semibold">' + Month + " " + Year + '</span> </div> <div class="depart pull-left"> <span class="size14 bold">' + ETripName + '</span> </div> <img src="/HOTEL/hotel/images/tab-arrow.png" alt="title"> <div class="clearfix"></div> </a> </li>')

                $('#divmybookinginfomobile').append('<li onclick="bookingInfoBindMobile(' + bookingID + ')" class=""> <a> <div class="flicon-h"></div> <div class="date pull-left"> <span class="size14 bold">' + Day + " " + Date + '</span> <span class="size12 semibold">' + Month + " " + Year + '</span> </div> <div class="depart pull-left"> <span class="size14 bold">' + ETripName + '</span>  </div> <img src="/HOTEL/hotel/images/tab-arrow.png" alt="title"> <div class="clearfix"></div> </a> </li>')

            }
            if ($('#divscroll').height() > 600) {

                $('#divscroll').toggleClass("scroll scrollauto");
            }
            $('#divmybookinginfo li:first').addClass("active");

            $('#divmybookinginfo li').click(function () {
                $('#divmybookinginfo li').removeClass("active");
                $(this).addClass("active");
            });

            $('#divmybookinginfomobile li:first').addClass("active");

            $('#divmybookinginfomobile li').click(function () {
                $('#divmybookinginfomobile li').removeClass("active");
                $(this).addClass("active");
            });

        }
    }
}
function Bookinfo(bookingID) {
    var DataBooking = {
        bookingID: bookingID
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/MyBookHotelinformation",
        data: JSON.stringify(DataBooking),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: BookHotelResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    BookPaxinfo(bookingID)
}
function BookHotelResults(data) {
    var HotelbookinfoData = JSON.parse(data.d);
    console.log(HotelbookinfoData);
    if (HotelbookinfoData == undefined || HotelbookinfoData == null) {

        window.location.href = "/HOTEL/searchhotel.html";
    }
    else {
        $('#StayIn').html('PILLOWS BOOKED AT ' + (HotelbookinfoData[0].ItemName).toUpperCase());

        ETripName = (HotelbookinfoData[0].ItemName).toUpperCase();
        MarkupValue = HotelbookinfoData[0].markup;
       // Rateofchange = HotelbookinfoData[0].rateofchange;
       // console.log(Rateofchange);
        var BookingDate = HotelbookinfoData[0].BookingDate;
        CheckinDate = HotelbookinfoData[0].CheckInDate;
        Cstatus = HotelbookinfoData[0].BookingStatusCode;
        Time = moment(HotelbookinfoData[0].fromdate).subtract(2, 'day');
        Voucherdet = HotelbookinfoData[0].VoucherUrl;
        Status = GetStatus(HotelbookinfoData[0].BookingStatusCode);
        var CheckOut = HotelbookinfoData[0].CheckOutDate;
        HotelAddress = HotelbookinfoData[0].HotelAddress;
        ETripID = HotelbookinfoData[0].BookingRefID;
        hotelId = HotelbookinfoData[0].HotelInformationID;
        GtaAotNumber = HotelbookinfoData[0].GtaAotNumber;
        if (GtaAotNumber == undefined) {
            GtaAotNumber = 'Not Available';
        }
        voucherbookingdate = moment(BookingDate).format("DD MMM YYYY");
        $("#BookedOn").html('Booked on ' + (moment(BookingDate).format("DD MMM YYYY")));
        $('#RefernceNo').html(HotelbookinfoData[0].BookingRefID);
        //document.title = AgencyShortName +'booked at ' + (HotelbookinfoData[0].ItemName).toLowerCase() + ' - ' + HotelbookinfoData[0].BookingRefID;
        $("#BookingDate").html((moment(BookingDate).format("DD MMM YYYY")));
        $('#CheckinDate').html(moment(CheckinDate).format("DD MMM YYYY"));
        $('#Status').html(Status + '<span id="divhistory"></span><a><i id="history" data-open="0" onclick="GetHistory(' + HotelbookinfoData[0].BookingRefID + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Status_History" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');
        $('#CheckOut').html(moment(CheckOut).format("DD MMM YYYY"));
        var Checkin = moment(CheckinDate);
        var CheckOutt = moment(CheckOut);
        chin = moment(CheckinDate).format("DD MMM YYYY");
        chout = moment(CheckOut).format("DD MMM YYYY");
        gettoday = moment().format("DD MMM YYYY");
        var DifferenceMilli = CheckOutt - Checkin;
        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
        var Night = DifferenceMilliDuration.days();
        $('#Nights').html(Night);
        var totalprice = parseFloat(0);
        for (var iRoom = 0; iRoom < HotelbookinfoData.length; iRoom++) {
            totalprice += parseFloat(HotelbookinfoData[iRoom].price);
        }
        if (Time == 'Invalid date') {
            $('#TimeLimit').html("Not Available");
        }
        else {
            $('#TimeLimit').html(moment(Time).format("DD MMM YYYY"));
        }
        $('#Accomedation').html(HotelbookinfoData[0].ItemName);
        $('#divaddress').html(HotelAddress);
        $('#SupplyRef').html(HotelbookinfoData[0].APIRefID);
        APIid = HotelbookinfoData[0].APIRefID;
        grandtotal = (GetRoomPriceConvMark(totalprice, MarkupValue, Rateofchange)) + " USD";
        cancelpriceamount = HotelbookinfoData[0].cancelamount;
        if (cancelpriceamount == undefined || cancelpriceamount == null) {

            $('#Total').html((GetRoomPriceConvMark(totalprice, MarkupValue, Rateofchange)) + " USD" + '<span id="divFarebrkup"></span><a><i id="farebreakup" data-open="0" onclick="GetpriceBreak(' + hotelId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Price_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');

        }
        else {

            CancelNetAmount = HotelbookinfoData[0].CancelNetAmount;
            $('#Total').html((GetRoomPriceConvMark(cancelpriceamount, MarkupValue, Rateofchange)) + "( " + (GetRoomPriceConvMark(totalprice, MarkupValue, Rateofchange)) + " )" + " USD" + '<span id="divFarebrkup"></span><a><i id="farebreakup" data-open="0" onclick="GetpriceBreak(' + hotelId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Price_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');

        }

    }
    if (RoleBookingHis == '0') {
        $("#history").remove();
    }
    if (RoleNetcostbrkup == '0') {
        $("#farebreakup").remove();
    }
}

function GetStatus(status) {
    var Checkiffinished = false;
    var today = new Date();
    today = moment(today);
    var up = '0';
    var Chdatee = moment(CheckinDate);
    //  var c = 24 * 60 * 60 * 1000, diffDays = Math.round(Math.abs((Chdatee - today) / (c)));
    var diffDays = Math.floor((((Chdatee - today) % 31536000000) % 2628000000) / 86400000);
    if (diffDays <= 0) {
        $("#divbuttonappend").html(' ');
        Checkiffinished = true;
    }
    var CurrentDate = new Date();
    CurrentDate = moment(CurrentDate);
    var limit = moment(Time);
    var F = 24 * 60 * 60 * 1000
    var days = Math.floor((((limit - CurrentDate) % 31536000000) % 2628000000) / 86400000);
    var diff = Math.floor((((Chdatee - CurrentDate) % 31536000000) % 2628000000) / 86400000);
    if (days <= 0 && diff >= 0) {
        $('#Divcancellationalert').html("Please note: Booking is under cancellation period")
    }

    var Sat = "";
    if (status == "HK") {
        Sat = "Confirmed";
        $("#divbuttonappend").html(' ');
        $("#divbuttonappend").append('<a id="btnreconfirmed" onclick="Reconfirmed()"><i class="addclass"></i>RECONFIRM</a>');
        if (Checkiffinished == false) {
            $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
        }
    }
    else if (status == "RR") {
        Sat = "Confirmation Pending";
        $("#divbuttonappend").html(' ');
        $("#divbuttonappend").append('<a id="btnUpdate" onclick="Update(' + up + ')"><i class="greatclass"></i>UPDATE</a>');
        if (Checkiffinished == false) {
            $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
        }

    }
    else if (status == "XX") {
        Sat = "Cancelled";
        $("#divbuttonappend").html(' ');
        $('#Divcancellationalert').html('');
    }
    else if (status == "RC") {
        Sat = "Reconfirmed";
        $("#divbuttonappend").html(' ');
        if (Checkiffinished == false) {
            $("#divbuttonappend").append('<a id="btnCancelPnr" data-toggle="modal" data-target="#Cancelbookingpopup" title="Cancel booking"><i class="newclass"></i>CANCEL</a>');
        }

        if (Voucherdet !== null) {
            $("#divbuttonappend").append('<a href="' + Voucherdet + '" id="btnDownloadVochuer" download>DOWNLOAD VOUCHER</a>');

        }
        else {
            $("#divbuttonappend").append('<a id="btnDownloadVochuer" download data-open="0" onclick="DownloadVochuer()">DOWNLOAD VOUCHER</a>');

        }
        $("#divbuttonappend").append('<a data-toggle="modal" data-target="#modalCancelMail" title="Email Voucher" id="btnEmailVochuer">EMAIL VOUCHER</a>');
        //$("#divbuttonappend").append('<a id="btnInvoice" onclick="Invoice()">INVOICE</a>');
    }
    else if (status == "RQ") {
        Sat = "On request";
        $("#divbuttonappend").html(' ');

        $("#divbuttonappend").append('<a id="btnUpdate" onclick="Update(' + up + ')"><i class="greatclass"></i>UPDATE</a>');

    }
    else if (status == "XR") {
        Sat = "Cancellation On request";
        $("#divbuttonappend").html(' ');
        $("#divbuttonappend").append('<a id="btnUpdate" onclick="Update(' + up + ')"><i class="greatclass"></i>UPDATE</a>');

    }
    if (RoleUpdate == '0') {
        $("#btnUpdate").remove();
    }
    if (RoleVoucher == '0') {
        $("#btnDownloadVochuer").remove();
        $("#btnEmailVochuer").remove();
    }
    if (RoleCancel == '0') {
        $("#btnCancelPnr").remove();
    }

    return Sat;
}
function BookPaxinfo(bookingID) {
    var DataBooking = {
        bookingID: bookingID
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/MyBookPassengersHotelinfo",
        data: JSON.stringify(DataBooking),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: BookpaxHotelResults,
        failure: function (response) {
            alert(response.d);
        }
    });

}
function BookpaxHotelResults(data) {
    $('#ROOPAX').html('');
    EPaxData = '';
    VoucherPax = '';
    var HotelPaxData = JSON.parse(data.d);
    console.log(HotelPaxData);

    for (var iPax = 0; iPax < HotelPaxData.length; iPax++) {
        Paxname = HotelPaxData[iPax].Column1;
        var val = parseInt(iPax) + parseInt(1);
        var Passname = HotelPaxData[iPax].Column1;
        var rooms = HotelPaxData[iPax].RoomCategory;
        var roomcots = HotelPaxData[iPax].HotelPaxRoomCots;
        var brkfst = HotelPaxData[iPax].MealBasis;
        var Sharebed = HotelPaxData[iPax].SharingBedding;
        var ChildAge = HotelPaxData[iPax].ChildAge;
        var agesplitchd = ChildAge.split("/");
        var vochurage = '';
        for (var ichd = 0; ichd < agesplitchd.length; ichd++) {
            var chdsplit = agesplitchd[ichd].split(",");
            if (chdsplit[0] == "CHD") {
                vochurage += chdsplit[1] + ",";
            }

        }
        vochurage = vochurage.substr(0, vochurage.length - 1) + '';
        vochurage = " ( " + vochurage + " ) ";
        console.log(brkfst)
        if (brkfst == "Breakfast,") {
            brkfst = "Full Breakfast";
        }
        var Adult = HotelPaxData[iPax].HotelPaxRoomAdults;
        var Child = HotelPaxData[iPax].HotelPaxRoomChildren;
        var Sharebedresult = '';
        if (Child != 0) {
            if (Sharebed == true) {
                Sharebedresult = " (extra bed is not provided)";
            }
            else if (Sharebed == false) {
                Sharebedresult = " (extra bed is provided)";
            }

        }
        var RoomId = HotelPaxData[iPax].BookingRoomID;
        if (roomcots != 0) {
            var cotstodisplay = " ," + roomcots + " x Cot(s)";
            $("#divcots").show();
        }
        else {
            var cotstodisplay = "";
        }
        //$('#ROOPAX').append('<tr><td>' + rooms + ' (' + brkfst + ')' + cotstodisplay + ' <a><i style="display:none" id="idEssentialInfo" data-open="0" onclick="GetEssentialInfo(' + RoomId + ')" class="fa fa-info-circle" data-toggle="modal" data-target="#Essential_info" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td> <td>' + Adult + '</td> <td>' + Child + '</td> <td>' + Passname + '<span id="divTotal"></span><a><i id="ifbreakup" data-open="0" onclick="GetPaxBreak(' + RoomId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#PAX_breakup" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td></tr>');
        var Info = HotelPaxData[iPax].Info;
        if (Info != undefined) {
            $('#ROOPAX').append('<tr><td>' + rooms + ' (' + brkfst + ')' + cotstodisplay + ' <a><i  title="Essential Information"  id="idEssentialInfo" data-open="0" onclick="GetEssentialInfo(' + RoomId + ')" class="fa fa-info-circle" data-toggle="modal" data-target="#Essential_info" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td> <td>' + Adult + '</td> <td>' + Child + Sharebedresult + '</td> <td>' + Passname + '<span id="divTotal"></span><a><i id="ifbreakup" data-open="0" onclick="GetPaxBreak(' + RoomId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#PAX_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td></tr>');
            EPaxData += '<tr><td>' + rooms + ' (' + brkfst + ')' + cotstodisplay + '</td> <td>' + Adult + '</td> <td>' + Child + Sharebedresult + '</td> <td>' + Passname + '</td></tr><tr> <td colspan="6" style="text-align:center; font-weight:bold;">' + Info + '</td> </tr>';
            VoucherPax += '<tr> <td style="text-align:center;">' + val + '</td> <td colspan="2" style="text-align:center;">' + rooms + ' (' + brkfst + ')' + cotstodisplay + '</td> <td style="text-align:center;">' + Adult + '</td> <td style="text-align:center;">' + Child + vochurage + Sharebedresult + '</td> <td style="text-align:center;">' + Passname + '</td> </tr><tr> <td colspan="6" style="text-align:center; font-weight:bold;">' + Info + '</td> </tr>'

        }
        else {
            $('#ROOPAX').append('<tr><td>' + rooms + ' (' + brkfst + ')' + cotstodisplay + '</td> <td>' + Adult + '</td> <td>' + Child + Sharebedresult + '</td> <td>' + Passname + '<span id="divTotal"></span><a><i id="ifbreakup" data-open="0" onclick="GetPaxBreak(' + RoomId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#PAX_breakup" style="color :#ff5929;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a></td></tr>');
            EPaxData += '<tr><td>' + rooms + ' (' + brkfst + ')' + cotstodisplay + '</td> <td>' + Adult + '</td> <td>' + Child + Sharebedresult + '</td> <td>' + Passname + '</td></tr>';
            VoucherPax += '<tr> <td style="text-align:center;">' + val + '</td> <td colspan="2" style="text-align:center;">' + rooms + ' (' + brkfst + ')' + cotstodisplay + '</td> <td style="text-align:center;">' + Adult + '</td> <td style="text-align:center;">' + Child + vochurage + Sharebedresult + '</td> <td style="text-align:center;">' + Passname + '</td> </tr>'
        }

    }
    //$('[data-toggle="tooltip"]').tooltip();
}
function GetEssentialInfo(RoomId) {

    var Datass = {

        RoomId: RoomId

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/BookingHotelEssentialInfo",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelEssentialInfoSuccess,
        failure: function (response) {
            alert(response.d);

        }
    });
}
function BookingHotelEssentialInfoSuccess(data) {
    $("#tableEssentialinfo").html('');

    if (data.d == "NODATA") {
        $("#tableEssentialinfo").append('<tr><td>No information available</tr></td>');
    }
    else {
        var HotelEssentialInfo = JSON.parse(data.d);
        console.log(HotelEssentialInfo);
        for (var info = 0; info < HotelEssentialInfo.length; info++) {
            var val = info;
            val = parseFloat(parseFloat(val) + parseFloat(1));
            var Info = HotelEssentialInfo[info].Information;
            $("#tableEssentialinfo").append('<tr><td>' + val + " . " + Info + '</tr></td>');
        }
    }

}
function GetPaxBreak(ItemRef) {
    $(".loadingGIF").show();
    var Datass = {

        ItemRef: ItemRef

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/BookingHotelRoomPaxBrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelRoomPaxBrkup,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function BookingHotelRoomPaxBrkup(data) {
    $("#tablePAXBreakUps").html('');
    var HotelPaxDatabrkup = JSON.parse(data.d);
    console.log(HotelPaxDatabrkup);

    for (var ifb = 0; ifb < HotelPaxDatabrkup.length; ifb++) {
        var val = ifb;
        val = parseFloat(parseFloat(val) + parseFloat(1));
        var Fname = HotelPaxDatabrkup[ifb].PaxFirstName;
        var LName = HotelPaxDatabrkup[ifb].PaxLastName;
        var Title = HotelPaxDatabrkup[ifb].PaxTitle;
        var PaxAge = HotelPaxDatabrkup[ifb].PaxTypeAge;
        if (Title == 2 || Title == 5) {
            var agesplit = PaxAge.split(",");

            $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Gettitle(Title) + " " + Fname + " " + LName + " ( Age " + agesplit[1] + ")" + '</tr></td>');
        }
        else {
            $("#tablePAXBreakUps").append('<tr><td>' + val + ". " + Gettitle(Title) + " " + Fname + " " + LName + '</tr></td>');
        }

    }
    $(".loadingGIF").hide();
}

function Gettitle(Title) {
    var Out = '';
    if (Title == "1") {
        Out = "Mr";
    }
    else if (Title == "2") {
        Out = "Miss";
    }
    else if (Title == "3") {
        Out = "Mrs";
    }
    else if (Title == "4") {
        Out = "Ms";
    }
    else if (Title == "5") {
        Out = "Master";
    }

    return Out;

}

function GetFareBreak() {
    $(".loadingGIF").show();
    var Datass = {

        hotelId: hotelId

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/GetFarebrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelFarebrkupResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetHotelFarebrkupResults(data) {
        $("#tableFareBreakUps").html('');
        var HotelFareDatabrkup = JSON.parse(data.d);
        console.log(HotelFareDatabrkup);
        var total = parseFloat(0);
        $('#tableFareBreakUps').append('<thead> <tr> <th class="sp_table">Rooms</th> <th class="sp_table">Room Cost</th> </tr> </thead> <tbody>')
        for (var ifb = 0; ifb < HotelFareDatabrkup.length; ifb++) {
            var val = parseFloat(parseFloat(ifb) + parseFloat(1));
            var trroom = val;
            var trBaseFare = +((parseFloat(HotelFareDatabrkup[ifb].TotalSellAmount) * (parseFloat(1)))) + " " + "USD" + '';
            var fare = (parseFloat(HotelFareDatabrkup[ifb].TotalSellAmount) * (parseFloat(1)));
            total += parseFloat(fare);
            var mealtype = '';
            if (HotelFareDatabrkup[ifb].MealBasis == "Breakfast,") {
                mealtype = 'Full Breakfast'
            }
            else {
                mealtype = HotelFareDatabrkup[ifb].MealBasis;
            }
            $('#tableFareBreakUps').append('<tr><td>' + val + ". " + HotelFareDatabrkup[ifb].RoomCategory + "(" + mealtype + ")" + '</td> <td>' + Math.ceil(fare * 100) / 100 + " " + "USD" + '</td></tr>');

        }
        $('#tableFareBreakUps').append('</tbody>');
        $("#TotalCost").html('Total Cost : ' + Math.ceil(total * 100) / 100 + " USD");
    }
    $(".loadingGIF").hide();
}
function GetpriceBreak(hotelId) {
    $(".loadingGIF").show();
    var Datass = {

        hotelId: hotelId

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/GetFarebrkup",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelpricebrkupResults,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetHotelpricebrkupResults(data) {
        $("#tablePriceBreakUps").html('');
        var HotelFareDatabrkup = JSON.parse(data.d);
        console.log(HotelFareDatabrkup);
        var totalnet = HotelFareDatabrkup[0].NetVale;
        var netamount = '<tr> <td class="fare_head_area">Net Amount</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].NetVale + '</td></tr>';
        var netCurrency = '<tr> <td class="fare_head_area">Net Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].NetCurrency + '</td></tr>';
        var RateofChange = '<tr> <td class="fare_head_area">Rate Of Exchange</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + HotelFareDatabrkup[0].RateOfExchange + '</td></tr>';
        var mvalue = HotelFareDatabrkup[0].MarkUpValue;
        var Markup = '<tr> <td class="fare_head_area">Markup %</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + mvalue + '</td></tr>';
        var fare = GetRoomPriceConvMark(totalnet, mvalue, HotelFareDatabrkup[0].RateOfExchange);
        var Total = '<tr> <td class="fare_head_area">Sell Amount</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + fare + '</td></tr>';
        var FareCurrency = '<tr> <td class="fare_head_area">Sell Currency</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + "USD" + '</td></tr>';
        if (cancelpriceamount == undefined || cancelpriceamount == null) {
            $("#tablePriceBreakUps").append(Total + FareCurrency + netamount + netCurrency + RateofChange + Markup);

        }
        else {
            var pricebrupcancel = GetRoomPriceConvMark(cancelpriceamount, MarkupValue, Rateofchange);
            var canamount = '<tr> <td class="fare_head_area">Cancellation Amount</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + pricebrupcancel + " USD" + '</td></tr>';
            var netAmnt = '<tr> <td class="fare_head_area">Net Cancellation  Charge</td> <td>&nbsp;</td> <td class="fare_head_area01">' + ": " + CancelNetAmount + " USD" + '</td></tr>';

            $("#tablePriceBreakUps").append(Total + FareCurrency + netamount + netCurrency + RateofChange + Markup + canamount + netAmnt);
        }
    }
    $(".loadingGIF").hide();
}
function GetRoomPriceConvMark(ogprice, markup, Rateofchange) {
    var OGPricee = parseFloat(ogprice);
    var NewPrice = parseFloat(0);
    var NewPriceMarkup = parseFloat(0);
    var MarkupValuePerc = parseFloat(markup);
    var SellingRateUSD = parseFloat(Rateofchange);
    var BuyyingRateOMR = parseFloat(1);
    NewPrice = (OGPricee * SellingRateUSD) / BuyyingRateOMR;
    NewPriceMarkup = NewPrice + (NewPrice * (MarkupValuePerc / 100));
    return Math.ceil(NewPriceMarkup * 100) / 100;
    //return ogprice;
}

function CancellationPolicy(tthis) {
    $(".loadingGIF").show();
    var dataopen = $(tthis).attr("data-open");
    //alert(dataopen);
    if (dataopen == 0) {

        var Datass = {

            hotelId: hotelId

        }
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodDbHotel.aspx/Getcancellationpolicy",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: GetHotelcancellationpolicyResults,
            failure: function (response) {
                alert(response.d);
            }
        });
        function GetHotelcancellationpolicyResults(data) {
            $("#divcancellationpolicy").html('');
            var Hotelcancellationpolicy = JSON.parse(data.d);
            console.log(Hotelcancellationpolicy);
            var CancPolicyString = '';
            var bookidold = '';
            var roomcount = parseInt(0);

            var arr = [];
            for (var iarr = 0; iarr < Hotelcancellationpolicy.length; iarr++) {
                arr.push(Hotelcancellationpolicy[iarr].BookingRoomID)
            }
            console.log(arr)
            var uniarr = _.uniq(arr)
            console.log(uniarr)

            for (var i = 0; i < uniarr.length; i++) {
                CancPolicyString = '#RoomCategory#</button> <div class="panel"> <div class="accodion_cont"><p>';

                for (var ifc = (Hotelcancellationpolicy.length - 1) ; ifc >= 0; ifc--) {

                    if (uniarr[i] == Hotelcancellationpolicy[ifc].BookingRoomID) {
                        if (Hotelcancellationpolicy.length < 3) {
                            var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                            if (Meal == "Breakfast,") {
                                Meal = "Full Breakfast";
                            }
                            else {
                                var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                            }
                            var RCat = Hotelcancellationpolicy[ifc].RoomCategory + " (" + Meal + ")";
                            CancPolicyString = CancPolicyString.replace("#RoomCategory#", RCat);
                            var cancelfrmdate = Hotelcancellationpolicy[ifc].FromDate;
                            var canceltodate = Hotelcancellationpolicy[ifc].ToDate;
                            var cancelamount = Hotelcancellationpolicy[ifc].ChargeAmount;
                            cancelfrmdate = moment(cancelfrmdate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                            //CancPolicyString += '<li style="color:red;font-weight:bolder;">Non Refundable Booking with Cancellation Charges <span>' + +GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue, Rateofchange) + ' ' + " OMR" + '</span></li>';
                            if (cancelamount == "0") {
                                var cancelfrmdate = Hotelcancellationpolicy[ifc].FromDate;
                                var canceltodate = Hotelcancellationpolicy[ifc].ToDate;
                                canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Free cancellation on or before <span>' + canceltodate + '</span></li>';


                            }
                            else if (moment(Hotelcancellationpolicy[ifc].ToDate).format("DD MMM YYYY") == moment(CheckinDate).format("DD MMM YYYY")) {
                                var canceltodate = moment(Hotelcancellationpolicy[ifc].FromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                //canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Cancellation on or after <span>' + canceltodate +
                                                                        '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue, Rateofchange) + ' ' + " USD" + '</span></li>';
                            }
                            else {
                                var cancelfrmdate = moment(Hotelcancellationpolicy[ifc].FromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                var canceltodate = moment(Hotelcancellationpolicy[ifc].ToDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Cancellation between <span>' + cancelfrmdate +
                                                                        '</span> and <span>' + canceltodate + '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue, Rateofchange) + ' ' + " USD" + '</span></li>';
                            }
                        }
                        else {
                            var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                            if (Meal == "Breakfast,") {
                                Meal = "Full Breakfast";
                            }
                            else {
                                var Meal = Hotelcancellationpolicy[ifc].MealBasis;
                            }
                            var RCat = Hotelcancellationpolicy[ifc].RoomCategory + " (" + Meal + ")";
                            var cancelamount = Hotelcancellationpolicy[ifc].ChargeAmount;

                            CancPolicyString = CancPolicyString.replace("#RoomCategory#", RCat);
                            if (cancelamount == "0") {
                                var cancelfrmdate = Hotelcancellationpolicy[ifc].FromDate;
                                var canceltodate = Hotelcancellationpolicy[ifc].ToDate;
                                canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Free cancellation on or before <span>' + canceltodate + '</span></li>';


                            }
                            else if (moment(Hotelcancellationpolicy[ifc].ToDate).format("DD MMM YYYY") == moment(CheckinDate).format("DD MMM YYYY")) {
                                var canceltodate = moment(Hotelcancellationpolicy[ifc].FromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                //canceltodate = moment(canceltodate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Cancellation on or after <span>' + canceltodate +
                                                                        '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue, Rateofchange) + ' ' + " USD" + '</span></li>';
                            }
                            else {
                                var cancelfrmdate = moment(Hotelcancellationpolicy[ifc].FromDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                var canceltodate = moment(Hotelcancellationpolicy[ifc].ToDate).subtract(CancellationGPeriod, 'day').format("MMM DD YYYY, dddd");
                                CancPolicyString += '<li style="color:#0c5b9a;">Cancellation between <span>' + cancelfrmdate +
                                                                        '</span> and <span>' + canceltodate + '</span> will be charged <span>' + GetRoomPriceConvMark(Hotelcancellationpolicy[ifc].ChargeAmount, MarkupValue, Rateofchange) + ' ' + " USD" + '</span></li>';
                            }

                        }
                    }


                }
                $("#divcancellationpolicy").append('<button class="accordion">' + CancPolicyString + '</p></div></div>');

            }




            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].onclick = function () {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    if (panel.style.maxHeight) {
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }
                }
            }
        }
        $(tthis).attr("data-open", "1");
        //  $("#divcancellationpolicy").append('</ul>');
    }
    $(".loadingGIF").hide();
}

function GetHistory(BookingID) {
    $(".loadingGIF").show();
    $('#tablehistory').html('');
    $("loadingGIFFareBr").show();
    var Datass = {

        BookingID: BookingID

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/GetHotelBookingHistory",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: GetHotelBookingHistory,
        failure: function (response) {
            alert(response.d);
        }
    });

    function GetHotelBookingHistory(data) {
        $("#tablehistory").html('');
       
        if (data.d != 'NOINFO') {
            var Hotelhistory = JSON.parse(data.d);
            console.log(Hotelhistory);
            for (var iH = 0; iH < Hotelhistory.length; iH++) {
                var Status = Hotelhistory[iH].BookingStatusCode;
                if (Status == "RQ") {
                    Status = "On Requested";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }
                else if (Status == "HK") {
                    Status = "Confirmed";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');


                }
                else if (Status == "RR") {
                    Status = "Confirmation Pending";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }
                else if (Status == "XX") {
                    Status = "Cancelled";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }
                else if (Status == "RC") {
                    Status = "Reconfirmed";
                    var Actiondate = moment(Hotelhistory[iH].ActionDate).format("DD MMM YYYY");
                    $('#tablehistory').append('<li class="list-group-item"> <span class="badge  new_lable">' + moment(Hotelhistory[iH].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + Status + '</b> - ' + Hotelhistory[iH].FirstName + ' ' + Hotelhistory[iH].LastName + '</li>');

                }

            }

        }
        else {
            $('#tablehistory').html('<tr><td>' + "No history available ! " + '</td></tr>');

        }
        $("loadingGIFFareBr").hide();
        $(".loadingGIF").hide();
    }


}
function SendMailBookingInfo(bstatus) {
    var mailid = window.localStorage.getItem("EMail");
    var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
    var Dastasseq = {
        EUName: mailname,
        ETripID: ETripID,
        EPaxData: EPaxData,
        UserMailID: mailid,
        ETripName: ETripName,
        ETotalCharge: grandtotal,
        bstatus: bstatus

    };
    console.log(Dastasseq)
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/SendMailBookhotel",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SendMailTicketIssueSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                //alert('500');
            }
        }
    });
}
function SendMailTicketIssueSuccess() {

}

function CancelBooking() {
    sendmailbookcancelb2c();

}

function sendmailbookcancelb2c()
{
    var parass = {
        APIid: APIid,
        ETripIDD: ETripID,
        EUNamee: EUName,
        ECommentt: $("#txtChangeTravelDateComment").val()
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/CancelbookinghotelMail",
        data: JSON.stringify(parass),
        contentType: "application/json; charset=utf-8",
        success: CancelbookingmaillSuccess,
        failure: function (response) {
            alert(response.d);
        },
        dataType: "json"
    });
}
function CancelbookingmaillSuccess() {
    alertify.alert('Cancel', 'Admin is notified about your cancellation request by mail');

}
function BookingHotelCancel(data) {
    var Hotelbookcancelinfo = data.d;
    var x2js2 = new X2JS();
    var jsonHotelCancelRes = x2js2.xml_str2json(Hotelbookcancelinfo);
    console.log(jsonHotelCancelRes);
    if (jsonHotelCancelRes != '') {
        JBookCancelRes = jsonHotelCancelRes.Response.ResponseDetails.BookingResponse;
        var Error = JBookCancelRes.Errors;
        if (Error == undefined) {
            Statuscancellation = JBookCancelRes.BookingStatus._Code;
            cancelAmount = JBookCancelRes.BookingFee;
            console.log(cancelAmount);

            if (cancelAmount != undefined) {
                cancelAmount = JBookCancelRes.BookingFee.__text;
                CancelCurrency = JBookCancelRes.BookingFee._Currency;
                var actualdisplayamount = GetRoomPriceConvMark(cancelAmount, MarkupValue, Rateofchange) + " USD";
                CustomerAmount = GetRoomPriceConvMark(cancelAmount, MarkupValue, Rateofchange);
            }
            else {
                var actualdisplayamount = "0.00 USD"
                CustomerAmount = "0";
                cancelAmount = "0";
                CancelCurrency = "USD";
            }

            var Datass = {

                Statuscancellation: Statuscancellation

            }
            $.ajax({
                type: "POST",
                url: "/HOTEL/WebMethodDbHotel.aspx/Changehotelbookingstatusgetcanceldata",
                data: JSON.stringify(Datass),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: BookingHotelCancelsuccess,
                failure: function (response) {
                    alert(response.d);

                }
            });

        }
        else {
            alertify.alert('Cancel', 'You cannot cancel this booking,its already exceed the limit of cancellation !');

            setTimeout(function () {
                $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
            }, 3000);

            return true;
        }
    }
    else {
        alertify.alert('Cancel', 'Error occured!Please contact admin !');

        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);

        return true;
    }

}
function BookingHotelCancelsuccess(data) {
    var dbcancelamount = $.parseJSON(data.d);
    console.log(dbcancelamount);
    console.log(cancelAmount);
    console.log(CancelCurrency);
    console.log(CustomerAmount);
    var dbcancellationcharge = dbcancelamount[0].comparecharge;
    var Datass = {

        cancelAmount: cancelAmount,
        CancelCurrency: CancelCurrency,
        CustomerAmount: CustomerAmount,
        dbcancellationcharge: dbcancellationcharge


    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/InsertCancelBookingPriceDetails",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: InsertCancelBookingPriceDetailsSuccess,
        failure: function (response) {
            alert(response.d);

        }
    });

}
function InsertCancelBookingPriceDetailsSuccess(data) {
    if (data.d == 'NOSESSION') {
        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);
        Bookinfo();

        // window.location.href = "/index.html";
    }
    else {
        var Pricedetails = $.parseJSON(data.d);
        console.log(Pricedetails);
        var cancelcustomeramount = Pricedetails[0].DbCancelAmount;
        if (cancelcustomeramount != undefined) {
            cancelcustomeramount = GetRoomPriceConvMark(cancelcustomeramount, MarkupValue, Rateofchange);

        }
        else {
            cancelcustomeramount = "0.00 USD";
        }
        if (Statuscancellation == "X ") {
            alertify.alert('Cancel', 'Your booking has been cancelled successfully with cancellation charges  ' + cancelcustomeramount + '!');
            SendMailCancelbooking("Cancelled");
            Bookinfo();
        }
        else if (Statuscancellation == "CP " || Statuscancellation == "CP" || Statuscancellation == "P" || Statuscancellation == "XP") {
            alertify.alert('Cancel', 'Your booking has been pending for cancellation  with  charges ' + cancelcustomeramount + '!');
            Bookinfo();

        }
        else if (Statuscancellation == "C ") {
            alertify.alert('Cancel', 'You cannot cancel this booking,its already exceed the limit of cancellation !');
            Bookinfo();


        }
        Bookinfo();
        //   $('#Total').html(cancelcustomeramount + "("+(GetRoomPriceConvMark(totalprice, MarkupValue, Rateofchange))+")" + " OMR" + '<span id="divFarebrkup"></span><a><i id="farebreakup" data-open="0" onclick="GetpriceBreak(' + hotelId + ')" class="fa fa-plus-square" data-toggle="modal" data-target="#Price_breakup" style="color :#2f3292;margin-left:10px;cursor:pointer" aria-hidden="true"></i>');
        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);

    }
}
function Reconfirmed() {
    $(".addclass").addClass("fa fa-circle-o-notch fa-spin");
    var Statuscancel = "RC";
    var Datass = {

        Statuscancel: Statuscancel

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/Changehotelbookingstatus",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelReconfirmed,
        failure: function (response) {
            alert(response.d);

        }
    });
}
function BookingHotelReconfirmed() {
    // var bstatus="reconfirmed"
    //// SendMailBookingInfo(bstatus)
    // Bookinfo();

    // alertify.alert('Reconfirm', 'Your booking has been reconfirmed successfully');
    // setTimeout(function () {
    //     $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
    // }, 3000);

}
function Update(Check) {
    checkval = Check;
    $(".greatclass").addClass("fa fa-circle-o-notch fa-spin");

    var Datass = {

        APIid: APIid

    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/BookingHotelUpdate",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHotelUpdate,
        failure: function (response) {
            alert(response.d);

        }
    });

}

function BookingHotelUpdate(data) {
    var HotelBookingHotelUpdateinfo = data.d;
    var x2js2 = new X2JS();
    var jsonHotelUpdateRes = x2js2.xml_str2json(HotelBookingHotelUpdateinfo);
    console.log(jsonHotelUpdateRes);
    if (jsonHotelUpdateRes != '' && jsonHotelUpdateRes != undefined) {
        var JBookUpdateRes = jsonHotelUpdateRes.Response.ResponseDetails.SearchBookingItemResponse;
        if (JBookUpdateRes != '' && JBookUpdateRes != undefined) {
            updatestatus = jsonHotelUpdateRes.Response.ResponseDetails.SearchBookingItemResponse.BookingStatus._Code;
            if (updatestatus != undefined && updatestatus != '') {
                var Datass = {

                    Statuscancellation: updatestatus

                }
                $.ajax({
                    type: "POST",
                    url: "/HOTEL/WebMethodDbHotel.aspx/Changehotelbookingstatusgetcanceldata",
                    data: JSON.stringify(Datass),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: BookingHotelupdatesuccess,
                    failure: function (response) {
                        alert(response.d);

                    }
                });
                if (checkval == "XX") {
                    if (updatestatus == "C ") {
                        Bookinfo();
                        alertify.alert('Confirmed', 'your booking got confirmed!please cancel it again if you really want to cancel!');
                        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");

                        return true;
                    }
                    else if (updatestatus == "X ") {
                        // Bookinfo();
                    }
                    else {
                        cancelfrmupdate();
                    }
                }
                else {
                    if (updatestatus == "C ") {

                        alertify.alert('Confirmed', 'your booking got confirmed!');
                        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");

                        return true;
                    }
                    else if (updatestatus == "CP") {
                        alertify.alert('Confirmation Pending', 'your booking is not confirmed yet!');
                        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");

                        return true;
                    }
                }
            }

            else {
                alertify.alert('Update', 'Error occured! Please contact admin');
                $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
                return true;
            }
        }
        else {
            alertify.alert('Update', 'Error occured! Please contact admin');
            $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
            return true;
        }
    }
    else {
        alertify.alert('Update', 'Error occured!Please contact admin');
        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
        return true;

    }
    setTimeout(function () {
        $(".greatclass").removeClass("fa fa-circle-o-notch fa-spin");
    }, 3000);

}
function BookingHotelupdatesuccess() {
    if (checkval == "0") {
        Bookinfo();
    }

}
function ShowCancellationChargeIcon() {
    $(".cancpolicydiv h3").each(function () {
        var Xlstatus = $(this).attr("xlcharge");
        if (Xlstatus == "true") {
            var tempid = $(this).parent().parent().parent().parent().attr("id").replace("cmod", "r");
            $("#" + tempid).find("div.cost_area").find("a").append('<i class="fa fa-money red" data-toggle="tooltip" data-placement="top" data-original-title="Under cancellation fare" aria-hidden="true"></i>');
        }
    });
}

//Show hidden cancel policies
function ShowAllCancPolicy(thiss) {
    $(thiss).hide();
    $($(thiss).parent().parent().find("h4")).each(function () {
        $(this).show();
    });
}


function cancelfrmupdate() {
    var CurrentDate = new Date();
    CurrentDate = moment(CurrentDate);
    var limit = moment(Time);
    var F = 24 * 60 * 60 * 1000, Daysdiff = Math.round(Math.abs((limit - CurrentDate) / (F)));
    if (Daysdiff <= 0) {
        alertify.confirm('Note', 'You are under cancellation period!', function () {
            var Datass = {

                APIid: APIid

            }
            $.ajax({
                type: "POST",
                url: "/HOTEL/WebMethodDbHotel.aspx/BookingHotelCancel",
                data: JSON.stringify(Datass),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: BookingHotelCancelupdate,
                failure: function (response) {
                    alert(response.d);

                }
            });
        }, function () { $('#sort_popup').css("z-index", "99999"); });


    }
    else {
        var Datass = {

            APIid: APIid

        }
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodDbHotel.aspx/BookingHotelCancel",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: BookingHotelCancelupdate,
            failure: function (response) {
                alert(response.d);

            }
        });
    }
}
function BookingHotelCancelupdate(data) {
    var Hotelbookcancelinfo = data.d;
    var x2js2 = new X2JS();
    var jsonHotelCancelRes = x2js2.xml_str2json(Hotelbookcancelinfo);
    console.log(jsonHotelCancelRes);
    JBookCancelRes = jsonHotelCancelRes.Response.ResponseDetails.BookingResponse;
    var Error = JBookCancelRes.Errors;
    if (Error == undefined) {
        Statuscancellation = JBookCancelRes.BookingStatus._Code;
        cancelAmount = JBookCancelRes.BookingFee;
        console.log(cancelAmount);

        if (cancelAmount != undefined) {
            cancelAmount = JBookCancelRes.BookingFee.__text;
            CancelCurrency = JBookCancelRes.BookingFee._Currency;
            var actualdisplayamount = GetRoomPriceConvMark(cancelAmount, MarkupValue, Rateofchange) + " USD";
            CustomerAmount = GetRoomPriceConvMark(cancelAmount, MarkupValue, Rateofchange);
        }
        else {
            var actualdisplayamount = "0.00 USD"
            CustomerAmount = "0";
            cancelAmount = "0";
            CancelCurrency = "USD";
        }

        var Datass = {

            Statuscancellation: Statuscancellation

        }
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodDbHotel.aspx/Changehotelbookingstatusgetcanceldata",
            data: JSON.stringify(Datass),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: BookingHotelCancelsuccessupdate,
            failure: function (response) {
                alert(response.d);

            }
        });

    }
    else {
        alertify.alert('Cancel', 'You cannot cancel this booking,its already exceed the limit of cancellation !');

        setTimeout(function () {
            $(".newclass").removeClass("fa fa-circle-o-notch fa-spin");
        }, 3000);

        return true;
    }

}

function BookingHotelCancelsuccessupdate(data) {
    var dbcancelamount = $.parseJSON(data.d);
    if (cancelAmount == "0") {
        var dbcancellationcharge = "0";
    }
    else {
        var dbcancellationcharge = dbcancelamount[0].comparecharge;
    }

    var Datass = {

        cancelAmount: cancelAmount,
        CancelCurrency: CancelCurrency,
        CustomerAmount: CustomerAmount,
        dbcancellationcharge: dbcancellationcharge


    }
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/InsertCancelBookingPriceDetails",
        data: JSON.stringify(Datass),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: InsertCancelBookingPriceDetailsSuccess,
        failure: function (response) {
            alert(response.d);

        }
    });

}

function DownloadVochuer() {
    if ($("#btnDownloadVochuer").attr("data-open") == "0") {
        var Dastasseq = {
            AgencyName: AgencyName,
            AgencyAddress: AgencyAddress,
            AgencyContact: AgencyContact,
            gettoday: gettoday,
            ETripName: ETripName,
            HotelAddress: HotelAddress,
            ETripID: ETripID,
            voucherbookingdate: voucherbookingdate,
            APIid: APIid,
            chin: chin,
            chout: chout,
            VoucherPax: VoucherPax,
            GtaAotNumber: GtaAotNumber
        };
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodDbHotel.aspx/VoucherBookhotel",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: VoucherBookhotelSuccess,
            failure: function (response) {
                alert(response.d);
            },
            statusCode: {
                500: function () {
                    //window.location.href = "/index.html";
                    //$('#loginbt').click();
                    //EmailTktTo();
                    //  alert('500');
                }
            }
        });
    }


}
function VoucherBookhotelSuccess(data) {

    var link = data.d
    $("#btnDownloadVochuer").attr("href", link);
    $("#btnDownloadVochuer").attr("data-open", "1");
    // $("#btnDownloadVochuer").prop("download");
    //$("#btnDownloadVochuer").removeAttr("onclick");
    $("#btnDownloadVochuer").click();

}

function EmailVochuerTo() {
    var mailid = $("#txtTktToMail").val();
    if (mailid != '' || mailid != null || mailid != undefined) {
    var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
    var Dastasseq = {
        AgencyName: AgencyName,
        AgencyAddress: AgencyAddress,
        AgencyContact: AgencyContact,
        gettoday: gettoday,
        ETripName: ETripName,
        HotelAddress: HotelAddress,
        ETripID: ETripID,
        voucherbookingdate: voucherbookingdate,
        APIid: APIid,
        chin: chin,
        chout: chout,
        VoucherPax: VoucherPax,
        Email: mailid,
        mailname: mailname,
        GtaAotNumber: GtaAotNumber
    };

    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/EmailVoucherBookhotel",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: EmailVoucherBookhotelSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                //  alert('500');
            }
        }
    });
    }
    else {
        alertify.alert('Validation', 'Please fill the emailid');
    }
}

function EmailVoucherBookhotelSuccess() {

    alertify.alert('Email', 'Email send successfully');
}


function SendMailCancelbooking(bstatus) {
    var mailid = window.localStorage.getItem("EMail");
    if (mailid == '') {
        mailid = EMailID;
    }
    var mailname = mailid.substring(0, mailid.lastIndexOf("@"));
    var Dastasseq = {
        EUName: mailname,
        ETripID: ETripID,
        EPaxData: EPaxData,
        UserMailID: mailid,
        ETripName: ETripName,
        ETotalCharge: grandtotal,
        bstatus: bstatus

    };
    console.log(Dastasseq)
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodDbHotel.aspx/SendMailCancelbooking",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SendMailCancelbookingSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                //alert('500');
            }
        }
    });
}
function SendMailCancelbookingSuccess() {

}

function bookingInfoBindMobile(bookingID) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: bookingID
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess() {
    window.location.href = "/hotelbookinginfo.html";
}
