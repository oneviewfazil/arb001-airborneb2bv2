﻿/// <reference path="jquery-3.1.1.min.js" />
var DateFormate = 'mm/dd/yy';
//var DateFormate = 'dd/mm/yy';

$(document).ready(function () {
    SessionChecking('UserDetails');
    SearchWithStatus();
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
    $('#DepartureDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#DepartureDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#DepartureDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#DepartureDateTo').datepicker({
        defaultDate: "0d",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#DepartureDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#DepartureDateTo').datepicker("widget").css({ "z-index": 10000 });

    $('#ArrivalDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#ArrivalDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#ArrivalDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#ArrivalDateTo').datepicker({
        defaultDate: "0d",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#ArrivalDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#ArrivalDateTo').datepicker("widget").css({ "z-index": 10000 });

    $('#BookingDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#BookingDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#BookingDateTo').datepicker({
        defaultDate: "0d",
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#BookingDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#BookingDateTo').datepicker("widget").css({ "z-index": 10000 });

    $('#DeadlineDateFrom').datepicker({
        defaultDate: "0d",
        changeYear: true,
        numberOfMonths: 1,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#DeadlineDateTo").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#DeadlineDateFrom').datepicker("widget").css({ "z-index": 10000 });
    $('#DeadlineDateTo').datepicker({
        defaultDate: "0d",
        changeYear: true,
        changeMonth: true,
        numberOfMonths: 1,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#DeadlineDateFrom").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#DeadlineDateTo').datepicker("widget").css({ "z-index": 10000 });
});
function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        $('.AgencyName').html(userdata[0].AgencyName);
        var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }

        var AdminStatus = userdata[0].AdminStatus;
        if (AgencyTypeIDD == '1' && AdminStatus == '1') {
            $('#divddlBranch').show();
            BindBranch();
        }
        else {
            $("#ddlBranch").html($("<option></option>").val(userdata[0].AgencyID).html(userdata[0].AgencyName));
            $('#divddlBranch').hide();

        }
        //Get Markup
        var Dastasseqq = {
            AgencyID: userdata[0].AgencyIDD,
            ServiceTypeID: '1'
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/GetMarkupData",
            data: JSON.stringify(Dastasseqq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: MarkupSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No credit !');
    }
}
function BindBranch() {
    var Datatable = {
        selectdata: '*',
        tablename: 'tblAgency',
        condition: 'NULL'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetTableData",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BindBranchSuccess,
        failure: function (response) {
            alert(response.d);
        }

    });
}
function BindBranchSuccess(data) {
    var Branchdata = $.parseJSON(data.d);
    console.log(Branchdata);
    for (var i = 0; i < Branchdata.length; i++) {
        $("#ddlBranch").append($("<option></option>").val(Branchdata[i].AgencyID).html(Branchdata[i].AgencyName));
    }
}

function SearchWithStatus() {
    var status;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        status = hash[1];
    }
    if (status != undefined) {
        $('#divSearch').hide();
        $('#divSearchRes').show();
        $('#loadingGIFFlSearchDet').show();
        $('#tableSearch').hide();
        var BkStatus;
        if (status != "all") {
            BkStatus = status;
        }
        else {
            BkStatus = $('#ddlStatus').val();
        }
        var Datatable = {
            DepartureCity: $('#DepartureCity').val(),
            ArrivalCity: $('#ArrivalCity').val(),
            DepartureDateFrom: $('#DepartureDateFrom').val(),
            DepartureDateTo: $('#DepartureDateTo').val(),
            ArrivalDateFrom: $('#ArrivalDateFrom').val(),
            ArrivalDateTo: $('#ArrivalDateTo').val(),

            BookingDateFrom: $('#BookingDateFrom').val(),
            BookingDateTo: $('#BookingDateTo').val(),
            DeadlineDateFrom: $('#DeadlineDateFrom').val(),
            DeadlineDateTo: $('#DeadlineDateTo').val(),
            BookingRefNo: $('#BookingRefNo').val(),
            SupplierRefNo: $('#SupplierRefNo').val(),
            TicketNo: $('#TicketNo').val(),
            PassengerName: $('#PassengerName').val(),
            ddlBranch: $('#ddlBranch').val(),
            ddlStatus: BkStatus
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/GetSearchData",
            data: JSON.stringify(Datatable),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SearchSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $('#divSearch').show();
    }
}

function SearchSuccess(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divNoData').hide();
        var Searchdata = $.parseJSON(datab.d);
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td data-toggle="tooltip" data-placement="top" title="Agency Name:' + Searchdata[isd].AgencyName + '">'
                + Searchdata[isd].FirstName + '</td><td>' + Searchdata[isd].SupplierBookingReference +
                '</td><td>' + Searchdata[isd].IstPax + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + getAirportNameUTC(Searchdata[isd].DepartureAirportLocationCode) + '">'
                + Searchdata[isd].DepartureAirportLocationCode + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + getAirportNameUTC(Searchdata[isd].ArrivalAirportLocationCode) + '">' + Searchdata[isd].ArrivalAirportLocationCode +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + Searchdata[isd].BokingStatName + '">'
                + Searchdata[isd].BookingStatusCode + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].CancellationDeadline).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].CancellationDeadline).format("DD-MM-YY") + '</td></tr>';
        }
        $("#SearchResult").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableSearch').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableSearch').show();
    }
    else {
        $('#divNoData').show();
    }

    $('#loadingGIFFlSearchDet').hide();
    $('#divSearch').hide();
}



function Search() {
        $('#divSearch').hide();
        $('#divSearchRes').show();
        $('#loadingGIFFlSearchDet').show();
        $('#tableSearch').hide();
        var Datatable = {
            DepartureCity: $('#DepartureCity').val(),
            ArrivalCity: $('#ArrivalCity').val(),
            DepartureDateFrom: $('#DepartureDateFrom').val(),
            DepartureDateTo: $('#DepartureDateTo').val(),
            ArrivalDateFrom: $('#ArrivalDateFrom').val(),
            ArrivalDateTo: $('#ArrivalDateTo').val(),

            BookingDateFrom: $('#BookingDateFrom').val(),
            BookingDateTo: $('#BookingDateTo').val(),
            DeadlineDateFrom: $('#DeadlineDateFrom').val(),
            DeadlineDateTo: $('#DeadlineDateTo').val(),
            BookingRefNo: $('#BookingRefNo').val(),
            SupplierRefNo: $('#SupplierRefNo').val(),
            TicketNo: $('#TicketNo').val(),
            PassengerName: $('#PassengerName').val(),
            ddlBranch: $('#ddlBranch').val(),
            ddlStatus: $('#ddlStatus').val()
        };
        $.ajax({
            type: "POST",
            url: "WebMethodsDB.aspx/GetSearchData",
            data: JSON.stringify(Datatable),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: SearchSuccessfun,
            failure: function (response) {
                alert(response.d);
            }
        });
}
function SearchSuccessfun(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divNoData').hide();
        var Searchdata = $.parseJSON(datab.d);
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td data-toggle="tooltip" data-placement="top" title="Agency Name:' + Searchdata[isd].AgencyName + '">'
                + Searchdata[isd].FirstName + '</td><td>' + Searchdata[isd].SupplierBookingReference +
                '</td><td>' + Searchdata[isd].IstPax + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + getAirportNameUTC(Searchdata[isd].DepartureAirportLocationCode) + '">'
                + Searchdata[isd].DepartureAirportLocationCode + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + getAirportNameUTC(Searchdata[isd].ArrivalAirportLocationCode) + '">' + Searchdata[isd].ArrivalAirportLocationCode +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + Searchdata[isd].BokingStatName + '">'
                + Searchdata[isd].BookingStatusCode + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].CancellationDeadline).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].CancellationDeadline).format("DD-MM-YY") + '</td></tr>';
        }
        $("#SearchResult").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        $('#tableSearch').DataTable({
            "order": [[0, "desc"]]
        });
        $('#tableSearch').show();
    }
    else {
        $('#divNoData').show();
    }

    $('#loadingGIFFlSearchDet').hide();
    $('#divSearch').hide();
}
function SearchDetailsClick(sid) {
    var Datatable = {
        sessionname: 'BookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccess() {
    window.location.href = "/FLIGHT/bookinginfo.html";
}

function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}