﻿var Issuetickets = '';
var RoleReConfirm = parseFloat(0);
var PNR = '';
// Email
var EUName = '';
var ETripID = '';
var EPaxData = '';
var EUserID = '';
var EMailID = '';
var ETripName = '';
var EFlightLeg = '';
var EAirlinesSegAll = '';
var ETotalCharge = '';

var TktEmailPending = parseInt(0);
var ClickFromIssueTktBut = parseInt(0);
var CPNR = '';
var CSurname = '';

var ETktAttachLegs = '';
var ETktAttachMainLeg = '';
var ETktAttachTraveller = '';

var bookerUAgType = '';
var PNRMailSend = 'FALSE';
var PaymentStatus = '';
var RoleDeadLine = parseFloat(0);
var RoleBookingHis = parseFloat(0);
var RoleNetcostbrkup = parseFloat(0);
var PayedTotalPrice = parseFloat(0);
var PaymentCurrency = FlightCurrencyepow;
var CreditLimittAmount = parseFloat(0);
var AgencyTypeIDDD = '';
var RoleEmailVoucher = parseFloat(0);

$(document).ready(function () {
    $("#btnIssueTkt").hide();
    $("#btnCancelPnr").hide();
    SessionCheckingBookingRef('BookingRefID');
    //logout
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
});

//Load
function SessionCheckingBookingRef(session) {
    var Dastasseqq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionRefSuccess,
        failure: function (response) {
            alert(response.d);
            window.location.href = "/FLIGHT/bookinginfo.html";
        }
    });
} //01
function SessionRefSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        SessionChecking('UserDetails');
    }
} //02
function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
} //03
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        RoleChecking('UserRoleStatus');
        console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        EUName = userdata[0].FirstName + ' ' + userdata[0].LastName;
        EUserID = userdata[0].UserID;
        EMailID = userdata[0].EmailID;

        $('.AgencyName').html(userdata[0].AgencyName);
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        AgencyTypeIDDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }
        //Get Markup
        var Dastasseqqq = {
            AgencyID: userdata[0].AgencyIDD,
            ServiceTypeID: '1'
        };
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/GetMarkupData",
            data: JSON.stringify(Dastasseqqq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: MarkupSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
        BindBookingInfo();
    }
} //04
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        console.log(userdata);
        console.log(userdata[0].CreditLimitAmount)
        CreditLimittAmount = parseFloat(userdata[0].CreditLimitAmount);
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No Credit !');
    }
}

//Role
function RoleChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: RoleSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
} //04
function RoleSuccess(datarole) {
    var Roledata = $.parseJSON(datarole.d);
    console.log(Roledata);
    RoleReConfirm = _.find(Roledata, function (item) {
        return item.RoleID == "5";
    }).RoleStatus;

    RoleNetcostbrkup = _.find(Roledata, function (item) {
        return item.RoleID == "22";
    }).RoleStatus;

    RoleBookingHis = _.find(Roledata, function (item) {
        return item.RoleID == "23";
    }).RoleStatus;

    RoleDeadLine = _.find(Roledata, function (item) {
        return item.RoleID == "24";
    }).RoleStatus;

    if (RoleReConfirm == 0) {
        $("#btnIssueTkt").remove();
    }
    else {
        //$("#btnIssueTkt").show();
    }

    RoleEmailVoucher = _.find(Roledata, function (item) {
        return item.RoleID == "9";
    }).RoleStatus;
} //05

//Bind Info From DB

//Bind Booking Info
function BindBookingInfo() {
    $(".loadingGIFFlDet").show();
    $("#deadline").html(' ');
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookingInfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessBookInfo,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                window.location.href = "/FLIGHT/bookinginfo.html";
                //BindBookingInfo();
            }
        }
    });
} //06
function SuccessBookInfo(datab) {
    if (datab.d != "NOINFO") {
        //var BookData = JSON.parse(datab.d);
        var BookData = $.parseJSON(datab.d);
        console.log(BookData);
        var FlightLeg = '';

        var Legs = _(BookData).chain().flatten().pluck('OriginDestinationID').unique().value();
        var DirectionsUniq = _(BookData).chain().flatten().pluck('DirectionID').unique().value();

        var BookingDate = BookData[0].BookingDate;
        var CancelDate = BookData[0].CancellationDeadline;
        $("#deadline").append('Cancellation DeadLine On ' + (moment(CancelDate).format("DD MMM YYYY")));
        $("#TimeLimit").append((moment(CancelDate).format("DD MMM YYYY")) + '<a title="Last ticketing date" id="Lastticketingdate" onclick="Lastticketingdate()"><i class="fa fa-refresh" style="color:#cca789;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a>');
        if (RoleDeadLine == 0) {
            $("#Lastticketingdate").remove();
        }
        if (RoleEmailVoucher == 0) {
            $("#EmailTkta").remove();
        }
        $("#BookedOn").append('Booked on ' + (moment(BookingDate).format("DD MMM YYYY")));
        $("#BookingDate").append((moment(BookingDate).format("DD MMM YYYY")));
        $("#RefernceNo").append(BookData[0].BookingRefID + '');
        $(".bookingID").append('Booked ID ' + AgencyRef + '-' + BookData[0].BookingRefID + '');
        ETripID = '' + AgencyRef + '-' + BookData[0].BookingRefID + '';
        var tripnameflag = 'NO';
        EFlightLeg = '';
        ETktAttachMainLeg = '';
        for (var ib02 = 0; ib02 < DirectionsUniq.length; ib02++) {
            var LegSingleValue = Legs[ib02];
            var LegSingleData = _.where(BookData, { DirectionID: ib02 });
            console.log(LegSingleData)
            if (LegSingleData == "") {
                window.location.href = "/FLIGHT/bookinginfo.html";
            }
            var From = _.first(LegSingleData).DepartureAirportLocationCode;
            var FromDate = _.first(LegSingleData).DepartureDateTime;
            var To = _.last(LegSingleData).ArrivalAirportLocationCode;
            var ToDate = _.last(LegSingleData).ArrivalDateTime;
            var FromTimeSplit = _.first(LegSingleData).DTTimesplit;
            var ToTimeSplit = _.last(LegSingleData).ATTimesplit;
            if (tripnameflag == 'NO') {
                $("#tripName").append(' ' + getCityNameUTC(To) + '');
                ETripName = getCityNameUTC(To);
                tripnameflag = 'YES';
            }

            var ElapTime = LegSingleData[0].ElapsedTime.toString().substr(0, 2) + 'h ' + LegSingleData[0].ElapsedTime.toString().substr(2, 2) + 'm';
            if (LegSingleData.length >= 2) {
                var Stops = parseFloat(LegSingleData.length / 2).toPrecision(1);
                var Stops = Stops + '-stop'
            }
            else {
                var Stops = 'Non-stop'
            }

            var FlightLegSeg = '';
            EAirlinesSegAll = '';
            ETktAttachLegs = '';
            for (var ib03 = 0; ib03 < LegSingleData.length; ib03++) {

                var OperatedByAirline = '';
                if (LegSingleData[ib03].MarketingAirlineCode != LegSingleData[ib03].OperatingAirlineCode) {
                    OperatedByAirline = '<p>(Operated by ' + getAirLineName(LegSingleData[ib03].OperatingAirlineCode) + ')</p>';
                }
                var DepTer = LegSingleData[ib03].DepartureAirportTerminal;
                var ArriTer = LegSingleData[ib03].ArrivalAirportTerminal;

                var DeptTime = LegSingleData[ib03].DepartureDateTime;
                var ArriTime = LegSingleData[ib03].ArrivalDateTime;

                var DeptTime = LegSingleData[ib03].DepartureDateTime;
                var ArriTime = LegSingleData[ib03].ArrivalDateTime;
                var DeptTimeSplit = LegSingleData[ib03].DTTimesplit;
                var ArriTimeSplit = LegSingleData[ib03].ATTimesplit;

                var aireqp = LegSingleData[ib03].EquipmentAirEquipType;
                var ResBookDesigCabinCodee = LegSingleData[ib03].ResBookDesigCabinCode;
                var ResBookDesigCodee = LegSingleData[ib03].ResBookDesigCode;
                //var ResBookDesigCabinCodee=

                if (DepTer == null) {
                    DepTer = '';
                }
                else {
                    DepTer = 'Terminal ' + DepTer;
                }
                if (ArriTer == null) {
                    ArriTer = '';
                }
                else {
                    ArriTer = 'Terminal ' + ArriTer;
                }

                //Layover Time
                var LayoverTimee = '';
                var LayoverPlace = '';
                if (LegSingleData.length - 1 >= ib03 + 1) {
                    var FirstArrivalTime = moment(LegSingleData[ib03].ArrivalDateTime);
                    var NextDeptTime = moment(LegSingleData[ib03 + 1].DepartureDateTime);
                    LayoverPlace = 'at ' + getCityNameUTCOnly(LegSingleData[ib03].ArrivalAirportLocationCode);

                    var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                    var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                    LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                }
                var LayOverDIV = '';
                var ETktAttachLayover = '';
                if (LayoverTimee != '' && LayoverTimee != undefined) {
                    LayOverDIV = '<div class="layover01 text-center"><span class="layover"><i class="fa fa-clock-o" aria-hidden="true"></i>Layover ' + LayoverPlace + ' | Time ' + LayoverTimee + '</span></div>';
                    ETktAttachLayover = '<div style="width: 100%;float: left;font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif;font-size: 13px;color: #2f38a6;line-height: 18px;text-align: center;font-weight: bold;margin-bottom: 10px; position:relative;"> <div style="width:100%; height:1px; position:absolute;margin-top: 9px; border-bottom:1px dashed #0059c0;"></div> <div style="margin: auto;width: 250px;"><span style="width:100%; float:left; background:#FFF; text-align:center; position: relative;">Layover ' + LayoverPlace + ' | Time ' + LayoverTimee + '</span></div> </div>';
                }
                //Layover Time

                var TechStopDiv = '';
                if (LegSingleData[ib03].LoccationCode != null) {
                    TechStopDiv = 'Technical stop at <span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(LegSingleData[ib03].LoccationCode) + '">' + getCityNameUTCOnly(LegSingleData[ib03].LoccationCode) + '</span> - ' + moment(LegSingleData[ib03].DepartureTime).format("HH:mm") + ' to ' + moment(LegSingleData[ib03].ArrivalTime).format("HH:mm") + '';
                }

                FlightLegSeg += '<div class="flight_itinerary extra_padding"> <div class="col-sm-2 col-xs-12 text-left no_padding_left itinerary01"> <img src="/COMMON/images/Airlines/'
                    + LegSingleData[ib03].MarketingAirlineCode + '.gif" class="img_itinerary"> <h1>' + getAirLineName(LegSingleData[ib03].MarketingAirlineCode) + '</h1> '
                    + OperatedByAirline + ' <h1>' + LegSingleData[ib03].MarketingAirlineCode + '-' + LegSingleData[ib03].FlightNumber +
                    '</h1> </div> <div class="col-xs-12 col-sm-7 no_padding_left mrg_btm"> <div class="flight_from left_txt"> <p>'
                    + DeptTimeSplit + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(LegSingleData[ib03].DepartureAirportLocationCode) + '">'
                    + LegSingleData[ib03].DepartureAirportLocationCode + '</span></p> <h4>' + moment(DeptTime).format("DD MMM YYYY") +
                    '</h4> <h5>' + DepTer + '</h5> </div> <div class="flight_from sp08"> <div class="filghtImg02"> <p>'
                    + GetFlightdurationtime(LegSingleData[ib03].DepartureAirportLocationCode, LegSingleData[ib03].ArrivalAirportLocationCode, DeptTime, ArriTime) +
                    '</p> <div class="line_flight01"> <span></span> </div> <p>' + TechStopDiv + '</p> </div> </div> <div class="flight_from txt_right01"> <p>'
                    + ArriTimeSplit + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(LegSingleData[ib03].ArrivalAirportLocationCode) + '">' + LegSingleData[ib03].ArrivalAirportLocationCode + '</span></p> <h4>'
                    + moment(ArriTime).format("DD MMM YYYY") + '</h4> <h5>' + ArriTer + '</h5></div> </div><div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_padding01"> <div class="itinerary_flight_details"> <p><span>Aircraft Type:</span> ' + getAirCraftName(aireqp) + '</p> <p><span>Cabin:</span> ' + getCabinClass(ResBookDesigCabinCodee) + '</p> <p><span>Cabin class:</span> ' + ResBookDesigCodee + '</p> <p><span></span></p> <p></p> </div> </div> </div>' + LayOverDIV + '';

                EAirlinesSegAll += '<div style="width:100%;float:left"> <div style="width:auto;float:left;margin-right:9px"><img src="' + Domainpath + '/COMMON/images/Airlines/' + LegSingleData[ib03].MarketingAirlineCode + '.gif"></div> <h1 style="font-family:&#39;Trebuchet MS&#39;,Arial,Helvetica,sans-serif;font-size:13px;color:#4b4a4a;padding:0px;margin:0px">' + getAirLineName(LegSingleData[ib03].MarketingAirlineCode) + '</h1> <p style="font-family:&#39;Trebuchet MS&#39;,Arial,Helvetica,sans-serif;font-size:12px;color:#4b4a4a;padding:0px;margin:0px">' + LegSingleData[ib03].MarketingAirlineCode + '-' + LegSingleData[ib03].FlightNumber + '</p> </div>';

                ETktAttachLegs += '<div style="width:100%;float:left; padding-bottom: 5px; margin-bottom:10px;"> <div style="width:136px; float:left; text-align:left;"> <h1 style="text-align:left;margin:0px;"><img src="' + Domainpath + '/COMMON/images/Airlines/' + LegSingleData[ib03].MarketingAirlineCode + '.gif"></h1> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                    + getAirLineName(LegSingleData[ib03].MarketingAirlineCode) + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" +
                    ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">' + LegSingleData[ib03].MarketingAirlineCode + '-' + LegSingleData[ib03].FlightNumber +
                    '</h2> </div> <div style="width:107px; float:left; text-align:right;"> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">' + DeptTimeSplit +
                    '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                    + LegSingleData[ib03].DepartureAirportLocationCode + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" +
                    ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">' + moment(DeptTime).format("DD MMM YYYY") + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                    + DepTer + '</h2> </div> <div style="width:150px; float:left; text-align:center;"> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px; padding-top: 26px;">'
                    + GetFlightdurationtime(LegSingleData[ib03].DepartureAirportLocationCode, LegSingleData[ib03].ArrivalAirportLocationCode, DeptTime, ArriTime) +
                    '</h2> </div> <div style="width:107px; float:left; text-align:left;"> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                + ArriTimeSplit + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                + LegSingleData[ib03].ArrivalAirportLocationCode + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" +
                    ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">' + moment(ArriTime).format("DD MMM YYYY") + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">'
                + ArriTer + '</h2> </div> <div style="width:220px; float:left; text-align:left;"> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">Aircraft Type:  '
                + getAirCraftName(aireqp) + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">Cabin: '
                + getCabinClass(ResBookDesigCabinCodee) + '</h2> <h2 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:14px; color:#0059c0; font-weight:normal;margin:0px;">Cabin class: '
                 + ResBookDesigCodee + '</h2> </div> </div>' + ETktAttachLayover + '';
            }
            //FlightLeg += '<div class="flight_sec"> <div class="itinerary_head"> <h2><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getAirportNameUTC(From) + ' to ' + getAirportNameUTC(To) + '</h2><h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getCityNameUTC(From) + ' to ' + getCityNameUTC(To) + '</h4> <h3><i class="fa fa-clock-o" aria-hidden="true"></i>' + ElapTime + ' - ' + Stops + '</h3> </div>' + FlightLegSeg + '</div>';
            FlightLeg += '<div class="flight_sec"> <div class="itinerary_head"> <h2><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getAirportNameUTC(From) + ' to ' + getAirportNameUTC(To) + '<span> (' + Stops + ') </span></h2><h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getCityNameUTC(From) + ' to ' + getCityNameUTC(To) + '</h4></div>' + FlightLegSeg + '</div>';
            EFlightLeg += '<div style="width:100%;float:left;padding-bottom:10px;margin-bottom:10px;border-bottom:1px solid #dfdae6"> ' + EAirlinesSegAll + ' <div style="width:100%;float:left;font-family:' + "'" + 'Trebuchet MS' + "'" + ',Arial,Helvetica,sans-serif;font-size:12px;color:#4b4a4a;padding:0px;margin:0px;margin-top:10px"> <table width="100%" cellpadding="0" cellspacing="0" border="0"> <tbody> <tr> <td style="color:#0059c0;"><strong>' + getCityNameUTC(From) + '</strong> → <strong>' + getCityNameUTC(To) + '</strong></td> <td style="color:#0059c0;"><strong>' + FromTimeSplit + '</strong> − <strong>' + ToTimeSplit + '</strong></td> </tr> <tr> <td style="font-size: 13px;">' + moment(FromDate).format("ddd DD MMM YYYY") + '</td> <td style="font-size: 13px;">' + ElapTime + ',' + Stops + '</td> </tr> </tbody> </table> </div> </div>';
            ETktAttachMainLeg += '<div style="width:100%; float:left; font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:13px; color:#949396;line-height: 18px;"> <h1 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ', Arial, Helvetica, sans-serif; font-size:17px; color:#0059c0; font-weight:normal; padding:0px; margin:0px; margin-bottom: 8px; padding-bottom: 5px;">'
                + getAirportNameUTC(From) + ' to ' + getAirportNameUTC(To) + '<span style="font-size:13px">(' + Stops + ')</span></h1>' + ETktAttachLegs + '</div>';
        }
        $("#divFlightDetails").html(FlightLeg);
        $(".loadingGIFFlDet").hide();
        $('[data-toggle="tooltip"]').tooltip();
    }
    BindPaxData();
    GetBookerAgencyDetails();
} //07

//Bind Pax Data
function BindPaxData() {
    $("#loadingGIFFlPaxDet").show();
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookingPaxInfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessPaxInfo,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                BindPaxData();
            }
        }
    });
} //08
function SuccessPaxInfo(datapax) {
    var BookPaxData = JSON.parse(datapax.d);
    console.log(BookPaxData);
    var PaxData = '';
    EPaxData = '';
    ETktAttachTraveller = '';
    for (var ip01 = 0; ip01 < BookPaxData.length; ip01++) {
        var tktinfo = BookPaxData[ip01].ETicketNo;
        if (tktinfo == '') {
            tktinfo = 'NOT ISSUED';
            $("#Getpnardata").show();
        }
        else {
            $("#Getpnardata").hide();
        }
        PaxData += '<div class="passenger_info01"> <div class="col-lg-4 col-sm-8 col-md-4 col-xs-8 no_padding_left"><label>'
            + parseInt(ip01 + 1) + '. ' + BookPaxData[ip01].Title + ' ' + BookPaxData[ip01].GivenName + ' ' + BookPaxData[ip01].Surname + ' - '
            + BookPaxData[ip01].PaxType + '</label></div> <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 text-left"><label>'
            + BookPaxData[ip01].PNR + '</label></div><div class="col-lg-4 col-sm-4 col-md-4 col-xs-4 text-left no_mobile"><label>'
            + tktinfo.replace(';', '') + '</label></div></div></div>';
        CPNR = BookPaxData[ip01].PNR;
        $("#PNR").html(CPNR);
        CSurname = BookPaxData[ip01].Surname;
        EPaxData += '<div style="width:100%;float:left; margin-bottom: 8px; padding-bottom: 8px; border-bottom: 1px solid #dfdae6;"> <h1 style="font-family:' + "'" + 'Trebuchet MS' + "'" + ',Arial,Helvetica,sans-serif;font-size:13px;color:#4b4a4a;padding:0px;margin:0px">'
            + parseInt(ip01 + 1) + '. ' + BookPaxData[ip01].Title + ' ' + BookPaxData[ip01].GivenName + ' ' + BookPaxData[ip01].Surname +
            '</h1> <p style="font-family:' + "'" + 'Trebuchet MS' + "'" + ',Arial,Helvetica,sans-serif;font-size:12px;color:#4b4a4a;padding:0px;margin:0px">PNR: '
            + BookPaxData[ip01].PNR + '</p><p style="font-family:' + "'" + 'Trebuchet MS' + "'" + ',Arial,Helvetica,sans-serif;font-size:12px;color:#4b4a4a;padding:0px;margin:0px">TICKET NO: '
            + tktinfo.replace(';', '') + '</p> </div>';

        ETktAttachTraveller += '<tr> <td>' + BookPaxData[ip01].Title + ' ' + BookPaxData[ip01].GivenName + ' ' + BookPaxData[ip01].Surname +
            '</td> <td style="text-align:center;">' + BookPaxData[ip01].PNR + '</td> <td style="text-align:center;">' + tktinfo.replace(';', '') + '</td> </tr>';
    }
    $("#divPaxData").html(PaxData);

    if (TktEmailPending == 1) {
        SendMailTicketIssue('TKT');
        TktEmailPending = 0;
    }

    $("#loadingGIFFlPaxDet").hide();
    BindTransData();
} //09

//Bind Trans
function BindTransData() {
    $("#loadingGIFFlTransDet").show();
    GetBookerAgencyDetails();

} //10
function SuccessTransInfo(datatrans) {
    var BookTransData = JSON.parse(datatrans.d);
    console.log(BookTransData);
    if (BookTransData[0].BookingStatusCode == "OK") {
        $("#btnIssueTkt").hide();
        $("#btnCancelPnr").hide();
        $("#btnCancelEmail").show();
        $("#Lastticketingdate").hide();
        $("#deadline").hide();
    }
    else if (BookTransData[0].BookingStatusCode == "XR") {
        $("#btnIssueTkt").hide();
        $("#btnCancelPnr").hide();
        $("#btnCancelEmail").hide();
        $("#Lastticketingdate").hide();
        $("#deadline").hide();
    }
    else if (BookTransData[0].BookingStatusCode == "HK") {
        $("#btnIssueTkt").show();
        $("#btnCancelPnr").show();
        $("#btnCancelEmail").hide();
        $("#Lastticketingdate").show();
        $("#deadline").show();
    }
    $("#bookStat").html('STATUS - ' + BookTransData[0].BookingStatus);
    $("#Status").html((BookTransData[0].BookingStatus) + '<a title="Booking history" id="aBookHistory" onclick="BookingHistory()" data-toggle="modal" data-target="#modalBookHistory"><i class="fa fa-plus-square" style="color :#cca789;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a>');
    if (RoleBookingHis == 0) {
        $("#aBookHistory").remove();
    }
    //var PriceMarkRound = Math.round(parseFloat(BookTransData[0].SellAmount));
    var AdditionalServiceFee = parseFloat(BookTransData[0].AdditionalServiceFee);
    var PriceMarkRound = parseFloat(BookTransData[0].SellAmount) + AdditionalServiceFee;
    PayedTotalPrice = Math.round(PriceMarkRound * 100) / 100;
    PaymentCurrency = BookTransData[0].SellCurrency;
    $("#divTotal").html(BookTransData[0].SellCurrency + ' ' + Math.round(PriceMarkRound * 100) / 100);
    $("#Total").html((BookTransData[0].SellCurrency + ' ' + Math.round(PriceMarkRound * 100) / 100) + '<a><i id="ifbreakup" data-open="0" onclick="GetFareBreak()" class="fa fa-plus-square" data-toggle="modal" data-target="#fare_breakup" style="color :#cca789;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a>');

    if (RoleNetcostbrkup == 0) {
        $("#ifbreakup").remove();
    }
    ETotalCharge = BookTransData[0].SellCurrency + ' ' + PriceMarkRound;
    $("#divPayStat").html(BookTransData[0].PaymentStatus);
    PaymentStatus = BookTransData[0].PaymentStatus;
    $("#divTotNet").html(BookTransData[0].SellCurrency + ' ' + BookTransData[0].TotalBaseNet);
    var TotalTaxxe = parseFloat(parseFloat(BookTransData[0].GrandTotalTax) + AdditionalServiceFee);
    $("#divTotTax").html(BookTransData[0].SellCurrency + ' ' + Math.round(TotalTaxxe * 100) / 100);
    if (bookerUAgType == '4') {
        $("#divConvFeee").show();
        $("#divConvFee").html(BookTransData[0].SellCurrency + ' ' + BookTransData[0].MarkupValue);
    }
    else {
        $("#divConvFeee").hide();
    }
    $("#loadingGIFFlTransDet").hide();
    if (ClickFromIssueTktBut == 0) {
        BindBaggageData();
    }


} //11

//Bind Baggage
function BindBaggageData() {
    $("#loadingGIFFlBagDet").show();
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookingBaggageInfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessBaggInfo,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //BindBaggageData();
            }
        }
    });
} //12
function SuccessBaggInfo(databag) {
    //if (databag.d != "NOINFO") {
    //    var BookBagData = JSON.parse(databag.d);
    //    console.log(BookBagData);
    //    var BagInfo = '';
    //    for (var ibag = 0; ibag < BookBagData.length; ibag++) {
    //        BagInfo += '<div class="baggage_details_area"> <div class="itinerary_head"> <h2><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getAirportNameUTC(BookBagData[ibag].FromSeg) + ' to ' + getAirportNameUTC(BookBagData[ibag].ToSeg) + ' - ' + getAirLineName(BookBagData[ibag].MarketingAirlineCode) + '</h2><h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + BookBagData[ibag].FromSeg + ' to ' + BookBagData[ibag].ToSeg + ' - ' + getAirLineName(BookBagData[ibag].MarketingAirlineCode) + '</h4>  </div> <table width="20%;" style="min-width:250px;" border="0" cellpadding="0" cellspacing="0"> <tr> <th></th> <th>CHECK IN</th> <th>CABIN</th> </tr> ' +
    //            '<tr><td>' + BookBagData[ibag].PAXType + '</td><td>' + BookBagData[ibag].CheckinBaggageQuantity + ' ' + BookBagData[ibag].CheckinBaggageUnit + '</td><td>7 KG</td></tr>' +
    //            '</tr> </table> </div>';
    //    }
    //    $("#divBagInfoNew").html(BagInfo);
    //    $("#loadingGIFFlBagDet").hide();
    //}
    if (databag.d != "NOINFO") {
        var BookBagData = JSON.parse(databag.d);
        console.log(BookBagData);
        var BagInfo = '';
        var currentairprt = '';
        var firstiteray = '0';
        for (var ibag = 0; ibag < BookBagData.length; ibag++) {
            if (currentairprt == getAirportNameUTC(BookBagData[ibag].FromSeg)) {
                BagInfo += '<tr><td>' + BookBagData[ibag].PAXType + '</td><td>' + BookBagData[ibag].CheckinBaggageQuantity + ' ' + BookBagData[ibag].CheckinBaggageUnit + '</td><td>7 KG</td></tr>' + '';
            }
            else {
                if (firstiteray = '1') {
                    BagInfo += '</tr> </table> </div>';
                }
                firstiteray = '1';
                currentairprt = getAirportNameUTC(BookBagData[ibag].FromSeg);
                BagInfo += '<div class="baggage_details_area"> <div class="itinerary_head"> <h2><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + getAirportNameUTC(BookBagData[ibag].FromSeg) + ' to ' + getAirportNameUTC(BookBagData[ibag].ToSeg) + ' - ' + getAirLineName(BookBagData[ibag].MarketingAirlineCode) + '</h2><h4><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + BookBagData[ibag].FromSeg + ' to ' + BookBagData[ibag].ToSeg + ' - ' + getAirLineName(BookBagData[ibag].MarketingAirlineCode) + '</h4>  </div> <table width="20%;" style="width:100%;border: 1px solid #d6d6d6;" border="0" cellpadding="0" cellspacing="0"> <tr> <th>PAX</th> <th>CHECK IN</th> <th>CABIN</th> </tr> ' +
                    '<tr><td><i class="fa fa-child" aria-hidden="true"></i>' + BookBagData[ibag].PAXType + '</td><td><i class="fa fa-suitcase" aria-hidden="true"></i>' + BookBagData[ibag].CheckinBaggageQuantity + ' ' + BookBagData[ibag].CheckinBaggageUnit + '</td><td><i class="fa fa-suitcase" aria-hidden="true"></i> 7 KG</td></tr>' + '';
                //'</tr> </table> </div>';
            }
        }
        BagInfo += '</tr> </table> </div>';
        $("#divBagInfoNew").html(BagInfo);
        $("#loadingGIFFlBagDet").hide();
    }
    else {
        $("#divBagInfoNew").html('<div class="queries_sec"><h2>NO BAGGAGE DATA AVAILABLE !</h2></div>');
        $("#loadingGIFFlBagDet").hide();
    }
    AutoTicketting('AutoTicketStat');
} //13

function AutoTicketting(sessiont) {
    var Dastasseq = {
        sesname: sessiont
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccessAutoTkt,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccessAutoTkt(dataaut) {
    console.log(dataaut.d);
    if (dataaut.d == '1') {
        if (PayedTotalPrice > CreditLimittAmount && (AgencyTypeIDDD != '1' && AgencyTypeIDDD != '2')) {

        }
        else {
            IssueTktDB();
        }
    }
    else if (dataaut.d == '0') {
        if (PNRMailSend == 'FALSE') {
            SendMailTicketIssue('PNR');
            PNRMailSend = 'TRUE';
        }
    }

    var Dastasseqww = {
        sessionname: 'AutoTicketStat',
        valueses: 'NOSESSION'
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Dastasseqww),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        }
    });
}

//Issue Tkt cLICK
function IssueTktClick() {
    if (RoleReConfirm == 0) {
        alertify.alert('Permission', 'You are not authorized to issue the ticket!');
    }
    else {
        if (PayedTotalPrice > CreditLimittAmount && (AgencyTypeIDDD != '1' && AgencyTypeIDDD != '2')) {
            $("#btnIssueTkt").attr("disabled", true);
            alertify.alert('Credit balance', 'Please note ! Not enough credit balance to proceed with this booking.');
        }
        else {
            $("#btnIssueTkt").attr("disabled", true);
            //tktBind();
            ClickFromIssueTktBut = 1;
            IssueTktDB();
        }
    }
}

//From DB
function IssueTktDB() {
    $("#loaderIssueTkt").show();
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetIssueTktDataDB",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessIssueTktDB,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //IssueTktDB();
            }
        }
    });
}
function SuccessIssueTktDB(datatkt) {
    var IssueData = JSON.parse(datatkt.d);
    console.log('issue tkt data');
    console.log(IssueData);
    var sessionidd = '';
    var PNR = IssueData[0].SupplierBookingReference;
    if (PNR != '' || PNR != null) {
        var passcount = IssueData.length;
        var passtype = '';
        var passdata = '';
        var fname = '';
        var nameprefix = '';
        var surname = '';
        var birthdat = '';
        var passport = '';
        var passexpire = '';
        var issuedate = '';

        for (var iitkt = 0; iitkt < IssueData.length; iitkt++) {
            if (passtype == '') {
                passtype += GetPaxTypeID(IssueData[iitkt].PaxTypeID);
            }
            else {
                passtype += ',' + GetPaxTypeID(IssueData[iitkt].PaxTypeID);
            }

            if (fname == '') {
                fname += IssueData[iitkt].GivenName;
            }
            else {
                fname += ',' + IssueData[iitkt].GivenName;
            }

            if (nameprefix == '') {
                nameprefix += IssueData[iitkt].TitleID;
            }
            else {
                nameprefix += ',' + IssueData[iitkt].TitleID;
            }

            if (surname == '') {
                surname += IssueData[iitkt].Surname;
            }
            else {
                surname += ',' + IssueData[iitkt].Surname;
            }

            if (birthdat == '') {
                birthdat += moment(IssueData[iitkt].DateofBirth).format("YYYY-MM-DD");
            }
            else {
                birthdat += ',' + moment(IssueData[iitkt].DateofBirth).format("YYYY-MM-DD");
            }

            if (passport == '') {
                passport += IssueData[iitkt].DocumentNumber;
            }
            else {
                passport += ',' + IssueData[iitkt].DocumentNumber;
            }

            if (passexpire == '') {
                passexpire += moment(IssueData[iitkt].ExpiryDate).format("YYYY-MM-DD");
            }
            else {
                passexpire += ',' + moment(IssueData[iitkt].ExpiryDate).format("YYYY-MM-DD");
            }

            if (issuedate == '') {
                issuedate += IssueData[iitkt].IssueCountry;
            }
            else {
                issuedate += ',' + IssueData[iitkt].IssueCountry;
            }
        }
        var Dastas1 = {
            PN: PNR,
            passcoun: passcount,
            passtyp: passtype,
            passdat: passdata,
            fnam: fname,
            nameprefi: nameprefix,
            surnam: surname,
            birthdate: birthdat,
            passpo: passport,
            passexpir: passexpire,
            issuedat: issuedate,
            session: sessionidd
        }
        $.ajax({
            type: "POST",
            url: "Webservice.aspx/Createticket",
            data: JSON.stringify(Dastas1),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: populateticketinfoo,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        alertify.alert('PNR', 'No PNR !');
    }
}

function populateticketinfoo(data) {
    var xmlticket = data.d;
    //console.log(xmlticket);
    var x2jss1 = new X2JS();
    var jsonticket = x2jss1.xml_str2json(xmlticket);
    console.log(jsonticket);
    var errorticket = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.Errors;
    var Warningticket = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.Warnings;
    if (errorticket != null || errorticket != undefined) {
        alertify.alert('API', 'Internal API Error');
    }
    else if (Warningticket != null || Warningticket != undefined) {
        alertify.alert('ETICKET_ERROR', 'E-Ticket couldnot be issued due to technical difficulties.');
    }
    else {
        var jsonngentickt = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.AirReservation.TravelerInfo;
        //console.log(jsonngentickt);
        var TicketNumber = '';
        var PersonName = '';
        var surname = '';
        var BirthDate = '';
        var jsontikt = jsonngentickt.AirTraveler;
        console.log(jsontikt);

        if (jsontikt.length != null) {
            for (var V = 0; V < jsontikt.length; V++) {
                if (V == 0) {
                    TicketNumber = jsontikt[V]._eTicketNumber;
                    PersonName = jsontikt[V].PersonName.GivenName;
                    surname = jsontikt[V].PersonName.Surname;
                    BirthDate = jsontikt[V].BirthDate;
                }
                else {
                    TicketNumber += ',' + jsontikt[V]._eTicketNumber;
                    PersonName += ',' + jsontikt[V].PersonName.GivenName;
                    surname += ',' + jsontikt[V].PersonName.Surname;
                    BirthDate += ',' + jsontikt[V].BirthDate;
                }
                //updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
            }
        }
        else {
            TicketNumber = jsontikt._eTicketNumber;
            PersonName = jsontikt.PersonName.GivenName;
            surname = jsontikt.PersonName.Surname;
            BirthDate = jsontikt.BirthDate;
            //updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
        }
        ClearVoucherDataDB();
        updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
        updateBookStatus('OK');
        InsertUpdatePayment();
        alertify.alert('Ticket', 'Ticket generated successfully');
    }
    $("#loaderIssueTkt").hide();
}
//Ticket End

//Update Ticket Number
function updateTKTNum(tktnum, PersonName, surname, BirthDate) {
    var Dastas1 = {
        TicketNumber: tktnum,
        PersonName: PersonName,
        surname: surname,
        BirthDate: BirthDate
    }
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/UpdateTkt",
        data: JSON.stringify(Dastas1),
        success: sucupdateTKTNum,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        }
    });
}
function sucupdateTKTNum() {
    TktEmailPending = 1;
    BindPaxData();
}
//Update Book Status
function updateBookStatus(status) {
    var Dastasbst = {
        bstat: status
    }
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/UpdateBookStatus",
        data: JSON.stringify(Dastasbst),
        success: sucupdateBookStatus,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        }
    });
}
function sucupdateBookStatus() {
    BindTransData();
}
//Update Ticket Number End

//Get Full Airport Name
function getAirportName(airportcode) {
    var AirportName = _.where(Airport_AllList, { a: airportcode, T: 'A' });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].label;
    }
    return AirportName;
}
function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}
function getCityNameUTCOnly(airportcode) {
    var CityName = _.where(AirlinesTimezone, { I: airportcode });
    if (CityName == "") {
        CityName = airportcode;
    }
    else {
        CityName = CityName[0].C;
    }
    return CityName;
}
function getCityNameUTC(airportcode) {
    var CityName = _.where(AirlinesTimezone, { I: airportcode });
    if (CityName == "") {
        CityName = airportcode;
    }
    else {
        CityName = CityName[0].C + ' (' + airportcode + ')';
    }
    return CityName;
}
//Time Zone
function getAirLineTimeZone(airlinecode) {
    var AirLineTimeZone = _.where(AirlinesTimezone, { I: airlinecode });
    if (AirLineTimeZone == "") {
        AirLineTimeZone = "--h:--m";
    }
    else {
        AirLineTimeZone = AirLineTimeZone[0].TZ;
    }
    return AirLineTimeZone;
}
function CreateLegTitleissueticket(fromleg, toleg, Stops) {

    LegTitle = '<div class="itinerary_head"> <h2><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>' + fromleg + ' to ' + toleg + '</h2> <h3><i class="fa fa-clock-o" aria-hidden="true"></i><span>(' + Stops + ')</span></h3></div>';

}
function showticketdetails(OperatingAirline, AirlineCode, FlightNum, FromSeg, DeptTimeSeg, ToSeg, ArriTimeSeg, DepTer, ArriTer, LayOverTimess, DTime) {

    if (DepTer == null) {
        DepTer = '';
    }
    else {
        DepTer = 'Terminal ' + DepTer;
    }
    if (ArriTer == null) {
        ArriTer = '';
    }
    else {
        ArriTer = 'Terminal ' + ArriTer;
    }

    var LayOverDIV = '';
    if (LayOverTimess != "" && LayOverTimess != undefined) {
        LayOverDIV = '<div class="layover01 text-center"><span class="layover"><i class="fa fa-clock-o" aria-hidden="true"></i>' + LayOverTimess + '</span></div';

    }
    else {
        LayOverDIV = '';

    }

    Issuetickets += '' + LegTitle + '<div class="flight_itinerary extra_padding"> <div class="col-sm-3 col-xs-12 text-left no_padding_left itinerary01"> <img src="/images/Airlines/'
          + AirlineCode + '.gif" class="img_itinerary"> <p>' + getAirLineName(AirlineCode) + '</p> <p>(Operated by' + " " + getAirLineName(OperatingAirline) + ')</p> <p>' + AirlineCode + '-' + FlightNum + '</p> </div> <div class="col-xs-12 col-sm-8 no_padding_left mrg_btm"> <div class="flight_from left_txt"> <p>' + DeptTimeSeg.split(",")[1] + '</p> <p><span>' + FromSeg + '</span></p> <h4>' + DeptTimeSeg.split(",")[0] + '</h4> <h5>' + DepTer + '</h5> </div><div class="flight_from sp08"> <div class="filghtImg02"> <p>' + DTime + '</p> <div class="line_flight01"> <span></span> </div> <p>   </p> </div> </div> <div class="flight_from txt_right01"> <p>' + ArriTimeSeg.split(",")[1] + '</p> <p><span>' + ToSeg + '</span></p> <h4>' + ArriTimeSeg.split(",")[0] + '</h4> </div> </div> </div> </div> </div>' + LayOverDIV + '';

    LegTitle = '';
}
//Get Full Airline Name
function getAirLineName(airlinecode) {
    var AirLineName = _.where(AirlinesDatas, { C: airlinecode });
    if (AirLineName == "") {
        AirLineName = airlinecode;
    }
    else {
        AirLineName = AirLineName[0].A;
    }
    return AirLineName;
}
//Get PAX Type//

function GetNamePassengerTYpe(PAXType) {
    var Pax = "";
    if (PAXType == "ADT") {
        Pax = "Adult";
    }
    else if (PAXType == "CHD") {
        Pax = "Child";
    }
    else if (PAXType == "INF") {
        Pax = "Infant";
    }
    return Pax;
}

function GetPaxTypeID(PAXID) {
    var Pax = "";
    if (PAXID == "1") {
        Pax = "ADT";
    }
    else if (PAXID == "2") {
        Pax = "CHD";
    }
    else if (PAXID == "3") {
        Pax = "INF";
    }
    return Pax;
}
//Get PAX Type//

//Get Flight duration time
function GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo) {
    var DepartureAirlineTimezone = getAirLineTimeZone(FromGo);
    var ArrivalAirlineTimezone = getAirLineTimeZone(ToGo);
    var DurationTimezone = DepartureAirlineTimezone - ArrivalAirlineTimezone;
    var DurationTimezoneMinutes = parseFloat(DurationTimezone) * 60;
    DurationTimezoneMilliseconds = parseFloat(DurationTimezoneMinutes) * 60000;
    var depariduration = moment(ArriTimeGo) - moment(DepTimeGo);
    var GrandDurationTimezone = depariduration + DurationTimezoneMilliseconds;

    GrandDurationTimezone = GrandDurationTimezone / 60000;

    var temphours = 0;
    var tempminutes = 0;
    var DTime = '';
    if (GrandDurationTimezone > 59) {
        temphours = Math.floor(GrandDurationTimezone / 60);
        tempminutes = GrandDurationTimezone - (temphours * 60);
        if (tempminutes == 0) {
            DTime = temphours + 'h ';
        }
        else {
            DTime = temphours + 'h ' + tempminutes + 'm';
        }

    }
    else {
        tempminutes = GrandDurationTimezone;
        DTime = tempminutes + 'm';
    }
    if (DTime.indexOf("N") > -1) {
        DTime = "";
    }

    return DTime;
}

//Send Mail
function SendMailTicketIssue(Type) {
    if (Type == 'TKTTOMAIL') {
        EMailID = $("#txtTktToMail").val();
        EUName = EMailID.substring(0, EMailID.lastIndexOf("@"));
    }
    var Dastasseq = {
        EUName: EUName,
        ETripID: ETripID,
        EPaxData: EPaxData,
        UID: EUserID,
        UserMailID: EMailID,
        ETripName: ETripName,
        EFlightLeg: EFlightLeg,
        ETotalCharge: '<div style="width:48%;float:right;border:1px solid #dfdae6"><div style="width:100%;float:left;margin-bottom:8px;padding:10px 0px"><div style="font-family:&#39;Trebuchet MS&#39;,Arial,Helvetica,sans-serif;font-size:12px;color:#cca75b;text-align:center">TOTAL CHARGE</div><div style="font-family:&#39;Trebuchet MS&#39;,Arial,Helvetica,sans-serif;font-size:22px;color:#0059c0;text-align:center;font-weight:bold">' + ETotalCharge + '</div></div></div>',
        Type: Type,
        ETktAttachMainLeg: ETktAttachMainLeg,
        ETktAttachTraveller: ETktAttachTraveller,
        ETktAttachFareBreakup: '<tr> <td width="153" style="font-weight:bold;">FARE BREAKUP</td> <td width="171"></td> </tr>  <tr> <td>Total fare:</td> <td>' + $("#divTotal").html() + '</td> </tr>',
        //ETktAttachFareBreakup: '<tr> <td width="153" style="font-weight:bold;">FARE BREAKUP</td> <td width="171"></td> </tr> <tr> <td>Base fare:</td> <td>' + $("#divTotNet").html() + '</td> </tr> <tr> <td>Taxes and fees:</td> <td>' + $("#divTotTax").html() + '</td> </tr> <tr> <td>Total fare:</td> <td>' + $("#divTotal").html() + '</td> </tr>',
        ETktBaggageInfo: $("#divBagInfoNew").html(),
        DeadlineDiv: $("#TimeLimit").text()
    };
    console.log(Dastasseq)
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/SendMailTicketIssue",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SendMailTicketIssueSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                //$('#loginbt').click();
                //EmailTktTo();
                alert('500');
            }
        }
    });
}
function SendMailTicketIssueSuccess(datmtkt) {
    console.log('Mail-' + datmtkt.d);
    if (datmtkt.d == "1") {
    }
}

function EmailTkt() {
    SendMailTicketIssue('TKT');
}
function EmailTktTo() {
    //ClearVoucherDataDB();
    if ($("#txtTktToMail").val() != '') {
        SendMailTicketIssue('TKTTOMAIL');
        alertify.alert('Mail', 'Your mail has been sent');
    }
    else {
        alertify.alert('Mail', 'Please enter email address !');
    }
}

//Get Cabin Class
function getCabinClass(cabin) {
    var Out = "";
    var Economy = _.contains(['B', 'H', 'K', 'L', 'M', 'N', 'Q', 'S', 'T', 'V', 'X', 'Y'], cabin);
    var PremiumEconomy = _.contains(['W', 'E'], cabin);
    var Business = _.contains(['C', 'D', 'J', 'Z'], cabin);
    var First = _.contains(['A', 'F', 'P', 'R'], cabin);

    if (Economy == true) {
        Out = "Economy";
    }
    else if (PremiumEconomy == true) {
        Out = "Premium Economy";
    }
    else if (Business == true) {
        Out = "Business";
    }
    else if (First == true) {
        Out = "First";
    }
    return Out;
}

//Get AirCraft Name
function getAirCraftName(aireqpcode) {
    var AirCraftName = _.where(AirplanesData, { C: aireqpcode });
    if (AirCraftName == "") {
        AirCraftName = aireqpcode;
    }
    else {
        AirCraftName = AirCraftName[0].N;
    }
    return AirCraftName;
}

//Booking history
function BookingHistory() {
    $("#loaderBookHistory").show();
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookingHistory",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: BookingHistorySuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function BookingHistorySuccess(datahis) {
    if (datahis.d != 'NOINFO') {
        var datahisd = $.parseJSON(datahis.d);
        console.log(datahisd);
        var bookhistappend = '';
        for (var ibh = 0; ibh < datahisd.length; ibh++) {
            bookhistappend += '<li class="list-group-item"> <span class="badge">' + moment(datahisd[ibh].ActionDate).format("ddd, Do MMM YYYY - hh:mm:ss a") + '</span><b>' + datahisd[ibh].BookingStatus + '</b> - ' + datahisd[ibh].FirstName + ' ' + datahisd[ibh].LastName + '</li>';
        }
    }
    else {
        bookhistappend += '<li class="list-group-item">No history available !</li>';
    }
    $("#ulBookHistory").html(bookhistappend);
    $("#loaderBookHistory").hide();
}

//Cancel Request
function CancelBooking() {
    if (CSurname != '' && CPNR != '') {
        var paras = { PNRC: CPNR, SURNAMEC: CSurname };
        $.ajax({
            type: "POST",
            url: "Webservice.aspx/Cancel",
            data: JSON.stringify(paras),
            contentType: "application/json; charset=utf-8",
            success: CancelBookingSuccess,
            failure: function (response) {
                alert(response.d);
            },
            dataType: "json"
        });
    }
}
function CancelBookingSuccess(datcancel) {
    var xmlcan = datcancel.d;
    var x2jss1 = new X2JS();
    var datcancels = x2jss1.xml_str2json(xmlcan);
    console.log(datcancels);
    var Status = datcancels.Envelope.Body.CancelResponse.OTA_CancelRS._Status;
    if (Status == "Pending") {
        updateBookStatus('XR');
    }
    console.log(Status);
    alertify.alert('Cancel', 'PNR cancel status : ' + Status);
}

//Cancel Booking mail
function CancelBookingMail() {
    var parass = {
        PNRC: CPNR,
        ETripIDD: ETripID,
        EUNamee: EUName,
        ECommentt: $("#txtCancelComment").val()
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/CancelBookingMail",
        data: JSON.stringify(parass),
        contentType: "application/json; charset=utf-8",
        success: CancelBookingMailSuccess,
        failure: function (response) {
            alert(response.d);
        },
        dataType: "json"
    });
}
function CancelBookingMailSuccess() {
    InsertBookHistory('XM');
}

function InsertBookHistory(statx) {
    var Dastasbstx = {
        bstat: statx
    }
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/InsertBookHistory",
        data: JSON.stringify(Dastasbstx),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        }
    });
}

//Change travel date mail
function ChangeTravelDateMail() {
    var parass = {
        PNRC: CPNR,
        ETripIDD: ETripID,
        EUNamee: EUName,
        ECommentt: $("#txtChangeTravelDateComment").val()
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/ChangeTravelDateMail",
        data: JSON.stringify(parass),
        contentType: "application/json; charset=utf-8",
        success: ChangeTravelDateMailSuccess,
        failure: function (response) {
            alert(response.d);
        },
        dataType: "json"
    });
}
function ChangeTravelDateMailSuccess() {

}

//FAre Rules
function CheckFareRules() {
    var datafare = $('#divflightrules').attr("data-open");
    if (datafare == 0) {
        //$("#divflightrules").show();
        $('#loadingmessageFlightRules').show();
        $('#divflightrules').attr("data-open", 1);
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/GetFareRules",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: CheckFareRulesSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        //$("#divflightrules").show();
        $('#loadingmessageFlightRules').hide();
    }
}
function CheckFareRulesSuccess(datafare) {
    if (datafare.d != 'NOINFO') {
        var faredata = $.parseJSON(datafare.d);
        console.log(faredata);
        var farerulesdata = '';
        for (var ifa = 0; ifa < faredata.length; ifa++) {
            //farerulesdata += '</br><b>' + faredata[ifa].Segment + '</b></br>';
            //farerulesdata += '<p class="fare_rule_style">' + faredata[ifa].FareRule + '</p>';
            var arr = faredata[ifa].Segment;
            arr = arr.split('-');
            farerulesdata += '<p><div class="itinerary_head"><h2><i class="fa fa-plane summary_flight"></i>' + getAirportNameUTC(arr[0]) + " to " + getAirportNameUTC(arr[1]) + '</h2><h4><i class="fa fa-plane summary_flight"></i>' + getCityNameUTC(arr[0]) + " to " + getCityNameUTC(arr[1]) + '</h4></div></p>';

            if (faredata[ifa].FareRule == "") {
                farerulesdata += '<h4 style="font-size: 20px;margin: 0;line-height: 1.42857;font-weight: 400;display: block;-webkit-margin-before: 1.33em;-webkit-margin-after: 1.33em;-webkit-margin-start: 0px;-webkit-margin-end: 0px;"></h4><p style="font-size: 14px;line-height: 1.42857;color: #0059c0;padding: 0 0px;display: block;font-family: monospace, sans-serif;">Farerules information not available</p>';
            }
            else {
                farerulesdata += '<h4 style="font-size: 20px;margin: 0;line-height: 1.42857;font-weight: 400;display: block;-webkit-margin-before: 1.33em;-webkit-margin-after: 1.33em;-webkit-margin-start: 0px;-webkit-margin-end: 0px;"></h4><p style="font-size: 14px;line-height: 1.42857;color: #0059c0;padding: 0 0px;display: block;font-family: monospace, sans-serif;">' + faredata[ifa].FareRule + '</p>';
            }
        }
        $('#divflightrules').html(farerulesdata);
    }
    else {
        $('#divflightrules').html('<b>Farerules information not available</b>');
    }
    $('#loadingmessageFlightRules').hide();
}

//Get fare break up
function GetFareBreak() {
    var dataopenfb = $("#ifbreakup").attr("data-open");
    if (dataopenfb == 0) {
        $('#loadingGIFFareBr').show();
        $('#ifbreakup').attr("data-open", 1);
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/GetFareBreak",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: GetFareBreakSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
function GetFareBreakSuccess(datafb) {
    if (datafb.d != 'NOINFO') {
        var farebdata = $.parseJSON(datafb.d);
        console.log(farebdata);
        var trBaseFare = '<tr> <td class="fare_head_area">Base Fare</td> <td>&nbsp;</td> </tr>';
        var trSurCharge = '<tr> <td class="fare_head_area">Surcharges</td> <td>&nbsp;</td> </tr>';
        var trPaymentstatus = '<tr> <td class="fare_head_area">Payment Status</td> <td>&nbsp;</td> </tr>';
        var trTotal = '';
        if ($("#divConvFee").html() != '' && $("#divConvFee").html() != undefined) {
            trTotal += '<tr> <td class="fare_head_area">Convenience Fee:</td> <td class="fare_head_area01">' + $("#divConvFee").html() + '</td> </tr>';
        }
        trTotal += '<tr> <td class="fare_head_area02">Total Amount:</td> <td class="fare_head_area02">' + $("#divTotal").html() + '</td> </tr>';
        for (var ifb = 0; ifb < farebdata.length; ifb++) {
            trBaseFare += '<tr><td class="fare_head_area01">' + farebdata[ifb].PaxType + '(s) (' + farebdata[ifb].NoOfPAx + 'x' + Math.round(farebdata[ifb].BaseNet * 100) / 100 + ')</td> <td class="fare_head_area01">' + PaymentCurrency + ' ' + Math.round(parseFloat(farebdata[ifb].NoOfPAx) * parseFloat(farebdata[ifb].BaseNet) * 100) / 100 + '</td></tr>';
            trSurCharge += '<tr><td class="fare_head_area01">' + farebdata[ifb].PaxType + '(s) (' + farebdata[ifb].NoOfPAx + 'x' + Math.round(parseFloat(parseFloat(farebdata[ifb].TaxNet) + parseFloat(farebdata[ifb].MarkupValue) + parseFloat(farebdata[ifb].AdditionalServiceFee)) * 100) / 100 + ')</td> <td class="fare_head_area01">' + PaymentCurrency + ' ' + Math.round(parseFloat(farebdata[ifb].NoOfPAx) * (parseFloat(farebdata[ifb].TaxNet) + parseFloat(farebdata[ifb].MarkupValue) + parseFloat(farebdata[ifb].AdditionalServiceFee)) * 100) / 100 + '</td></tr>';
        }
        trPaymentstatus += '<tr><td class="fare_head_area01">' + PaymentStatus + '</td><td></td></tr>';
        $("#tableFareBreakUps").html(trBaseFare + trSurCharge + trPaymentstatus + trTotal);
    }
    $('#loadingGIFFareBr').hide();
}

function GetBookerAgencyDetails() {
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookerAgencyDetails",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetBookerAgencyDetailsSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function GetBookerAgencyDetailsSuccess(databooker) {
    if (databooker.d != 'NOINFO') {
        var bookerdata = $.parseJSON(databooker.d);
        console.log(bookerdata);
        bookerUAgType = bookerdata[0].AgencyTypeID;
    }
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetBookingTransInfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessTransInfo,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                //window.location.href = "/index.html";
                BindTransData();
            }
        }
    });
}
//TKT
function tktBind() {
    var sessionidd = '';
    var PNR = window.localStorage.getItem('hdpnr');
    var passcount = window.localStorage.getItem('hdPassCount');
    var passtype = window.localStorage.getItem('hdPassType');
    var passdata = window.localStorage.getItem('hdPassQuantity');
    var fname = window.localStorage.getItem('hdfrisnamarry');
    var nameprefix = window.localStorage.getItem('hdnameprefixarry');
    var surname = window.localStorage.getItem('hdsurnmearry');
    var birthdat = window.localStorage.getItem('hdbirthdatearry');
    var passport = window.localStorage.getItem('hdpassportarry');
    var passexpire = window.localStorage.getItem('hdexpiredatearry');
    var issuedate = window.localStorage.getItem('hdissuecuntryarry');

    var Dastas1 = {
        PN: PNR,
        passcoun: passcount,
        passtyp: passtype,
        passdat: passdata,
        fnam: fname,
        nameprefi: nameprefix,
        surnam: surname,
        birthdate: birthdat,
        passpo: passport,
        passexpir: passexpire,
        issuedat: issuedate,
        session: sessionidd
    }
    $.ajax({
        type: "POST",
        url: "Webservice.aspx/Createticket",
        data: JSON.stringify(Dastas1),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populateticketinfo,
        failure: function (response) {
            alert(response.d);
        }
    });
    function populateticketinfo(data) {
        var xmlticket = data.d;
        console.log(xmlticket);
        var x2jss1 = new X2JS();
        var jsonticket = x2jss1.xml_str2json(xmlticket);
        console.log(jsonticket);
        var errorticket = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.Errors;
        if (errorticket != null || errorticket != undefined) {
            alert('Internal API Error');
        }
        else {
            var jsonngentickt = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.AirReservation.TravelerInfo;
            console.log(jsonngentickt);
            var FullGenerate = '';
            var PersonName = '';
            var prefix = '';
            var surname = '';
            var price = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.AirReservation.PriceInfo.ItinTotalFare.TotalFare._Amount;
            var currency = jsonticket.Envelope.Body.CreateTicketResponse.OTA_AirBookRS.AirReservation.PriceInfo.ItinTotalFare.TotalFare._Currency;

            var jsontikt = jsonngentickt.AirTraveler;
            var Airlinedetails = '';
            var PAXtype = '';
            var baggage = '';
            console.log(jsontikt);
            if (jsontikt.length != null) {
                for (var V = 0; V < jsontikt.length; V++) {
                    PersonName = jsontikt[V].PersonName.GivenName;
                    prefix = jsontikt[V].PersonName.NamePrefix;
                    surname = jsontikt[V].PersonName.Surname;
                    FullGenerate = jsontikt[V]._eTicketNumber;
                }
            }
            else {
                var jsontikt = jsonngentickt.AirTraveler;
                console.log(jsontikt);
                FullGenerate = jsonngentickt.AirTraveler._eTicketNumber;
            }
            alert(FullGenerate);
        }
    }
}

function Lastticketingdate() {
    $("#Lastticketingdate i").addClass("fa-spin");
    var datacancel = {
        PNRC: CPNR
    };

    $.ajax({
        type: "POST",
        url: "Webservice.aspx/GetLastticketingDate",
        data: JSON.stringify(datacancel),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessCancelinfo,
        failure: function (response) {
            alert(response.d);
        },

    });

}
function SuccessCancelinfo(data) {

    var xmlBookData = data.d;
    var x2js2 = new X2JS();
    var JsonCancelDate = x2js2.xml_str2json(xmlBookData);
    console.log(JsonCancelDate);
    var DeadLIneError = JsonCancelDate.Envelope.Body.GetLastTicketingDateResponse.LastTicketingDateRS.Errors.Error;
    console.log(DeadLIneError);
    if (DeadLIneError == '') {
        var CancelDate = JsonCancelDate.Envelope.Body.GetLastTicketingDateResponse.LastTicketingDateRS.LastTicketingDateData.LastTicketingDateData._LastTicketingDate;
        $("#deadline").append('CancellationDeadLine On ' + (moment(CancelDate).format("DD MMM YYYY")));
        $("#TimeLimit").append((moment(CancelDate).format("DD MMM YYYY")) + '<a title="Last ticketing date" id="Lastticketingdate" onclick="Lastticketingdate()"><i class="fa fa-refresh" style="color:#cca789;margin-left:10px;cursor:pointer" aria-hidden="true"></i></a>');

        var datacancel = {
            bookingIDD: FareruleBID,
            CancelDatee: CancelDate
        };
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/updatelastticketingdate",
            data: JSON.stringify(datacancel),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: Successupdatelastticketingdate,
            failure: function (response) {
                alert(response.d);
            },

        });
    }
    else {

    }
    $("#Lastticketingdate i").removeClass("fa-spin");
}

//Clear Voucher Db
function ClearVoucherDataDB() {
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/DeleteVoucherUrlinformation",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        failure: function (response) {
            alert(response.d);
        }
    });
}
function Printt() {
    var Dastasseq = {
        ETripID: ETripID,
        ETktAttachMainLeg: ETktAttachMainLeg,
        ETktAttachTraveller: ETktAttachTraveller,
        ETktAttachFareBreakup: '<tr> <td width="153" style="font-weight:bold;">FARE BREAKUP</td> <td width="171"></td> </tr><tr> <td>Total fare:</td> <td>' + $("#divTotal").html() + '</td> </tr>',
        //ETktAttachFareBreakup: '<tr> <td width="153" style="font-weight:bold;">FARE BREAKUP</td> <td width="171"></td> </tr> <tr> <td>Base fare:</td> <td>' + $("#divTotNet").html() + '</td> </tr> <tr> <td>Taxes and fees:</td> <td>' + $("#divTotTax").html() + '</td> </tr> <tr> <td>Total fare:</td> <td>' + $("#divTotal").html() + '</td> </tr>',
        ETktBaggageInfo: $("#divBagInfoNew").html()
    };
    console.log(Dastasseq)
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/PrintTicket",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: PrinttSuccess,
        failure: function (response) {
            alert(response.d);
        },
        statusCode: {
            500: function () {
                alert('500');
            }
        }
    });
    function PrinttSuccess(dpri) {
        if (dpri.d != "NODATA") {
            var divContents = dpri.d;
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title>Ticket</title>');
            printWindow.document.write('</head><body style="width:21cm;height:29.7cm;">');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(
  function () {
      printWindow.print();
  }, 1000);
        }
    }
}
function Getticketdetailsforupadte() {
    var datacancel = {
        PNRC: CPNR,
        CSurname: CSurname
    };

    $.ajax({
        type: "POST",
        url: "Webservice.aspx/GetPNR",
        data: JSON.stringify(datacancel),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SuccessGetPNRinfo,
        failure: function (response) {
            alert(response.d);
        },

    });



}
function SuccessGetPNRinfo(data) {
    var xmlticket = data.d;
    var x2jss1 = new X2JS();
    var jsonticket = x2jss1.xml_str2json(xmlticket);
    console.log(jsonticket);
    var errorticket = jsonticket.Envelope.Body.GetPNRResponse.OTA_AirBookRS.Errors;
    var Warningticket = jsonticket.Envelope.Body.GetPNRResponse.OTA_AirBookRS.Warnings;
    if (errorticket != null || errorticket != undefined) {
        alertify.alert('API', 'Internal API Error');
    }
    else if (Warningticket != null || Warningticket != undefined) {
        alertify.alert('ETICKET_ERROR', 'E-Ticket couldnot be issued due to technical difficulties.');
    }
    else {
        var jsonngentickt = jsonticket.Envelope.Body.GetPNRResponse.OTA_AirBookRS.AirReservation.TravelerInfo;
        //console.log(jsonngentickt);
        var TicketNumber = '';
        var PersonName = '';
        var surname = '';
        var BirthDate = '';
        var jsontikt = jsonngentickt.AirTraveler;
        console.log(jsontikt);

        if (jsontikt.length != null) {
            for (var V = 0; V < jsontikt.length; V++) {
                if (V == 0) {
                    TicketNumber = jsontikt[V]._eTicketNumber;
                    PersonName = jsontikt[V].PersonName.GivenName;
                    surname = jsontikt[V].PersonName.Surname;
                    BirthDate = jsontikt[V].BirthDate;
                }
                else {
                    TicketNumber += ',' + jsontikt[V]._eTicketNumber;
                    PersonName += ',' + jsontikt[V].PersonName.GivenName;
                    surname += ',' + jsontikt[V].PersonName.Surname;
                    BirthDate += ',' + jsontikt[V].BirthDate;
                }
                //updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
            }
        }
        else {
            TicketNumber = jsontikt._eTicketNumber;
            PersonName = jsontikt.PersonName.GivenName;
            surname = jsontikt.PersonName.Surname;
            BirthDate = jsontikt.BirthDate;
            //updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
        }
        if (TicketNumber != undefined) {
            ClearVoucherDataDB();
            updateTKTNum(TicketNumber, PersonName, surname, BirthDate);
            updateBookStatus('OK');
            InsertUpdatePayment();
            alertify.alert('Ticket', 'Ticket generated successfully');
        }
        else {
            alertify.alert('Ticket', 'Ticket is not issued yet');
        }

    }

}

//Insert tblPayment
function InsertUpdatePayment() {
    InsertTblPayment(PayedTotalPrice, PaymentCurrency, '1');
}
function InsertTblPayment(PaidAmount, CurrencyCode, PaymentTypeID) {
    var DataInsert = {
        PaidAmount: PaidAmount,
        CurrencyCode: CurrencyCode,
        PaymentTypeID: PaymentTypeID
    }
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/InsertTblPayment",
        data: JSON.stringify(DataInsert),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: succesInsertTblPayment,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function succesInsertTblPayment(dataa) {
    //alert(dataa.d)
}