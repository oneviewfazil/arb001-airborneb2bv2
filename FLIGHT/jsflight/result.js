﻿//Global Vars
var ClientCountryName = 'Travelling';
var DateFormate = 'mm/dd/yy';
var ButtonSelect = '';
var ButtonGetBaggage = '';
var ButtonGetFareRules = '';
var FirstResultFlightSegments = '';
var LegTitle = '';
var LegTitleCombi = '';
var FirstResultFlightResult = '';
var jsonn = '';

//Filters Vars
var HiddenLiBlocks = [];
//var AllAirlinesArray = [];
//var AllAirlinesArray1D = [];
//var AllStopsArray = [];
//var AllStopsArray1D = [];
var TotalLegsDuration = parseFloat(0);
var DepartureTimeeeeFirstLeg = '';
var ArrivalTimeeeeFirstLeg = '';

//Departure Time Filter
var AllDepartureTimeLocArray = [];
//var AllDepartureTimeArray1D = [];
//var AllDeparturePlaceArray1D = [];

//Arrival Time Filter
var AllArrivalTimeLocArray = [];
//var AllArrivalTimeArray1D = [];
//var AllArrivalPlaceArray1D = [];

//Multiple Coombinations
var ShowMoreCombiButton = '';
var FirstResultFlightSegmentsCombi = '';
var FirstResultFlightResultCombi = '';
var One;

//NEW
var MainLegValAirLine = [];
var MainLegValAirLineCombi = [];
var LoadingGIFBag = '<img class="loadingGIFBag" src="/COMMON/images/Loading_red.gif" style="display:none;width:10%;height:10%;align-items:flex-start" />'

var MarkupTypeID = parseFloat(0);
var MarkupValue = parseFloat(0);

var airlinelogopath = '/COMMON/images/Airlines/';

var baggginfo = '';

$(document).ready(function () {
    localStorage.clear();
    $('#sort_popup').css("z-index", "0000");
    showIntrestingFact();
    if (location.hostname === "localhost") {
        DateFormate = 'dd/mm/yy';
    }
    else {
        DateFormate = 'mm/dd/yy';
    }
    SessionChecking('UserDetails');
});

function ShowFlightResults() {
    $(".loadingGIF").show();
    var triptype = GetParameterValues('triptype');
    var from = GetParameterValues('from');
    var to = GetParameterValues('to');
    var depdate = GetParameterValues('depdate');
    var ad = GetParameterValues('ad');
    var ch = GetParameterValues('ch');
    var inf = GetParameterValues('inf');
    var retdate = GetParameterValues('retdate');
    var cabinclass = GetParameterValues('class');
    var Direct = GetParameterValues('Direct');

    var leg1 = GetParameterValues('leg1');
    var leg2 = GetParameterValues('leg2');
    var leg3 = GetParameterValues('leg3');
    var leg4 = GetParameterValues('leg4');
    var leg5 = GetParameterValues('leg5');
    var leg6 = GetParameterValues('leg6');


    if (triptype == 'M') {
        $('.top_details').hide();
        document.title = '' + AgencyShortName + ' - ' + getCityNameUTC(leg1.split('-')[0]) + ' to ' + getCityNameUTC(leg1.split('-')[1]) + ' Flights';
    }
    else {
        $('.top_details').show();
        document.title = '' + AgencyShortName + ' - ' + getCityNameUTC(from) + ' to ' + getCityNameUTC(to) + ' Flights';
        Showsearchsummary(triptype, from, to, depdate, retdate, ad, ch, inf);
    }

    if (triptype == 'NODATA') {
        window.location.href = "/search.html";
    }
    else {
        var qstrings = {
            triptype: triptype,
            from: from,
            to: to,
            depdate: depdate,
            retdate: retdate,
            ad: ad,
            ch: ch,
            inf: inf,
            leg1: leg1,
            leg2: leg2,
            leg3: leg3,
            leg4: leg4,
            leg5: leg5,
            leg6: leg6,
            cc: cabinclass,
            direct: Direct
        }
        $.ajax({
            type: "POST",
            url: "Webservice.aspx/FlightSearchResponse",
            data: JSON.stringify(qstrings),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: searchsucess,
            failure: function (response) {
                alert(response.d);
            }
        });
        function searchsucess(data) {
            //Result Generation Start
            var xml = data.d;
            var x2js = new X2JS();
            var jsonObj = x2js.xml_str2json(xml);
            //console.log(jsonObj);
            var Errorrs = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.Errors;
            //jsonn = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries.PricedItinerary;
            jsonn = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries;
            if (Errorrs != null) {
                console.log('Error - No Flight');
                var MinAmount = 0;
                var MaxAmount = 1;
                var CurrencyFirst = '';
                $("#divNoFlight").show();
            }
            if (jsonn != null) {
                jsonn = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries.PricedItinerary;
                if (jsonn != null) {
                    //console.log(jsonn);
                    if (jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries.FreeBaggages != "") {
                        baggginfo = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries.FreeBaggages.Baggage;
                        window.localStorage.setItem("bagginfoo", JSON.stringify(baggginfo));
                    }
                    else {
                        baggginfo = '';
                        window.localStorage.setItem("bagginfoo", JSON.stringify(baggginfo));
                    }
                    //console.log(baggginfo);
                    if (jsonn._Currency == null) {
                        //Multi Seq
                        var MinAmount = CalcMarkup(_.first(jsonn).AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, _.first(jsonn).AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount);
                        var MaxAmount = CalcMarkup(_.last(jsonn).AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, _.first(jsonn).AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount);
                        var CurrencyFirst = _.first(jsonn)._Currency;
                        $.each(jsonn, function (i, item) {
                            //Vars
                            var IndexList;
                            var ValidatingAirlineCode;
                            var CombinationID;

                            //Flags
                            var MainResultCreated = 0;

                            //Clearing
                            ButtonSelect = '';
                            ButtonGetBaggage = '';
                            ButtonGetFareRules = '';
                            FirstResultFlightSegments = '';
                            LegTitle = '';
                            LegTitleCombi = '';
                            FirstResultFlightResult = '';
                            TotalLegsDuration = 0;
                            DepartureTimeeeeFirstLeg = '';
                            ArrivalTimeeeeFirstLeg = '';

                            //OtherCombi
                            ShowMoreCombiButton = '';
                            FirstResultFlightSegmentsCombi = '';
                            FirstResultFlightResultCombi = '';
                            One = '';

                            MainLegValAirLine = [];
                            MainLegValAirLineCombi = [];

                            //Calculate Fare Details
                            var PassengerTypeQuant = parseFloat(0);
                            var BaseFareTotal = parseFloat(0);
                            var TaxTotal = parseFloat(0);
                            var TotalFare = parseFloat(0);

                            var PaxCountData = '';

                            if (item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.length != null) {
                                for (var ish = 0; ish < item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.length; ish++) {
                                    PassengerTypeQuant = parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Quantity);
                                    BaseFareTotal += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.BaseFare._Amount) * PassengerTypeQuant;
                                    TaxTotal += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.Taxes.Tax._Amount) * PassengerTypeQuant;
                                    TotalFare += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.TotalFare._Amount) * PassengerTypeQuant;
                                    PaxCountData += '' + item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Quantity + ' ' + GetPaxName(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Code) + ' ';
                                }
                            }
                            else {
                                PassengerTypeQuant = parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Quantity);
                                BaseFareTotal += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.BaseFare._Amount) * PassengerTypeQuant;
                                TaxTotal += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.Taxes.Tax._Amount) * PassengerTypeQuant;
                                TotalFare += parseFloat(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.TotalFare._Amount) * PassengerTypeQuant;
                                PaxCountData += '' + item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Quantity + ' ' + GetPaxName(item.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Code) + ' ';
                            }
                            //var FareDetailsGen = '<div class="fare_head"> <h3><i class="fa fa-money" aria-hidden="true"></i>TOTAL (' + PaxCountData + ') ' + item._Currency + ' ' + TotalFare + '</h3> </div>     <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Base Fare</h1> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Adults (6 x 21,480)</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>Rs 128,880</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Surcharges</h1> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Adults (6 x 21,480)</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>Rs 128,880</h2> </div> </li> </ul> </div> <div class="right_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1></h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2></h2> </div> </li> </ul> </div> </div>';
                            //Calculate Fare Details

                            if (item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length != null) {

                                CreateMoreCombiButton(item._SequenceNumber, item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length);

                                //Multiple Combinations
                                for (var i4 = 0; i4 < item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length; i4++) {
                                    if (MainResultCreated == 0) {
                                        //Creation of FIrst Result
                                        MainResultCreated = 1;

                                        IndexList = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                                        ValidatingAirlineCode = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._ValidatingAirlineCode;
                                        CombinationID = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;

                                        CreateButton(item._SequenceNumber, CombinationID, IndexList);
                                        var strArr = IndexList.split(';');
                                        for (var i6 = 0; i6 < strArr.length; i6++) {
                                            var Index = strArr[i6];
                                            if (Index != null) {

                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                                    //Multiple OriginDestination Options
                                                    for (var i7 = 0; i7 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {

                                                        if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {

                                                            var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';

                                                            var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                            if (FlightSegCount != null) {
                                                                //Multiple Flight Segments
                                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                                var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                                var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                                CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);

                                                                for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                                    var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                                    var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                                    var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                                    var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                                    var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                                    var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                    var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;

                                                                    var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                                    var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                                    var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                                    if (lugaage != undefined) {
                                                                        if (lugaage.length != null) {
                                                                            for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                                var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                                var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;
                                                                            }
                                                                        }
                                                                        else {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = 'undefined';
                                                                    }


                                                                    DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                    ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                    //Layover Time
                                                                    var LayoverTimee = '';
                                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                        var FirstArrivalTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                        var NextDeptTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                        var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                                        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                                        LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                                    }
                                                                    //Layover Time

                                                                    //Cabin
                                                                    var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                    if (BookingClassAvailCount != null) {
                                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                    }
                                                                    else {
                                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                    }
                                                                    //Cabin

                                                                    var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                                    var TechStopLoc = '';
                                                                    var TechStopDept = '';
                                                                    var TechStopArri = '';
                                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                        if (TechStopLoc == undefined) {
                                                                            TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                            TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                            TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                        }
                                                                    }
                                                                    ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                                }
                                                            }
                                                            else {
                                                                //Single Flight Segments
                                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                                var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;
                                                                var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                                CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                                var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                                var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                                var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                                var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }
                                                                var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                                var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                                var LayoverTimee = '';

                                                                //Cabin
                                                                var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }
                                                            if (FlightSegCount >= 2) {
                                                                var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                                var Stops = Stops + '-stop'
                                                            }
                                                            else {
                                                                var Stops = 'Non-stop'
                                                            }

                                                            ShowFlightResultGo(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                                        }
                                                    }
                                                }
                                                else {
                                                    //Single Origin DestinationOption Segments
                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                                        var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';

                                                        var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                                        if (FlightSegCount != null) {

                                                            var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                                            var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                            var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                            var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                            var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                            var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                            for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                                                var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                                var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                                var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                                var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                                var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                                var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                                var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._Terminal;
                                                                var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._Terminal;

                                                                var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }
                                                                DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                //Layover Time
                                                                var LayoverTimee = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length - 1 >= i9 + 1) {
                                                                    var FirstArrivalTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime);
                                                                    var NextDeptTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9 + 1]._DepartureDateTime);

                                                                    var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                                    var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                                    LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                                }
                                                                //Layover Time

                                                                //Cabin
                                                                var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation != null) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }
                                                        }
                                                        else {
                                                            var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                                            var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;

                                                            var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                            var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                            var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                                            var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                            var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                                            var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                                            var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                                            var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                                    var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;

                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }


                                                            var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                                            var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                                            var LayoverTimee = '';

                                                            //Cabin
                                                            var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                                TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                                TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                                TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri);
                                                        }

                                                        if (FlightSegCount >= 2) {
                                                            var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                            var Stops = Stops + '-stop'
                                                        }
                                                        else {
                                                            var Stops = 'Non-stop'
                                                        }
                                                        ShowFlightResultGo(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        //Other Combinations Results
                                        IndexList = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                                        ValidatingAirlineCode = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._ValidatingAirlineCode;
                                        CombinationID = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;

                                        //CreateDivRightPrice(item._SequenceNumber, CombinationID, IndexList, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount);
                                        //CreateButton(item._SequenceNumber, CombinationID, IndexList);

                                        var strArr = IndexList.split(';');
                                        for (var i6 = 0; i6 < strArr.length; i6++) {
                                            var Index = strArr[i6];
                                            if (Index != null) {
                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                                    //Multiple OriginDestination Options
                                                    for (var i7 = 0; i7 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {

                                                        if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {

                                                            var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';

                                                            var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                            if (FlightSegCount != null) {
                                                                //Multiple Flight Segments
                                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                                CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");


                                                                for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                                    var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                                    var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                                    var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                                    var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                                    var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                                    var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                    var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;

                                                                    var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                                    var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                                    var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                                    if (lugaage != undefined) {
                                                                        if (lugaage.length != null) {
                                                                            for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                                var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                                var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;
                                                                            }
                                                                        }
                                                                        else {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = 'undefined';
                                                                    }


                                                                    DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                    ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                    //Layover Time
                                                                    var LayoverTimeeCombi = '';
                                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                        var FirstArrivalTimeCombi = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                        var NextDeptTimeCombi = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                        var DifferenceMilliCombi = NextDeptTimeCombi - FirstArrivalTimeCombi;
                                                                        var DifferenceMilliDurationCombi = moment.duration(DifferenceMilliCombi);
                                                                        LayoverTimeeCombi = DifferenceMilliDurationCombi.hours() + 'h ' + DifferenceMilliDurationCombi.minutes() + 'm';
                                                                    }
                                                                    //Layover Time

                                                                    //Cabin
                                                                    var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                    if (BookingClassAvailCount != null) {
                                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                    }
                                                                    else {
                                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                    }
                                                                    //Cabin

                                                                    var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                                    var TechStopLoc = '';
                                                                    var TechStopDept = '';
                                                                    var TechStopArri = '';
                                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                        if (TechStopLoc == undefined) {
                                                                            TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                            TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                            TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                        }
                                                                    }

                                                                    ShowFlightItenaryGoCombi(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                                }

                                                            }
                                                            else {
                                                                //Single Flight Segments
                                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                                CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                                var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                                var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                                var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                                var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }


                                                                var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                                var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                                var LayoverTimeeCombi = '';

                                                                //Cabin
                                                                var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGoCombi(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }
                                                            if (FlightSegCount >= 2) {
                                                                var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                                var Stops = Stops + '-stop'
                                                            }
                                                            else {
                                                                var Stops = 'Non-stop'
                                                            }

                                                            ShowFlightResultGoCombi(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount);
                                                        }


                                                    }

                                                }
                                                else {
                                                    //Single Origin DestinationOption Segments
                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                                        var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';
                                                        var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                                        if (FlightSegCount != null) {

                                                            var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                                            var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                            var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                            var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                            CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                            for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                                                var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                                var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                                var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                                var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                                var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                                var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                                var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._Terminal;
                                                                var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._Terminal;

                                                                var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Type;
                                                                        }
                                                                    }
                                                                    else {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }


                                                                DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                //Layover Time
                                                                var LayoverTimeeCombi = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length - 1 >= i9 + 1) {
                                                                    var FirstArrivalTimeCombi = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime);
                                                                    var NextDeptTimeCombi = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9 + 1]._DepartureDateTime);

                                                                    var DifferenceMilliCombi = NextDeptTimeCombi - FirstArrivalTimeCombi;
                                                                    var DifferenceMilliDurationCombi = moment.duration(DifferenceMilliCombi);
                                                                    LayoverTimeeCombi = DifferenceMilliDurationCombi.hours() + 'h ' + DifferenceMilliDurationCombi.minutes() + 'm';
                                                                }
                                                                //Layover Time

                                                                //Cabin
                                                                var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation != null) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGoCombi(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }
                                                        }
                                                        else {
                                                            var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                                            var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;

                                                            var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                            var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                                            CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                            var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                                            var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                                            var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                                            var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                                    var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }



                                                            var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                                            var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                                            var LayoverTimeeCombi = '';

                                                            //Cabin
                                                            var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                                TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                                TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                                TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGoCombi(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }

                                                        if (FlightSegCount >= 2) {
                                                            var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                            var Stops = Stops + '-stop'
                                                        }
                                                        else {
                                                            var Stops = 'Non-stop'
                                                        }
                                                        ShowFlightResultGoCombi(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount);
                                                    }
                                                }
                                            }
                                        }


                                        var ButtonSelectNew = '<input id="btnSubmit" type="button" class="more_search" onclick="selectflight(' + item._SequenceNumber + ',' + CombinationID + ',/' + IndexList + '/)" value="Select">';

                                        var MobileValAirlineIMGCombi = '';
                                        var MainLegValAirLineUNIQCombi = _.uniq(MainLegValAirLineCombi);
                                        for (var ivalairlineCombi = 0; ivalairlineCombi < MainLegValAirLineUNIQCombi.length; ivalairlineCombi++) {
                                            MobileValAirlineIMGCombi += "<img src='" + airlinelogopath + MainLegValAirLineUNIQCombi[ivalairlineCombi] + ".gif' alt='" + MainLegValAirLineUNIQCombi[ivalairlineCombi] + "' title='" + MainLegValAirLineUNIQCombi[ivalairlineCombi] + "'>";
                                        }

                                        One += '<div class="search_details sp07" id="more_info_' + item._SequenceNumber + '_' + CombinationID + '"><div class="flights_left_area">'
                                            + FirstResultFlightResultCombi + '</div><div class="flight_rate_area"><div class="rate_area01">  <h4>'
                                       + item._Currency + ' ' + CalcMarkup(item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div>' + ButtonSelectNew +
                                       '<a class="showpop" id="showpop_' + item._SequenceNumber + '_' + CombinationID + '">More info</a></div><div id="popup_' + item._SequenceNumber + '_' + CombinationID + '" class="simplePopup"></div><!-----Mobile view area-------> <div class="mb_flight_rate_area"><div class="mb_rate_right"> ' + ButtonSelectNew +
                                       '<div class="rate_area02"> <h4>' + item._Currency + ' ' + CalcMarkup(item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> </div> <div class="mb_more"> <a class="showpop" id="showpop_'
                                       + item._SequenceNumber + '_' + CombinationID + '">More info</a> </div> </div> <!-----Mobile view area-------></div>' +
                                       '<div id="more_info_popup_' + item._SequenceNumber + '_' + CombinationID + '" class="more_info_searchresultpopup popuphiddenfield"><a class="closetb"><i class="fa fa-window-close" aria-hidden="true"></i></a><div class="tabs animated-fade"><ul class="tab-links">' +
                                       '<li class="active"><a href="#tab1_' + item._SequenceNumber + '_' + CombinationID + '_1">Itinerary </a></li>' +
                                       '<li><a href="#tab2_' + item._SequenceNumber + '_' + CombinationID + '_2">Fare info </a></li>' +
                                       '<li data-open="0" onclick="GetBaggageInfoClick(' + item._SequenceNumber + ',' + CombinationID + ',this)"><a href="#tab2_' + item._SequenceNumber + '_' + CombinationID + '_3">Baggage </a></li></ul><div class="tab-content">' +
                                       '<div id="tab1_' + item._SequenceNumber + '_' + CombinationID + '_1" class="tab active">' + FirstResultFlightSegmentsCombi + ' </div>' +
                                       '<div id="tab2_' + item._SequenceNumber + '_' + CombinationID + '_2" class="tab"> <div class="fare_details"> <div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + PaxCountData + '</h3> </div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Fare</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                       + item._Currency + ' ' + BaseFareTotal + '</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Taxes & Fees</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                       + item._Currency + ' ' + CalcMarkupTax(TotalFare, TaxTotal, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li><li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Total</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                       + item._Currency + ' ' + CalcMarkup(TotalFare, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li> </ul> </div> <div class="right_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1></h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2></h2> </div> </li>' +
                                       '</ul></div></div></div></div>  <div id="tab2_' + item._SequenceNumber + '_' + CombinationID + '_3" class="tab"> <div id="divBindBagInfo_' + item._SequenceNumber + '_' + CombinationID + '" class="fare_details">' + LoadingGIFBag + '</div></div>   </div></div></div>';

                                        FirstResultFlightResultCombi = '';
                                        FirstResultFlightSegmentsCombi = '';
                                    }
                                }
                            }
                            else {
                                //Single Combinations
                                IndexList = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._IndexList;
                                ValidatingAirlineCode = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._ValidatingAirlineCode;
                                CombinationID = item.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._CombinationID;
                                CreateButton(item._SequenceNumber, CombinationID, IndexList);

                                var strArr = IndexList.split(';');
                                for (var i6 = 0; i6 < strArr.length; i6++) {
                                    var Index = strArr[i6];
                                    if (Index != null) {
                                        if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                            //Multiple Flight Segments
                                            for (var i7 = 0; i7 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                                    var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';
                                                    var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                    if (FlightSegCount != null) {
                                                        var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                        var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                        var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                        var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                        var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                        var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                        CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                        for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                            var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                            var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                            var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                            var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                            var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                            var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                            var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;
                                                            var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                            var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                            var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                    var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }

                                                            DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                            ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                            //Layover Time
                                                            var LayoverTimee = '';
                                                            if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                var FirstArrivalTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                var NextDeptTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                                var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                                LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                            }
                                                            //Layover Time

                                                            //Cabin
                                                            var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }
                                                    }
                                                    else {
                                                        var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                        var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                        var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                        var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                        var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;
                                                        var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                        CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                        var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                        var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                        var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                        var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                        if (lugaage != undefined) {
                                                            if (lugaage.length != null) {
                                                                for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                    var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                    var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;

                                                                }

                                                            }
                                                            else {
                                                                var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                                var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                            }
                                                        }
                                                        else {
                                                            var bagonload = 'undefined';
                                                        }

                                                        var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                        var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                        var LayoverTimee = '';

                                                        //Cabin
                                                        var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                        if (BookingClassAvailCount != null) {
                                                            var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                        }
                                                        else {
                                                            var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                        }
                                                        //Cabin

                                                        var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                        var TechStopLoc = '';
                                                        var TechStopDept = '';
                                                        var TechStopArri = '';
                                                        if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                            TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                            TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                            TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                            if (TechStopLoc == undefined) {
                                                                TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                                TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                            }
                                                        }

                                                        ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                    }
                                                    if (FlightSegCount >= 2) {
                                                        var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                        var Stops = Stops + '-stop'
                                                    }
                                                    else {
                                                        var Stops = 'Non-stop'
                                                    }
                                                    ShowFlightResultGo(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                                }
                                            }
                                        }
                                        else {
                                            //Single Flight Segments
                                            var ElapsedTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + item.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';

                                            var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                            var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                            var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                            var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                            var FlightSegCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                            if (FlightSegCount != null) {
                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                for (var i9 = 0; i9 < item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
                                                    var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                    var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                    var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                    var From = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                    var DepartureDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                    var To = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                    var ArrivalDateTime = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                    var DeptTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._Terminal;
                                                    var ArriTerminal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._Terminal;

                                                    var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages
                                                    if (lugaage != undefined) {
                                                        if (lugaage.length != null) {
                                                            for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Type;
                                                            }
                                                        }
                                                        else {
                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Index;
                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Type;
                                                        }
                                                    }
                                                    else {
                                                        var bagonload = 'undefined';
                                                    }

                                                    DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                    ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                    //Layover Time
                                                    var LayoverTimee = '';
                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length - 1 >= i9 + 1) {
                                                        var FirstArrivalTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime);
                                                        var NextDeptTime = moment(item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9 + 1]._DepartureDateTime);

                                                        var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                        var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                        LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                    }
                                                    //Layover Time

                                                    //Cabin
                                                    var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                    if (BookingClassAvailCount != null) {
                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                    }
                                                    else {
                                                        var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                        var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                    }
                                                    //Cabin

                                                    var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Equipment._AirEquipType;

                                                    var TechStopLoc = '';
                                                    var TechStopDept = '';
                                                    var TechStopArri = '';
                                                    if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation != null) {
                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._LocationCode;
                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._DepartureDateTime;
                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                        if (TechStopLoc == undefined) {
                                                            TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._LocationCode;
                                                            TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                            TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                        }
                                                    }

                                                    ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                }
                                            }
                                            else {
                                                var FromGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                                var ToGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;


                                                var DepTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                var DepTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                var ArriTimeGo = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                                var ArriTimeGoOriginal = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                                DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                var MarketingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                                var OperatingAirline = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                                var FlightNumber = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                                var lugaage = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                                if (lugaage != undefined) {
                                                    if (lugaage.length != null) {
                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                            var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                            var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;
                                                        }

                                                    }
                                                    else {
                                                        var bagonload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                        var bagoPaxload = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;
                                                    }
                                                }
                                                else {
                                                    var bagonload = 'undefined';
                                                }

                                                var DeptTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                                var ArriTerminalSing = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                                var LayoverTimee = '';

                                                //Cabin
                                                var BookingClassAvailCount = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                if (BookingClassAvailCount != null) {
                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                }
                                                else {
                                                    var ResBookDesigCabinCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                    var ResBookDesigCode = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                }
                                                //Cabin

                                                var AirEquipType = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                                var TechStopLoc = '';
                                                var TechStopDept = '';
                                                var TechStopArri = '';
                                                if (item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                    TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                    TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                    TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                    if (TechStopLoc == undefined) {
                                                        TechStopLoc = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                        TechStopDept = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                        TechStopArri = item.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                    }
                                                }

                                                ShowFlightItenaryGo(item._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                            }
                                            if (FlightSegCount >= 2) {
                                                var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                var Stops = Stops + '-stop'
                                            }
                                            else {
                                                var Stops = 'Non-stop'
                                            }
                                            ShowFlightResultGo(item._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, item._Currency, item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                        }
                                    }
                                }
                            }

                            //Generation Starts Here
                            var Combinations = '';
                            if (One != null) {
                                //Combinations = '<div class="MoreCombiDetails" id="downcombi' + item._SequenceNumber + '"><div class="deviderMoreCombi"><hr /></div><div class="fullArea">'
                                //   + One + '</div><div class="deviderMoreCombi"><hr /></div></div>';

                                //Combinations = '<div id="downcombi' + item._SequenceNumber + '">' + One + '</div>';
                                Combinations = '<div class="hide_area_show" id="HideComb_' + item._SequenceNumber + '">' + One + '</div>';
                            }

                            var MobileValAirlineIMG = '';
                            var MainLegValAirLineUNIQ = _.uniq(MainLegValAirLine);
                            for (var ivalairline = 0; ivalairline < MainLegValAirLineUNIQ.length; ivalairline++) {
                                MobileValAirlineIMG += "<img src='" + airlinelogopath + MainLegValAirLineUNIQ[ivalairline] + ".gif' alt='" + MainLegValAirLineUNIQ[ivalairline] + "' title='" + MainLegValAirLineUNIQ[ivalairline] + "'>";
                            }

                            $('#divFLSearch').append('<div data-show="0" class="show_more_visible" id="show_more_visible_seq_' + item._SequenceNumber + '_0' +
                                '" data-price="' + CalcMarkup(item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '" data-duration="' + TotalLegsDuration + '" data-dept="' + DepartureTimeeeeFirstLeg + '" data-arri="' + ArrivalTimeeeeFirstLeg + '"><!----flight details 1 and popup------><div class="search_details sp07" id="more_info_' + item._SequenceNumber +
                                '"><div class="flights_left_area">' + FirstResultFlightResult + '</div> <div class="flight_rate_area"> <div class="rate_area01">  <h4>'
                                + item._Currency + ' ' + CalcMarkup(item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> ' + ButtonSelect + ' <a class="showpop" id="showpop_'
                                + item._SequenceNumber + '">More info</a></div> <div id="popup_' + item._SequenceNumber + '" class="simplePopup"></div> <!-----Mobile view area-------> <div class="mb_flight_rate_area">  <div class="mb_rate_right"> ' + ButtonSelect + ' <div class="rate_area02">  <h4>'
                                + item._Currency + ' ' + CalcMarkup(item.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> </div> <div class="mb_more"> <a class="showpop" id="showpop_'
                                + item._SequenceNumber + '">More info</a> </div> </div> <!-----Mobile view area-------> </div> <div id="more_info_popup_' + item._SequenceNumber +
                                '" class="more_info_searchresultpopup popuphiddenfield"><a class="closetb"><i class="fa fa-window-close" aria-hidden="true"></i></a> <div class="tabs animated-fade"> <ul class="tab-links"> <li class="active"><a href="#tab1_'
                                + item._SequenceNumber + '_1">Itinerary </a></li>' +
                                '<li><a href="#tab1_' + item._SequenceNumber + '_2">Fare info</a></li>' +
                                '<li data-open="0" onclick="GetBaggageInfoClick(' + item._SequenceNumber + ',0,this)"><a href="#tab1_' + item._SequenceNumber + '_3">Baggage</a></li> </ul> <div class="tab-content">' +
                                '<div id="tab1_' + item._SequenceNumber + '_1" class="tab active">' + FirstResultFlightSegments + '</div>' +
                                '<div id="tab1_' + item._SequenceNumber + '_2" class="tab"> <div class="fare_details"> <div class="fare_head"> <h3><i class="fa fa-money" aria-hidden="true"></i>' + PaxCountData + '</h3> </div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Fare</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                + item._Currency + ' ' + BaseFareTotal + '</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Taxes & Fees</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                + item._Currency + ' ' + CalcMarkupTax(TotalFare, TaxTotal, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li><li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Total</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                + item._Currency + ' ' + CalcMarkup(TotalFare, item.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li> </ul> </div> <div class="right_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1></h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2></h2> </div> </li> </ul> </div> </div> </div> </div>' +
                                '' + ButtonGetBaggage + '</div></div></div>'
                                + Combinations + '' + ShowMoreCombiButton + '</div>');
                        });
                    }
                    else {
                        //one seq
                        var MinAmount = CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount);
                        var MaxAmount = CalcMarkup(parseFloat(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount), parseFloat(jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount)) + parseFloat(1);
                        var CurrencyFirst = jsonn._Currency;

                        //Vars
                        var IndexList;
                        var ValidatingAirlineCode;
                        var CombinationID;

                        //Flags
                        var MainResultCreated = 0;

                        //Clearing
                        ButtonSelect = '';
                        ButtonGetBaggage = '';
                        ButtonGetFareRules = '';
                        FirstResultFlightSegments = '';
                        LegTitle = '';
                        LegTitleCombi = '';
                        FirstResultFlightResult = '';
                        TotalLegsDuration = 0;
                        DepartureTimeeeeFirstLeg = '';
                        ArrivalTimeeeeFirstLeg = '';

                        //OtherCombi
                        ShowMoreCombiButton = '';
                        FirstResultFlightSegmentsCombi = '';
                        FirstResultFlightResultCombi = '';
                        One = '';

                        MainLegValAirLine = [];
                        MainLegValAirLineCombi = [];

                        //Calculate Fare Details
                        var PassengerTypeQuant = parseFloat(0);
                        var BaseFareTotal = parseFloat(0);
                        var TaxTotal = parseFloat(0);
                        var TotalFare = parseFloat(0);

                        var PaxCountData = '';

                        if (jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.length != null) {
                            for (var ish = 0; ish < jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.length; ish++) {
                                PassengerTypeQuant = parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Quantity);
                                BaseFareTotal += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.BaseFare._Amount) * PassengerTypeQuant;
                                TaxTotal += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.Taxes.Tax._Amount) * PassengerTypeQuant;
                                TotalFare += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerFare.TotalFare._Amount) * PassengerTypeQuant;
                                PaxCountData += '' + jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Quantity + ' ' + GetPaxName(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown[ish].PassengerTypeQuantity._Code) + ' ';
                            }
                        }
                        else {
                            PassengerTypeQuant = parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Quantity);
                            BaseFareTotal += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.BaseFare._Amount) * PassengerTypeQuant;
                            TaxTotal += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.Taxes.Tax._Amount) * PassengerTypeQuant;
                            TotalFare += parseFloat(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerFare.TotalFare._Amount) * PassengerTypeQuant;
                            PaxCountData += '' + jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Quantity + ' ' + GetPaxName(jsonn.AirItineraryPricingInfo.PTC_FareBreakdowns.PTC_FareBreakdown.PassengerTypeQuantity._Code) + ' ';

                        }
                        //Calculate Fare Details

                        if (jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length != null) {

                            CreateMoreCombiButton(jsonn._SequenceNumber, jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length);

                            //Multiple Combinations
                            for (var i4 = 0; i4 < jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length; i4++) {
                                if (MainResultCreated == 0) {
                                    //Creation of FIrst Result
                                    MainResultCreated = 1;

                                    IndexList = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                                    ValidatingAirlineCode = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._ValidatingAirlineCode;
                                    CombinationID = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;

                                    CreateButton(jsonn._SequenceNumber, CombinationID, IndexList);
                                    var strArr = IndexList.split(';');
                                    for (var i6 = 0; i6 < strArr.length; i6++) {
                                        var Index = strArr[i6];
                                        if (Index != null) {

                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                                //Multiple OriginDestination Options
                                                for (var i7 = 0; i7 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {

                                                    if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {

                                                        var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';

                                                        var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                        if (FlightSegCount != null) {
                                                            //Multiple Flight Segments
                                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                            var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                            var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);

                                                            for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                                var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                                var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                                var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                                var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                                var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                                var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;

                                                                var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                                var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                                ArriTerminalSing
                                                                var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                        }

                                                                    }
                                                                    else {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }

                                                                DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                //Layover Time
                                                                var LayoverTimee = '';
                                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                    var FirstArrivalTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                    var NextDeptTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                    var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                                    var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                                    LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                                }
                                                                //Layover Time

                                                                //Cabin
                                                                var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                        TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }
                                                        }
                                                        else {
                                                            //Single Flight Segments
                                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                            var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;
                                                            var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                            var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                            var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                            var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                            var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }

                                                            var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                            var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                            var LayoverTimee = '';

                                                            //Cabin
                                                            var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }
                                                        if (FlightSegCount >= 2) {
                                                            var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                            var Stops = Stops + '-stop'
                                                        }
                                                        else {
                                                            var Stops = 'Non-stop'
                                                        }

                                                        ShowFlightResultGo(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                                    }
                                                }
                                            }
                                            else {
                                                //Single Origin DestinationOption Segments
                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                                    var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';

                                                    var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                                    if (FlightSegCount != null) {

                                                        var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                                        var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                        var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                        var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                        var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                        var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                        CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                        for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                                            var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                            var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                            var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                            var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                            var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                            var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                            var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                            var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._Terminal;
                                                            var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._Terminal;

                                                            var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }

                                                            DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                            ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                            //Layover Time
                                                            var LayoverTimee = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length - 1 >= i9 + 1) {
                                                                var FirstArrivalTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime);
                                                                var NextDeptTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9 + 1]._DepartureDateTime);

                                                                var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                                var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                                LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                            }
                                                            //Layover Time

                                                            //Cabin
                                                            var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation != null) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }
                                                    }
                                                    else {
                                                        var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                                        var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;

                                                        var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                        var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                        var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                                        var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                        CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                        var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                                        var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                                        var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                                        var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                                        if (lugaage != undefined) {
                                                            if (lugaage.length != null) {
                                                                for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                                var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;
                                                            }
                                                        }
                                                        else {
                                                            var bagonload = 'undefined';
                                                        }

                                                        var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                                        var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                                        var LayoverTimee = '';

                                                        //Cabin
                                                        var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                        if (BookingClassAvailCount != null) {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                        }
                                                        else {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                        }
                                                        //Cabin

                                                        var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                                        var TechStopLoc = '';
                                                        var TechStopDept = '';
                                                        var TechStopArri = '';
                                                        if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                            TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                            TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                            TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                            if (TechStopLoc == undefined) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                            }
                                                        }

                                                        ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                    }

                                                    if (FlightSegCount >= 2) {
                                                        var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                        var Stops = Stops + '-stop'
                                                    }
                                                    else {
                                                        var Stops = 'Non-stop'
                                                    }
                                                    ShowFlightResultGo(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    //Other Combinations Results
                                    IndexList = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                                    ValidatingAirlineCode = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._ValidatingAirlineCode;
                                    CombinationID = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;

                                    var strArr = IndexList.split(';');
                                    for (var i6 = 0; i6 < strArr.length; i6++) {
                                        var Index = strArr[i6];
                                        if (Index != null) {
                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                                //Multiple OriginDestination Options
                                                for (var i7 = 0; i7 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {

                                                    if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {

                                                        var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';

                                                        var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                        if (FlightSegCount != null) {
                                                            //Multiple Flight Segments
                                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                            CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");


                                                            for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                                var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                                var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                                var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                                var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                                var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                                var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                                var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;

                                                                var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                                var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                                var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                                if (lugaage != undefined) {
                                                                    if (lugaage.length != null) {
                                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                            var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                            var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                        }

                                                                    }
                                                                    else {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = 'undefined';
                                                                }

                                                                DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                                ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                                //Layover Time
                                                                var LayoverTimeeCombi = '';
                                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                    var FirstArrivalTimeCombi = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                    var NextDeptTimeCombi = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                    var DifferenceMilliCombi = NextDeptTimeCombi - FirstArrivalTimeCombi;
                                                                    var DifferenceMilliDurationCombi = moment.duration(DifferenceMilliCombi);
                                                                    LayoverTimeeCombi = DifferenceMilliDurationCombi.hours() + 'h ' + DifferenceMilliDurationCombi.minutes() + 'm';
                                                                }
                                                                //Layover Time

                                                                //Cabin
                                                                var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                                if (BookingClassAvailCount != null) {
                                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                                }
                                                                else {
                                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                                }
                                                                //Cabin

                                                                var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                                var TechStopLoc = '';
                                                                var TechStopDept = '';
                                                                var TechStopArri = '';
                                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                    if (TechStopLoc == undefined) {
                                                                        TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                        TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                        TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                    }
                                                                }

                                                                ShowFlightItenaryGoCombi(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                            }

                                                        }
                                                        else {
                                                            //Single Flight Segments
                                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                            CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                            var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                            var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                            var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                            var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;
                                                                    }
                                                                }
                                                                else {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }

                                                            var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                            var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                            var LayoverTimeeCombi = '';

                                                            //Cabin
                                                            var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }

                                                            ShowFlightItenaryGoCombi(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }
                                                        if (FlightSegCount >= 2) {
                                                            var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                            var Stops = Stops + '-stop'
                                                        }
                                                        else {
                                                            var Stops = 'Non-stop'
                                                        }

                                                        ShowFlightResultGoCombi(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount);
                                                    }


                                                }

                                            }
                                            else {
                                                //Single Origin DestinationOption Segments
                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                                    var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';
                                                    var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                                    if (FlightSegCount != null) {

                                                        var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                                        var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                        var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                                        var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                        CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                        for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                                            var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                            var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                            var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                            var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                            var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                            var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                            var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                            var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                            var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                            var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                            if (lugaage != undefined) {
                                                                if (lugaage.length != null) {
                                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                    }

                                                                }
                                                                else {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                                }
                                                            }
                                                            else {
                                                                var bagonload = 'undefined';
                                                            }

                                                            DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                            ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                            //Layover Time
                                                            var LayoverTimeeCombi = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                                var FirstArrivalTimeCombi = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                                var NextDeptTimeCombi = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                                var DifferenceMilliCombi = NextDeptTimeCombi - FirstArrivalTimeCombi;
                                                                var DifferenceMilliDurationCombi = moment.duration(DifferenceMilliCombi);
                                                                LayoverTimeeCombi = DifferenceMilliDurationCombi.hours() + 'h ' + DifferenceMilliDurationCombi.minutes() + 'm';
                                                            }
                                                            //Layover Time

                                                            //Cabin
                                                            var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                            if (BookingClassAvailCount != null) {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                            }
                                                            else {
                                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                            }
                                                            //Cabin

                                                            var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                            var TechStopLoc = '';
                                                            var TechStopDept = '';
                                                            var TechStopArri = '';
                                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                                if (TechStopLoc == undefined) {
                                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                                }
                                                            }


                                                            ShowFlightItenaryGoCombi(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                        }
                                                    }
                                                    else {
                                                        var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                                        var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;

                                                        var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                                        var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                                        CreateLegTitleCombi(DepTimeGo, FromGo, ToGo, FlightSegCount);

                                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");



                                                        var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                                        var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                                        var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                                        var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                                        if (lugaage != undefined) {
                                                            if (lugaage.length != null) {
                                                                for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;

                                                                }

                                                            }
                                                            else {
                                                                var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                                var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;
                                                            }
                                                        }
                                                        else {
                                                            var bagonload = 'undefined';
                                                        }

                                                        var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                                        var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                                        var LayoverTimeeCombi = '';

                                                        //Cabin
                                                        var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                        if (BookingClassAvailCount != null) {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                        }
                                                        else {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                        }
                                                        //Cabin

                                                        var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                                        var TechStopLoc = '';
                                                        var TechStopDept = '';
                                                        var TechStopArri = '';
                                                        if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                            TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                            TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                            TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                            if (TechStopLoc == undefined) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                            }
                                                        }

                                                        ShowFlightItenaryGoCombi(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimeeCombi, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                    }

                                                    if (FlightSegCount >= 2) {
                                                        var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                        var Stops = Stops + '-stop'
                                                    }
                                                    else {
                                                        var Stops = 'Non-stop'
                                                    }
                                                    ShowFlightResultGoCombi(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount);
                                                }
                                            }
                                        }
                                    }

                                    var ButtonSelectNew = '<input id="btnSubmit" type="button" class="more_search" onclick="selectflight(' + jsonn._SequenceNumber + ',' + CombinationID + ',/' + IndexList + '/)" value="Select">';

                                    var MobileValAirlineIMGCombi = '';
                                    var MainLegValAirLineUNIQCombi = _.uniq(MainLegValAirLineCombi);
                                    for (var ivalairlineCombi = 0; ivalairlineCombi < MainLegValAirLineUNIQCombi.length; ivalairlineCombi++) {
                                        MobileValAirlineIMGCombi += "<img src='" + airlinelogopath + MainLegValAirLineUNIQCombi[ivalairlineCombi] + ".gif' alt='" + MainLegValAirLineUNIQCombi[ivalairlineCombi] + "' title='" + MainLegValAirLineUNIQCombi[ivalairlineCombi] + "'>";
                                    }

                                    One += '<div class="search_details sp07" id="more_info_' + jsonn._SequenceNumber + '_' + CombinationID + '"><div class="flights_left_area">'
                                        + FirstResultFlightResultCombi + '</div><div class="flight_rate_area"><div class="rate_area01"> <h4>'
                                   + jsonn._Currency + ' ' + CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div>' + ButtonSelectNew +
                                   '<a class="showpop" id="showpop_' + jsonn._SequenceNumber + '_' + CombinationID + '">More info</a></div><div id="popup_' + jsonn._SequenceNumber + '_' + CombinationID + '" class="simplePopup"></div><!-----Mobile view area-------> <div class="mb_flight_rate_area"><div class="mb_rate_right"> ' + ButtonSelectNew +
                                   '<div class="rate_area02"> <h4>' + jsonn._Currency + ' ' + CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> </div> <div class="mb_more"> <a class="showpop" id="showpop_'
                                   + jsonn._SequenceNumber + '_' + CombinationID + '">More info</a> </div> </div> <!-----Mobile view area-------></div>' +
                                   '<div id="more_info_popup_' + jsonn._SequenceNumber + '_' + CombinationID + '" class="more_info_searchresultpopup popuphiddenfield"><a class="closetb"><i class="fa fa-window-close" aria-hidden="true"></i></a><div class="tabs animated-fade"><ul class="tab-links"> <li class="active"><a href="#tab1_'
                                   + jsonn._SequenceNumber + '_' + CombinationID + '_1">Flight itinerary </a></li> <li><a href="#tab2_' + jsonn._SequenceNumber + '_' + CombinationID + '_2">Fare details</a></li> </ul><div class="tab-content"><div id="tab1_' + jsonn._SequenceNumber + '_' + CombinationID + '_1" class="tab active">' + FirstResultFlightSegmentsCombi + ' </div><div id="tab2_' + jsonn._SequenceNumber + '_' + CombinationID + '_2" class="tab"> <div class="fare_details"> <div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + PaxCountData + '</h3> </div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Fare</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                   + jsonn._Currency + ' ' + BaseFareTotal + '</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Taxes & Fees</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                   + jsonn._Currency + ' ' + CalcMarkupTax(TotalFare, TaxTotal, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li><li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Total</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                                   + jsonn._Currency + ' ' + CalcMarkup(TotalFare, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li> </ul> </div> <div class="right_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1></h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2></h2> </div> </li> </ul> </div> </div> </div>  </div></div></div></div>';

                                    FirstResultFlightResultCombi = '';
                                    FirstResultFlightSegmentsCombi = '';

                                }

                            }
                        }
                        else {
                            //Single Combinations
                            IndexList = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._IndexList;
                            ValidatingAirlineCode = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._ValidatingAirlineCode;
                            CombinationID = jsonn.AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._CombinationID;
                            CreateButton(jsonn._SequenceNumber, CombinationID, IndexList);

                            var strArr = IndexList.split(';');
                            for (var i6 = 0; i6 < strArr.length; i6++) {
                                var Index = strArr[i6];
                                if (Index != null) {
                                    if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                        //Multiple Flight Segments
                                        for (var i7 = 0; i7 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                                var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._ElapsedTime.toString().substr(2, 2) + 'm';
                                                var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                                if (FlightSegCount != null) {
                                                    var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0].DepartureAirport._LocationCode;
                                                    var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                                    var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                    var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[0]._DepartureDateTime;
                                                    var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                                    var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                                    DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                    ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                    CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                    for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                        var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].MarketingAirline._Code;
                                                        var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].OperatingAirline._Code;
                                                        var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._FlightNumber;
                                                        var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                        var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._DepartureDateTime;
                                                        var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                        var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime;
                                                        var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._Terminal;
                                                        var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._Terminal;

                                                        var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages
                                                        if (lugaage != undefined) {
                                                            if (lugaage.length != null) {
                                                                for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                                }

                                                            }
                                                            else {
                                                                var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Index;
                                                                var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Baggages.Baggage._Type;
                                                            }
                                                        }
                                                        else {
                                                            var bagonload = 'undefined';
                                                        }

                                                        DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                        ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                        //Layover Time
                                                        var LayoverTimee = '';
                                                        if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length - 1 >= i9 + 1) {
                                                            var FirstArrivalTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9]._ArrivalDateTime);
                                                            var NextDeptTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9 + 1]._DepartureDateTime);

                                                            var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                            var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                            LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                        }
                                                        //Layover Time

                                                        //Cabin
                                                        var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                        if (BookingClassAvailCount != null) {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                        }
                                                        else {
                                                            var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                            var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                        }
                                                        //Cabin

                                                        var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].Equipment._AirEquipType;

                                                        var TechStopLoc = '';
                                                        var TechStopDept = '';
                                                        var TechStopArri = '';
                                                        if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation != null) {
                                                            TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._LocationCode;
                                                            TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._DepartureDateTime;
                                                            TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                            if (TechStopLoc == undefined) {
                                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._LocationCode;
                                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                            }
                                                        }

                                                        ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                    }
                                                }
                                                else {
                                                    var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                                    var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;

                                                    var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                    var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._DepartureDateTime;
                                                    var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;
                                                    var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._ArrivalDateTime;

                                                    DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                                    ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                                    CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                                    var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.MarketingAirline._Code;
                                                    var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.OperatingAirline._Code;
                                                    var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment._FlightNumber;

                                                    var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages
                                                    if (lugaage != undefined) {
                                                        if (lugaage.length != null) {
                                                            for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                                var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Index;
                                                                var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages[ilugg].Baggage._Type;

                                                            }

                                                        }
                                                        else {
                                                            var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Index;
                                                            var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Baggages.Baggage._Type;
                                                        }
                                                    }
                                                    else {
                                                        var bagonload = 'undefined';
                                                    }

                                                    var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._Terminal;
                                                    var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._Terminal;

                                                    var LayoverTimee = '';

                                                    //Cabin
                                                    var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                                    if (BookingClassAvailCount != null) {
                                                        var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                        var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                    }
                                                    else {
                                                        var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                        var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                    }
                                                    //Cabin

                                                    var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.Equipment._AirEquipType;

                                                    var TechStopLoc = '';
                                                    var TechStopDept = '';
                                                    var TechStopArri = '';
                                                    if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation != null) {
                                                        TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._LocationCode;
                                                        TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._DepartureDateTime;
                                                        TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation._ArrivalDateTime;
                                                        if (TechStopLoc == undefined) {
                                                            TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._LocationCode;
                                                            TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._DepartureDateTime;
                                                            TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                        }
                                                    }

                                                    ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                                }
                                                if (FlightSegCount >= 2) {
                                                    var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                                    var Stops = Stops + '-stop'
                                                }
                                                else {
                                                    var Stops = 'Non-stop'
                                                }
                                                ShowFlightResultGo(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                            }
                                        }
                                    }
                                    else {
                                        //Single Flight Segments
                                        var ElapsedTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(0, 2) + 'h ' + jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption._ElapsedTime.toString().substr(2, 2) + 'm';

                                        var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                        var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                        var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                        var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                        DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                        ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                        var FlightSegCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                        if (FlightSegCount != null) {
                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0].DepartureAirport._LocationCode;
                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1].ArrivalAirport._LocationCode;

                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                            var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[0]._DepartureDateTime;
                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;
                                            var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[FlightSegCount - 1]._ArrivalDateTime;

                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                            for (var i9 = 0; i9 < jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
                                                var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].MarketingAirline._Code;
                                                var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].OperatingAirline._Code;
                                                var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._FlightNumber;
                                                var From = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                                var DepartureDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._DepartureDateTime;
                                                var To = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                                var ArrivalDateTime = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime;

                                                var DeptTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._Terminal;
                                                var ArriTerminal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._Terminal;

                                                var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages
                                                if (lugaage != undefined) {
                                                    if (lugaage.length != null) {
                                                        for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                            var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Index;
                                                            var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages[ilugg].Baggage._Type;

                                                        }

                                                    }
                                                    else {
                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOptio.FlightSegment[i9].Baggages.Baggage._Index;
                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Baggages.Baggage._Type;
                                                    }
                                                }
                                                else {
                                                    var bagonload = 'undefined';
                                                }

                                                DepartureDateTime = moment(DepartureDateTime).format("DD MMM, HH:mm");
                                                ArrivalDateTime = moment(ArrivalDateTime).format("DD MMM, HH:mm");

                                                //Layover Time
                                                var LayoverTimee = '';
                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length - 1 >= i9 + 1) {
                                                    var FirstArrivalTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9]._ArrivalDateTime);
                                                    var NextDeptTime = moment(jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9 + 1]._DepartureDateTime);

                                                    var DifferenceMilli = NextDeptTime - FirstArrivalTime;
                                                    var DifferenceMilliDuration = moment.duration(DifferenceMilli);
                                                    LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
                                                }
                                                //Layover Time

                                                //Cabin
                                                var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail.length;
                                                if (BookingClassAvailCount != null) {
                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                                }
                                                else {
                                                    var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                    var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                                }
                                                //Cabin

                                                var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].Equipment._AirEquipType;

                                                var TechStopLoc = '';
                                                var TechStopDept = '';
                                                var TechStopArri = '';
                                                if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation != null) {
                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._LocationCode;
                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._DepartureDateTime;
                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation._ArrivalDateTime;
                                                    if (TechStopLoc == undefined) {
                                                        TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._LocationCode;
                                                        TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._DepartureDateTime;
                                                        TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].StopLocation[0]._ArrivalDateTime;
                                                    }
                                                }

                                                ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, From, DepartureDateTime, To, ArrivalDateTime, DeptTerminal, ArriTerminal, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                            }
                                        }
                                        else {
                                            var FromGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                            var ToGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;


                                            var DepTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                            var DepTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._DepartureDateTime;
                                            var ArriTimeGo = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;
                                            var ArriTimeGoOriginal = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._ArrivalDateTime;

                                            DepTimeGo = moment(DepTimeGo).format("DD MMM, HH:mm");
                                            ArriTimeGo = moment(ArriTimeGo).format("DD MMM, HH:mm");

                                            CreateLegTitle(DepTimeGoOriginal, FromGo, ToGo, FlightSegCount);


                                            var MarketingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.MarketingAirline._Code;
                                            var OperatingAirline = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.OperatingAirline._Code;
                                            var FlightNumber = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment._FlightNumber;

                                            var lugaage = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages
                                            if (lugaage != undefined) {
                                                if (lugaage.length != null) {
                                                    for (ilugg = 0; ilugg < lugaage.length; ilugg++) {
                                                        var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Index;
                                                        var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages[ilugg].Baggage._Type;

                                                    }

                                                }
                                                else {
                                                    var bagonload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Index;
                                                    var bagoPaxload = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Baggages.Baggage._Type;
                                                }
                                            }
                                            else {
                                                var bagonload = 'undefined';
                                            }

                                            var DeptTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._Terminal;
                                            var ArriTerminalSing = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._Terminal;

                                            var LayoverTimee = '';

                                            //Cabin
                                            var BookingClassAvailCount = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail.length;
                                            if (BookingClassAvailCount != null) {
                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCabinCode;
                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail[0]._ResBookDesigCode;
                                            }
                                            else {
                                                var ResBookDesigCabinCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCabinCode;
                                                var ResBookDesigCode = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.BookingClassAvails.BookingClassAvail._ResBookDesigCode;
                                            }
                                            //Cabin

                                            var AirEquipType = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.Equipment._AirEquipType;

                                            var TechStopLoc = '';
                                            var TechStopDept = '';
                                            var TechStopArri = '';
                                            if (jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation != null) {
                                                TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._LocationCode;
                                                TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._DepartureDateTime;
                                                TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation._ArrivalDateTime;
                                                if (TechStopLoc == undefined) {
                                                    TechStopLoc = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._LocationCode;
                                                    TechStopDept = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._DepartureDateTime;
                                                    TechStopArri = jsonn.AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.StopLocation[0]._ArrivalDateTime;
                                                }
                                            }

                                            ShowFlightItenaryGo(jsonn._SequenceNumber, MarketingAirline, FlightNumber, FromGo, DepTimeGo, ToGo, ArriTimeGo, DeptTerminalSing, ArriTerminalSing, LayoverTimee, OperatingAirline, ResBookDesigCabinCode, ResBookDesigCode, AirEquipType, TechStopLoc, TechStopDept, TechStopArri, bagonload);
                                        }
                                        if (FlightSegCount >= 2) {
                                            var Stops = parseFloat(FlightSegCount / 2).toPrecision(1);
                                            var Stops = Stops + '-stop'
                                        }
                                        else {
                                            var Stops = 'Non-stop'
                                        }
                                        ShowFlightResultGo(jsonn._SequenceNumber, ElapsedTime, ValidatingAirlineCode, FromGo, ToGo, DepTimeGo, ArriTimeGo, Stops, jsonn._Currency, jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, FlightSegCount, DepTimeGoOriginal, ArriTimeGoOriginal);
                                    }
                                }
                            }
                        }

                        //Generation Starts Here
                        var Combinations = '';
                        if (One != null) {
                            Combinations = '<div class="hide_area_show" id="HideComb_' + jsonn._SequenceNumber + '">' + One + '</div>';
                        }
                        var MobileValAirlineIMG = '';
                        var MainLegValAirLineUNIQ = _.uniq(MainLegValAirLine);
                        for (var ivalairline = 0; ivalairline < MainLegValAirLineUNIQ.length; ivalairline++) {
                            MobileValAirlineIMG += "<img src='" + airlinelogopath + MainLegValAirLineUNIQ[ivalairline] + ".gif' alt='" + MainLegValAirLineUNIQ[ivalairline] + "' title='" + MainLegValAirLineUNIQ[ivalairline] + "'>";
                        }
                        $('#divFLSearch').append('<div data-show="0" class="show_more_visible" id="show_more_visible_seq_' + jsonn._SequenceNumber + '_0' +
                            '" data-price="' + CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '" data-duration="' + TotalLegsDuration + '" data-dept="' + DepartureTimeeeeFirstLeg + '" data-arri="' + ArrivalTimeeeeFirstLeg + '"><!----flight details 1 and popup------><div class="search_details sp07" id="more_info_' + jsonn._SequenceNumber +
                            '"><div class="flights_left_area">' + FirstResultFlightResult + '</div> <div class="flight_rate_area"> <div class="rate_area01"> <h4>'
                            + jsonn._Currency + ' ' + CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> ' + ButtonSelect + ' <a class="showpop" id="showpop_'
                            + jsonn._SequenceNumber + '">More info</a></div> <div id="popup_' + jsonn._SequenceNumber + '" class="simplePopup"></div> <!-----Mobile view area-------> <div class="mb_flight_rate_area">  <div class="mb_rate_right"> ' + ButtonSelect + ' <div class="rate_area02">  <h4>'
                            + jsonn._Currency + ' ' + CalcMarkup(jsonn.AirItineraryPricingInfo.ItinTotalFare.TotalFare._Amount, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h4> </div> </div> <div class="mb_more"> <a class="showpop" id="showpop_'
                            + jsonn._SequenceNumber + '">More info</a> </div> </div> <!-----Mobile view area-------> </div> <div id="more_info_popup_' + jsonn._SequenceNumber +
                            '" class="more_info_searchresultpopup popuphiddenfield"><a class="closetb"><i class="fa fa-window-close" aria-hidden="true"></i></a> <div class="tabs animated-fade"> <ul class="tab-links"> <li class="active"><a href="#tab1_'
                            + jsonn._SequenceNumber + '_1">Flight itinerary </a></li> <li><a href="#tab1_' + jsonn._SequenceNumber + '_2">Fare details</a></li> </ul> <div class="tab-content"> <div id="tab1_' + jsonn._SequenceNumber +
                            '_1" class="tab active">' + FirstResultFlightSegments + '</div> <div id="tab1_' + jsonn._SequenceNumber + '_2" class="tab"> <div class="fare_details"> <div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + PaxCountData + '</h3> </div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Fare</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                            + jsonn._Currency + ' ' + BaseFareTotal + '</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Taxes & Fees</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                            + jsonn._Currency + ' ' + CalcMarkupTax(TotalFare, TaxTotal, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Total</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>'
                            + jsonn._Currency + ' ' + CalcMarkup(TotalFare, jsonn.AirItineraryPricingInfo.ItinTotalFare.MarkupFare._Amount) + '</h2> </div> </li> </ul> </div> <div class="right_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1></h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2></h2> </div> </li> </ul> </div> </div> </div> </div> </div></div></div>' + Combinations + '' + ShowMoreCombiButton + '</div>');
                    }
                }
                else {
                    console.log('Error - OWC');
                    //var OWC = jsonObj.Envelope.Body.SearchFlightResponse.OTA_AirLowFareSearchRS.PricedItineraries.PricedItineraryForOWC;
                    var MinAmount = 0;
                    var MaxAmount = 1;
                    var CurrencyFirst = '';
                    $("#divNoFlight").show();
                }
            }
            //Result Generation End
            $('[data-toggle="tooltip"]').tooltip();
            //Filters Start

            //Price Slider Start
            if ($('#property-price-range')[0]) {
                //MaxAmount = parseFloat(MaxAmount) + 1;
                var propertyPriceRange = document.getElementById('property-price-range');
                var propertyPriceRangeValues = [
                    document.getElementById('property-price-upper'),
                    document.getElementById('property-price-lower')
                ]
                noUiSlider.create(propertyPriceRange, {
                    start: [parseFloat(MinAmount), parseFloat(MaxAmount)],
                    connect: true,
                    range: {
                        'min': parseFloat(MinAmount),
                        'max': parseFloat(MaxAmount)
                    }
                });
                propertyPriceRange.noUiSlider.on('update', function (values, handle) {
                    propertyPriceRangeValues[handle].innerHTML = values[handle];
                    showProducts(values[0], values[1]);
                });
            }
            function showProducts(minPrice, maxPrice) {
                $("#divFLSearch .show_more_visible").hide().filter(function () {
                    var price = parseInt($(this).data("price"), 10);
                    var dshow = $(this).attr("data-show");
                    if (dshow == '0') {
                        return price >= minPrice - 1 && price <= maxPrice + 1;
                    }
                    else {
                        return false;
                    }
                }).show();
                ShowNoResultsFilterChange();
            }
            //Price Slider End


            //Fare Details Click
            $(".clickmorefaredet").click(function () {
                var temp = $(this).attr("id");
                temp = temp.replace("clickfaredet", "downfaredet");
                $('#' + temp).slideToggle("fast");
            });
            //More Combi Click
            $(".clickmorecombi").click(function () {
                var temp = $(this).attr("id");
                temp = temp.replace("clickcombi", "downcombi");
                $('#' + temp).slideToggle("fast");
            });
            //More Combi Seg Click
            $(".clickmorecombiseg").click(function () {
                var temp = $(this).attr("id");
                temp = temp.replace("clickCombiSeg", "downcombiseg");
                $('#' + temp).slideToggle("fast");
            });
            //More Combi Fare det Click
            $(".clickmorefaredetCombi").click(function () {
                var temp = $(this).attr("id");
                temp = temp.replace("clickfaredetCombi", "downfaredetCombi");
                $('#' + temp).slideToggle("fast");
            });
            //More Baggage Info
            $(".clickmoreBaggageInfoo").click(function () {
                var temp = $(this).attr("id");
                temp = temp.replace("clickBaggageInfo", "downBaggageInfo");
                $('#' + temp).slideToggle("fast");
            });

            //No Image Load
            $(".AirlineLogos img, .hotelImgesItenary img")
          .error(function () {
              $(this).attr("src", "images/flightnoimage.png");
          })

            //Price Sort
            $(".PriceFilterClick").click(function () {
                //tinysort('ul#itemContainer>li', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: 'data-price' });
                tinysort('div#divFLSearch>div', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: 'data-price' });
            });

            //Duration Sort
            $(".DurationFilterClick").click(function () {
                tinysort('div#divFLSearch>div', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: 'data-duration' });
            });

            //Dept Time Sort
            $(".DepartureTimeFilterClick").click(function () {
                tinysort('div#divFLSearch>div', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: 'data-dept' });
            });

            //Arri Time Sort
            $(".ArrivalTimeFilterClick").click(function () {
                tinysort('div#divFLSearch>div', { order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc', attr: 'data-arri' });
            });

            //NEW Start
            $(".profile_view").click(function () {
                $(".profile_view_toggle").toggle();
            });
            $(".showtravel_area").click(function () {
                $(".travel_details_show").toggle();
            });
            //$(".show_more_details").click(function () {
            //});

            $('.show1').click(function () {
                $('#pop1').simplePopup();
                document.getElementById('summary_search_field').style.display = "none";
                document.getElementById('modify_search_field').style.display = "block";
                document.getElementById('modify_close_btn').style.display = "block";
                $('.summary_popup').css("z-index", "9999999");
                $('.search_details').css("z-index", "9999");
                $('#sort_popup').css("z-index", "9999");

            });


            $('.show2').click(function () {


                $('#pop2').simplePopup();
                $('#sort_popup').css("z-index", "99999999999999");
                document.getElementById('sort_popup').style.display = "block";
                $('html, body').animate({ scrollTop: 0 }, "slow");




                //      $('.search_details').css("z-index", "99");
                //$('#more_info_popup').css("z-index", "999999");
            });


            $('.showpop').click(function () {
                var tempID = $(this).attr("id");
                tempID = tempID.replace("showpop_", "");
                $('#popup_' + tempID).simplePopup();

                $('#more_info_popup_' + tempID).css("z-index", "9999999");
                $('#more_info_' + tempID).css("z-index", "9999999");

                //$('.summary_popup').css("z-index", "9999");
                document.getElementById('more_info_' + tempID).style.display = "block";
                document.getElementById('more_info_popup_' + tempID).style.display = "block";

                //$(this).toggleClass("showpop01 simplePopupClose");
                //var HTML = $(this).html();
                //if (HTML == "More info") {
                //    $(this).html("Hide info");
                //} else {
                //    $(this).html("More info");
                //    //var ID = $(this).attr("id");
                //    //ID = ID.replace("showpop_", "more_info_popup_");
                //    //$("#" + ID).css("display", "none");
                //    //$("#" + ID).css("z-index", "9999");
                //    //$(".simplePopupBackground").css("display", "none");

                //}
            });

            $('.show_more_details').click(function () {
                var remcount = $(this).attr("data-rem");
                var hidestatus = $(this).attr("data-hide");
                var seqid = $(this).attr("id");
                seqid = seqid.replace("ShowCombi_", "HideComb_");
                $('#' + seqid).toggle();
                $(this).parent('div').toggleClass("show_more_visible show_more_visible_withborder");
                $(this).find("i").toggleClass("fa-plus-circle fa-minus-circle");

                //var DATA = $(this).find("span").html();
                ////  $(this).find("span").toggle("more options with same price hide options with same price");
                //if (DATA == "show " + remcount + " more options") {
                //    $(this).find("span").html("Hide more options");
                //} else {
                //    $(this).find("span").html("show " + remcount + " more options");
                //}

                if (hidestatus == "0") {
                    $(this).find("span").html("Hide more options");
                    $(this).attr("data-hide", 1);
                } else {
                    $(this).find("span").html("show " + remcount + " more options");
                    $(this).attr("data-hide", 0);
                }
            });


            // Standard
            jQuery('.tabs.standard .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            // Animated Fade
            jQuery('.tabs.animated-fade .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            // Animated Slide 1
            jQuery('.tabs.animated-slide-1 .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
                jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            // Animated Slide 2
            jQuery('.tabs.animated-slide-2 .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });
            //NEW End

            $(function () {
                var Accordion = function (el, multiple) {
                    this.el = el || {};
                    this.multiple = multiple || false;

                    // Variables privadas
                    var links = this.el.find('.link');
                    // Evento
                    links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
                }

                Accordion.prototype.dropdown = function (e) {
                    var $el = e.data.el;
                    $this = $(this),
                    $next = $this.next();

                    $next.slideToggle();
                    $this.parent().toggleClass('open');

                    if (!e.data.multiple) {
                        $el.find($(this) + '.submenu').not($next).slideUp().parent().removeClass('open');
                    };
                }

                var accordion = new Accordion($(this), false);
            });
            $(".loadingGIF").hide();
            $('#sort_popup').css("z-index", "999999");

            //Stops Filter
            GenerateStopsFilter();
            //Airline Filter
            GenerateAirlineFilter();
            //Dept TIme Filter
            GenerateDeptTimeFilterOptions();
            //Arrival Time Filter
            GenerateArrivalTimeFilterOptions();
            //Click
            $(".AirlineeChk").click(function () {
                var IDCHK = $(this).attr("id");
                var data_seqs = $(this).attr("data-seq");
                var data_seqsArray = data_seqs.split(',');
                for (var iseqonly = 0; iseqonly < data_seqsArray.length; iseqonly++) {
                    var liblockID = data_seqsArray[iseqonly];
                    //var DataShowValue = parseInt($("#res" + liblockID).attr("data-show"));
                    var DataShowValue = parseInt($("#show_more_visible_seq_" + liblockID + "_0").attr("data-show"));
                    var DataShowValuePlus = parseInt(0);
                    //console.log(DataShowValuePlus);
                    if (document.getElementById(IDCHK).checked == true) {
                        if (DataShowValue == 0) {
                            DataShowValuePlus = 0;
                        }
                        else {
                            DataShowValuePlus = DataShowValue - 1;
                        }
                    }
                    else {
                        DataShowValuePlus = DataShowValue + 1;
                    }
                    //console.log(DataShowValuePlus);
                    $("#show_more_visible_seq_" + liblockID + "_0").attr("data-show", DataShowValuePlus);
                }
                ShowLIBlocks();
            });
            $(".shastops").click(function () {
                var IDCHK = $(this).attr("id");
                var data_seqs = $(this).attr("data-seq");
                var data_seqsArray = data_seqs.split(',');
                //console.log(data_seqsArray);
                for (var iseqonly = 0; iseqonly < data_seqsArray.length; iseqonly++) {
                    var liblockID = data_seqsArray[iseqonly];
                    var DataShowValue = parseInt($("#show_more_visible_seq_" + liblockID + "_0").attr("data-show"));
                    var DataShowValuePlus = parseInt(0);
                    //console.log(DataShowValuePlus);
                    if (document.getElementById(IDCHK).checked == true) {
                        if (DataShowValue == 0) {
                            DataShowValuePlus = 0;

                        }
                        else {
                            DataShowValuePlus = DataShowValue - 1;
                        }
                    }
                    else {
                        DataShowValuePlus = DataShowValue + 1;
                    }
                    //console.log(DataShowValuePlus);
                    $("#show_more_visible_seq_" + liblockID + "_0").attr("data-show", DataShowValuePlus);
                }
                ShowLIBlocks();
            });
            $(".DeptTime").click(function () {
                var IDCHK = $(this).attr("id");
                var data_seqs = $(this).attr("data-seq");
                var data_seqsArray = data_seqs.split(',');
                //console.log(data_seqsArray);
                for (var iseqonly = 0; iseqonly < data_seqsArray.length; iseqonly++) {
                    var liblockID = data_seqsArray[iseqonly];
                    var DataShowValue = parseInt($("#show_more_visible_seq_" + liblockID + "_0").attr("data-show"));
                    var DataShowValuePlus = parseInt(0);
                    //console.log(DataShowValuePlus);
                    if (document.getElementById(IDCHK).checked == true) {
                        if (DataShowValue == 0) {
                            DataShowValuePlus = 0;
                        }
                        else {
                            DataShowValuePlus = DataShowValue - 1;
                        }
                    }
                    else {
                        DataShowValuePlus = DataShowValue + 1;
                    }
                    //console.log(DataShowValuePlus);
                    $("#show_more_visible_seq_" + liblockID + "_0").attr("data-show", DataShowValuePlus);
                }
                ShowLIBlocks();
            });
            $(".ArrivalTime").click(function () {
                var IDCHK = $(this).attr("id");
                //alert(IDCHK);
                var data_seqs = $(this).attr("data-seq");
                //alert(data_seqs);
                var data_seqsArray = data_seqs.split(',');
                for (var iseqonly = 0; iseqonly < data_seqsArray.length; iseqonly++) {
                    var liblockID = data_seqsArray[iseqonly];
                    var DataShowValue = parseInt($("#show_more_visible_seq_" + liblockID + "_0").attr("data-show"));
                    var DataShowValuePlus = parseInt(0);
                    if (document.getElementById(IDCHK).checked == true) {
                        if (DataShowValue == 0) {
                            DataShowValuePlus = 0;
                        }
                        else {
                            DataShowValuePlus = DataShowValue - 1;
                        }
                    }
                    else {
                        DataShowValuePlus = DataShowValue + 1;
                    }
                    $("#show_more_visible_seq_" + liblockID + "_0").attr("data-show", DataShowValuePlus);
                }
                //alert(DataShowValuePlus);
                ShowLIBlocks();
            });
            //setTimeout(function () {
            //}, 3000);
            airlinefilteronload();
            directflightsfilteronload();
            finalizeonlyshowclick();
            finalizeshowshowallclick();
        }
    }

    //NEW SHABEER
    $('#topFilterUL li').click(function (e) {
        e.preventDefault();
        $('li').removeClass('active');
        $(this).addClass('active');
    });
}

function ShowLIBlocks() {
    var MinAmount = parseFloat($("#property-price-upper").html());
    if (MinAmount === 'NaN') { MinAmount = parseFloat(0); }
    var MaxAmount = parseFloat($("#property-price-lower").html());
    if (MaxAmount === 'NaN') { MaxAmount = parseFloat(1); }

    $("#divFLSearch .show_more_visible").each(function () {
        var DataShowValue = $(this).attr("data-show");
        var tempprice = parseFloat($(this).data("price"), 10);
        var ID = $(this).attr("id");
        if (ID != "-1" && isNaN(tempprice) == false) {
            //alert(ID + '--' + DataShowValue);
            if (tempprice >= MinAmount && tempprice <= MaxAmount) {
                if (DataShowValue == 0) {
                    //alert('show me ' + ID + '--' + DataShowValue);
                    $('#' + ID).show();
                }
                else {
                    //alert('hide  me ' + ID + '--' + DataShowValue);
                    $('#' + ID).hide();
                }
            }
            else { $('#' + ID).hide(); }
        }
    });
    ShowNoResultsFilterChange();
}
function ShowNoResultsFilterChange() {
    var numres = $("#divFLSearch .show_more_visible").filter(function () { return $(this).css('display') !== 'none'; }).length;
    console.log('num res-' + numres)
    if (numres == '0') {
        $("#divNoFlight").html('<div class="box-content"> <h2>No flights !</h2> </div>');
        $("#divNoFlight").show();
    }
    else {
        $("#divNoFlight").hide();
    }
}
//First Result Functions

function CreateButton(seqnoo, combid, indexlist) {
    ButtonSelect = '<input id="btnSubmit" type="button" class="more_search" onclick="selectflight(' + seqnoo + ',' + combid + ',/'
        + indexlist + '/)" value="Select">';

    //ButtonGetBaggage = '<a data-open="0" class="showMore01New clickmoreBaggageInfo" onclick="GetBaggageInfoClick(' + seqnoo + ',' + combid + ')" id="clickBaggageInfo' + seqnoo + '">Baggage Info</a>';
    ButtonGetBaggage = '<div id="tab1_' + seqnoo + '_3" class="tab"> <div id="divBindBagInfo_' + seqnoo + '_' + combid + '" class="fare_details">' + LoadingGIFBag + '</div></div>';
    ButtonGetFareRules = '<a data-open="0" class="showMore01New clickmorefaredet" onclick="GetFareInfoClick(' + seqnoo + ',' + combid + ')"  id="clickfaredet' + seqnoo + '">Fare details</a>';
}

function CreateLegTitle(deptdatleg, fromleg, toleg, FlightSegCountt) {
    var deptdatlegg = moment(deptdatleg, ['YYYY-MM-DDTHH:mm:ss']).format("dddd, DD-MM-YYYY, HH:mm");
    if (FlightSegCountt >= 2) {
        var Stopst = parseFloat(FlightSegCountt / 2).toPrecision(1);
        var Stopst = Stopst + '-stop'
    }
    else {
        var Stopst = 'Non-stop'
    }
    LegTitle = '<div class="itinerary_head"> <h2><i class="fa fa-plane" aria-hidden="true"></i>' + getAirportNameUTC(fromleg) + ' to ' + getAirportNameUTC(toleg) + ' <span>(' + Stopst + ')</span></h2><h4><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(fromleg) + ' to ' + getCityNameUTC(toleg) + ' <span>(' + Stopst + ')</span></h4></div>';
}

function CreateLegTitleCombi(deptdatlegCombi, fromlegCombi, tolegCombi, FlightSegCountt) {
    var deptdatlegCombii = moment(deptdatlegCombi, ['YYYY-MM-DDTHH:mm:ss']).format("dddd, DD-MM-YYYY, HH:mm");
    if (FlightSegCountt >= 2) {
        var Stopstz = parseFloat(FlightSegCountt / 2).toPrecision(1);
        var Stopstz = Stopstz + '-stop'
    }
    else {
        var Stopstz = 'Non-stop'
    }
    LegTitleCombi = '<div class="itinerary_head"> <h2><i class="fa fa-plane" aria-hidden="true"></i>' + getAirportNameUTC(fromlegCombi) + ' to ' + getAirportNameUTC(tolegCombi) + ' <span>(' + Stopstz + ')</span></h2><h4><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(fromlegCombi) + ' to ' + getCityNameUTC(tolegCombi) + ' <span>(' + Stopstz + ')</span></h4></div>';
}

function ShowFlightItenaryGo(SequenceNumber, AirlineCode, FlightNum, FromSeg, DeptTimeSeg, ToSeg, ArriTimeSeg, DepTer, ArriTer, LayOverTimess, OperatingAirlinee, ResBookDesigCabinCodee, ResBookDesigCodee, aireqp, TechStopLoc, TechStopDept, TechStopArri, bagonload) {

    var bagdatafull = '';
    if (bagonload == '' || bagonload == 'undefined') {
        bagdatafull = 'Not available';
    }
    else {
        bagdatafull = _.where(baggginfo, { "_Index": bagonload });
        //console.log(bagdatafull);
        if (bagdatafull == '') {
            if (baggginfo._Unit == "PC" && baggginfo._Quantity == "0") {
                bagdatafull = "Carry-on only";
            }
            else if (baggginfo._Unit == "PC") {
                bagdatafull = baggginfo._Quantity + " " + "Piece"
            }
            else {
                bagdatafull = baggginfo._Quantity + " " + baggginfo._Unit
            }
        }
        else {
            if (bagdatafull["0"]._Unit == "PC" && bagdatafull["0"]._Quantity == "0") {
                bagdatafull = "Carry-on only";
            }
            else if (bagdatafull["0"]._Unit == "PC") {
                bagdatafull = bagdatafull["0"]._Quantity + " " + "Piece"
            }
            else {
                bagdatafull = bagdatafull["0"]._Quantity + " " + bagdatafull["0"]._Unit
            }
        }
    }

    if (DepTer == null) {
        DepTer = '';
    }
    else {
        DepTer = 'Terminal ' + DepTer;
    }
    if (ArriTer == null) {
        ArriTer = '';
    }
    else {
        ArriTer = 'Terminal ' + ArriTer;
    }

    var LayOverDIV = '';
    if (LayOverTimess != '' && LayOverTimess != undefined) {
        LayOverDIV = '<div class="new_layover"> <h2 title="' + getAirportNameUTC(ToSeg) + '"><i class="fa fa-clock-o" aria-hidden="true"></i> Layover at ' + getCityNameUTCOnly(ToSeg) + ' | ' + LayOverTimess + '</h2></div>';
    }

    var OperatedByAirline = '';
    if (AirlineCode != OperatingAirlinee) {
        OperatedByAirline = '<p>(Operated by ' + getAirLineName(OperatingAirlinee) + ')</p>';
    }

    var TechStopDiv = '';
    if (TechStopLoc != '') {
        TechStopDiv = 'Technical stop at <span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(TechStopLoc) + '">' + getCityNameUTCOnly(TechStopLoc) + '</span> - ' + moment(TechStopDept).format("HH:mm") + ' to ' + moment(TechStopArri).format("HH:mm") + '';
    }



    FirstResultFlightSegments += '<div class="itinerary_details">' + LegTitle + '<div class="flight_itinerary"> <div class="col-sm-2 col-xs-12 text-left no_padding_left itinerary01"> <img src="'
        + airlinelogopath + AirlineCode + '.gif" class="img_itinerary"/> <h1>' + getAirLineName(AirlineCode) + '</h1> ' + OperatedByAirline + ' <h1>' + AirlineCode + '-' + FlightNum +
        '</h1> </div> <div class="col-xs-12 col-sm-7 no_padding_left mrg_btm"> <div class="flight_from left_txt"> <p>' + DeptTimeSeg.split(",")[1] + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(FromSeg) + '">'
        + FromSeg + '</span></p> <h4>' + DeptTimeSeg.split(",")[0] + '</h4> <h5>' + DepTer +
        '</h5> </div> <div class="flight_from sp08"> <div class="filghtImg02"> <p>' + GetFlightdurationtime(FromSeg, ToSeg, DeptTimeSeg, ArriTimeSeg) + '</p> <div class="line_flight01"> <span></span> </div> <p>' + TechStopDiv + '</p> </div> </div> <div class="flight_from txt_right01"> <p>'
        + ArriTimeSeg.split(",")[1] + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(ToSeg) + '">' + ToSeg + '</span></p> <h4>' + ArriTimeSeg.split(",")[0] + '</h4><h5>' + ArriTer +
        '</h5> </div> </div> <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_padding01"> <div class="itinerary_flight_details"> <p><span>Aircraft Type:</span> ' + getAirCraftName(aireqp) + '</p> <p><span>Cabin:</span> ' + getCabinClass(ResBookDesigCabinCodee) + '</p> <p><span>Cabin class:</span> ' + ResBookDesigCodee + '</p> <p><span>Check-in baggage:</span> ' + bagdatafull + '</p> <p><span>Carry-on:</span> 7 KG / Person</p> </div> </div> </div></div>' + LayOverDIV + '';

    LegTitle = '';
}
function ShowFlightResultGo(SeqNo, ElapTime, ValAirlineCode, Frm, Too, DeptTime, ArriTime, Stopp, Curr, Price, FlightSegCountt, DepTimeGoOriginals, ArriTimeGoOriginals) {
    var AirIMG = "<img src='" + airlinelogopath + ValAirlineCode + ".gif' alt='" + ValAirlineCode + "' title='" + ValAirlineCode + "'>";
    //AllAirlinesArray.push({ "ValAirlineCode": ValAirlineCode, "SeqNo": SeqNo });

    //if (_.findWhere(AllAirlinesArray1D, ValAirlineCode) == null) {
    //AllAirlinesArray1D.push(ValAirlineCode);
    //AllAirlinesArray1D = _.uniq(AllAirlinesArray1D);
    //}

    var stopps = parseInt(FlightSegCountt) - 1;
    if (isNaN(stopps)) {
        stopps = 0;
    }

    //AllStopsArray.push({ "stopps": stopps, "SeqNo": SeqNo });
    //AllStopsArray1D.push(stopps);

    MainLegValAirLine.push(ValAirlineCode);

    //FirstResultFlightResult += '<div stops="' + stopps + '" class="imgDtls ' + Stopp + '"> <div class="AirlineLogos"> '
    //    + AirIMG + ' <p>' + getAirLineName(ValAirlineCode) + '</p> </div> <div class="intl_time"> <p> <span>' + Frm +
    //    '</span> <span class="arrowdir"> <img src="images/arw01.png" /></span> <span>' + Too +
    //    '</span></p> <p class="pstyle"> <span><strong>' + DeptTime +
    //    '</strong></span> <span class="stop_time_info">' + ElapTime + ' | &rlm;' + Stopp + '</span> <span><strong>' + ArriTime +
    //    '</strong></span> </p> </div> </div>';

    FirstResultFlightResult += '<div class="flights_left_area01" d-stops="' + stopps + '" d-air="' + ValAirlineCode + '" d-from="' + Frm + '" d-dtime="' + DeptTime + '" d-to="' + Too + '" d-atime="' + ArriTime + '"> <div class="col-lg-4 col-sm-4 sm_padding sp03"> <div class="flight_details"> ' + AirIMG +
        ' <p>' + getAirLineName(ValAirlineCode) + '</p> <p></p> </div> </div> <div class="col-lg-4 sm_padding sp_view">' + AirIMG + '</div> <div class="col-lg-4 col-sm-4 sm_padding sp_view01"> <div class="flight_from"> <p>' + DeptTime.split(",")[1] +
        '</p> <p><span  data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(Frm) + '">' + Frm + '</span></p><h6> ' + DeptTime.split(",")[0] + '</h6> </div> <div class="flight_from sp04"> <div class="filghtImg01"> <p>' + ElapTime +
        '</p> <div class="line_flight"> <span></span> </div> <p>' + Stopp + '</p> </div> <div class="filghtImg"> <img src="/COMMON/images/flight_to.png"> </div> </div> <div class="flight_from txt_right"> <p>' + ArriTime.split(",")[1] +
        '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(Too) + '">' + Too + '</span></p><h6>' + ArriTime.split(",")[0] + '</h6> </div> </div> <div class="col-lg-2 col-sm-2 sm_padding sp03"> <div class="non_stop"> <i class="fa fa-clock-o" aria-hidden="true"></i> <p>' + ElapTime +
        '</p> <h5>' + Stopp + '</h5> </div> </div> <div class="col-lg-2 col-sm-2 sm_padding sp03"></div> </div>';

    generateDepartureFilterOptionsArray(SeqNo, Frm, DeptTime);
    generateArrivalFilterOptionsArray(SeqNo, Too, ArriTime);


    var ElapTimeFormat = ElapTime.replace("h ", ":");
    ElapTimeFormat = ElapTimeFormat.replace("m", "");
    TotalLegsDuration = TotalLegsDuration + moment.duration(ElapTimeFormat).asMinutes();

    //DeptTimeSort
    if (DepartureTimeeeeFirstLeg == '') {
        DepartureTimeeeeFirstLeg = moment(DepTimeGoOriginals, ['YYYY-MM-DDTHH:mm:ss']).format("YYYYMMDDHHmm");
    }
    if (ArrivalTimeeeeFirstLeg == '') {
        ArrivalTimeeeeFirstLeg = moment(ArriTimeGoOriginals, ['YYYY-MM-DDTHH:mm:ss']).format("YYYYMMDDHHmm");
    }
}

//Combination Result Functions

//Create Combination Button
function CreateMoreCombiButton(seqenumber, Count) {
    var RemCombiCount = parseInt(Count) - 1;
    //ShowMoreCombiButton = '<a class="showMore01New clickmorecombi" id="clickcombi' + seqenumber + '">' + RemCombiCount + ' More Options</a>';
    ShowMoreCombiButton = '<div id="ShowCombi_' + seqenumber + '" class="show_more_details" data-hide="0" data-rem="' + RemCombiCount + '"> <a class="more_show"><i class="fa fa-plus-circle fa-lg"></i><span class="change_txt">show ' + RemCombiCount + ' more options</span></a> </div>';
}

function CreateDivRightPrice(seqnoo, combid, indexlist, Curr, Price) {
    One += '<div class="flight_rate_area"> <div class="rate_area01"> <h4>'
                    + Curr + ' ' + Price + '</h4> </div> <input id="btnSubmit" type="button" class="more_search" onclick="selectflight(' + seqnoo + ',' + combid + ',/'
        + indexlist + '/)" value="Select"> <a class="showpop" id="showpop_'
                    + seqnoo + '">More info</a></div>';
}

function ShowFlightResultGoCombi(SeqNo, ElapTime, ValAirlineCode, Frm, Too, DeptTime, ArriTime, Stopp, Curr, Price) {

    var AirIMG = "<img src='" + airlinelogopath + ValAirlineCode + ".gif' alt='" + ValAirlineCode + "' title='" + ValAirlineCode + "'>";

    //FirstResultFlightResultCombi += '<div class="imgDtls"> <div class="AirlineLogos"> '
    //    + AirIMG + ' <p>' + getAirLineName(ValAirlineCode) + '</p> </div> <div class="intl_time"> <p> <span>' + Frm +
    //    '</span> <span class="arrowdir"> <img src="images/arw01.png" /></span> <span>' + Too +
    //    '</span></p> <p class="pstyle"> <span><strong>' + DeptTime +
    //    '</strong></span> <span class="stop_time_info">' + ElapTime + ' | &rlm;' + Stopp + '</span> <span><strong>' + ArriTime +
    //    '</strong></span> </p> </div> </div>';

    MainLegValAirLineCombi.push(ValAirlineCode);


    FirstResultFlightResultCombi += '<div class="flights_left_area01"> <div class="col-lg-4 col-sm-4 sm_padding sp03"> <div class="flight_details"> ' + AirIMG +
        ' <p>' + getAirLineName(ValAirlineCode) + '</p> <p></p> </div> </div><div class="col-lg-4 sm_padding sp_view">' + AirIMG + '</div><div class="col-lg-4 col-sm-4 sm_padding sp_view01"> <div class="flight_from"> <p>' + DeptTime.split(",")[1] +
        '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(Frm) + '">' + Frm + '</span></p><h6>' + DeptTime.split(",")[0] + '</h6> </div> <div class="flight_from sp04"> <div class="filghtImg01"> <p>' + ElapTime +
        '</p> <div class="line_flight"> <span></span> </div> <p>' + Stopp + '</p> </div> <div class="filghtImg"> <img src="/COMMON/images/flight_to.png"> </div> </div> <div class="flight_from txt_right"> <p>' + ArriTime.split(",")[1] +
        '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(Too) + '">' + Too + '</span></p><h6>' + ArriTime.split(",")[0] + '</h6> </div> </div> <div class="col-lg-2 col-sm-2 sm_padding sp03"> <div class="non_stop"> <i class="fa fa-clock-o" aria-hidden="true"></i> <p>' + ElapTime +
        '</p> <h5>' + Stopp + '</h5> </div> </div> <div class="col-lg-2 col-sm-2 sm_padding sp03"></div> </div>';
}

function ShowFlightItenaryGoCombi(SequenceNumber, AirlineCode, FlightNum, FromSeg, DeptTimeSeg, ToSeg, ArriTimeSeg, DepTer, ArriTer, LayOverTimessCombi, OperatingAirlinee, ResBookDesigCabinCodeee, ResBookDesigCodeee, aireqp, TechStopLoc, TechStopDept, TechStopArri, bagonload) {

    var bagdatafull = '';
    if (bagonload == '' || bagonload == 'undefined') {
        bagdatafull = 'Not available'
    }
    else {
        bagdatafull = _.where(baggginfo, { "_Index": bagonload });
        //console.log(bagdatafull);
        if (bagdatafull == '') {
            if (baggginfo._Unit == "PC" && baggginfo._Quantity == "0") {
                bagdatafull = "Carry-on only";
            }
            else if (baggginfo._Unit == "PC") {
                bagdatafull = baggginfo._Quantity + " " + "Piece"
            }
            else {
                bagdatafull = baggginfo._Quantity + " " + baggginfo._Unit
            }

        }
        else {
            if (bagdatafull["0"]._Unit == "PC" && bagdatafull["0"]._Quantity == "0") {
                bagdatafull = "Carry-on only";
            }
            else if (bagdatafull["0"]._Unit == "PC") {
                bagdatafull = bagdatafull["0"]._Quantity + " " + "Piece"
            }
            else {
                bagdatafull = bagdatafull["0"]._Quantity + " " + bagdatafull["0"]._Unit
            }
        }
    }

    if (DepTer == null) {
        DepTer = '';
    }
    else {
        DepTer = 'Terminal ' + DepTer;
    }
    if (ArriTer == null) {
        ArriTer = '';
    }
    else {
        ArriTer = 'Terminal ' + ArriTer;
    }

    var LayOverDIVCombi = '';
    if (LayOverTimessCombi != '' && LayOverTimessCombi != undefined) {
        LayOverDIVCombi = '<div class="new_layover"> <h2 title="' + getAirportNameUTC(ToSeg) + '"><i class="fa fa-clock-o" aria-hidden="true"></i>  Layover at ' + getCityNameUTCOnly(ToSeg) + ' | ' + LayOverTimessCombi + '</h2></div>';
    }
    else {
        //LayOverDIVCombi = '<div class="devider"><hr></div>';
    }

    var OperatedByAirline = '';
    if (AirlineCode != OperatingAirlinee) {
        OperatedByAirline = '<p>(Operated by ' + getAirLineName(OperatingAirlinee) + ')</p>';
    }

    var TechStopDiv = '';
    if (TechStopLoc != '') {
        TechStopDiv = 'Technical stop at <span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(TechStopLoc) + '">' + getCityNameUTCOnly(TechStopLoc) + '</span> - ' + moment(TechStopDept).format("HH:mm") + ' to ' + moment(TechStopArri).format("HH:mm") + '';
    }



    FirstResultFlightSegmentsCombi += '<div class="itinerary_details">' + LegTitleCombi + '<div class="flight_itinerary"> <div class="col-sm-2 col-xs-12 text-left no_padding_left itinerary01"> <img src="'
        + airlinelogopath + AirlineCode + '.gif" class="img_itinerary"/> <h1>' + getAirLineName(AirlineCode) + '</h1> ' + OperatedByAirline + ' <h1>' + AirlineCode + '-' + FlightNum +
        '</h1> </div> <div class="col-xs-12 col-sm-7 no_padding_left mrg_btm"> <div class="flight_from left_txt"> <p>' + DeptTimeSeg.split(",")[1] + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(FromSeg) + '">'
        + FromSeg + '</span></p> <h4>' + DeptTimeSeg.split(",")[0] + '</h4> <h5>' + DepTer +
        '</h5> </div> <div class="flight_from sp08"> <div class="filghtImg02"> <p>' + GetFlightdurationtime(FromSeg, ToSeg, DeptTimeSeg, ArriTimeSeg) + '</p> <div class="line_flight01"> <span></span> </div> <p>' + TechStopDiv + '</p> </div> </div> <div class="flight_from txt_right01"> <p>'
        + ArriTimeSeg.split(",")[1] + '</p> <p><span data-toggle="tooltip" data-placement="top" title="' + getAirportNameUTC(ToSeg) + '">' + ToSeg + '</span></p> <h4>' + ArriTimeSeg.split(",")[0] + '</h4><h5>' + ArriTer +
        '</h5> </div> </div> <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_padding01"> <div class="itinerary_flight_details"> <p><span>Aircraft Type:</span> ' + getAirCraftName(aireqp) + '</p> <p><span>Cabin:</span> ' + getCabinClass(ResBookDesigCabinCodeee) + '</p> <p><span>Cabin class:</span> ' + ResBookDesigCodeee + '</p> <p><span>Check-in baggage: </span> ' + bagdatafull + '</p> <p><span>Carry-on: </span> 7 KG / Person</p></div> </div> </div></div>' + LayOverDIVCombi + '';

    LegTitleCombi = '';
}

//Select Flight
function selectflight(sequencenum, combinationid, indexlists) {
    $(".more_search").attr("disabled", true);
    var sessionidd = $('#hdsessionid').val();
    window.localStorage.setItem("hdSEQNO", sequencenum);
    window.localStorage.setItem("hdCID", combinationid);
    window.localStorage.setItem("hdIND", indexlists);
    window.localStorage.setItem("SessionIDDD", sessionidd);
    var jsonseq = _.where(jsonn, { _SequenceNumber: "" + sequencenum + "" });
    if (jsonseq == "") {
        jsonseq[0] = jsonn;
    }
    //window.localStorage.setItem("SelectedFlight", JSON.stringify(jsonseq));
    var Dastasseq = {
        SelectedFlightSEQ: JSON.stringify(jsonseq)
    }
    $.ajax({
        type: "POST",
        url: "Webservice.aspx/SaveFlightSequence",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SavedFlightSeq,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SavedFlightSeq(data) {
        window.location.href = "/FLIGHT/travellerdetails.html";
    }
}

//------------------Baggage----------------------
function GetBaggageInfoClick(seqnum, CombiIDs, thiss) {
    var datafare = $(thiss).attr("data-open");
    if (datafare == 0) {
        $(".loadingGIFBag").show();
        Getbaggagedata(seqnum, CombiIDs);
        $(thiss).attr("data-open", 1);
    }
}

//function Getbaggagedata(sequencenum, combinationid) {
//    var combiid = combinationid;
//    var frombaggseg = [];
//    var Tobaggseg = [];
//    var jsonseqbag = _.where(jsonn, { _SequenceNumber: "" + sequencenum + "" });
//    console.log(jsonseqbag);
//    if (jsonseqbag != null) {
//        if (jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length != null) {
//            for (var i4 = 0; i4 < jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length; i4++) {
//                var IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
//                var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;
//                if (combiid == Combi) {
//                    var strArr = IndexList.split(';');
//                    for (var i6 = 0; i6 < strArr.length; i6++) {
//                        var Index = strArr[i6];
//                        if (Index != null) {
//                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
//                                for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
//                                    if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
//                                        var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
//                                        if (FlightSegCount != null) {
//                                            //Multiple Flight Segments
//                                            for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
//                                                var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
//                                                var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
//                                                frombaggseg.push(From);
//                                                Tobaggseg.push(To);
//                                            }
//                                        }
//                                        else {
//                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
//                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;
//                                            frombaggseg.push(FromGo);
//                                            Tobaggseg.push(ToGo);
//                                        }
//                                    }
//                                }
//                            }
//                            else {
//                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
//                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
//                                    if (FlightSegCount != null) {
//                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
//                                            var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
//                                            frombaggseg.push(From);
//                                            var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
//                                            Tobaggseg.push(To);
//                                        }
//                                    }
//                                    else {
//                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
//                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
//                                        frombaggseg.push(FromGo);
//                                        Tobaggseg.push(ToGo);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        else {
//            IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._IndexList;
//            var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._CombinationID;
//            if (combiid = Combi) {
//                var strArr = IndexList.split(';');
//                for (var i6 = 0; i6 < strArr.length; i6++) {
//                    var Index = strArr[i6];
//                    if (Index != null) {
//                        if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
//                            for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
//                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
//                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
//                                    if (FlightSegCount != null) {
//                                        //Multiple Flight Segments
//                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
//                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
//                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
//                                            frombaggseg.push(FromGo);
//                                            Tobaggseg.push(ToGo);
//                                        }
//                                    }
//                                    else {
//                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
//                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;
//                                        frombaggseg.push(FromGo);
//                                        Tobaggseg.push(ToGo);
//                                    }
//                                }
//                            }
//                        }
//                        else {
//                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
//                                var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
//                                if (FlightSegCount != null) {
//                                    for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
//                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
//                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
//                                        frombaggseg.push(FromGo);
//                                        Tobaggseg.push(ToGo);
//                                    }
//                                }
//                                else {
//                                    var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
//                                    var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
//                                    frombaggseg.push(FromGo);
//                                    Tobaggseg.push(ToGo);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//    console.log(frombaggseg);
//    console.log(Tobaggseg);
//    var sessionidd = 'null';
//    var Dastas = {
//        sequencenu: sequencenum,
//        combinationi: combinationid,
//        session: sessionidd
//    }
//    $.ajax({
//        type: "POST",
//        url: "Webservice.aspx/Getbaggageinfo",
//        data: JSON.stringify(Dastas),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: populatebaggageinfo,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//    function populatebaggageinfo(data) {
//        var fullbaaageinfo = '';
//        var xmlbaggae = data.d;
//        var x2js1 = new X2JS();
//        var jsonbaggages = x2js1.xml_str2json(xmlbaggae);
//        console.log(jsonbaggages);
//        var errors = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.Errors;
//        if (errors != null) {
//            $('#divBindBagInfo_' + sequencenum + '_' + combinationid + '').append('<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>Baggage information not available !</h3></div>');
//        }
//        else {
//            var jsonnbaggage = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.FreeBaggageAllowances.FreeBaggageAllowance.FreeBaggageAllowanceTypes;
//            console.log(jsonnbaggage);
//            var checkedadt = "0";
//            var checkedchd = "0";
//            var checkedinf = "0";
//            if (jsonnbaggage.length != null) {
//                for (var j = 0, k = 0, l = 0; j < frombaggseg.length;) {
//                    for (var i = 0; i < jsonnbaggage.length; i++) {
//                        if (jsonnbaggage[i]._PassengerType == "ADT") {
//                            fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[j]) + ' to ' + getCityNameUTC(Tobaggseg[j]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
//                            j++;
//                        }
//                        else if (jsonnbaggage[i]._PassengerType == "CHD") {
//                            fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[k]) + ' to ' + getCityNameUTC(Tobaggseg[k]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
//                            k++;
//                        }
//                        else if (jsonnbaggage[i]._PassengerType == "INF") {
//                            fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[l]) + ' to ' + getCityNameUTC(Tobaggseg[l]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
//                            l++;
//                        }
//                    }
//                }
//            }
//            else {
//                if (jsonnbaggage._PassengerType == "ADT") {
//                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
//                }
//                else if (jsonnbaggage._PassengerType == "CHD") {
//                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
//                }
//                else if (item._PassengerType == "INF") {
//                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
//                }
//            }
//            $('#divBindBagInfo_' + sequencenum + '_' + combinationid + '').append(fullbaaageinfo);
//        }
//        $(".loadingGIFBag").hide();
//    }
//}

function Getbaggagedata(sequencenum, combinationid) {
    var combiid = combinationid;
    var frombaggseg = [];
    var Tobaggseg = [];
    var jsonseqbag = _.where(jsonn, { _SequenceNumber: "" + sequencenum + "" });
    //console.log(jsonseqbag);

    if (jsonseqbag != null) {

        if (jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length != null) {
            for (var i4 = 0; i4 < jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length; i4++) {
                var IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;
                if (combiid == Combi) {
                    var strArr = IndexList.split(';');
                    for (var i6 = 0; i6 < strArr.length; i6++) {
                        var Index = strArr[i6];
                        if (Index != null) {
                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                    if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                        var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                        if (FlightSegCount != null) {
                                            //Multiple Flight Segments
                                            for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                frombaggseg.push(From);
                                                Tobaggseg.push(To);
                                            }
                                        }
                                        else {
                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;
                                            frombaggseg.push(FromGo);
                                            Tobaggseg.push(ToGo);

                                        }
                                    }
                                }

                            }
                            else {
                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                    if (FlightSegCount != null) {
                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                            var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                            frombaggseg.push(From);

                                            var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                            Tobaggseg.push(To);
                                        }


                                    }
                                    else {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._IndexList;
            var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._CombinationID;
            if (combiid = Combi) {
                var strArr = IndexList.split(';');
                for (var i6 = 0; i6 < strArr.length; i6++) {
                    var Index = strArr[i6];
                    if (Index != null) {
                        if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                            for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                    if (FlightSegCount != null) {
                                        //Multiple Flight Segments
                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                            frombaggseg.push(FromGo);
                                            Tobaggseg.push(ToGo);
                                        }
                                    }
                                    else {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);
                                    }
                                }
                            }
                        }
                        else {
                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                if (FlightSegCount != null) {
                                    for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);
                                    }
                                }
                                else {
                                    var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                    var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
                                    frombaggseg.push(FromGo);
                                    Tobaggseg.push(ToGo);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //console.log(frombaggseg);
    //console.log(Tobaggseg);
    var sessionidd = 'null';
    var Dastas = {
        sequencenu: sequencenum,
        combinationi: combinationid,
        session: sessionidd
    }
    $.ajax({
        type: "POST",
        url: "Webservice.aspx/Getbaggageinfo",
        data: JSON.stringify(Dastas),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populatebaggageinfo,
        failure: function (response) {
            alert(response.d);
        }
    });
    function populatebaggageinfo(data) {
        var fullbaaageinfo = '';
        var xmlbaggae = data.d;
        var x2js1 = new X2JS();
        var jsonbaggages = x2js1.xml_str2json(xmlbaggae);
        //console.log(jsonbaggages);
        var errors = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.FreeBaggageAllowances.Errors;
        if (errors != null) {
            $('#divBindBagInfo_' + sequencenum + '_' + combinationid + '').append('<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>Baggage information not available !</h3></div>');
        }
        else {
            var jsonnbaggage = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.FreeBaggageAllowances.FreeBaggageAllowance.FreeBaggageAllowanceTypes;
            //console.log(jsonnbaggage);
            if (jsonnbaggage.length != null) {
                var startairport = '';
                for (var j = 0, k = 0, l = 0; j < frombaggseg.length;) {
                    for (var i = 0; i < jsonnbaggage.length; i++) {
                        if (jsonnbaggage[i]._PassengerType == "ADT") {
                            if (startairport != getCityNameUTC(frombaggseg[j])) {
                                startairport = getCityNameUTC(frombaggseg[j]);
                                fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[j]) + ' to ' + getCityNameUTC(Tobaggseg[j]) + '</h3></div> ';
                            }
                            //fullbaaageinfo += '   <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            fullbaaageinfo += '   <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Adult</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            j++;
                        }
                        else if (jsonnbaggage[i]._PassengerType == "CHD") {
                            if (startairport != getCityNameUTC(frombaggseg[k])) {
                                startairport = getCityNameUTC(frombaggseg[k]);
                                fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[k]) + ' to ' + getCityNameUTC(Tobaggseg[k]) + '</h3></div>';

                            }
                            //fullbaaageinfo += '<div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            fullbaaageinfo += '<div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Child</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            k++;
                        }
                        else if (jsonnbaggage[i]._PassengerType == "INF") {
                            if (startairport != getCityNameUTC(frombaggseg[l])) {
                                startairport = getCityNameUTC(frombaggseg[l]);
                                fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[l]) + ' to ' + getCityNameUTC(Tobaggseg[l]) + '</h3></div> ';
                            }
                            //fullbaaageinfo += '<div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage[i]._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            fullbaaageinfo += '<div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Infant</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage[i]._Quantity + ' ' + jsonnbaggage[i]._Unit + '</h2> </div> </li></ul></div></div>';
                            l++;
                        }
                    }
                }
            }
            else {
                if (jsonnbaggage._PassengerType == "ADT") {

                    //fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>' + jsonnbaggage._PassengerType + '</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Adult</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
                }
                else if (jsonnbaggage._PassengerType == "CHD") {
                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Child</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
                }
                else if (item._PassengerType == "INF") {
                    fullbaaageinfo += '<div class="fare_head"> <h3><i class="fa fa-plane" aria-hidden="true"></i>' + getCityNameUTC(frombaggseg[0]) + ' to ' + getCityNameUTC(Tobaggseg[0]) + '</h3></div> <div class="fare_cont"> <div class="left_fare"> <ul> <li> <div class="col-lg-6 col-sm-6 col-xs-6"> <h1>Infant</h1> </div> <div class="col-lg-6 col-sm-6 col-xs-6"> <h2>' + jsonnbaggage._Quantity + ' ' + jsonnbaggage._Unit + '</h2> </div> </li></ul></div></div>';
                }
            }
            $('#divBindBagInfo_' + sequencenum + '_' + combinationid + '').append(fullbaaageinfo);
        }
        $(".loadingGIFBag").hide();
    }
}


//Not Used
function GetbaggagedataMore(sequencenum, combinationid) {
    var combiid = combinationid;
    var frombaggseg = [];
    var Tobaggseg = [];
    var jsonseqbag = _.where(jsonn, { _SequenceNumber: "" + sequencenum + "" });
    //console.log(jsonseqbag);

    if (jsonseqbag != null) {

        if (jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length != null) {
            for (var i4 = 0; i4 < jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination.length; i4++) {
                var IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._IndexList;
                var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination[i4]._CombinationID;
                if (combiid == Combi) {
                    var strArr = IndexList.split(';');
                    for (var i6 = 0; i6 < strArr.length; i6++) {
                        var Index = strArr[i6];
                        if (Index != null) {
                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                                for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                    if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                        var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                        if (FlightSegCount != null) {
                                            //Multiple Flight Segments
                                            for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                                var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                                var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                                frombaggseg.push(From);
                                                Tobaggseg.push(To);


                                            }
                                        }
                                        else {
                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
                                            frombaggseg.push(FromGo);
                                            Tobaggseg.push(ToGo);

                                        }
                                    }
                                }

                            }
                            else {
                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                    if (FlightSegCount != null) {
                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {

                                            var From = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                            frombaggseg.push(From);

                                            var To = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                            Tobaggseg.push(To);
                                        }


                                    }
                                    else {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            IndexList = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._IndexList;
            var Combi = jsonseqbag[0].AirItinerary.OriginDestinationCombinations.OriginDestinationCombination._CombinationID;
            if (combinationid = Combi) {
                var strArr = IndexList.split(';');
                for (var i6 = 0; i6 < strArr.length; i6++) {
                    var Index = strArr[i6];
                    if (Index != null) {
                        if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length != null) {
                            for (var i7 = 0; i7 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.length; i7++) {
                                if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7]._DirectionId == i6) {
                                    var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length;
                                    if (FlightSegCount != null) {
                                        //Multiple Flight Segments
                                        for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.length; i9++) {
                                            var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].DepartureAirport._LocationCode;
                                            var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment[i9].ArrivalAirport._LocationCode;
                                            frombaggseg.push(FromGo);
                                            Tobaggseg.push(ToGo);
                                        }
                                    }
                                    else {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption[i7].FlightSegment.ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);

                                    }
                                }
                            }

                        }
                        else {
                            if (jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._RefNumber == Index && jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption._DirectionId == i6) {
                                var FlightSegCount = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length;
                                if (FlightSegCount != null) {
                                    for (var i9 = 0; i9 < jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.length; i9++) {
                                        var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].DepartureAirport._LocationCode;
                                        var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment[i9].ArrivalAirport._LocationCode;
                                        frombaggseg.push(FromGo);
                                        Tobaggseg.push(ToGo);
                                    }

                                }
                                else {
                                    var FromGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.DepartureAirport._LocationCode;
                                    var ToGo = jsonseqbag[0].AirItinerary.OriginDestinationOptions.OriginDestinationOption.FlightSegment.ArrivalAirport._LocationCode;
                                    frombaggseg.push(FromGo);
                                    Tobaggseg.push(ToGo);

                                }
                            }
                        }
                    }
                }
            }
        }

    }
    //console.log(frombaggseg);
    //console.log(Tobaggseg);


    var sessionidd = $('#hdsessionid').val();

    var Dastas = {
        sequencenu: sequencenum,
        combinationi: combinationid,
        session: sessionidd
    }
    $.ajax({
        type: "POST",
        url: "Webservice.aspx/Getbaggageinfo",
        data: JSON.stringify(Dastas),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populatebaggageinfo,
        failure: function (response) {
            alert(response.d);
        }
    });

    function populatebaggageinfo(data) {

        var xmlbaggae = data.d;
        //alert(xmlbaggae);
        var x2js1 = new X2JS();
        var jsonbaggages = x2js1.xml_str2json(xmlbaggae);
        //console.log(jsonbaggages);
        var errors = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.Errors;
        var fullbaaageinfo = '<div class="deviderMoreCombi"><hr></div><div class="imgDtls"><p><strong>Baggage Details</strong></p>';
        if (errors != null) {

            fullbaaageinfo = '<div class="deviderMoreCombi"><hr></div><div class="imgDtls"><p><strong>Baggage Details</strong></p>';
            fullbaaageinfo += "Baggage information not available";
            $('#downBaggagecombi' + sequencenum + combinationid + '').append(fullbaaageinfo + '<div class="deviderMoreCombi"><hr></div><div class="imgDtls">');
            $(".downBaggageInfodetcombi").show();

        }
        else {
            var jsonnbaggage = jsonbaggages.Envelope.Body.GetBaggageInfoResponse.BaggageInfoRS.FreeBaggageAllowances.FreeBaggageAllowance.FreeBaggageAllowanceTypes;


            //console.log(jsonnbaggage);
            fullbaaageinfo = '<div class="deviderMoreCombi"><hr></div><div class="imgDtls"><p><strong>Baggage Details</strong></p>';

            if (jsonnbaggage.length != null) {
                $.each(jsonnbaggage, function (i, item) {


                    if (item._PassengerType == "ADT") {
                        fullbaaageinfo += '<strong>' + item._PassengerType + '</strong>' + '<p>' + frombaggseg[i] + "--->" + Tobaggseg[i] + '</p><p>' + item._Quantity + " " + item._Unit + '</p>';

                    }

                    else if (item._PassengerType == "CHD") {
                        fullbaaageinfo += '<strong>' + item._PassengerType + '</strong>' + '<p>' + frombaggseg[i] + "--->" + Tobaggseg[i] + '</p><p>' + item._Quantity + " " + item._Unit + '</p>';
                    }

                    else if (item._PassengerType == "INF") {
                        fullbaaageinfo += '<strong>' + item._PassengerType + '</strong>' + '<p>' + frombaggseg[i] + "--->" + Tobaggseg[i] + '</p><p>' + item._Quantity + " " + item._Unit + '</p>';
                    }


                });
            }
            else {

                if (jsonnbaggage._PassengerType == "ADT") {
                    fullbaaageinfo += '<strong>' + jsonnbaggage._PassengerType + '</strong>' + '<p>' + frombaggseg[0] + "--->" + Tobaggseg[0] + '</p><p>' + jsonnbaggage._Quantity + " " + jsonnbaggage._Unit + '</p>';
                }

                else if (jsonnbaggage._PassengerType == "CHD") {
                    fullbaaageinfo += '<strong>' + jsonnbaggage._PassengerType + '</strong>' + '<p>' + frombaggseg[0] + "--->" + Tobaggseg[0] + '</p><p>' + jsonnbaggage._Quantity + " " + jsonnbaggage._Unit + '</p>';
                }

                else if (item._PassengerType == "INF") {
                    fullbaaageinfo += '<strong>' + jsonnbaggage._PassengerType + '</strong>' + '<p>' + frombaggseg[0] + "--->" + Tobaggseg[0] + '</p><p>' + jsonnbaggage._Quantity + " " + jsonnbaggage._Unit + '</p>';
                }

            }

            $('#downBaggagecombi' + sequencenum + combinationid + '').append(fullbaaageinfo + '</div><div class="deviderMoreCombi"><hr></div>');
            $(".downBaggageInfodetcombi").show();
        }
    }
}
function GetBaggageInfoClickMore(seqnum, CombiIDs) {
    if ($('#downBaggagecombi' + seqnum + CombiIDs + '').is(":hidden")) {
        var datafare = $('#clickBaggagecombi' + seqnum + CombiIDs + '').attr("data-open");
        if (datafare == 0) {
            $('#downBaggagecombi' + seqnum + CombiIDs).empty();
            GetbaggagedataMore(seqnum, CombiIDs);
            $('#clickBaggagecombi' + seqnum + CombiIDs + '').attr("data-open", 1);
            $('#downBaggagecombi' + seqnum + CombiIDs).slideToggle("fast");
        }
        else {
            $('#downBaggagecombi' + seqnum + CombiIDs).slideToggle("fast");
        }
    }
    else {
        $('#downBaggagecombi' + seqnum + CombiIDs).slideToggle("fast");
    }

}
//------------------Baggage----------------------

//Validate Passenger Data
function ValidatePassData() {
    if ($("#ddlCountry option:selected").val() == "-Select-") {
        alert("Please select country!");
        return false;
    }
}

function Filterbystops() {
    alert('funstart');
    $(".azr").each(function () {
        //var ID = $(this).attr("id");
        //if ($(".azr .Non-stop").length > 0) {
        //    $("#" + ID).toggle();
        //}
        alert('inside each itinerary');
        var ID = $(this).attr("id");
        alert(ID);
        $('#' + ID + " .imgDtls").each(function () {
            if ($(this).attr("stops") != null) {

            }
        });
    });
}

//Get Full Airport Name
function getAirportName(airportcode) {
    var AirportName = _.where(Airport_AllList, { a: airportcode, T: 'A' });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].label;
        //console.log(AirportName);
    }
    return AirportName;
}
function getCityNameUTC(airportcode) {
    var CityName = _.where(AirlinesTimezone, { I: airportcode });
    if (CityName == "") {
        CityName = airportcode;
    }
    else {
        CityName = CityName[0].C + ' (' + airportcode + ')';
    }
    return CityName;
}
function getCityNameUTCOnly(airportcode) {
    var CityName = _.where(AirlinesTimezone, { I: airportcode });
    if (CityName == "") {
        CityName = airportcode;
    }
    else {
        CityName = CityName[0].C;
    }
    return CityName;
}
function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}
//Get Full Airline Name
function getAirLineName(airlinecode) {
    var AirLineName = _.where(AirlinesDatas, { C: airlinecode });
    if (AirLineName == "") {
        AirLineName = airlinecode;
    }
    else {
        AirLineName = AirLineName[0].A;
        //console.log(AirportName);
    }
    return AirLineName;
}

//Get Cabin Class
function getCabinClass(cabin) {
    var Out = "";
    var Economy = _.contains(['B', 'H', 'K', 'L', 'M', 'N', 'Q', 'S', 'T', 'V', 'X', 'Y'], cabin);
    var PremiumEconomy = _.contains(['W', 'E'], cabin);
    var Business = _.contains(['C', 'D', 'J', 'Z'], cabin);
    var First = _.contains(['A', 'F', 'P', 'R'], cabin);

    if (Economy == true) {
        Out = "Economy";
    }
    else if (PremiumEconomy == true) {
        Out = "Premium Economy";
    }
    else if (Business == true) {
        Out = "Business";
    }
    else if (First == true) {
        Out = "First";
    }
    return Out;
}

//Get AirCraft Name
function getAirCraftName(aireqpcode) {
    var AirCraftName = _.where(AirplanesData, { C: aireqpcode });
    if (AirCraftName == "") {
        AirCraftName = aireqpcode;
    }
    else {
        AirCraftName = AirCraftName[0].N;
    }
    return AirCraftName;
}

//Generate Airline Filter
function GenerateAirlineFilter() {
    var AirlinesCheckBoxes = '';
    var airarray = [];
    var AllAirlinesArray = [];

    $("#divFLSearch .show_more_visible .search_details .flights_left_area .flights_left_area01").each(function () {
        var airleg = $(this).attr("d-air");
        if (airleg != undefined) {
            airarray.push(airleg);
            var divresid = $(this).parent().parent().parent().attr("id");
            divresid = divresid.replace('show_more_visible_seq_', '');
            divresid = divresid.replace('_0', '');
            //stopsarray.push(divresid + '-' + stopsleg);
            AllAirlinesArray.push({ "ValAirlineCode": airleg, "SeqNo": divresid });
        }
    });
    airarray = _.uniq(airarray);
    console.log(airarray)

    //console.log(AllAirlinesArray);
    if (airarray.length > 1) {
        $("#divAirliness").show();
        for (var iairline = 0; iairline < airarray.length; iairline++) {
            var eachAirlineCode = airarray[iairline];

            var SeqsWhere = _.where(AllAirlinesArray, { ValAirlineCode: eachAirlineCode });
            //console.log(SeqsWhere);

            var SeqNumbers = [];
            for (var iseqnum = 0; iseqnum < SeqsWhere.length; iseqnum++) {
                SeqNumbers.push(SeqsWhere[iseqnum].SeqNo);
            }
            var SeqNumbersUniq = _.uniq(SeqNumbers);
            //console.log(SeqNumbersUniq);

            //AirlinesCheckBoxes += '<div> <div class="mainChckArea"> <input data-seq="' + SeqNumbersUniq.join(",") + '" id="chkAirline'
            //    + AllAirlinesArrayUniq[iairline] +
            //    '" type="checkbox" value="' + AllAirlinesArrayUniq[iairline] + '" class="checkboxes azrtype_19 AirlineeChk" checked="checked"><label>'
            // + getAirLineName(AllAirlinesArrayUniq[iairline]) +
            //    '</label> </div> </div>';

            AirlinesCheckBoxes += '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq.join(",") + '" id="chkAirline'
                + airarray[iairline] +
                '" type="checkbox" value="' + airarray[iairline] + '" class="AirlineeChk" checked="checked"> <label for="web" class="formsfont">' + getAirLineName(airarray[iairline]) + '</label> <span>Only</span> </div>';
        }
        $('#AirlinesUL').append(AirlinesCheckBoxes);
    }
    else {
        $("#divAirliness").hide();
    }

}

//Generate Stops Filter
function GenerateStopsFilter() {
    var StopsCheckBoxes = '';
    var stopsarray = [];
    var dividstoparray = [];

    $("#divFLSearch .show_more_visible .search_details .flights_left_area .flights_left_area01").each(function () {
        var stopsleg = $(this).attr("d-stops");
        if (stopsleg != undefined) {
            stopsarray.push(stopsleg);
            var divresid = $(this).parent().parent().parent().attr("id");
            divresid = divresid.replace('show_more_visible_seq_', '');
            divresid = divresid.replace('_0', '');
            //stopsarray.push(divresid + '-' + stopsleg);
            dividstoparray.push({ "stopps": stopsleg, "SeqNo": divresid });
        }
    });
    stopsarray = _.uniq(stopsarray);
    console.log(stopsarray)

    if (stopsarray.length > 1) {
        $("#divStopss").show();
        for (var iairline = 0; iairline < stopsarray.length; iairline++) {
            var SeqsWhere = _.where(dividstoparray, { stopps: stopsarray[iairline] });
            var SeqNumbers = [];
            for (var iseqnum = 0; iseqnum < SeqsWhere.length; iseqnum++) {
                SeqNumbers.push(SeqsWhere[iseqnum].SeqNo);
            }
            var SeqNumbersUniq = _.uniq(SeqNumbers);
            if (SeqNumbersUniq.length != 0) {
                if (stopsarray[iairline] == 0) {
                    stopsarray[iairline] = 'Non -'
                }
                StopsCheckBoxes += '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq.join(",") + '" id="chkStops' + stopsarray[iairline] +
                    '" type="checkbox" value="' + stopsarray[iairline] + '" class="shastops" checked="checked"> <label for="web" class="formsfont">' + stopsarray[iairline] + ' stop</label> <span>Only</span> </div>';
            }
        }
        $('#StopsUL').append(StopsCheckBoxes);
    }
    else {
        $("#divStopss").hide();
    }
    StopsCheckBoxes = '';
    stopsarray = '';
    dividstoparray = '';
}

//Generate Dept Time Filter
function generateDepartureFilterOptionsArray(seqqno, frrm, deptttime) {
    AllDepartureTimeLocArray.push({ "seqqno": seqqno, "frrm": frrm, "deptttime": deptttime });
    //AllDepartureTimeArray1D.push(deptttime);
    //AllDepartureTimeArray1D = _.uniq(AllDepartureTimeArray1D);
    //AllDeparturePlaceArray1D.push(frrm);
    //AllDeparturePlaceArray1D = _.uniq(AllDeparturePlaceArray1D);
}

function GenerateDeptTimeFilterOptions() {
    var AllDepartureLocArrayUniq = [];
    var AllDepartureTimeArrayUniq = [];
    $("#divFLSearch .show_more_visible .search_details .flights_left_area .flights_left_area01").each(function () {
        var fromleg = $(this).attr("d-from");
        if (fromleg != undefined) {
            AllDepartureLocArrayUniq.push(fromleg);
        }
        var dtimeleg = $(this).attr("d-dtime");
        if (dtimeleg != undefined) {
            AllDepartureTimeArrayUniq.push(dtimeleg);
        }
    });

    AllDepartureTimeArrayUniq = _.uniq(AllDepartureTimeArrayUniq);
    AllDepartureLocArrayUniq = _.uniq(AllDepartureLocArrayUniq);
    console.log(AllDepartureTimeArrayUniq)
    console.log(AllDepartureLocArrayUniq)
    var DepartureTimeCheckBoxes = '';
    for (var ideploc = 0; ideploc < AllDepartureLocArrayUniq.length; ideploc++) {
        var SeqsWhereLOC = _.where(AllDepartureTimeLocArray, { frrm: AllDepartureLocArrayUniq[ideploc] });
        var SeqNumbers1 = [];
        var SeqNumbers2 = [];
        var SeqNumbers3 = [];
        var SeqNumbers4 = [];
        for (var ideptt = 0; ideptt < AllDepartureTimeArrayUniq.length; ideptt++) {
            var DeptTimeOnly = moment(AllDepartureTimeArrayUniq[ideptt], ['DD MMMM,HH:mm']).format("HH:mm");
            var IntDeptTimeOnly = parseInt(DeptTimeOnly.substr(0, 2));
            var SeqsWhere = _.where(SeqsWhereLOC, { deptttime: AllDepartureTimeArrayUniq[ideptt] });
            for (var iseqnum = 0; iseqnum < SeqsWhere.length; iseqnum++) {
                if (IntDeptTimeOnly >= 0 && IntDeptTimeOnly < 5) {
                    //alert('1,'+IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers1.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 6 && IntDeptTimeOnly < 11) {
                    //alert('2,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers2.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 12 && IntDeptTimeOnly < 17) {
                    //alert('3,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers3.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 18 && IntDeptTimeOnly < 24) {
                    //alert('4,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers4.push(SeqsWhere[iseqnum].seqqno);
                }
            }
        }

        var SeqNumbersUniq1 = _.uniq(SeqNumbers1);
        var SeqNumbersUniq2 = _.uniq(SeqNumbers2);
        var SeqNumbersUniq3 = _.uniq(SeqNumbers3);
        var SeqNumbersUniq4 = _.uniq(SeqNumbers4);

        DepartureTimeCheckBoxes += '<h4>Timings : From ' + getCityNameUTCOnly(AllDepartureLocArrayUniq[ideploc]) + '</h4><div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq1.join(",") + '" id="12AM06AM' + ideploc + '" type="checkbox" value="12AM06AM" class="DeptTime" checked="checked"> <label for="web" class="formsfont">Early Morning 12AM-6AM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq2.join(",") + '" id="06AM12PM' + ideploc + '" type="checkbox" value="06AM12PM" class="DeptTime" checked="checked"> <label for="web" class="formsfont">Morning 06AM-12PM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq3.join(",") + '" id="12PM06PM' + ideploc + '" type="checkbox" value="12PM06PM" class="DeptTime" checked="checked"> <label for="web" class="formsfont">Mid Day 12PM-06PM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq4.join(",") + '" id="06PM12AM' + ideploc + '" type="checkbox" value="06PM12AM" class="DeptTime" checked="checked"> <label for="web" class="formsfont">Night 06PM-12AM</label><span>Only</span></div>';
    }
    $('#DepartureTimeUL').append(DepartureTimeCheckBoxes);
    AllDepartureLocArrayUniq = '';
    AllDepartureTimeArrayUniq = '';
}

//Generate Arrival Time Filter
function generateArrivalFilterOptionsArray(seqqno, tooo, arriitime) {
    AllArrivalTimeLocArray.push({ "seqqno": seqqno, "tooo": tooo, "arriitime": arriitime });
    //AllArrivalTimeArray1D.push(arriitime);
    //AllArrivalTimeArray1D = _.uniq(AllArrivalTimeArray1D);
    //AllArrivalPlaceArray1D.push(tooo);
    //AllArrivalPlaceArray1D = _.uniq(AllArrivalPlaceArray1D);
}
function GenerateArrivalTimeFilterOptions() {
    var AllArrivalTimeArrayUniq = [];
    var AllArrivalLocArrayUniq = [];
    $("#divFLSearch .show_more_visible .search_details .flights_left_area .flights_left_area01").each(function () {
        var toleg = $(this).attr("d-to");
        if (toleg != undefined) {
            AllArrivalLocArrayUniq.push(toleg);
        }
        var atimeleg = $(this).attr("d-atime");
        if (atimeleg != undefined) {
            AllArrivalTimeArrayUniq.push(atimeleg);
        }
    });

    AllArrivalTimeArrayUniq = _.uniq(AllArrivalTimeArrayUniq);
    AllArrivalLocArrayUniq = _.uniq(AllArrivalLocArrayUniq);
    var ArrivalTimeCheckBoxes = '';
    for (var ideploc = 0; ideploc < AllArrivalLocArrayUniq.length; ideploc++) {
        var SeqsWhereLOC = _.where(AllArrivalTimeLocArray, { tooo: AllArrivalLocArrayUniq[ideploc] });
        var SeqNumbers1 = [];
        var SeqNumbers2 = [];
        var SeqNumbers3 = [];
        var SeqNumbers4 = [];
        for (var ideptt = 0; ideptt < AllArrivalTimeArrayUniq.length; ideptt++) {
            var DeptTimeOnly = moment(AllArrivalTimeArrayUniq[ideptt], ['DD MMMM,HH:mm']).format("HH:mm");
            var IntDeptTimeOnly = parseInt(DeptTimeOnly.substr(0, 2));
            var SeqsWhere = _.where(SeqsWhereLOC, { arriitime: AllArrivalTimeArrayUniq[ideptt] });
            for (var iseqnum = 0; iseqnum < SeqsWhere.length; iseqnum++) {
                if (IntDeptTimeOnly >= 0 && IntDeptTimeOnly < 5) {
                    //alert('1,'+IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers1.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 6 && IntDeptTimeOnly < 11) {
                    //alert('2,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers2.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 12 && IntDeptTimeOnly < 17) {
                    //alert('3,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers3.push(SeqsWhere[iseqnum].seqqno);
                }
                if (IntDeptTimeOnly >= 18 && IntDeptTimeOnly < 24) {
                    //alert('4,' + IntDeptTimeOnly + ',' + SeqsWhere[iseqnum].seqqno);
                    SeqNumbers4.push(SeqsWhere[iseqnum].seqqno);
                }
            }
        }
        var SeqNumbersUniq1 = _.uniq(SeqNumbers1);
        var SeqNumbersUniq2 = _.uniq(SeqNumbers2);
        var SeqNumbersUniq3 = _.uniq(SeqNumbers3);
        var SeqNumbersUniq4 = _.uniq(SeqNumbers4);
        ArrivalTimeCheckBoxes += '<h4>Timings : To ' + getCityNameUTCOnly(AllArrivalLocArrayUniq[ideploc]) + '</h4><div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq1.join(",") + '" id="12AM06AMARR' + ideploc + '" type="checkbox" value="12AM06AM" class="ArrivalTime" checked="checked"> <label for="web" class="formsfont">Early Morning 12AM-6AM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq2.join(",") + '" id="06AM12PMARR' + ideploc + '" type="checkbox" value="06AM12PM" class="ArrivalTime" checked="checked"> <label for="web" class="formsfont">Morning 06AM-12PM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq3.join(",") + '" id="12PM06PMARR' + ideploc + '" type="checkbox" value="12PM06PM" class="ArrivalTime" checked="checked"> <label for="web" class="formsfont">Mid Day 12PM-06PM</label><span>Only</span></div>' +
            '<div class="checkBox_area"> <input data-seq="' + SeqNumbersUniq4.join(",") + '" id="06PM12AMARR' + ideploc + '" type="checkbox" value="06PM12AM" class="ArrivalTime" checked="checked"> <label for="web" class="formsfont">Night 06PM-12AM</label><span>Only</span></div>';
    }
    $('#ArrivalTimeUL').append(ArrivalTimeCheckBoxes);
    AllArrivalTimeArrayUniq = '';
    AllArrivalLocArrayUniq = '';
}

function GetFareInfoClick(sequencenum, combinationid) {
    var datafare = $('#clickfaredet' + sequencenum + '').attr("data-open");
    if ($('#downfaredet' + sequencenum + '').is(":hidden")) {

        if (datafare == 0) {


            Flightrulesonload(sequencenum, combinationid);
            $('#downfaredet' + sequencenum).css("display", "block");
            $('#clickfaredet' + sequencenum + '').attr("data-open", 1);
            $('#downfaredet' + sequencenum).slideToggle("fast");
        }
        else if (datafare == 1) {
            $('#downfaredet' + sequencenum).css("display", "block");
            $('#downfaredet' + sequencenum).slideToggle("fast");
        }
        else {
            Flightrulesonload(sequencenum, combinationid);
            $('#clickfaredet' + sequencenum + '').attr("data-open", 1);
            $('#downfaredet' + sequencenum).css("display", "block");
            $('#downfaredet' + sequencenum).slideToggle("fast");
        }
    }
    else {
        $('#downfaredet' + sequencenum).css("display", "none");
        $('#downfaredet' + sequencenum).slideToggle("fast");

    }

}
function Flightrulesonload(sequencenum, combinationid) {

    var sessionidd = $('#hdsessionid').val();

    var Dastas = {
        sequencenu: sequencenum,
        PassTyp: "ADT",
        combinationi: combinationid,
        session: sessionidd
    }
    $.ajax({
        type: "POST",
        url: "Webservice.aspx/Checkfarerules",
        data: JSON.stringify(Dastas),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populateFlightRulesOnLoad,
        failure: function (response) {
            alert(response.d);

        }
    });

    function populateFlightRulesOnLoad(data) {
        var xmlfarerules = data.d;
        //alert(xmlfarerules);
        var x2js2 = new X2JS();
        var jsonfarerules = x2js2.xml_str2json(xmlfarerules);
        //console.log(jsonfarerules);
        var fullFareData = '';
        var errors = jsonfarerules.Envelope.Body.GetFlightRulesResponse.OTA_AirRulesRS.Errors;
        if (errors != null) {
            fullFareData = '';
            fullFareData = '<div class="deviderMoreCombi"><hr></div><h2>Fare Rules</h2>';
            fullFareData += "Fare Rules information not available";
            $('#downfaredet' + sequencenum + '').append(fullFareData + '<div class="deviderMoreCombi"><hr></div>');
            //$("#divflightrules").show();

        }
        else {

            fullFareData = '';
            fullFareData += '<p>' + jsonfarerules.Envelope.Body.GetFlightRulesResponse.OTA_AirRulesRS.PriceMessageInfoType.PriceMessageInfo.MiniRulesPriceMessages.Text.MiniRulesPriceText["0"]._PriceMessageValue + '</p>';
            var arrfare = fullFareData.split('_');
            // alert(arrfare[1]);
            if (arrfare[1] == "TICKETS ARE NON-REFUNDABLE</p>") {
                // the value is in the array

                $('#downfaredet' + sequencenum + '').append(arrfare[1] + '<div class="deviderMoreCombi"><hr></div>');
                //$("#divflightrules").show();
            }
            else {
                if (arrfare[1] == "PENALTY APPLIES - CHECK RULES</p>") {
                    $('#downfaredet' + sequencenum + '').append('<strong>Cancellation</strong>' + '</br>' + "Cancellation charge not avilable please check flight rules" + '<div class="deviderMoreCombi"><hr></div>');
                }
                else {
                    var arrfarepen = arrfare[1].split('AED');
                    $('#downfaredet' + sequencenum + '').append('<strong>Cancellation</strong>' + '</br>' + "ONEVIEW CHARGES AED 100.00" + '</br>' + "AIRLINE CHARGES AED " + "" + arrfarepen[1] + '<div class="deviderMoreCombi"><hr></div>');

                    //$("#divflightrules").show();
                }
            }
        }
    }
}
function GetParameterValues(param) {
    var QData = 'NODATA';
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return QData = urlparam[1];
        }
    }
    return QData;
}

//Mob Filter Change
function MobFilterChange() {
    var SelectedText = $('option:selected', '#ddlMobFilter').text();
    var SelectedValue = $('option:selected', '#ddlMobFilter').val();
    if (SelectedValue == "1") {
        tinysort('div#divFLSearch>div', { order: 'asc', attr: 'data-price' });
    }
    else if (SelectedValue == "2") {
        tinysort('div#divFLSearch>div', { order: 'desc', attr: 'data-price' });
    }
    else if (SelectedValue == "3") {
        tinysort('div#divFLSearch>div', { order: 'asc', attr: 'data-duration' });
    }
    else if (SelectedValue == "4") {
        tinysort('div#divFLSearch>div', { order: 'desc', attr: 'data-duration' });
    }
    else if (SelectedValue == "5") {
        tinysort('div#divFLSearch>div', { order: 'asc', attr: 'data-dept' });
    }
    else if (SelectedValue == "6") {
        tinysort('div#divFLSearch>div', { order: 'desc', attr: 'data-dept' });
    }
    else if (SelectedValue == "7") {
        tinysort('div#divFLSearch>div', { order: 'asc', attr: 'data-arri' });
    }
    else if (SelectedValue == "8") {
        tinysort('div#divFLSearch>div', { order: 'desc', attr: 'data-arri' });
    }
    $("#mobddlfilterselected").empty();
    $("#mobddlfilterselected").append(SelectedText);
}
//Get Utc
function getAirLineTimeZone(airlinecode) {
    var AirLineTimeZone = _.where(AirlinesTimezone, { I: airlinecode });
    if (AirLineTimeZone == "") {
        AirLineTimeZone = "--h:--m";
    }
    else {
        AirLineTimeZone = AirLineTimeZone[0].TZ;
    }
    return AirLineTimeZone;
}
//Get Duration
function GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo) {
    var DepartureAirlineTimezone = getAirLineTimeZone(FromGo);
    var ArrivalAirlineTimezone = getAirLineTimeZone(ToGo);
    var DurationTimezone = DepartureAirlineTimezone - ArrivalAirlineTimezone;
    var DurationTimezoneMinutes = parseFloat(DurationTimezone) * 60;
    DurationTimezoneMilliseconds = parseFloat(DurationTimezoneMinutes) * 60000;
    var depariduration = moment(ArriTimeGo) - moment(DepTimeGo);
    //var depariduration = moment(ArriTimeGo, "DD MMM YYYY, HH:mm") - moment(DepTimeGo, "DD MMM YYYY, HH:mm");
    var GrandDurationTimezone = depariduration + DurationTimezoneMilliseconds;

    GrandDurationTimezone = GrandDurationTimezone / 60000;

    var temphours = 0;
    var tempminutes = 0;
    var DTime = '';
    if (GrandDurationTimezone > 59) {
        temphours = Math.floor(GrandDurationTimezone / 60);
        tempminutes = GrandDurationTimezone - (temphours * 60);
        if (tempminutes == 0) {
            DTime = temphours + 'h ';
        }
        else {
            DTime = temphours + 'h ' + tempminutes + 'm';
        }

    }
    else {
        tempminutes = GrandDurationTimezone;
        DTime = tempminutes + 'm';
    }
    if (DTime.indexOf("N") > -1) {
        DTime = "";
    }

    return DTime;
}
//Get PAX Name
function GetPaxName(PaxCode) {
    var PaxName = PaxCode;
    if (PaxCode == 'ADT') {
        PaxName = 'Adults';
    }
    else if (PaxCode == 'CHD') {
        PaxName = 'Child';
    }
    else if (PaxCode == 'INF') {
        PaxName = 'Infants';
    }
    return PaxName;
}
//Get Markup
function GetMarkupData() {
    var AgencyID = '65';
    var ServiceTypeID = '1';
    var markdat = {
        AgencyID: AgencyID,
        ServiceTypeID: ServiceTypeID
    }
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/GetMarkupData",
        data: JSON.stringify(markdat),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: MarkupSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function MarkupSuccess(markdat) {
    //console.log(JSON.parse(markdat.d));
    var MarkDataa = {
        MarkupData: JSON.stringify(markdat.d)
    }
    //$.ajax({
    //    type: "POST",
    //    url: "Webservice.aspx/SaveMarkupData",
    //    data: JSON.stringify(MarkDataa),
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: MarkupSuccessSave,
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});
}
//function MarkupSuccessSave() {
//}

function SessionChecking(session) {
    $(".loadingGIF").show();
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        //console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        $('.AgencyName').html(userdata[0].AgencyName);
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }
        GetMarkupDataa('MarkupData');
    }
}
//Calculate Markup
function CalcMarkup(ogprice, xmlmarkfare) {
    var ogpricee = parseFloat(ogprice);
    var MarkupValuee = parseFloat(MarkupValue);
    var PriceWithMarkup = parseFloat(0);

    if (MarkupTypeID == 1) {//percentage
        var PercAmount = ogpricee * (MarkupValuee / 100);
        PriceWithMarkup = ogpricee + PercAmount;
    }
    else if (MarkupTypeID == 2) {//value
        PriceWithMarkup = ogpricee + MarkupValuee;
    }
    PriceWithMarkup = PriceWithMarkup + parseFloat(xmlmarkfare);
    //return Math.round(PriceWithMarkup);
    return Math.round(PriceWithMarkup * 100) / 100;
}
//Calculate Markup Tax
function CalcMarkupTax(ogprice, tax, xmlmarkfare) {
    var ogpricee = parseFloat(ogprice);
    var MarkupValuee = parseFloat(MarkupValue);
    var PriceWithMarkup = parseFloat(0);

    if (MarkupTypeID == 1) {//percentage
        var PercAmount = ogpricee * (MarkupValuee / 100);
        PriceWithMarkup = tax + PercAmount;
    }
    else if (MarkupTypeID == 2) {//value
        PriceWithMarkup = tax + MarkupValuee;
    }
    PriceWithMarkup = PriceWithMarkup + parseFloat(xmlmarkfare);
    //return Math.round(PriceWithMarkup);
    return Math.round(PriceWithMarkup * 100) / 100;
}

//Get Markup Data
function GetMarkupDataa(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: MarkSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function MarkSuccess(data) {
    if (data.d != 'NOSESSION') {
        if (data.d != 'NOMARKUP') {
            var markupdata = $.parseJSON(data.d);
            //console.log(markupdata);
            MarkupTypeID = markupdata[0].MarkupTypeID;
            MarkupValue = markupdata[0].MarkupValue;
            //console.log(MarkupValue);
            $('.CreditLimitAmount').html('Credit Limit - ' + markupdata[0].CreditLimitAmount + ' ' + markupdata[0].CurrenyCode);
        }
        else {//No Markup Data
            MarkupTypeID = 2;
            MarkupValue = 0;
        }
        ShowFlightResults();
    }
    else {//No Session
        window.location.href = "/index.html";
    }
}
function showIntrestingFact() {
    var Caption = _.where(InterestingFacts, { No: "0" });
    var randomid = [];
    randomid = InterestingFacts[Math.floor(Math.random() * InterestingFacts.length)];
    var cap = randomid.Caption;
    $("#loadingdata").append('<h4>Interesting Facts About ' + ClientCountryName + '</h4><p>' + Caption[0].Info + '</p>');
    if (cap == "") {
        randomid = InterestingFacts[Math.floor(Math.random() * InterestingFacts.length)];
        $("#loadingdata").append('<h4>' + randomid.Caption + '</h4>' + '<p>' + randomid.Info + '</p>');
    }
    else {
        $("#loadingdata").append('<h4>' + randomid.Caption + '</h4><p>' + randomid.Info + '</p>');
    }
}

//airlinefilter onload
function airlinefilteronload() {
    var airlinepara = GetParameterValues('airline');
    if (airlinepara != 'NODATA') {
        $("#AirlinesUL div.checkBox_area .AirlineeChk").each(function () {
            var tempairlinecode = $(this).attr("value");
            if (airlinepara != tempairlinecode) {
                $(this).click();
            }
        });
    }
}

function directflightsfilteronload() {
    var Direct = GetParameterValues('DirectFlights');
    if (Direct != 'NODATA') {
        $("#StopsUL div.checkBox_area .shastops").each(function () {
            var tempcode = $(this).attr("value");
            if (tempcode != "Non -") {
                $(this).click();
            }
        });
    }
}

function Showsearchsummary(triptype, from, to, depdatee, retdate, ad, ch, inf) {
    $('.deptCity').html('' + from + ' <span>' + getCityNameUTCOnly(from) + '</span>');
    $('.arriCity').html('' + to + ' <span>' + getCityNameUTCOnly(to) + '</span>');
    if (DateFormate == 'dd/mm/yy') {
        depdatee = new Date(depdatee.split('/')[1] + '-' + depdatee.split('/')[0] + '-' + depdatee.split('/')[2]);
        retdate = new Date(retdate.split('/')[1] + '-' + retdate.split('/')[0] + '-' + retdate.split('/')[2]);
    }
    else {
        depdatee = new Date(depdatee);
        retdate = new Date(retdate);
    }

    var datepax = '';
    //Oneway
    var onward = moment(depdatee, DateFormate).format("DD MMM YY");
    datepax += ' <i class="fa fa-calendar" aria-hidden="true"></i> Onward <span>' + onward + '</span>';

    if (triptype == 'R') {//Return
        var returndate = moment(retdate, DateFormate).format("DD MMM YY");
        datepax += ' Return <span>' + returndate + '</span>';
    }
    var totpaxcount = 'AD : ' + parseInt(ad) + '';
    if (parseInt(ch) > 0) {
        totpaxcount += '  CH : ' + parseInt(ch) + ' INF : ' + parseInt(inf);
    }
    else if (parseInt(inf) > 0) {
        totpaxcount += ' INF : ' + parseInt(inf);
    }
    datepax += '<i class="fa fa-users icn_htl" aria-hidden="true"></i> <span>' + totpaxcount + '</span>';
    $('.datepax').append(datepax);
    $("#loadingdata h5").append('' + from + ' <span> ' + getCityNameUTCOnly(from) + '</span> to ' + to + ' <span>' + getCityNameUTCOnly(to) + '</span> - ' + datepax + '');
}
//Only Filter Click		
function finalizeonlyshowclick() {
    $(".checkBox_area span").click(function () {
        $("#" + $(this).parent().parent().attr("id") + " .checkBox_area input").each(function () {
            if ($(this).is(":checked")) {
                $(this).click();
            }
        });
        if ($(this).parent().find("input").is(":checked")) { }
        else { $(this).parent().find("input").click(); }
    });
}
//Show all filter click		
function finalizeshowshowallclick() {
    $(".showall").click(function () {
        $(this).parent().find(" .checkBox_area input").each(function () {
            if ($(this).is(":checked")) {
            }
            else {
                $(this).click();
            }
        });
    });
}