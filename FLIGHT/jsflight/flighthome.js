﻿var DateFormate = 'mm/dd/yy';
//var DateFormate = 'dd/mm/yy';
var tempautocompleteid = "";
var serviceblocked = false;


$(document).ready(function () {
    if (location.hostname === "localhost") {
        DateFormate = 'dd/mm/yy';
    }
    else {
        DateFormate = 'mm/dd/yy';
    }
    $("#divMultiLeg").hide();
    //Role Checking
    //RoleChecking('UserRoleStatus');
    //Session Checking
    SessionChecking('UserDetails');
    //window.setInterval(function () {
    //    SessionChecking('UserDetails');
    //}, 10000);

    $(".close-warning").click(function () {
        $(".browser-warning").fadeOut("slow");
    });
    //Airport
    $(".flightcity").autocomplete({
        source: AirlinesTimezone, minLength: 3, position: { my: "left top", at: "left bottom" },
        change: function (event, ui) { if (!ui.item) { $(this).val(''); $(this).prop('placeholder', 'No Match Found'); } },
        select: function (event, ui) {
            if (ui.item.id != 0) {
                if (ui.item.value.indexOf('No Match Found') < 0) {
                    this.value = ui.item.label;
                }
            } return false;
        },
        //response: function (event, ui) {
        //    if (ui.content.length == 1) {
        //        $(this).val(ui.content[0].value);
        //        $(this).autocomplete("close");
        //    }
        //},


        focus: function () { return false; }
    });
    $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
        var airporttype = new RegExp("^" + "A", "i");
        //return $.grep(array, function (value) { return (airporttype.test(value.T) && (matcher.test(value.a) || matcher.test(value.label) || matcher.test(value.c) || matcher.test(value.n))); });
        //return $.grep(array, function (value) { return ((matcher.test(value.a) || matcher.test(value.label) || matcher.test(value.c) || matcher.test(value.n))); });
        return $.grep(array, function (value) { return ((matcher.test(value.I) || matcher.test(value.label) || matcher.test(value.C) || matcher.test(value.CN)) && (airporttype.test(value.T))); });
    };

    //Date 1,2
    var tempnumberofmonths = parseInt(1);
    if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
        tempnumberofmonths = parseInt(2);
    }
    else if (parseInt($(window).width()) > 999) {
        tempnumberofmonths = parseInt(3);
    }


    $('#deptDate01').datepicker({
        defaultDate: "0d",
        minDate: "0d",
        maxDate: "360d",
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            var $visibility = $('#RetDiv').css('visibility');
            var $value = $(this).val();
            //$('#retDate').focus();
            $("#retDate").datepicker("option", "minDate", selectedDate);
            $("#retDate").datepicker("setDate", selectedDate);
            $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
        },
        onClose: function (dateText, inst) {
            $('#retDate').focus();
        }
    });


    $('#deptDate01').datepicker("widget").css({ "z-index": 10000 });

    $('#retDate').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeMonth: true,
        numberOfMonths: tempnumberofmonths,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            //$("#deptDate01").datepicker("option", "maxDate", selectedDate);
        }
    });
    $('#retDate').datepicker("widget").css({ "z-index": 10000 });

    //Date MultiLeg

    //$('.dateflight').datepicker({
    //    defaultDate: "+1w",
    //    changeMonth: true,
    //    numberOfMonths: 1,
    //    showButtonPanel: false,
    //    dateFormat: DateFormate,
    //});
    //$('.dateflight').datepicker("widget").css({ "z-index": 10000 });

    $('#txtLeg2Date').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeYear: true,
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#txtLeg2Date').datepicker("widget").css({ "z-index": 10000 });

    $('#txtLeg3Date').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeYear: true,
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#txtLeg3Date').datepicker("widget").css({ "z-index": 10000 });

    $('#txtLeg4Date').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeYear: true,
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#txtLeg4Date').datepicker("widget").css({ "z-index": 10000 });

    $('#txtLeg5Date').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeYear: true,
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate,
        onSelect: function (selectedDate) {
            $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
        }
    });
    $('#txtLeg5Date').datepicker("widget").css({ "z-index": 10000 });

    $('#txtLeg6Date').datepicker({
        defaultDate: "0d",
        maxDate: "360d",
        changeYear: true,
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        showButtonPanel: false,
        dateFormat: DateFormate
    });
    $('#txtLeg6Date').datepicker("widget").css({ "z-index": 10000 });

    //DropDown
    $("#ddladult").change(function () {
        var selectedText = $(this).find("option:selected").text();
        var selectedValue = $(this).val();
        //Inf
        var ddlinfants = $("#ddlinfant");
        ddlinfants.empty().append('<option selected="selected" value="0">0</option>');
        for (var i = 1; i <= selectedValue; i++) {
            ddlinfants.append($("<option value=" + i + ">" + i + "</option>"));
        }
        //Child
        var TotChild = 9 - selectedValue;
        var ddlChild = $("#ddlchild");
        ddlChild.empty().append('<option selected="selected" value="0">0</option>');
        for (var i2 = 1; i2 <= TotChild; i2++) {
            ddlChild.append($("<option value=" + i2 + ">" + i2 + "</option>"));
        }
    });
    $("#ddlinfant").change(function () {
        var selectedINFValue = $(this).val();
        $('#hdSelectedINF').val(selectedINFValue);
    });

    //logout
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'UserDetails'
        };
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData",
            data: JSON.stringify(Dastasseq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });
});

function fltypeclick(mode) {
    if (mode == 1) {
        document.getElementById('retDate').style.visibility = "Hidden";
        $("#divMultiLeg").hide();
        $("#retDateLabel").hide();
    }
    if (mode == 2) {
        document.getElementById('retDate').style.visibility = "visible";
        $("#retDateLabel").show();
        $("#divMultiLeg").hide();
    }
    if (mode == 3) {
        $("#legnumber").val(2);
        $("#divAddLeg").show();
        document.getElementById('retDate').style.visibility = "Hidden";
        $("#retDateLabel").hide();
        $("#divMultiLeg").show();
        $("#multi_leg_3").hide();
        $("#multi_leg_4").hide();
        $("#multi_leg_5").hide();
        $("#multi_leg_6").hide();
    }
}

function removeleg(legnum) {
    var legCount = parseInt($("#legnumber").val());
    if (legCount > 2) {
        var legCountsub = parseInt(legCount - 1);
        if (legCountsub < 6) {
            $("#divAddLeg").show();
        }
        $("#multi_leg_" + legCount).hide();
        $("#legnumber").val(legCountsub);
    }
}
function addleg() {
    var legCount = parseInt($("#legnumber").val());
    if (legCount < 6) {
        var legCountadded = parseInt(legCount + 1);
        if (legCountadded == 6) {
            $("#divAddLeg").hide();
        }
        else {
            $("#divAddLeg").show();
        }
        $("#multi_leg_" + legCountadded).show();
        $("#legnumber").val(legCountadded);
    }
}

function Search() {
    var DeptDate = $('#deptDate01').val();
    var RetDate = $('#retDate').val();

    var AdultPax = parseInt($('#ddladult').find(":selected").text());
    var ChildPax = parseInt($('#ddlchild').find(":selected").text());
    var InfantPax = parseInt($('#ddlinfant').find(":selected").text());

    var Cabin = $('#ddlCabin').find(":selected").text();

    var DirectF = '';
    if ($("#rbDirect").prop("checked")) {
        DirectF = '&DirectFlights=true';
    }
    var AirlineF = '';
    if ($('#ddlAirline').find(":selected").val() != 'All') {
        AirlineF += '&airline=' + $('#ddlAirline').find(":selected").val();
    }

    if ($("#radio-btn-1").prop("checked")) {
        if (txtFrom.value != '' && txtTo.value != '' && DeptDate != '') {
            //var qfrom = txtFrom.value.match(/\(([^)]+)\)/)[1];
            //var qto = txtTo.value.match(/\(([^)]+)\)/)[1];
            var qfrom = getairportvalue(txtFrom.value);
            var qto = getairportvalue(txtTo.value);
            if (qfrom == qto) {
                alertify.alert('Please fill !', 'Departure and arrival airports should not be same !');
                return false;
            }
            window.location.href = "flightresults.html?triptype=O&from=" + qfrom + "&to=" + qto + "&depdate=" + DeptDate + "&class=" + Cabin + "&ad=" + AdultPax + "&ch=" + ChildPax + "&inf=" + InfantPax + DirectF + AirlineF + "";
        }
        else {
            alertify.alert('Please fill !', 'Please fill origin, destination and departure date !');
        }
    }
    else if ($("#radio-btn-2").prop("checked")) {
        if (txtFrom.value != '' && txtTo.value != '' && DeptDate != '' && RetDate != '') {
            //var qfrom = txtFrom.value.match(/\(([^)]+)\)/)[1];
            //var qto = txtTo.value.match(/\(([^)]+)\)/)[1];
            var qfrom = getairportvalue(txtFrom.value);
            var qto = getairportvalue(txtTo.value);
            if (qfrom == qto) {
                alertify.alert('Please fill !', 'Departure and arrival airports should not be same !');
                return false;
            }
            window.location.href = "flightresults.html?triptype=R&from=" + qfrom + "&to=" + qto + "&depdate=" + DeptDate + "&retdate=" + RetDate + "&class=" + Cabin + "&ad=" + AdultPax + "&ch=" + ChildPax + "&inf=" + InfantPax + DirectF + AirlineF + "";
        }
        else {
            alertify.alert('Please fill !', 'Please fill origin, destination, departure date, return date !');
        }
    }
    else if ($("#radio-btn-3").prop("checked")) {
        if (txtFrom.value != '' && txtTo.value != '' && DeptDate != '') {
            //var qfrom = txtFrom.value.match(/\(([^)]+)\)/)[1];
            //var qto = txtTo.value.match(/\(([^)]+)\)/)[1];
            var qfrom = getairportvalue(txtFrom.value);
            var qto = getairportvalue(txtTo.value);
            if (qfrom == qto) {
                alertify.alert('Please fill !', 'Departure and arrival airports should not be same !');
                return false;
            }
            var Query = "&leg1=" + qfrom + "-" + qto + "-" + DeptDate + "";

            if ($('#txtLeg2From').val() != "" && $('#txtLeg2To').val() != "" && $('#txtLeg2Date').val() != "") {
                //var qfrom2 = $('#txtLeg2From').val().match(/\(([^)]+)\)/)[1];
                //var qto2 = $('#txtLeg2To').val().match(/\(([^)]+)\)/)[1];
                var qfrom2 = getairportvalue(txtLeg2From.value);
                var qto2 = getairportvalue(txtLeg2To.value);
                if (qfrom2 == qto2) {
                    alertify.alert('Please fill !', 'Departure and arrival airports should not be same in leg 2 !');
                    $('#txtLeg2From').focus();
                    return false;
                }
                Query += "&leg2=" + qfrom2 + "-" + qto2 + "-" + $('#txtLeg2Date').val() + "";
            }
            if ($('#txtLeg3From').val() != "" && $('#txtLeg3To').val() != "" && $('#txtLeg3Date').val() != "") {
                //var qfrom3 = $('#txtLeg3From').val().match(/\(([^)]+)\)/)[1];
                //var qto3 = $('#txtLeg3To').val().match(/\(([^)]+)\)/)[1];
                var qfrom3 = getairportvalue(txtLeg3From.value);
                var qto3 = getairportvalue(txtLeg3To.value);
                if (qfrom3 == qto3) {
                    alertify.alert('Please fill !', 'Departure and arrival airports should not be same in leg 3 !');
                    $('#txtLeg3From').focus();
                    return false;
                }
                Query += "&leg3=" + qfrom3 + "-" + qto3 + "-" + $('#txtLeg3Date').val() + "";
            }
            if ($('#txtLeg4From').val() != "" && $('#txtLeg4To').val() != "" && $('#txtLeg4Date').val() != "") {
                //var qfrom4 = $('#txtLeg4From').val().match(/\(([^)]+)\)/)[1];
                //var qto4 = $('#txtLeg4To').val().match(/\(([^)]+)\)/)[1];
                var qfrom4 = getairportvalue(txtLeg4From.value);
                var qto4 = getairportvalue(txtLeg4To.value);
                if (qfrom4 == qto4) {
                    alertify.alert('Please fill !', 'Departure and arrival airports should not be same in leg 4 !');
                    $('#txtLeg4From').focus();
                    return false;
                }
                Query += "&leg4=" + qfrom4 + "-" + qto4 + "-" + $('#txtLeg4Date').val() + "";
            }
            if ($('#txtLeg5From').val() != "" && $('#txtLeg5To').val() != "" && $('#txtLeg5Date').val() != "") {
                //var qfrom5 = $('#txtLeg5From').val().match(/\(([^)]+)\)/)[1];
                //var qto5 = $('#txtLeg5To').val().match(/\(([^)]+)\)/)[1];
                var qfrom5 = getairportvalue(txtLeg5From.value);
                var qto5 = getairportvalue(txtLeg5To.value);
                if (qfrom5 == qto5) {
                    alertify.alert('Please fill !', 'Departure and arrival airports should not be same in leg 5 !');
                    $('#txtLeg5From').focus();
                    return false;
                }
                Query += "&leg5=" + qfrom5 + "-" + qto5 + "-" + $('#txtLeg5Date').val() + "";
            }
            if ($('#txtLeg6From').val() != "" && $('#txtLeg6To').val() != "" && $('#txtLeg6Date').val() != "") {
                //var qfrom6 = $('#txtLeg6From').val().match(/\(([^)]+)\)/)[1];
                //var qto6 = $('#txtLeg6To').val().match(/\(([^)]+)\)/)[1];
                var qfrom6 = getairportvalue(txtLeg6From.value);
                var qto6 = getairportvalue(txtLeg6To.value);
                if (qfrom6 == qto6) {
                    alertify.alert('Please fill !', 'Departure and arrival airports should not be same in leg 6 !');
                    $('#txtLeg6From').focus();
                    return false;
                }
                Query += "&leg6=" + qfrom6 + "-" + qto6 + "-" + $('#txtLeg6Date').val() + "";
            }

            window.location.href = "flightresults.html?triptype=M" + Query + "&class=" + Cabin + "&ad=" + AdultPax + "&ch=" + ChildPax + "&inf=" + InfantPax + DirectF + AirlineF + "";
        }
        else {
            alertify.alert('Please fill !', 'Please fill origin, destination and departure date !');
        }
    }
}

function SessionChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SessionSuccess(data) {
    if (data.d == 'NOSESSION') {
        window.location.href = "/index.html";
    }
    else {
        //console.log(JSON.parse(data.d));
        var userdata = $.parseJSON(data.d);
        $('.username').html(userdata[0].FirstName);
        $('.AgencyName').html(userdata[0].AgencyName);
        var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
        $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
        var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
        if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
            $('.CreditLimitAmount').hide();
        }
        var AgencyID = userdata[0].AgencyID;

        //Get Markup
        var Dastasseqq = {
            AgencyID: userdata[0].AgencyIDD,
            ServiceTypeID: '1'
        };
        $.ajax({
            type: "POST",
            url: "/FLIGHT/WebMethodsDB.aspx/GetMarkupData",
            data: JSON.stringify(Dastasseqq),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: MarkupSuccess,
            failure: function (response) {
                alert(response.d);
            }
        });
        GetActionReqFlight(AgencyID, AgencyTypeIDD)
        checkServiceBlocked(AgencyID);
    }
}
function MarkupSuccess(datamark) {
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(userdata);
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No Credit !');
    }
    //BindAirlines();
}
//Role
function RoleChecking(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: RoleSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function RoleSuccess(datarole) {
    //alert(datarole.d);
}

function getLocation() {
    var locu = $.getJSON("http://freegeoip.net/json/", function (data) {
        var country_code = data.country_code;
        var country = data.country_name;
        var ip = data.ip;
        var time_zone = data.time_zone;
        var latitude = data.latitude;
        var longitude = data.longitude;
        console.log(data);
    });
}

function getairportvalue(qfrom) {

    var Tfrom = qfrom.substr(qfrom.lastIndexOf("(") + 1);
    var Farr = Tfrom.split(')');
    Tfrom = Farr[0];

    return Tfrom;

}

//function BindAirlines() {
//    var airlistItems = "<option value='All'>All</option>";
//    for (var iar = 0; iar < AirlinesDatas.length; iar++) {
//        airlistItems += "<option value='" + AirlinesDatas[iar].C + "'>" + AirlinesDatas[iar].A + "</option>";
//    }
//    $("#ddlAirline").html(airlistItems);

//    var options = $('select#ddlAirline option');
//    var arr = options.map(function (_, o) { return { t: $(o).text(), v: o.value }; }).get();
//    arr.sort(function (o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
//    options.each(function (i, o) {
//        o.value = arr[i].v;
//        $(o).text(arr[i].t);
//    });
//}

function checkServiceBlocked(AgencyIDD) {
    //Get Markup
    var Dastasseqq = {
        AgencyId: AgencyIDD,
        ServiceTypeID: '1'
    };
    $.ajax({
        type: "POST",
        url: "WebMethodsDB.aspx/checkServiceBlocked",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: checkServiceBlockedSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function checkServiceBlockedSuccess(datablock) {
        //alert(datablock.d)
        if (datablock.d == '1') {
            serviceblocked = true;
            $('#flight').remove();
            $('#hotelnoservice').show();
        }
        else {
            serviceblocked = false;
            $('#hotelnoservice').hide();
        }
    }
}
//Action Required Flight
function GetActionReqFlight(AgencyID, AgencyTypeIDDD) {
    $("#loadergiffl").show();
    //$("#tableActFl").hide();
    var Dastasseqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetActionReqFlight",
        data: JSON.stringify(Dastasseqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqFlightSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqFlightSuccess(datacreee) {
        if (datacreee.d != 'NODATA') {
            var Searchdata = $.parseJSON(datacreee.d);
            console.log(Searchdata);
            $('#divactFl').show();
            var searchresf = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                searchresf += '<tr onclick="ActionReqFlightClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                    '</td><td data-toggle="tooltip" data-placement="top" title="Agency Name:' + Searchdata[isd].AgencyName + '">'
                    + Searchdata[isd].FirstName + '</td><td class="onlyhq">' + Searchdata[isd].AgencyName + '</td><td class="no_mobile">' + Searchdata[isd].SupplierBookingReference +
                    '</td><td class="no_mobile">' + Searchdata[isd].IstPax + '</td><td data-toggle="tooltip" data-placement="top" title="'
                    + getAirportNameUTC(Searchdata[isd].DepartureAirportLocationCode) + ' to ' + getAirportNameUTC(Searchdata[isd].ArrivalAirportLocationCode) + '">'
                    + Searchdata[isd].DepartureAirportLocationCode + ' to ' + Searchdata[isd].ArrivalAirportLocationCode + '</td><td data-toggle="tooltip" data-placement="top" title="' + Searchdata[isd].BokingStatName + '">'
                    + Searchdata[isd].BookingStatusCode + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                    '</td><td  data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].CancellationDeadline).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].CancellationDeadline).format("DD-MM-YY") + '</td></tr>';
            }
            $("#ActReq1").html(searchresf);
            $('[data-toggle="tooltip"]').tooltip();
            $('#tableActFl').DataTable({
                "order": [[0, "desc"]]
            });
            if (AgencyTypeIDDD != '1') {
                $('.onlyhq').remove();
            }
        }
        else {
            $('#divactFl').hide();
        }
        $("#loadergiffl").hide();
        //$("#tableActFl").show();
    }
}
function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}
function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}
function ActionReqFlightClick(sid) {
    var Datatable = {
        sessionname: 'BookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqFlightClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function ActionReqFlightClickSuccess() {
    window.location.href = "/FLIGHT/bookinginfo.html";
}