﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Configuration;

public partial class Testlogrequest : System.Web.UI.Page
{
    public static string HubURL = WebConfigurationManager.AppSettings["HubURL"];
    public static string HubPort = WebConfigurationManager.AppSettings["HubPort"];

    protected void Page_Load(object sender, EventArgs e)
    {


    }

    [System.Web.Services.WebMethod]
    public static string ForgotPassword(string Request)
    {
        // ForgotPassword pass = new ForgotPassword();
        //   string abcd = Request;
        String InterfaceURL = HubURL + ":" + HubPort + "/profile/password";
        string strResult = "NORESULT";
        try
        {
            //string oldpassword = "123";
            //string newpassword = "456";


            //string req="{ "oldPassword": "123", "newPassword": "456" }";

            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "PUT";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(Request);
                streamWriter.Flush();
            }



            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;
    }

    [System.Web.Services.WebMethod]
    public static string FlightSearchResponse(string uname, string pwd, string agcy)
    {
        AuthenticateResponse aws = new AuthenticateResponse();
        //string abcd = Request;

        String InterfaceURL = HubURL + ":" + HubPort + "/authenticate/" + agcy + "";

        string strResult = "NORESULT";
        try
        {
            //String username = "irshad";
            //String password = "456";
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(uname + ":" + pwd));


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
            httpWebRequest.Method = "GET";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
         | SecurityProtocolType.Tls11
         | SecurityProtocolType.Tls12
         | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];
            HttpContext.Current.Session["Access_Token"] = accesstocken;
            if (accesstocken != null)

            {
                //string Out = string.Empty;
                //BALSettings bals = new BALSettings();
                //bals.Username = uname;
                //bals.Password = bals.Encrypt(pwd);
                //DataTable dt = bals.CheckLogin();
                //if (dt.Rows.Count > 0)
                //{
                //    string UserStat = dt.Rows[0]["LoginStatus"].ToString();
                //    string AgencyStat = dt.Rows[0]["AgencyLoginStat"].ToString();
                //    if (UserStat == "1" && AgencyStat == "1")
                //    {
                //        Out = "SUCCESS";
                //        HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                //        Int64 UID = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());

                //        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                //        Dictionary<string, object> row;
                //        foreach (DataRow dr in dt.Rows)
                //        {
                //            row = new Dictionary<string, object>();
                //            foreach (DataColumn col in dt.Columns)
                //            {
                //                row.Add(col.ColumnName, dr[col]);
                //            }
                //            rows.Add(row);
                //        }
                //        HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);

                //        GetRoleStatus(UID);
                //    }
                //    else
                //    {
                //        Out = "LOCKED";
                //    }

                //}
                //else
                //{
                //    Out = "NOUSER";
                //}
                ////return Out;




                string requestStr = JsonConvert.SerializeObject(httpResponse);
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    //string token= httpResponse.Headers.AllKeys.
                    var responseText = streamReader.ReadToEnd();
                    Console.WriteLine(responseText);
                    strResult = responseText;
                    HttpContext.Current.Session["Loginuser"] = strResult;
                    aws = JsonConvert.DeserializeObject<AuthenticateResponse>(strResult);
                }
            }



        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            //strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //  strResult = errMessage;
                ex.Response.Close();
            }
        }



        // = JsonConvert.DeserializeObject<User>(json);

        return strResult;

    }

    public static void GetRoleStatus(Int64 UID)
    {
        //Get Roles
        BALSettings bals = new BALSettings();
        bals.UserID = UID;
        DataTable dtroles = bals.GetUserRoleStatus();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            HttpContext.Current.Session["UserRoleStatus"] = serializer1.Serialize(rows1);
        }
    }

    [System.Web.Services.WebMethod]
    public static string DebitEntry(string Request)
    {
        DebitEntry deb = new DebitEntry();
        string abcd = Request;
        String InterfaceURL = HubURL + ":" + HubPort + "/transaction/debitentry";

        string strResult = "NORESULT";
        try
        {


            string req = "{\"amount\": \"10.00\",\"description\":\"test entry\",\"blocked\": \"true\"}";


            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(abcd);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                strResult = errMessage;
                ex.Response.Close();
            }

        }

        return strResult;

    }

    [System.Web.Services.WebMethod]
    public static string GetCreditLimit(string AgencyID)
    {

        //string abcd = Request;
        String InterfaceURL = HubURL + ":" + HubPort + "/transaction/availablecreditlimit";

        string strResult = "NORESULT";
        try
        {
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "GET";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    streamWriter.Write(req);
            //    streamWriter.Flush();
            //}

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];
            HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
                HttpContext.Current.Session["CreditLimit"] = strResult;
            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;

    }
    [System.Web.Services.WebMethod]
    public static string PostBooking()
    {

        //string abcd = Request;
        String InterfaceURL = HubURL + ":" + HubPort + "/node/allchildnodes";

        string strResult = "NORESULT";
        try
        {
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "GET";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    streamWriter.Write(req);
            //    streamWriter.Flush();
            //}

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            //string accesstocken = headerresponse["access_token"];
            //HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;

            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;

    }

    [System.Web.Services.WebMethod]
    public static string PostBookingByUserId(string AgencyID)
    {
        //string abcd = Request;
        String InterfaceURL = "https://67.209.127.116:9443/node/" + AgencyID + "/users";

        string strResult = "NORESULT";
        try
        {
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "GET";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    streamWriter.Write(req);
            //    streamWriter.Flush();
            //}

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            //string accesstocken = headerresponse["access_token"];
            //HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;

            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;









    }

    [System.Web.Services.WebMethod]
    public static string GetSearchDataHotel()
    {

        //string abcd = Request;
        String InterfaceURL = HubURL + ":" + HubPort + "/hotel/allBookings";

        string strResult = "NORESULT";
        try
        {
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "GET";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    streamWriter.Write(req);
            //    streamWriter.Flush();
            //}

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            //string accesstocken = headerresponse["access_token"];
            //HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;

            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;

    }
    [System.Web.Services.WebMethod]
    public static string GetAllSentQuotes()
    {

        //string abcd = Request;
        String InterfaceURL = "https://67.209.127.116:9443/quote/allQuotes";

        string strResult = "NORESULT";
        try
        {
            //string token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiNDQiLCJleHAiOjE1MjkzMjIyNjZ9.iN3OdNfGiEwho-W4p1_ZF0CPz_RSkfLfvHdwBL6HrAQ";
            string token = HttpContext.Current.Session["Access_Token"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "GET";

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    streamWriter.Write(req);
            //    streamWriter.Flush();
            //}

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            //string accesstocken = headerresponse["access_token"];
            //HttpContext.Current.Session["Access_Token"] = accesstocken;

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;

            }
        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            // strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                //   strResult = errMessage;
                ex.Response.Close();
            }

        }
        // pass = JsonConvert.DeserializeObject<ForgotPassword>(strResult);

        // = JsonConvert.DeserializeObject<User>(json);
        return strResult;

    }
    [System.Web.Services.WebMethod]
    public static string GetsentHotelQuoteDetails(string Request)
    {

        string abcd = Request;
        String InterfaceURL = "https://67.209.127.116:9443/quote/quoteDetails ";

        string strResult = "NORESULT";
        try
        {





            string token = HttpContext.Current.Session["Access_Token"].ToString();
            // string token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiNDQiLCJleHAiOjE1MjkzMjIyNjZ9.iN3OdNfGiEwho-W4p1_ZF0CPz_RSkfLfvHdwBL6HrAQ";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);


            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", "Bearer " + token);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";




            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(abcd);
                streamWriter.Flush();
            }

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
        | SecurityProtocolType.Tls11
        | SecurityProtocolType.Tls12
        | SecurityProtocolType.Ssl3;
            // Skip validation of SSL/TLS certificate
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];

            string requestStr = JsonConvert.SerializeObject(httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //string token= httpResponse.Headers.AllKeys.
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                strResult = responseText;
            }

        }
        catch (WebException ex)
        {
            string errMessage = ex.Message;
            strResult = errMessage;
            if (ex.Response != null)
            {
                System.IO.Stream resStr = ex.Response.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                string soapResponse = sr.ReadToEnd();
                sr.Close();
                resStr.Close();
                errMessage += Environment.NewLine + soapResponse;
                strResult = errMessage;
                ex.Response.Close();
            }

        }

        return strResult;

    }

}