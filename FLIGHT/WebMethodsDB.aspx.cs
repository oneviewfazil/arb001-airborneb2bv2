﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using System.Configuration;

public partial class WebMethodsDB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [System.Web.Services.WebMethod]
    public static string GetMarkupData(Int64 AgencyID, int ServiceTypeID)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.AgencyID = AgencyID;
        bals.ServiceTypeID = ServiceTypeID;
        DataTable dt = bals.GetMarkupDetails();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            //Out = "[{" + '"' + "MarkupTypeID" + '"' + ":0," + '"' + "MarkupValue" + '"' + ":0," + '"' + "CurrenyCode" + '"' + ":" + '"' + "AED" + '"' + "}]";
            Out = "NOMARKUP";
        }
        HttpContext.Current.Session["MarkupData"] = Out;
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CheckLogin(string uname, string pwd)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.Username = uname;
        bals.Password = bals.Encrypt(pwd);
        DataTable dt = bals.CheckLogin();
        if (dt.Rows.Count > 0)
        {
            string UserStat = dt.Rows[0]["LoginStatus"].ToString();
            string AgencyStat = dt.Rows[0]["AgencyLoginStat"].ToString();
            if (UserStat == "1" && AgencyStat == "1")
            {
                Out = "SUCCESS";
                HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                Int64 UID = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);

                GetRoleStatus(UID);
            }
            else
            {
                Out = "LOCKED";
            }

        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetSessionData(string sesname)
    {
        string SessionData = string.Empty;
        if (HttpContext.Current.Session[sesname] != null)
        {
            SessionData = HttpContext.Current.Session[sesname].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }
        return SessionData;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void ClearSessionData(string sesname)
    {
        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.Signout(SessionIDFinal);
        HttpContext.Current.Session.Clear();
    }
    [System.Web.Services.WebMethod]
    public static string GetCreditLimitAmount(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.AgencyID = AgencyID;
        DataTable dt = bals.GetCreditLimitAmount();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            //Out = "[{" + '"' + "MarkupTypeID" + '"' + ":0," + '"' + "MarkupValue" + '"' + ":0," + '"' + "CurrenyCode" + '"' + ":" + '"' + "AED" + '"' + "}]";
            Out = "NOCREDITLIMIT";
        }
        //HttpContext.Current.Session["MarkupData"] = Out;
        return Out;
    }


    public static void GetRoleStatus(Int64 UID)
    {
        //Get Roles
        BALSettings bals = new BALSettings();
        bals.UserID = UID;
        DataTable dtroles = bals.GetUserRoleStatus();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            HttpContext.Current.Session["UserRoleStatus"] = serializer1.Serialize(rows1);
        }
    }
    [System.Web.Services.WebMethod]
    public static string InsertTblPayment(string PaidAmount, string CurrencyCode, Int32 PaymentTypeID)
    {
        BALBook BALAD = new BALBook();
        BALAD.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        BALAD.PaidAmount = PaidAmount;
        BALAD.CurrencyCode = CurrencyCode;
        BALAD.PaymentTypeID = PaymentTypeID;
        int AirSegment = BALAD.InsertTblPayment();
        return AirSegment.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string GetTableData(string selectdata, string tablename, string condition)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.selectdata = selectdata;
        bals.tablename = tablename;
        bals.condition = condition;
        DataTable dtroles = bals.GetTableDatas();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string UpdateUserData(string Title, string FName, string LName, string MobNo)
    {
        BALSettings BALSET = new BALSettings();
        BALSET.Title = Title;
        BALSET.FName = FName;
        BALSET.LName = LName;
        BALSET.MobNo = MobNo;
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int AirSegment = BALSET.UpdateUserData();
        GetUserData(Convert.ToInt64(HttpContext.Current.Session["UserID"]));
        return AirSegment.ToString();
    }
    public static void GetUserData(Int64 UID)
    {
        BALSettings bals = new BALSettings();
        bals.UserID = UID;
        DataTable dt = bals.GetUserData();
        if (dt.Rows.Count > 0)
        {
            HttpContext.Current.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
            Int64 UIDD = Convert.ToInt64(dt.Rows[0]["UserID"].ToString());
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            HttpContext.Current.Session["UserDetails"] = serializer.Serialize(rows);
            GetRoleStatus(UIDD);
        }
    }
    [System.Web.Services.WebMethod]
    public static string UpdateUserPassword(string CurrentPass, string NewPass, string ConfPass)
    {
        string OUT = string.Empty;
        BALSettings BALSET = new BALSettings();
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        DataTable dt = BALSET.GetUserData();
        if (dt.Rows.Count > 0)
        {
            string DBPass = dt.Rows[0]["Password"].ToString();
            string DBPassDecrypt = BALSET.Decrypt(DBPass);
            if (DBPassDecrypt == CurrentPass)
            {
                BALSET.NewPass = BALSET.Encrypt(NewPass);
                int AirSegment = BALSET.UpdateUserPass();
                OUT = "PASSCHANGESUCCESS";
            }
            else
            {
                OUT = "PASSCHANGEFAILED";
            }
        }
        return OUT;
    }

    //Book
    public class objectName
    {
        public Int64 MarkupTypeID { get; set; }
        public Int64 MarkupValue { get; set; }
        public string CurrenyCode { get; set; }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirBookingCost(Double TotalBaseNett, Double TotalFaree, string NetCurrencyy, double XmlMarkupp)
    {

        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        if (HttpContext.Current.Session["MarkupData"] != null)
        {
            string markupdata = HttpContext.Current.Session["MarkupData"].ToString();
            List<objectName> obj = new List<objectName>(JsonConvert.DeserializeObject<objectName[]>(markupdata));
            Double MarkupID = Convert.ToDouble(obj[0].MarkupTypeID);
            if (MarkupID == 0)
            {
                BALAD.MarkupTypeID = 2;
                BALAD.MarkupValue = 0;
                BALAD.CurrenyCode = ConfigurationManager.AppSettings["dbCurrencyCode"];
            }
            else
            {
                BALAD.MarkupTypeID = Convert.ToDouble(obj[0].MarkupTypeID);
                BALAD.MarkupValue = Convert.ToDouble(obj[0].MarkupValue);
                BALAD.CurrenyCode = obj[0].CurrenyCode;
            }

        }

        if (BALAD.MarkupTypeID == 1)
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamount = TotalFaree * (MarkValue / 100);
            Double Sellamounttotal = TotalFaree + Sellamount;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        else
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamounttotal = TotalFaree + MarkValue;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        BALAD.TotalBaseNet = Convert.ToDouble(TotalBaseNett);
        BALAD.TotalFare = Convert.ToDouble(TotalFaree);
        BALAD.XmlMarkup = Convert.ToDouble(XmlMarkupp);
        BALAD.NetCurrency = NetCurrencyy;
        string sucess = "";
        DataTable AirBookingCost = BALAD.InsertAirBookingCost();
        if (AirBookingCost.Rows.Count > 0)
        {
            sucess = AirBookingCost.Rows[0]["BookingCostID"].ToString();
        }

        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirBookingCostBreakup(Int64 Costbreakupp, Double TotalFaree, Double Taxx, Double BaseFaree, string PassengerTypee, Int64 PassengerQuantityy, string NetCurrencyy, int GrandTotalPax, double xmlfarebreakupp, Double TotalOgFare)
    {
        BALBook BALAD = new BALBook();
        BALAD.Costbreakup = Convert.ToInt64(Costbreakupp);
        BALAD.TotalFare = Convert.ToDouble(TotalFaree);
        BALAD.BaseFare = Convert.ToDouble(BaseFaree);
        BALAD.Tax = Convert.ToDouble(Taxx);
        BALAD.NetCurrency = NetCurrencyy;
        Double OGMarkupValue = 0;
        if (HttpContext.Current.Session["MarkupData"] != null)
        {
            string markupdata = HttpContext.Current.Session["MarkupData"].ToString();
            List<objectName> obj = new List<objectName>(JsonConvert.DeserializeObject<objectName[]>(markupdata));
            Double MarkupID = Convert.ToDouble(obj[0].MarkupTypeID);
            OGMarkupValue = Convert.ToDouble(obj[0].MarkupValue);
            if (MarkupID == 0)
            {
                BALAD.MarkupTypeID = 2;
                BALAD.MarkupValue = 0;
                BALAD.XMLMarkupValueDivision = xmlfarebreakupp / GrandTotalPax;
                BALAD.CurrenyCode = ConfigurationManager.AppSettings["dbCurrencyCode"];
            }
            else
            {
                BALAD.MarkupTypeID = Convert.ToDouble(obj[0].MarkupTypeID);
                BALAD.MarkupValue = Convert.ToDouble(obj[0].MarkupValue) / GrandTotalPax;
                BALAD.XMLMarkupValueDivision = xmlfarebreakupp / GrandTotalPax;
                BALAD.CurrenyCode = obj[0].CurrenyCode;
            }
        }
        if (BALAD.MarkupTypeID == 1)
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamount = (TotalOgFare * (OGMarkupValue / 100)) / GrandTotalPax;
            BALAD.MarkupValue = Sellamount;
            Double Sellamounttotal = TotalFaree + Sellamount;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        else
        {
            Double MarkValue = Convert.ToDouble(BALAD.MarkupValue);
            Double Sellamounttotal = TotalFaree + MarkValue;
            BALAD.SellAmount = Convert.ToDouble(Sellamounttotal);
        }
        BALAD.PassengerType = PassengerTypee;
        BALAD.PassengerQuantity = Convert.ToInt64(PassengerQuantityy);
        int AirBookingCostBreakup = BALAD.InsertAirBookingCostBreakup();
        string sucess = AirBookingCostBreakup.ToString();
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirsegmentBookingAvail(List<string> FareBasisArrayy)
    {
        BALBook BALAD = new BALBook();
        string sucess = "0";
        HttpContext.Current.Session["FareBasisArray"] = FareBasisArrayy;
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertAirOriginDestinationOptions(List<string> RefNumberArryyy, List<string> DirectionIdArryyy, List<string> ElapsedTimeArryyy, List<string> FromlocArryyyy, List<string> TolocArryyy, List<string> depttimeArryyy, List<string> arritimeArryyy, List<string> FnoArryyy, List<string> depterminalArryyy, List<string> arriterminalArryyy, List<string> operatingairlineArryyy, List<string> EquipmentArryyy, List<string> MarketingAirlineArryyy, string PN, string CANCEL, List<string> FlightSegCount, List<string> FareBasisArrayy, List<string> DepartureDateTimeArrayy)
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        BALAD.PNR = PN;
        string cancel = CANCEL;
        BALAD.CANCELDATE = Convert.ToDateTime(CANCEL);
        int Success = BALAD.upadtepnrBooking();
        int Updateair = BALAD.Upadtepnrairpassenger();

        for (int i = 0; i < RefNumberArryyy.Count; i++)
        {
            BALAD.REFNO = RefNumberArryyy[i].ToString();
            BALAD.DIRECTIONID = DirectionIdArryyy[i].ToString();
            BALAD.ELASPED = ElapsedTimeArryyy[i].ToString();
            DataTable UpdateAirOrigin = BALAD.InsertAirOriginOptions();
            if (UpdateAirOrigin.Rows.Count > 0)
            {
                BALAD.OriginDestinationID = UpdateAirOrigin.Rows[0]["OriginDestinationID"].ToString();
                for (int j = 0; j < FromlocArryyyy.Count; j++)
                {
                    BALAD.Depttime = Convert.ToDateTime(depttimeArryyy[j].ToString());
                    BALAD.Arritime = Convert.ToDateTime(arritimeArryyy[j].ToString());
                    BALAD.Fno = FnoArryyy[j].ToString();
                    BALAD.Fromloc = FromlocArryyyy[j].ToString();
                    BALAD.Depterminal = depterminalArryyy[j].ToString();
                    BALAD.Toloc = TolocArryyy[j].ToString();
                    BALAD.Arriterminal = arriterminalArryyy[j].ToString();
                    BALAD.Operatingairline = operatingairlineArryyy[j].ToString();
                    BALAD.Equipment = EquipmentArryyy[j].ToString();
                    BALAD.MarketingAirline = MarketingAirlineArryyy[j].ToString();

                    DataTable AirSegment = BALAD.InsertAirSegment();
                    if (AirSegment.Rows.Count > 0)
                    {
                        sucess = AirSegment.Rows[0]["SegmentID"].ToString();
                    }
                }

            }

        }
        BookingAvailableinsertion(FareBasisArrayy);
        StopOverInsertion(DepartureDateTimeArrayy);
        return sucess;
    }
    public static string BookingAvailableinsertion(List<string> FareBasisArray)
    {
        List<string> FareBasisArrayy = new List<string>();
        string sucess;
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        //if (HttpContext.Current.Session["FareBasisArray"] != null)
        //{
        //    FareBasisArrayy = (List<string>)HttpContext.Current.Session["FareBasisArray"];
        FareBasisArrayy = FareBasisArray;

        for (int j = 0; j < FareBasisArrayy.Count; j++)
        {
            string strVale = FareBasisArrayy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.Fromloc = arr[4].ToString();
            BALAD.Toloc = arr[5].ToString();
            BALAD.REFNO = arr[6].ToString();
            BALAD.DIRECTIONID = arr[7].ToString();
            DataTable SegmentID = BALAD.GetSegmentID();
            if (SegmentID.Rows.Count > 0)
            {
                BALAD.SegmentID = Convert.ToInt64(SegmentID.Rows[0]["SegmentID"].ToString());
                BALAD.ResBookDesigCabinCode = arr[0].ToString();
                BALAD.ResBookDesigCode = arr[1].ToString();
                BALAD.AvailablePTC = arr[2].ToString();
                BALAD.FareBasis = arr[3].ToString();
                int AirsegmentBookingAvail = BALAD.InsertAirsegmentBookingAvail();
            }
        }
        //}
        sucess = "0";
        return sucess;
    }
    public static string StopOverInsertion(List<string> DepartureDateTimeArrayy)
    {
        List<string> DepartureDateTimeArrayyy = new List<string>();
        string sucess;
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        DepartureDateTimeArrayyy = DepartureDateTimeArrayy;
        for (int j = 0; j < DepartureDateTimeArrayyy.Count; j++)
        {
            string strVale = DepartureDateTimeArrayyy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.Fromloc = arr[3].ToString();
            BALAD.Toloc = arr[4].ToString();
            BALAD.REFNO = arr[5].ToString();
            BALAD.DIRECTIONID = arr[6].ToString();
            DataTable SegmentID = BALAD.GetSegmentID();
            if (SegmentID.Rows.Count > 0)
            {
                BALAD.SegmentID = Convert.ToInt64(SegmentID.Rows[0]["SegmentID"].ToString());
                BALAD.LocationCode = arr[0].ToString();
                BALAD.DepartureDateTime = Convert.ToDateTime(arr[1].ToString());
                BALAD.ArrivalDateTime = Convert.ToDateTime(arr[2].ToString());
                int AirsegmentStopOver = BALAD.InsertAirsegmentStopOver();
            }
        }
        sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirbaggageDetails(string PassengerTypee, int Quantityy, string Unitt, string frombaggsegg, string Tobaggsegg)
    {
        BALBook BALAD = new BALBook();

        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }

        string success = "";
        BALAD.PassengerType = PassengerTypee;
        BALAD.Quantity = Quantityy;
        BALAD.Unit = Unitt;
        BALAD.Fromloc = frombaggsegg;
        BALAD.Toloc = Tobaggsegg;
        int AirbaggageDetails = BALAD.insertAirbaggageDetails();
        success = "0";
        return success;

    }
    [System.Web.Services.WebMethod]
    public static string ForgotPass(string Email)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.EmailID = Email;
        DataTable dt = bals.GetUserDataEmail();
        if (dt.Rows.Count > 0)
        {
            string PassDecry = bals.Decrypt(dt.Rows[0]["Password"].ToString());
            string FName = dt.Rows[0]["FirstName"].ToString();
            string LName = dt.Rows[0]["LastName"].ToString();
            string EmailTo = dt.Rows[0]["EmailID"].ToString();

            DataTable dt2 = bals.GetEmailTemplate(2);
            if (dt2.Rows.Count > 0)
            {
                string EmailSub = dt2.Rows[0]["Subject"].ToString();
                string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
                EmailBody = EmailBody.Replace("#PersonName#", FName + ' ' + LName);
                EmailBody = EmailBody.Replace("#Password#", PassDecry);
                bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
                Out = "SUCCESS";
            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingPaxInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingPaxInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string GetBookingTransInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingTransInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingBaggageInfo()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingBagInfo();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string Getbaggageinfo(string sequencenu, string combinationi)
    {
        string SessionIDFinal = "";
        if (HttpContext.Current.Session["GlobelSession"] != null)
        {
            SessionIDFinal = HttpContext.Current.Session["GlobelSession"].ToString();
        }
        SendWebServiceRequest SWSR = new SendWebServiceRequest();
        string xmlResponse = SWSR.BaggageInfo(sequencenu, combinationi, SessionIDFinal);
        return xmlResponse;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string DeletebookingInfo()
    {
        BALBook BALAD = new BALBook();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        int DeletebookingInfo = BALAD.DeletebookingInfo();
        string sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod]
    public static string GetIssueTktDataDB()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetIssueTktDataDB();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string UpdateTkt(string TicketNumber, string PersonName, string surname, string BirthDate)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);

        List<string> TicketNumberArray = TicketNumber.Split(',').ToList<string>();
        List<string> PersonNameArray = PersonName.Split(',').ToList<string>();
        List<string> surnameArray = surname.Split(',').ToList<string>();
        List<string> BirthDateArray = BirthDate.Split(',').ToList<string>();
        for (int itn = 0; itn < TicketNumberArray.Count; itn++)
        {
            bals.TicketNumber = TicketNumberArray[itn];
            bals.PersonName = PersonNameArray[itn];
            bals.surname = surnameArray[itn];
            bals.BirthDatepax = BirthDateArray[itn];
            int Tkt = bals.UpdateTkt();
        }
        return 1.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string UpdateBookStatus(string bstat)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        bals.BookStatus = bstat;
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int Tkt = bals.UpdateBookStatus();
        return Tkt.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string InsertBookHistory(string bstat)
    {
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        bals.BookStatus = bstat;
        bals.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        int Tkt = bals.InsertBookHistory();
        return Tkt.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string GetSearchData(string DepartureCity, string ArrivalCity, string DepartureDateFrom, string DepartureDateTo, string ArrivalDateFrom, string ArrivalDateTo, string BookingDateFrom, string BookingDateTo, string DeadlineDateFrom, string DeadlineDateTo, string BookingRefNo, string SupplierRefNo, string TicketNo, string PassengerName, string ddlBranch, string ddlStatus)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.DepartureCity = DepartureCity;
        bals.ArrivalCity = ArrivalCity;
        bals.DepartureDateFrom = DepartureDateFrom;
        bals.DepartureDateTo = DepartureDateTo;
        bals.ArrivalDateFrom = ArrivalDateFrom;
        bals.ArrivalDateTo = ArrivalDateTo;
        bals.BookingDateFrom = BookingDateFrom;
        bals.BookingDateTo = BookingDateTo;
        bals.DeadlineDateFrom = DeadlineDateFrom;
        bals.DeadlineDateTo = DeadlineDateTo;
        bals.BookingRefNo = BookingRefNo;
        bals.SupplierRefNo = SupplierRefNo;
        bals.TicketNo = TicketNo;
        bals.PassengerName = PassengerName;
        bals.ddlBranch = Convert.ToInt64(ddlBranch);
        bals.ddlStatus = ddlStatus;

        DataTable dtroles = bals.GetSearchData();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static void SaveDataToSession(string sessionname, string valueses)
    {
        HttpContext.Current.Session[sessionname] = valueses;
    }
    [System.Web.Services.WebMethod]
    public static string SendMailTicketIssue(string EUName, string ETripID, string EPaxData, string UID, string UserMailID, string ETripName, string EFlightLeg, string ETotalCharge, string Type, string ETktAttachMainLeg, string ETktAttachTraveller, string ETktAttachFareBreakup, string ETktBaggageInfo, string DeadlineDiv)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.UserID = Convert.ToInt64(UID);
        DataTable dt = bals.SendMailTicketIssueCCList();
        if (dt.Rows.Count > 0)
        {
            String CCLIST = dt.AsEnumerable()
                     .Select(row => row["EmailAddress"].ToString())
                     .Aggregate((s1, s2) => String.Concat(s1, "," + s2));
            string host2 = HttpContext.Current.Request.Url.Host.ToLower();
            if (host2 == "localhost")
            {
                CCLIST = "shabeerm@oneviewit.com";
            }
            //Checking Voucher Link Availability
            string AttachLink = string.Empty;
            bool voucherlinkavailable = false;
            BALSettings BALSET = new BALSettings();
            if (HttpContext.Current.Session["BookingRefID"] != null)
            {
                BALSET.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
                DataTable dtcheckvou = BALSET.CheckVochurlinkavailable();
                if (dtcheckvou.Rows.Count > 0)
                {//Link Available
                    voucherlinkavailable = true;
                    AttachLink = dtcheckvou.Rows[0]["VoucherUrl"].ToString();
                }
                else
                {
                    voucherlinkavailable = false;
                }
            }

            using (WebClient client = new WebClient())
            {
                string ticks = DateTime.Now.Ticks.ToString();

                string htmlCode = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/bookingmail.html");
                if (voucherlinkavailable == false)
                {
                    string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/ticket_attachment_template.html");
                    ticket_attachment = ticket_attachment.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                    ticket_attachment = ticket_attachment.Replace("#AGENCYNAME#", ConfigurationManager.AppSettings["AgencyShortName"]);
                    ticket_attachment = ticket_attachment.Replace("#AgencyPhoneNumber#", ConfigurationManager.AppSettings["AgencyPhoneNumber"]);
                    ticket_attachment = ticket_attachment.Replace("#ETripID#", ETripID);
                    ticket_attachment = ticket_attachment.Replace("#FLIGHTDETAILS#", ETktAttachMainLeg);
                    ticket_attachment = ticket_attachment.Replace("#PAXDET#", ETktAttachTraveller);
                    ticket_attachment = ticket_attachment.Replace("#FAREBREAKUPS#", ETktAttachFareBreakup);
                    ticket_attachment = ticket_attachment.Replace("#BAGGAGEDATA#", ETktBaggageInfo);


                    string storePath = System.Web.HttpContext.Current.Server.MapPath("") + "/Documents/TicketAttachments/" + ETripID + "/";
                    if (!Directory.Exists(storePath))
                        Directory.CreateDirectory(storePath);
                    //if (File.Exists(filepathee))
                    //{
                    //    File.Delete(filepathee);
                    //}
                    TextWriter attachhtml = new StreamWriter(HttpContext.Current.Server.MapPath("") + "/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html");
                    attachhtml.WriteLine(ticket_attachment);
                    attachhtml.Flush();
                    attachhtml.Close();
                    attachhtml.Dispose();

                    string vouurl = "/FLIGHT/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html";
                    BALSET.VoucherUrl = vouurl;
                    string host = HttpContext.Current.Request.Url.Host.ToLower();
                    if (host != "localhost")
                    {
                        int dtVoucher = BALSET.insertVoucherUrlinformation();
                    }
                    AttachLink = vouurl;
                }

                //DataTable dt2 = bals.GetEmailTemplate(1);
                //if (dt2.Rows.Count > 0)
                //{
                string EmailSub = "Your #SUBTEXT# - Trip ID #ETripID#";
                string EmailBody = htmlCode;
                EmailSub = EmailSub.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EUName#", EUName);
                EmailBody = EmailBody.Replace("#ETripID#", ETripID);
                EmailBody = EmailBody.Replace("#EFlightLeg#", EFlightLeg);
                EmailBody = EmailBody.Replace("#EPaxData#", EPaxData);
                EmailBody = EmailBody.Replace("#TRIPNAME#", ETripName);
                EmailBody = EmailBody.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
                if (Type == "TKT")//TKT
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Ticket Issued");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Ticket Attached");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "confirmed");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Your itinerary is attached along with this email");
                }
                else if (Type == "PNR")//PNR
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", ETotalCharge);
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Trip is Booked");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Trip Information");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "booked");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Please find the trip details below");
                }
                else if (Type == "TKTTOMAIL")//TKTTOMAIL
                {
                    EmailBody = EmailBody.Replace("#ETotalCharge#", "");
                    EmailSub = EmailSub.Replace("#SUBTEXT#", "Itinerary");
                    EmailBody = EmailBody.Replace("#MAILHEADER#", "Trip Information");
                    EmailBody = EmailBody.Replace("#STATUSBOOK#", "confirmed");
                    EmailBody = EmailBody.Replace("#ATTACHDET#", "Your itinerary is attached along with this email");
                    CCLIST = "";
                }
                EmailBody = EmailBody.Replace("#EDeadline#", DeadlineDiv);
                //Out = bals.SendMailSubjectwithBcc(UserMailID, "", "", EmailBody, EmailSub, "~/Documents/TicketAttachments/" + ETripID + "/ticket_attachment_" + ETripID + "_" + ticks + ".html");
                Out = bals.SendMailSubjectwithBcc(UserMailID, CCLIST, "", EmailBody, EmailSub, AttachLink);
                //}
                //Delete Files
                //if (Directory.Exists(storePath))
                //{
                //    //Delete all files from the Directory
                //    foreach (string file in Directory.GetFiles(storePath))
                //    {
                //        File.Delete(file);
                //    }
                //    //Delete a Directory
                //    Directory.Delete(storePath);
                //}
            }
        }
        else
        {
            Out = "NOUSER";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string ChangeStatustoHK()
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
            BALAD.BookingStatusCode = "HK";
            BALAD.BOOKDATE = System.DateTime.Now;
            BALAD.UID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
            int successss = BALAD.InsertBookingHistoryDetails();
        }
        return sucess;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingHistory()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookingHistory();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CancelBookingMail(string PNRC, string ETripIDD, string EUNamee, string ECommentt)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt2 = bals.GetEmailTemplate(4);
        if (dt2.Rows.Count > 0)
        {
            string EmailSub = dt2.Rows[0]["Subject"].ToString();
            string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
            string EmailTo = dt2.Rows[0]["Recipient"].ToString();

            EmailSub = EmailSub.Replace("#BOOKINGREFID#", ETripIDD);
            EmailSub = EmailSub.Replace("#PNRNUMBER#", PNRC);

            EmailBody = EmailBody.Replace("#USERNAME#", EUNamee);
            EmailBody = EmailBody.Replace("#BOOKINGREFID#", ETripIDD);
            EmailBody = EmailBody.Replace("#PNRNUMBER#", PNRC);
            EmailBody = EmailBody.Replace("#COMMENT#", ECommentt);
            bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string ChangeTravelDateMail(string PNRC, string ETripIDD, string EUNamee, string ECommentt)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        DataTable dt2 = bals.GetEmailTemplate(5);
        if (dt2.Rows.Count > 0)
        {
            string EmailSub = dt2.Rows[0]["Subject"].ToString();
            string EmailBody = dt2.Rows[0]["EmailBody"].ToString();
            string EmailTo = dt2.Rows[0]["Recipient"].ToString();

            EmailSub = EmailSub.Replace("#BOOKINGREFID#", ETripIDD);
            EmailSub = EmailSub.Replace("#PNRNUMBER#", PNRC);

            EmailBody = EmailBody.Replace("#USERNAME#", EUNamee);
            EmailBody = EmailBody.Replace("#BOOKINGREFID#", ETripIDD);
            EmailBody = EmailBody.Replace("#PNRNUMBER#", PNRC);
            EmailBody = EmailBody.Replace("#COMMENT#", ECommentt);
            bals.SendMailSubjectwithBcc(EmailTo, "", "", EmailBody, EmailSub, "");
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetFareRules()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetFareRules();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }

    [System.Web.Services.WebMethod]
    public static string GetFareBreak()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetFareBreak();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string insertfarerules(List<string> farerulee)
    {
        BALBook BALAD = new BALBook();
        string sucess = "";
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        //List<string> FareRuleArrayy = new List<string>();
        //FareRuleArrayy = fullFareDataa;
        for (int j = 0; j < farerulee.Count; j++)
        {
            string strVale = farerulee[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.FareRule = arr[0].ToString();
            BALAD.Segment = arr[1].ToString() + "-" + arr[2].ToString();
            BALAD.FareRef = arr[3].ToString(); ;
            BALAD.Operatingairline = arr[4].ToString();
            BALAD.MarketingAirline = arr[5].ToString();
            DataTable FareRule = BALAD.InsertFareRule();
            if (FareRule.Rows.Count > 0)
            {
                sucess = "Success";

            }
            else
            {
                sucess = "Failed";
            }
        }



        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirbaggageDetails(List<string> Airbaggagee)
    {
        BALBook BALAD = new BALBook();
        string success = "";
        List<string> AirbaggageeArrayy = new List<string>();
        if (HttpContext.Current.Session["BookingRefID"] != null)
        {
            BALAD.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        }
        AirbaggageeArrayy = Airbaggagee;

        for (int j = 0; j < AirbaggageeArrayy.Count; j++)
        {
            string strVale = AirbaggageeArrayy[j].ToString();
            List<string> arr = strVale.Split(',').ToList<string>();
            BALAD.PassengerType = arr[0].ToString();
            BALAD.Quantity = Convert.ToInt32(arr[1].ToString());
            BALAD.Unit = arr[2].ToString();
            BALAD.Fromloc = arr[3].ToString();
            BALAD.Toloc = arr[4].ToString();
            int AirbaggageDetails = BALAD.insertAirbaggageDetails();
        }
        success = "0";
        return success;

    }
    [System.Web.Services.WebMethod]
    public static string GetBookerAgencyDetails()
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        //bals.BookingRefID = 347;
        bals.BookingRefID = Convert.ToInt64(HttpContext.Current.Session["BookingRefID"]);
        DataTable dt = bals.GetBookerAgencyDetails();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string updatelastticketingdate(string bookingIDD, string CancelDatee)
    {
        string Out = string.Empty;
        BALBook BALAD = new BALBook();
        BALAD.BOOKINGREFID = bookingIDD.ToString();
        BALAD.CANCELDATE = Convert.ToDateTime(CancelDatee.ToString());
        DataTable dtcanceldate = BALAD.Updatelasttktdate();
        if (dtcanceldate.Rows.Count > 0)
        {
            Out = "Success";
        }
        else
        {
            Out = "NOINFO";
        }
        return Out;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string AirsegmentStopOver(List<string> DepartureDateTimeArrayy)
    {
        HttpContext.Current.Session["DepartureDateTimeArrayy"] = DepartureDateTimeArrayy;
        string sucess = "0";
        return sucess;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string DeleteVoucherUrlinformation()
    {
        BALSettings BALSET = new BALSettings();
        BALSET.BOOKINGREFID = HttpContext.Current.Session["BookingRefID"].ToString();
        int Success = BALSET.DeleteVoucherUrlinformation();
        return Success.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string PrintTicket(string ETripID, string ETktAttachMainLeg, string ETktAttachTraveller, string ETktAttachFareBreakup, string ETktBaggageInfo)
    {
        string Out = "NODATA";
        using (WebClient client = new WebClient())
        {
            string ticket_attachment = client.DownloadString(System.Web.HttpContext.Current.Server.MapPath("") + "/emailtemplates/ticket_attachment_template.html");
            ticket_attachment = ticket_attachment.Replace("#ETripID#", ETripID);
            ticket_attachment = ticket_attachment.Replace("#FLIGHTDETAILS#", ETktAttachMainLeg);
            ticket_attachment = ticket_attachment.Replace("#PAXDET#", ETktAttachTraveller);
            ticket_attachment = ticket_attachment.Replace("#FAREBREAKUPS#", ETktAttachFareBreakup);
            ticket_attachment = ticket_attachment.Replace("#BAGGAGEDATA#", ETktBaggageInfo);
            ticket_attachment = ticket_attachment.Replace("#URL#", "" + ConfigurationManager.AppSettings["PortalURL"] + "/COMMON/images/logo.png");
            ticket_attachment = ticket_attachment.Replace("#AGENCYNAME#", ConfigurationManager.AppSettings["AgencyShortName"]);
            ticket_attachment = ticket_attachment.Replace("#AgencyPhoneNumber#", ConfigurationManager.AppSettings["AgencyPhoneNumber"]);
            Out = ticket_attachment;
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CRUDBlockService(string AgencyId, string ServiceId, string Operation)
    {
        BALSettings BALSET = new BALSettings();
        BALSET.AgencyID = Convert.ToInt64(AgencyId);
        BALSET.ServiceTypeID = Convert.ToInt32(ServiceId);
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALSET.Operation = Operation;
        BALSET.BlockDate = DateTime.Now;
        int CrudSuccess = BALSET.CRUDBlockService();
        return CrudSuccess.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string checkServiceBlocked(string AgencyId, string ServiceTypeID)
    {
        int BlockedStat = 1;
        BALSettings BALSET = new BALSettings();
        BALSET.AgencyID = Convert.ToInt64(0);
        BALSET.ServiceTypeID = Convert.ToInt32(ServiceTypeID);
        DataTable dtCheckServiceBlockAll = BALSET.checkServiceBlocked();
        if (dtCheckServiceBlockAll.Rows.Count > 0)
        {
            BlockedStat = 1;
        }
        else
        {
            BALSET.AgencyID = Convert.ToInt64(AgencyId);
            DataTable dtCheckServiceBlockAgency = BALSET.checkServiceBlocked();
            if (dtCheckServiceBlockAgency.Rows.Count > 0)
            {
                BlockedStat = 1;
            }
            else
            {
                BlockedStat = 0;
            }
        }
        return BlockedStat.ToString();
    }
    [System.Web.Services.WebMethod]
    public static string GetActionReqFlight(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        DataTable dt = baldbhot.GetActionReqFlight();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
}