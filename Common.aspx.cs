﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.Configuration;

public partial class Common : System.Web.UI.Page
{
    public static string HubURL = WebConfigurationManager.AppSettings["HubURL"];
    public static string HubPort = WebConfigurationManager.AppSettings["HubPort"];
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public enum TokenType
    {
        Bearer
    }

    public enum RequestType
    {
        GET, POST, PUT, DELETE
    }

    [System.Web.Services.WebMethod]
    public static string ForgotPass(string Email)
    {

        string SessionData = string.Empty;
        string Out = string.Empty;
        string XMLData = "{\"emailId\": \""+ Email + "\"}";

        String InterfaceURL = HubURL + ":" + HubPort + "/profile/forgot";
        string strResult = "NOMARKUP";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json"; 
            httpWebRequest.Method = RequestType.POST.ToString();

            string request = JsonConvert.SerializeObject(XMLData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                //streamWriter.Flush();
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            Stream receiveStream = httpResponse.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();           
        }
        catch (Exception ex)
        {
        }
        return strResult;

    }

    [System.Web.Services.WebMethod]
    public static string GetSumDepoCreditPayment(Int64 AgencyID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        DataTable dt = baldbhot.GetSumDepoCreditPayment();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetBookingStatCount(Int64 AgencyID, int ServiceTypeId,Int64 UserID)
    {
        string Out = string.Empty;
        BALDBHotel baldbhot = new BALDBHotel();
        baldbhot.AgencyID = AgencyID;
        baldbhot.USERID = UserID;
        baldbhot.ServiceTypeId = ServiceTypeId;
        DataTable dt = baldbhot.GetBookingStatCount();
        if (dt.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Out = serializer.Serialize(rows);
        }
        else
        {
            Out = "NODATA";
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string GetTableData(string selectdata, string tablename, string condition)
    {
        string Out = string.Empty;
        BALSettings bals = new BALSettings();
        bals.selectdata = selectdata;
        bals.tablename = tablename;
        bals.condition = condition;
        DataTable dtroles = bals.GetTableDatas();
        if (dtroles.Rows.Count > 0)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer1 = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row1;
            foreach (DataRow dr1 in dtroles.Rows)
            {
                row1 = new Dictionary<string, object>();
                foreach (DataColumn col1 in dtroles.Columns)
                {
                    row1.Add(col1.ColumnName, dr1[col1]);
                }
                rows1.Add(row1);
            }
            Out = serializer1.Serialize(rows1);
        }
        return Out;
    }
    [System.Web.Services.WebMethod]
    public static string CRUDBlockService(string AgencyId, string ServiceId, string Operation)
    {
        BALSettings BALSET = new BALSettings();
        BALSET.AgencyID = Convert.ToInt64(AgencyId);
        BALSET.ServiceTypeID = Convert.ToInt32(ServiceId);
        BALSET.UserID = Convert.ToInt64(HttpContext.Current.Session["UserID"]);
        BALSET.Operation = Operation;
        BALSET.BlockDate = DateTime.Now;
        int CrudSuccess = BALSET.CRUDBlockService();
        return CrudSuccess.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void ClearAllSession()
    {
        HttpContext.Current.Session.Clear();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetServiceMarkup(string Currency, string Servicetype) //advancedMarkup
    {

        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = "{  \"criteria\": {            \"service\": \"Hotel\"  }   } ";
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "markup/fetchapplicablemarkups"; ;
        string strResult = "NOMARKUP";
        string tempResultJSON = "[";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.PreAuthenticate = true;
            httpWebRequest.Headers.Add("Authorization", TokenType.Bearer.ToString() + " " + SessionData);
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = RequestType.POST.ToString();

            string request = JsonConvert.SerializeObject(XMLData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                //streamWriter.Flush();
            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var headerresponse = httpResponse.Headers;
            string accesstocken = headerresponse["access_token"];
            HttpContext.Current.Session["Access_Token"] = accesstocken;
            Stream receiveStream = httpResponse.GetResponseStream();
            StreamReader sr = new StreamReader(receiveStream);
            strResult = sr.ReadToEnd();

            //////// post document 
            //////System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //////Byte[] byte1 = encoding.GetBytes(XMLData);
            //////ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //////WebRequest HttpWReq = WebRequest.Create(InterfaceURL);
            //////HttpWReq.ContentType = "application/JSON";
            ///////////
            //////HttpWReq.Headers.Add("Accept-Encoding", "application/JSON");
            ///////////
            //////HttpWReq.ContentLength = XMLData.Length;
            ////////      HttpWReq.ContentLength = 331;
            //////HttpWReq.Method = "POST";
            ////////HttpWReq.Timeout = 3;                
            //////Stream StreamData = HttpWReq.GetRequestStream();
            //////StreamData.Write(byte1, 0, byte1.Length);
            //////// get response
            //////WebResponse HttpWRes = HttpWReq.GetResponse();

            //////Stream receiveStream = HttpWRes.GetResponseStream();
            //////StreamReader sr = new StreamReader(receiveStream);
            //////strResult = sr.ReadToEnd();
            //////string strHeaders = HttpWRes.Headers.ToString();

            Int64 tempLevel = 0;
            JObject jObject = JObject.Parse(strResult);
            string tempCount = jObject["groupedApplicableMarkupList"].Count().ToString();
            foreach (var nodeItem in jObject["groupedApplicableMarkupList"])
            {
                foreach (var ruleItem in nodeItem["applicableMarkupList"])
                {
                    if (tempLevel != 0)
                    {
                        tempResultJSON += ",";
                    }
                    string rPercentage = (string)ruleItem["percentage"];
                    string rAmount = (string)ruleItem["amount"];
                    string rSupplier = (string)ruleItem["rsCriteria"]["supplier"];
                    // tempResultJSON += "{\\\"L\\\"=" + tempLevel + ",\\\"V1\\\"=0,\\\"T1\\\"=0,\\\"V2\\\"=" + percent + ",\\\"T2\\\"=1,\\\"Agency\\\"=" + tempLevel + "}";
                    //tempResultJSON += "{\"L\":" + tempLevel + ",\"V1\":0,\"T1\":0,\"V2\":" + percent + ",\"T2\":1,\"Agency\":" + tempLevel + "}";
                    //    tempResultJSON += "{'L'=" + tempLevel + ",'V1'=0,'T1'=0,'V2'=100,'T2'=1,'Agency'=" + tempLevel + "}";
                    //  tempResultJSON += "{'L'=" + tempLevel + ",'V1'=0,'T1'=0,'V2'=100,'T2'=1,'Agency'=" + tempLevel + "}";
                    // tempResultJSON += "{L=" + tempLevel + ",V1=0,T1=0,V2=100,T2=1,Agency=" + tempLevel + "}";

                    if (rSupplier == "24*7")
                    {
                        rSupplier = "R24";
                    }

                    tempResultJSON += JsonConvert.SerializeObject(new { amount = rAmount, percentage = rPercentage, supplier = String.IsNullOrWhiteSpace(rSupplier) ? "Any" : rSupplier });

                    //tempResultJSON += "{\"L\":\"" + tempLevel + "\",\"V1\":\"0\",\"T1\":\"0\",\"V2\":\"" + percent + "\",\"T2\":\"1\",\"Agency\":\"" + tempLevel + "\"}";

                    //    tempResultJSON += "{\"L\":\"" + tempLevel + "\",\"V1\":\"0\",\"T1\":\"0\",\"V2\":\"10\",\"T2\":\"1\",\"Agency\":\"" + tempLevel + "\"}";


                    tempLevel++;



                }
            }
            tempResultJSON += "]";

            string markupdata = tempResultJSON;
            //    string markupdata = "{\"Markup\":" + tempResultJSON + "}";

            //   strResult = JsonConvert.SerializeObject(markupdata);
            //strResult = strResult.Trim('"');
            strResult = tempResultJSON;


            //   strResult = "NOMARKUP";
            //   HttpContext.Current.Session["MarkupDataHotel"] = strResult;
            //   List<Markupss> fdf = JsonConvert.DeserializeObject<List<Markupss>>(tempResultJSON);
        }
        catch (Exception ex)
        {
        }
        return strResult;
    }
    [System.Web.Services.WebMethod]
    public static string GetServiceMarkupbkup(string Currency, string Servicetype) //advancedMarkup
    {
        //return "success";
        string userid = HttpContext.Current.Session["UserID"].ToString();
        // userid = "252";
        SqlCommand cmd = new SqlCommand();
        DataTable dtbook = new DataTable();
        Dalref Dalref = new Dalref();
        string curr = Currency;
        //Database3 db3 = new Database3();
        //string sql = "insert into tblBooking(BookingID,Position,UserID,ServiceTypeId,BookingDate,SupplierID,SupplierBookingReference,CancellationDeadline,BookingStatusCode,AgencyRemarks,PaymentStatusID) values(23,1,'" + userid + "',5,'" + bookdate + "',2,'nil','" + bookdate + "','RQ','nil',1)";
        //db3.executeNonQuery(sql);
        //return "success";
        cmd.Parameters.Clear();
        cmd.Parameters.AddWithValue("@CurrenyCode", curr);
        cmd.Parameters.AddWithValue("@ServiceTypeID", Servicetype);
        cmd.Parameters.AddWithValue("@UserID", userid);


        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "procgetmarkupbyuserid";//procgetadvancedmarkupbyuserid
     //   cmd.CommandText = "procgetsinglelevelmarkupbyuserid";//procgetadvancedmarkupbyuserid
        dtbook = Dalref.ExeReader(cmd);

        string successResponse = DataTableToJSONWithStringBuilder(dtbook);
        return successResponse;

    }

    public static string DataTableToJSONWithStringBuilder(DataTable table)
    {
        var JSONString = new StringBuilder();
        if (table.Rows.Count > 0)
        {
            JSONString.Append("[");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                JSONString.Append("{");
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (j < table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                    }
                    else if (j == table.Columns.Count - 1)
                    {
                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                    }
                }
                if (i == table.Rows.Count - 1)
                {
                    JSONString.Append("}");
                }
                else
                {
                    JSONString.Append("},");
                }
            }
            JSONString.Append("]");
        }
        return JSONString.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotel(string cin, string cout, string night, string numroom, string rooms, string roomcategoryid, string CityCode, string HotelCode, string FNames, string NamePrefixx, string Surnamee, string CountryCode, List<string> ExpectedPrice, string Curr, string Lang, string SupplierCode, string HotelName, string RoomDes, string AgencyCode, List<string> DebitId, string HotelAddress, string Starcount, string EmailID, string Contactno, string nation, string ChargeConditionss)
    {

        List<string> FNamesArray = FNames.Split(',').ToList<string>();//aaaaa,bbbbbbbb,ccccccccc,dddddd
        List<string> NamePrefixArray = NamePrefixx.Split(',').ToList<string>();//3,3,2,5
        List<string> SurnameArray = Surnamee.Split(',').ToList<string>();//aaaaa,bbbbbbbb,ccccccccc,dddddd
        List<string> RoomsArray = rooms.Split('/').ToList<string>();//"ADT_1,CHD_0","ADT_1,CHD_2_15_3"
        List<string> roomcategoryidArray ;
        if (SupplierCode == "R24"||SupplierCode == "GRNC")
        {
            roomcategoryidArray = SplitAtOccurence(roomcategoryid,',', 3);
        }
        else
        {
            roomcategoryidArray = roomcategoryid.Split(',').ToList<string>();//"001:ALB:8729:S8624:9680:39984","001:ALB:8729:S8624:9680:39975"
        }
        List<string> ExpectedPriceArray = ExpectedPrice;//"297.5","353"
        string CityCodee = CityCode;//DXB
        string HotelCodee = HotelCode;//ALB
        string Checkin = cin;//"29/06/2018"
        string Checkout = cout;//"30/06/2018"
        string NoofNights = night;//"1"
        string NoofRoom = numroom;//"2"

        string BookID = HotelBook(FNamesArray, NamePrefixArray, SurnameArray, RoomsArray, roomcategoryidArray, CityCodee, HotelCodee, Checkin, Checkout, NoofNights, NoofRoom, CountryCode, ExpectedPriceArray, Curr, Lang, SupplierCode, HotelName, RoomDes, AgencyCode, DebitId, HotelAddress, Starcount, EmailID, Contactno, nation, ChargeConditionss);
        //   return hws.HotelBook(FNamesArray, NamePrefixArray, SurnameArray, RoomsArray, roomcategoryidArray, CityCodee, HotelCodee, Checkin, Checkout, NoofNights, NoofRoom, CountryCode, ExpectedPriceArray);
        return BookID;
    }

    public static List<string> SplitAtOccurence(string input, char separator, int occurence)
    {
        var parts = input.Split(separator);
        var partlist = new List<string>();
        var result = new List<string>();
        for (int i = 0; i < parts.Length; i++)
        {
            if (partlist.Count == occurence)
            {
                result.Add(string.Join(separator.ToString(), partlist));
                partlist.Clear();
            }
            partlist.Add(parts[i]);
            if (i == parts.Length - 1) result.Add(string.Join(separator.ToString(), partlist)); // if no more parts, add the rest
        }
        return result;
    }

    public class ClassPolicy
    {
        public List<CancelPolicy> cancelPolicies;
    }

    public class CancelPolicy
    {
        [JsonProperty("Condition")]
        public List<Condition> Condition { get; set; }
        [JsonProperty("_Type")]
        public string Type { get; set; }

        [JsonProperty("_Allowable")]
        public string Allowable { get; set; }

    }

    public partial class Condition
    {
        [JsonProperty("_Charge")]
        public string Charge { get; set; }

        [JsonProperty("_ChargeAmount")]
        public string ChargeAmount { get; set; }

        [JsonProperty("_Currency")]
        public string Currency { get; set; }

        [JsonProperty("_FromDay")]
        public string FromDay { get; set; }

        [JsonProperty("_ToDay")]
        public string ToDay { get; set; }
    }

    public static string HotelBook(List<string> FNamesArray, List<string> NamePrefixArray, List<string> SurnameArray, List<string> RoomsArray, List<string> roomcategoryidArray, string CityCodee, string HotelCodee, string Checkin, string Checkout, string NoofNights, string NoofRoom, string CountryCode, List<string> ExpectedPriceArray, string Curr, string Lang, string SupplierCode, string HotelName, string RoomDes, string AgencyCode, List<string> DebitId, string HotelAddress, string Starcount, string EmailID, string Contactno, string nation, string ChargeConditionss)
    {

        string deadLineTempDate = "";

        string strResult = "";
        List<CancelPolicy> cancelPolicy = new List<CancelPolicy>();

        try
        {
            ChargeConditionss = ChargeConditionss.Substring(1, ChargeConditionss.Length - 2);
            ChargeConditionss = ChargeConditionss.Replace("],[", ",");
            cancelPolicy = JsonConvert.DeserializeObject<List<CancelPolicy>>(ChargeConditionss);
        }
        catch (Exception ex)
        {

        }


        //  JArray textArray = JArray.Parse(ChargeConditionss);

        /// JObject jQueryObject = JObject.Parse(ChargeConditionss);
        // string PropertyID = (string)jQueryObject["request"]["hotel"]["hotels"][0]["supp_hotelId"];



        int NoofRooms = Convert.ToInt16(NoofRoom);
        List<Hotel> hotels = new List<Hotel>();
        List<Room> rooms = new List<Room>();

        int sn = 1;
        int countadt = 0;
        string pxtype = "";
        string IsLead = "false";
        string Agechild = null;
        string childages = "";
        List<string> count = new List<string>();
        List<string> agesof = new List<string>();
        for (int iRr = 0; iRr < RoomsArray.Count; iRr++)
        {

            List<string> Arr1 = RoomsArray[iRr].Split(',').ToList<string>();
            for (var i3 = 0; i3 < Arr1.Count; i3++)
            {
                List<string> Paxage1 = Arr1[i3].Split('_').ToList<string>();
                if (Paxage1[0] == "ADT")
                {
                    countadt = Convert.ToInt16(Paxage1[1]);
                    Agechild = null;
                }
                else if (Paxage1[0] == "CHD" && Paxage1[1] != "0")
                {
                    countadt += Convert.ToInt16(Paxage1[1]);
                    for (int ichcount = 0; ichcount < Convert.ToInt32(Paxage1[1]); ichcount++)
                    {
                        childages += "," + Convert.ToInt32(Paxage1[ichcount + 2]);
                    }
                    if (childages.StartsWith(","))
                    {
                        childages = childages.Substring(1);
                    }
                    agesof.Add(childages);
                }


            }
            count.Add(countadt.ToString());
        }

        List<string> paxdet = new List<string>();
        List<string> ages = new List<string>();
        //to get the types of rooms in booking
        int roomcount = roomcategoryidArray.Distinct().Count();
        List<string> roomcatlist = new List<string>();
        roomcatlist = roomcategoryidArray.Distinct().ToList();
        List<string> roomdescriptionbrkup = RoomDes.Split(',').ToList<string>();
        List<string> rdes = new List<string>();
        rdes = roomdescriptionbrkup.Distinct().ToList();
        Int16 rid = 1;
        for (int itt = 0; itt < roomcount; itt++)
        {

            List<RoomQuote> roomquote = new List<RoomQuote>();
            int ArraypaxCount = 0;
            int ArrayNameCountt = 0;
            int flag = 0;
            int PaxID = 0;
            for (int iR = 0; iR < RoomsArray.Count; iR++)
            {
                List<Guest> guests = new List<Guest>();
                List<Policy> cancellationPolicy = new List<Policy>();
                //List<Policy> amendmentPolicy = new List<Policy>();
                int valuecount = Convert.ToInt16(count[ArraypaxCount]);

                if (agesof.Count != 0)
                {
                    if (agesof.Count == 1 && flag == 0)
                    {
                        flag = 1;
                        ages = agesof[ArraypaxCount].Split(',').ToList<string>();
                    }
                    else if (agesof.Count >= 1 && flag == 0)
                    {
                        try
                        {
                            ages = agesof[ArraypaxCount].Split(',').ToList<string>();
                            flag = 0;
                        }
                        catch (Exception)
                        {
                            flag = 1;
                        }
                    }

                }


                int j = 0;
                for (int i = 0; i < valuecount; i++)
                {
                    if ((NamePrefixArray[ArrayNameCountt]) == "5" || (NamePrefixArray[ArrayNameCountt]) == "2")
                    {
                        pxtype = "CHD";
                        IsLead = "false";
                        Agechild = ages[j];
                        j++;
                    }
                    else
                    {
                        if (i == 0)
                        {
                            IsLead = "true";
                        }

                        pxtype = "ADT";
                        Agechild = null;
                    }

                    int paxid = i + 1;

                    //amendmentPolicy.Add(new Policy()
                    //     {

                    //         FromDate = null,
                    //         ToDate = null,
                    //         Remarks = null,
                    //         TotalCost = 0


                    //     });
  //                  DeadlineDate = timeconversion(System.DateTime.Now.AddDays(-1).ToString()),
                    for (int ic = 0; ic <cancelPolicy[iR].Condition.Count; ic++)
                    {
                        Int32 deadLineTempDateStatus = 0;
                        string value = cancelPolicy[iR].Condition[ic].Charge;
                        string FromDay = "";
                        string ToDay = "";
                        string Charge = "";
                        double ChargeAmount = 0;
                        if (value=="true")
                        {
                            FromDay = cancelPolicy[iR].Condition[ic].FromDay;
                            ToDay = cancelPolicy[iR].Condition[ic].ToDay;
                            Charge = cancelPolicy[iR].Condition[ic].Charge;
                            ChargeAmount =Convert.ToDouble(cancelPolicy[iR].Condition[ic].ChargeAmount);
                            if (deadLineTempDateStatus == 0)
                            {
                                deadLineTempDate = FromDay;
                                deadLineTempDateStatus = 1;
                            }
                        }
                        else
                        {
                            FromDay = System.DateTime.Now.ToString();
                            ToDay = cancelPolicy[iR].Condition[ic].FromDay;
                            Charge = "false";
                            ChargeAmount = ChargeAmount;
                            deadLineTempDate = ToDay;
                            deadLineTempDateStatus = 1;
                        }
                        cancellationPolicy.Add(new Policy()
                        {
                            FromDate = timeconversion(FromDay),
                            ToDate = timeconversion(ToDay),
                            Remarks = Charge,
                            TotalCost = ChargeAmount

                        });
                    }

                    guests.Add(new Guest()
                    {

                        FirstName = FNamesArray[ArrayNameCountt],
                        SurName = SurnameArray[ArrayNameCountt],
                        TitleId = Convert.ToInt16(NamePrefixArray[ArrayNameCountt]),
                        CountryOfResidenceCode = CountryCode,
                        NationalityCode = nation,
                        Email = EmailID,
                        IsLead = IsLead,
                        Phone = Contactno,
                        SerialNumber = paxid,
                        PaxTypeCode = pxtype,
                        Age = Agechild

                    });
                    ArrayNameCountt++;
                }
                List<string> Arr = RoomsArray[iR].Split(',').ToList<string>();
                for (var i3 = 0; i3 < Arr.Count; i3++)
                {
                    List<string> Paxage = Arr[i3].Split('_').ToList<string>();
                    if (Paxage[0] == "ADT")
                    {
                        countadt = Convert.ToInt16(Paxage[1]);
                    }
                }
                double costpernight = (Convert.ToDouble(ExpectedPriceArray[iR]) / Convert.ToUInt16(NoofNights));
                if (rdes[itt]== roomdescriptionbrkup[iR])
                {
                    roomquote.Add(new RoomQuote()
                    {
                        CheckInDate = timeconversion(Checkin),
                        CheckOutDate = timeconversion(Checkout),

                        Date = timeconversion(System.DateTime.Now.ToString()),
                        Nights = Convert.ToUInt16(NoofNights),
                        SerialNumber = sn,
                        TotalCost = Convert.ToDouble(ExpectedPriceArray[iR]),
                        CostPerNight = costpernight,
                        Status = "pending",
                        NumberOfAdults = countadt,
                       CancellationPolicy = cancellationPolicy,
                        Guests = guests


                    }); 
                }
                sn++;
                ArraypaxCount++;
            }
            ArraypaxCount = 0;
            ArrayNameCountt = 0;
            flag = 0;
            PaxID = 0;

            rooms.Add(new Room()
            {
                Category = rdes[itt],
                IsBabyCot = false,
                IsExtraBed = false,
                Meal = null,
                RoomDetail = null,
                RoomTypeId = rid,
                SupplierRoomType = rdes[itt],
                RoomQuotes = roomquote

            });
            rid++;
        }
        hotels.Add(new Hotel()
        {
            Address = HotelAddress,
            CityCode = CityCodee,
            EmergencyNumber = "556646545",
            MinimumNights = 1,
            Name = HotelName,
            StarCategory = Convert.ToInt16(Convert.ToDouble(Starcount)),
            PayableBy = SupplierCode,
            Rooms = rooms,
            SerialNumber = 1,
            SupplierCode = SupplierCode,
            TariffRemarks = "www",
            Telephone = "56454545"

        });
        Booking booking = new Booking()
        {
            BookDate = timeconversion(System.DateTime.Now.ToString()),
            BookPosition = 1,
            DeadlineDate = timeconversion(deadLineTempDate.ToString()),
            Reference = "3215454",
            ServiceId = 2,
            SupplierId = GetSupplierID(SupplierCode),
            OtherDetails = "Chzcbzbdjvbjj",
            SupplierRemarks = "hghjhhhjh",
            Hotels = hotels
        };
        BookEntry bookEntry = new BookEntry()
        {
            Booking = booking,
            debitIdList = DebitId
        };
        strResult = JsonConvert.SerializeObject(bookEntry, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });

        string xmlresponse = Sendrequest(strResult);
        return xmlresponse;

    }
    public static string GetTitle(string Title)
    {
        string Out = string.Empty;
        if (Title == "1")
        {
            Out = "Mr";
        }
        else if (Title == "2")
        {
            Out = "Miss";
        }
        else if (Title == "3")
        {
            Out = "Mrs";
        }
        else if (Title == "4")
        {
            Out = "Ms";
        }
        else if (Title == "5")
        {
            Out = "Master";
        }

        return Out;
    }
    public static Int32 GetSupplierID(string SCode)
    {
        Int32 Out = 4;
        if (SCode.ToUpper() == "GTA")
        {
            Out = 4;
        }
        else if (SCode.ToUpper() == "R24")
        {
            Out = 6;
        }
        else if (SCode.ToUpper() == "JAC")
        {
            Out = 11;
        }
        else if (SCode.ToUpper() == "GRNC")
        {
            Out = 20;
        }
        //else if (Title == "2")
        //{
        //    Out = "Miss";
        //}
        //else if (Title == "3")
        //{
        //    Out = "Mrs";
        //}
        //else if (Title == "4")
        //{
        //    Out = "Ms";
        //}
        //else if (Title == "5")
        //{
        //    Out = "Master";
        //}

        return Out;
    }
    public static string timeconversion(string test)
    {
        DateTime Tempdate = DateTime.Now;
        try { Tempdate = DateTime.ParseExact(test, "dd/MM/yyyy", CultureInfo.InvariantCulture); }
        catch
        {
            try { Tempdate = DateTime.ParseExact(test, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture); }
            catch
            {
                try { Tempdate = DateTime.ParseExact(test, "dd-MM-yyyy h:mm:ss tt", CultureInfo.InvariantCulture); }
                catch
                {
                    try { Tempdate = DateTime.ParseExact(test, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture); }
                    catch
                    {
                        try { Tempdate = DateTime.ParseExact(test, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture); }
                        catch
                        {
                            try { Tempdate = DateTime.ParseExact(test, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture); }
                            catch { }
                        }
                    }
                }
            }
        }
        string javaDate = Convert.ToDateTime(Tempdate).ToString("yyyy-MM-ddTHH:mm:ss.000+0000");
        return (javaDate);
    }
    public static string Sendrequest(string requestentry)
    {

        string SessionData = string.Empty;
        SessionData = HttpContext.Current.Session["Access_Token"].ToString();


        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = requestentry;
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + "hotel/booking";
        string strResult = "NORESULT";
        string tempResultJSON = "[";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + SessionData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                //HttpContext.Current.Session["Access_Token"] = SessionData;
                strResult = responseText;

            }
        }
        catch (Exception ex)

        {
        }
        return strResult;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotelStatusUpdate(string requestentry,string url) 
    {
     
        string SessionData = string.Empty;
        if (HttpContext.Current.Session["Access_Token"] != null)
        {
            SessionData = HttpContext.Current.Session["Access_Token"].ToString();
        }
        else
        {
            SessionData = "NOSESSION";
        }

        string Out = string.Empty;
        //    token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaHViLm1hdHJpeC5jb20vIiwic3ViIjoiMjciLCJleHAiOjE1MjM4ODA2NjR9.VMS5RRTweocAjJaiWTzc6V_LXxFm79O4dd9BuGn8oSg";
        string XMLData = requestentry;
        string baseUri = "https://67.209.127.116:9443/";
        String InterfaceURL = baseUri + url; 
        string strResult = "NORESULT";
        string tempResultJSON = "[";
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(InterfaceURL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + SessionData);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(XMLData);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            SessionData = httpResponse.Headers["access_token"].ToString();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                HttpContext.Current.Session["Access_Token"] = SessionData;
                strResult = responseText;
               
            }
        }
        catch (Exception ex)
        {
        }
        return strResult;
    }
    [System.Web.Services.WebMethod]
    public static string GetAllPackages(string AgencyCode, string Language)
    {
        string Data = "NODATA";

        String conStr = "Data Source=192.169.154.116;Initial Catalog=webadmin;User Id=oneviewadmin;Password=Onev13w@adm1n15";
        SqlConnection con = new SqlConnection(conStr);
        con.Open();
        SqlCommand com = new SqlCommand("procselallpackagesForhomePage", con);
        com.Parameters.AddWithValue("@Language", Language);
        com.Parameters.AddWithValue("@AgencyCode", AgencyCode);
        com.Parameters.AddWithValue("@limit", Convert.ToInt64(6));
        com.CommandType = CommandType.StoredProcedure;


        //com.Connection = con;
        //  con.Open();      


        SqlDataAdapter da = new SqlDataAdapter(com);
        DataTable Dt = new DataTable();

        da.Fill(Dt);

        if (Dt.Rows.Count > 0)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in Dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in Dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            Data = serializer.Serialize(rows);
        }
        return Data;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string BookHotelinfonew()
    {

        string strResult = "NORESULT";
        string accessToken = "";
        string refid = "";
        try
        {
            accessToken = HttpContext.Current.Session["Access_Token"].ToString();
            refid= HttpContext.Current.Session["HotelBookingRefID"].ToString();
           // refid = "AGY74-99";
          //  refid = "AGY75-292";
        }
        catch
        {
            accessToken = "";
            refid = "";
        }
        if (accessToken != "" && refid != "")
        {
            try
            {

                string baseUri = "https://67.209.127.116:9443/";
                string reqeusturl = baseUri + "hotel/booking/bookingbyref/" + refid + "";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(reqeusturl);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "GET";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                accessToken = httpResponse.Headers["access_token"].ToString();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    HttpContext.Current.Session["Access_Token"] = accessToken;
                    strResult = responseText;
                }
            }
            catch (Exception ex)
            {
                strResult = "NORESULT";
            }
        }
        else
        {
            strResult = "NORESULT";
        }
        return strResult;
    }
}