﻿/// <reference path="jquery-3.1.1.min.js" />
var travelagencyname = Setup.Agency.AgencyName;
var phoneNumber = Setup.Agency.Helpline;
var email = Setup.Agency.Email;
var footercredit = '© ' + (new Date()).getFullYear() + ' ' + Setup.Agency.AgencyShortName + '. All Rights Reserved. <a href="#home"><i class="fa fa-angle-double-up wow bounceInUp" aria-hidden="true"></i></a>';

$(document).ready(function myfunction() {
    window.localStorage.clear();
    document.title = travelagencyname;
    $('#AboutAgencyShort').append(Setup.Agency.AboutAgencyShort);
    $('#helpline').append(Setup.Agency.Helpline);
    $('#emailid').append(Setup.Agency.Email);
    $('.social').append('<li><a href="' + Setup.Agency.Facebook + '"><i class="fa fa-facebook" aria-hidden="true"></i></a></li> <li> <a href="' + Setup.Agency.YouTube + '"><i class="fa fa-youtube" aria-hidden="true"></i></a></li > <li><a href="' + Setup.Agency.Twitter + '"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>');
    $('#footercredit').append(footercredit);


    if (GetParameterValues('lout') == 'true') {
        Logout();
    }
    //CheckLoginStatus();
    $('#loginbt').click(function myfunction() {
        window.localStorage.setItem("SessionTimer", new Date().getTime());

        $('.send_loading').show();
        $(this).attr("disabled", true);
        var unamee = $('#txtUname').val();
        var pwdd = $('#txtPwd').val();
        var agcyy = $('#txtAgy').val();
        if (unamee != "" && unamee != null && pwdd != "" && pwdd != null) {
            //    var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            //var matchArray = unamee.match(emailPat);
            //if (matchArray != null) {
            //    var Dastasseq = {
            //        uname: unamee,
            //        pwd: pwdd
            //    };
            //    $.ajax({
            //        type: "POST",
            //        url: "/FLIGHT/WebMethodsDB.aspx/CheckLogin",
            //        data: JSON.stringify(Dastasseq),
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: CheckLogin,
            //        failure: function (response) {
            //            alert(response.d);
            //        },
            //        statusCode: {
            //            500: function () {
            //                //window.location.href = "/index.html";
            //                $('#loginbt').click();
            //            }
            //        }
            //    });
            //}

            var Dastasseq = {
                uname: unamee,
                pwd: pwdd,
                agcy: agcyy
            };

            var statusCode = {
                500: function () {
                    //window.location.href = "/index.html";
                    $('#loginbt').click();
                }
            };
            ajaxHelper(TestLogin, null, null, { url: "/FLIGHT/Testlogrequest.aspx/FlightSearchResponse", data: Dastasseq, statusCode: statusCode })

          //  }
            //else {
            //    $('#loginbt').attr("disabled", false);
            //    $('#txtUname').focus();
            //    $('.send_loading').hide();
            //    alertify.alert('Incorrect email !', 'Your email address seems incorrect. Please try again !');
            //}
        }
        else {
            $('#loginbt').attr("disabled", false);
            $('#txtUname').focus();
            $('.send_loading').hide();
            alertify.alert('Required !', 'Email ID and password required !');
        }

    });
    $(document).bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $('#loginbt').trigger('click');
        }
    });
    //$('*').each(function () {
    //    if ($(this).css("background-color") == "rgb(49, 52, 144)" || $(this).css("background-color") == "rgb(49, 52, 147)") {
    //        $(this).css("background-color", "#E91E63")
    //    }
    //});
});

function TestLogin(data) {
 //   window.location.href = "/HOTEL/searchhotel.html";
    var unamee = $('#txtUname').val();
    var pwdd = $('#txtPwd').val();
    var agcyy = $('#txtAgy').val();
    if (data.d == "NORESULT") {


        $('#loginbt').attr("disabled", false);
        $('#txtUname').focus();
        $('.send_loading').hide();
        alertify.alert('Invalid !', 'Credentials are invalid. Please try again!');
    }
    else {
        CheckLoginStatus();
        //CheckLoginStatus1();
        $('#loginbt').attr("disabled", false);
        window.location.href = "/HOTEL/searchhotel.html";
    }

}
function CheckLogin(data) {
    var unamee = $('#txtUname').val();
    var pwdd = $('#txtPwd').val();
    console.log(data.d);
    if (data.d == "SUCCESS") {
        $('#loginbt').attr("disabled", false);
        window.location.href = "/HOTEL/searchhotel.html";
    }
    else if (data.d == "NOUSER") {
        $('#loginbt').attr("disabled", false);
        $('#txtUname').focus();
        $('.send_loading').hide();
        alertify.alert('Invalid !', 'Credentials are invalid. Please try again!');
    }
    else if (data.d == "LOCKED") {
        $('#loginbt').attr("disabled", false);
        $('#txtUname').focus();
        $('.send_loading').hide();
        alertify.alert('Locked !', 'User or agency is locked !');
    }
}

function CheckLoginStatus() {
    var Dastasseq = {
        sesname: 'Loginuser'
    };

    ajaxHelper(SessionSuccess, null, null, { url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData", data: Dastasseq })
}

function SessionSuccess(data) {
    if (data.d != 'NOSESSION') {
        window.location.href = "/HOTEL/searchhotel.html";
    }
}
//function CheckLoginStatus1() {
//    var Dastasseq = {
//        sesname: 'UserDetails'
//    };
//    $.ajax({
//        type: "POST",
//        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
//        data: JSON.stringify(Dastasseq),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: SessionSuccess1,
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

//function SessionSuccess1(data) {
//    console.log(data.d);
//    if (data.d != 'NOSESSION') {
//        window.location.href = "/HOTEL/searchhotel.html";
//    }
//}


function GetParameterValues(param) {

    var parameterUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    var QData = 'NODATA';

    if (!Array.prototype.find) {

        //Add find polyfill just incase other pages are using it.
        // https://tc39.github.io/ecma262/#sec-array.prototype.find
        if (!Array.prototype.find) {
            Object.defineProperty(Array.prototype, 'find', {
                value: function (predicate) {
                    // 1. Let O be ? ToObject(this value).
                    if (this == null) {
                        throw new TypeError('"this" is null or not defined');
                    }

                    var o = Object(this);

                    // 2. Let len be ? ToLength(? Get(O, "length")).
                    var len = o.length >>> 0;

                    // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                    if (typeof predicate !== 'function') {
                        throw new TypeError('predicate must be a function');
                    }

                    // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                    var thisArg = arguments[1];

                    // 5. Let k be 0.
                    var k = 0;

                    // 6. Repeat, while k < len
                    while (k < len) {
                        // a. Let Pk be ! ToString(k).
                        // b. Let kValue be ? Get(O, Pk).
                        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
                        // d. If testResult is true, return kValue.
                        var kValue = o[k];
                        if (predicate.call(thisArg, kValue, k, o)) {
                            return kValue;
                        }
                        // e. Increase k by 1.
                        k++;
                    }

                    // 7. Return undefined.
                    return undefined;
                },
                configurable: true,
                writable: true
            });
        }

        for (var i = 0; i < parameterUrl.length; i++) {
            var urlparam = parameterUrl[i].split('=');
            if (urlparam[0] == param) {
                QData = urlparam[1];
                return decodeURIComponent(QData);
            }
        }
        return QData;
    } else {
        QData = parameterUrl.find(function (e) {
            return e.split('=')[0] == param;
        });

        return QData ? decodeURIComponent(QData.split('=')[1]) : 'NODATA';
    }


}

function Logout() {
    var Dastasseq = {
        sesname: 'Loginuser'
    };

    var successFunc = function (data) {
        if (window.history && history.pushState) {
            history.pushState(null, "", location.href.split("?")[0]);
        } else {
            window.location.href = "/index.html";
        }
    }

    ajaxHelper(successFunc, null, null, { url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData", data: Dastasseq })
}
//function Logout1() {
//    //alert('louted')
//    var Dastasseq = {
//        sesname: 'UserDetails'
//    };
//    $.ajax({
//        type: "POST",
//        url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData",
//        data: JSON.stringify(Dastasseq),
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (dat) {
//            window.location.href = "/index.html";
//        },
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

function ajaxHelper(successCallback, failureCallback, errorCallback, requestData) {

    var data = {};

    data.type = "POST";
    data.url = requestData.url;

    if (requestData.data) {
        data.data = JSON.stringify(requestData.data);
    }

    data.contentType = "application/json; charset=utf-8";
    data.dataType = "json";
    data.success = successCallback;

    if (failureCallback) {
        data.failure = failureCallback;
    } else {
        data.failure = function (response) {
            console.log(response.d);
        }
    }

    if (errorCallback) {
        data.error = errorCallback;
    }

    if (requestData.statusCode) {
        data.statusCode = requestData.statusCode
    }
    $.ajax(data);
}