﻿var travelagencyname = 'Twofour54 media & entertainment hub | Online Booking for Flights,Hotels,Sightseeing,Insurance';
var AgencyShortName = 'Twofour54 media & entertainment hub';
var Address = 'Twofour54 media & entertainment hub';
var Website = 'www.treasurestt.com';
var Helpline = '+971 4 361 5551';
var Email = 'info@treasurestt.com';
var AgencyRef = 'ONE';
var GTACurrencyCode = 'USD';
var GTARateOfExchange = '1';
var Domain = 'http://albadiedemo.oneviewitsolutions.com';
var Domainpath = 'http://albadiedemo.oneviewitsolutions.com';
var RoleSuppLogo = 0;
var RoleCabin = 0;
var agencyCodeHQ = 'ONVTEST00';
//Sighseeing start
var GTACurrencyCodeActivity = 'AED';
var RateOfExchangeActivity = parseFloat(3.67);
var DayToSubToAct = parseInt(2);
//Sighseeing End
//insurance
var WISCurrency = ' AED';
var WISRateofchange = '1';
//insurance

var FlightCurrencyepow = 'USD';
var dbCurrencyCode = 'USD';

var Header =
    '<div class="container-fluid"><div class="main_header ">' +
    '<div class="logo_area"><a href="/dashboard.html"><img src="/COMMON/images/logo.png" alt=""></a></div>' +
    '<div class="header"><a href="#menu"><span></span></a></div>';
Header += '<nav id="menu"><div class="left_menu_sec"><div class="users_info"><h2 class="username"></h2>' +
    '<h3 class="AgencyName"></h3><h3 class="CreditLimitAmount"></h3></div><ul>' +
     //Mobile Side Menu
     '<li style="display: none;"><a href=""><i class="fa fa-user-md" aria-hidden="true"></i>Admin Controls</a>' +
     '<ul>' +
     '<li><a href=""><i class="fa fa-stack-exchange" aria-hidden="true"></i>Agency</a>' +
     '<ul>' +
        '<li><a href=""><i class="fa fa-stack-exchange" aria-hidden="true"></i>Show Agency</a></li>' +
        '<li><a href=""><i class="fa fa-plus-square" aria-hidden="true"></i>Add Agency</a></li>' +
     '</ul>' +
     '</li>' +
     '<li><a href=""><i class="fa fa-users" aria-hidden="true"></i>Users</a>' +
        '<ul>' +
        '<li><a href=""><i class="fa fa-users" aria-hidden="true"></i>Users</a></li>' +
        '<li><a href=""><i class="fa fa-plus-square" aria-hidden="true"></i>Add Users</a></li>' +
     '</ul>' +
     '</li>' +
     '<li><a href=""><i class="fa fa-cogs" aria-hidden="true"></i>Settings</a>' +
        '<ul>' +
        '<li><a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i>Email Templates</a></li>' +
     '</ul>' +
     '</li>' +
     '<li><a href=""><i class="fa fa-usd" aria-hidden="true"></i>Deposit</a></li>' +
     '</ul>' +
     '</li>' +
     '<li style="display: none;"><a href=""><i class="fa fa-book" aria-hidden="true"></i>Reports</a>' +
        '<ul>' +
        '<li><a href=""><i class="fa fa-cube" aria-hidden="true"></i>Dashboard</a></li>' +
        '<li><a href=""><i class="fa fa-eye" aria-hidden="true"></i>View All Report</a></li>' +
        '<li><a href=""><i class="fa fa-usd" aria-hidden="true"></i>Deposit Report</a></li>' +
        '<li><a href=""><i class="fa fa-search" aria-hidden="true"></i>Search Booking</a></li>' +
        '<li><a href=""><i class="fa fa-globe" aria-hidden="true"></i>Travel Trend</a></li>' +
     '</ul>' +
     '</li>' +
     '<li style="display: none;"><a href=""><i class="fa fa-user-md" aria-hidden="true"></i>Me</a>' +
        '<ul>' +
        '<li><a href=""><i class="fa fa-pencil-square" aria-hidden="true"></i>Upadte Profile</a></li>' +
        '<li><a href=""><i class="fa fa-lock" aria-hidden="true"></i>Change Password</a></li>' +
     '</ul>' +
     '</li>' +
    '<li style="display:none;"><a href="/dashboard.html"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</a></li>' +
    '<li><a href="/myprofile.html"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>' +
    '<li class="rleflight menuflightactive"><a href="/amaflight/search.html"><i class="fa fa-plane" aria-hidden="true"></i>Book a Flight</a></li>' +
    '<li class="rlehotel"><a href="/HOTEL/searchhotel.html"><i class="fa fa-bed" aria-hidden="true"></i>Book a Hotel</a></li>' +
    //Activity Start 
    '<li class="rlesight"><a href="/Activity/searchsightseeing.html"><i class="fa fa-binoculars" aria-hidden="true"></i>Book an Sightseeing</a></li>' +
    //Activity End
     //insurance Start 
    '<li class="rleinsurance"><a href="/Insurance/searchinsurance.html"><i class="fa fa-handshake-o" aria-hidden="true"></i>Book insurance</a></li>' +
    //insurance End
    '<li class="rleinsurance"><a href="/transfer/searchtransfer.html"><i class="fa fa-car" aria-hidden="true"></i>Book Transfer</a></li>' +
    '<li class="rleflight1 menuflightactive" style="display: none;"><a href="/amaflight/search.html"><i class="fa fa-plane" aria-hidden="true"></i>Book a Flight</a></li>' +

    '<li class="rlereportflight"><a href="/amaflight/searchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Flight Bookings</a></li>' +
    '<li class="rlereporthotel"><a href="/HOTEL/hotelsearchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Hotel Bookings</a></li>' +
    //insurance Start
    '<li class="rlereportinsurance"><a href="/Insurance/Insurancesearchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Insurance Bookings</a></li>' +
     //insurance End
    //Activity Start
     '<li class="rlereportactivity"><a href="/Activity/sighseeingsearchbooking.html"><i class="fa fa-search" aria-hidden="true"></i>Search Sightseeing Bookings</a></li>' +
    //Activity End

     //Activity Start
     '<li class="rlereportactivity"><a href="/Transfer/SearchBookingTransfer.html"><i class="fa fa-search" aria-hidden="true"></i>Search Transfer Bookings</a></li>' +
    //Activity End

    '<li class="cliklogout"><a href=""><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>' +
    '<!--<li><a class="left_padding icon02" href="">Hotels</a></li><li><a class="left_padding icon03" href="">Cars</a></li>-->' +
    '</ul></div></nav>';
Header += '<div class="user-menu pull-right profile_view"><div class="user-info"><div class="profile-img" style="background-image:url(/COMMON/images/userpic.png)"></div>' +
    '<div class="top_info_padding"><h3 class="name">Hello! <span class="username"></span></h3><h4 class="name">Your Account</h4></div></div>' +
    '<div class="profile_view_toggle"><div class="list_first"><ul>' +
    '<li><a href="/myprofile.html">My Profile</a></li>' +
    '<!--<li><a href="">My Bookings</a></li>--><!--<li><a href="/amaflight/searchbooking.html">Search Bookings</a></li>--></ul></div>' +
    '<div class="list_first01"><ul><!--<li><a href="">View E-Ticket</a></li>' +
    '<li><a href="">Cancel Bookings</a></li><li><a href="">Change Travel Date</a></li>--><!--<li><a href="">Make Payment</a></li>-->' +
    '<li class="cliklogout"><a>Logout</a></li></ul></div>' +
    '</div></div>' +
    '<div class="header_menu"><ul>' +
    '<li id="headerlinkflight" class="flight_icon rleflight menuflightactive"><a href="/amaflight/search.html">Flights</a></li>' +
    '<li id="headerlinkhotel" class="hotel_icon rlehotel"><a href="/HOTEL/searchhotel.html">Hotels</a></li>' +
    //Activity Start 
    '<li id="brdsightseeing" class="hotel_icon rlesight"><a href="/Activity/searchsightseeing.html">Tours & Sightseeing</a></li>' +
    //Activity End
    //insurance Start 
'<li id="headerlinkinsurance" class="hotel_icon rleinsurance"><a href="/Insurance/searchinsurance.html">Insurance</a></li>' +
//insurance End
'<li id="headerlinktransfer" class="hotel_icon rleinsurance"><a href="/transfer/searchtransfer.html">Transfer</a></li>' +



    '<!--<li class="car_icon"><a href="">Cars</a></li>--></ul></div>' +
    '</div></div>';

var Footer = '<section class="thirdbelt"><div class="container"> <div class="row"> <div class="copyrightmain"> <p class="al_cp">© ' + (new Date()).getFullYear() + ' ' + Website + '. All Rights Reserved.</p> <p class="ar_cp semibold">Powered by <a href="http://www.oneviewit.com/" target="_blank"><img src="/login/treasure_login/images/powered.png" style="width: 85px;" alt="oneview"/></a></p> </div> </div> </div> </section>';

var Sidebar = '<div class="common-box"> <div class="admin_menu"> <ul class="accordion"> <!--<li>' +
    '<div class="link sp10 clrchange">Admin Controls<i class="fa fa-chevron-down"></i></div>' +
    '<ul class="submenu"> <div class="links_area"> <a href="">Sub Menu</a> </div> <div class="links_area"> <a href="">Sub Menu</a> </div> </ul> </li>--> ' +
    '<li> <div class="link sp10 clrchange">Booking<i class="fa fa-chevron-down"></i></div> <ul class="submenu">' +
    '<div class="links_area"> <a id="SideMenuDashboard" href="/dashboard.html">Dashboard</a> </div>' +
    '<div class="links_area rleflight"> <a id="SideMenuBookFlight" onclick="GotoUrl(\'/amaflight/search.html\')" href="#flight">Book a seat</a> </div>' +
    '<div class="links_area rlehotel"> <a id="SideMenuBookHotel" href="/HOTEL/searchhotel.html">Book a pillow</a> </div>' +
    '<div class="links_area rlesight"> <a id="SideMenusighthotel" href="/Activity/searchsightseeing.html">Book an Sightseeing</a> </div>' +
     '<div class="links_area rleinsurance"> <a id="SideMenuBookInsurance" href="/Insurance/searchinsurance.html">Book insurance</a> </div>' +
    '<div class="links_area rleflight"> <a id="SideMenuBookFlight" href="/amaflight/search.html">Book a Flight</a> </div>' +

    '<div class="links_area rlereportflight"> <a id="SideMenuSearchBookings"  href="/amaflight/searchbooking.html">Search Seat Bookings</a> </div>' +
    '<div class="links_area rlereporthotel"> <a id="searchpillow" href="/HOTEL/hotelsearchbooking.html">Search Pillow Bookings</a> </div>' +
    '<div class="links_area rlereportactivity"> <a id="searchBkactivity" href="/Activity/sighseeingsearchbooking.html">Search Sightseeing Bookings</a> </div>' +
    '<div class="links_area rlereportinsurance"> <a id="searchinsurance" href="/Insurance/Insurancesearchbooking.html">Search insurance Bookings</a> </div> ' +
   '<div class="links_area rleonrequest"> <a id="searchpillowonrequest" href="/HOTEL/hotelonrequest.html">On Request Bookings</a> </div> ' +
   '<div class="links_area rleactivityonrequest"> <a id="searchactivityonrequest" href="/activity/sighseeingonrequest.html">On Request Sightseeing Bookings</a></div>' +
    ' </ul> </li> <!--<li> <div class="link sp10 clrchange">Reports<i class="fa fa-chevron-down"></i></div> <ul class="submenu"> <div class="links_area"> <a href="">Sub Menu</a> </div> <div class="links_area"> <a href="">Sub Menu</a> </div> </ul> </li>--> </ul> </div> </div> <div class="admin_details"> <h1 class="username"></h1> <h2 class="AgencyName"></h2> <h2 class="CreditLimitAmount"></h2> <h2 id="datetimenow"></h2> </div>';

var tablinkssearch = '<li class="rleflight" onclick="GotoUrl(\'/amaflight/search.html\')"><a href="#flight"><i class="fa fa-plane" aria-hidden="true"></i>Flights</a></li>' +
                            '<li class="rlehotel" onclick="GotoUrl(\'/HOTEL/searchhotel.html\')"><a href="#hotel"><i class="fa fa-bed" aria-hidden="true"></i>Hotels</a></li>' +
                            '<li class="rlesight" onclick="GotoUrl(\'/ACTIVITY/searchsightseeing.html\')"><a href="#sightseeing"><i class="fa fa-binoculars" aria-hidden="true"></i>Tours & Sightseeing</a></li>' +
'<li class="rleinsurance" id="idrleinsurance" onclick="GotoUrl(\'/Insurance/searchinsurance.html\')"><a href="#insurance"><i class="fa fa-handshake-o" aria-hidden="true"></i>Insurance</a></li>' +
'<li class="rleinsurance"  onclick="GotoUrl(\'/transfer/searchtransfer.html\')"><a href="#insurance"><i class="fa fa-handshake-o" aria-hidden="true"></i>Transfer</a></li>';


var RoleBooksHotel = -1;
var RoleBooksSighntseeing = -1;
var DisableDev = true;

$(document).ready(function () {
    document.title = travelagencyname;
    $('header').html(Header);
    $('footer').html(Footer);
    $('.admin_control_area').html(Sidebar);
    $('.search_area > .tabs > ul').html(tablinkssearch);
    RoleCheckingg('UserRoleStatus');
    $('.help_number').html('<i class="fa fa-phone-square" aria-hidden="true"></i>' + Helpline);
    $('.help_email').html('<i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:' + Email + '">' + Email + '</a>');
    $('.flpriceslidercurrency').html(FlightCurrencyepow + ' ');
    $('#agentWebLink').append('<a href="http://' + Website + '">' + Website + '</a>');


    //logout
    $(".cliklogout").click(function myfunction() {
        $.ajax({
            type: "POST",
            url: "/Common.aspx/ClearAllSession",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dat) {
                window.location.href = "/index.html?lout=true";
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });

});
function GotoUrl(urrll) {
    window.location.href = urrll;
}
//Prevent dev option
$(document).keydown(function (event) {
    if (location.hostname !== "localhost" && DisableDev == true) {
        if (event.keyCode == 123) {
            return false;
        }
        else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
            return false;
        }
    }
});
//Prevent dev option
$(document).on("contextmenu", function (e) {
    if (location.hostname !== "localhost" && DisableDev == true) {
        e.preventDefault();
    }
});

function RoleCheckingg(session) {
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/amaflight/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: RoleSuccesss,
        failure: function (response) {
            alert(response.d);
        }
    });
}
$(document).ready(function () {
    var vr;
    var sl;
    sl = "0";
    $(".profile_view_toggle").click(function () {
        sl = "1";
    });
    $(".profile_view").click(function () {
        if (sl == "0") {
            $(".profile_view_toggle").toggle();
            vr = "1";
        }
        else {
            vr = "1";
            sl = "0";
        }

    });
    $(".wrapper").click(function () {
        if (sl == "0") {
            if (vr == "0") {

                $(".profile_view_toggle").hide();
            }
            else {
                vr = "0";
                sl = "0";
            }
        }
        else {
            vr = "0";
            sl = "0";
        }
    });
});
function RoleSuccesss(datarole) {
    var Roledata = $.parseJSON(datarole.d);
    console.log(Roledata);

    //Hide service contents from dashboard
    //Flight
    var RoleServiceFlight = _.find(Roledata, function (item) {
        return item.RoleID == "1";
    }).RoleStatus;
    if (RoleServiceFlight == 0) {
        $(".rleserflight").remove();
    }
    else {
        $(".rleserflight").show();
    }

    //Hotel
    var RoleServiceHotel = _.find(Roledata, function (item) {
        return item.RoleID == "2";
    }).RoleStatus;
    if (RoleServiceHotel == 0) {
        $(".rleserhotel").remove();
    }
    else {
        $(".rleserhotel").show();
    }

    //Sightseeing
    var RoleServiceSight = _.find(Roledata, function (item) {
        return item.RoleID == "26";
    }).RoleStatus;
    if (RoleServiceSight == 0) {
        $(".rlesight").remove();
    }
    else {
        $(".rlesight").show();
    }
    //insurance
    var RoleServiceInsurance = _.find(Roledata, function (item) {
        return item.RoleID == "43";
    }).RoleStatus;
    if (RoleServiceInsurance == 0) {
        $(".rleinsurance").remove();
    }
    else {
        $(".rleinsurance").show();
    }

    //Onrequestbookingrole
    var RoleServiceonrequest = _.find(Roledata, function (item) {
        return item.RoleID == "21";
    }).RoleStatus;
    if (RoleServiceonrequest == 0) {
        $(".rleonrequest").remove();
    }
    else {
        $(".rleonrequest").show();
    }


    //Hide menu and footer contents
    var RoleFlight = _.find(Roledata, function (item) {
        return item.RoleID == "3";
    }).RoleStatus;
    if (RoleFlight == 0) {
        $(".rleflight").remove();
        if (window.location.pathname === '/search.html' || window.location.pathname === '/travellerdetails.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rleflight").show();
    }

    var RoleHotel = _.find(Roledata, function (item) {
        return item.RoleID == "10";
    }).RoleStatus;
    if (RoleHotel == 0) {
        $(".rlehotel").remove();
        if (window.location.pathname === '/searchhotel.html' || window.location.pathname === '/hotels.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlehotel").show();
    }
    //Sighseeing
    var RoleSighseeing = _.find(Roledata, function (item) {
        return item.RoleID == "27";
    }).RoleStatus;
    if (RoleSighseeing == 0) {
        $(".rlesight").remove();
        if (window.location.pathname === '/searchsightseeing.html' || window.location.pathname === '/sightseeing.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlesight").show();
    }
    //insurance
    var Roleinsurance = _.find(Roledata, function (item) {
        return item.RoleID == "44";
    }).RoleStatus;
    if (Roleinsurance == 0) {
        $(".rleinsurance").remove();
        if (window.location.pathname === '/searchinsurance.html' || window.location.pathname === '/insurance.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rleinsurance").show();
    }
    //Reports flight
    var RoleReportsFlight = _.find(Roledata, function (item) {
        return item.RoleID == "17";
    }).RoleStatus;
    if (RoleReportsFlight == 0) {
        $(".rlereportflight").remove();
        if (window.location.pathname === '/searchbooking.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlereportflight").show();
    }

    //Reports hotel
    var RoleReportsHotel = _.find(Roledata, function (item) {
        return item.RoleID == "18";
    }).RoleStatus;
    if (RoleReportsHotel == 0) {
        $(".rlereporthotel").remove();
        if (window.location.pathname === '/hotelsearchbooking.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlereporthotel").show();
    }
    //Reports Activity
    var RoleReportsActivity = _.find(Roledata, function (item) {
        return item.RoleID == "34";
    }).RoleStatus;
    if (RoleReportsActivity == 0) {
        $(".rlereportactivity").remove();
        if (window.location.pathname === '/sighseeingsearchbooking.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlereportactivity").show();
    }
    //Reports insurance
    var RoleReportsInsurance = _.find(Roledata, function (item) {
        return item.RoleID == "45";
    }).RoleStatus;
    if (RoleReportsInsurance == 0) {
        $(".rlereportinsurance").remove();
        if (window.location.pathname === '/Insurancesearchbooking.html') {
            window.location.href = "/dashboard.html";
        }
    }
    else {
        $(".rlereportinsurance").show();
    }
    //Book Hotel
    RoleBooksHotel = _.find(Roledata, function (item) {
        return item.RoleID == "11";
    }).RoleStatus;
    //Book Sightseeing
    RoleBooksSighntseeing = _.find(Roledata, function (item) {
        return item.RoleID == "28";
    }).RoleStatus;
    //Confirm Booking
    //var RoleBookStatusHotel = _.find(Roledata, function (item) {
    //    return item.RoleID == "13";
    //}).RoleStatus;
    //if (RoleBookStatusHotel == 0) {
    //    $('#btnBook').remove();
    //}
    //else {
    //    $('#btnBook').show();
    //}


    //Supplier
    var RoleSupplier = _.find(Roledata, function (item) {
        return item.RoleID == "46";
    }).RoleStatus;
    if (RoleSupplier == 0) {
        $(".rlesupplier").remove();

    }
    else {
        $(".rlesupplier").show();

    }
    //Cabin

    RoleCabin = _.find(Roledata, function (item) {
        return item.RoleID == "46";
    }).RoleStatus;
    if (RoleCabin == 0) {
        $(".rlecabin").remove();
    }
    else {
        $(".rlecabin").show();
    }

    //supplierlogo
    RoleSuppLogo = _.find(Roledata, function (item) {
        return item.RoleID == "46";
    }).RoleStatus;
    //Block Service
    var RoleBookStatusHotel = _.find(Roledata, function (item) {
        return item.RoleID == "25";
    }).RoleStatus;
    if (RoleBookStatusHotel == 0) {
        $('.rleblockservice').remove();
    }
    else {
        $('.rleblockservice').show();
    }
}

