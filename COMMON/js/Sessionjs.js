﻿var sess_pollInterval = 60000;
var sess_expirationMinutes = 20;
var sess_warningMinutes = 15;
var sess_intervalID;
var sess_lastActivity;

function initSession() {
    sess_lastActivity = new Date();
    sessSetInterval();
    $(document).bind('keypress.session', function (ed, e) {
        sessKeyPressed(ed, e);
    });
    $("body").on('click keypress', function () {
        sess_lastActivity = new Date();
    });
}
function sessSetInterval() {
    sess_intervalID = setInterval('sessInterval()', sess_pollInterval);
}
function sessClearInterval() {
    clearInterval(sess_intervalID);

}
function sessKeyPressed(ed, e) {
    sess_lastActivity = new Date();
}
function sessLogOut() {
    //window.location.href = 'Logout.aspx';
    sessionout();
}
function sessInterval() {
    var now = new Date();
    //get milliseconds of differneces
    var diff = now - sess_lastActivity;
    //get minutes between differences
    var diffMins = (diff / 1000 / 60);
    if (diffMins >= sess_warningMinutes) {
        //warn before expiring
        //stop the timer
        sessClearInterval();
        //prompt for attention
        var active = alertify.confirm('Session Expiry', 'Your session will expire in ' +
        (sess_expirationMinutes - sess_warningMinutes) +
        ' minutes (as of ' + now.toTimeString() + '),  press OK to remain logged in ' + 'or press Cancel to log off. \nIf you are logged off any changes will be lost.',
            function() {
            now = new Date();
            diff = now - sess_lastActivity;
            diffMins = (diff / 1000 / 60);
            if (diffMins > sess_expirationMinutes) {
                sessLogOut();
            }
            else {
                initSession();
                sessSetInterval();
                sess_lastActivity = new Date();
            }
            }, function() {
                sessLogOut();
            });
        //if (active == true) {
    }
}
function sessionout() {
    var Dastasseq = {
        sesname: 'UserDetails'
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dat) {
            //alertify.alert('Session', 'Your session is expired !');
            alertify.alert('Session Expired', 'Your session is expired !', function() {
            window.location.href = "/index.html";
            });
        },
        failure: function (response) {
            console.log(response.d);
        }
    });
}