﻿/// <reference path="jquery-3.1.1.min.js" />
var travelagencyname = 'Oneview | Online Booking for Flights,Hotels,Sightseeing,Insurance';
var phoneNumber = '+971 24418565';
var email = 'info@oneviewit.com';
var footercredit = '© 2017 Oneview. All Rights Reserved.';
var fblink = 'http://www.fb.com/#';
var twlink = 'http://www.twitter.com/#';
var gpluslink = 'http://plus.google.com';
var linkedinlink = 'http://www.linkedin.com';

$(document).ready(function myfunction() {
    document.title = travelagencyname;
    $('.emailid').append(email);
    $('.emailid').attr('href', 'mailto:' + email);
    $('.phonenum').append(phoneNumber);
    $('.phonenum').attr('href', 'tel:' + phoneNumber);
    $('.footercredit').append(footercredit);
    $('.fblink').attr('href', fblink);
    $('.twlink').attr('href', twlink);
    $('.gpluslink').attr('href', gpluslink);
    $('.linkedinlink').attr('href', linkedinlink);


    if (GetParameterValues('lout') == 'true') {
        Logout();
    }
    CheckLoginStatus();
    $('#loginbt').click(function myfunction() {
        $('.send_loading').show();
        $(this).attr("disabled", true);
        var unamee = $('#txtUname').val();
        var pwdd = $('#txtPwd').val();
        if (unamee != "" && unamee != null && pwdd != "" && pwdd != null) {
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = unamee.match(emailPat);
            if (matchArray != null) {
                var Dastasseq = {
                    uname: unamee,
                    pwd: pwdd
                };
                $.ajax({
                    type: "POST",
                    url: "/FLIGHT/WebMethodsDB.aspx/CheckLogin",
                    data: JSON.stringify(Dastasseq),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: CheckLogin,
                    failure: function (response) {
                        alert(response.d);
                    },
                    statusCode: {
                        500: function () {
                            //window.location.href = "/index.html";
                            $('#loginbt').click();
                        }
                    }
                });
            }
            else {
                $('#loginbt').attr("disabled", false);
                $('#txtUname').focus();
                $('.send_loading').hide();
                alertify.alert('Incorrect email !', 'Your email address seems incorrect. Please try again !');
            }
        }
        else {
            $('#loginbt').attr("disabled", false);
            $('#txtUname').focus();
            $('.send_loading').hide();
            alertify.alert('Required !', 'Email ID and password required !');
        }

    });
    $(document).bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $('#loginbt').trigger('click');
        }
    });
    //$('*').each(function () {
    //    if ($(this).css("background-color") == "rgb(49, 52, 144)" || $(this).css("background-color") == "rgb(49, 52, 147)") {
    //        $(this).css("background-color", "#E91E63")
    //    }
    //});
});
function CheckLogin(data) {
    var unamee = $('#txtUname').val();
    var pwdd = $('#txtPwd').val();
    console.log(data.d);
    if (data.d == "SUCCESS") {
        $('#loginbt').attr("disabled", false);
        window.location.href = "/dashboard.html";
    }
    else if (data.d == "NOUSER") {
        $('#loginbt').attr("disabled", false);
        $('#txtUname').focus();
        $('.send_loading').hide();
        alertify.alert('Invalid !', 'Invalid Email or password !');
    }
    else if (data.d == "LOCKED") {
        $('#loginbt').attr("disabled", false);
        $('#txtUname').focus();
        $('.send_loading').hide();
        alertify.alert('Locked !', 'User or agency is locked !');
    }
}

function CheckLoginStatus() {
    var Dastasseq = {
        sesname: 'UserDetails'
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function SessionSuccess(data) {
    if (data.d != 'NOSESSION') {
        window.location.href = "/dashboard.html";
    }
}

function GetParameterValues(param) {
    var QData = 'NODATA';
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            QData = urlparam[1].replace('#', '');
            QData.replace('%20', ' ');
            return QData;
        }
    }
    return QData;
}
function Logout() {
    //alert('louted')
    var Dastasseq = {
        sesname: 'UserDetails'
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/ClearSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dat) {
            window.location.href = "/index.html";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}