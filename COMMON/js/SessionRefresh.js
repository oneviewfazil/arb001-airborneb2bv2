﻿$(document).ready(function () {
    timerIncrement();
    var idleInterval = setInterval(timerIncrement, 30000); // 30secs
});

function timerIncrement() {
    var timeNow = new Date().getTime();
    var tokenTime = timeNow - window.localStorage.getItem("SessionTimer");

    if (tokenTime >= 270000) {
        $.ajax({
            type: "POST",
            url: "/HOTEL/WebMethodsDB.aspx/RefreshSessionToken",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: RefreshToken,
            failure: function (response) {
                console.log(response.d);
            }
        });
    }
}

function RefreshToken(data) {
    if (data.d === Setup.Session.NoSession) {
      alertify.alert('Session', 'Session expired please login to continue.', function () {
        window.location.href = "/index.html";
      });

    }
    else {
        window.localStorage.setItem("accesstocken", data.d);
        window.localStorage.setItem("SessionTimer", new Date().getTime());
        accesstocken = window.localStorage.getItem("accesstocken");

    }
}