﻿var markupdata = '';
var MarkupServiceType = window.localStorage.getItem("MarkupServiceType");
var MarkupCurr = window.localStorage.getItem("MarkupCurr");
var isPausedOnMarkup = true;
var markupResponseData = '';

$(document).ready(function () {
 //   relogin();
    if (!window.localStorage.getItem("MarkupResponseData")) {
        SetMarkupDetails();
    } else {
        markupResponseData = JSON.parse(window.localStorage.getItem("MarkupResponseData"));
        isPausedOnMarkup = false;
    }
});

function SetMarkupDetails() {

    var Dastasseq = {
        Currency: MarkupCurr,
        Servicetype: MarkupServiceType
    };
    $.ajax({
        url: "../Common.aspx/GetServiceMarkup",
        type: "POST",
        data: JSON.stringify(Dastasseq),
        //data: JSON.stringify({ test: checkindate }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        //data: datastr,
        success: SuccessTransferMarkResponse
    });

    function SuccessTransferMarkResponse(datares) {
        if (datares.d == "NOMARKUP") {
            alertify.alert("Session", "Session expired!!Please login again to continue.", function () {
                window.location.href = "/index.html?lout=true";
            });
            }
        if (datares.d == "[]") {
            markupResponseData = '{"Markup":[{ "amount":"0", "percentage":"0", "supplier":"Any"}]}';
            window.localStorage.setItem("MarkupResponseData", JSON.stringify(markupResponseData));
        }
        else {
            markupResponseData = '{"Markup":' + datares.d + '}';
            window.localStorage.setItem("MarkupResponseData", JSON.stringify(markupResponseData));
            isPausedOnMarkup = false;
        }
    }
}



function getMarkupDetails(price, rooms, supplier) {
    var jsonData = JSON.parse(markupResponseData);

    var totalMarkup = 0;

    // loop to find all supplier that match then add markup
    jsonData.Markup.forEach(function (e) {

        if (e.supplier == supplier) {
            var tempPct = price * (e.percentage / 100);
            totalMarkup += tempPct + (e.amount / rooms);
        }

        //add the global markup
        if (e.supplier == "Any") {
            var tempPct = price * (e.percentage / 100);
            totalMarkup += tempPct + (e.amount / rooms);
        }
    });

    //add markup to base price
    var markupPrice = price + totalMarkup;

    //var counter1 = jsonData.Markup[jsonData.Markup.length - 1]

    //var creditamount = counter1.V2;
    var creditamount = jsonData.Markup[jsonData.Markup.length - 1].percentage;

    window.localStorage.setItem("creditamount", creditamount);
    return markupPrice;
}