$(document).ready(function () {
    $("div.main-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.main-tab>div.main-tab-content").removeClass("active");
        $("div.main-tab>div.main-tab-content").eq(index).addClass("active");
    });
});

function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}
/*-----------------------------------------------------
     Fancy Select
------------------------------------------------------*/
$('.form-control').fancySelect();
/*-----------------------------------------------------
     - Price Slider
------------------------------------------------------*/

/*-----------------------------------------------------
        
/*-----------------------------------------------------
         - Departure Time
    ------------------------------------------------------*/
if ($('#property-time-range')[0]) {
    var propertyTimeRange = document.getElementById('property-time-range');
    var propertyTimeRangeValues = [
        document.getElementById('property-time-upper'),
        document.getElementById('property-time-lower')
    ]

    noUiSlider.create(propertyTimeRange, {
        start: [579, 7430],
        connect: true,
        range: {
            'min': 579,
            'max': 7430
        }
    });

    propertyTimeRange.noUiSlider.on('update', function (values, handle) {
        propertyTimeRangeValues[handle].innerHTML = values[handle];
    });
}
/*-----------------------------------------------------
         - Departure Time 01
    ------------------------------------------------------*/
if ($('#property-time2-range')[0]) {
    var propertyTime2Range = document.getElementById('property-time2-range');
    var propertyTime2RangeValues = [
        document.getElementById('property-time2-upper'),
        document.getElementById('property-time2-lower')
    ]

    noUiSlider.create(propertyTime2Range, {
        start: [1.30, 23.30],
        connect: true,
        range: {
            'min': 1.30,
            'max': 23.30
        }
    });

    propertyTime2Range.noUiSlider.on('update', function (values, handle) {
        propertyTime2RangeValues[handle].innerHTML = values[handle];
    });
}
/*-----------------------------------------------------
         - Arrival Time
    ------------------------------------------------------*/
if ($('#property-time3-range')[0]) {
    var propertyTime3Range = document.getElementById('property-time3-range');
    var propertyTime3RangeValues = [
        document.getElementById('property-time3-upper'),
        document.getElementById('property-time3-lower')
    ]

    noUiSlider.create(propertyTime3Range, {
        start: [1.30, 23.30],
        connect: true,
        range: {
            'min': 1.30,
            'max': 23.30
        }
    });

    propertyTime3Range.noUiSlider.on('update', function (values, handle) {
        propertyTime3RangeValues[handle].innerHTML = values[handle];
    });
}
/*-----------------------------------------------------
         - Arrival Time 01
    ------------------------------------------------------*/
if ($('#property-time4-range')[0]) {
    var propertyTime4Range = document.getElementById('property-time4-range');
    var propertyTime4RangeValues = [
        document.getElementById('property-time4-upper'),
        document.getElementById('property-time4-lower')
    ]

    noUiSlider.create(propertyTime4Range, {
        start: [1.30, 23.30],
        connect: true,
        range: {
            'min': 1.30,
            'max': 23.30
        }
    });

    propertyTime4Range.noUiSlider.on('update', function (values, handle) {
        propertyTime4RangeValues[handle].innerHTML = values[handle];
    });
}