﻿/// <reference path="jquery-3.1.1.min.js" />

$(document).ready(function () {
    //BindTitle();
    var loginUserDetails = {
        userSession: {
            loginUser: Setup.Session.LoginUser,
        }
    };

    SessionCheckDetails(JSON.stringify(loginUserDetails));

    //logout
    $(".cliklogout").click(function myfunction() {
        var Dastasseq = {
            sesname: 'Loginuser'
        };
        var successFunc = function (dat) {
            window.location.href = "/index.html";
        }
        ajaxHelper(successFunc, null, null, { url: Setup.Session.Url.ClearSessionData, data: Dastasseq });
    });

    $("#txtPhone").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            alertify.alert('Validate', 'Number only !');
            return false;
        }
    });

    $(".alphaonly").keypress(function (e) {
        var code = e.keyCode || e.which;
        if ((code < 65 || code > 90)
        && (code < 97 || code > 122) && code != 32 && code != 46) {
            alertify.alert('Validate', 'Only alphabates are allowed');
            return false;
        }
    });
});

function MarkupSuccess(datamark) {
    $(this).attr("disabled", false);
    if (datamark.d != 'NOMARKUP') {
        var userdata = $.parseJSON(datamark.d);
        //console.log(JSON.parse(userdata));
        $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
    }
    else {
        $('.CreditLimitAmount').html('Credit Limit - No markup !');
    }
}
//function BindTitle() {
//    var Datatable = {
//        selectdata: '*',
//        tablename: 'tblTitle',
//        condition: 'NULL'
//    };
//    ajaxHelper(TitleSuccess, null, null, { url: Setup.Session.Url.GetTableData, data: Datatable });
//}
//function TitleSuccess(data) {
//    var Titledata = $.parseJSON(data.d);
//    //console.log(Titledata);
//    for (var i = 0; i < Titledata.length; i++) {
//        $("#ddlTitle").append($("<option></option>").val(Titledata[i].TitleID).html(Titledata[i].Title));
//    }
//}
//function SaveUserData() {
//    let firstName = $('#txtFName').val();
//    let lastName = $('#txtLastName').val();
//    let phone = $('#txtPhone').val();

//    var name_regex = /^[a-zA-Z]+$/;
//    if (!firstName.match(name_regex) ) {
//        alertify.alert('Validate', 'For your name please use alphabets only.');
//    }
//    else if (firstName.length > 50) {
//        alertify.alert('Validate', 'First Name maximum length exceeded.');
//    }
//    else if (lastName.length > 50) {
//        alertify.alert('Validate', 'Last Name maximum length exceeded.');
//    }
//    else if (phone.length > 50) {
//        alertify.alert('Validate', 'Phone number maximum length exceeded.');
//    }
//    else if ((firstName.length > 0 && firstName.length < 50) && (lastName.length > 0 && lastName.length < 50) && phone.length > 0) {

//        $(this).attr("disabled", true);
//        var Title = $('#ddlTitle').val();
//        var FName = $('#txtFName').val();
//        var LName = $('#txtLastName').val();
//        var MobNo = $('#txtPhone').val();
//        var DataUser = {
//            Title: Title,
//            FName: FName,
//            LName: LName,
//            MobNo: MobNo
//        };

//        ajaxHelper(SaveUserDataSuccess, null, null, { url: Setup.Session.Url.UpdateUserData, data: DataUser });

//    }
//    else {
//        alertify.alert('Validate', 'Please fill all details !');
//    }

//}
//function SaveUserDataSuccess() {
//    var userDetails = {
//        userSession: {
//            loginUser: Setup.Session.LoginUser,
//        }
//    };

//    SessionCheckDetails(JSON.stringify(loginUserDetails));

//    $(this).attr("disabled", false);
//    alertify.alert('Saved', 'Data updated successfully.');
//}
function SavePasswordData() {
    var CurrentPass = $('#txtCurPass').val();
    var NewPass = $('#txtNewPass').val();
    var ConfPass = $('#txtConfPass').val();
    if (CurrentPass.length > 0 && NewPass.length > 0 && ConfPass.length > 0) {
        if (NewPass != ConfPass) {
            alertify.alert('Password', 'Password mismatch !');
        }
        else {
            var DataUserPass = {
                CurrentPass: CurrentPass,
                NewPass: NewPass
            };
            ajaxHelper(SaveUserPassSuccess, null, null, { url: Setup.Session.Url.UpdateUserPasswordNew, data: DataUserPass });
        }
    }
    else {
        alertify.alert('Validate', 'Please fill all details !');
    }
}
function SaveUserPassSuccess(datapass) {
    $(this).attr("disabled", false);
    var data = datapass;
    if (data.d == "NORESULT") {
        alertify.alert('Error', 'Something went wrong. Please try again later.');
    }
    else {
        if (JSON.parse(data.d).message == "Password changed.") {
            alertify.alert('Saved', 'Password changed successfully.');
        }
        else {
            alertify.alert('Error', 'Current password is invalid !');
        }
    }
    $('#txtCurPass').val('');
    $('#txtNewPass').val('');
    $('#txtConfPass').val('');
}

function SessionCheckDetails(session) {
    var Dastasseq = {
        sesname: session
    };

    function SessionCheckingSuccess(data) {

        if (data.d.Loginuser == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var logininfo = $.parseJSON(data.d.Loginuser);

            $('.username').html(logininfo.user.firstName);
            $('.AgencyName').html(logininfo.user.loginNode.name);
            $('#txtFName').val(logininfo.user.firstName);
            $('#txtLastName').val(logininfo.user.lastName);
            $('#ddlTitle').val(logininfo.user.title.name);
            $('#txtEmail').val(logininfo.user.emailId);
            $('#txtPhone').val(logininfo.user.contactNumber);

            var AgencyTypeIDD = "1";
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
            }
            //Get Markup
            var Dastasseqq = {
                AgencyID: logininfo.user.loginNode.tenant.id,
                ServiceTypeID: '1'
            };

            ajaxHelper(MarkupSuccess, null, null, { url: Setup.Session.Url.GetMarkupData, data: Dastasseqq });
        }
    }
    ajaxHelper(SessionCheckingSuccess, null, null, { url: Setup.Session.Url.GetSessionDataNew, data: Dastasseq });
}

function ajaxHelper(successCallback, failureCallback, errorCallback, requestData) {

    var data = {};

    data.type = "POST";
    data.url = requestData.url;

    if (requestData.data) {
        data.data = JSON.stringify(requestData.data);
    }

    data.contentType = "application/json; charset=utf-8";
    data.dataType = "json";
    data.success = successCallback;

    if (failureCallback) {
        data.failure = failureCallback;
    } else {
        data.failure = function (response) {
            console.log(response.d);
        }
    }

    if (errorCallback) {
        data.error = errorCallback;
    }


    if (navigator.onLine) {
        $.ajax(data);
    } else {
        alertify.alert('No Connection', 'There seems to be a network issue. Please try again later.', function () {
            window.location.href = "/myprofile.html";
        });
    }
}