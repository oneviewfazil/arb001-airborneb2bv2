﻿var InterestingFacts = [
  {
      No: "0",
      Caption: "Dubai police",
      Info: "The Dubai police fleet includes a Lamborghini, Ferrari and Bentley.1 This is to allow them to catch speeders who can outrun other cars."
  },
  {
      No: "1",
      Caption: "ATMs in Dubai",
      Info: "There are ATMs in Dubai that dispense gold bars."
  },
  {
      No: "2",
      Caption: "Liquor license",
      Info: "In Dubai, you must obtain a “liquor license” to drink from the privacy of your own home."
  },
  {
      No: "3",
      Caption: "Arab population",
      Info: "The United Arab Emirates, despite its name has a minority Arab ulation (13%), with South Asians (primarily Indians, Pakistanis and Bangladeshis) forming the largest group."
  },
  {
      No: "4",
      Caption: "Financial crisis in 2008",
      Info: "After the global financial crisis in 2008, many exotic cars were abandoned all over Dubai and its airport. Most of these cars were bought with easy credit during the boom and the owners were unable to meet car payments after job cutbacks due to financial recession."
  },
  {
      No: "5",
      Caption: "Address system in Dubai",
      Info: "Dubai has no ress system, no zip codes, no area codes and no postal system. For a package to be sent properly, the sender would have to leave proper directions to the destination of said package."
  }
]