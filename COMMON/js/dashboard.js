﻿var DayToSubTo = DayToSubToAct;
var Currency = dbCurrencyCode;
var TotBookFlight = parseFloat(-1);
var TotBookHotel = parseFloat(-1);
var TotBookActivty = parseFloat(-1);
var TotBookinsurance = parseFloat(-1);
$(document).ready(function () {
    SessionCheckingdash('UserDetails');
});
function SessionCheckingdash(session) {
    $(".loadingGIF").show();
    var Dastasseq = {
        sesname: session
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetSessionData",
        data: JSON.stringify(Dastasseq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SessionSuccessdash,
        failure: function (response) {
            alert(response.d);
        }
    });
    function SessionSuccessdash(data) {
        if (data.d == 'NOSESSION') {
            window.location.href = "/index.html";
        }
        else {
            var userdata = $.parseJSON(data.d);
            console.log(userdata);
            $('.username').html(userdata[0].FirstName);
            $('.AgencyName').html(userdata[0].AgencyName);
            var now = moment().format("dddd, MMMM Do YYYY, h:mm:ss a");
            $('#datetimenow').html('<i class="fa fa-clock-o" aria-hidden="true"></i> ' + now);
            var AgencyTypeIDD = userdata[0].AgencyTypeIDD;
            if (AgencyTypeIDD == '1' || AgencyTypeIDD == '2') {
                $('.CreditLimitAmount').hide();
                $('.AvaCreLmtAmt , .onlysubagency').hide();
            }

            var AgencyID = userdata[0].AgencyID;
            var UserID = userdata[0].UserID;

            GetBookingStattoChart(AgencyID, 1, UserID);
            GetBookingStattoChart(AgencyID, 2, UserID);
            GetBookingStattoChart(AgencyID, 3, UserID);
            GetBookingStattoChart(AgencyID, 4, UserID);
            GetBookingStattoChart(AgencyID, 5, UserID);


            //GetMarkupValue(AgencyID);
            GetCreditInfo(AgencyID);
            //GetBookingStatCount(AgencyID, 1,UserID);
            //GetBookingStatCount(AgencyID, 2, UserID);
            //GetBookingStatCount(AgencyID, 3, UserID);
            //GetBookingStatCount(AgencyID, 4, UserID);
            //GetBookingStatCount(AgencyID, 5, UserID);
            GetActionReqFlight(AgencyID, AgencyTypeIDD);
            GetActionReqHotel(AgencyID);
            GetActionReqActivity(AgencyID);
            GetActionReqInsurance(AgencyID);
            GetActionReqTransfer(AgencyID);

            

        }
    }
}
function GetActionReqActivity(AgencyID) {
    $("#loadergifact").show();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/activity/WebRequestSightseeing.aspx/GetActionReqActivity",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqActivitySuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqActivitySuccess(response) {
        if (response.d != "NOINFO") {
            var Result = JSON.parse(response.d);
            console.log(Result);
            var Table = '<tr>';
            for (var i = 0; i < Result.length; i++) {
                Table = '<tr onclick="ActionReqActivityClick(' + Result[i].BookingRefID + ')" role="row" class="odd">' +
                                    '<td class="sorting_1">' + Result[i].BookingRefID + '</td>' +
                                    '<td data-toggle="tooltip" data-placement="top" title="' + Result[i].FirstName + ' ' + Result[i].LastName + '">' + Result[i].FirstName + '</td>' +
                                    '<td>' + Result[i].AgencyName + '</td>' +
                                    '<td data-toggle="tooltip" data-placement="top" title="' + Result[i].CityNamewithCountry + '">' + Result[i].ItemCityCode + '</td>' +
                                    '<td>' + Result[i].ItemName + '</td>' +
                                    '<td data-toggle="tooltip" data-placement="top" title="' + moment(Result[i].TourDate).format("DD MMMM YYYY") + '">' + moment(Result[i].TourDate).format("DD MMM YYYY") + '</td>' +
                                    '<td data-toggle="tooltip" data-placement="top" title="' + moment(Result[i].LimitDate).subtract(DayToSubTo, "days").format("ddd MMMM YYYY") + '">' + moment(Result[i].LimitDate).subtract(DayToSubTo, "days").format("ddd MMM YYYY") + '</td>' +
                                    '<td class="no_mobile" data-toggle="tooltip" data-placement="top" title="' + moment(Result[i].BookingCreationDate).format('DD MMM YYYY') + '">' + moment(Result[i].BookingCreationDate).format('DD MMM YYYY') + '</td>' +
                                    '<td data-toggle="tooltip" data-placement="top" title="' + GetBookStatusName(Result[i].BookingStatusCode) + '">' + Result[i].BookingStatusCode + '</td>';
            }
            Table = Table + '</tr>';
            $("#ActReqAct").html(Table);
            $("#loadergifact").hide();
            $('[data-toggle="tooltip"]').tooltip();
            //if ($.fn.dataTable.isDataTable('#tableActAct')) {
            //    table = $('#tableActAct').DataTable();
            //}
            //else {
            //    table = $('#tableActAct').DataTable({
            //        "order": [[0, "desc"]]
            //    });
            //}
            //$('#tableActAct').DataTable({
            //    "order": [[0, "desc"]]
            //});
        } else {
            $("#divactAct").hide();
        }

    }

}
function ActionReqActivityClick(RefID) {
    var Datatable = {
        RefID: RefID
    };
    $.ajax({
        type: "POST",
        url: "/activity/WebRequestSightseeing.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqActClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function ActionReqActClickSuccess(response) {
        window.location.href = '/activity/sighseeingbookinfo.html';
    }
}
function GetBookStatusName(Status) {
    if (Status == 'HK') {
        return 'Confirmed';

    } else if (Status == 'RR') {
        return 'Confirmation pending';
    }
    else if (Status == 'XX') {
        return 'Cancelled';
    }
    else if (Status == 'RC') {
        return 'Reconcfirmed';
    }
    else if (Status == 'RQ') {
        return 'On Request';
    }
    else if (Status == 'XR') {
        return 'cancellation On Request';
    }
    else {

    }
}
function GetMarkupValueHotel(AgencyID) {
    //Get Markup
    var Dastasseqq = {
        AgencyID: AgencyID,
        ServiceTypeID: '2'
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebserviceHotel.aspx/GetMarkupDataHotel",
        data: JSON.stringify(Dastasseqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: MarkupSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function MarkupSuccess(datamark) {
        if (datamark.d != 'NOMARKUP') {
            var userdata = $.parseJSON(datamark.d);
            console.log(userdata);
            $('.CreditLimitAmount').html('Credit Limit - ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);
            $('.AvaCreLmtAmt').html('Available Credit : ' + userdata[0].CreditLimitAmount + ' ' + userdata[0].CurrenyCode);

        }
        else {
            $('.CreditLimitAmount').html('Credit Limit - No Credit !');
        }
    }
}

//Get Credit info
function GetCreditInfo(AgencyID) {
    //Get Markup
    var Dastasseqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/Common.aspx/GetSumDepoCreditPayment",
        data: JSON.stringify(Dastasseqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetCreditInfoSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetCreditInfoSuccess(datacre) {
        if (datacre.d != 'NODATA') {
            var datacree = $.parseJSON(datacre.d);
            console.log(datacree);
            $('#totDeposit').html(+ datacree[0].TotalDeposit + ' ' + Currency);
            $('#CrLimit').html(+ datacree[0].TotalCredit + ' ' + Currency);
            $('#util').html(+ datacree[0].TotalPayment + ' ' + Currency);
        }
    }
}
//Get Hotel Booking Info
function GetBookingStatCount(AgencyID, ServiceTypeId,UserID) {
    //Get Markup
    var Dastasseqqqq = {
        AgencyID: AgencyID,
        ServiceTypeId: ServiceTypeId,
        UserID: UserID
    };
    $.ajax({
        type: "POST",
        url: "/Common.aspx/GetBookingStatCount",
        data: JSON.stringify(Dastasseqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetBookingStatCountSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetBookingStatCountSuccess(datacree) {
        if (datacree.d != 'NODATA') {
            var datacreee = $.parseJSON(datacree.d);
            console.log(datacreee);
            $('#RQ' + ServiceTypeId).html(datacreee[0].StatusRequested);
            $('#RR' + ServiceTypeId).html(datacreee[0].PendingConfirmation);
            $('#HK' + ServiceTypeId).html(datacreee[0].ConfirmedNotTicketed);
            $('#XR' + ServiceTypeId).html(datacreee[0].CancellationRequest);
            $('#XX' + ServiceTypeId).html(datacreee[0].Cancelled);
            $('#TOT' + ServiceTypeId).html(datacreee[0].TotalBooking);
            if (ServiceTypeId == '1') {
                $('#RC' + ServiceTypeId).html(datacreee[0].TicketIssued);
                TotBookFlight = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '2') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                $('#RJ' + ServiceTypeId).html(datacreee[0].Rejected);
                TotBookHotel = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '3') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                TotBookActivty = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '4') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                TotBookinsurance = datacreee[0].TotalBooking;
            }
            else if (ServiceTypeId == '5') {
                $('#RC' + ServiceTypeId).html(datacreee[0].Reconfirmed);
                TotBookinsurance = datacreee[0].TotalBooking;
            }
            if (TotBookFlight == 0 && TotBookHotel == 0 && TotBookActivty == 0 && TotBookinsurance == 0) {
                $('#divWaiting').show();
            }
        }
    }
}



//donut charts



function createDonutCharts() {
    $("<style type='text/css' id='dynamic' />").appendTo("head");
    $("div[chart-type*=donut]").each(function () {
        var d = $(this);
        var id = $(this).attr('id');
        var max = $(this).data('chart-max');
        if ($(this).data('chart-text')) {
            var text = $(this).data('chart-text');
        } else {
            var text = "";
        }
        if ($(this).data('chart-caption')) {
            var caption = $(this).data('chart-caption');
        } else {
            var caption = "";
        }
        if ($(this).data('chart-initial-rotate')) {
            var rotate = $(this).data('chart-initial-rotate');
        } else {
            var rotate = 0;
        }
        var segments = $(this).data('chart-segments');

        if (segments == undefined || segments == null) {
            return;
        }
       
        for (var i = 0; i < Object.keys(segments).length; i++) {
            var s = segments[i];
            var start = ((s[0] / max) * 360) + rotate;
            var deg = ((s[1] / max) * 360);
            if (s[1] >= (max / 2)) {
                d.append('<div class="large donut-bite" data-segment-index="' + i + '"> ');
            } else {
                d.append('<div class="donut-bite" data-segment-index="' + i + '"> ');
            }
            var style = $("#dynamic").text() + "#" + id + " .donut-bite[data-segment-index=\"" + i + "\"]{-moz-transform:rotate(" + start + "deg);-ms-transform:rotate(" + start + "deg);-webkit-transform:rotate(" + start + "deg);-o-transform:rotate(" + start + "deg);transform:rotate(" + start + "deg);}#" + id + " .donut-bite[data-segment-index=\"" + i + "\"]:BEFORE{-moz-transform:rotate(" + deg + "deg);-ms-transform:rotate(" + deg + "deg);-webkit-transform:rotate(" + deg + "deg);-o-transform:rotate(" + deg + "deg);transform:rotate(" + deg + "deg); background-color: " + s[2] + ";}#" + id + " .donut-bite[data-segment-index=\"" + i + "\"]:BEFORE{ background-color: " + s[2] + ";}#" + id + " .donut-bite[data-segment-index=\"" + i + "\"].large:AFTER{ background-color: " + s[2] + ";}";
            $("#dynamic").text(style);
        }
        d.children().first().before("<div class='donut-hole'><span class='donut-filling'>" + text + "</span></div>");
        d.append("<div class='donut-caption-wrapper'><span class='donut-caption'>" + caption + "</span></div>");
    });
    $("div[chart-type*=donut]").each(function () {
        $(this).children().each(function () {
            var cs1 = $(this).attr('class');
            if (cs1 == 'donut-hole') {
                var cls = $(this).children().attr('class');
                $(this).children().css('color', '#F18605 !important');
            }
        });
    });
}


   
function GetBookingStattoChart(AgencyID, ServiceTypeId, UserID) {
    var Dastasseqqqq = {
        AgencyID: AgencyID,
        ServiceTypeId: ServiceTypeId,
        UserID: UserID
    };
    $.ajax({
        type: "POST",
        url: "/Common.aspx/GetBookingStatCount",
        data: JSON.stringify(Dastasseqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetBookingStatCountSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetBookingStatCountSuccess(datacree) {
        if (datacree.d != 'NODATA') {
            var datacreee = $.parseJSON(datacree.d);
            console.log(datacreee);          
            if (ServiceTypeId == '1') {
                $('#failureChart2').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart2').attr('data-chart-segments', getsegments(datacreee[0].StatusRequested, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureChart2').attr('data-chart-text', datacreee[0].StatusRequested);
                //datacreee[0].PendingConfirmation
                $('#failureChart3').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart3').attr('data-chart-segments', getsegments(datacreee[0].ConfirmedNotTicketed, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureChart3').attr('data-chart-text', datacreee[0].ConfirmedNotTicketed);
                
                $('#failureChart4').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart4').attr('data-chart-segments', getsegments(datacreee[0].TicketIssued, datacreee[0].TotalBooking, "#426e97"));
                $('#failureChart4').attr('data-chart-text', datacreee[0].TicketIssued);

                $('#failureChart5').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart5').attr('data-chart-segments', getsegments(datacreee[0].CancellationRequest, datacreee[0].TotalBooking, "#b3915e"));
                $('#failureChart5').attr('data-chart-text', datacreee[0].CancellationRequest);

                $('#failureChart6').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart6').attr('data-chart-segments', getsegments(datacreee[0].Cancelled, datacreee[0].TotalBooking, "#ac4748"));
                $('#failureChart6').attr('data-chart-text', datacreee[0].Cancelled);

                $('#failureChart7').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChart7').attr('data-chart-segments', getsegments(datacreee[0].TotalBooking, datacreee[0].TotalBooking, "#6891a8"));
                $('#failureChart7').attr('data-chart-text', datacreee[0].TotalBooking);
            }
           
            if (ServiceTypeId == '2') {

                $('#failureCharthotlRQ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlRQ').attr('data-chart-segments', getsegments(datacreee[0].StatusRequested, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureCharthotlRQ').attr('data-chart-text', datacreee[0].StatusRequested);
                //datacreee[0].PendingConfirmation
                $('#failureCharthotlRR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlRR').attr('data-chart-segments', getsegments(datacreee[0].PendingConfirmation, datacreee[0].TotalBooking, "#8f6ec8"));
                $('#failureCharthotlRR').attr('data-chart-text', datacreee[0].PendingConfirmation);

                $('#failureCharthotlHK').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlHK').attr('data-chart-segments', getsegments(datacreee[0].ConfirmedNotTicketed, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureCharthotlHK').attr('data-chart-text', datacreee[0].ConfirmedNotTicketed);

                $('#failureCharthotlRC').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlRC').attr('data-chart-segments', getsegments(datacreee[0].Reconfirmed, datacreee[0].TotalBooking, "#8e9567"));
                $('#failureCharthotlRC').attr('data-chart-text', datacreee[0].Reconfirmed);

                $('#failureCharthotlXR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlXR').attr('data-chart-segments', getsegments(datacreee[0].CancellationRequest, datacreee[0].TotalBooking, "#b3915e"));
                $('#failureCharthotlXR').attr('data-chart-text', datacreee[0].CancellationRequest);

                $('#failureCharthotlXX').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlXX').attr('data-chart-segments', getsegments(datacreee[0].Cancelled, datacreee[0].TotalBooking, "#ac4748"));
                $('#failureCharthotlXX').attr('data-chart-text', datacreee[0].Cancelled);

                $('#failureCharthotlRJ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlRJ').attr('data-chart-segments', getsegments(datacreee[0].Rejected, datacreee[0].TotalBooking, "#894e7c"));
                $('#failureCharthotlRJ').attr('data-chart-text', datacreee[0].Rejected);
              
                $('#failureCharthotlTOT').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlTOT').attr('data-chart-segments', getsegments(datacreee[0].TotalBooking, datacreee[0].TotalBooking, "#6891a8"));
                $('#failureCharthotlTOT').attr('data-chart-text', datacreee[0].TotalBooking);

                $('#failureCharthotlIS').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharthotlIS').attr('data-chart-segments', getsegments(datacreee[0].TicketIssued, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureCharthotlIS').attr('data-chart-text', datacreee[0].TicketIssued);

                
            }
            if (ServiceTypeId == '3') {
              
                $('#failureChartsgtRQ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtRQ').attr('data-chart-segments', getsegments(datacreee[0].StatusRequested, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureChartsgtRQ').attr('data-chart-text', datacreee[0].StatusRequested);
               
                $('#failureChartsgtRR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtRR').attr('data-chart-segments', getsegments(datacreee[0].PendingConfirmation, datacreee[0].TotalBooking, "#8f6ec8"));
                $('#failureChartsgtRR').attr('data-chart-text', datacreee[0].PendingConfirmation);

                $('#failureChartsgtHK').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtHK').attr('data-chart-segments', getsegments(datacreee[0].ConfirmedNotTicketed, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureChartsgtHK').attr('data-chart-text', datacreee[0].ConfirmedNotTicketed);

                $('#failureChartsgtRC').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtRC').attr('data-chart-segments', getsegments(datacreee[0].Reconfirmed, datacreee[0].TotalBooking, "#8e9567"));
                $('#failureChartsgtRC').attr('data-chart-text', datacreee[0].Reconfirmed);

                $('#failureChartsgtXR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtXR').attr('data-chart-segments', getsegments(datacreee[0].CancellationRequest, datacreee[0].TotalBooking, "#b3915e"));
                $('#failureChartsgtXR').attr('data-chart-text', datacreee[0].CancellationRequest);

                $('#failureChartsgtXX').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtXX').attr('data-chart-segments', getsegments(datacreee[0].Cancelled, datacreee[0].TotalBooking, "#ac4748"));
                $('#failureChartsgtXX').attr('data-chart-text', datacreee[0].Cancelled);

                $('#failureChartsgtRJ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtRJ').attr('data-chart-segments', getsegments(datacreee[0].Rejected, datacreee[0].TotalBooking, "#894e7c"));
                $('#failureChartsgtRJ').attr('data-chart-text', datacreee[0].Rejected);

                
                $('#failureChartsgtIS').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtIS').attr('data-chart-segments', getsegments(datacreee[0].TicketIssued, datacreee[0].TotalBooking, "#6891a8"));
                $('#failureChartsgtIS').attr('data-chart-text', datacreee[0].TicketIssued);

                $('#failureChartsgtTOT').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartsgtTOT').attr('data-chart-segments', getsegments(datacreee[0].TotalBooking, datacreee[0].TotalBooking, "#8f6ec8"));
                $('#failureChartsgtTOT').attr('data-chart-text', datacreee[0].TotalBooking);
            }
            if (ServiceTypeId == '4') {

                $('#failureChartinsRQ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartinsRQ').attr('data-chart-segments', getsegments(datacreee[0].StatusRequested, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureChartinsRQ').attr('data-chart-text', datacreee[0].StatusRequested);

                $('#failureChartinsRC').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartinsRC').attr('data-chart-segments', getsegments(datacreee[0].Reconfirmed, datacreee[0].TotalBooking, "#8e9567"));
                $('#failureChartinsRC').attr('data-chart-text', datacreee[0].Reconfirmed);

                $('#failureChartinsHK').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartinsHK').attr('data-chart-segments', getsegments(datacreee[0].ConfirmedNotTicketed, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureChartinsHK').attr('data-chart-text', datacreee[0].ConfirmedNotTicketed);
                

                $('#failureChartinsIs').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartinsIs').attr('data-chart-segments', getsegments(datacreee[0].TicketIssued, datacreee[0].TotalBooking, "#6891a8"));
                $('#failureChartinsIs').attr('data-chart-text', datacreee[0].TicketIssued);


                $('#failureChartinsTOT').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureChartinsTOT').attr('data-chart-segments', getsegments(datacreee[0].TotalBooking, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureChartinsTOT').attr('data-chart-text', datacreee[0].TotalBooking);
            }
            if (ServiceTypeId == '5') {
                
                $('#failureCharttransRQ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransRQ').attr('data-chart-segments', getsegments(datacreee[0].StatusRequested, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureCharttransRQ').attr('data-chart-text', datacreee[0].StatusRequested);

                $('#failureCharttransRR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransRR').attr('data-chart-segments', getsegments(datacreee[0].PendingConfirmation, datacreee[0].TotalBooking, "#8f6ec8"));
                $('#failureCharttransRR').attr('data-chart-text', datacreee[0].PendingConfirmation);

                $('#failureCharttransHK').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransHK').attr('data-chart-segments', getsegments(datacreee[0].ConfirmedNotTicketed, datacreee[0].TotalBooking, "#717fb3"));
                $('#failureCharttransHK').attr('data-chart-text', datacreee[0].ConfirmedNotTicketed);

                $('#failureCharttransRC').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransRC').attr('data-chart-segments', getsegments(datacreee[0].Reconfirmed, datacreee[0].TotalBooking, "#8e9567"));
                $('#failureCharttransRC').attr('data-chart-text', datacreee[0].Reconfirmed);

                $('#failureCharttransXR').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransXR').attr('data-chart-segments', getsegments(datacreee[0].CancellationRequest, datacreee[0].TotalBooking, "#b3915e"));
                $('#failureCharttransXR').attr('data-chart-text', datacreee[0].CancellationRequest);

                $('#failureCharttransXX').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransXX').attr('data-chart-segments', getsegments(datacreee[0].Cancelled, datacreee[0].TotalBooking, "#ac4748"));
                $('#failureCharttransXX').attr('data-chart-text', datacreee[0].Cancelled);

                $('#failureCharttransRJ').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransRJ').attr('data-chart-segments', getsegments(datacreee[0].Rejected, datacreee[0].TotalBooking, "#894e7c"));
                $('#failureCharttransRJ').attr('data-chart-text', datacreee[0].Rejected);

                $('#failureCharttransIs').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransIs').attr('data-chart-segments', getsegments(datacreee[0].TicketIssued, datacreee[0].TotalBooking, "#6891a8"));
                $('#failureCharttransIs').attr('data-chart-text', datacreee[0].TicketIssued);

                $('#failureCharttransTOT').attr('data-chart-max', datacreee[0].TotalBooking);
                $('#failureCharttransTOT').attr('data-chart-segments', getsegments(datacreee[0].TotalBooking, datacreee[0].TotalBooking, "#ba985f"));
                $('#failureCharttransTOT').attr('data-chart-text', datacreee[0].TotalBooking);
            }

            createDonutCharts();
        }
    }
}
function getsegments(Requested, TotalBooking, color) {
   
    var diff = (parseInt(TotalBooking) - parseInt(Requested));
    var seg = '{ "0":["0","' + Requested + '","' +color+ '"],  "1":["' + Requested + '","' + diff + '","#ecebeb"] }'
    return seg;
}





//Action Required Flight
function GetActionReqFlight(AgencyID, AgencyTypeIDDD) {
    $("#loadergiffl").show();
    //$("#tableActFl").hide();
    var Dastasseqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/GetActionReqFlight",
        data: JSON.stringify(Dastasseqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqFlightSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqFlightSuccess(datacreee) {
        if (datacreee.d != 'NODATA') {
            var Searchdata = $.parseJSON(datacreee.d);
            console.log(Searchdata);
            $('#divactFl').show();
            var searchresf = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                searchresf += '<tr onclick="ActionReqFlightClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                    '</td><td data-toggle="tooltip" data-placement="top" title="Agency Name:' + Searchdata[isd].AgencyName + '">'
                    + Searchdata[isd].FirstName + '</td><td class="onlyhq">' + Searchdata[isd].AgencyName + '</td><td class="no_mobile">' + Searchdata[isd].SupplierBookingReference +
                    '</td><td class="no_mobile">' + Searchdata[isd].IstPax + '</td><td data-toggle="tooltip" data-placement="top" title="'
                    + getAirportNameUTC(Searchdata[isd].DepartureAirportLocationCode) + ' to ' + getAirportNameUTC(Searchdata[isd].ArrivalAirportLocationCode) + '">'
                    + Searchdata[isd].DepartureAirportLocationCode + ' to ' + Searchdata[isd].ArrivalAirportLocationCode + '</td><td data-toggle="tooltip" data-placement="top" title="' + Searchdata[isd].BokingStatName + '">'
                    + Searchdata[isd].BookingStatusCode + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                    '</td><td  data-toggle="tooltip" data-placement="top" title="'
                    + moment(Searchdata[isd].CancellationDeadline).format("ddd-MMM-YYYY") + '">'
                    + moment(Searchdata[isd].CancellationDeadline).format("DD-MM-YY") + '</td></tr>';
            }
            $("#ActReq1").html(searchresf);
            $('[data-toggle="tooltip"]').tooltip();
            //if ($.fn.dataTable.isDataTable('#tableActFl')) {
            //    table = $('#tableActFl').DataTable();
            //}
            //else {
            //    table = $('#tableActFl').DataTable({
            //        "order": [[0, "desc"]]
            //    });
            //}

            //$('#tableActFl').DataTable({
            //    "order": [[0, "desc"]]
            //});
            if (AgencyTypeIDDD != '1') {
                $('.onlyhq').remove();
            }
        }
        else {
            $('#divactFl').hide();
        }
        $("#loadergiffl").hide();
        //$("#tableActFl").show();
    }
}
function getAirportNameUTC(airportcode) {
    var AirportName = _.where(AirlinesTimezone, { I: airportcode });
    if (AirportName == "") {
        AirportName = airportcode;
    }
    else {
        AirportName = AirportName[0].N + ' (' + airportcode + ')';
    }
    return AirportName;
}
function getCityName(citycode) {
    var CItyName = _.where(AreaCityList, { C: citycode, T: 'C' });
    if (CItyName == "") {
        CItyName = citycode;
    }
    else {
        CItyName = CItyName[0].label + ' (' + citycode + ')';
    }
    return CItyName;
}
function ActionReqFlightClick(sid) {
    var Datatable = {
        sessionname: 'BookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqFlightClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function ActionReqFlightClickSuccess() {
    window.location.href = "/amaflight/bookinginfo.html";
}
//Action Required Hotel
function GetActionReqHotel(AgencyID) {
    $("#loadergifht").show();
    //$("#tableActHot").hide();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebserviceHotel.aspx/GetActionReqHotel",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: GetActionReqHotelSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
    function GetActionReqHotelSuccess(datacreeee) {
        if (datacreeee.d != 'NODATA') {
            var Searchdata = $.parseJSON(datacreeee.d);
            console.log(Searchdata);
            $('#divactHot').show();
            var searchresh = '';
            for (var isd = 0; isd < Searchdata.length; isd++) {
                var BookStatName = '';
                if (Searchdata[isd].BookingStatusCode == 'HK') {
                    BookStatName = 'Confirmed';
                }
                else {
                    BookStatName = Searchdata[isd].BokingStatName;
                }
                searchresh += '<tr onclick="ActionReqHotelClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                + Searchdata[isd].AgencyName + '</td><td data-toggle="tooltip" data-placement="top" title="' + getCityName(Searchdata[isd].ItemCityCode) + '">'
                + Searchdata[isd].ItemCityCode + '</td><td>'
                + Searchdata[isd].ItemName + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].CheckInDate).format("ddd-MMM-YYYY") + ' - ' + Searchdata[isd].Nights + ' Nights">'
                + moment(Searchdata[isd].CheckInDate).format("DD-MM-YY") + ' (' + Searchdata[isd].Nights + 'N)</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].timelimit).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].timelimit).format("DD-MM-YY") + '</td><td class="no_mobile" data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + BookStatName + '">'
                + Searchdata[isd].BookingStatusCode + '</td></tr>';
            }
            $("#ActReq2").html(searchresh);
            $('[data-toggle="tooltip"]').tooltip();

            //if ($.fn.dataTable.isDataTable('#tableActHot')) {
            //    table = $('#tableActHot').DataTable();
            //}
            //else {
            //    table = $('#tableActHot').DataTable({
            //        "order": [[0, "desc"]]
            //    });
            //}

            //$('#tableActHot').DataTable({
            //    "order": [[0, "desc"]]
            //});
        }
        else {
            $('#divactHot').hide();
        }
        $("#loadergifht").hide();
        //$("#tableActHot").show();
    }
}
function ActionReqHotelClick(sid) {
    var Datatable = {
        sessionname: 'HotelBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/HOTEL/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: ActionReqHotelClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function ActionReqHotelClickSuccess() {
    window.location.href = "/HOTEL/hotelbookinginfo.html";
}

function GetActionReqInsurance(AgencyID) {
    $("#loadergifht").show();
    //$("#tableActHot").hide();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/Insurance/WebMethodDbInsurance.aspx/GetActionReqInsurance",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchSuccess(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#divactInsu').show();
        var Searchdata = $.parseJSON(datab.d);
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClick(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                + Searchdata[isd].AgencyName + '</td><td>'
                + Searchdata[isd].Premium + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].StartDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].StartDate).format("DD-MM-YY") + '</td><td  data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].EndDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].EndDate).format("DD-MM-YY") + '</td><td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
                + Searchdata[isd].BookingStatusCode + '</td></tr>';

        }
        $("#ActReq4").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
        //$('#tableActInsu').DataTable({
        //    "order": [[0, "desc"]]
        //});
        $('#tableActInsu').show();

    }
    else {
        $('#divactInsu').hide();
    }
    $("#loadergifht").hide();

}
function SearchDetailsClick(sid) {
    var Datatable = {
        sessionname: 'InsuranceBookingRefID',
        valueses: sid
    };
    $.ajax({
        type: "POST",
        url: "/FLIGHT/WebMethodsDB.aspx/SaveDataToSession",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function SearchDetailsClickSuccess() {
    window.location.href = "/Insurance/Policyinfo.html";
}


function GetActionReqTransfer(AgencyID) {
    $("#loadergifht").show();
    //$("#tableActHot").hide();
    var Dastasseqqqqqq = {
        AgencyID: AgencyID
    };
    $.ajax({
        type: "POST",
        url: "/Transfer/Webservicetransfer.aspx/GetActionReqTransfer",
        data: JSON.stringify(Dastasseqqqqqq),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchSuccesstransfer,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchSuccesstransfer(datab) {
    console.log(datab.d);
    if (datab.d != "NODATA") {
        $('#tableActTrans').show();
        var Searchdata = $.parseJSON(datab.d);
        console.log("srf");
        console.log(Searchdata);
        var searchres = '';
        for (var isd = 0; isd < Searchdata.length; isd++) {
            searchres += '<tr onclick="SearchDetailsClickTransfer(' + Searchdata[isd].BookingRefID + ')"><td>' + Searchdata[isd].BookingRefID +
                '</td><td>' + Searchdata[isd].FirstName + '</td><td>'
                + Searchdata[isd].AgencyName + '</td><td>'
                + Searchdata[isd].PNR + '</td>'
              + '<td data-toggle="tooltip" data-placement="top" title="'
                + moment(Searchdata[isd].BookingDate).format("ddd-MMM-YYYY") + '">'
                + moment(Searchdata[isd].BookingDate).format("DD-MM-YY") +
                '</td><td data-toggle="tooltip" data-placement="top" title="' + GetStatus(Searchdata[isd].BookingStatusCode) + '">'
                + Searchdata[isd].BookingStatusCode + '</td></tr>';

        }
        $("#ActReq5").html(searchres);
        $('[data-toggle="tooltip"]').tooltip();
       
        $('#tableActTrans').show();

    }
    else {
        $('#divactTrans').hide();
    }
    $("#loadergifht").hide();

}
function GetStatus(status) {
    var Sat = "";
    if (status == "HK") {
        Sat = "Confirmed";

    }
    else if (status == "RR") {
        Sat = "Confirmation Pending";

    }
    else if (status == "XX") {
        Sat = "Cancelled";
    }
    else if (status == "RC") {
        Sat = "Reconfirmed";

    }
    else if (status == "RQ") {
        Sat = "On request";

    }
    else if (status == "XR") {
        Sat = "Cancellation On request";

    }
    return Sat;
}

function SearchDetailsClickTransfer(sid) {
    var Datatable = {
        value: sid
    };
    $.ajax({
        type: "POST",
        url: "/Transfer/Webservicetransfer.aspx/SaveDataToSessionTransfer",
        data: JSON.stringify(Datatable),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: SearchDetailsClickSuccessTransfer,
        failure: function (response) {
            alert(response.d);
        }
    });
}
function SearchDetailsClickSuccessTransfer() {
    window.location.href = "/Transfer/bookinfo.html";
}

