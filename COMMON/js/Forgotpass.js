﻿$(document).ready(function () {
    $('#btnForgot').click(function myfunction() {
        $('.send_loading').show();
        $(this).attr("disabled", true);
        if ($('#txtEmail').val() != "") {
            var Dastasseq = {
                Email: $('#txtEmail').val()
            };
            console.log(Dastasseq)
            $.ajax({
                type: "POST",
                url: "../Common.aspx/ForgotPass",
                data: JSON.stringify(Dastasseq),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: ForgotSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                statusCode: {
                    500: function () {
                        //window.location.href = "/index.html";
                        //$('#loginbt').click();
                        alert('500');
                    }
                }
            });
        }
        else {
            $("#btnForgot").attr("disabled", false);
            alertify.alert('Validate', 'Please enter email !');
            $('.send_loading').hide();
        }
    });
});
function ForgotSuccess(data) {
    $("#btnForgot").attr("disabled", false);
    if (data.d == "NOUSER") {
        alertify.alert('No User', 'No user registered with this email !');
    }
    else if (data.d == "SUCCESS") {
        alertify.alert('Email', 'Please check your email for password');
        $(".login_form02").slideUp();
        $(".login_form01").slideToggle("800");
    }
    $('.send_loading').hide();
}
