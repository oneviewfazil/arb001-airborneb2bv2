﻿var Setup = {
    Session: {
        AccessToken: "Access_Token",
        LoginUser: "Loginuser",
        UserDetails: "UserDetails",
        AgencyCurrencyExRate: "AgencyCurrencyExRate",
        AgencyCurrencyCode: "AgencyCurrencyCode",
        Url: {
            GetAllPackages: "/Common.aspx/GetAllPackages",
            ClearAllSession: "/Common.aspx/ClearAllSession",
            GetSessionData: "/HOTEL/WebMethodsDB.aspx/GetSessionData",
            ClearSessionData: "/HOTEL/WebMethodsDB.aspx/ClearSessionData",
            GetMarkupDataHotel: "/HOTEL/WebserviceHotel.aspx/GetMarkupDataHotel",
            GetMarkupData: "/HOTEL/WebMethodsDB.aspx/GetMarkupData",
            CheckServiceBlocked: "/HOTEL/WebMethodsDB.aspx/checkServiceBlocked",
            GetActionReqHotel: "/HOTEL/WebserviceHotel.aspx/GetActionReqHotel",
            SaveDataToSession: "/HOTEL/WebMethodsDB.aspx/SaveDataToSession",
            GetCurrencyDetails: "/HOTEL/WebMethodsDB.aspx/GetCurrencyDetails",
            GetSessionDataNew: "/HOTEL/WebMethodsDB.aspx/GetSessionDataNew",
            SaveHotelSequence: "WebserviceHotel.aspx/SaveHotelSequence",
            BindHotelSuppliers: "/HOTEL/WebMethodsDB.aspx/BindHotelSuppliers",
            PostBookingByUserId: "/FLIGHT/Testlogrequest.aspx/PostBookingByUserId",
            PostBooking: "/FLIGHT/Testlogrequest.aspx/PostBooking",
            GetCreditLimit: "/FLIGHT/Testlogrequest.aspx/GetCreditLimit",
            GetTableData: "/HOTEL/WebMethodsDB.aspx/GetTableData",
            UpdateUserData: "/HOTEL/WebMethodsDB.aspx/UpdateUserData",
            UpdateUserPasswordNew: "/HOTEL/WebMethodsDB.aspx/UpdateUserPasswordNew",
            GetServiceMarkupfordebit: "WebMethodDbHotel.aspx/GetServiceMarkupfordebit",
            BookHotel: "/HOTEL/WebserviceHotel.aspx/BookHotel",
            BookHotelStatusUpdate: "../Common.aspx/BookHotelStatusUpdate",
            inserthotelDebitentrydetails: "WebMethodDbHotel.aspx/inserthotelDebitentrydetails",
            Getbookingsession: "/HOTEL/WebMethodDbHotel.aspx/Getbookingsession",
            CancelDevitEntry: "/HOTEL/WebMethodDbHotel.aspx/cancelDevitEntry",
            IssueDevitEntry: "/HOTEL/WebMethodDbHotel.aspx/issueDevitEntry",
            GetStaticXmlHotel: "WebserviceHotel.aspx/GetStaticXmlHotel",
            GetPayDataHotel: "Webservice.aspx/GetPayDataHotel",
            GetHotelSequence: "/HOTEL/WebserviceHotel.aspx/GetHotelSequence",
            BindCotravellerdetails: "WebMethodDbHotel.aspx/BindCotravellerdetails",
            GetSessionDatadet: "/HOTEL/WebMethodsDB.aspx/GetSessionDatadet",
            BookHotelinfonew: "../Common.aspx/BookHotelinfonew",
            SendMailBookhotel: "WebMethodDbHotel.aspx/SendMailBookhotel",
            CancelbookinghotelMail: "/HOTEL/WebMethodDbHotel.aspx/CancelbookinghotelMail",
            VoucherBookhotel: "WebMethodDbHotel.aspx/VoucherBookhotel",
            EmailVoucherBookhotel: "WebMethodDbHotel.aspx/EmailVoucherBookhotel",
            SendMailCancelbooking: "WebMethodDbHotel.aspx/SendMailCancelbooking"

        },
        Pages: {
            HotelBookingInfo: "/HOTEL/hotelbookinginfo.html"
        },
        NoSession: "NOSESSION",
        NoMarkup: "NOMARKUP",
        NoData: "NODATA",
        NoResult: "NORESULT",
        RequestSend: "REQUESTSEND"
    },
    Errors: {
        E100: {
            Details: "SORRY NO RESULTS FOUND",
            DisplayMessage: "Try refining your search criteria and use the navigation above to return to search."
        }
    },

    Common: {
        DomainUrl: "http://oneviewb2bv2.oneviewitsolutions.com",
        LogoPath: "http://oneviewb2bv2.oneviewitsolutions.com/new_login/image/logo.png"
    },
    Agency: {
        AgencyLogo: "http://booking.airbornetrip.com/new_login/image/logo.png",
        AgencyName: "Airborne Travel | Online Booking",
        AgencyShortName: "Airborne",
        AgencyCode: "ARBNHQ001",
        Language: "en",
        AgencyAddress: "Airborne Building, Ghala Heights, Po Box- 167 PC 112 Ruwi, Sultanate of Oman.",
        AgencyWebsite: "www.airbornettc.com",
        Helpline: "+971 2 441 8565",
        Email: "cs@airbornetrip.com",
        AgencyReference: "ONE",
        Facebook: "http://www.fb.com/#",
        Twitter: "http://www.twitter.com/#",
        GooglePlus: "http://plus.google.com",
        Linkedln: "http://www.linkedin.com",
        YouTube: "http://www.youtube.com",
        AboutAgencyShort: "Airborne Travel & Tourism, Incorporated in the year 2003 as an enterprising Travel Company, A Professional, IATA-Accredited Travel Management Company catering to travelers worldwide."
    },
    Flight: {
        FlightService: true,
        PrivacyPolicy: '',
        TermsConditions: '',
        AirlinesPath: "http://booking.airbornetrip.com//amaflight/Flightcomponents/images/Airlines/"
    },
    Hotel: {

    },
    Insurance: {
        ServiceTypeID: '4',
        Service: 'Insurance',
        AgencyShortName: "Airborne Travel",
        Helpline: "+971 2 441 8565",
        Userid: '1',
        Sessionname: 'InsuranceBookingRefID',
        DefaultDate: '0d',
        Loadimage: '/INSURANCE/insuranceimages/insurancenotfound.jpg'

    }
}
